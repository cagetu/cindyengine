//**************************************************************//
//  Effect File exported by RenderMonkey 1.6
//
//  - Although many improvements were made to RenderMonkey FX  
//    file export, there are still situations that may cause   
//    compilation problems once the file is exported, such as  
//    occasional naming conflicts for methods, since FX format 
//    does not support any notions of name spaces. You need to 
//    try to create workspaces in such a way as to minimize    
//    potential naming conflicts on export.                    
//    
//  - Note that to minimize resulting name collisions in the FX 
//    file, RenderMonkey will mangle names for passes, shaders  
//    and function names as necessary to reduce name conflicts. 
//**************************************************************//

//--------------------------------------------------------------//
// Ambient Occlusion Effect Group
//--------------------------------------------------------------//
//--------------------------------------------------------------//
// AmbientOcclusion
//--------------------------------------------------------------//
//--------------------------------------------------------------//
// Background
//--------------------------------------------------------------//
string Ambient_Occlusion_Effect_Group_AmbientOcclusion_Background_Sphere : ModelData = "C:\\Program Files\\ATI Research Inc\\RenderMonkey 1.62\\Examples\\Media\\Models\\Sphere.3ds";

float4 view_position;
float4x4 proj_matrix : Projection;
float4x4 view_proj_matrix;
float time_scale
<
   string UIName = "time_scale";
   string UIWidget = "Numeric";
   bool UIVisible =  true;
   float UIMin = -16.00;
   float UIMax = 16.00;
> = float( 4.00 );
float time_0_2PI : Time0_2PI;
struct VS_OUTPUT
{
   float4 Pos: POSITION;
   float3 texCoord: TEXCOORD0;
};

VS_OUTPUT Ambient_Occlusion_Effect_Group_AmbientOcclusion_Background_Vertex_Shader_main(float4 Pos: POSITION)
{
   VS_OUTPUT Out;

   // Lock the environment around the camera but rotate it slowly around y
   // 
   // Effectively multiply by the following matrix:
   //
   // [ cos(time)      0   -sin(time)]
   // [    0           1       0     ]
   // [ sin(time)      0    cos(time)]
   //

   // Compute rotated position
   float4 rotPos = Pos;
   rotPos.x = dot(Pos.xyz, float3(cos(time_scale*time_0_2PI), 0, -sin(time_scale*time_0_2PI)));
   rotPos.z = dot(Pos.xyz, float3(sin(time_scale*time_0_2PI), 0,  cos(time_scale*time_0_2PI)));

   Out.Pos = mul(proj_matrix, rotPos);


   Out.texCoord = Pos;

   return Out;
}







texture GraceCathedral_Tex
<
   string ResourceName = "C:\\Program Files\\ATI Research Inc\\RenderMonkey 1.62\\Examples\\Media\\Textures\\grace_cube.dds";
>;
sampler Environment = sampler_state
{
   Texture = (GraceCathedral_Tex);
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MAGFILTER = LINEAR;
   MINFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
float4 Ambient_Occlusion_Effect_Group_AmbientOcclusion_Background_Pixel_Shader_main(float3 texCoord: TEXCOORD) : COLOR
{
   return texCUBE(Environment, texCoord);
}



//--------------------------------------------------------------//
// Statue
//--------------------------------------------------------------//
string Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Statue : ModelData = "C:\\Program Files\\ATI Research Inc\\RenderMonkey 1.62\\Examples\\Media\\Models\\Hebe.3ds";

float4x4 Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_view_proj_matrix : ViewProjection;
float4x4 inv_view_matrix : ViewInverse;
float Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_0_2PI : Time0_2PI;
float Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_scale
<
   string UIName = "Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_scale";
   string UIWidget = "Numeric";
   bool UIVisible =  true;
   float UIMin = -16.00;
   float UIMax = 16.00;
> = float( 4.00 );
struct Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_VS_OUTPUT
{
   float4 Pos    : POSITION;
   float3 N      : TEXCOORD0;
   float3 Neye   : TEXCOORD1;
   float2 tcBase : TEXCOORD2;
};

Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_VS_OUTPUT Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_main(float4 Pos: POSITION, float3 vNormal: NORMAL, float2 tcBase:TEXCOORD0)
{
   Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_VS_OUTPUT Out = (Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_VS_OUTPUT) 0;

   // Tiny little model .. give it some size and center it better
   Out.Pos    = mul(Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_view_proj_matrix, Pos);

   float3 Nworld = mul(vNormal, inv_view_matrix); // Intrinsic argument ordering effectively multiplies by transpose of inverse view matrix

   // Lock the environment around the camera but rotate it slowly around y
   // 
   // Effectively multiply by the following matrix:
   //
   // [ cos(time)      0   -sin(time)]
   // [    0           1       0     ]
   // [ sin(time)      0    cos(time)]
   //

   // Rotate the normal back the other way so lighting is correct
   float3 rotNorm = Nworld;
   rotNorm .x = dot(Nworld.xyz, float3(cos(-Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_scale*Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_0_2PI), 0, -sin(-Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_scale*Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_0_2PI)));
   rotNorm .z = dot(Nworld.xyz, float3(sin(-Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_scale*Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_0_2PI), 0,  cos(-Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_scale*Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_time_0_2PI)));

   Out.N      = rotNorm ;

   Out.tcBase = tcBase;

   return Out;
}








texture AmbientOcclusion_Tex
<
   string ResourceName = "C:\\Program Files\\ATI Research Inc\\RenderMonkey 1.62\\Examples\\Media\\Textures\\AmbientOcclusion_1024.dds";
>;
sampler AmbOcclusion = sampler_state
{
   Texture = (AmbientOcclusion_Tex);
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MAGFILTER = LINEAR;
   MINFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
texture GraceDiffuse_Tex
<
   string ResourceName = "C:\\Program Files\\ATI Research Inc\\RenderMonkey 1.62\\Examples\\Media\\Textures\\grace_diffuse_cube.dds";
>;
sampler DiffuseEnvironment = sampler_state
{
   Texture = (GraceDiffuse_Tex);
};
float4 Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Pixel_Shader_main(float3 N: TEXCOORD0, float3 Neye : TEXCOORD1, float2 tcBase: TEXCOORD2) : COLOR
{
   // Sample the filtered environment map
   float4 ambient = texCUBE(DiffuseEnvironment, N);


   // Sample the ambient occlusion map
   float  ambientOcclusion = tex2D(AmbOcclusion, tcBase).r;

   return ambient * ambientOcclusion;
}







//--------------------------------------------------------------//
// Technique Section for Ambient Occlusion Effect Group
//--------------------------------------------------------------//
technique AmbientOcclusion
{
   pass Background
   {
      ZWRITEENABLE = FALSE;
      CULLMODE = NONE;

      VertexShader = compile vs_1_1 Ambient_Occlusion_Effect_Group_AmbientOcclusion_Background_Vertex_Shader_main();
      PixelShader = compile ps_1_1 Ambient_Occlusion_Effect_Group_AmbientOcclusion_Background_Pixel_Shader_main();
   }

   pass Statue
   {
      ZWRITEENABLE = TRUE;
      SRCBLEND = SRCALPHA;
      DESTBLEND = INVSRCALPHA;
      CULLMODE = CCW;
      ALPHABLENDENABLE = FALSE;

      VertexShader = compile vs_1_1 Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Vertex_Shader_main();
      PixelShader = compile ps_2_0 Ambient_Occlusion_Effect_Group_AmbientOcclusion_Statue_Pixel_Shader_main();
   }

}

