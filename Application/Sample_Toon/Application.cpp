#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderGroup.h>
#include <Cindy/Material/CnTextureGroup.h>

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Scene/CnModelNode.h>


CApplication::CApplication()
{
	m_pMeshDataGroup = new CnMeshDataGroup();
	m_pMaterialDataGroup = new CnMaterialDataGroup();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	CnRenderWindow* pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
															   nWidth, nHeight, usColorDepth,
															   usRefreshRate, bFullScreen,
															   bThreadSafe, bSetCurrentTarget );

	// Setup Scene..
	SetScene( _T("Scene") );

	// Camera
	m_spCamera = CnNew CnModelViewCamera( L"main " );
	m_spCamera->SetPosition( CnVector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 1000.0f );
	m_spCamera->SetRadius( 10.0f, 500.0f );
	m_spCamera->SetRadius( 0.0f );
	m_spCamera->SetLookAt( CnVector3( 0.0f, 10.0f, 0.0f ) );

	// viewport 1
	CnViewport* pViewport = pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnNew CnLight( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	CnVector3 position( 0.0f, 10.0f, -10.0f );
	CnVector3 direction( 0.3f, 0.4f, 1.0f );
	direction.Normalize();
	light->SetDirection( direction );

	light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.0f, 0.0f, 0.0f, 0.0f ) );

	CnSceneNode* lightNode = CnNew CnSceneNode( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );

	m_pSceneGraph->AttachChild( lightNode );

	return pRenderWindow;
}

void CApplication::SetData()
{
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnShaderGroup::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );
	CnTextureGroup::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	MeshDataPtr spMesh = m_pMeshDataGroup->Load( L"aa.mmf" );
	MtlDataPtr spMaterial = m_pMaterialDataGroup->Load( L"aa.mtl" );

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"atoom1", 0xffff );
	//modelnode->AttachSkeleton( spSkeleton );
	modelnode->AttachMesh( spMesh );
	modelnode->AttachMaterial( L"aa.mmf", spMaterial );

	modelnode->Yaw( RADIAN(20.0f) );
	m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	modelnode = CnNew CnModelNode( L"test2", 0xffff );
	modelnode->AttachMesh( spMesh );
	modelnode->AttachMaterial( L"aa.mmf", spMaterial );

	modelnode->SetPosition( CnVector3( 2.0f, 2.0f, 0.0f ) );
	modelnode->Yaw( RADIAN(-20.0f) );
	m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	//modelnode = CnNew CnModelNode( L"test2", 0xffff );
	//modelnode->SetPosition( CnVector3( 5.0f, 5.0f, 0.0f ) );
	//modelnode->AttachMesh( spMesh, false );
	//m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	//// ground
	//MeshDataPtr spGound = meshGroup->Load( L"Data\\base_grid.mmf" );
	//modelnode = CnNew CnModelNode( L"ground", 0xffff );
	//modelnode->AttachMesh( spGound, false );

	//m_pSceneGraph->GetRoot()->AttachChild( modelnode );
}

void CApplication::Run()
{
	if (::GetAsyncKeyState('W'))
	{
		CnVector3 dir;
		m_spCamera->GetDirection( dir );
		
		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		CnVector3 dir;
		m_spCamera->GetDirection( dir );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		CnVector3 right;
		m_spCamera->GetRight( right );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		CnVector3 right;
		m_spCamera->GetRight( right );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
	CnFramework::Run();
}


//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}