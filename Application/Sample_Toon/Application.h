#pragma once

#include <Cindy/CnFramework.h>
#include <Cindy/Geometry/CnMeshDataGroup.h>
#include <Cindy/Material/CnMaterialDataGroup.h>
#include <Cindy/Scene/CnCameraImpl.h>
#include <Cindy/System/CnMouse.h>

class CApplication : public CnFramework
{
private:
	MdvCamPtr			m_spCamera;

	CnMouse				m_Mouse;
	short				m_nRotX;
	short				m_nRotY;

	CnMeshDataGroup*		m_pMeshDataGroup;
	CnMaterialDataGroup*	m_pMaterialDataGroup;
public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*	AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
								  int nWidth, int nHeight, ushort usColorDepth,
								  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
								  bool bSetCurrentTarget = false ) override;

	void			SetData();
	void			Run() override;

	// MsgProc
	virtual bool	MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) override;
};