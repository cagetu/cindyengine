#include "stdafx.h"
#include "Application.h"

#include <Cindy/Graphics/Base/CnRenderWindow.h>
#include <Cindy/Graphics/Base/CnRenderTexture.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Lighting/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>
#include <Cindy/Material/Fragment/CnFragmentManager.h>

//----------------------------------------------------------------
/**
*/
CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

//----------------------------------------------------------------
/**
*/
CnRenderWindow* CApplication::AttachWindow( HWND hWnd,
											const wchar_t* pStrTitle,
											int nWidth,
											int nHeight,
											ushort usColorDepth, 
										    ushort usRefreshRate,
											bool bFullScreen,
											bool bThreadSafe,
											bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd,
												   pStrTitle,
												   nWidth,
												   nHeight,
												   usColorDepth,
												   usRefreshRate,
												   bFullScreen,
												   bThreadSafe,
												   bSetCurrentTarget );
	m_pRenderWindow->Active(true);
	return m_pRenderWindow;
}

//----------------------------------------------------------------
/**	@brief	초기화
*/
bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	// Set Data Path
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\" );

	// Setup Scene..
	SetScene( _T("Scene") );
	SetSceneGraph();

	// Scene
	CnRenderTexture* sceneTarget = RenderDevice->CreateRenderTexture( L"SceneToTexture",
																	  m_pRenderWindow->GetWidth(),
																	  m_pRenderWindow->GetHeight(),
																	  m_pRenderWindow->GetColorDepth(),
																	  PixelFormat::A16B16G16R16F );
	if (sceneTarget)
	{
		// viewport 1
		CnViewport* pViewport = sceneTarget->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetScene( m_pWorld );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	}

	// Depth
	CnRenderTexture* depthTarget = RenderDevice->CreateRenderTexture( L"SceneDepth",
																	  m_pRenderWindow->GetWidth(),
																	  m_pRenderWindow->GetHeight(),
																	  m_pRenderWindow->GetColorDepth(),
																	  PixelFormat::A8R8G8B8 );
	if (depthTarget)
	{
		Ptr<CnScene> newScene = m_pCindy->AddScene( L"SceneDepth" );
		Ptr<Scene::Component> sceneUpdator = m_pWorld->GetComponent( L"SceneUpdator" );

		Scene::RenderDepth* depthRenderer = Scene::RenderDepth::Create();
		depthRenderer->Link( sceneUpdator );

		//depthRenderer->Open();
		depthRenderer->SetShader(L"SolidTemplate.fx");

		newScene->AddComponent( sceneUpdator );
		newScene->AddComponent( Scene::BeginScene::Create() );
		newScene->AddComponent( depthRenderer );
		newScene->AddComponent( Scene::EndScene::Create() );

		// viewport 1
		CnViewport* pViewport = depthTarget->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetScene( newScene );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	}

	/// SSAO
	CnRenderTexture* ssaoTarget = RenderDevice->CreateRenderTexture( L"ssaoTarget",
																	 m_pRenderWindow->GetWidth(),
																	 m_pRenderWindow->GetHeight(),
																	 m_pRenderWindow->GetColorDepth(),
																	 PixelFormat::A8R8G8B8 );
	if (ssaoTarget)
	{
		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		screenQuad->Setup( m_pRenderWindow->GetWidth(), m_pRenderWindow->GetHeight() );

		CnMaterial* mtl = screenQuad->GetMaterial();
		mtl->SetTexture( MapType::DiffuseMap0, L"SceneDepth" );
		mtl->SetTexture( MapType::DiffuseMap1, L"vector_noise.dds" );
		mtl->SetShader( L"SSAO.fx" );
		//mtl->SetCurrentTechnique( L"t0" );

		// Scene
		CnScene* ssaoScene = m_pCindy->AddScene( L"ssaoScene" );
		ssaoScene->AddComponent( CnNew Scene::BeginScene() );
		ssaoScene->AddComponent( screenQuad );
		ssaoScene->AddComponent( CnNew Scene::EndScene() );

		CnViewport* pViewport = ssaoTarget->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( ssaoScene );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 100, 0, 0 ) );
	}

	// Debug용 Scene
	Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
	screenQuad->Setup( m_pRenderWindow->GetWidth(), m_pRenderWindow->GetHeight() );
	{
		CnMaterial* mtl = screenQuad->GetMaterial();

		mtl->SetTexture( MapType::DiffuseMap0, L"ssaoTarget" );
		mtl->SetShader( L"pe_compose.fx" );
		mtl->SetCurrentTechnique( L"RenderScreenQuad" );

		// Scene
		CnScene* mainScene = m_pCindy->AddScene( L"mainScene" );
		mainScene->AddComponent( Scene::BeginScene::Create() );
		mainScene->AddComponent( screenQuad );
		mainScene->AddComponent( Scene::RenderText::Create() );
		mainScene->AddComponent( Scene::EndScene::Create() );

		CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( mainScene );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 100, 0, 0 ) );
	}

	return true;
}

//----------------------------------------------------------------
/** Set Scene
    @remarks      장면 설정
	@param        strName : 장면 이름
	@return       none
*/
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->AddScene( strName.c_str() );

	// Camera
	m_spCamera = CnModelViewCamera::Create();
	m_spCamera->SetName( L"main" );

	m_spCamera->SetPosition( Math::Vector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 1000.0f );
	m_spCamera->SetRadius( 1.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::Create();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	Scene::UpdateScene* sceneUpdator = Scene::UpdateScene::Create();
	sceneUpdator->AddSceneGraph( m_pSceneGraph );

	Scene::RenderModel* modelRenderer = Scene::RenderModel::Create();
	modelRenderer->Link( sceneUpdator );

	Scene::RenderAABB* aabbRenderer = Scene::RenderAABB::Create();
	aabbRenderer->Link( sceneUpdator );

	m_pWorld->AddComponent( sceneUpdator );
	m_pWorld->AddComponent( Scene::BeginScene::Create() );
	m_pWorld->AddComponent( modelRenderer );
	m_pWorld->AddComponent( aabbRenderer );
	m_pWorld->AddComponent( Scene::EndScene::Create() );
}

//----------------------------------------------------------------
/**	@brief
*/
void CApplication::SetSceneGraph()
{
	// Light
	CnLight* light = CnLight::Create();
	light->SetName( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	Math::Vector3 position( 10.0f, 10.0f, -10.0f );
	Math::Vector3 direction = position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

	CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );
	lightNode->EnableCulling( false );

	//SetData();
	SetData2();
}

//----------------------------------------------------------------
/**	@brief	Data 설정
*/
void CApplication::SetData()
{
	// 아툼
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	// 반고
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	// 엘프
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\Tex\\" );

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\Ground\\" );

	MeshDataPtr spMesh;
	MtlDataPtr spMaterial;
	SkelDataPtr spSkeleton;

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
	m_pSceneGraph->AttachChild( modelnode );

	CnString skeleton( L"bango_rush_m.mbf" );
	CnString meshName( L"all.mmf" );
	CnString mtlName( L"newExpAll_mtl.xml" );
	CnString animName1( L"bango_m_rush_common_dance.maf" );
	CnString animName2( L"bango_m_rush_common_attack1.maf" );
	CnString animName3( L"bango_m_rush_common_run.maf" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"all.mmf", L"newExpAll_mtl.xml" );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model2" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( meshName, mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, -10.0f) );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model3" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"newExpAll.mmf", mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 10.0f) );

		// Animation
		//AnimDataPtr ani = m_pAnimationDataMgr->Load( animName3.c_str() );
		//Ptr<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, ani );
		//modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}
	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model4" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"newExpAll.mmf", L"newExpAll2_mtl.xml" );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 20.0f) );

		// Animation
		AnimDataPtr spAnimation1 = m_pAnimationDataMgr->Load( animName1.c_str() );
		//AnimDataPtr spAnimation2 = m_pAnimationDataMgr->Load( animName2.c_str() );

		Ptr<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, spAnimation1 );
		//channel1->GetMotion()->Lock( L"Bip01", L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 L Calf" );
		//channel1->GetMotion()->Lock( L"Bip01 R Calf" );
		//channel1->SetMixtureRatio( 0.001f );

		//Ptr<CnAnimChannel> channel2 = modelnode->AddAnimation( 1, spAnimation2 );
		//channel2->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel2->SetMixtureRatio( 0.2f );

		modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}

	//CnSceneNode* rootNode = m_pSceneGraph->GetRoot();
	//CnMaterialState* mtlState = CnMaterialState::Create();
	//mtlState->SetDiffuse( CnColor(1.0f, 0.5f, 0.0f) );
	//rootNode->AttachGlobalState( mtlState );
	//rootNode->UpdateRS();
}

//----------------------------------------------------------------
/**	@brief	Data 설정
*/
void CApplication::SetData2()
{
	CnModelNode* modelnode = NULL;

	// 베룬
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"베룬" );
	{
		modelnode->AddMesh( L"berun.mmf", L"berun(UberShader).mtl");
		modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );
		m_pSceneGraph->AttachChild( modelnode );
	}


	// 마커스
	//{
	//	m_pMeshDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
	//	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
	//	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
	//	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
	//	TextureMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );

	//	modelnode = CnModelNode::Create();
	//	modelnode->SetName( L"Marcus" );
	//	{
	//		SkelDataPtr skeleton = m_pSkeletonDataMgr->Load( L"marcus.mbf" );
	//		modelnode->AddSkeleton( skeleton );
	//		modelnode->AddMesh( L"marcus.mmf", L"marcus_.mtl");
	//		m_pSceneGraph->AttachChild( modelnode );

	//		// Animation
	//		AnimDataPtr ani = m_pAnimationDataMgr->Load( L"marcus.maf" );
	//		Ptr<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, ani );
	//		modelnode->GetAnimController()->Play();
	//		//modelnode->SetScale( Math::Vector3(0.01f, 0.01f, 0.01f) );
	//		//modelnode->SetPosition( Math::Vector3( 0.0f, 0.0f, 30.0f ));
	//	}
	//}

	//// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	TextureMgr->AddDataPath( L"..\\Data\\Ground\\" );

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
	m_pSceneGraph->AttachChild( modelnode );

}

//----------------------------------------------------------------
/**	@brief	
*/
void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//----------------------------------------------------------------
/**	@brief	
*/
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}