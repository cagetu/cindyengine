#include "stdafx.h"
#include "Application.h"

#include <Cindy/Graphics/Base/CnRenderWindow.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Lighting/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>
#include <Cindy/Material/Fragment/CnFragmentManager.h>

IMPLEMENT_MEM_POOL( Test )

CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd, pStrTitle,
												    nWidth, nHeight, usColorDepth,
												    usRefreshRate, bFullScreen,
												    bThreadSafe, bSetCurrentTarget );
	m_pRenderWindow->Active(true);
	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	// Setup Scene..
	SetScene( _T("Scene") );

	// Set Data Path
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	// Camera
	m_spCamera = CnModelViewCamera::Create();
	m_spCamera->SetName( L"main " );

	m_spCamera->SetPosition( Math::Vector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 10000.0f );
	m_spCamera->SetRadius( 1.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnLight::Create();
	light->SetName( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	Math::Vector3 position( 10.0f, 10.0f, -10.0f );
	Math::Vector3 direction = position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

	CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );
	lightNode->EnableCulling( false );

	// Shader Generator...
	FragmentShader::Manager* fragmentMgr = FragmentShader::Manager::Create();
	fragmentMgr->SetDataPath( L"..\\..\\sdk\\Shader\\fragment\\Works\\" );

	fragmentMgr->Load( L"Input.xml" );
	fragmentMgr->Load( L"Output.xml" );
	fragmentMgr->Load( L"TransformStatic.xml" );
	fragmentMgr->Load( L"TransformSkinned.xml" );
	fragmentMgr->Load( L"DiffuseMap.xml" );
	fragmentMgr->Load( L"Multiply(vs).xml" );
	fragmentMgr->Load( L"Multiply(ps).xml" );
	fragmentMgr->Load( L"Add(vs).xml" );
	fragmentMgr->Load( L"Add(ps).xml" );
	fragmentMgr->Load( L"Color.xml" );

	fragmentMgr->Compile( L"..\\..\\sdk\\Shader\\fragment\\Works\\Compile.xml" );

	return true;
}

//================================================================
/** Set Scene
    @remarks      장면 설정
	@param        strName : 장면 이름
	@return       none
*/
//================================================================
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->AddScene( strName.c_str() );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::Create();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	Scene::UpdateScene* sceneUpdator = Scene::UpdateScene::Create();
	sceneUpdator->AddSceneGraph( m_pSceneGraph );

	Scene::RenderModel* modelRenderer = Scene::RenderModel::Create();
	modelRenderer->Link( sceneUpdator );

	Scene::RenderAABB* aabbRenderer = Scene::RenderAABB::Create();
	aabbRenderer->Link( sceneUpdator );

	m_pWorld->AddComponent( sceneUpdator );
	m_pWorld->AddComponent( Scene::BeginScene::Create() );
	m_pWorld->AddComponent( modelRenderer );
	m_pWorld->AddComponent( aabbRenderer );
	m_pWorld->AddComponent( Scene::EndScene::Create() );
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}