// Sample_GFx.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "Sample_GFx.h"
#include "Application.h"

#define MAX_LOADSTRING 100

// 전역 변수:
HINSTANCE hInst;								// 현재 인스턴스입니다.
TCHAR szTitle[MAX_LOADSTRING];					// 제목 표시줄 텍스트입니다.
TCHAR szWindowClass[MAX_LOADSTRING];			// 기본 창 클래스 이름입니다.

CApplication* g_pApplication = NULL;

// 이 코드 모듈에 들어 있는 함수의 정방향 선언입니다.
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: 여기에 코드를 입력합니다.
	MSG msg;
	HACCEL hAccelTable;

	// 전역 문자열을 초기화합니다.
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SAMPLE_GFX, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	g_pApplication = new CApplication();
	g_pApplication->Initialize();

	// 응용 프로그램 초기화를 수행합니다.
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	g_pApplication->SetData();

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SAMPLE_GFX));

	// 기본 메시지 루프입니다.
	PeekMessage( &msg, NULL, 0U, 0U, PM_NOREMOVE );

	BOOL getmsg;
	while (WM_QUIT != msg.message)
	{
		if (g_pApplication)
		{
			getmsg = PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE );
			if (getmsg)
			{
				BOOL imeUIMsg = false;
				HWND hWndIME = 0;

				// This is necessary to hide the composition string pop up window on windows Vista. 
				hWndIME = ImmGetDefaultIMEWnd(msg.hwnd);
				ShowOwnedPopups(hWndIME, false);
				imeUIMsg = ImmIsUIMessage(NULL, msg.message, msg.wParam, msg.lParam);

				if ((msg.message == WM_KEYDOWN) || (msg.message == WM_KEYUP) || imeUIMsg)
				{
					if (CGFxManager::Instance())
					{
						CGFxManager::Instance()->OnIMEEvent( msg.message, msg.wParam, msg.lParam, msg.hwnd, true );
					}
				}

				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}
			else
			{
				g_pApplication->Run();
				//!! Exception 처리를 안한 상태이므로 거기에 대한 정책이 서면 추가하시오
			}
		}
		else
		{
			if (GetMessage(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	SAFEDEL( g_pApplication );

	return (int) msg.wParam;
}



//
//  함수: MyRegisterClass()
//
//  목적: 창 클래스를 등록합니다.
//
//  설명:
//
//    Windows 95에서 추가된 'RegisterClassEx' 함수보다 먼저
//    해당 코드가 Win32 시스템과 호환되도록
//    하려는 경우에만 이 함수를 사용합니다. 이 함수를 호출해야
//    해당 응용 프로그램에 연결된
//    '올바른 형식의' 작은 아이콘을 가져올 수 있습니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SAMPLE_GFX));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SAMPLE_GFX);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   목적: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   설명:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 640, 480, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   g_pApplication->AttachWindow( hWnd, szTitle, 640, 480, 32, 60, false, true, true );

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


enum KeyModifiers
{
    KM_Control = 0x1,
    KM_Alt     = 0x2,
    KM_Shift   = 0x4,
    KM_Num     = 0x8,
    KM_Caps    = 0x10,
    KM_Scroll  = 0x20
};

static int GetModifiers()
{
    int new_mods = 0;
    if (::GetKeyState(VK_SHIFT) & 0x8000)
        new_mods |= KM_Shift;
    if (::GetKeyState(VK_CONTROL) & 0x8000)
        new_mods |= KM_Control;
    if (::GetKeyState(VK_MENU) & 0x8000)
        new_mods |= KM_Alt;
    if (::GetKeyState(VK_NUMLOCK) & 0x1)
        new_mods |= KM_Num;
    if (::GetKeyState(VK_CAPITAL) & 0x1)
        new_mods |= KM_Caps;
    if (::GetKeyState(VK_SCROLL) & 0x1)
        new_mods |= KM_Scroll;

    return new_mods;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  목적: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND	- 응용 프로그램 메뉴를 처리합니다.
//  WM_PAINT	- 주 창을 그립니다.
//  WM_DESTROY	- 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// 메뉴의 선택 영역을 구문 분석합니다.
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 여기에 그리기 코드를 추가합니다.
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

    case WM_KEYDOWN:
    case WM_KEYUP:
        {
			GFxKey::Code kc((GFxKey::Code)wParam);

			//AB, in the case key is not found in the table let give a chance to 
			// plain key code, not just return GFxKey::VoidSymbol;
			if (wParam >= 'A' && wParam <= 'Z')
			{
				kc = (GFxKey::Code) ((wParam - 'A') + GFxKey::A);
			}
			else if (wParam >= VK_F1 && wParam <= VK_F15)
			{
				kc = (GFxKey::Code) ((wParam - VK_F1) + GFxKey::F1);
			}
			else if (wParam >= '0' && wParam <= '9')
			{
				kc = (GFxKey::Code) ((wParam - '0') + GFxKey::Num0);
			}
			else if (wParam >= VK_NUMPAD0 && wParam <= VK_DIVIDE)
			{
				kc = (GFxKey::Code) ((wParam - VK_NUMPAD0) + GFxKey::KP_0);
			}
			else 
			{
				// many keys don't correlate, so just use a look-up table.
				static struct //!AB added static modifier
				{
					int         vk;
					GFxKey::Code    gs;
				} table[] =
				{
					{ VK_RETURN,    GFxKey::Return },
					{ VK_ESCAPE,    GFxKey::Escape },
					{ VK_LEFT,      GFxKey::Left },
					{ VK_UP,        GFxKey::Up },
					{ VK_RIGHT,     GFxKey::Right },
					{ VK_DOWN,      GFxKey::Down },

					// @@ TODO fill this out some more
					{ 0, GFxKey::VoidSymbol }
				};

				for (int i = 0; table[i].vk != 0; i++)
				{
					if (wParam == (UInt)table[i].vk)
					{
						kc = table[i].gs;
						break;
					}
				}
			}

			unsigned char asciiCode = 0;
			if (kc != GFxKey::VoidSymbol)
			{
				// get the ASCII code, if possible.
				UINT uScanCode = ((UINT)lParam >> 16) & 0xFF; // fetch the scancode
				BYTE ks[256];
				WORD charCode;

				// Get the current keyboard state
				::GetKeyboardState(ks);

				if (::ToAscii((UINT)wParam, uScanCode, ks, &charCode, 0) > 0)
				{
					//!AB, what to do if ToAscii returns > 1?
					asciiCode = LOBYTE (charCode);
				}

				GFxKeyEvent Event((message == WM_KEYDOWN) ? GFxEvent::KeyDown : GFxKeyEvent::KeyUp, kc, asciiCode);
				Event.SpecialKeysState.SetShiftPressed((::GetKeyState(VK_SHIFT) & 0x8000) ? 1: 0);
				Event.SpecialKeysState.SetCtrlPressed((::GetKeyState(VK_CONTROL) & 0x8000) ? 1: 0);
				Event.SpecialKeysState.SetAltPressed((::GetKeyState(VK_MENU) & 0x8000) ? 1: 0);
				Event.SpecialKeysState.SetNumToggled((::GetKeyState(VK_NUMLOCK) & 0x1) ? 1: 0);
				Event.SpecialKeysState.SetCapsToggled((::GetKeyState(VK_CAPITAL) & 0x1) ? 1: 0);
				Event.SpecialKeysState.SetScrollToggled((::GetKeyState(VK_SCROLL) & 0x1) ? 1: 0);

				CGFxManager::Instance()->ActiveMovie()->HandleEvent( Event );
			}
			return 0;
        }
    case WM_CHAR:
        {
            UInt32 wcharCode = (UInt32)wParam;

			GFxCharEvent ev( wcharCode );
			CGFxManager::Instance()->ActiveMovie()->HandleEvent( ev );
        }
        break;

    case WM_IME_SETCONTEXT:
        // This is an attempt to hide Windows IME UI Windows.
        lParam = 0;
        break;

    case WM_IME_STARTCOMPOSITION:
    case WM_IME_KEYDOWN:
    case WM_IME_COMPOSITION:        
    case WM_IME_ENDCOMPOSITION:
    case WM_IME_NOTIFY:
    case WM_IME_CHAR:        // This message should not be passed down to DefWinProc otherwise it will
                             // generate WM_CHAR messages which will cause text duplication.
        {
			if (CGFxManager::Instance())
			{
				if (CGFxManager::Instance()->OnIMEEvent( message, wParam, lParam, hWnd, false ))
					return 0;
			}
        }
		break;

	case WM_MOUSEMOVE:
		{
			short xMouse = (short)LOWORD(lParam);
			short yMouse = (short)HIWORD(lParam);

			if (g_pApplication)
			{
				CGFxManager::Instance()->NotifyMouseState( xMouse, yMouse );

				//GFxMouseEvent ev( GFxEvent::MouseMove, 0, xMouse, yMouse, 0.0f );
				//CGFxManager::Instance()->ActiveMovie()->HandleEvent( ev );
			}
		}
		break;
	case WM_LBUTTONDOWN:
		{
			if (g_pApplication)
			{
				g_pApplication->m_bMouseLButtonDown = true;
			}
			//m_bMouseLButtonDown = true;

			short mouseX = LOWORD(lParam);
			short mouseY = HIWORD(lParam);
		}
		break;
	case WM_LBUTTONUP:
		{
			if (g_pApplication)
			{
				g_pApplication->m_bMouseLButtonDown = false;
			}
		}
		break;

	case WM_RBUTTONDOWN:
		{
			if (g_pApplication)
			{
				g_pApplication->m_bMouseRButtonDown = true;
			}

			short mouseX = LOWORD(lParam);
			short mouseY = HIWORD(lParam);
		}
		break;
	case WM_RBUTTONUP:
		{
			if (g_pApplication)
			{
				g_pApplication->m_bMouseRButtonDown = false;
			}
		}
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
