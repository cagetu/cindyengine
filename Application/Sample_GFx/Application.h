#pragma once

#include <Cindy/CnFramework.h>
#include <Cindy/Geometry/CnMeshDataGroup.h>
#include <Cindy/Material/CnMaterialDataGroup.h>

#include <Cindy/Scene/CnSceneComponentImpl.h>
#include <Cindy/Scene/CnSceneGraph.h>

#include "GFxMain.h"

// ���...
class CGFxScene : public SceneComponent
{
	static SceneID		ms_Name;

	CGFxManager		m_GFxManager;
public:
	virtual ~CGFxScene();

	const CGFxManager&	GetGFx() const	{	return m_GFxManager;	}

	const SceneID&		GetID() const	{	return ms_Name;			}

	virtual void	Update() override;
	virtual void	Run( CnCamera* pCamera ) override;
};

// Application
class CApplication : public CnFramework
{
private:
	CnCamera*			m_pCamera;

	CnMeshDataGroup*		m_pMeshDataGroup;
	CnMaterialDataGroup*	m_pMaterialDataGroup;

public:
	bool		m_bMouseLButtonDown;
	bool		m_bMouseMButtonDown;
	bool		m_bMouseRButtonDown;

	//--------------------------------------------------------------
	//	Methods
	//--------------------------------------------------------------
	void	SetScene( const CnString& strName );
public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*		AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
									  int nWidth, int nHeight, ushort usColorDepth,
									  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
									  bool bSetCurrentTarget = false ) override;

	void		SetData();
	void		Run() override;
};