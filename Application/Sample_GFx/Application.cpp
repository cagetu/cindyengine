#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderGroup.h>
#include <Cindy/Material/CnTextureGroup.h>

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnCamera.h>

//--------------------------------------------------------------
//	Class GFxScene
//--------------------------------------------------------------
SceneID CGFxScene::ms_Name = _T("GFxScene");
CGFxScene::~CGFxScene()
{
}

//--------------------------------------------------------------
void CGFxScene::Update()
{
}

//--------------------------------------------------------------
void CGFxScene::Run( CnCamera* pCamera )
{
	m_GFxManager.Run();
}

//--------------------------------------------------------------
//	Class Application
//--------------------------------------------------------------
CApplication::CApplication()
{
	m_pMeshDataGroup = new CnMeshDataGroup();
	m_pMaterialDataGroup = new CnMaterialDataGroup();

	m_bMouseLButtonDown = false;
	m_bMouseMButtonDown = false;
	m_bMouseRButtonDown = false;
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
}

//--------------------------------------------------------------
CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	CnRenderWindow* pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
															   nWidth, nHeight, usColorDepth,
															   usRefreshRate, bFullScreen,
															   bThreadSafe, bSetCurrentTarget );
	// Setup Scene..
	SetScene( _T("Scene") );

	m_pCamera = m_pCindy->CreateCamera( L"main" );
	m_pCamera->SetPosition( CnVector3( 0.0f, 7.0f, -14.0f ) );

	// viewport 1
	CnViewport* pViewport = pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_pCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnNew CnLight( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	CnVector3 position( 0.0f, 10.0f, -10.0f );
	CnVector3 direction = -position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 0.8f, 0.8f, 0.8f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.0f, 0.0f, 0.0f, 0.0f ) );

	CnSceneNode* lightNode = CnNew CnSceneNode( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );

	m_pSceneGraph->AttachChild( lightNode );

	// GFx
	CGFxManager::Instance()->SetupCallback( *new CGFxCallback );
	CGFxManager::Instance()->SetupFileOpener( *new CGFxFileOpener );

	LPDIRECT3DDEVICE9 device = NULL;
	CnRenderer::Instance()->GetCurrentDevice( &device );

	D3DPRESENT_PARAMETERS test;
	CnRenderer::Instance()->GetCurrentPresentParams( &test );

	CGFxManager::Instance()->SetupRenderer( pRenderWindow->GetHWnd(), device, &test );
	CGFxManager::Instance()->SetupFontProvider();
	CGFxManager::Instance()->SetupUserEventHandler( *new CGFxUserEventHandler() );

	GPtr<CGFxMovie> movie = CGFxManager::Instance()->Load( "..\\Data\\Flash\\Test2.swf" );
	//GPtr<CGFxMovie> movie = CGFxManager::Instance()->Load( "..\\Data\\Flash\\IMESample.swf" );
	movie->SetViewport( 0, 0, nWidth, nHeight );

	return pRenderWindow;
}

//--------------------------------------------------------------
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->CreateScene( L"Scene" );
	
	// 장면 그래프
	m_pSceneGraph = new CnSceneGraph( strName );

	SceneComponentImpl::UpdateScene* sceneUpdator = CnNew SceneComponentImpl::UpdateScene();
	sceneUpdator->Register( m_pSceneGraph );

	SceneComponentImpl::RenderModel* modelRenderer = CnNew SceneComponentImpl::RenderModel();
	modelRenderer->Link( sceneUpdator );

	m_pWorld->Register( sceneUpdator );
	m_pWorld->Register( CnNew SceneComponentImpl::BeginScene() );
	m_pWorld->Register( modelRenderer );
	m_pWorld->Register( CnNew SceneComponentImpl::EndScene() );
	m_pWorld->Register( CnNew CGFxScene() );
}

//--------------------------------------------------------------
void CApplication::SetData()
{
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnShaderGroup::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );
	CnTextureGroup::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	MeshDataPtr spMesh = m_pMeshDataGroup->Load( L"atoom.mmf" );
	MtlDataPtr spMaterial = m_pMaterialDataGroup->Load( L"atoom_mtl.xml" );

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"atoom1", 0xffff );
	modelnode->AttachMesh( spMesh );
	modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
	m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	//modelnode = CnNew CnModelNode( L"test2", 0xffff );
	//modelnode->SetPosition( CnVector3( 5.0f, 5.0f, 0.0f ) );
	//modelnode->AttachMesh( spMesh );
	//m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	//// ground
	//MeshDataPtr spGound = meshGroup->Load( L"Data\\base_grid.mmf" );
	//modelnode = CnNew CnModelNode( L"ground", 0xffff );
	//modelnode->AttachMesh( spGound, false );

	//m_pSceneGraph->GetRoot()->AttachChild( modelnode );
}

void CApplication::Run()
{
	if (::GetAsyncKeyState('W'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.z += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('S'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.z -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('D'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.x += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );		
	}

	if (::GetAsyncKeyState('A'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.x -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState(VK_UP))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.y += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}
	if (::GetAsyncKeyState(VK_DOWN))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.y -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('O'))
	{
		GPtr<CGFxMovie> movie = CGFxManager::Instance()->Find( "..\\Data\\Flash\\Test2.swf" );

		// IME를 활성화 시킬 때, SetFocus 이벤트 처리를 해준 녀석의 IME만 활성화 된다.
		//movie->GetMovie()->HandleEvent( GFxEvent::SetFocus );

		//movie->GetMovie()->Invoke( "active", " %d %s", 10, "HAHAHA,12345,QBCDE,이건 머야 이거,나 하나만 사랑해준 그대 어디에도 찾을 수 없겠죠 그대 하나만 바라볼게요 내 사랑아 oh my my girl 여태껏 그녀만한 여자는 없다고 여지껏 난 그녀만한 내 사랑은 본 적 없다고 하루에도 몇번을 난 그녀를 생각하면 방긋 이제부터 나는 너만 볼래 볼래 사랑할래 이리 저리 어딜봐도 내겐 정말 넌 이뻐 uh 오늘 하루 나는 너만 보기 바뻐 숨이 가퍼 난 뛸 듯이 기뻐 난 oh 니가 있어 난 행복하구나 사랑의 멜로디가 너를 부르고 사랑의 그 가사가 바로 우리 얘기고 너와 내가 만들어 갈 아름답고 아름다운 사랑의 멜로디 세상에서 가장 날 설레 설레게 하는 그대 어떡해 좋아 좋아 난 니가 좋아 널 보면 정말 사랑해 사랑해 말을 하고 싶어져 어쩌면 좋아 몰래 몰래 그대와 입맞추고 살며시 그대 그대 품에 안기고 말해줘 나만 사랑해 준다고 곁에 있어 준다고 꼭 듣고 싶어 하루종일 니가 생각이 났어", 2 );
		//movie->GetMovie()->Invoke( "active", "%d %s", 3, "Test,1234, 가나다라", 2 );
	}

	int MouseButtons = 0;
	if ( m_bMouseLButtonDown )
	{
		MouseButtons |= 1;
	}

	if ( m_bMouseMButtonDown )
	{
		MouseButtons |= 1 << 1;
	}

	if ( m_bMouseRButtonDown )
	{
		MouseButtons |= 1 << 2;
	}

	CGFxManager::Instance()->NotifyMouseButtons( MouseButtons );

	CnFramework::Run();
}
