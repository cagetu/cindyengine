#include "stdafx.h"
#include "GFxMovie.h"

//==================================================================
//	Class GFxMovie
//==================================================================
CGFxMovie::CGFxMovie( GFxLoader& Loader, const char* pFileName )
{
	m_pDevice = 0;
	m_nPosX = 0;
	m_nPosY = 0;
	m_nWidth = 0;
	m_nHeight = 0;
	m_bTransparent = false;

	m_fFrameFrequency = 0.8f;

	gfc_strcpy(m_strFileName, 256, pFileName);

	// Get info about the width & height of the movie.
	if (Loader.GetMovieInfo( pFileName, &m_MovieInfo ))
	{
		// 실제 movie를 읽는다.
		m_spMovieDef = *Loader.CreateMovie( pFileName );
		if (!m_spMovieDef)
			return;

		// instance 생성
		m_spMovie = *m_spMovieDef->CreateInstance();
		if (!m_spMovie)
			return;

		m_nWidth = m_MovieInfo.Width;
		m_nHeight = m_MovieInfo.Height;

		m_ulStartTicks = timeGetTime();
		m_ulLastAdvanceTicks = m_ulStartTicks;
		m_ulNextAdvanceTicks = m_ulStartTicks;
		m_bAdvanceCounter = false;

		// All around the communication between flash and the program
		//http://www.scaleform.com/forums/viewtopic.php?t=247
		//http://www.scaleform.com/forums/viewtopic.php?t=182
	}
}
CGFxMovie::~CGFxMovie()
{
	if (m_pDevice)
		m_pDevice->Release();
}

//-------------------------------------------------------------------------
/** @brief	Movie에 대해 Renderer를 설정한다 */
//-------------------------------------------------------------------------
void CGFxMovie::SetRenderConfig( GFxRenderConfig* pRenderConfig )
{
	if (!m_spMovie)
		return;

	m_spRenderConfig = pRenderConfig;

	GRendererD3D9* renderer = (GRendererD3D9*)pRenderConfig->GetRenderer();
	LPDIRECT3DDEVICE9 newDevice = renderer->GetDirect3DDevice();
	
	newDevice->AddRef();
	if (m_pDevice)
		m_pDevice->Release();

	m_pDevice = newDevice;
}

//-------------------------------------------------------------------------
/** @brief	Set the viewport. Only necessary for non-stransparent rendering */
//-------------------------------------------------------------------------
void CGFxMovie::SetViewport( int nPosX, int nPosY, int nWidth, int nHeight, bool bTransparent )
{
	m_nPosX = nPosX;
	m_nPosY = nPosY;

	m_nWidth = (nWidth <= 0) ? m_MovieInfo.Width : nWidth;
	m_nHeight = (nHeight <= 0) ? m_MovieInfo.Height : nHeight;

	m_bTransparent = bTransparent;
}

//-------------------------------------------------------------------------
/** @brief	마우스 정보 설정 */
//-------------------------------------------------------------------------
void CGFxMovie::NotifyMouseState( int nPosX, int nPosY, int nButtons )
{
	m_nMouseX = nPosX;
	m_nMouseY = nPosY;
	m_nMouseButtons = nButtons;
}

//-------------------------------------------------------------------------
/** @brief	그리기 */
//-------------------------------------------------------------------------
void CGFxMovie::Render( unsigned long Tick )
{
	// Note: second condition can occur in first frame of movie if it was just loaded,
    // and the cached ticks value is passed from a loop
	if ((Tick == -1) || (m_ulLastAdvanceTicks > Tick))
		return;

	if (Tick > m_ulNextAdvanceTicks)
	{
		int deltaTicks = Tick - m_ulLastAdvanceTicks;
		float delta = deltaTicks * 0.001f;

		// 다음 advance ticks을 계산, a bit higher rate then necessary
		m_ulNextAdvanceTicks = Tick + (UInt32) ((m_fFrameFrequency * 1000.0f) / m_MovieInfo.FPS);
		m_ulLastAdvanceTicks = Tick;

		m_spMovie->Advance( delta );

		m_spMovie->NotifyMouseState( m_nMouseX, m_nMouseY, m_nMouseButtons );
	}

	// flash를 그릴 영역
	m_spMovie->SetViewport( m_nWidth, m_nHeight, m_nPosX, m_nPosY, m_nWidth, m_nHeight );

	// 반드시 viewport 변경 후에 clear 해야 한다.
	float alpha = m_bTransparent ? 0.0f : 1.0f;
	m_spMovie->SetBackgroundAlpha( alpha );

	// 랜더링 준비(알파 테스트를 사용할 수 없다)
	m_spMovie->Display();
}