#include "stdafx.h"
#include "GFxMain.h"
#include <GFxFontProviderWin32.h>

//=============================================================================
// Class GFxIMEEvent
//=============================================================================
//-----------------------------------------------------------------------------
/** @brief	IME Manager 생성 */
//-----------------------------------------------------------------------------
GFxIMEManager* CGFxIMEManager::CreateManager( HWND hWnd )
{
	GFxIMEManager* imeManager = new GFxIMEManagerWin32( hWnd );
	return imeManager;
}

//-----------------------------------------------------------------------------
/** @brief	 OnEvent */
//-----------------------------------------------------------------------------
bool CGFxIMEManager::OnEvent( const FxIMEEvent& Event, GFxMovieView* pMovie )
{
	if (!pMovie)
		return false;

	// 새로운 입력을 넣을 수 있도록 한다.
	if (Event.Preprocess)
	{
		GFxIMEWin32Event ev( GFxIMEEvent::IME_PreProcessKeyboard, (UPInt)Event.hWND, Event.message, Event.wParam, Event.lParam, 0 );
		pMovie->HandleEvent( ev );
		return true;
	}

	// 입력된 글자를 넣는다.
	GFxIMEWin32Event ev( GFxIMEEvent::IME_Default, (UPInt)Event.hWND, Event.message, Event.wParam, Event.lParam, true );

	UInt handleEventRetval = pMovie->HandleEvent( ev );
	return (handleEventRetval & GFxMovieView::HE_NoDefaultAction ) != 0;
}

CGFxManager* CGFxManager::ms_pInstance = NULL;
//==================================================================
//	Class GFxManager
//==================================================================
CGFxManager::CGFxManager()
{
	ms_pInstance = this;
}

//-------------------------------------------------------------------------
/** @brief	GFx Renderer 설정 */
//-------------------------------------------------------------------------
bool CGFxManager::SetupRenderer( HWND hWnd, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS* pd3dPP )
{
	if (m_spRenderer = GRendererD3D9::CreateRenderer())
	{
		m_spRenderer->SetDependentVideoMode( pDevice, pd3dPP, 0, hWnd );
	}
	else
	{
		return false;
	}

	// 모든 자식들에게 적용하기 위해, loader에 renderer를 설정한다.
	m_spRenderConfig = *new GFxRenderConfig( m_spRenderer );
	m_Loader.SetRenderConfig( m_spRenderConfig );

	// 기본 안티 알리아싱
	m_spRenderConfig->SetRenderFlags( m_spRenderConfig->GetRenderFlags() | GFxRenderConfig::RF_EdgeAA );

	// 통계를 처리하기 위해 랜더러의 수치 관련 객체 생성
	m_spRenderStats = *new GFxRenderStats();
	m_Loader.SetRenderStats( m_spRenderStats );

	m_pDevice = pDevice;

	GPtr<GFxTranslator> translator = *new Translator();
	m_Loader.SetTranslator( translator );
	return true;
}

//-------------------------------------------------------------------------
/** @brief	안티 알리아싱 설정 */
//-------------------------------------------------------------------------
void CGFxManager::SetAntiAliasing( CGFxManager::AAModeType eType )
{
	if (m_spRenderConfig.GetPtr() == NULL)
		return;

	switch (eType)
	{
	case None:
		m_eAAModeType = EdgeAA;
		break;

	case EdgeAA:
		m_eAAModeType = FSAA;
		break;

	case FSAA:
		m_eAAModeType = None;
		break;
	}

	ulong renderFlags = m_spRenderConfig->GetRenderFlags() & ~GFxRenderConfig::RF_EdgeAA;
	if (m_eAAModeType == EdgeAA)
	{
		renderFlags |= GFxRenderConfig::RF_EdgeAA;
	}

	m_spRenderConfig->SetRenderFlags( renderFlags );

	// Renderer 설정해야함...
}

//-------------------------------------------------------------------------
/** @brief	ParseControl 설정
			GFxParseControl은 로딩하는 동안 Generate되는 SWF/GFX 파일이 메세지 파싱(parsing)하는 것을 결정에 사용.

*/
//-------------------------------------------------------------------------
void CGFxManager::SetupParseControl( CGFxManager::ParseType Type )
{
	if (NULL != m_Loader.GetParseControl().GetPtr())
		return;

	GPtr<GFxParseControl> parseControl = *new GFxParseControl(Type);

	m_Loader.SetParseControl( parseControl );
}

//-------------------------------------------------------------------------
/** @brief	Log 설정 */
//-------------------------------------------------------------------------
void CGFxManager::SetupLog( GPtr<GFxLog> spLog )
{
	if (!m_Loader.GetLog())
		return;

	m_Loader.SetLog( spLog );
}

//-------------------------------------------------------------------------
/** @brief	Callback 설정 */
//-------------------------------------------------------------------------
void CGFxManager::SetupCallback( GPtr<GFxFSCommandHandler> spHandler )
{
	if (NULL != m_Loader.GetFSCommandHandler().GetPtr())
		return;

	m_Loader.SetFSCommandHandler( spHandler );
}

//-------------------------------------------------------------------------
/** @brief	FileOpener 설정 */
//-------------------------------------------------------------------------
void CGFxManager::SetupFileOpener( GPtr<GFxFileOpener> spOpener )
{
	if (NULL != m_Loader.GetFileOpener().GetPtr())
		return;

	m_Loader.SetFileOpener( spOpener );
}

//-------------------------------------------------------------------------
/** @brief	UserEventHandler 설정 */
//-------------------------------------------------------------------------
void CGFxManager::SetupUserEventHandler( GPtr<GFxUserEventHandler> spUserEventHandler )
{
	if (NULL != m_Loader.GetUserEventHandler().GetPtr())
		return;

	m_Loader.SetUserEventHandler( spUserEventHandler );
}

//-------------------------------------------------------------------------
/** @brief	윈도우에 시스템 폰트 제공자를 생성한다.
			만약, 이것을 설정하지 않으면, 시스템 폰트 문자들은 사각형으로 출력된다.
*/
//-------------------------------------------------------------------------
void CGFxManager::SetupFontProvider()
{
	GPtr<GFxFontProviderWin32> fontProvider = *new GFxFontProviderWin32(::GetDC(0));
	m_Loader.SetFontProvider( fontProvider );
}

//-------------------------------------------------------------------------
/** @brief	IME 설정 */
//-------------------------------------------------------------------------
void CGFxManager::SetupIME( HWND hWnd )
{
	GFxIMEManager* imeManager = CGFxIMEManager::CreateManager( hWnd );
	if (imeManager)
	{
		imeManager->SetIMEMoviePath( "..\\Data\\Flash\\IME.swf" );
		m_Loader.SetIMEManager( imeManager );

		// Loader keeps the object from this point
		imeManager->Release();
	}
}

//-----------------------------------------------------------------------------
/** @brief	IME Event */
//-----------------------------------------------------------------------------
bool CGFxManager::OnIMEEvent( UINT message, WPARAM wParam, LPARAM lParam, HWND hWnd, bool bPreProcess )
{
	FxIMEEvent ev(bPreProcess);
	ev.message = message;
	ev.wParam = wParam;
	ev.lParam = lParam;
	ev.hWND = hWnd;

	return CGFxIMEManager::OnEvent( ev, m_spActiveMovie );
}

//-------------------------------------------------------------------------
/** @brief	Flash 로딩 */
//-------------------------------------------------------------------------
GPtr<CGFxMovie> CGFxManager::Load( const char* strFileName )
{
	GPtr<CGFxMovie> movie = *new CGFxMovie( m_Loader, strFileName );
	movie->SetViewport( 0, 0, 800, 600 );

	movie->SetRenderConfig( m_spRenderConfig );

	m_aMovies.push_back( movie );

	m_spActiveMovie = movie->GetMovie();
	return movie;
}

GPtr<CGFxMovie> CGFxManager::Find( const char* strFileName )
{
	for (int i=0; i < m_aMovies.size(); ++i)
	{
		if (!gfc_stricmp( strFileName, m_aMovies[i]->GetFileName() ))
			return m_aMovies[i];
	}

	return NULL;
}

//-------------------------------------------------------------------------
/** @brief	마우스 정보 설정 */
//-------------------------------------------------------------------------
void CGFxManager::NotifyMouseState( int nPosX, int nPosY )
{
	m_nMouseX = nPosX;
	m_nMouseY = nPosY;
}

void CGFxManager::NotifyMouseButtons( int nButtons )
{
	m_nMouseButtons = nButtons;
}

//-------------------------------------------------------------------------
/** @brief	Run */
//-------------------------------------------------------------------------
void CGFxManager::Run()
{
	static ulong ticks;
	static ulong startTicks = timeGetTime();

	ticks = timeGetTime();

	if (m_spRenderer)
	{	// 3D 랜더링이 필요할 때에 설정된다.
		GRendererD3D9::DisplayStatus status = m_spRenderer->CheckDisplayStatus();
		if (status == GRendererD3D9::DisplayStatus_Unavailable)
		{	// 나중에 다시 이용..
			::Sleep(10);
			return;
		}

		if (status == GRendererD3D9::DisplayStatus_NeedsReset)
		{
			// Render Target 재설정..
		}
	}

	size_t numMovies = m_aMovies.size();
	for (size_t i=0; i<numMovies; ++i)
	{
		m_aMovies[i]->NotifyMouseState( m_nMouseX, m_nMouseY, m_nMouseButtons );
		m_aMovies[i]->Render( ticks );
	}

	//m_aMovies.clear();
}

//==================================================================
//	Class GFxCallback 
//==================================================================
void CGFxCallback::Callback( GFxMovieView* pMovie, const char* pCommand, const char* pArg )
{
	if (!strcmp(pCommand, "test"))
	{
	}
}

//==================================================================
//	Class GFxUserEventHandler
//==================================================================
void CGFxUserEventHandler::HandleEvent( GFxMovieView* pMovie, const class GFxEvent& event )
{
	switch (event.Type)
	{
	case GFxEvent::SetFocus:
#ifdef _DEBUG
		OutputDebugString( _T("Focus") );
#endif
		break;
	}
}

bool Translator::Translate( GFxWStringBuffer* pbuffer, const wchar_t *pkey )
{
	int a = 1;
	return false;
}