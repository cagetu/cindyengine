#pragma once

//==================================================================
/** GFxMovie
	@author		cagetu
	@since		2008년 5월 6일
	@remarks	Flash 하나에 대한 객체

*/
//==================================================================
class CGFxMovie : public GRefCountBase<CGFxMovie>
{
private:
	//------------------------------------------------------
	//	Variables
	//------------------------------------------------------
    // Loaded movie data
    char					m_strFileName[256];
    GFxMovieInfo			m_MovieInfo;
    GPtr<GFxMovieDef>		m_spMovieDef;
    GPtr<GFxMovieView>		m_spMovie;

	// Renderer
	GPtr<GFxRenderConfig>	m_spRenderConfig;
	LPDIRECT3DDEVICE9		m_pDevice;

	// SWF display 위치와 크기
	int						m_nPosX;
	int						m_nPosY;
	int						m_nWidth;
	int						m_nHeight;
	bool					m_bTransparent;

	// 프레임 스피드 비율 (1.0은 같다. 0.8은 20% 빠르게)
	float					m_fFrameFrequency;

	//
	int						m_nMouseX;
	int						m_nMouseY;
	int						m_nMouseButtons;

	// play 하는 동안 사용되는 시간 변수
    UInt32					m_ulStartTicks;
    UInt32					m_ulLastAdvanceTicks;
    UInt32					m_ulNextAdvanceTicks;
    int						m_bAdvanceCounter;
public:
	CGFxMovie( GFxLoader& Loader, const char* pFileName );
	~CGFxMovie();

	const char*			GetFileName() const		{	return m_strFileName;	}
	GPtr<GFxMovieView>	GetMovie() const		{	return m_spMovie;		}

	void	SetRenderConfig( GFxRenderConfig* pRenderConfig );
	void	SetViewport( int nPosX, int nPosY, int nWidth = -1, int nHeight = -1, bool bTransparent = true );

	void	SetFrameFrequency( float fFrequency )	{	m_fFrameFrequency = fFrequency;		}

	void	NotifyMouseState( int nPosX, int nPosY, int nButtons );

	void	Render( unsigned long Tick = -1 );
};