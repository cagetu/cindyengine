#pragma once

#include "GFxMovie.h"

#include <GFxIMEManager.h>
#include <GFxIMEManagerWin32.h>

#pragma comment (lib, "imm32.lib")
#pragma comment (lib, "uuid.lib")

struct FxIMEEvent
{
	FxIMEEvent(bool preprocess = false) : Preprocess(preprocess) {}

	UINT        message;
	WPARAM      wParam;
	LPARAM      lParam;
	HWND        hWND;
	bool        Preprocess;
};

class CGFxIMEManager
{
public:
	static GFxIMEManager* CreateManager( HWND hWnd );
	static bool OnEvent( const FxIMEEvent& Event, GFxMovieView* pMovie );
};

//==================================================================
/** GFxManager
	@author		cagetu
	@since		2008년 5월 6일
	@remarks	GFx의 모든 것을 관리

*/
//==================================================================
class CGFxManager
{
	enum AAModeType
	{
		None,
		EdgeAA,
		FSAA,
	};

	// 로딩 관련 메시지 파싱 Flag
    enum ParseConstants
    {
        ParseNone        = 0x00,
        Parse            = 0x01,
        ParseAction      = 0x02,
        ParseShape       = 0x10,
        ParseMorphShape  = 0x20,
        ParseAllShapes   = ParseShape | ParseMorphShape,
        ParseAll         = Parse | ParseAction | ParseAllShapes
    };

	typedef ulong	ParseType;

private:
	//------------------------------------------------------
	//	Variables
	//------------------------------------------------------
	GFxLoader				m_Loader;

	GPtr<GRendererD3D9>		m_spRenderer;
	GPtr<GFxRenderConfig>	m_spRenderConfig;
	GPtr<GFxRenderStats>	m_spRenderStats;

	AAModeType				m_eAAModeType;

	LPDIRECT3DDEVICE9		m_pDevice;

	//
	int						m_nMouseX;
	int						m_nMouseY;
	int						m_nMouseButtons;

	GPtr<GFxMovieView>				m_spActiveMovie;

	GTL::garray< GPtr<CGFxMovie> >	m_aMovies;

	static CGFxManager*		ms_pInstance;

public:
	CGFxManager();

	bool		SetupRenderer( HWND hWnd, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS* pd3dPP );
	void		SetAntiAliasing( AAModeType eType );

	void		SetupParseControl( ParseType Type = ParseNone );
	void		SetupLog( GPtr<GFxLog> spLog );
	void		SetupCallback( GPtr<GFxFSCommandHandler> spHandler );
	void		SetupFileOpener( GPtr<GFxFileOpener> spOpener );
	void		SetupUserEventHandler( GPtr<GFxUserEventHandler> spUserEventHandler );
	void		SetupFontProvider();
	void		SetupIME( HWND hWnd );

	bool		OnIMEEvent( UINT message, WPARAM wParam, LPARAM lParam, HWND hWnd, bool bPreProcess);

	GPtr<GFxMovieView>	ActiveMovie() const		{	return m_spActiveMovie;		}

	GPtr<CGFxMovie>		Load( const char* strFileName );
	GPtr<CGFxMovie>		Find( const char* strFileName );

	void		NotifyMouseState( int nPosX, int nPosY );
	void		NotifyMouseButtons( int nButtons );

	void		Run();

	static CGFxManager*	Instance()	{	return ms_pInstance;	}
};

//==================================================================
// File Opener Class
class CGFxFileOpener : public GFxFileOpener
{
public:
	virtual GFile*	OpenFile( const char* pStrFileName )
	{
		return new GSysFile( pStrFileName );

		// 검색을 최적화하기 해서, 더 빠르게 사용하기 위한 Buffered file wrapper
        //return new GBufferedFile( GPtr<GSysFile>( *new GSysFile(pStrFileName) ) );

		//return new GMemoryFile(purl, fxplayer_swf, sizeof(fxplayer_swf));
	}
};

//==================================================================
// "fscommand" callback, 액션 스크립트(action script)로 부터 핸들 통지가 돌아온다.
class CGFxCallback : public GFxFSCommandHandler
{
public:
	virtual void	Callback( GFxMovieView* pMovie, const char* pCommand, const char* pArg );
};

//==================================================================
//
class CGFxUserEventHandler : public GFxUserEventHandler
{
public:
	virtual void	HandleEvent( GFxMovieView* pMovie, const class GFxEvent& event ) override;
};

class Translator : public GFxTranslator
{
public:
	virtual bool Translate(GFxWStringBuffer* pbuffer, const wchar_t *pkey);
};