#include "stdafx.h"
#include "Application.h"

#include <Cindy/Graphics/Base/CnRenderWindow.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Lighting/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>
#include <Cindy/Material/Fragment/CnFragmentManager.h>

IMPLEMENT_MEM_POOL( Test )

CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd, pStrTitle,
													nWidth, nHeight, usColorDepth,
													usRefreshRate, bFullScreen,
													bThreadSafe, bSetCurrentTarget );
	m_pRenderWindow->Active(true);
	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	// Setup Scene..
	SetScene( _T("Scene") );

	// Set Data Path
	ShaderMgr->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	// Camera
	m_spCamera = CnModelViewCamera::Create();
	m_spCamera->SetName( L"main " );

	m_spCamera->SetPosition( Math::Vector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 10000.0f );
	m_spCamera->SetRadius( 1.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnLight::Create();
	light->SetName( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	Math::Vector3 position( 10.0f, 10.0f, -10.0f );
	Math::Vector3 direction = position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

	CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );
	lightNode->EnableCulling( false );

	SetData();
	//SetData2();

	//DWORD time = timeGetTime();
	//for (int i=0; i<1000; ++i)
	//{
	//	Test* t = CnNew Test;
	//}

	//DWORD result = timeGetTime() - time;
	//wchar buffer[256];
	//swprintf( buffer, L"pool: %d\n", result );
	//OutputDebugString( buffer );

	//time = timeGetTime();
	//for (int i=0; i<1000; ++i)
	//{
	//	TestB* t = new TestB;
	//}

	//result = timeGetTime() - time;
	//swprintf( buffer, L"heap: %d\n", result );
	//OutputDebugString( buffer );

	//FragmentShader::Manager* fragmentMgr = FragmentShader::Manager::Create();
	//fragmentMgr->SetDataPath( L"..\\..\\sdk\\Shader\\fragment\\Works\\" );

	//fragmentMgr->Load( L"Input.xml" );
	//fragmentMgr->Load( L"Output.xml" );
	//fragmentMgr->Load( L"TransformStatic.xml" );
	//fragmentMgr->Load( L"TransformSkinned.xml" );
	//fragmentMgr->Load( L"DiffuseMap.xml" );
	//fragmentMgr->Load( L"Multiply(vs).xml" );
	//fragmentMgr->Load( L"Multiply(ps).xml" );
	//fragmentMgr->Load( L"Add(vs).xml" );
	//fragmentMgr->Load( L"Add(ps).xml" );
	//fragmentMgr->Load( L"Color.xml" );

	//fragmentMgr->Compile( L"..\\..\\sdk\\Shader\\fragment\\Works\\Compile.xml" );

	return true;
}

//================================================================
/** Set Scene
    @remarks      장면 설정
	@param        strName : 장면 이름
	@return       none
*/
//================================================================
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->AddScene( strName.c_str() );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::Create();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	Scene::UpdateScene* sceneUpdator = Scene::UpdateScene::Create();
	sceneUpdator->AddSceneGraph( m_pSceneGraph );

	Scene::RenderModel* modelRenderer = Scene::RenderModel::Create();
	modelRenderer->Link( sceneUpdator );

	Scene::RenderAABB* aabbRenderer = Scene::RenderAABB::Create();
	aabbRenderer->Link( sceneUpdator );

	m_pWorld->AddComponent( sceneUpdator );
	m_pWorld->AddComponent( Scene::BeginScene::Create() );
	m_pWorld->AddComponent( modelRenderer );
	m_pWorld->AddComponent( aabbRenderer );
	m_pWorld->AddComponent( Scene::EndScene::Create() );
}


void CApplication::SetData()
{
	// 아툼
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	TextureMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	// 반고
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	TextureMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	// TeamFortress2 - medic
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\medic\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\medic\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\medic\\" );
	TextureMgr->AddDataPath( L"..\\Data\\medic\\" );

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	TextureMgr->AddDataPath( L"..\\Data\\Ground\\" );

	MeshDataPtr spMesh;
	MtlDataPtr spMaterial;
	SkelDataPtr spSkeleton;

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
	m_pSceneGraph->AttachChild( modelnode );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	{
		modelnode->AddSkeleton( L"medic.mbf" );
		modelnode->AddMesh( L"medic.mmf", L"medic.mtl" );

		m_pSceneGraph->AttachChild( modelnode );
	}

	CnString skeleton( L"bango_rush_m.mbf" );
	CnString meshName( L"bango_rush_m.mmf" );
	CnString mtlName( L"bango_rush_m.mtl" );
	CnString animName1( L"bango_m_rush_common_dance.maf" );
	CnString animName2( L"bango_m_rush_common_attack1.maf" );
	CnString animName3( L"bango_m_rush_common_run.maf" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"bango_rush_m.mmf", L"newExpAll_mtl.xml" );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model2" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( meshName, mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, -10.0f) );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model3" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"bango_rush_m.mmf", mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 10.0f) );

		// Animation
		//AnimDataPtr ani = m_pAnimationDataMgr->Load( animName3.c_str() );
		//Pointer<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, ani );
		//modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}
	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model4" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"bango_rush_m.mmf", L"newExpAll2_mtl.xml" );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 20.0f) );

		// Animation
		AnimDataPtr spAnimation1 = m_pAnimationDataMgr->Load( animName1.c_str() );
		//AnimDataPtr spAnimation2 = m_pAnimationDataMgr->Load( animName2.c_str() );

		Pointer<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, spAnimation1 );
		//channel1->GetMotion()->Lock( L"Bip01", L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 L Calf" );
		//channel1->GetMotion()->Lock( L"Bip01 R Calf" );
		//channel1->SetMixtureRatio( 0.001f );

		//Pointer<CnAnimChannel> channel2 = modelnode->AddAnimation( 1, spAnimation2 );
		//channel2->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel2->SetMixtureRatio( 0.2f );

		modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}

	//CnSceneNode* rootNode = m_pSceneGraph->GetRoot();
	//CnMaterialState* mtlState = CnMaterialState::Create();
	//mtlState->SetDiffuse( CnColor(1.0f, 0.5f, 0.0f) );
	//rootNode->AttachGlobalState( mtlState );
	//rootNode->UpdateRS();
}

void CApplication::SetData2()
{
	// 베룬
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	TextureMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\" );

	// 아크레시아
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	TextureMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );

	CnModelNode* modelnode = NULL;

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"베룬" );
	{
		modelnode->AddMesh( L"berun.mmf", L"berun.mtl");
		modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );
		m_pSceneGraph->AttachChild( modelnode );
	}

	//modelnode = CnModelNode::Create();
	//modelnode->SetName( L"rf" );
	//{
	//	SkelDataPtr skeleton = m_pSkeletonDataMgr->Load( L"mmBel.mbf" );
	//	modelnode->AddSkeleton( skeleton );
	//	modelnode->AddMesh( L"mmBel.mmf", L"mmBel_mtl.xml");
	//	//modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );

	//	// Animation
	//	AnimDataPtr animation1 = m_pAnimationDataMgr->Load( L"mmBel.maf" );

	//	Pointer<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, animation1 );
	//	modelnode->GetAnimController()->Play();

	//	m_pSceneGraph->AttachChild( modelnode );
	//}
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}