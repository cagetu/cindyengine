#include "stdafx.h"
#include "AnimationSystem.h"
#include <Cagetu/Core/Component/ComponentManager.h>

namespace SubSystem
{
	//IMPLEMENT_CLASS( SubSystem, AnimationSystem, Core::SubSystem<Component::IAnimation>, 0 );
	IMPLEMENT_COMPONENT_SYSTEM( AnimationSystem );
	REGISTER_COMPONENT_SYSTEM( AnimationSystem );
	//------------------------------------------------------------------
	// Const/Dest
	AnimationSystem::AnimationSystem()
	{
	}
	AnimationSystem::~AnimationSystem()
	{
	}

	//------------------------------------------------------------------
	/** @brief	������Ʈ */
	//------------------------------------------------------------------
	void AnimationSystem::Process()
	{
		ComponentList::iterator i, iend;
		iend = m_Components.end();
		for (i=m_Components.begin(); i!=iend; ++i)
		{
			(i->second)->Update();
		}
	}

}