#pragma once

#include "../Components/CollisionComponent.h"
#include <Cagetu/Core/Component/SubSystem.h>
#include <MoCommon/MoSingleton.h>

namespace SubSystem
{
	// 입력 시스템
	class CollisionSystem : public Core::SubSystem<Component::ICollision>,
							public MoSingleton<CollisionSystem>
	{
		//DECLARE_CLASS(CollisionSystem);
		DECLARE_COMPONENT_SYSTEM;
	public:
		CollisionSystem();
		virtual ~CollisionSystem();

		//Core::Priority	GetPriority() const override	{	return 2;	}

		virtual void	Process() override;
	};
}

#define SubSystemOfCollision	SubSystem::CollisionSystem::GetSingletonPtr()
