#pragma once

#include <Cagetu/Core/Component/SubSystem.h>
#include <MoCommon/MoSingleton.h>
#include "../Components/TransformComponent.h"

namespace SubSystem
{
	// 변환 시스템
	class TransformSystem : public Core::SubSystem<Component::ITransform>,
							public MoSingleton<TransformSystem>
	{
		//DECLARE_CLASS(TransformSystem);
		DECLARE_COMPONENT_SYSTEM;
	public:
		TransformSystem();
		virtual ~TransformSystem();

		Core::Priority	GetPriority() const override	{	return 2;			}

		void			Process() override;

		static TransformSystem*	Instance();
	};
}

#define SubSystemOfTransform	SubSystem::TransformSystem::GetSingletonPtr()
