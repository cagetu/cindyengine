#pragma once

#include "../Components/TimerComponent.h"
#include <Cagetu/Core/Component/SubSystem.h>
#include <MoCommon/MoSingleton.h>

namespace SubSystem
{
	// 타이머 시스템
	class TimerSystem : public Core::SubSystem<Component::ITimer>,
						public MoSingleton<TimerSystem>
	{
		DECLARE_COMPONENT_SYSTEM;
	public:
		TimerSystem();
		virtual ~TimerSystem();

		//Core::Priority	GetPriority() const override	{	return 0;	}

		virtual void	Process() override;
	};
}

#define SubSystemOfTimer	SubSystem::TimerSystem::GetSingletonPtr()
