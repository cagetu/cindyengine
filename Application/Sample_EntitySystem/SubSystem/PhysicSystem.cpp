#include "stdafx.h"
#include "PhysicSystem.h"
#include <Cagetu/Core/Component/ComponentManager.h>

namespace SubSystem
{
	IMPLEMENT_COMPONENT_SYSTEM( PhysicSystem );
	REGISTER_COMPONENT_SYSTEM( PhysicSystem );
	//------------------------------------------------------------------
	// Const/Dest
	PhysicSystem::PhysicSystem()
	{
	}
	PhysicSystem::~PhysicSystem()
	{
	}

	//------------------------------------------------------------------
	/** @brief	������Ʈ */
	//------------------------------------------------------------------
	void PhysicSystem::Process()
	{
		if (m_Components.empty())
			return;

		PhysXInstance->GetPhysicsResults();
		PhysXInstance->Simulate();

		ComponentList::iterator i, iend;
		iend = m_Components.end();
		for (i=m_Components.begin(); i!=iend; ++i)
		{
			(i->second)->Process();
		}
	}
}
