#pragma once

#include "../Components/PhysicComponent.h"
#include <Cagetu/Core/Component/SubSystem.h>
#include <MoCommon/MoSingleton.h>

namespace SubSystem
{
	// 입력 시스템
	class PhysicSystem : public Core::SubSystem<Component::IPhysic>,
						 public MoSingleton<PhysicSystem>
	{
		//DECLARE_CLASS(PhysicSystem);
		DECLARE_COMPONENT_SYSTEM;
	public:
		PhysicSystem();
		virtual ~PhysicSystem();

		//Core::Priority	GetPriority() const override	{	return 2;	}

		virtual void	Process() override;
	};
}

#define SubSystemOfPhysic	SubSystem::PhysicSystem::GetSingletonPtr()
