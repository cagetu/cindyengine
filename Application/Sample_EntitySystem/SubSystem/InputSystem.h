#pragma once

#include "../Components/InputComponent.h"
#include <Cagetu/Core/Component/SubSystem.h>
#include <MoCommon/MoSingleton.h>

namespace SubSystem
{
	// 입력 시스템
	class InputSystem : public Core::SubSystem<Component::IInput>,
						public MoSingleton<InputSystem>
	{
		//DECLARE_CLASS(InputSystem);
		DECLARE_COMPONENT_SYSTEM;
	public:
		InputSystem();
		virtual ~InputSystem();

		//Core::Priority	GetPriority() const override	{	return 1;	}

		virtual void	Process() override;
	};
}

#define SubSystemOfInput	SubSystem::InputSystem::GetSingletonPtr()
