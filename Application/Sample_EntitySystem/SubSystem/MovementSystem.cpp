#include "stdafx.h"
#include "MovementSystem.h"
#include <Cagetu/Core/Component/ComponentManager.h>

namespace SubSystem
{
	IMPLEMENT_COMPONENT_SYSTEM( MovementSystem );
	REGISTER_COMPONENT_SYSTEM( MovementSystem );
	//------------------------------------------------------------------
	// Const/Dest
	MovementSystem::MovementSystem()
	{
	}
	MovementSystem::~MovementSystem()
	{
	}

	//------------------------------------------------------------------
	/** @brief	������Ʈ */
	//------------------------------------------------------------------
	void MovementSystem::Process()
	{
		ComponentList::iterator i, iend;
		iend = m_Components.end();
		for (i=m_Components.begin(); i!=iend; ++i)
		{
			(i->second)->Update();
		}
	}

}