#pragma once

#include "../Components/AnimationComponent.h"
#include <Cagetu/Core/Component/SubSystem.h>
#include <MoCommon/MoSingleton.h>

namespace SubSystem
{
	//==================================================================
	/** Animation System
		@author		cagetu
		@remarks	애니메이션을 처리하기 위한 GameSystem
	*/
	//==================================================================
	class AnimationSystem : public Core::SubSystem<Component::IAnimation>,
							public MoSingleton<AnimationSystem>
	{
		//DECLARE_CLASS(AnimationSystem);
		DECLARE_COMPONENT_SYSTEM;
	public:
		AnimationSystem();
		virtual ~AnimationSystem();

		//Core::Priority	GetPriority() const override	{	return 2;	}

		void			Process() override;
	};
}

#define SubSystemOfAnimation	SubSystem::AnimationSystem::GetSingletonPtr()
