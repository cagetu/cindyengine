#include "stdafx.h"
#include "InputSystem.h"

namespace SubSystem
{
	//IMPLEMENT_CLASS( SubSystem, InputSystem, Core::SubSystem<Component::IInput>, 0 );
	IMPLEMENT_COMPONENT_SYSTEM( InputSystem );
	REGISTER_COMPONENT_SYSTEM( InputSystem );
	//------------------------------------------------------------------
	// Const/Dest
	InputSystem::InputSystem()
	{
	}
	InputSystem::~InputSystem()
	{
	}

	//------------------------------------------------------------------
	/** @brief	������Ʈ */
	//------------------------------------------------------------------
	void InputSystem::Process()
	{
		ComponentList::iterator i, iend;
		iend = m_Components.end();
		for (i=m_Components.begin(); i!=iend; ++i)
		{
			(i->second)->Process();
		}
	}
}
