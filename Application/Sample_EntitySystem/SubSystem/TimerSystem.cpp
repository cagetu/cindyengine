#include "stdafx.h"
#include "TimerSystem.h"
#include <Cagetu/Core/Component/ComponentManager.h>

namespace SubSystem
{
	IMPLEMENT_COMPONENT_SYSTEM( TimerSystem );
	REGISTER_COMPONENT_SYSTEM( TimerSystem );
	//------------------------------------------------------------------
	// Const/Dest
	TimerSystem::TimerSystem()
	{
	}
	TimerSystem::~TimerSystem()
	{
	}

	//------------------------------------------------------------------
	/** @brief	������Ʈ */
	//------------------------------------------------------------------
	void TimerSystem::Process()
	{
		ComponentList::iterator i, iend;
		iend = m_Components.end();
		for (i=m_Components.begin(); i!=iend; ++i)
		{
			(i->second)->Update();
		}
	}
}
