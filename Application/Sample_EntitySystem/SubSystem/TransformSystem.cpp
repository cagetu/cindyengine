#include "stdafx.h"
#include "TransformSystem.h"

namespace SubSystem
{
	//IMPLEMENT_CLASS( SubSystem, TransformSystem, Core::SubSystem<Component::ITransform>, 0 );
	IMPLEMENT_COMPONENT_SYSTEM(TransformSystem);
	REGISTER_COMPONENT_SYSTEM(TransformSystem);
	//------------------------------------------------------------------
	// Const/Dest
	TransformSystem::TransformSystem()
	{
	}
	TransformSystem::~TransformSystem()
	{
	}

	//------------------------------------------------------------------
	/** @brief	������Ʈ */
	//------------------------------------------------------------------
	void TransformSystem::Process()
	{
		ComponentList::iterator i, iend;
		iend = m_Components.end();
		for (i=m_Components.begin(); i!=iend; ++i)
		{
			(i->second)->Update();
		}
	}
}
