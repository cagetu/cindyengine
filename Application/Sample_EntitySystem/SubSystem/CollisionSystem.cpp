#include "stdafx.h"
#include "CollisionSystem.h"
#include <Cagetu/Core/Component/ComponentManager.h>

namespace SubSystem
{
	IMPLEMENT_COMPONENT_SYSTEM( CollisionSystem );
	REGISTER_COMPONENT_SYSTEM( CollisionSystem );
	//------------------------------------------------------------------
	// Const/Dest
	CollisionSystem::CollisionSystem()
	{
	}
	CollisionSystem::~CollisionSystem()
	{
	}

	//------------------------------------------------------------------
	/** @brief	������Ʈ */
	//------------------------------------------------------------------
	void CollisionSystem::Process()
	{
		if (m_Components.empty())
			return;

		ComponentList::iterator i, iend;
		iend = m_Components.end();
		for (i=m_Components.begin(); i!=iend; ++i)
		{
			(i->second)->Process();
		}
	}
}
