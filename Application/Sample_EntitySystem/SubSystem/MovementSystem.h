#pragma once

#include "../Components/MovementComponent.h"
#include <Cagetu/Core/Component/SubSystem.h>
#include <MoCommon/MoSingleton.h>

namespace SubSystem
{
	//==================================================================
	/** Movement System
		@author		cagetu
		@remarks	애니메이션을 처리하기 위한 GameSystem
	*/
	//==================================================================
	class MovementSystem : public Core::SubSystem<Component::IMovement>,
							public MoSingleton<MovementSystem>
	{
		DECLARE_COMPONENT_SYSTEM;
	public:
		MovementSystem();
		virtual ~MovementSystem();

		//Core::Priority	GetPriority() const override	{	return 2;	}

		void			Process() override;
	};
}

#define SubSystemOfMovement	SubSystem::MovementSystem::GetSingletonPtr()
