#pragma once

#include <Cindy/CnFramework.h>
#include <Cindy/Geometry/CnMeshDataManager.h>
#include <Cindy/Material/CnMaterialDataManager.h>
#include <Cindy/Animation/CnSkeletonDataManager.h>
#include <Cindy/Scene/CnCameraImpl.h>
#include <Cindy/System/CnMouse.h>
#include <Cindy/Foundation/CnMemObject.h>

#include <Cagetu/Core/Message/Server.h>
#include <Cagetu/Core/Message/Dispatcher.h>
#include <Cagetu/Core/Entity/EntityManager.h>
#include "PhysX/Physic.h"

class CApplication : public CnFramework
{
public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*	AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
								  int nWidth, int nHeight, ushort usColorDepth,
								  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
								  bool bSetCurrentTarget = false ) override;

	bool			Initialize() override;
	void			DeInitialize() override;

	void			Run() override;

	// MsgProc
	virtual bool	MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) override;

private:
	MdvCamPtr			m_spCamera;

	CnMouse				m_Mouse;
	short				m_nRotX;
	short				m_nRotY;

	PhysX				m_PhysX;

	CnRenderWindow*			m_pRenderWindow;

	Message::Server			m_MsgServer;
	Message::Dispatcher		m_Dispatcher;
	Core::EntityManager		m_EntityManager;

	CnMeshDataManager*		m_pMeshDataGroup;
	CnMaterialDataManager*	m_pMaterialDataGroup;
	CnSkeletonDataManager*	m_pSkeletonDataGroup;

	//-----------------------------------------------------------------------------
	//	Methods
	//-----------------------------------------------------------------------------
	void		SetScene( const CnString& strName ) override;

	CnViewport* SetViewport();
	void		SetData();

	void		InitEntities();
	void		SetCamera();
	void		SetLight();
	void		SetCreature();

	void		TestProperties();
};
