#include "stdafx.h"
#include "MoveMessage.h"

namespace Msg
{
	//==================================================================
	IMPLEMENT_MSGID(MoveToForward);
	IMPLEMENT_MSGID(MoveToBack);
	IMPLEMENT_MSGID(MoveToRight);
	IMPLEMENT_MSGID(MoveToLeft);
}
