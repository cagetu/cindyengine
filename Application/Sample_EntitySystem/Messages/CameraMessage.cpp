#include "stdafx.h"
#include "CameraMessage.h"

namespace Msg
{
	//==================================================================
	IMPLEMENT_MSGID(CameraToViewport);
	//------------------------------------------------------------------
	CameraToViewport::CameraToViewport( Cindy::CnViewport* pViewport )
		: viewport( pViewport )
	{
	}
}
