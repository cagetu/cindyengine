#pragma once

#include <Cagetu/Core/Message/Msg.h>

namespace Msg
{
	class MoveToForward : public Message::IMsg
	{
		DECLARE_MSGID;
	};
	
	class MoveToBack : public Message::IMsg
	{
		DECLARE_MSGID;
	};
	
	class MoveToRight : public Message::IMsg
	{
		DECLARE_MSGID;
	};
	
	class MoveToLeft : public Message::IMsg
	{
		DECLARE_MSGID;
	};
}
