#pragma once

#include <Cindy/Scene/CnCameraImpl.h>
#include <Cagetu/Core/Message/Msg.h>

namespace Msg
{
	class CameraToViewport : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		CameraToViewport() {}
		CameraToViewport( Cindy::CnViewport* pViewport );

		Cindy::CnViewport*		viewport;
	};
}
