#include "stdafx.h"
#include "SceneMessage.h"

#include <Cindy/Scene/CnSceneEntity.h>

namespace Msg
{
	//==================================================================
	IMPLEMENT_MSGID(AttachNode);
	//------------------------------------------------------------------
	AttachNode::AttachNode( CnSceneNode* pNode )
		: m_pNode( pNode )
	{
	}

	//==================================================================
	IMPLEMENT_MSGID(AttachEntity)
	//------------------------------------------------------------------
	AttachEntity::AttachEntity( CnSceneEntity* pEntity )
		: m_pEntity( pEntity )
	{
	}

	//==================================================================
	IMPLEMENT_MSGID(AttachMesh);
	IMPLEMENT_MSGID(DetachMesh);

	//==================================================================
	IMPLEMENT_MSGID(SetTransform);
	IMPLEMENT_MSGID(SetPosition);
	IMPLEMENT_MSGID(SetRotation);
	IMPLEMENT_MSGID(SetScale);
}

