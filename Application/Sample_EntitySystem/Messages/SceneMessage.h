#pragma once

#include <Cindy/Scene/CnSceneNode.h>
#include <Cagetu/Core/Message/Msg.h>

namespace Msg
{
	//-----------------------------------------------------------------------------
	/**	@class		Message AttachNode
		@author		cagetu

		@brief		노드를 추가한다.
	*/
	//-----------------------------------------------------------------------------
	class AttachNode : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		//------------------------------------------------------------------
		// Methods
		//------------------------------------------------------------------
		AttachNode() {}
		AttachNode( CnSceneNode* Node );

		CnSceneNode* GetSceneNode() const	{	return m_pNode;		}
	private:
		//------------------------------------------------------------------
		// Variables
		//------------------------------------------------------------------
		CnSceneNode*	m_pNode;
	};

	//-----------------------------------------------------------------------------
	/**	@class		Message AttachEntity
		@author		cagetu

		@brief		Entity를 추가한다.
	*/
	//-----------------------------------------------------------------------------
	class AttachEntity : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		AttachEntity() {}
		AttachEntity( CnSceneEntity* pEntity );

		CnSceneEntity*	GetEntity() const	{	return m_pEntity;	}
	private:
		CnSceneEntity*	m_pEntity;
	};

	class AttachMesh : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		CnString	MeshName;
		CnString	MtlName;
	};

	class DetachMesh : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		CnString	MeshName;
	};

	//-----------------------------------------------------------------------------
	/**	@class		Message SetTransform
		@author		cagetu

		@brief		변환값을 설정한다.
	*/
	//-----------------------------------------------------------------------------
	class SetTransform : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		CnVector3		m_Position;
		CnQuaternion	m_Rotation;
		CnVector3		m_Scale;	
	};

	class SetPosition : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		CnVector3		m_Position;
	};

	class SetRotation : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		CnQuaternion	m_Rotation;
	};

	class SetScale : public Message::IMsg
	{
		DECLARE_MSGID;
	public:
		CnVector3		m_Scale;
	};


	//-----------------------------------------------------------------------------
	/**	@class		Message UpdateTransform
		@author		cagetu

		@brief		장면 그래프를 업데이트 한다.
	*/
	//-----------------------------------------------------------------------------
	//class UpdateTransform : public Messgae::IMsg
	//{
	//	DECLARE_MSGID;
	//}
}