#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnSceneComponentImpl.h>

// Entity-Component
#include <Cagetu/Core/Entity/Entity.h>
#include <Cagetu/Core/Message/Server.h>
#include <Cagetu/Core/Template/EntityFactory.h>
#include <Cagetu/Core/Component/ComponentManager.h>
//#include <Cagetu/Core/Attribute/AttributeContainer.h>

#include "Components/SceneEntityComponent.h"
#include "Components/SceneComponent.h"

#include "SubSystem/InputSystem.h"
#include "SubSystem/AnimationSystem.h"
#include "SubSystem/MovementSystem.h"
#include "SubSystem/TransformSystem.h"
#include "SubSystem/PhysicSystem.h"

#include <Cindy/Foundation/CnProperty.h>

// Const/Dest
CApplication::CApplication()
{
	m_pMeshDataGroup = CnMeshDataManager::CreateClass();
	m_pMaterialDataGroup = CnMaterialDataManager::CreateClass();
	m_pSkeletonDataGroup = CnSkeletonDataManager::CreateClass();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
	SAFEDEL( m_pSkeletonDataGroup );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
											   nWidth, nHeight, usColorDepth,
											   usRefreshRate, bFullScreen,
											   bThreadSafe, bSetCurrentTarget );
	return m_pRenderWindow;
}

//--------------------------------------------------------------
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->CreateScene( L"Scene" );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::CreateClass();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	SceneComponent::UpdateScene* sceneUpdator = SceneComponent::UpdateScene::CreateClass();
	sceneUpdator->Register( m_pSceneGraph );

	SceneComponent::RenderModel* modelRenderer = SceneComponent::RenderModel::CreateClass();
	modelRenderer->Link( sceneUpdator );

	SceneComponent::RenderAABB* aabbRenderer = SceneComponent::RenderAABB::CreateClass();
	aabbRenderer->Link( sceneUpdator );

	m_pWorld->Register( sceneUpdator );
	m_pWorld->Register( SceneComponent::BeginScene::CreateClass() );
	m_pWorld->Register( modelRenderer );
	m_pWorld->Register( aabbRenderer );
	m_pWorld->Register( SceneComponent::EndScene::CreateClass() );
}

//--------------------------------------------------------------
/** @brief	초기화 */
//--------------------------------------------------------------
bool CApplication::Initialize()
{
	// Setup Scene..
	SetScene( _T("Scene") );

	// viewport 1
	CnViewport* viewport = SetViewport();

	m_PhysX.Initialize();

	// 데이터 설정
	SetData();

	// Entity 설정
	InitEntities();

	//Msg::SetupCamera msg( pViewport );
	//msg.SendSync( &m_Dispatcher );

	m_pSceneGraph->UpdateTransform();

	EntityDatabase->Start();

	// Event처리해야 하는데...

	Msg::CameraToViewport msg( viewport );
	ComponentDataBase->SendSyncMessage( L"Camera", &msg );

	//Core::Entity* camEntity = m_EntityManager.GetEntity( L"Camera" );
	//if (camEntity)
	//{
	//	Msg::CameraToViewport msg( viewport );

	//	camEntity->HandleMessage( L"Camera", &msg );
	//}

	//CnNode* newNode = m_pSceneGraph->GetRoot()->CreateChild( L"Test" );

	//CnProperty<CnNode, const CnVector3&> prop( "Position", &CnNode::GetWorldPosition, &CnNode::SetPosition );
	//prop.SetValue( newNode, CnVector3(1.0f, 2.0f, 0.5f) );

	//PropertyTypeID::Enum type = prop.GetType();
	//CnVector3 position = prop.GetValue( newNode );
	return true;
}

void CApplication::DeInitialize()
{
	m_PhysX.Release();
}

//--------------------------------------------------------------
/** @brief	Viewport 설정 */
//--------------------------------------------------------------
CnViewport* CApplication::SetViewport()
{
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	return pViewport;
}

//--------------------------------------------------------------
/** @brief	Entity */
//--------------------------------------------------------------
void CApplication::InitEntities()
{
	bool result = EntityFactoryInst->Load( L"Components.xml" );
	if (!result)
		return;

	SetCamera();
	SetLight();
	SetCreature();
}

void CApplication::SetCamera()
{
	Core::Entity* cameraEntity = EntityFactoryInst->Create( L"ModelViewCamera", L"Camera" );
	if (cameraEntity)
	{
		m_EntityManager.AddEntity( cameraEntity );
	}

	/*
		Core::Entity* cameraEntity = new Core::Entity( L"ModelViewCamera" );
		{
			Component::ModelViewCamera* comCamera = Foundation::CreateObject<Component::ModelViewCamera>();
			Component::CameraInput* comCamInput = Foundation::CreateObject<Component::CameraInput>();

			cameraEntity->AddComponent( comCamera );
			cameraEntity->AddComponent( comCamInput );

			// Messgae...
			m_Dispatcher.AddPort( comCamera );

			cameraEntity->Initialize();
		}
		m_EntityManager.AddEntity( cameraEntity );
	*/
}

void CApplication::SetLight()
{
	// Light 추가
	Core::Entity* lightEntity = new Core::Entity( L"GlobalLight" );
	{
		// light
		Component::Light* lightComponent = Foundation::CreateObject<Component::Light>();
		lightComponent->SetType( CnLight::Directional );
		lightComponent->SetDiffuse( CnColor( 0.8f, 0.3f, 0.3f, 1.0f ) );
		lightComponent->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
		lightComponent->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

		// Scene
		Component::SceneNode* sceneNodeComponent = Foundation::CreateObject<Component::SceneNode>();
		sceneNodeComponent->SetName( L"GlobalLight" );
		sceneNodeComponent->SetRootNode( m_pSceneGraph->GetRoot() );
		//sceneNodeComponent->SetPosition( CnVector3(10.0f, 10.0f, -10.0f) );

		// Animation
		//Component::CustumAnimation* animComponent = Foundation::CreateObject<Component::CustumAnimation>();
		Component::AutoMovement* moveComponent = Foundation::CreateObject<Component::AutoMovement>();

		lightEntity->AddComponent( lightComponent );
		lightEntity->AddComponent( moveComponent );
		lightEntity->AddComponent( sceneNodeComponent );
	}
	m_EntityManager.AddEntity( lightEntity );

}

void CApplication::SetCreature()
{
	Core::Entity* actorEntity = NULL;

	// Ground
	actorEntity = EntityFactoryInst->Create( L"Static", L"Grid" );
	if (actorEntity)
	{
		Component::Mesh* meshComponent = DynamicCast<Component::Mesh>( actorEntity->GetComponent(L"SceneEntity") );
		if (meshComponent)
		{
			meshComponent->SetData( L"base_grid.mmf", L"base_grid.xml" );
		}

		Component::ModelNode* nodeComponent = DynamicCast<Component::ModelNode>( actorEntity->GetComponent( L"Scene" ) );		
		nodeComponent->SetName( L"Grid" );
		nodeComponent->SetRootNode( m_pSceneGraph->GetRoot() );
		
		Component::CollisionPlane* collisionComponent = Foundation::CreateObject<Component::CollisionPlane>();
		actorEntity->AddComponent( collisionComponent );

		m_EntityManager.AddEntity( actorEntity );
	}

	// Character
	actorEntity = new Core::Entity( L"Atoom" );
	{
		// Mesh
		Component::Mesh* meshComponent = Foundation::CreateObject<Component::Mesh>();
		meshComponent->SetData( L"atoom.mmf", L"atoom_mtl.xml" );

		// Scene
		Component::ModelNode* nodeComponent = Foundation::CreateObject<Component::ModelNode>();
		nodeComponent->SetName( L"Atoom" );
		nodeComponent->SetRootNode( m_pSceneGraph->GetRoot() );
		nodeComponent->SetPosition( CnVector3(0.0f, 30.0f, 0.0f) );

		// Collision
		Component::CollisionBox* collisionComponent = Foundation::CreateObject<Component::CollisionBox>();
		
		actorEntity->AddComponent( meshComponent );
		actorEntity->AddComponent( nodeComponent );
		actorEntity->AddComponent( collisionComponent );

		m_EntityManager.AddEntity( actorEntity );
	}
	actorEntity = EntityFactoryInst->Create( L"Actor", L"Atoom2" );
	if (actorEntity)
	{
		Component::ModelNode* nodeComponent = DynamicCast<Component::ModelNode>( actorEntity->GetComponent( L"Scene" ) );		
		nodeComponent->SetName( L"Atoom2" );
		nodeComponent->SetRootNode( m_pSceneGraph->GetRoot() );
		nodeComponent->SetPosition( CnVector3(10.0f, 0.0f, -10.0f) );

		//nodeComponent->AddMesh( L"atoom.mmf", L"atoom_mtl.xml" );

		// Mesh
		Component::Mesh* meshComponent = DynamicCast<Component::Mesh>( actorEntity->GetComponent(L"SceneEntity") );
		if (meshComponent)
		{
			meshComponent->SetData( L"atoom.mmf", L"atoom_mtl.xml" );
		}
		
		m_EntityManager.AddEntity( actorEntity );
	}
}

//--------------------------------------------------------------
/** @brief	Property 테스트 */
//--------------------------------------------------------------
void CApplication::TestProperties()
{
	//Component::ModelNode* testNodeComponent = Foundation::CreateObject<Component::ModelNode>();
	//Foundation::AbstractProperty* prop = testNodeComponent->GetProperty( "SceneNode" );
	//Foundation::PropertyTypeID::Enum id = prop->GetType();

	//const Foundation::PropertyTable* propTable = testNodeComponent->GetPropertyList();
	//Foundation::AbstractProperty* prop = propTable->Lookup( "SceneNode" );
	//Foundation::PropertyTypeID::Enum id = prop->GetType();

	//Foundation::PropertyList* properties = testNodeComponent->GetProperties();
	//Foundation::PropertyIter ibegin, iend;
	//iend = properties->end();
	//for (ibegin = properties->begin(); ibegin!=iend; ++ibegin)
	//{
	//	Foundation::AbstractProperty* prop = ibegin->second;
	//	Foundation::PropertyTypeID::Enum id = prop->GetType();
	//	switch (id)
	//	{
	//	case Foundation::PropertyTypeID::Bool:
	//		break;
	//	case Foundation::PropertyTypeID::Dword:
	//		break;
	//	case Foundation::PropertyTypeID::Int:
	//		break;
	//	case Foundation::PropertyTypeID::Float:
	//		break;
	//	case Foundation::PropertyTypeID::String:
	//		break;
	//	case Foundation::PropertyTypeID::WString:
	//		break;
	//	case Foundation::PropertyTypeID::Pointer:
	//		{
	//		}
	//		break;
	//	}
	//}

	// Variables
	//Attr::Container container;
	//container.SetBool( L"Test", true );
	//container.SetFloat( L"Test1", 100.0f );
	//container.SetVector4( L"Diffuse", CnVector4(0.5f, 0.5f, 0.2f, 1.0f) );

	//float test1 = container.GetFloat( L"Test1" );
	//bool test = container.GetBool( L"Test" );
	//CnVector4 color = container.GetVector4( L"Diffuse" );
}

// 데이터 로딩
void CApplication::SetData()
{
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	m_pMeshDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pSkeletonDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	m_pMeshDataGroup->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pSkeletonDataGroup->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	// Ground
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\" );
}

void CApplication::Run()
{
	SubSystemOfInput->Process();
	SubSystemOfAnimation->Process();
	SubSystemOfMovement->Process();
	SubSystemOfPhysic->Process();

	//if (::GetAsyncKeyState('W'))
	//{
	//	CnVector3 dir;
	//	m_spCamera->GetDirection( dir );
	//	
	//	CnVector3 lookAt = m_spCamera->GetLookAt();
	//	m_spCamera->SetLookAt( lookAt + dir );
	//}
	//if (::GetAsyncKeyState('S'))
	//{
	//	CnVector3 dir;
	//	m_spCamera->GetDirection( dir );

	//	CnVector3 lookAt = m_spCamera->GetLookAt();
	//	m_spCamera->SetLookAt( lookAt - dir );
	//}
	//if (::GetAsyncKeyState('A'))
	//{
	//	CnVector3 right;
	//	m_spCamera->GetRight( right );

	//	CnVector3 lookAt = m_spCamera->GetLookAt();
	//	m_spCamera->SetLookAt( lookAt + right );
	//}
	//if (::GetAsyncKeyState('D'))
	//{
	//	CnVector3 right;
	//	m_spCamera->GetRight( right );

	//	CnVector3 lookAt = m_spCamera->GetLookAt();
	//	m_spCamera->SetLookAt( lookAt - right );
	//}

	CnFramework::Run();
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}