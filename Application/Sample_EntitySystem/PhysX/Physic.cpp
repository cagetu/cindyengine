#include "stdafx.h"
#include "Physic.h"

PhysX::PhysX()
: m_DefaultGravity( 0.0f, -9.8f, 0.0f )
{
}
PhysX::~PhysX()
{
}

//--------------------------------------------------------------
void PhysX::Initialize()
{
	// 초기화
	Init();

    // Create the scene
    m_pScene = CreateScene();

	// 기본 매터리얼 생성
	SetDefaultMaterial();

	// 현재 시간 구하기
	UpdateTime();

	if (m_pScene)
	{
		Simulate();
	}
}

//--------------------------------------------------------------
void PhysX::Init()
{
	/// 재정의된 메모리 할당자
	class NxAllocator : public NxUserAllocator
	{
	public:
		virtual void* mallocDEBUG( size_t Size, const char* FileName, int Line )	{
			return ::malloc(Size);
		}

		virtual void* malloc( size_t Size ) {
			return ::malloc(Size);
		}

		virtual void* realloc( void* Memory, size_t Size ) {
			return ::realloc(Memory, Size);
		}

		virtual void free( void* Memory ) {
			::free(Memory);
		}
	};

	/// Error 보고
	class NxErrorReport : public NxUserOutputStream
	{
		virtual void reportError(NxErrorCode code, const char * message, const char *file, int line)
		{
		}
		virtual NxAssertResponse reportAssertViolation(const char * message, const char *file, int line)
		{
			return NX_AR_CONTINUE;
		}

		virtual void print(const char * message)
		{
		}
	};

	static NxErrorReport	s_ErrorReport;

	// Create the physics SDK
	m_pPhysicsSDK = NxCreatePhysicsSDK( NX_PHYSICS_SDK_VERSION, 0, &s_ErrorReport );
	if (!m_pPhysicsSDK)
		return;

	// Set the physics parameters
	m_pPhysicsSDK->setParameter( NX_SKIN_WIDTH, 0.01f );				// 접촉 표면 길이 설정

	// Set the debug visualization parameters
	m_pPhysicsSDK->setParameter( NX_VISUALIZATION_SCALE, 1.0f );		// 디버거 와이어 정보 1.0m 단위로 설정
	m_pPhysicsSDK->setParameter( NX_VISUALIZE_COLLISION_SHAPES, 1.0f );	// 충돌 라인 길이 비율 설정
	m_pPhysicsSDK->setParameter( NX_VISUALIZE_ACTOR_AXES, 1.0f );		// 축 길이 비율 설정

	// 모든 재질에 마찰력 스케일링을 적용할 수 있다.
	// 기본 1 상태에서 0.5 스케일링을 하여 더 잘 미끄러지게 할수 있다.
	// 점차적으로 다시 원상태로 돌아온다 ? 일회성?
	// 게임에서 얼음판상태가 되었을때 사용할 수 있다.
	//m_pPhysicsSDK->setParameter(NX_DYN_FRICT_SCALING, 0.5);
	//m_pPhysicsSDK->setParameter(NX_STA_FRICT_SCALING, 0.5);

	// relative velocity <상대속도> 
	// 어떤 물체에서 다른 물체를 본 상대적인 속도이다.
	// 관찰자가 등속운동중일 때는 자신이 정지해 있다고 느끼므로 어떤 물체가 관찰자가 되는가에
	// 따라 상대속도는 변한다. 상대성원리는 이것을 체계화한 것이다
	// 높은 탄성력을 가진 오브젝트에 쓸만한 기능임
	//m_pPhysicsSDK->setParameter(NX_BOUNCE_THRESHOLD, -100);

	m_pPhysicsSDK->getFoundationSDK().getRemoteDebugger()->connect ("localhost", 5425);
}

//--------------------------------------------------------------
NxScene* PhysX::CreateScene()
{
	NxScene* scene = NULL;

    NxSceneDesc sceneDesc;
 	sceneDesc.simType = NX_SIMULATION_HW;
    sceneDesc.gravity = m_DefaultGravity;
    scene = m_pPhysicsSDK->createScene( sceneDesc );	
	if(!scene)
	{ 
		sceneDesc.simType = NX_SIMULATION_SW; 
		scene = m_pPhysicsSDK->createScene( sceneDesc );  
		if(!scene)
			return NULL;
	}

	return scene;
}

//--------------------------------------------------------------
void PhysX::SetDefaultMaterial()
{
	// 기본 매터리얼 생성
	NxMaterial* defaultMaterial = m_pScene->getMaterialFromIndex(0);	// 0번 세팅된 재질을 가져 옴
	defaultMaterial->setRestitution( 0.5f );							// 탄성력(회복력) 사용
	defaultMaterial->setStaticFriction( 0.5f );							// 정지 마찰력
	defaultMaterial->setDynamicFriction( 0.5f );						// 운동 마찰력

	/*  NX_CM_AVERAGE - average the values (default)
		NX_CM_MIN - take the minimum of the values
		NX_CM_MULTIPLY - multiply the values
		NX_CM_MAX - take the maximum of the values
		NX_CM_TABLE - use the result of a table to combine the values    */
	//defaultMaterial->setFrictionCombineMode(NX_CM_MIN);
	//defaultMaterial->setRestitutionCombineMode(NX_CM_MIN);
}

//--------------------------------------------------------------
void PhysX::Release()
{
	if (m_pScene)
	{
		// Make sure to fetchResults() before shutting down
		GetPhysicsResults();
		m_pPhysicsSDK->releaseScene( *m_pScene );
	}

	if (m_pPhysicsSDK)
	{
		m_pPhysicsSDK->release();
	}
}

//--------------------------------------------------------------
void PhysX::Reset()
{
	Release();
	Initialize();
}

//--------------------------------------------------------------
void PhysX::Simulate()
{
	// time step를 업데이트
	m_fDeltaTime = UpdateTime();

	// 마지막 frame 이후로 delta time 에 대해 collision(충돌) 과 dynamics(운동) 시작
	m_pScene->simulate( m_fDeltaTime );
	m_pScene->flushStream();
}

void PhysX::GetPhysicsResults()
{
	// m_pScene->simulate(gDeltaTime)로 부터 결과를 얻어온다.
	while (!m_pScene->fetchResults( NX_RIGID_BODY_FINISHED, false ));
}

float PhysX::UpdateTime()
{
	static unsigned int previousTime = timeGetTime();
	unsigned int currentTime = timeGetTime();
	unsigned int elapsedTime = currentTime - previousTime;
	previousTime = currentTime;
	return (float)(elapsedTime)*0.001f;
}

//--------------------------------------------------------------
/** @brief	Kinematic과 Dynamic이 뭐지?? */
//--------------------------------------------------------------
void PhysX::KinematicToDynamic( NxActor* pActor, bool bKinematic )
{
	if (bKinematic)
	{
		if (pActor->readBodyFlag(NX_BF_KINEMATIC))
		{
			pActor->clearBodyFlag(NX_BF_KINEMATIC);
		}
		else
		{
			pActor->raiseBodyFlag(NX_BF_KINEMATIC);
		}
	}
}

//--------------------------------------------------------------
/** @brief	움직임 제한 
			NX_BF_FROZEN_POS: 이동 불가
				NX_BF_FROZEN_POS_X: X축 이동 불가
				NX_BF_FROZEN_POS_Y: Y축 이동 불가
				NX_BF_FROZEN_POS_Z: Z축 이동 불가
			NX_BF_FROZEN_ROT: 회전 불가
				NX_BF_FROZEN_ROT_X: X축 회전 불가
				NX_BF_FROZEN_ROT_Y: Y축 회전 불가
				NX_BF_FROZEN_ROT_Z: Z축 회전 불가
			NX_BF_FROZEN: 모두 불가
*/
//--------------------------------------------------------------
void PhysX::Frozen( NxActor* pActor, NxBodyFlag eFlag )
{
	pActor->raiseBodyFlag( eFlag );
}

//--------------------------------------------------------------
/** @brief	중력 상태 설정
			NX_BF_DISABLE_GRAVITY: 중력 무시
*/
//--------------------------------------------------------------
void PhysX::Gravity( NxActor* pActor, bool bEnable )
{
	if (bEnable)
	{
		pActor->clearBodyFlag( NX_BF_DISABLE_GRAVITY );
	}
	else
	{
		pActor->raiseBodyFlag( NX_BF_DISABLE_GRAVITY );
	}
}

//--------------------------------------------------------------
NxActor* PhysX::CreateBox( const char* Name, const NxVec3& Pos, const NxVec3& LocalPos, const NxVec3& Dimension, float Density )
{
	// 장면에 single-shape actor를 추가
	NxActorDesc actorDesc;
	NxBodyDesc bodyDesc;

	// actor는 하나의 shape를 가진다, 박스, side 마다 1m
	NxBoxShapeDesc boxDesc;
	boxDesc.name = Name;
	boxDesc.dimensions = Dimension;
	boxDesc.localPose.t = LocalPos;
	actorDesc.shapes.pushBack( &boxDesc );

	if (0.0f == Density)
	{
		actorDesc.body = NULL;
	}
	else
	{
		actorDesc.body = &bodyDesc;
		actorDesc.density = Density;
	}

	actorDesc.globalPose.t = Pos;
	
	return m_pScene->createActor( actorDesc );
}

NxActor* PhysX::CreateSphere( const char* Name, const NxVec3& Pos, const NxVec3& LocalPos, float Radius, float Density )
{
	NxActorDesc actorDesc;
	NxBodyDesc bodyDesc;

	// actor는 하나의 shape를 가진다. sphere로, 반지름 1m
	NxSphereShapeDesc sphereDesc;
	sphereDesc.radius = Radius;
	sphereDesc.localPose.t = LocalPos;
	sphereDesc.name = Name;
	actorDesc.shapes.pushBack( &sphereDesc );

	if (0.0f == Density)
	{
		actorDesc.body = NULL;
	}
	else
	{
		actorDesc.body = &bodyDesc;
		actorDesc.density = Density;
	}

	actorDesc.globalPose.t = Pos;

	return m_pScene->createActor( actorDesc );
}

//--------------------------------------------------------------
/** @brief	Actor에 힘을 적용한다 */
//--------------------------------------------------------------
void PhysX::ApplyForceToActor( NxActor* pActor, const NxVec3& ForceDir, const float ForceStrength, bool bForceMode )
{
	NxVec3 forceVec = ForceStrength * ForceDir * m_fDeltaTime;

	if (bForceMode)
	{
		pActor->addForce( forceVec );
	}
	else
	{
		pActor->addTorque( forceVec );
	}
}

//--------------------------------------------------------------
/** @brief	Actor의 Shape에 그룹 ID 부여 */
//--------------------------------------------------------------
void PhysX::SetActorCollisionGroup( NxActor* pActor, NxCollisionGroup GroupID )
{
	NxU32 numShapes = pActor->getNbShapes();
	NxShape*const* shapes = pActor->getShapes();

	while (numShapes--)
	{
		shapes[numShapes]->setGroup( GroupID );
	}
}

//--------------------------------------------------------------
void SetGlobalTM( NxActor* pActor, const CnMatrix44& Mat )
{
	NxMat33 tm;
	tm.setColumnMajorStride4( &Mat._11 );

	pActor->setGlobalOrientation( tm );
	pActor->setGlobalPosition( NxVec3( Mat._41, Mat._42, Mat._43 ) );
}

void GetGlobalTM( NxShape* pShape, CnMatrix44& Output )
{
	NxMat34 pose = pShape->getGlobalPose();

	pose.M.getColumnMajorStride4( &Output._11 );
	Output._14 = Output._24 = Output._34 = 0.0f;

	Output._41 = pose.t.x;
	Output._42 = pose.t.y;
	Output._43 = pose.t.z;
	Output._44 = 1.0f;
}