#ifndef __PHYSIC_H__
#define __PHYSIC_H__

// Singleton
#include <MoCommon/MoSingleton.h>

// PhysX
#pragma comment ( lib, "NxCharacter.lib" )
#pragma comment ( lib, "NxCooking.lib" )
#pragma comment ( lib, "NxExtensions.lib" )

#include "NxPhysics.h"
#include "NxCooking.h"

class PhysX : public MoSingleton<PhysX>
{
public:
	NxScene*		m_pScene;
	NxReal			m_fDeltaTime;

	//--------------------------------------------------------------
	//	Methods
	//--------------------------------------------------------------
	PhysX();
	~PhysX();

	NxScene*	Scene() const	{	return m_pScene;	}

	void		Initialize();
	void		Release();
	void		Reset();

	// Update
	void		Simulate();
	void		GetPhysicsResults();

	NxActor*	CreateBox( const char* Name,
						   const NxVec3& Pos, 
						   const NxVec3& LocalPos = NxVec3(0,0,0),
						   const NxVec3& Dimension = NxVec3(0.5f, 0.5f, 0.5f),
						   float Density = 10.0f );

	NxActor*	CreateSphere( const char* Name,
							  const NxVec3& Pos,
							  const NxVec3& LocaPos = NxVec3(0,0,0),
							  float Radius = 1.0f,
							  float Density = 10.0f );

	void		ApplyForceToActor( NxActor* pActor, const NxVec3& ForceDir, const float ForceStrength, bool bForceMode );

	void		SetActorCollisionGroup( NxActor* pActor, NxCollisionGroup GroupID );

	void		KinematicToDynamic( NxActor* pActor, bool bKinematic );
	void		Frozen( NxActor* pActor, NxBodyFlag eFlag );
	void		Gravity( NxActor* pActor, bool bEnable );

	static float	UpdateTime();

private:
	// Physics SDK globals
	NxPhysicsSDK*	m_pPhysicsSDK;
	NxVec3			m_DefaultGravity;

	//--------------------------------------------------------------
	//	Methods
	//--------------------------------------------------------------
	void		Init();
	NxScene*	CreateScene();
	void		SetDefaultMaterial();
};

#define PhysXInstance	PhysX::GetSingletonPtr()

void	SetGlobalTM( NxActor* pActor, const CnMatrix44& Mat );
void	GetGlobalTM( NxShape* pShape, CnMatrix44& Output );

#endif	// __PHYSIC_H__