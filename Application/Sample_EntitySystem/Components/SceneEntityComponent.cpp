#include "stdafx.h"
#include "SceneEntityComponent.h"
#include "SceneComponent.h"

#include <Cagetu/Core/Entity/Entity.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, ISceneEntity, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( ISceneEntity, L"SceneEntity" );
	//------------------------------------------------------------------
	ISceneEntity::ISceneEntity()
	{
	}
	ISceneEntity::~ISceneEntity()
	{
	}

	//------------------------------------------------------------------
	void ISceneEntity::SetName( const CnString& Name )
	{
		m_Name = Name;
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, Mesh, ISceneEntity, 0 );
	IMPLEMENT_COMPONENT_ID( Mesh );
	//------------------------------------------------------------------
	Mesh::Mesh()
		: m_pSceneComponent(0)
	{
	}
	Mesh::~Mesh()
	{
	}

	//------------------------------------------------------------------
	/** @brief	초기화 */
	//------------------------------------------------------------------
	void Mesh::Start()
	{
		Core::IComponent* component = this->GetOwner()->GetComponent( L"Scene" );
		if (component)
		{
			m_pSceneComponent = DynamicCast<Component::ModelNode>(component);

			if (!m_MeshName.empty() &&
				!m_MtlName.empty())
			{
				Msg::AttachMesh msg;
				msg.MeshName = m_MeshName;
				msg.MtlName = m_MtlName;
				m_pSceneComponent->HandleMessage( &msg );
			}
		}
	}
	void Mesh::End()
	{
		//if (m_pSceneComponent)
		//{
		//	Msg::DetachMesh msg;
		//	msg.MeshName = m_MeshName;
		//	m_pSceneComponent->HandleMessage( &msg );
		//}
	}

	//------------------------------------------------------------------
	void Mesh::SetData( const CnString& MeshName, const CnString& MtlName )
	{
		m_MeshName = MeshName;
		m_MtlName = MtlName;

		if (m_pSceneComponent)
		{
			Msg::AttachMesh msg;
			msg.MeshName = m_MeshName;
			msg.MtlName = m_MtlName;
			m_pSceneComponent->HandleMessage( &msg );
		}
	}

	//------------------------------------------------------------------
	/** @brief	*/
	//------------------------------------------------------------------
	void Mesh::SetName( const CnString& Name )
	{
		m_Name = Name;
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, Light, ISceneEntity, Light::RegisterProperties );
	IMPLEMENT_COMPONENT_ID( Light );
	//------------------------------------------------------------------
	Light::Light()
		: m_pLight(0)
	{
	}
	Light::~Light()
	{
	}

	//------------------------------------------------------------------
	/** @brief	초기화 */
	//------------------------------------------------------------------
	void Light::Start()
	{
		if (0 == m_pLight)
		{
			// Light
			m_pLight = CnNew CnLight( m_Name );
			m_pLight->SetType( m_LightType );
			m_pLight->SetAmbient( m_Ambient );
			m_pLight->SetDiffuse( m_Diffuse );
			m_pLight->SetSpecular( m_Specular );
		}

		Core::IComponent* component = this->GetOwner()->GetComponent( L"Scene" );
		if (component)
		{
			m_pSceneComponent = DynamicCast<Component::SceneNode>(component);
			if (m_pSceneComponent)
			{
				m_pSceneComponent->GetSceneNode()->AttachEntity( m_pLight );
			}
		}
	}
	void Light::End()
	{
		//m_pSceneComponent->GetSceneNode()->DetachEntity( m_Name );
		//SAFEDEL( m_pLight );
	}

	//------------------------------------------------------------------
	/** @brief	*/
	//------------------------------------------------------------------
	void Light::SetName( const CnString& Name )
	{
		m_Name = Name;
		if (m_pLight)
		{
			m_pLight->SetName( Name );
		}
	}
	void Light::SetType( const CnLight::Type Type )
	{
		m_LightType = Type;
	}

	//------------------------------------------------------------------
	/** @brief	Ambient 색상 설정 */
	//------------------------------------------------------------------
	void Light::SetAmbient( const Cindy::CnColor& Color )
	{
		m_Ambient = Color;
		if (m_pLight)
		{
			m_pLight->SetAmbient( Color );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Diffuse 색상 설정 */
	//------------------------------------------------------------------
	void Light::SetDiffuse( const Cindy::CnColor& Color )
	{
		m_Diffuse = Color;
		if (m_pLight)
		{
			m_pLight->SetDiffuse( Color );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Specular 색상 설정 */
	//------------------------------------------------------------------
	void Light::SetSpecular( const Cindy::CnColor& Color )
	{
		m_Specular = Color;
		if (m_pLight)
		{
			m_pLight->SetSpecular( Color );
		}
	}


	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void Light::RegisterProperties()
	{
		RegisterProperty<Light, const CnColor&>( "Ambient", &Light::GetAmibient, &Light::SetAmbient );
		RegisterProperty<Light, const CnColor&>( "Diffuse", &Light::GetDiffuse, &Light::SetDiffuse );
		RegisterProperty<Light, const CnColor&>( "Specular", &Light::GetSpecular, &Light::SetSpecular );
	}
}
