#pragma once

#include <Cagetu/Core/Component/ComponentManager.h>
#include <Cindy/Scene/CnLight.h>

namespace Component
{
	class SceneNode;

	class ILight : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		ILight();
		virtual ~ILight();
	};

	// Global Light Component
	class Light : public ILight
	{
		DECLARE_CLASS( Light );
		DECLARE_COMPONENT_ID;
	public:
		Light();
		virtual ~Light();

		// Initialize
		void	Start() override;
		void	End() override;

		// Set/Get
		void	SetName( const CnString& Name );
		void	SetType( const CnLight::Type LightType );

		void	SetAmbient( const CnColor& Color );
		void	SetDiffuse( const CnColor& Color );
		void	SetSpecular( const CnColor& Color );

		const CnColor&	GetAmibient()	{	return m_Ambient;	}
		const CnColor&	GetDiffuse()	{	return m_Diffuse;	}
		const CnColor&	GetSpecular()	{	return m_Specular;	}

		static void		RegisterProperties();
	private:
		CnLight*		m_pLight;

		CnString		m_LightName;
		CnLight::Type	m_LightType;

		CnColor			m_Ambient;
		CnColor			m_Diffuse;
		CnColor			m_Specular;

		Component::SceneNode*	m_pSceneComponent;
	};

	//REGISTER_FACTORY( Light );
}