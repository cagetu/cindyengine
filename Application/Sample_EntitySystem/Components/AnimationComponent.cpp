#include "stdafx.h"
#include "AnimationComponent.h"
#include "../SubSystem/AnimationSystem.h"
#include <Cagetu/Core/Entity/Entity.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, IAnimation, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( IAnimation, L"Animation" );
	//------------------------------------------------------------------
	IAnimation::IAnimation()
	{
	}
	IAnimation::~IAnimation()
	{
	}

}
