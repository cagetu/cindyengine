#include "stdafx.h"
#include "LightComponent.h"
#include "SceneComponent.h"

#include <Cagetu/Core/Entity/Entity.h>
#include <Cindy/Scene/CnLight.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, ILight, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( ILight, L"Light" );
	//------------------------------------------------------------------
	ILight::ILight()
	{
	}
	ILight::~ILight()
	{
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, Light, ILight, Light::RegisterProperties );
	IMPLEMENT_COMPONENT_ID( Light );
	//------------------------------------------------------------------
	Light::Light()
		: m_pLight(0)
	{
	}
	Light::~Light()
	{
	}

	//------------------------------------------------------------------
	/** @brief	초기화 */
	//------------------------------------------------------------------
	void Light::Start()
	{
		if (0 == m_pLight)
		{
			// Light
			m_pLight = CnNew CnLight( m_LightName );
			m_pLight->SetType( m_LightType );
			m_pLight->SetAmbient( m_Ambient );
			m_pLight->SetDiffuse( m_Diffuse );
			m_pLight->SetSpecular( m_Specular );
		}

		Core::IComponent* component = this->GetOwner()->GetComponent( L"Scene" );
		if (component)
		{
			m_pSceneComponent = DynamicCast<Component::SceneNode>(component);
			if (m_pSceneComponent)
			{
				m_pSceneComponent->GetSceneNode()->AttachEntity( m_pLight );
			}
		}
	}
	void Light::End()
	{
		m_pSceneComponent->GetSceneNode()->DetachEntity( m_pLight->GetName() );
		SAFEDEL( m_pLight );
	}

	//------------------------------------------------------------------
	/** @brief	*/
	//------------------------------------------------------------------
	void Light::SetName( const CnString& Name )
	{
		m_LightName = Name;
	}
	void Light::SetType( const CnLight::Type Type )
	{
		m_LightType = Type;
	}

	//------------------------------------------------------------------
	/** @brief	Ambient 색상 설정 */
	//------------------------------------------------------------------
	void Light::SetAmbient( const Cindy::CnColor& Color )
	{
		m_Ambient = Color;
	}

	//------------------------------------------------------------------
	/** @brief	Diffuse 색상 설정 */
	//------------------------------------------------------------------
	void Light::SetDiffuse( const Cindy::CnColor& Color )
	{
		m_Diffuse = Color;
	}

	//------------------------------------------------------------------
	/** @brief	Specular 색상 설정 */
	//------------------------------------------------------------------
	void Light::SetSpecular( const Cindy::CnColor& Color )
	{
		m_Specular = Color;
	}


	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void Light::RegisterProperties()
	{
		RegisterProperty<Light, const CnColor&>( "Ambient", &Light::GetAmibient, &Light::SetAmbient );
		RegisterProperty<Light, const CnColor&>( "Diffuse", &Light::GetDiffuse, &Light::SetDiffuse );
		RegisterProperty<Light, const CnColor&>( "Specular", &Light::GetSpecular, &Light::SetSpecular );
	}
}
