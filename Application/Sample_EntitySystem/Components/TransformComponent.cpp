#include "stdafx.h"
#include "TransformComponent.h"
#include "../SubSystem/TransformSystem.h"
#include "../Messages/SceneMessage.h"
#include <Cagetu/Core/Entity/Entity.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, ITransform, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( ITransform, L"Transform" );
	//------------------------------------------------------------------
	ITransform::ITransform()
	{
	}
	ITransform::~ITransform()
	{
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, Transform, ITransform, 0 );
	IMPLEMENT_COMPONENT_ID( Transform );
	//------------------------------------------------------------------
	Transform::Transform()
	{
		int a = 1;
	}
	Transform::~Transform()
	{
	}

	//------------------------------------------------------------------
	void Transform::Start()
	{
	}
	void Transform::End()
	{
	}

	//------------------------------------------------------------------
	void Transform::Update()
	{
		Msg::SetTransform msg;
		msg.m_Position = this->m_Position;
		msg.m_Rotation = this->m_Rotation;
		this->GetOwner()->HandleMessage( L"Scene", &msg );
	}
}
