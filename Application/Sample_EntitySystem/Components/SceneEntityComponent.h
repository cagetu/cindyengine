#pragma once

#include <Cagetu/Core/Component/ComponentManager.h>
#include <Cindy/Scene/CnLight.h>
//#include <Cindy/Geometry/CnMeshData.h>
//#include "<Cindy/Material/CnMaterialData.h>

namespace Component
{
	class SceneNode;
	class ModelNode;

	class ISceneEntity : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		ISceneEntity();
		virtual ~ISceneEntity();

		virtual void	SetName( const CnString& Name ) abstract;
		const CnString&	GetName() const		{	return m_Name;	}
	protected:
		CnString	m_Name;
	};

	// Mesh Entity
	class Mesh : public ISceneEntity
	{
		DECLARE_CLASS( Mesh );
		DECLARE_COMPONENT_ID;
	public:
		Mesh();
		virtual ~Mesh();

		// Initialize
		void	Start() override;
		void	End() override;

		void	SetName( const CnString& Name ) override;

		void	SetData( const CnString& MeshName, const CnString& MtlName );
	protected:
		CnString	m_MeshName;
		CnString	m_MtlName;

		Component::ModelNode*	m_pSceneComponent;
	};

	// Global Light Component
	class Light : public ISceneEntity
	{
		DECLARE_CLASS( Light );
		DECLARE_COMPONENT_ID;
	public:
		Light();
		virtual ~Light();

		// Initialize
		void	Start() override;
		void	End() override;

		// Set/Get
		void	SetName( const CnString& Name ) override;
		void	SetType( const CnLight::Type LightType );

		void	SetAmbient( const CnColor& Color );
		void	SetDiffuse( const CnColor& Color );
		void	SetSpecular( const CnColor& Color );

		const CnColor&	GetAmibient()	{	return m_Ambient;	}
		const CnColor&	GetDiffuse()	{	return m_Diffuse;	}
		const CnColor&	GetSpecular()	{	return m_Specular;	}

		static void		RegisterProperties();
	private:
		CnLight*		m_pLight;
		CnLight::Type	m_LightType;

		CnColor			m_Ambient;
		CnColor			m_Diffuse;
		CnColor			m_Specular;

		Component::SceneNode*	m_pSceneComponent;
	};

	//REGISTER_FACTORY( Entity );
}