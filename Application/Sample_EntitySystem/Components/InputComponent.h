#pragma once

#include <Cagetu/Core/Component/ComponentManager.h>
#include "CameraComponent.h"

namespace Component
{
	class ITransform;

	// BaseInput
	class IInput : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		IInput();
		virtual ~IInput();

		virtual void	Process() abstract;
	};

	class DeviceInput : public IInput
	{
		DECLARE_CLASS(DeviceInput);
		DECLARE_COMPONENT_ID;
	public:
		DeviceInput();
		virtual ~DeviceInput();

		void	Start() override;
		void	End() override;

		void	Process() override;
	};
}
