#pragma once

#include <Cagetu/Core/Component/ComponentManager.h>
#include <Cindy/Math/CindyMath.h>

namespace Component
{
	class ITransform : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		ITransform();
		virtual ~ITransform();

		virtual void	Update() abstract;
	protected:
		CnVector3		m_Position;
		CnQuaternion	m_Rotation;
		CnVector3		m_Scale;
	};

	// Implement...
	class Transform : public ITransform
	{
		DECLARE_CLASS( Transform );
		DECLARE_COMPONENT_ID;
	public:
		Transform();
		virtual ~Transform();

		// Initialize
		void				Start() override;
		void				End() override;

		// Position
		void				SetPosition( const CnVector3& Position )	{	m_Position = Position;	}
		const CnVector3&	GetPosition() const							{	return m_Position;		}

		// Rotate
		void				SetRotation( const CnQuaternion& Rotation )	{	m_Rotation = Rotation;	}
		const CnQuaternion&	GetRotation() const							{	return m_Rotation;		}

		// Scale
		void				SetScale( const CnVector3& Scale )			{	m_Scale = Scale;		}
		const CnVector3&	GetScale() const							{	return m_Scale;			}

		// Update
		void				Update() override;
	};
}
