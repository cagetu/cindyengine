#pragma once

#include <Cagetu/Core/Component/Component.h>
#include "../Messages/CameraMessage.h"

class Cindy::CnViewport;

namespace Component
{
	class ICamera : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		ICamera();
		virtual ~ICamera();

		virtual CameraPtr	GetCamera() const abstract;
	};

	// ModelViewCameraComponent
	class ModelViewCamera : public ICamera
	{
		DECLARE_CLASS( ModelViewCamera );
		DECLARE_COMPONENT_ID;
	public:
		ModelViewCamera();
		virtual ~ModelViewCamera();

		// Initialize
		void				Start() override;
		void				End() override;

		// Message
		bool				IsValid( Message::IMsg* pMsg ) override;
		void				HandleMessage( Message::IMsg* pMsg ) override;

		// Camera
		virtual CameraPtr	GetCamera() const override	{	return m_spCamera.GetPtr();	}

		// 
		void	SetNumber( int Number )	{	m_Number = Number;	}
		int		GetNumber()				{	return m_Number;	}
	protected:
		MdvCamPtr	m_spCamera;

		int		m_Number;
	private:
		// Setup
		void		OnMsgSetup( const Msg::CameraToViewport* pMessage );

		static void		RegisterProperties();
	};
}
