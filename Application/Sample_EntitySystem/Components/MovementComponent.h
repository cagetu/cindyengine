#pragma once

#include <Cagetu/Core/Component/Component.h>
#include "CameraComponent.h"

namespace Component
{
	class IMovement : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		IMovement();
		virtual ~IMovement();

		virtual void	Update() abstract;
	};

	//==================================================================
	/** CameraMovement
		@author		cagetu
		@remarks	카메라 이동 처리
	*/
	//==================================================================
	class CameraMovement : public IMovement
	{
		DECLARE_CLASS(CameraMovement);
		DECLARE_COMPONENT_ID;
	public:
		CameraMovement();
		virtual ~CameraMovement();

		// 초기화
		void	Start() override;
		void	End() override;

		// Update
		void	Update() override;

		// Message
		bool	IsValid( Message::IMsg* pMsg ) override;
		void	HandleMessage( Message::IMsg* pMsg ) override;

	private:
		Component::ModelViewCamera*		m_CameraComponent;
	};


	// Custum Animation
	class AutoMovement : public IMovement
	{
		DECLARE_CLASS( AutoMovement );
		DECLARE_COMPONENT_ID;
	public:
		AutoMovement();
		virtual ~AutoMovement();

		// Initialize
		void	Start() override;
		void	End() override;

		// Update
		void	Update() override;

	private:
	};
}
