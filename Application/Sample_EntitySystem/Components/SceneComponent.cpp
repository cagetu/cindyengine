#include "stdafx.h"
#include "SceneComponent.h"
#include "TransformComponent.h"
#include <Cagetu/Core/Entity/Entity.h>
#include <Cindy/Geometry/CnMeshDataManager.h>
#include <Cindy/Material/CnMaterialDataManager.h>
#include <Cindy/Animation/CnSkeletonDataManager.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, ISceneGraph, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( ISceneGraph, L"Scene" );
	//------------------------------------------------------------------
	ISceneGraph::ISceneGraph()
	{
	}
	ISceneGraph::~ISceneGraph()
	{
	}

	//------------------------------------------------------------------
	void ISceneGraph::SetRootNode( CnSceneNode* pRoot )
	{
		m_pRootNode = pRoot;
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, SceneNode, ISceneGraph, &SceneNode::RegisterProperties );
	IMPLEMENT_COMPONENT_ID( SceneNode );
	//------------------------------------------------------------------
	SceneNode::SceneNode( const String& Name )
		: m_pSceneNode(0)
		, m_Name( Name )
	{
		//this->RegisterProcFunction( this, &SceneNode::OnMsgAttach );
		//this->RegisterProcFunction( this, &SceneNode::OnMsgTransform );
	}
	SceneNode::~SceneNode()
	{
	}

	//------------------------------------------------------------------
	/** @brief	초기화 */
	//------------------------------------------------------------------
	void SceneNode::Start()
	{
	}
	void SceneNode::End()
	{
		m_pRootNode->DeleteChild( m_pSceneNode->GetName() );
		m_pSceneNode = NULL;
	}

	//-------------------------------------------------------------------------
	/** @brief	부모 노드 */
	//-------------------------------------------------------------------------
	void SceneNode::SetRootNode( CnSceneNode* pRoot )
	{
		ISceneGraph::SetRootNode( pRoot );

		m_pSceneNode = CnDynamicCast(CnSceneNode, pRoot->CreateChild( m_Name ));
	}

	//------------------------------------------------------------------
	void SceneNode::SetPosition( const CnVector3& Position )
	{
		m_pSceneNode->SetPosition( Position );
	}
	const CnVector3& SceneNode::GetPosition()
	{
		return m_pSceneNode->GetWorldPosition();
	}

	//------------------------------------------------------------------
	void SceneNode::SetRotation( const CnQuaternion& Rotation )
	{
		m_pSceneNode->SetRotation( Rotation );
	}
	const CnQuaternion& SceneNode::GetRotation() const
	{
		return m_pSceneNode->GetWorldRotation();
	}

	//------------------------------------------------------------------
	void SceneNode::SetScale( const CnVector3& Scale )
	{
		m_pSceneNode->SetScale( Scale );
	}
	const CnVector3& SceneNode::GetScale() const
	{
		return m_pSceneNode->GetWorldScale();
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지*/
	//-------------------------------------------------------------------------
	bool SceneNode::IsValid( Message::IMsg* pMsg )
	{
		if (pMsg->GetId() == Msg::AttachNode::Id ||
			pMsg->GetId() == Msg::SetTransform::Id ||
			pMsg->GetId() == Msg::SetPosition::Id ||
			pMsg->GetId() == Msg::SetRotation::Id ||
			pMsg->GetId() == Msg::SetScale::Id)
			return true;

		return false;
	}
	void SceneNode::HandleMessage( Message::IMsg* pMsg )
	{
		if (Msg::AttachNode::Id == pMsg->GetId())
		{
			OnMsgAttach( static_cast<Msg::AttachNode*>(pMsg) );
		}
		else if (Msg::SetTransform::Id == pMsg->GetId())
		{
			OnMsgTransform( static_cast<Msg::SetTransform*>(pMsg) );
		}
		else if (Msg::SetPosition::Id == pMsg->GetId())
		{
			Msg::SetPosition* msg = static_cast<Msg::SetPosition*>(pMsg);
			m_pSceneNode->SetPosition( msg->m_Position );
		}
		else if (Msg::SetRotation::Id == pMsg->GetId())
		{
			Msg::SetRotation* msg = static_cast<Msg::SetRotation*>(pMsg);
			m_pSceneNode->SetRotation( msg->m_Rotation );
		}
		else if (Msg::SetScale::Id == pMsg->GetId())
		{
			Msg::SetScale* msg = static_cast<Msg::SetScale*>(pMsg);
			m_pSceneNode->SetScale( msg->m_Scale );
		}

		return;
	}

	//------------------------------------------------------------------
	void SceneNode::OnMsgAttach( Msg::AttachNode* pMsg )
	{
		pMsg->GetSceneNode()->AttachChild( this->m_pSceneNode );
	}
	void SceneNode::OnMsgTransform( Msg::SetTransform* pMsg )
	{
		m_pSceneNode->SetPosition( pMsg->m_Position );
		m_pSceneNode->SetRotation( pMsg->m_Rotation );
	}

	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void SceneNode::RegisterProperties()
	{
		//RegisterProperty<SceneNode, CnSceneNode*>( "SceneNode", &SceneNode::GetSceneNode, NULL );
		RegisterProperty<SceneNode, const CnVector3&>( "Position", &SceneNode::GetPosition, &SceneNode::SetPosition );
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, ModelNode, ISceneGraph, &ModelNode::RegisterProperties );
	IMPLEMENT_COMPONENT_ID( ModelNode );
	//------------------------------------------------------------------
	ModelNode::ModelNode( const String& Name )
		: m_Name( Name )
	{
	}
	ModelNode::~ModelNode()
	{
	}

	//------------------------------------------------------------------
	/** @brief	초기화 */
	//------------------------------------------------------------------
	void ModelNode::Start()
	{
	}
	void ModelNode::End()
	{
		m_pRootNode->DeleteChild( m_pModelNode->GetName() );
		m_pModelNode = NULL;
	}

	//-------------------------------------------------------------------------
	/** @brief	부모 노드 */
	//-------------------------------------------------------------------------
	void ModelNode::SetRootNode( CnSceneNode* pRoot )
	{
		ISceneGraph::SetRootNode( pRoot );

		m_pModelNode = (CnModelNode*)CnFactory::Instance()->Create( "Cindy@CnModelNode" );
		m_pModelNode->SetName( this->GetName() );
		pRoot->AttachChild( m_pModelNode );
	}

	//------------------------------------------------------------------
	void ModelNode::SetPosition( const CnVector3& Position )
	{
		m_pModelNode->SetPosition( Position );
	}
	const CnVector3& ModelNode::GetPosition()
	{
		return m_pModelNode->GetWorldPosition();
	}

	//------------------------------------------------------------------
	void ModelNode::SetRotation( const CnQuaternion& Rotation )
	{
		m_pModelNode->SetRotation( Rotation );
	}
	const CnQuaternion& ModelNode::GetRotation() const
	{
		return m_pModelNode->GetWorldRotation();
	}

	//------------------------------------------------------------------
	void ModelNode::SetScale( const CnVector3& Scale )
	{
		m_pModelNode->SetScale( Scale );
	}
	const CnVector3& ModelNode::GetScale() const
	{
		return m_pModelNode->GetWorldScale();
	}

	//------------------------------------------------------------------
	/** @brief	메쉬 추가 */
	//------------------------------------------------------------------
	bool ModelNode::AddMesh( const String& Mesh, const String& Material )
	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( Mesh.c_str() );
		MtlDataPtr spMtl = CnMaterialDataManager::Instance()->Load( Material.c_str() );

		bool resultMesh = m_pModelNode->AttachMesh( spMesh );
		bool resultMtl = m_pModelNode->AttachMaterial( Mesh, spMtl );

		//m_pModelNode->UpdateTransform( true, false );

		return resultMesh & resultMtl;
	}
	//------------------------------------------------------------------
	/** @brief	뼈대 추가 */
	//------------------------------------------------------------------
	bool ModelNode::AddSkeleton( const String& Name )
	{
		SkelDataPtr spSkeleton = CnSkeletonDataManager::Instance()->Load( Name.c_str() );
		return m_pModelNode->AttachSkeleton( spSkeleton );
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지*/
	//-------------------------------------------------------------------------
	bool ModelNode::IsValid( Message::IMsg* pMsg )
	{
		if (//pMsg->GetId() == Msg::AttachNode::Id ||
			pMsg->GetId() == Msg::SetTransform::Id ||
			pMsg->GetId() == Msg::SetPosition::Id ||
			pMsg->GetId() == Msg::SetRotation::Id ||
			pMsg->GetId() == Msg::SetScale::Id ||
			pMsg->GetId() == Msg::AttachMesh::Id ||
			pMsg->GetId() == Msg::DetachMesh::Id)
			return true;

		return false;
	}
	void ModelNode::HandleMessage( Message::IMsg* pMsg )
	{
		if (Msg::AttachNode::Id == pMsg->GetId())
		{
			//OnMsgAttach( static_cast<Msg::AttachNode*>(pMsg) );
		}
		else if (Msg::SetTransform::Id == pMsg->GetId())
		{
			Msg::SetTransform* msg = static_cast<Msg::SetTransform*>(pMsg);
			m_pModelNode->SetPosition( msg->m_Position );
		}
		else if (Msg::SetPosition::Id == pMsg->GetId())
		{
			Msg::SetPosition* msg = static_cast<Msg::SetPosition*>(pMsg);
			m_pModelNode->SetPosition( msg->m_Position );
		}
		else if (Msg::SetRotation::Id == pMsg->GetId())
		{
			Msg::SetRotation* msg = static_cast<Msg::SetRotation*>(pMsg);
			m_pModelNode->SetRotation( msg->m_Rotation );
		}
		else if (Msg::SetScale::Id == pMsg->GetId())
		{
			Msg::SetScale* msg = static_cast<Msg::SetScale*>(pMsg);
			m_pModelNode->SetScale( msg->m_Scale );
		}
		else if (Msg::AttachMesh::Id == pMsg->GetId())
		{
			Msg::AttachMesh* msg = static_cast<Msg::AttachMesh*>(pMsg);
			AddMesh( msg->MeshName, msg->MtlName );
		}
		else if (Msg::DetachMesh::Id == pMsg->GetId())
		{
			Msg::DetachMesh* msg = static_cast<Msg::DetachMesh*>(pMsg);
			m_pModelNode->DetachMesh( msg->MeshName );
		}

		return;
	}

	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void ModelNode::RegisterProperties()
	{
		RegisterProperty<ModelNode, const String&>( "Name", &ModelNode::GetName, &ModelNode::SetName );
	}
}
