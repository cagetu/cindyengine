#pragma once

#include <Cagetu/Core/Component/Component.h>

namespace Component
{
	class ITimer : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		ITimer();
		virtual ~ITimer();

		virtual void	Update() abstract;
	};

	//==================================================================
	/** Timer
		@author		cagetu
		@remarks	시간 업데이터
	*/
	//==================================================================
	class Timer : public ITimer
	{
		DECLARE_CLASS(Timer);
		DECLARE_COMPONENT_ID;
	public:
		Timer();
		virtual ~Timer();

		float	GetElapesedTime() const	{	return m_ElapsedTime;		}

		void	Update() override;
	protected:
		float	m_ElapsedTime;
		DWORD	m_PrevFrameTick;
	};
}
