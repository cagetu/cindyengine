#pragma once

#include <Cagetu/Core/Component/Component.h>
#include "../PhysX/Physic.h"

namespace Component
{
	class ISceneGraph;

	class IPhysic : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		IPhysic();
		virtual ~IPhysic();

		virtual void	Process() abstract;
	};

	//==================================================================
	/** Collision - PhysX
		@author		cagetu
		@remarks	충돌 처리 Component
					PhysX를 이용한다.
	*/
	//==================================================================
	class CollisionPhysX : public IPhysic
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_ID;
	public:
		enum BoundingType
		{
			BT_Plane = 0,
			BT_Box,
			BT_Sphere,
		};

		//------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------
		CollisionPhysX();
		virtual ~CollisionPhysX();

		// Name
		void				SetName( const char* Name )	{	m_Name = Name;	}
		const std::string&	GetName() const				{	return m_Name;	}

		// Start/End
		virtual void		Start() override;
		virtual void		End() override;
	protected:
		std::string		m_Name;
		NxActor*		m_Actor;
		BoundingType	m_ActorType;

		Component::ISceneGraph*	m_SceneComponent;

		// Actor
		void	Process() override;
	};

	class CollisionPlane : public CollisionPhysX
	{
		DECLARE_CLASS(CollisionPlane);
		DECLARE_COMPONENT_ID;
	public:
		CollisionPlane();
		virtual ~CollisionPlane();

		void	Start() override;
		void	End() override;

	protected:
		void	SetActor( const char* Name, const CnVector3& Normal, float Distance );
	};

	class CollisionBox : public CollisionPhysX
	{
		DECLARE_CLASS(CollisionBox);
		DECLARE_COMPONENT_ID;
	public:
		CollisionBox();
		virtual ~CollisionBox();

		void	Start() override;
		void	End() override;

	protected:
		void	SetActor( const char* Name, const CnVector3& Center, const CnVector3& Extent, const CnVector3& Position );
	};

	class CollisionSphere : public CollisionPhysX
	{
		DECLARE_CLASS(CollisionSphere);
		DECLARE_COMPONENT_ID;
	public:
		CollisionSphere();
		virtual ~CollisionSphere();

		void	Start() override;
		void	End() override;

	protected:
		void	SetActor( const char* Name, const CnVector3& Center, float Radius, const CnVector3& Position );
	};
}
