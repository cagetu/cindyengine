#include "stdafx.h"
#include "TimerComponent.h"
#include "../Messages/SceneMessage.h"

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, ITimer, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( ITimer, L"Timer" );
	//------------------------------------------------------------------
	ITimer::ITimer()
	{
	}
	ITimer::~ITimer()
	{
	}

	//==================================================================
	IMPLEMENT_RTTI( Component, Timer, ITimer );
	IMPLEMENT_COMPONENT_ID( Timer );
	//------------------------------------------------------------------
	Timer::Timer()
		: m_ElapsedTime(0.0f)
		, m_PrevFrameTick(0)
	{
		m_PrevFrameTick = timeGetTime();
	}
	Timer::~Timer()
	{
	}

	//------------------------------------------------------------------
	void Timer::Update()
	{
		DWORD currentTime = timeGetTime();
		DWORD elapsedTime = currentTime - m_PrevFrameTick;
		m_PrevFrameTick = currentTime;
		m_ElapsedTime = (float)(elapsedTime)*0.001f;		
	}
}
