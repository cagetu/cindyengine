#include "stdafx.h"
#include "PhysicComponent.h"
#include "SceneComponent.h"
#include "SceneEntityComponent.h"
#include <Cagetu/Core/Entity/Entity.h>
#include <MoCommon/MoStringUtil.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, IPhysic, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( IPhysic, L"Physic" );
	//------------------------------------------------------------------
	IPhysic::IPhysic()
	{
	}
	IPhysic::~IPhysic()
	{
	}

	//==================================================================
	IMPLEMENT_RTTI( Component, CollisionPhysX, IPhysic );
	IMPLEMENT_COMPONENT_ID( CollisionPhysX );
	//------------------------------------------------------------------
	CollisionPhysX::CollisionPhysX()
		: m_Actor(0)
	{
	}
	CollisionPhysX::~CollisionPhysX()
	{
	}
	
	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void CollisionPhysX::Start()
	{
		m_SceneComponent = (Component::ISceneGraph*)this->GetOwner()->GetComponent( L"Scene" );
	}
	void CollisionPhysX::End()
	{
		m_SceneComponent = NULL;
	}

	//------------------------------------------------------------------
	void CollisionPhysX::Process()
	{
		if (!m_Actor)
			return;

		NxShape* const* shapes = m_Actor->getShapes();
		unsigned int numShapes = m_Actor->getNbShapes();
		while (numShapes--)
		{
			NxShape* shape = shapes[numShapes];

			NxShapeType type = shape->getType();
			switch (type)
			{
			case NX_SHAPE_PLANE:
				break;
			case NX_SHAPE_BOX:
				{
					NxVec3 localPos = shape->getLocalPosition();
					NxVec3 globalPos = shape->getGlobalPosition();

					NxMat34 pose = shape->getGlobalPose();
					NxVec3 boxDim = shape->isBox()->getDimensions();

					// draw...
					CnVector3 pos( pose.t.x, pose.t.y, pose.t.z );

					CnVector3 right = pose.M.getRow(0).get();
					CnVector3 up = pose.M.getRow(1).get();
					CnVector3 dir = pose.M.getRow(2).get();

					CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
									 up.x, up.y, up.z, 0.0f,
									 dir.x, dir.y, dir.z, 0.0f,
									 0.0f, 0.0f, 0.0f, 1.0f );

					CnQuaternion quat;
					rot.GetRotation( quat );

					CnVector3 scale( boxDim.x,boxDim.y, boxDim.z );

					if (m_SceneComponent)
					{
						m_SceneComponent->SetPosition( pos );
						m_SceneComponent->SetRotation( quat );
					}
				}
				break;
			case NX_SHAPE_SPHERE:
				{
					NxMat34 pose = shape->getGlobalPose();

					CnVector3 pos( pose.t.x, pose.t.y, pose.t.z );

					CnVector3 right = pose.M.getRow(0).get();
					CnVector3 up = pose.M.getRow(1).get();
					CnVector3 dir = pose.M.getRow(2).get();

					CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
									 up.x, up.y, up.z, 0.0f,
									 dir.x, dir.y, dir.z, 0.0f,
									 0.0f, 0.0f, 0.0f, 1.0f );

					CnQuaternion quat;
					rot.GetRotation( quat );

					if (m_SceneComponent)
					{
						m_SceneComponent->SetPosition( pos );
						m_SceneComponent->SetRotation( quat );
					}
				}
				break;
			case NX_SHAPE_CAPSULE:
				break;
			case NX_SHAPE_CONVEX:
				break;
			case NX_SHAPE_MESH:
				break;
			case NX_SHAPE_WHEEL:
				break;
			case NX_SHAPE_HEIGHTFIELD:
				break;
			}
		}
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, CollisionPlane, CollisionPhysX, 0 );
	IMPLEMENT_COMPONENT_ID( CollisionPlane );
	//------------------------------------------------------------------
	CollisionPlane::CollisionPlane()
	{
	}
	CollisionPlane::~CollisionPlane()
	{
	}

	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void CollisionPlane::Start()
	{
		CollisionPhysX::Start();

		SetActor( 0, CnVector3(), 1.0f );
	}
	void CollisionPlane::End()
	{
		CollisionPhysX::End();
	}

	//------------------------------------------------------------------
	void CollisionPlane::SetActor( const char* Name, const CnVector3& Normal, float Distance )
	{
		NxPlaneShapeDesc planeDesc;
		planeDesc.name = Name;
		//planeDesc.localPose.t = NxVec3(0.0f, 0.0f, 0.0f);

		NxActorDesc actorDesc;
		//actorDesc.globalPose.t = NxVec3(0.0f, -1.0f, 0.0f);
		actorDesc.shapes.pushBack( &planeDesc );

		m_Actor = PhysXInstance->Scene()->createActor( actorDesc );

		m_ActorType = BT_Plane;
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, CollisionBox, CollisionPhysX, 0 );
	IMPLEMENT_COMPONENT_ID( CollisionBox );
	//------------------------------------------------------------------
	CollisionBox::CollisionBox()
	{
	}
	CollisionBox::~CollisionBox()
	{
	}

	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void CollisionBox::Start()
	{
		CollisionPhysX::Start();

		Component::ISceneGraph* sceneComponent = DynamicCast<Component::ISceneGraph>( this->GetOwner()->GetComponent( L"Scene" ) );
		Component::Mesh* meshComponent = DynamicCast<Component::Mesh>( this->GetOwner()->GetComponent( L"SceneEntity" ) );
		if (sceneComponent && meshComponent)
		{
			CnSceneNode* sceneNode = sceneComponent->GetSceneNode();
			CnVector3 pos = sceneNode->GetWorldPosition();
			//CnVector3 pos = m_SceneComponent->GetPosition();

			sceneNode->UpdateTransform( true, false );

			CnAABB aabb;
			sceneNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();
			CnVector3 min = aabb.GetMin();
			CnVector3 max = aabb.GetMax();

			CnVector3 extent = max - min;
			extent *= 0.5f;

			char buffer[128];
			MoStringUtil::WidecharToMultibyte( sceneComponent->GetName().c_str(), buffer );

			SetActor( buffer, cen, extent, pos );
		}
	}
	void CollisionBox::End()
	{
		CollisionPhysX::End();
	}

	//------------------------------------------------------------------
	void CollisionBox::SetActor( const char* Name, const CnVector3& Center, const CnVector3& Extent, const CnVector3& Position )
	{
		NxVec3 local( Center.x, Center.y, Center.z );
		NxVec3 global( Position.x, Position.y, Position.z );

		m_Actor = PhysXInstance->CreateBox( Name, global, local, NxVec3( Extent.x, Extent.y, Extent.z ) );

		m_ActorType = BT_Box;
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, CollisionSphere, CollisionPhysX, 0 );
	IMPLEMENT_COMPONENT_ID( CollisionSphere );
	//------------------------------------------------------------------
	CollisionSphere::CollisionSphere()
	{
	}
	CollisionSphere::~CollisionSphere()
	{
	}

	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void CollisionSphere::Start()
	{
		CollisionPhysX::Start();

		Component::ISceneGraph* sceneComponent = DynamicCast<Component::ISceneGraph>( this->GetOwner()->GetComponent( L"Scene" ) );
		if (sceneComponent)
		{
			CnSceneNode* sceneNode = sceneComponent->GetSceneNode();
			CnVector3 pos = sceneNode->GetWorldPosition();
			//CnVector3 pos = m_SceneComponent->GetPosition();

			CnAABB aabb;
			sceneNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();
			CnVector3 min = aabb.GetMin();
			CnVector3 max = aabb.GetMax();

			CnVector3 extent = max - cen;
			float radius = max.x - cen.x;

			char buffer[128];
			MoStringUtil::WidecharToMultibyte( sceneComponent->GetName().c_str(), buffer );

			SetActor( buffer, cen, radius, pos );
		}
	}
	void CollisionSphere::End()
	{
		CollisionPhysX::End();
	}

	//------------------------------------------------------------------
	void CollisionSphere::SetActor( const char* Name, const CnVector3& Center, float Radius, const CnVector3& Position )
	{
		NxVec3 local( Center.x, Center.y, Center.z );
		NxVec3 global( Position.x, Position.y, Position.z );

		m_Actor = PhysXInstance->CreateSphere( Name, global, local, Radius );

		m_ActorType = BT_Sphere;
	}

}