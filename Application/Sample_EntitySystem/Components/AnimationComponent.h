#pragma once

#include <Cagetu/Core/Component/Component.h>

namespace Component
{
	class Transform;

	class IAnimation : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		IAnimation();
		virtual ~IAnimation();

		virtual void	Update() abstract;
	};
}
