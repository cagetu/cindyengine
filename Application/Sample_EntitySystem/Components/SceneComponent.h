#pragma once

#include <Cagetu/Core/Component/ComponentManager.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnSceneGraph.h>
#include "../Messages/SceneMessage.h"

namespace Component
{
	// 장면 그래프 컴포넌트
	class ISceneGraph : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		ISceneGraph();
		virtual ~ISceneGraph();

		// 장면 그래프
		virtual void			SetRootNode( CnSceneNode* pRoot );

		virtual const String&	GetName() abstract;
		virtual CnSceneNode*	GetSceneNode() abstract;

		// Position
		virtual void				SetPosition( const CnVector3& Position ) abstract;
		virtual const CnVector3&	GetPosition() abstract;

		// Rotate
		virtual void				SetRotation( const CnQuaternion& Rotation ) abstract;
		virtual const CnQuaternion&	GetRotation() const abstract;

		// Scale
		virtual void				SetScale( const CnVector3& Scale ) abstract;
		virtual const CnVector3&	GetScale() const abstract;

	protected:
		CnSceneNode*	m_pRootNode;
	};

	// Implements
	class SceneNode : public ISceneGraph
	{
		DECLARE_CLASS( SceneNode );
		DECLARE_COMPONENT_ID;
	public:
		SceneNode( const String& Name = L"" ); 
		virtual ~SceneNode();

		// Initialize
		void			Start() override;
		void			End() override;

		//
		void			SetName( const String& Name )	{	m_Name = Name;	}
		const String&	GetName() override				{	return m_Name;	}

		// 장면 그래프
		void			SetRootNode( CnSceneNode* pRoot ) override;
		CnSceneNode*	GetSceneNode() override		{	return m_pSceneNode;	}

		// Position
		void				SetPosition( const CnVector3& Position ) override;
		const CnVector3&	GetPosition() override;

		// Rotate
		void				SetRotation( const CnQuaternion& Rotation ) override;
		const CnQuaternion&	GetRotation() const override;

		// Scale
		void				SetScale( const CnVector3& Scale ) override;
		const CnVector3&	GetScale() const override;

		// Message
		bool			IsValid( Message::IMsg* pMsg );
		void			HandleMessage( Message::IMsg* pMsg );

	private:
		String			m_Name;
		CnSceneNode*	m_pSceneNode;

		// Message
		void	OnMsgAttach( Msg::AttachNode* pMsg );
		void	OnMsgTransform( Msg::SetTransform* pMsg );

		static void		RegisterProperties();
	};

	class ModelNode : public ISceneGraph
	{
		DECLARE_CLASS( ModelNode );
		DECLARE_COMPONENT_ID;
	public:
		ModelNode( const String& Name = L"" );
		virtual ~ModelNode();

		// Initialize
		void			Start() override;
		void			End() override;

		// Node 이름
		void			SetName( const String& Name )	{	m_Name = Name;	}
		const String&	GetName() override				{	return m_Name;	}

		// 장면 그래프
		void			SetRootNode( CnSceneNode* pRoot ) override;
		CnSceneNode*	GetSceneNode() override		{	return m_pModelNode;	}
		
		// Position
		void				SetPosition( const CnVector3& Position ) override;
		const CnVector3&	GetPosition() override;

		// Rotate
		void				SetRotation( const CnQuaternion& Rotation ) override;
		const CnQuaternion&	GetRotation() const override;

		// Scale
		void				SetScale( const CnVector3& Scale ) override;
		const CnVector3&	GetScale() const override;

		// 메쉬
		bool			AddMesh( const String& Mesh, const String& Material );

		// 뼈대
		bool			AddSkeleton( const String& Name );

		// 애니메이션

		// Message
		bool			IsValid( Message::IMsg* pMsg ) override;
		void			HandleMessage( Message::IMsg* pMsg ) override;

	private:
		String			m_Name;
		CnModelNode*	m_pModelNode;

		static void		RegisterProperties();
	};
}
