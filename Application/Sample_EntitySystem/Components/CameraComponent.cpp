#include "stdafx.h"
#include "CameraComponent.h"
#include "../Messages/CameraMessage.h"
#include "../Messages/MoveMessage.h"

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Render/CnRenderer.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, ICamera, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( ICamera, L"Camera" );
	//------------------------------------------------------------------
	ICamera::ICamera()
	{
	}
	ICamera::~ICamera()
	{
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, ModelViewCamera, ICamera, &ModelViewCamera::RegisterProperties );
	IMPLEMENT_COMPONENT_ID( ModelViewCamera );
	//------------------------------------------------------------------
	//------------------------------------------------------------------
	void ModelViewCamera::RegisterProperties()
	{
		RegisterProperty<ModelViewCamera, int>( "Number", &ModelViewCamera::GetNumber, &ModelViewCamera::SetNumber );
	}
	//------------------------------------------------------------------
	ModelViewCamera::ModelViewCamera()
	{
		//this->RegisterProcFunction( this, &ModelViewCamera::OnMsgSetup );
	}
	ModelViewCamera::~ModelViewCamera()
	{
	}

	//------------------------------------------------------------------
	/** @brief	초기화 */
	//------------------------------------------------------------------
	void ModelViewCamera::Start()
	{
		if (false == m_bInitialized)
		{
			// Camera
			m_spCamera = CnNew CnModelViewCamera( L"main" );

			m_spCamera->SetFar( 1000.0f );
			m_spCamera->SetRadius( 1.0f, 500.0f );
			m_spCamera->SetRadius( 50.0f );
			m_spCamera->SetLookAt( CnVector3( 0.0f, 10.0f, 0.0f ) );

			SetEnable( true );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Clear */
	//------------------------------------------------------------------
	void ModelViewCamera::End()
	{
		if (m_bInitialized)
		{
			m_spCamera.SetNull();
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지*/
	//-------------------------------------------------------------------------
	bool ModelViewCamera::IsValid( Message::IMsg* pMsg )
	{
		if (pMsg->GetId() == Msg::CameraToViewport::Id)
			//pMsg->GetId() == Msg::MoveToForward::Id ||
			//pMsg->GetId() == Msg::MoveToBack::Id ||
			//pMsg->GetId() == Msg::MoveToRight::Id ||
			//pMsg->GetId() == Msg::MoveToLeft::Id )
			return true;

		return false;
	}
	void ModelViewCamera::HandleMessage( Message::IMsg* pMsg )
	{
		static CnVector3 dir, lookAt;

		if (Msg::CameraToViewport::Id == pMsg->GetId())
		{
			OnMsgSetup( static_cast<Msg::CameraToViewport*>(pMsg) );
		}
		//else if (Msg::MoveToForward::Id == pMsg->GetId())
		//{
		//	m_spCamera->GetDirection( dir );
		//	
		//	lookAt = m_spCamera->GetLookAt();
		//	m_spCamera->SetLookAt( lookAt + dir );
		//}
		//else if (Msg::MoveToBack::Id == pMsg->GetId())
		//{
		//	m_spCamera->GetDirection( dir );

		//	lookAt = m_spCamera->GetLookAt();
		//	m_spCamera->SetLookAt( lookAt - dir );
		//}
		//else if (Msg::MoveToLeft::Id == pMsg->GetId())
		//{
		//	m_spCamera->GetRight( dir );

		//	lookAt = m_spCamera->GetLookAt();
		//	m_spCamera->SetLookAt( lookAt + dir );
		//}
		//else if (Msg::MoveToRight::Id == pMsg->GetId())
		//{
		//	m_spCamera->GetRight( dir );

		//	lookAt = m_spCamera->GetLookAt();
		//	m_spCamera->SetLookAt( lookAt - dir );
		//}
		return;
	}

	//------------------------------------------------------------------
	/** @brief	메세지 처리 */
	//------------------------------------------------------------------
	void ModelViewCamera::OnMsgSetup( const Msg::CameraToViewport* pMessage )
	{
		pMessage->viewport->SetCamera( m_spCamera );
	}
}
