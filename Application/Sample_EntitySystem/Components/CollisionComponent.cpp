#include "stdafx.h"
#include "CollisionComponent.h"
#include "TransformComponent.h"
#include "SceneComponent.h"
#include <Cagetu/Core/Entity/Entity.h>
#include <MoCommon/MoStringUtil.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, ICollision, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( ICollision, L"Collision" );
	//------------------------------------------------------------------
	ICollision::ICollision()
	{
	}
	ICollision::~ICollision()
	{
	}
}