#include "stdafx.h"
#include "MovementComponent.h"
#include "SceneComponent.h"
#include "../Messages/MoveMessage.h"

#include <Cagetu/Core/Entity/Entity.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, IMovement, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( IMovement, L"Movement" );
	//------------------------------------------------------------------
	IMovement::IMovement()
	{
	}
	IMovement::~IMovement()
	{
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, CameraMovement, ICamera, 0 );
	IMPLEMENT_COMPONENT_ID( CameraMovement );
	//------------------------------------------------------------------
	CameraMovement::CameraMovement()
	{
	}
	CameraMovement::~CameraMovement()
	{
	}

	void CameraMovement::Update()
	{	// Blank...Blank...Blank...Blank...
	}

	//------------------------------------------------------------------
	/** @brief	초기화 */
	//------------------------------------------------------------------
	void CameraMovement::Start()
	{
		if (false == m_bInitialized)
		{
			Core::Entity* owner = this->GetOwner();
			IComponent* component = owner->GetComponent( L"Camera" );
			if (component)
			{
				m_CameraComponent = DynamicCast<Component::ModelViewCamera>( component );
				if (m_CameraComponent)
				{
					SetEnable( true );
				}
			}
		}
	}

	//------------------------------------------------------------------
	/** @brief	Clear */
	//------------------------------------------------------------------
	void CameraMovement::End()
	{
		if (m_bInitialized)
		{
			m_CameraComponent = NULL;
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지*/
	//-------------------------------------------------------------------------
	bool CameraMovement::IsValid( Message::IMsg* pMsg )
	{
		if (pMsg->GetId() == Msg::MoveToForward::Id ||
			pMsg->GetId() == Msg::MoveToBack::Id ||
			pMsg->GetId() == Msg::MoveToRight::Id ||
			pMsg->GetId() == Msg::MoveToLeft::Id )
			return true;

		return false;
	}
	void CameraMovement::HandleMessage( Message::IMsg* pMsg )
	{
		if (m_CameraComponent)
		{
			static CnVector3 dir, lookAt;

			MdvCamPtr camPtr = m_CameraComponent->GetCamera();

			if (pMsg->GetId() == Msg::MoveToForward::Id)	// 앞으로
			{
				camPtr->GetDirection( dir );
				
				lookAt = camPtr->GetLookAt();
				camPtr->SetLookAt( lookAt + dir );
			}
			if (pMsg->GetId() == Msg::MoveToBack::Id)	// 뒤로
			{
				camPtr->GetDirection( dir );

				lookAt = camPtr->GetLookAt();
				camPtr->SetLookAt( lookAt - dir );
			}
			if (pMsg->GetId() == Msg::MoveToLeft::Id)	// 왼쪽
			{
				camPtr->GetRight( dir );

				lookAt = camPtr->GetLookAt();
				camPtr->SetLookAt( lookAt + dir );
			}
			if (pMsg->GetId() == Msg::MoveToRight::Id)	// 오른쪽
			{
				camPtr->GetRight( dir );

				lookAt = camPtr->GetLookAt();
				camPtr->SetLookAt( lookAt - dir );
			}
		}
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, AutoMovement, IMovement, 0 );
	IMPLEMENT_COMPONENT_ID( AutoMovement );
	//------------------------------------------------------------------
	AutoMovement::AutoMovement()
	{
	}
	AutoMovement::~AutoMovement()
	{
	}

	//------------------------------------------------------------------
	void AutoMovement::Start()
	{
	}
	void AutoMovement::End()
	{
	}

	//------------------------------------------------------------------
	void AutoMovement::Update()
	{
		Component::ISceneGraph* scene = static_cast<Component::ISceneGraph*>( this->GetOwner()->GetComponent( L"Scene" ) );
		if (scene)
		{
			static float angle = 0.0f;
			const float dangle = 1.0f;
			const float radius = 40.0f;

			angle += (dangle* 3.14f/180.0f);

			CnVector3 pos;
			pos.x = 10.0f + (float)(10 * cos(angle));
			pos.y = 10.0f;
			pos.z = 10.0f + (float)(10 * sin(angle));

			scene->SetPosition( pos );
		}
	}
}
