#pragma once

#include <Cagetu/Core/Component/Component.h>
#include "../PhysX/Physic.h"

namespace Component
{
	class Transform;

	class ICollision : public Core::IComponent
	{
		DECLARE_RTTI;
		DECLARE_COMPONENT_TYPE;
	public:
		ICollision();
		virtual ~ICollision();

		virtual void	Process() abstract;
	};
}
