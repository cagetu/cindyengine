#include "stdafx.h"
#include "InputComponent.h"
#include "../SubSystem/InputSystem.h"
#include "../Messages/MoveMessage.h"

#include <Cagetu/Core/Entity/Entity.h>

namespace Component
{
	//==================================================================
	IMPLEMENT_RTTI( Component, IInput, Core::IComponent );
	IMPLEMENT_COMPONENT_TYPE( IInput, L"Input" );
	//------------------------------------------------------------------
	IInput::IInput()
	{
	}
	IInput::~IInput()
	{
	}

	//==================================================================
	IMPLEMENT_CLASS( Component, DeviceInput, IInput, 0 );
	IMPLEMENT_COMPONENT_ID( DeviceInput );
	//------------------------------------------------------------------
	DeviceInput::DeviceInput()
	{
	}
	DeviceInput::~DeviceInput()
	{
	}

	//------------------------------------------------------------------
	void DeviceInput::Start()
	{
		if (m_bInitialized)
			return;

		SetEnable( true );

		m_bInitialized = true;
	}
	void DeviceInput::End()
	{
		if (false == m_bInitialized)
			return;

		SetEnable(false);
	}

	//------------------------------------------------------------------
	/** @brief	Update */
	//------------------------------------------------------------------
	void DeviceInput::Process()
	{
		if (false == IsEnabled())
			return;

		if (::GetAsyncKeyState('W'))
		{	// 앞으로
			GetOwner()->HandleMessage( L"Movement", &Msg::MoveToForward() );
		}
		if (::GetAsyncKeyState('S'))
		{	// 뒤로
			GetOwner()->HandleMessage( L"Movement", &Msg::MoveToBack() );
		}
		if (::GetAsyncKeyState('A'))
		{	// 왼쪽으로
			GetOwner()->HandleMessage( L"Movement", &Msg::MoveToLeft() );
		}
		if (::GetAsyncKeyState('D'))
		{	// 오른쪽
			GetOwner()->HandleMessage( L"Movement", &Msg::MoveToRight() );
		}
	}

}
