#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Lighting/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

#include <Cindy/Graphics/Base/CnRenderTexture.h>
#include <Cindy/Graphics/Base/CnRenderWindow.h>
#include <Cindy/Graphics/Base/CnMultipleRenderTarget.h>

//#include <Cindy/Geometry/CnScreenAlignedQuad.h>

CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	ShaderMgr->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd, pStrTitle,
													nWidth, nHeight, usColorDepth,
													usRefreshRate, bFullScreen,
													bThreadSafe, bSetCurrentTarget );
	m_pRenderWindow->Active(true);
	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	int nWidth = m_pRenderWindow->GetWidth();
	int nHeight = m_pRenderWindow->GetHeight();
	ushort usColorDepth = m_pRenderWindow->GetColorDepth();

	int nCropWidth = nWidth - nWidth%4;
	int nCropHeight = nHeight - nHeight%4;

	int nTexWidth = nCropWidth/2;
	int nTexHeight = nCropHeight/2;

	CnMultiRenderTarget* sceneMRT = RenderDevice->CreateMultiRenderTarget( L"SceneMRT" );
	if (sceneMRT)
	{
		sceneMRT->Active(true);

		sceneMRT->BindSurface( 0, L"SceneRGB", nWidth, nHeight, usColorDepth, PixelFormat::A8R8G8B8 );
		sceneMRT->BindSurface( 1, L"SceneDepthAlpha", nWidth, nHeight, usColorDepth, PixelFormat::A8R8G8B8 );

		// Setup Scene..
		SetScene( _T("Scene") );

		m_spCamera = CnModelViewCamera::Create();
		m_spCamera->SetName( L"main" );
		m_spCamera->SetPosition( Math::Vector3( 0.0f, 5.0f, -7.0f ) );

		// viewport 1
		CnViewport* pViewport = sceneMRT->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetScene( m_pWorld );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	}

	SetData();

	// Depth
	CnRenderTexture* sceneDepth = RenderDevice->CreateRenderTexture( L"DepthAlpha", nWidth, nHeight, usColorDepth, PixelFormat::A8R8G8B8 );
	if (sceneDepth)
	{
		sceneDepth->Active(true);

		CnScene* newScene = m_pCindy->AddScene( L"DepthAlpha" );

		SceneComPtr sceneUpdator = m_pWorld->GetComponent( L"SceneUpdator" );
		//SceneComPtr modelRenderer = m_pWorld->Lookup( L"RenderModel" );

		Scene::RenderDepth* depthRenderer = Scene::RenderDepth::Create();
		depthRenderer->Link( sceneUpdator );

		depthRenderer->Open();
		depthRenderer->SetShader( L"Depth.fx" );

		//Scene::RenderAABB* aabbRenderer = Scene::RenderAABB::Create();
		//aabbRenderer->Link( sceneUpdator );

		//CnMaterial* mtl = depthRenderer->GetMaterial();
		//mtl->SetShader( L"Depth.fx" );
		//mtl->SetCurrentTechnique( L"RenderStaticDepth" );

		newScene->AddComponent( Scene::BeginScene::Create() );
		newScene->AddComponent( depthRenderer );
		//newScene->AddComponent( aabbRenderer );
		newScene->AddComponent( Scene::EndScene::Create() );

		// viewport 1
		CnViewport* pViewport = sceneDepth->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetScene( newScene );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	}

	SetLight();

	// DownSample And Pre-Blur
	nTexWidth = nCropWidth/4;
	nTexHeight = nCropHeight/4;

	CnRenderTexture* sceneDownSample4x4 = RenderDevice->CreateRenderTexture( L"SceneDownSample", nTexWidth, nTexHeight,
																			   usColorDepth, PixelFormat::A8R8G8B8 );
	if (sceneDownSample4x4)
	{
		sceneDownSample4x4->Active(true);

		CnScene* newScene = m_pCindy->AddScene( L"SceneDownSample" );

		// viewport 1
		CnViewport* pViewport = sceneDownSample4x4->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( newScene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		Scene::RenderScreenQuad* screenQuad = CnNew Scene::RenderScreenQuad();

		newScene->AddComponent( CnNew Scene::BeginScene() );
		newScene->AddComponent( screenQuad );
		newScene->AddComponent( CnNew Scene::EndScene() );

		screenQuad->Setup( nTexWidth, nTexHeight );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"SceneRGB" );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"DownFilter4x4" );

			screenQuad->CalcDown4x4SampleOffsets();

			//mtl->SetCurrentTechnique( L"DownFilter2x2" );

			//Math::Vector2 offsets[16];
			//float tu = 1.0f / nWidth;
			//float tv = 1.0f / nHeight;

			//int index=0;
			//for (int y=0; y<2; ++y)
			//{
			//	for (int x=0; x<2; ++x)
			//	{
			//		offsets[index].x = (x-0.5f) * tu;
			//		offsets[index].y = (y-0.5f) * tv;
			//		index++;
			//	}
			//}
			//screenQuad->SetSampleOffsets( offsets, 16 );
		}
	}

	CnRenderTexture* preBlur = RenderDevice->CreateRenderTexture( L"PreBlur", nTexWidth, nTexHeight,
																					   usColorDepth, PixelFormat::A8R8G8B8 );
	if (preBlur)
	{
		preBlur->Active(true);

		// Scene
		CnScene* newScene = m_pCindy->AddScene( L"PreBlur" );

		Scene::RenderScreenQuad* screenQuad = CnNew Scene::RenderScreenQuad();

		newScene->AddComponent( CnNew Scene::BeginScene() );
		newScene->AddComponent( screenQuad );
		newScene->AddComponent( CnNew Scene::EndScene() );

		// viewport 1
		CnViewport* pViewport = preBlur->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( newScene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		screenQuad->Setup( nTexWidth, nTexHeight );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"SceneDownSample" );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"GaussBlur3x3" );

			Math::Vector4 weights[16];
			Math::Vector2 offsets[16];

			float tu = 1.0f / (float)nWidth;
			float tv = 1.0f / (float)nHeight;

			Math::Vector4 white( 1.0f, 1.0f, 1.0f, 1.0f );
			float totalWeight = 0.0f;
			int index = 0;
			for (int x=-1; x<=1; ++x)
			{
				for (int y=-1; y<=1; ++y)
				{
					// 계수가 작아질 부분은 소거
					if (abs(x) + abs(y) > 2)
						continue;

					offsets[index].x = x * tu;
					offsets[index].y = y * tv;

					weights[index] = white * Math::GaussianDistribution( (float)x, (float)y, 1.0f );

					totalWeight += weights[index].x;
					index++;
				}
			}

			for (int i=0; i<index; ++i)
				weights[i] *= 1.0f/totalWeight;

			screenQuad->SetSampleOffsets( offsets, 16 );
			screenQuad->SetSampleWeights( weights, 16 );
		}
	}

	nTexWidth = nWidth;
	nTexHeight = nHeight;

	CnRenderTexture* possionFilter = RenderDevice->CreateRenderTexture( L"filter", nTexWidth, nTexHeight,
																				  usColorDepth, PixelFormat::A8R8G8B8 );
	if (possionFilter)
	{
		possionFilter->Active(true);

		// Scene
		CnScene* newScene = m_pCindy->AddScene( L"filter" );

		Scene::RenderScreenQuad* screenQuad = CnNew Scene::RenderScreenQuad();

		newScene->AddComponent( CnNew Scene::BeginScene() );
		newScene->AddComponent( screenQuad );
		newScene->AddComponent( CnNew Scene::EndScene() );

		// viewport 1
		CnViewport* pViewport = possionFilter->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( newScene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		screenQuad->Setup( nTexWidth, nTexHeight );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"DepthAlpha" );	// Depth
			mtl->SetTexture( MapType::DiffuseMap1, L"PreBlur" );
			mtl->SetTexture( MapType::DiffuseMap2, L"SceneRGB" );

			mtl->SetShader( L"DepthOfField.fx" );
			mtl->SetCurrentTechnique( L"PossionFilter" );
		}
	}

	// Debug용 Scene
	{
		Scene::RenderScreenQuad* screenQuad = CnNew Scene::RenderScreenQuad();

		screenQuad->Setup( nWidth, nHeight );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();

			//mtl->SetTexture( CnMaterial::DiffuseMap0, L"filter" );
			//mtl->SetShader( L"DepthOfField.fx" );
			//mtl->SetCurrentTechnique( L"Depth" );

			mtl->SetTexture( MapType::DiffuseMap0, L"filter" );
			mtl->SetShader( L"PostEffect.fx" );
			mtl->SetCurrentTechnique( L"RenderScreenQuad" );

			// Scene
			CnScene* mainScene = m_pCindy->AddScene( L"mainScene" );

			mainScene->AddComponent( CnNew Scene::BeginScene() );
			mainScene->AddComponent( screenQuad );
			mainScene->AddComponent( CnNew Scene::EndScene() );

			CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
			pViewport->SetScene( mainScene );
			pViewport->SetBackgroundColor( CN_RGBA( 0, 100, 0, 0 ) );
		}
	}

	//SetData();
	return true;
}

void CApplication::SetLight()
{
	// Directional Light
	CnLight* light = CnNew CnLight( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	Math::Vector3 direction( 0.0f, 1.0f, 1.0f );
	direction.Normalize();
	light->SetDirection( direction );

	light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 0.8f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

	Math::Vector3 position( 10.0f, 10.0f, -5.0f );
	CnSceneNode* lightNode = CnNew CnSceneNode( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );
	lightNode->EnableCulling( false );

	m_pSceneGraph->AttachChild( lightNode );
}

void CApplication::SetData()
{
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	CnString meshName = L"newAtoom.mmf";		// atoom_nrm_spec.mmf
	CnString mtlName = L"newAtoom_mtl.xml";	// atoom_nrm_spec_mtl.xml

	CnModelNode* modelnode = NULL;
	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	modelnode->AddMesh( meshName, mtlName );

	modelnode->Yaw( Math::DegreeToRadian(20.0f) );

	m_pSceneGraph->AttachChild( modelnode );

	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model2" );
	{
		SkelDataPtr skeleton = m_pSkeletonDataMgr->Load( L"bango_rush_m.mbf" );
		modelnode->AddSkeleton( skeleton );
		modelnode->AddMesh( L"newExpAll.mmf", L"newExpAll2_mtl.xml" );
	}
	modelnode->SetPosition( Math::Vector3( 0.0f, 0.0f, -10.0f ) );
	m_pSceneGraph->AttachChild( modelnode );

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\Ground\\" );

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );

	m_pSceneGraph->GetRoot()->AttachChild( modelnode );
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}