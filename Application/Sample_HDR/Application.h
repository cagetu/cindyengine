#pragma once

#include <Cindy/Application/CnApplication.h>
#include <Cindy/Geometry/CnMeshDataManager.h>
#include <Cindy/Material/CnMaterialDataManager.h>
#include <Cindy/Animation/CnSkeletonDataManager.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Graphics/Base/CnRenderTexture.h>

#include <Cindy/Scene/CnCameraImpl.h>
#include <Cindy/Util/CnMouse.h>

class CApplication : public CnApplication
{
public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*		AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
									  int nWidth, int nHeight, ushort usColorDepth,
									  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
									  bool bSetCurrentTarget = false ) override;

	bool		Initialize() override;

	void		SetData();

	// MsgProc
	virtual bool	MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) override;

protected:
	virtual void	OnProcessInput() override;

private:
	struct CoordRect
	{
		float u0, v0;
		float u1, v1;
	};

	//----------------------------------------------------------------
	// Variables
	//----------------------------------------------------------------
	MdvCamPtr			m_pCamera;

	CnMouse				m_Mouse;
	short				m_nRotX;
	short				m_nRotY;

	CnMeshDataManager*		m_pMeshDataGroup;
	CnMaterialDataManager*	m_pMaterialDataGroup;
	CnSkeletonDataManager*	m_pSkeletonDataMgr;

	CnRenderWindow*			m_pRenderWindow;

	//----------------------------------------------------------------
	// Methods
	//----------------------------------------------------------------
	void	GetTextureCoords( const TexPtr& spTexSrc, RECT* pRectSrc,
							  const TexPtr& spTexDest, RECT* pRectDest,
							  CoordRect* pResult );

	// Offset, Weights ���ϱ�
	void	GetDown2x2SampleOffsets( int nWidth, int nHeight, Math::Vector2* paSampleOffsets );
	void	GetDown4x4SampleOffsets( int nWidth, int nHeight, Math::Vector2* paSampleOffsets );
	void	GetGaussBlur5x5SampleOffsets( int nWidth, int nHeight, Math::Vector2* paSampleOffsets, Math::Vector4* paSampleWeights );
	void	GetBloomSampleOffsets( int nTexSize, float* paSampleOffsets, Math::Vector4* paSampleWeights,
									float fDeviation, float fMultiplier );

	//
	CnRenderTexture*	SetRenderScene( int nWidth, int nHeight, ushort usColorDepth );
	CnRenderTexture*	SetDownSample4x4( int nWidth, int nHeight, int nBackWidth, int nBackHeight, 
										  ushort usColorDepth, CnRenderTexture* pSrcTexture );

	CnRenderTexture*	SetTonemap( ushort usColorDepth, CnRenderTexture* pSrcTexture );

	CnRenderTexture*	SetBloomEffect( int nWidth, int nHeight, ushort usColorDepth, CnRenderTexture* pSrcTexture );
	CnRenderTexture*	SetStarEffect( int nWidth, int nHeight, ushort usColorDepth, CnRenderTexture* pStarSource);
};