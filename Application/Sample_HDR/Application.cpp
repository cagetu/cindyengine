#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Lighting/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

#include <Cindy/Graphics/Base/CnRenderWindow.h>
#include <Cindy/Frame/CnPostEffectSystem.h>

CApplication::CApplication()
{
	m_pMeshDataGroup = CnMeshDataManager::Create();
	m_pMaterialDataGroup = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
}

//----------------------------------------------------------------
CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	CnViewport* pViewport = NULL;

	m_pRenderWindow = CnApplication::AttachWindow( hWnd, pStrTitle,
											   nWidth, nHeight, usColorDepth,
											   usRefreshRate, bFullScreen,
											   bThreadSafe, bSetCurrentTarget );

	m_pRenderWindow->Active( true );
	return m_pRenderWindow;
}

//----------------------------------------------------------------
/**	@brief	초기화
*/
bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	int nWidth = m_pRenderWindow->GetWidth();
	int nHeight = m_pRenderWindow->GetHeight();
	ushort usColorDepth = m_pRenderWindow->GetColorDepth();

	CnRenderTexture* sceneTexture = SetRenderScene( nWidth, nHeight, usColorDepth );

	// 데이터 설정
	SetData();

	//PostEffect::EffectSystem::Instance()->LoadEffect( L"..\\..\\sdk\\PostEffect\\HDR.xml" );

	// 4의 배수가 아닌 녀석은 4의 배수로 맞춘다.
	int nCropWidth = nWidth - nWidth%4;
	int nCropHeight = nHeight - nHeight%4;

	// 1/4로 DownSampling
	CnRenderTexture* downSampleTexture = SetDownSample4x4( nCropWidth/4, nCropHeight/4, nWidth, nHeight, usColorDepth, sceneTexture );
	downSampleTexture->Active( true );

	// tonemap
	CnRenderTexture* toneMap = SetTonemap( usColorDepth, downSampleTexture );
	toneMap->Active( true );

	// 실제로 밝은 부분을 뽑아낸다.
	CnRenderTexture* brightMap = RenderDevice->CreateRenderTexture( L"BrightPassRT", nCropWidth/4 + 2, nCropHeight/4 + 2,
																	usColorDepth, PixelFormat::A8R8G8B8 );
	if (brightMap)
	{
		brightMap->Active( true );

		CnScene* brightScene = m_pCindy->AddScene( L"brightPass" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();

		brightScene->AddComponent( Scene::BeginScene::Create() );
		brightScene->AddComponent( screenQuad );
		brightScene->AddComponent( Scene::EndScene::Create() );

		CnViewport* pViewport = brightMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( brightScene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		//
		RECT rectSrc;
		rectSrc.left = 0;
		rectSrc.top = 0;
		rectSrc.right = downSampleTexture->GetWidth();
		rectSrc.bottom = downSampleTexture->GetHeight();
		InflateRect( &rectSrc, -1, -1 );

		RECT rectDest;
		rectDest.left = 0;
		rectDest.top = 0;
		rectDest.right = brightMap->GetWidth();
		rectDest.bottom = brightMap->GetHeight();
		InflateRect( &rectDest, -1, -1 );

		CoordRect coords;
		GetTextureCoords( downSampleTexture->GetTexture(), &rectSrc, brightMap->GetTexture(), &rectDest, &coords );

		//
		screenQuad->Setup( nCropWidth/4 + 2, nCropHeight/4 + 2, coords.u0, coords.v0, coords.u1, coords.v1 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();

			CnRenderState* renderState = mtl->GetRenderState();
			renderState->AddFlags( CnRenderState::_SCISSORTEST );
			renderState->SetScissorTestRect( rectDest );

			mtl->SetTexture( MapType::DiffuseMap0, L"DownSampleRenderTarget" );
			//mtl->SetTexture( MapType::DiffuseMap1, toneMap->GetName() );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"BrightPassFilter" );
		}
	}

	// Star Effect Source
	CnRenderTexture* starMap = RenderDevice->CreateRenderTexture( L"StarEffectRT", nCropWidth/4 + 2, nCropHeight/4 + 2,
																			usColorDepth, PixelFormat::A8R8G8B8 );
	if (starMap)
	{
		starMap->Active( true );

		CnScene* scene = m_pCindy->AddScene( L"starEffect" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();

		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		CnViewport* pViewport = starMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( scene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		RECT rectDest;
		rectDest.left = 0;
		rectDest.top = 0;
		rectDest.right = starMap->GetWidth();
		rectDest.bottom = starMap->GetHeight();
		InflateRect( &rectDest, -1, -1 );

		CoordRect coords;
		GetTextureCoords( brightMap->GetTexture(), NULL, starMap->GetTexture(), &rectDest, &coords );

		screenQuad->Setup( nCropWidth/4 + 2, nCropHeight/4 + 2, coords.u0, coords.v0, coords.u1, coords.v1 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();

			CnRenderState* renderState = mtl->GetRenderState();
			renderState->AddFlags( CnRenderState::_SCISSORTEST );
			renderState->SetScissorTestRect( rectDest );

			//mtl->SetTexture( CnMaterial::DiffuseMap0, L"toneMapResampe1x1nExpRT" );
			mtl->SetTexture( MapType::DiffuseMap0, L"BrightPassRT" );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			stageState.address.u = CnTextureState::TADDRESS_CLAMP;
			stageState.address.v = CnTextureState::TADDRESS_CLAMP;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"GaussBlur5x5" );

			Math::Vector2 offsets[16];
			Math::Vector4 weights[16];
			int width = (nCropWidth/4 + 2);
			int height = (nCropHeight/4 + 2);

			GetGaussBlur5x5SampleOffsets( width, height, offsets, weights );

			screenQuad->SetSampleOffsets( offsets, 16 );
			screenQuad->SetSampleWeights( weights, 16 );
		}
	}

	CnRenderTexture* bloomMap = RenderDevice->CreateRenderTexture( L"BloomRT", nCropWidth/8 + 2, nCropHeight/8 + 2,
																				usColorDepth, PixelFormat::A8R8G8B8 );
	if (bloomMap)
	{
		bloomMap->Active( true );

		CnScene* scene = m_pCindy->AddScene( L"bloomMap" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();

		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		CnViewport* pViewport = bloomMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( scene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		//
		RECT rectSrc;
		rectSrc.left = 0;
		rectSrc.top = 0;
		rectSrc.right = starMap->GetWidth();
		rectSrc.bottom = starMap->GetHeight();
		InflateRect( &rectSrc, -1, -1 );

		RECT rectDest;
		rectDest.left = 0;
		rectDest.top = 0;
		rectDest.right = bloomMap->GetWidth();
		rectDest.bottom = bloomMap->GetHeight();
		InflateRect( &rectDest, -1, -1 );

		CoordRect coords;
		GetTextureCoords( starMap->GetTexture(), &rectSrc, bloomMap->GetTexture(), &rectDest, &coords );

		screenQuad->Setup( nCropWidth/8 + 2, nCropHeight/8 + 2, coords.u0, coords.v0, coords.u1, coords.v1 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();

			CnRenderState* renderState = mtl->GetRenderState();
			renderState->AddFlags( CnRenderState::_SCISSORTEST );
			renderState->SetScissorTestRect( rectDest );

			//mtl->SetTexture( MapType::DiffuseMap0, L"toneMapResampe1x1nExpRT" );
			mtl->SetTexture( MapType::DiffuseMap0, L"StarEffectRT" );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			stageState.address.u = CnTextureState::TADDRESS_CLAMP;
			stageState.address.v = CnTextureState::TADDRESS_CLAMP;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"DownFilter2x2" );

			Math::Vector2 offsets[4];
			GetDown2x2SampleOffsets( brightMap->GetWidth(), brightMap->GetHeight(), offsets );

			screenQuad->SetSampleOffsets( offsets, 4 );
		}
	}

	CnRenderTexture* starTexture = SetStarEffect( nCropWidth/4, nCropHeight/4, usColorDepth, starMap );
	CnRenderTexture* bloomTexture = SetBloomEffect( nCropWidth/8 + 2, nCropHeight/8 + 2, usColorDepth, bloomMap );

	starTexture->Active( true );
	bloomTexture->Active( true );

	// Debug용 Scene
	{
		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		screenQuad->Setup( nWidth, nHeight );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();

			mtl->SetTexture( MapType::DiffuseMap0, sceneTexture->GetName() );
			mtl->SetTexture( MapType::DiffuseMap1, starTexture->GetName() );
			mtl->SetTexture( MapType::DiffuseMap2, bloomTexture->GetName() );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			//stageState.address.u = CnTextureState::TADDRESS_CLAMP;
			//stageState.address.v = CnTextureState::TADDRESS_CLAMP;
			textureState->SetStage( 0, stageState );

			stageState.filter.mag = CnTextureState::TEXF_LINEAR;
			stageState.filter.min = CnTextureState::TEXF_LINEAR;
			stageState.filter.mip = CnTextureState::TEXF_LINEAR;
			//stageState.address.u = CnTextureState::TADDRESS_CLAMP;
			//stageState.address.v = CnTextureState::TADDRESS_CLAMP;
			textureState->SetStage( 1, stageState );
			textureState->SetStage( 2, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"FinalScene" );

			CnScene* mainScene = m_pCindy->AddScene( L"DebugCamera" );

			mainScene->AddComponent( Scene::BeginScene::Create() );		
			mainScene->AddComponent( screenQuad );
			mainScene->AddComponent( Scene::EndScene::Create() );

			CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
			pViewport->SetScene( mainScene );
			pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
		}
	}

	return true;
}

//----------------------------------------------------------------
void CApplication::GetTextureCoords( const TexPtr& spTexSrc, RECT* pRectSrc,
									 const TexPtr& spTexDest, RECT* pRectDest,
									 CoordRect* pResult )
{
	pResult->u0 = 0.0f;
	pResult->v0 = 0.0f;
	pResult->u1 = 1.0f;
	pResult->v1 = 1.0f;

	// 입력 원본의 표면에 관한 보정
	if (NULL != pRectSrc)
	{
		pResult->u0 += pRectSrc->left / spTexSrc->GetWidth();
		pResult->v0 += pRectSrc->top / spTexSrc->GetHeight();
		pResult->u1 -= (spTexSrc->GetWidth() - pRectSrc->right) / spTexSrc->GetWidth();
		pResult->v1 -= (spTexSrc->GetHeight() - pRectSrc->bottom) / spTexSrc->GetHeight();
	}

	// 출력할 표면에 관한 보정
	if (NULL != pRectDest)
	{
		pResult->u0 -= pRectDest->left / spTexDest->GetWidth();
		pResult->v0 -= pRectDest->top / spTexDest->GetHeight();
		pResult->u1 += (spTexDest->GetWidth() - pRectDest->right) / spTexDest->GetWidth();
		pResult->v1 += (spTexDest->GetHeight() - pRectDest->bottom) / spTexDest->GetHeight();
	}
}

//----------------------------------------------------------------
/**	@brief	RenderScene 설정
*/
CnRenderTexture* CApplication::SetRenderScene( int nWidth, int nHeight, ushort usColorDepth )
{
	// RenderTarget
	CnRenderTexture* sceneTexture = RenderDevice->CreateRenderTexture( L"SceneRenderTarget", nWidth, nHeight,
																				  usColorDepth, PixelFormat::A16B16G16R16F );
	if (sceneTexture)
	{
		sceneTexture->Active( true );

		SetScene( L"Scene" );

		m_pCamera = CnModelViewCamera::Create();
		m_pCamera->SetName( L"main" );
		m_pCamera->SetPosition( Math::Vector3( 0.0f, 5.0f, -50.0f ) );
		m_pCamera->SetFar( 1000.0f );
		m_pCamera->SetRadius( 1.0f, 500.0f );
		m_pCamera->SetRadius( 20.0f );
		m_pCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );

		// viewport
		CnViewport* pViewport = sceneTexture->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetCamera( m_pCamera.Cast<CnCamera>() );
		pViewport->SetScene( m_pWorld );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );

		CnLight* light = CnLight::Create();
		light->SetName( L"dirLight0" );
		light->SetType( CnLight::Directional );

		Math::Vector3 position( 0.0f, 10.0f, -10.0f );
		Math::Vector3 direction( 0.3f, 0.4f, 1.0f );
		direction.Normalize();
		light->SetDirection( direction );

		light->SetDiffuse( CnColor( 0.8f, 0.8f, 0.8f, 0.8f ) );
		light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
		light->SetAmbient( CnColor( 0.0f, 0.0f, 0.0f, 0.0f ) );

		CnSceneNode* lightNode = CnSceneNode::Create();
		lightNode->SetName( L"dirLight0" );
		lightNode->SetPosition( position );
		lightNode->AttachEntity( light );

		m_pSceneGraph->AttachChild( lightNode );
	}

	return sceneTexture;
}

//----------------------------------------------------------------
/**	@brief	Downsample 1/4
*/
CnRenderTexture* CApplication::SetDownSample4x4( int nWidth, int nHeight, int nBackWidth, int nBackHeight, 
												 ushort usColorDepth, CnRenderTexture* pSrcTexture  )
{
	// 1/4로 DownSampling
	CnRenderTexture* downSampleTexture = RenderDevice->CreateRenderTexture( L"DownSampleRenderTarget", nWidth, nHeight,
																					  usColorDepth, PixelFormat::A16B16G16R16F );
	if (downSampleTexture)
	{
		// Scene
		CnScene* scene = m_pCindy->AddScene( L"downSampleScene" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		{
			RECT rectSrc;
			rectSrc.left = (nBackWidth - nWidth) / 2;
			rectSrc.top = (nBackHeight - nHeight) / 2;
			rectSrc.right = rectSrc.left + nWidth;
			rectSrc.bottom = rectSrc.top + nHeight;

			CoordRect coords;
			GetTextureCoords( pSrcTexture->GetTexture(), &rectSrc, downSampleTexture->GetTexture(), NULL, &coords );

			screenQuad->Setup( nWidth, nHeight, coords.u0, coords.v0, coords.u1, coords.v1 );

			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"SceneRenderTarget" );

			//CnRenderState* renderState = mtl->GetRenderState();
			//renderState->SetAlphaTestEnable( false );
			//renderState->SetLightEnable( false );
			//renderState->SetFogEnable( false );
			//renderState->SetZBufferEnable( true );
			//renderState->SetCullMode( CnRenderState::CULL_CCW );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"DownFilter4x4" );

			Math::Vector2 offsets[16];
			GetDown4x4SampleOffsets( downSampleTexture->GetWidth(), downSampleTexture->GetHeight(), offsets );
			
			screenQuad->SetSampleOffsets( offsets, 16 );
		}

		// Camera
		CnViewport* pViewport = downSampleTexture->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( scene );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	}

	return downSampleTexture;
}

//----------------------------------------------------------------
/**	@brief	Tonemap 설정
*/
CnRenderTexture* CApplication::SetTonemap( ushort usColorDepth, CnRenderTexture* pSrcTexture )
{
	CnViewport* pViewport = NULL;

	float tu = 1.0f / (3.0f * 64);
	float tv = 1.0f / (3.0f * 64);

	int index = 0;
	Math::Vector2 offsets[16];
	for (int y=-1; y<=1; ++y)
	{
		for (int x=-1; x<=1; ++x)
		{
			offsets[index].x = x * tu;
			offsets[index].y = y * tv;
			index++;
		}
	}

	// 밝은 부분을 뽑아낸다.
	// toneMap은 64x64 -> 32x32 -> 16x16 -> 4x4 로 4번 sampling 한다.

	CnRenderTexture* toneMap = RenderDevice->CreateRenderTexture( L"toneMapAvgRT", 64, 64,
																			usColorDepth, PixelFormat::R16F );
	if (toneMap)
	{
		CnScene* scene = m_pCindy->AddScene( L"toneMapAvgRT" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		pViewport = toneMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );
		pViewport->SetScene( scene );

		screenQuad->Setup( 64, 64 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, pSrcTexture->GetName() );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_LINEAR;
			stageState.filter.min = CnTextureState::TEXF_LINEAR;
			stageState.filter.mip = CnTextureState::TEXF_LINEAR;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"LuminanceSampleAvg" );

			screenQuad->SetSampleOffsets( offsets, 16 );
		}
	}

	toneMap = RenderDevice->CreateRenderTexture( L"toneMapResample32x32RT", 32, 32,
														   usColorDepth, PixelFormat::R16F );
	if (toneMap)
	{
		CnScene* scene = m_pCindy->AddScene( L"toneMapResample32x32" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		pViewport = toneMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );
		pViewport->SetScene( scene );

		screenQuad->Setup( 32, 32 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"toneMapAvgRT" );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"LuminanceResampleAvg" );
			
			screenQuad->SetSampleOffsets( offsets, 16 );
		}
	}

	toneMap = RenderDevice->CreateRenderTexture( L"toneMapResample16x16RT", 16, 16,
														   usColorDepth, PixelFormat::R16F );
	if (toneMap)
	{
		CnScene* scene = m_pCindy->AddScene( L"toneMapResample16x16" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		pViewport = toneMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );
		pViewport->SetScene( scene );

		screenQuad->Setup( 16, 16 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"toneMapResample32x32RT" );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"LuminanceResampleAvg" );
			
			screenQuad->SetSampleOffsets( offsets, 16 );
		}
	}

	toneMap = RenderDevice->CreateRenderTexture( L"toneMapResample4x4RT", 4, 4,
														   usColorDepth, PixelFormat::R16F );
	if (toneMap)
	{
		CnScene* scene = m_pCindy->AddScene( L"toneMapResample4x4" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		pViewport = toneMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );
		pViewport->SetScene( scene );

		screenQuad->Setup( 4, 4 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"toneMapResample16x16RT" );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"LuminanceResampleAvg" );
			
			screenQuad->SetSampleOffsets( offsets, 16 );
		}
	}

	toneMap = RenderDevice->CreateRenderTexture( L"toneMapResampe1x1nExpRT", 1, 1,
														   usColorDepth, PixelFormat::R16F );
	if (toneMap)
	{
		CnScene* scene = m_pCindy->AddScene( L"toneMapResampe1x1nExp" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();
		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		pViewport = toneMap->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );
		pViewport->SetScene( scene );

		screenQuad->Setup( 1, 1 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"toneMapResample4x4RT" );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"LuminanceResampleAvgExp" );

			screenQuad->SetSampleOffsets( offsets, 16 );
		}
	}

	return toneMap;
}

//----------------------------------------------------------------
/**	@brief	Bloom Effect
*/
CnRenderTexture* CApplication::SetBloomEffect( int nWidth, int nHeight, ushort usColorDepth, CnRenderTexture* pSrcTexture )
{
	// nCropWidth/8 + 2
	CnRenderTexture* bloomTexture = RenderDevice->CreateRenderTexture( L"BloomRT0", nWidth, nHeight,
																				 usColorDepth, PixelFormat::A8R8G8B8 );
	if (!bloomTexture)
		return NULL;

	// Scene
	CnScene* scene = m_pCindy->AddScene( L"BloomRT0" );

	Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();

	scene->AddComponent( Scene::BeginScene::Create() );
	scene->AddComponent( screenQuad );
	scene->AddComponent( Scene::EndScene::Create() );

	CnViewport* pViewport = bloomTexture->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetScene( scene );
	pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

	RECT rectSrc;
	rectSrc.left = 0;
	rectSrc.top = 0;
	rectSrc.right = pSrcTexture->GetWidth();
	rectSrc.bottom = pSrcTexture->GetHeight();
	InflateRect( &rectSrc, -1, -1 );

	RECT rectDest;
	rectDest.left = 0;
	rectDest.top = 0;
	rectDest.right = bloomTexture->GetWidth();
	rectDest.bottom = bloomTexture->GetHeight();
	InflateRect( &rectDest, -1, -1 );

	CoordRect coords;
	GetTextureCoords( pSrcTexture->GetTexture(), &rectSrc, bloomTexture->GetTexture(), &rectDest, &coords );

	screenQuad->Setup( nWidth, nHeight, coords.u0, coords.v0, coords.u1, coords.v1 );
	{
		CnMaterial* mtl = screenQuad->GetMaterial();
		mtl->SetTexture( MapType::DiffuseMap0, pSrcTexture->GetName() );

		CnRenderState* renderState = mtl->GetRenderState();
		renderState->AddFlags( CnRenderState::_SCISSORTEST );
		renderState->SetScissorTestRect( rectDest );

		CnTextureState* textureState = mtl->GetTextureState();
		CnTextureState::StageState stageState;
		stageState.filter.mag = CnTextureState::TEXF_POINT;
		stageState.filter.min = CnTextureState::TEXF_POINT;
		stageState.filter.mip = CnTextureState::TEXF_POINT;
		textureState->SetStage( 0, stageState );

		mtl->SetShader( L"Hdr.fx" );
		mtl->SetCurrentTechnique( L"GaussBlur5x5" );

		Math::Vector2 offsets[16];
		Math::Vector4 weights[16];
		GetGaussBlur5x5SampleOffsets( nWidth, nHeight, offsets, weights );

		screenQuad->SetSampleOffsets( offsets, 16 );
		screenQuad->SetSampleWeights( weights, 16 );
	}

	// 수평으로 전개
	CnRenderTexture* bloomWorkV = RenderDevice->CreateRenderTexture( L"BloomWorkRT_V", nWidth, nHeight,
																				usColorDepth, PixelFormat::A8R8G8B8 );
	if (bloomWorkV)
	{
		// Scene
		CnScene* scene = m_pCindy->AddScene( L"BloomWorkRT_V" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();

		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		CnViewport* pViewport = bloomWorkV->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( scene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		screenQuad->Setup( nWidth, nHeight, coords.u0, coords.v0, coords.u1, coords.v1 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, bloomTexture->GetName() );

			CnRenderState* renderState = mtl->GetRenderState();
			renderState->AddFlags( CnRenderState::_SCISSORTEST );
			renderState->SetScissorTestRect( rectDest );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"Bloom_Horizontal" );

			float bloomSampleOffsets[16];

			Math::Vector2 offsets[16];
			Math::Vector4 weights[16];
			GetBloomSampleOffsets( nWidth, bloomSampleOffsets, weights, 3.0f, 2.0f );

			for (int i=0; i < 16; ++i)
			{
				offsets[i].x = bloomSampleOffsets[i];
				offsets[i].y = 0.0f;
			}

			screenQuad->SetSampleOffsets( offsets, 16 );
			screenQuad->SetSampleWeights( weights, 16 );
		}
	}

	// 수직으로 전개
	CnRenderTexture* bloomWorkH = RenderDevice->CreateRenderTexture( L"BloomWorkRT_H", nWidth-2, nHeight-2,
																				usColorDepth, PixelFormat::A8R8G8B8 );
	if (bloomWorkH)
	{
		bloomWorkH->ColorFill( CnColor::BLACK );

		// Scene
		CnScene* scene = m_pCindy->AddScene( L"BloomWorkRT_H" );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();

		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		CnViewport* pViewport = bloomWorkH->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( scene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		rectSrc.left = 0;
		rectSrc.top = 0;
		rectSrc.right = bloomWorkV->GetWidth();
		rectSrc.bottom = bloomWorkV->GetHeight();
		InflateRect( &rectSrc, -1, -1 );

		GetTextureCoords( bloomWorkV->GetTexture(), &rectSrc, bloomWorkH->GetTexture(), NULL, &coords );

		screenQuad->Setup( nWidth-2, nHeight-2, coords.u0, coords.v0, coords.u1, coords.v1 );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, bloomWorkV->GetName() );

			CnRenderState* renderState = mtl->GetRenderState();
			renderState->AddFlags( CnRenderState::_SCISSORTEST );
			renderState->SetScissorTestRect( rectDest );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_POINT;
			stageState.filter.min = CnTextureState::TEXF_POINT;
			stageState.filter.mip = CnTextureState::TEXF_POINT;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"Bloom_Vertical" );

			float bloomSampleOffsets[16];

			Math::Vector2 offsets[16];
			Math::Vector4 weights[16];
			GetBloomSampleOffsets( nHeight, bloomSampleOffsets, weights, 3.0f, 2.0f );

			for (int i=0; i < 16; ++i)
			{
				offsets[i].x = 0.0f;
				offsets[i].y = bloomSampleOffsets[i];
			}

			screenQuad->SetSampleOffsets( offsets, 16 );
			screenQuad->SetSampleWeights( weights, 16 );
		}
	}

	return bloomWorkH;
}

//----------------------------------------------------------------
/**	@brief	StartEffect 설정
*/
CnRenderTexture* CApplication::SetStarEffect( int nWidth, int nHeight, ushort usColorDepth, CnRenderTexture* pStarSource )
{
	static const int s_maxPasses = 3;
	static const int s_nSamples = 8;
	static const CnColor s_white( 0.63f, 0.63f, 0.63f, 0.0f );
	static const CnColor s_chromaticAberrationColor[8] = 
	{
		CnColor( 0.5f, 0.5f, 0.5f, 0.0f ),	// 백색
		CnColor( 0.8f, 0.3f, 0.3f, 0.0f ),	// 적색
		CnColor( 1.0f, 0.2f, 0.2f, 0.0f ),	// 적색
		CnColor( 0.5f, 0.2f, 0.6f, 0.0f ),	// 자주색
		CnColor( 0.2f, 0.2f, 1.0f, 0.0f ),	// 청색
		CnColor( 0.2f, 0.3f, 0.7f, 0.0f ),	// 청색
		CnColor( 0.2f, 0.6f, 0.2f, 0.0f ),	// 녹색
		CnColor( 0.3f, 0.5f, 0.3f, 0.0f ),	// 녹색
	};

	static CnColor s_aaColor[s_maxPasses][s_nSamples];

	for (int p=0; p < s_maxPasses; ++p)
	{
		// 중심의 거리에 따른 광선 색을 만든다.
		float ratio = (float)(p+1) / (float)s_maxPasses;

		// 각각의 샘플링으로 적당한 색을 만든다.
		for (int s=0; s < s_nSamples; ++s)
		{
			CnColor chromaticAberrColor;
			chromaticAberrColor.Lerp( s_chromaticAberrationColor[s], s_white, ratio );

			// 전체적인 색의 변화를 조정
			s_aaColor[p][s].Lerp( s_white, chromaticAberrColor, 0.7f );
		}
	}

	// RenderTargets...
	CnRenderTexture* starDir[6];
	starDir[0] = RenderDevice->CreateRenderTexture( L"HDRStarDir0", nWidth, nHeight,
															  usColorDepth, PixelFormat::A16B16G16R16F );
	starDir[1] = RenderDevice->CreateRenderTexture( L"HDRStarDir1", nWidth, nHeight,
															  usColorDepth, PixelFormat::A16B16G16R16F );
	starDir[2] = RenderDevice->CreateRenderTexture( L"HDRStarDir2", nWidth, nHeight,
															  usColorDepth, PixelFormat::A16B16G16R16F );
	starDir[3] = RenderDevice->CreateRenderTexture( L"HDRStarDir3", nWidth, nHeight,
															  usColorDepth, PixelFormat::A16B16G16R16F );
	starDir[4] = RenderDevice->CreateRenderTexture( L"HDRStarDir4", nWidth, nHeight,
															  usColorDepth, PixelFormat::A16B16G16R16F );
	starDir[5] = RenderDevice->CreateRenderTexture( L"HDRStarDir5", nWidth, nHeight,
															  usColorDepth, PixelFormat::A16B16G16R16F );

	float radOffset = 0.3f;

	float srcWidth = (float)pStarSource->GetWidth();
	float srcHeight = (float)pStarSource->GetHeight();

	const float fTanFoV = atanf(Math::PI/4);

	int starLines = 6;
	for (int d=0; d < starLines; ++d)
	{
		float rad = radOffset + 2*d*Math::PI/(float)starLines; // 각도
		float sn = sinf(rad);
		float cn = cosf(rad);

		Math::Vector2 stepUV;
		stepUV.x = 0.3f*sn / srcWidth;
		stepUV.y = 0.3f*cn / srcHeight;

		float attnPowScale = (fTanFoV+0.1f) * 1.0f * (160.0f + 120.0f) / (srcWidth + srcHeight);

		CnColor weights[16];
		Math::Vector2 offsets[16];

		Scene::RenderScreenQuad* starWorkQuad[3];
		starWorkQuad[0] = Scene::RenderScreenQuad::Create();
		starWorkQuad[1] = Scene::RenderScreenQuad::Create();
		starWorkQuad[2] = Scene::RenderScreenQuad::Create();

		for (int p=0; p<s_maxPasses; ++p)
		{
			for (int i=0; i < s_nSamples; ++i)
			{
				// 가중치 계산
				float lum = powf( 0.95f, attnPowScale*i );

				int colorID = s_maxPasses-1-p;
				weights[i] = s_aaColor[colorID][i] * lum * (p+1.0f) * 0.5f;

				// 샘플링 uv 좌표
				offsets[i].x = stepUV.x * i;
				offsets[i].y = stepUV.y * i;

				if ( fabs(offsets[i].x) >= 0.9f ||
					 fabs(offsets[i].y) >= 0.9f )
				{
					offsets[i].x = 0.0f;
					offsets[i].y = 0.0f;
					weights[i] *= 0.0f;
				}
			}

			// 여기서 랜더링 한다면...
			// 한 방향의 빛 퍼짐을 3번의 랜더링으로 만들어서, 6방향의 텍스쳐를 만들어 낸다.
			starWorkQuad[p]->SetSampleOffsets( offsets, s_nSamples );
			starWorkQuad[p]->SetSampleWeights( (Math::Vector4*)weights, s_nSamples );

			// 다음 패스를 위한 인수 설정
			stepUV *= s_nSamples;
			attnPowScale *= s_nSamples;
		}

		wchar name[256];
		swprintf_s( name, 256, L"dir%d_HDRStarWork0", d );
		CnRenderTexture* starWork[2];
		starWork[0] = RenderDevice->CreateRenderTexture( name, nWidth, nHeight,
	  																usColorDepth, PixelFormat::A16B16G16R16F );

		starWork[0]->ColorFill( CnColor::BLACK );

		if (starWork[0])
		{
			// Scene
			CnScene* scene = m_pCindy->AddScene( name );

			scene->AddComponent( Scene::BeginScene::Create() );
			scene->AddComponent( starWorkQuad[0] );
			scene->AddComponent( Scene::EndScene::Create() );

			CnViewport* pViewport = starWork[0]->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
			pViewport->SetScene( scene );
			pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

			starWorkQuad[0]->Setup( nWidth, nHeight );
			{
				CnMaterial* mtl = starWorkQuad[0]->GetMaterial();
				mtl->SetTexture( MapType::DiffuseMap0, pStarSource->GetName() );

				CnTextureState* textureState = mtl->GetTextureState();
				CnTextureState::StageState stageState;
				stageState.filter.mag = CnTextureState::TEXF_LINEAR;
				stageState.filter.min = CnTextureState::TEXF_LINEAR;
				stageState.filter.mip = CnTextureState::TEXF_LINEAR;
				textureState->SetStage( 0, stageState );

				mtl->SetShader( L"Hdr.fx" );
				mtl->SetCurrentTechnique( L"StarEffect" );
			}
		}

		swprintf_s( name, 256, L"dir%d_HDRStarWork1", d );
		starWork[1] = RenderDevice->CreateRenderTexture( name, nWidth, nHeight,
														 usColorDepth, PixelFormat::A16B16G16R16F );

		if (starWork[1])
		{
			// Scene
			CnScene* scene = m_pCindy->AddScene( name );

			scene->AddComponent( Scene::BeginScene::Create() );
			scene->AddComponent( starWorkQuad[1] );
			scene->AddComponent( Scene::EndScene::Create() );

			CnViewport* pViewport = starWork[1]->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
			pViewport->SetScene( scene );
			pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

			starWorkQuad[1]->Setup( nWidth, nHeight );
			{
				CnMaterial* mtl = starWorkQuad[1]->GetMaterial();
				mtl->SetTexture( MapType::DiffuseMap0, starWork[0]->GetName() );

				CnTextureState* textureState = mtl->GetTextureState();
				CnTextureState::StageState stageState;
				stageState.filter.mag = CnTextureState::TEXF_LINEAR;
				stageState.filter.min = CnTextureState::TEXF_LINEAR;
				stageState.filter.mip = CnTextureState::TEXF_LINEAR;
				textureState->SetStage( 0, stageState );

				mtl->SetShader( L"Hdr.fx" );
				mtl->SetCurrentTechnique( L"StarEffect" );
			}
		}

		if (starDir[d])
		{
			CnString name = starDir[d]->GetName();

			// Scene
			CnScene* scene = m_pCindy->AddScene( name.c_str() );

			scene->AddComponent( Scene::BeginScene::Create() );
			scene->AddComponent( starWorkQuad[2] );
			scene->AddComponent( Scene::EndScene::Create() );

			CnViewport* pViewport = starDir[d]->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
			pViewport->SetScene( scene );
			pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

			starWorkQuad[2]->Setup( nWidth, nHeight );
			{
				CnMaterial* mtl = starWorkQuad[2]->GetMaterial();
				mtl->SetTexture( MapType::DiffuseMap0, starWork[1]->GetName() );

				CnTextureState* textureState = mtl->GetTextureState();
				CnTextureState::StageState stageState;
				stageState.filter.mag = CnTextureState::TEXF_LINEAR;
				stageState.filter.min = CnTextureState::TEXF_LINEAR;
				stageState.filter.mip = CnTextureState::TEXF_LINEAR;
				textureState->SetStage( 0, stageState );

				mtl->SetShader( L"Hdr.fx" );
				mtl->SetCurrentTechnique( L"StarEffect" );
			}
		}
	}

	CnRenderTexture* starMergeTex = RenderDevice->CreateRenderTexture( L"strMergeRT", nWidth, nHeight, usColorDepth,
																	   PixelFormat::A16B16G16R16F );
	if (starMergeTex)
	{
		CnString name = starMergeTex->GetName();

		// Scene
		CnScene* scene = m_pCindy->AddScene( name.c_str() );

		Scene::RenderScreenQuad* screenQuad = Scene::RenderScreenQuad::Create();

		scene->AddComponent( Scene::BeginScene::Create() );
		scene->AddComponent( screenQuad );
		scene->AddComponent( Scene::EndScene::Create() );

		CnViewport* pViewport = starMergeTex->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( scene );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		screenQuad->Setup( nWidth, nHeight );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();

			mtl->SetTexture( MapType::DiffuseMap0, starDir[0]->GetName() );
			mtl->SetTexture( MapType::DiffuseMap1, starDir[1]->GetName() );
			mtl->SetTexture( MapType::DiffuseMap2, starDir[2]->GetName() );
			mtl->SetTexture( MapType::DiffuseMap3, starDir[3]->GetName() );
			mtl->SetTexture( MapType::DiffuseMap4, starDir[4]->GetName() );
			mtl->SetTexture( MapType::DiffuseMap5, starDir[5]->GetName() );

			CnTextureState* textureState = mtl->GetTextureState();
			CnTextureState::StageState stageState;
			stageState.filter.mag = CnTextureState::TEXF_LINEAR;
			stageState.filter.min = CnTextureState::TEXF_LINEAR;
			stageState.filter.mip = CnTextureState::TEXF_LINEAR;
			textureState->SetStage( 0, stageState );

			mtl->SetShader( L"Hdr.fx" );
			mtl->SetCurrentTechnique( L"MergeStarTexture" );
		}
	}

	return starMergeTex;
}

//----------------------------------------------------------------
void CApplication::SetData()
{
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	CnString meshName = L"newAtoom.mmf";		// atoom_nrm_spec.mmf
	CnString mtlName = L"newAtoom_mtl.xml";	// atoom_nrm_spec_mtl.xml

	CnModelNode* modelnode = NULL;
	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	modelnode->AddMesh( meshName, mtlName );

	modelnode->Yaw( Math::DegreeToRadian(20.0f) );

	m_pSceneGraph->AttachChild( modelnode );

	m_pMeshDataGroup->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model2" );
	{
		SkelDataPtr skeleton = m_pSkeletonDataMgr->Load( L"bango_rush_m.mbf" );
		modelnode->AddSkeleton( skeleton );
		modelnode->AddMesh( L"newExpAll.mmf", L"newExpAll2_mtl.xml" );
	}
	modelnode->SetPosition( Math::Vector3( 0.0f, 0.0f, -10.0f ) );
	m_pSceneGraph->AttachChild( modelnode );

	//modelnode = CnNew CnModelNode( L"test2", 0xffff );
	//modelnode->SetPosition( Math::Vector3( 5.0f, 5.0f, 0.0f ) );
	//modelnode->AttachMesh( spMesh, false );
	//m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	//// ground
	//MeshDataPtr spGound = meshGroup->Load( L"Data\\base_grid.mmf" );
	//modelnode = CnNew CnModelNode( L"ground", 0xffff );
	//modelnode->AttachMesh( spGound, false );

	//m_pSceneGraph->GetRoot()->AttachChild( modelnode );
}

//----------------------------------------------------------------
void CApplication::GetDown2x2SampleOffsets( int nWidth, int nHeight, Math::Vector2 *paSampleOffsets )
{
	float tu = 1.0f / nWidth;
	float tv = 1.0f / nHeight;

	int index=0;
	for (int y=0; y<2; ++y)
	{
		for (int x=0; x<2; ++x)
		{
			paSampleOffsets[index].x = (x-0.5f) * tu;
			paSampleOffsets[index].y = (y-0.5f) * tv;
			index++;
		}
	}
}

//----------------------------------------------------------------
void CApplication::GetDown4x4SampleOffsets( int nWidth, int nHeight, Math::Vector2 *paSampleOffsets )
{
	float tu = 1.0f / nWidth;
	float tv = 1.0f / nHeight;

	int index=0;
	for (int y=0; y<4; ++y)
	{
		for (int x=0; x<4; ++x)
		{
			paSampleOffsets[index].x = (x-1.5f) * tu;
			paSampleOffsets[index].y = (y-1.5f) * tv;
			index++;
		}
	}
}

//----------------------------------------------------------------
void CApplication::GetBloomSampleOffsets( int nTexSize, float* paSampleOffsets, Math::Vector4* paSampleWeights,
											float fDeviation, float fMultiplier )
{
	float tu = 1.0f / (float)nTexSize;

	// 가운데 텍셀을 채운다.
	float weight = fMultiplier * Math::GaussianDistribution( 0, 0, fDeviation );
	paSampleWeights[0] = Math::Vector4( weight, weight, weight, 1.0f );
	paSampleOffsets[0] = 0.0f;

	int i = 0;
	for (i=1; i<8; ++i)
	{
		// 이 Offset에 대한 Gaussian intensity를 얻는다.
		weight = fMultiplier * Math::GaussianDistribution( (float)i, 0, fDeviation );
		paSampleOffsets[i] = i * tu;

		paSampleWeights[i] = Math::Vector4( weight, weight, weight, 1.0f );
	}

	// Mirror to the second half
	for (i=8; i<15; ++i)
	{
		paSampleWeights[i] = paSampleWeights[i-7];
		paSampleOffsets[i] = -paSampleOffsets[i-7];
	}
}

//----------------------------------------------------------------
void CApplication::GetGaussBlur5x5SampleOffsets( int nWidth, int nHeight, Math::Vector2* paSampleOffsets, Math::Vector4* paSampleWeights )
{
	float tu = 1.0f / (float)nWidth;
	float tv = 1.0f / (float)nHeight;

	Math::Vector4 white( 1.0f, 1.0f, 1.0f, 1.0f );
	float totalWeight = 0.0f;
	int index = 0;
	for (int x=-2; x<=2; ++x)
	{
		for (int y=-2; y<=2; ++y)
		{
			// 계수가 작아질 부분은 소거
			if (abs(x) + abs(y) > 2)
				continue;

			paSampleOffsets[index].x = x * tu;
			paSampleOffsets[index].y = y * tv;

			paSampleWeights[index] = white * Math::GaussianDistribution( (float)x, (float)y, 1.0f );

			totalWeight += paSampleWeights[index].x;
			index++;
		}
	}

	for (int i=0; i<index; ++i)
		paSampleWeights[i] *= 1.0f/totalWeight;
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_pCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_pCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_pCamera->GetRight( right );

		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_pCamera->GetRight( right );

		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_pCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_pCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_pCamera->GetRadius( min, max, radius );

			radius += delta;
			m_pCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}