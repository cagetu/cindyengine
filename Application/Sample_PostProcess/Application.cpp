#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>
#include <Cindy/Material/Fragment/CnFragmentManager.h>

#include <Cindy/Graphics/Base/CnRenderTexture.h>
#include <Cindy/Graphics/Base/CnRenderWindow.h>
#include <Cindy/Frame/CnPostEffectSystem.h>

//----------------------------------------------------------------
/**
*/
CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

//----------------------------------------------------------------
/**
*/
CnRenderWindow* CApplication::AttachWindow( HWND hWnd,
											const wchar_t* pStrTitle,
											int nWidth,
											int nHeight,
											ushort usColorDepth, 
										    ushort usRefreshRate,
											bool bFullScreen,
											bool bThreadSafe,
											bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd,
												   pStrTitle,
												   nWidth,
												   nHeight,
												   usColorDepth,
												   usRefreshRate,
												   bFullScreen,
												   bThreadSafe,
												   bSetCurrentTarget );
	return m_pRenderWindow;
}

//----------------------------------------------------------------
/**
*/
bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	// Setup Scene..
	SetScene( _T("Scene") );

	// Set Data Path
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	// Camera
	m_spCamera = CnModelViewCamera::Create();
	m_spCamera->SetName( L"main " );

	m_spCamera->SetPosition( Math::Vector3( 0.0f, 10.0f, -50.0f ) );
	m_spCamera->SetFar( 1000.0f );
	m_spCamera->SetRadius( 1.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );

	// Depth
	CnRenderTexture* sceneDepth = RenderDevice->CreateRenderTexture( L"SceneDepth",
																	 m_pRenderWindow->GetWidth(),
																	 m_pRenderWindow->GetHeight(),
																	 m_pRenderWindow->GetColorDepth(),
																	 PixelFormat::A8R8G8B8 );
	if (sceneDepth)
	{
		CnScene* newScene = m_pCindy->AddScene( L"SceneDepth" );

		SceneComPtr sceneUpdator = m_pWorld->GetComponent( L"SceneUpdator" );

		Scene::RenderDepth* depthRenderer = Scene::RenderDepth::Create();
		depthRenderer->Link( sceneUpdator );

		depthRenderer->Open();
		depthRenderer->SetShader( L"Depth.fx" );

		//Scene::RenderAABB* aabbRenderer = Scene::RenderAABB::Create();
		//aabbRenderer->Link( sceneUpdator );

		//CnMaterial* mtl = depthRenderer->GetMaterial();
		//mtl->SetShader( L"Depth.fx" );
		//mtl->SetCurrentTechnique( L"RenderStaticDepth" );

		newScene->AddComponent( sceneUpdator );
		newScene->AddComponent( Scene::BeginScene::Create() );
		newScene->AddComponent( depthRenderer );
		//newScene->AddComponent( aabbRenderer );
		newScene->AddComponent( Scene::EndScene::Create() );

		// viewport 1
		CnViewport* pViewport = sceneDepth->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetScene( newScene );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	}

	// CnFrameManager
	bool loadFrame = CnFrameManager::Instance()->Load( L"..\\..\\sdk\\PostEffect\\Frame.xml" );
	assert( loadFrame );

	//CnFrameManager::Instance()->SetType( CnFrameManager::IMAGE_BASED );
	//CnFrameManager::Instance()->LoadPostEffect( L"..\\..\\sdk\\PostEffect\\HDR.xml" );

	//Pointer<CnRenderTarget> sceneTarget = CnFrameManager::Instance()->GetSceneTarget();

	//// viewport 1
	//CnViewport* pViewport = sceneTarget->AddViewport();
	//pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
	//pViewport->SetScene( m_pWorld );
	//pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );

	SetLights();
	SetData();

	return true;
}

//----------------------------------------------------------------
/** Set Scene
    @@brief      장면 설정
	@param        strName : 장면 이름
	@return       none
*/
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->AddScene( strName.c_str() );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::Create();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	Scene::UpdateScene* sceneUpdator = Scene::UpdateScene::Create();
	sceneUpdator->AddSceneGraph( m_pSceneGraph );

	Scene::RenderModel* modelRenderer = Scene::RenderModel::Create();
	modelRenderer->Link( sceneUpdator );

	Scene::RenderAABB* aabbRenderer = Scene::RenderAABB::Create();
	aabbRenderer->Link( sceneUpdator );

	m_pWorld->AddComponent( sceneUpdator );
	m_pWorld->AddComponent( Scene::BeginScene::Create() );
	m_pWorld->AddComponent( modelRenderer );
	//m_pWorld->AddComponent( aabbRenderer );
	m_pWorld->AddComponent( Scene::EndScene::Create() );
}

//----------------------------------------------------------------
/**
*/
void CApplication::SetLights()
{
	{	// Directional Light
		CnLight* light = CnLight::Create();
		light->SetName( L"GlobalLight" );
		light->SetType( CnLight::Directional );

		Math::Vector3 direction( 0.0f, 1.0f, 1.0f );
		direction.Normalize();
		light->SetDirection( direction );

		light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 0.8f ) );
		light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
		light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

		Math::Vector3 position( 10.0f, 50.0f, -10.0f );
		CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"GlobalLight", 0xffff );
		lightNode->SetPosition( position );
		lightNode->AttachEntity( light );
		lightNode->EnableCulling( false );

		CnModelNode* modelNode = CnModelNode::Create();
		modelNode->SetName( L"lightModel" );

		m_pMeshDataMgr->AddDataPath( L"..\\Data\\TestPrimitive\\" );
		m_pMaterialDataMgr->AddDataPath( L"..\\Data\\TestPrimitive\\" );
		CnTextureManager::Instance()->AddDataPath( L"..\\Data\\TestPrimitive\\" );

		modelNode->AddMesh( L"testSphere.mmf", L"testSphere_mtl.xml" );

		lightNode->AttachChild( modelNode );

		m_pSceneGraph->AttachChild( lightNode );
	}

	{	// Ambient Light
		CnLight* ambientLight = CnLight::Create();
		ambientLight->SetName( L"AmbientLight" );
		ambientLight->SetType( CnLight::Ambient );
		ambientLight->SetAmbient( CnColor( 0.5f, 0.5f, 0.5f, 0.0f ) );

		CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"AmbientLight", 0xffff );
		lightNode->AttachEntity( ambientLight );

		m_pSceneGraph->AttachChild( lightNode );
	}
}

//----------------------------------------------------------------
/**
*/
void CApplication::SetData()
{
	// ground
	{
		m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
		m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
		CnTextureManager::Instance()->AddDataPath( L"..\\Data\\Ground\\" );
		
		CnModelNode* modelnode = NULL;

		modelnode = CnNew CnModelNode( L"ground", 0xffff );
		modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
		m_pSceneGraph->AttachChild( modelnode );
	}

	CnModelNode* modelnode = NULL;

	// 베룬
	//{
	//	m_pMeshDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	//	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	//	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\" );
	//	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\1k\\" );

	//	CnModelNode* modelnode = NULL;

	//	modelnode = CnModelNode::Create();
	//	modelnode->SetName( L"베룬" );
	//	{
	//		modelnode->AddMesh( L"berun_normal.mmf", L"berun_normal.mtl");
	//		modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );
	//		m_pSceneGraph->AttachChild( modelnode );
	//	}
	//}

	// 반고
	{
		m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

		modelnode = CnModelNode::Create();
		modelnode->SetName( L"model1" );
		{
			modelnode->AddSkeleton( L"bango_rush_m.mbf" );
			modelnode->AddMesh( L"bango_rush_m.mmf", L"bango_rush_m.mtl" );

			m_pSceneGraph->AttachChild( modelnode );
		}
	}
	// 반고2
	{
		m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
		CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

		modelnode = CnModelNode::Create();
		modelnode->SetName( L"model2" );
		{
			modelnode->AddSkeleton( L"bango_rush_m.mbf" );
			modelnode->AddMesh( L"bango_rush_m.mmf", L"bango_rush_m2.mtl" );
	
			m_pSceneGraph->AttachChild( modelnode );
						
			modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 10.0f) );
		}
	}
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}