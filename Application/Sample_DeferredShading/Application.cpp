#include "stdafx.h"
#include "Application.h"

#include <Cindy/Graphics/Base/CnRenderTexture.h>
#include <Cindy/Graphics/Base/CnMultipleRenderTarget.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Lighting/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

#include <Cindy/Util/CnRandomNumberTable.h>
#include <Cindy/Util/CnString.h>

//------------------------------------------------------------------------------
/**
*/
CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	m_pRenderWindow = 0;
	m_pWorld = 0;
	m_pSceneGraph = 0;

	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

//------------------------------------------------------------------------------
/**
*/
CnRenderWindow* CApplication::AttachWindow( HWND hWnd,
											const wchar_t* pStrTitle,
											int nWidth,
											int nHeight,
											ushort usColorDepth, 
										    ushort usRefreshRate,
											bool bFullScreen,
											bool bThreadSafe,
											bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd,
												   pStrTitle,
												   nWidth,
												   nHeight,
												   usColorDepth,
												   usRefreshRate,
												   bFullScreen,
												   bThreadSafe,
												   bSetCurrentTarget );
	m_pRenderWindow->Active(true);
	return m_pRenderWindow;
}

//------------------------------------------------------------------------------
/**
*/
bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	RenderDevice.SetShadeType( CnRenderer::Deferred );

	// Set Data Path
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	int windowWidth = m_pRenderWindow->GetWidth();
	int windowHeight = m_pRenderWindow->GetHeight();
	ushort windowColorDepth = m_pRenderWindow->GetColorDepth();

	// Setup Camera.
	m_spCamera = CnModelViewCamera::Create();
	m_spCamera->SetName( L"main" );
	m_spCamera->SetPosition( Math::Vector3( 0.0f, 5.0f, -7.0f ) );

	// Setup Scene..
	SetScene( _T("Scene") );
	SetLight();

	//// Depth
	//CnRenderTexture* sceneDepth = RenderDevice->CreateRenderTexture( L"Depth",
	//																 windowWidth,
	//																 windowHeight,
	//																 windowColorDepth,
	//																 PixelFormat::A8R8G8B8 );
	//if (sceneDepth)
	//{
	//	sceneDepth->Active(true);

	//	SceneComPtr sceneUpdator = m_pWorld->Lookup( L"SceneUpdator" );
	//	Scene::RenderDepth* depthRenderer = Scene::RenderDepth::Create();
	//	depthRenderer->Link( sceneUpdator );

	//	depthRenderer->Initialize();
	//	depthRenderer->SetShader( L"Depth.fx" );

	//	CnScene* newScene = m_pCindy->CreateScene( L"Depth" );
	//	newScene->AddComponent( Scene::BeginScene::Create() );
	//	newScene->AddComponent( depthRenderer );
	//	newScene->AddComponent( Scene::EndScene::Create() );

	//	// viewport 1
	//	CnViewport* pViewport = sceneDepth->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	//	pViewport->SetCamera( m_spCamera );
	//	pViewport->SetScene( newScene );
	//	pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	//}

	CnMultiRenderTarget* sceneMRT = RenderDevice->CreateMultiRenderTarget(L"SceneMRT");
	if (sceneMRT)
	{
		//sceneMRT->BindSurface( 0, L"Albedo", windowWidth, windowHeight, windowColorDepth, PixelFormat::A8R8G8B8 );
		//sceneMRT->BindSurface( 1, L"Depth", windowWidth, windowHeight, windowColorDepth, PixelFormat::A8R8G8B8 );
		//sceneMRT->BindSurface( 2, L"Normal", windowWidth, windowHeight, windowColorDepth, PixelFormat::A8R8G8B8 );
		sceneMRT->BindSurface( 0, L"Albedo", windowWidth, windowHeight, windowColorDepth, PixelFormat::A16B16G16R16F );
		sceneMRT->BindSurface( 1, L"Position", windowWidth, windowHeight, windowColorDepth, PixelFormat::A16B16G16R16F );
		sceneMRT->BindSurface( 2, L"Normal", windowWidth, windowHeight, windowColorDepth, PixelFormat::A16B16G16R16F );

		// viewport 1
		CnViewport* pViewport = sceneMRT->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetScene( m_pWorld );
		pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );
	}

	CnRenderTarget* lightRT = RenderDevice->CreateRenderTexture( L"LightPass0", windowWidth, windowHeight, windowColorDepth, PixelFormat::A16B16G16R16F );
	if (lightRT)
	{
		CnScene* newScene = m_pCindy->AddScene( L"LightPass0" );

		// viewport 1
		CnViewport* pViewport = lightRT->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
		pViewport->SetScene( newScene );
		pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
		pViewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );

		/// DeferredLighting
		SceneComPtr sceneUpdator = m_pWorld->GetComponent( L"SceneUpdator" );

		/// 1. GlobalLighting
		Scene::Deferred::GlobalLight* deferredGlobalLightComponent = CnNew Scene::Deferred::GlobalLight();
		deferredGlobalLightComponent->Setup( windowWidth, windowHeight );
		{
			CnMaterial* mtl = deferredGlobalLightComponent->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"Position" );
			mtl->SetTexture( MapType::DiffuseMap1, L"Normal" );
			mtl->SetTexture( MapType::DiffuseMap2, L"Albedo" );

			mtl->SetShader( L"DeferredLighting.fx" );
		}
		deferredGlobalLightComponent->Link( sceneUpdator );

		/// 2. PointLighting
		Scene::Deferred::PointLight* deferredPointLightComponent = CnNew Scene::Deferred::PointLight();
		deferredPointLightComponent->Open();
		{
			CnMaterial* mtl = deferredPointLightComponent->GetMaterial();
			mtl->SetShader( L"DeferredLighting.fx" );
		}
		deferredPointLightComponent->Link( sceneUpdator );

		/// scene composite
		Scene::BeginScene* beginSceneComponent = CnNew Scene::BeginScene();
		beginSceneComponent->SetClearFlag( 0 );
		newScene->AddComponent( beginSceneComponent );
		newScene->AddComponent( sceneUpdator );
		newScene->AddComponent( deferredGlobalLightComponent );
		newScene->AddComponent( deferredPointLightComponent );
		newScene->AddComponent( CnNew Scene::EndScene() );
	}

	SetData();

	// Debug용 Scene
	{
		Scene::RenderScreenQuad* screenQuad = CnNew Scene::RenderScreenQuad();
		screenQuad->Setup( windowWidth, windowHeight );
		{
			CnMaterial* mtl = screenQuad->GetMaterial();
			mtl->SetTexture( MapType::DiffuseMap0, L"LightPass0" );
			mtl->SetShader( L"pe_compose.fx" );
			mtl->SetCurrentTechnique( L"RenderScreenQuad" );

			// Scene
			CnScene* mainScene = m_pCindy->AddScene( L"mainScene" );

			mainScene->AddComponent( CnNew Scene::BeginScene() );
			mainScene->AddComponent( screenQuad );
			mainScene->AddComponent( CnNew Scene::EndScene() );

			CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
			pViewport->SetScene( mainScene );
			pViewport->SetBackgroundColor( CN_RGBA( 0, 100, 0, 0 ) );
		}
	}
	return true;
}

//------------------------------------------------------------------------------
/**
*/
void CApplication::SetLight()
{
	// ambientLight
	CnLight* ambientLight = CnLight::Create();
	{
		ambientLight->SetName( L"GlobalAmbient" );
		ambientLight->SetType( CnLight::Ambient );
	}

	/// directionalLights
	CnLight* directionalLight0 = CnLight::Create();
	{
		directionalLight0->SetName( L"GlobalLight0" );
		directionalLight0->SetType( CnLight::Directional );

		Math::Vector3 direction( 1.0f, -1.0f, -1.0f );
		direction.Normalize();

		directionalLight0->SetDirection( direction );
		directionalLight0->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
		//directionalLight0->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
		//directionalLight0->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );
	}
	CnLight* directionalLight1 = CnLight::Create();
	{
		directionalLight1->SetName( L"GlobalLight1" );
		directionalLight1->SetType( CnLight::Directional );

		Math::Vector3 direction( -1.0f, 0.0f, -1.0f );
		direction.Normalize();

		directionalLight1->SetDirection( direction );
		directionalLight1->SetDiffuse( CnColor( 0.0f, 0.0f, 1.0f, 1.0f ) );
		//directionalLight1->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
		//directionalLight1->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );
	}
	CnSceneNode* globallightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"globalLight", 0xffff );
	globallightNode->AttachEntity( ambientLight );
	globallightNode->AttachEntity( directionalLight0 );
	globallightNode->AttachEntity( directionalLight1 );
	globallightNode->EnableCulling( false );

	/// pointLights
	unsigned randomKey = 0;
	for (int p=0; p<8; ++p)
	{
		CnString lightName(L"PointLight");
		CnString lightNumber = CnStringUtil::ToString(p);
		lightName += lightNumber;

		CnLight* pointLight0 = CnLight::Create();
		{
			pointLight0->SetName( lightName );
			pointLight0->SetType( CnLight::Point );

			pointLight0->SetAttenuation( 1.0f, 0.0f, 0.0f );

			float r = CnRandomNumberTable::Rand(randomKey++, 0.0f, 1.0f);
			float g = CnRandomNumberTable::Rand(randomKey++, 0.0f, 1.0f);
			float b = CnRandomNumberTable::Rand(randomKey++, 0.0f, 1.0f);
			pointLight0->SetDiffuse( CnColor( r, g, b, 1.0f ) );

			pointLight0->SetRange( 10.0f );

			//pointLight0->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
			//pointLight0->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );
		}
		CnSceneNode* pointlightNode0 = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( lightName, 0xffff );
		pointlightNode0->AttachEntity( pointLight0 );
		pointlightNode0->EnableCulling( false );

		float x = CnRandomNumberTable::Rand(randomKey++, -20.0f, 20.0f);
		float y = CnRandomNumberTable::Rand(randomKey++, 3.0f, 50.0f);
		float z = CnRandomNumberTable::Rand(randomKey++, -20.0f, 20.0f);
		pointlightNode0->SetPosition(Math::Vector3( x, y, z ));
	}

	//CnLight* pointLight0 = CnLight::Create();
	//{
	//	pointLight0->SetName( L"PointLight0" );
	//	pointLight0->SetType( CnLight::Point );

	//	pointLight0->SetAttenuation( 1.0f, 0.0f, 0.0f );
	//	pointLight0->SetDiffuse( CnColor( 1.0f, 1.0f, 0.0f, 1.0f ) );
	//	pointLight0->SetRange( 10.0f );

	//	//pointLight0->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	//	//pointLight0->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );
	//}
	//CnSceneNode* pointlightNode0 = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"pointLight0", 0xffff );
	//pointlightNode0->AttachEntity( pointLight0 );
	//pointlightNode0->SetPosition(Math::Vector3( 0.0f, 20.0f, -5.0f ));
	//pointlightNode0->EnableCulling( false );
}

//------------------------------------------------------------------------------
/** Set Scene
    @remarks      장면 설정
	@param        strName : 장면 이름
	@return       none
*/
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->AddScene( strName.c_str() );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::Create();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	Scene::UpdateScene* sceneUpdator = Scene::UpdateScene::Create();
	sceneUpdator->AddSceneGraph( m_pSceneGraph );

	Scene::RenderGeometry* modelRenderer = Scene::RenderGeometry::Create();
	modelRenderer->Link( sceneUpdator );

	//Ptr<Scene::RenderAABB> aabbRenderer = Scene::RenderAABB::Create();
	//aabbRenderer->Link( sceneUpdator );

	m_pWorld->AddComponent( sceneUpdator );
	m_pWorld->AddComponent( Scene::BeginScene::Create() );
	m_pWorld->AddComponent( modelRenderer );
	//m_pWorld->AddComponent( aabbRenderer );
	m_pWorld->AddComponent( Scene::EndScene::Create() );
}

//------------------------------------------------------------------------------
/**
*/
void CApplication::SetData()
{
	MeshDataPtr spMesh;
	MtlDataPtr spMaterial;
	SkelDataPtr spSkeleton;

	CnModelNode* modelnode = NULL;

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\Ground\\" );

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid_deferred.xml" );
	m_pSceneGraph->AttachChild( modelnode );

	// 반고
	//m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//CnString skeleton( L"bango_rush_m.mbf" );
	//CnString meshName( L"all.mmf" );
	//CnString mtlName( L"newExpAll_mtl.xml" );
	//CnString animName1( L"bango_m_rush_common_dance.maf" );
	//CnString animName2( L"bango_m_rush_common_attack1.maf" );
	//CnString animName3( L"bango_m_rush_common_run.maf" );

	//modelnode = CnModelNode::Create();
	//modelnode->SetName( L"model1" );
	//{
	//	spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
	//	modelnode->AddSkeleton( spSkeleton );
	//	modelnode->AddMesh( L"bango_rush_m.mmf", L"bango_rush_m.mtl" );

	//	m_pSceneGraph->AttachChild( modelnode );
	//}

	// 베룬
	//{
	//	m_pMeshDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	//	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	//	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\" );

	//	modelnode = CnModelNode::Create();
	//	modelnode->SetName( L"베룬" );
	//	modelnode->AddMesh( L"berun.mmf", L"berun(UberShaderDeferred).mtl");
	//	modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );
	//	m_pSceneGraph->AttachChild( modelnode );
	//}

	// 마커스
	{
		m_pMeshDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		m_pMaterialDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		m_pAnimationDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		TextureMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );

		modelnode = CnModelNode::Create();
		modelnode->SetName( L"Marcus" );
		{
			SkelDataPtr skeleton = m_pSkeletonDataMgr->Load( L"marcus.mbf" );
			modelnode->AddSkeleton( skeleton );
			modelnode->AddMesh( L"marcus.mmf", L"marcus_deferred.mtl");
			m_pSceneGraph->AttachChild( modelnode );

			// Animation
			AnimDataPtr ani = m_pAnimationDataMgr->Load( L"marcus.maf" );
			Ptr<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, ani );
			modelnode->GetAnimController()->Play();
			//modelnode->SetScale( Math::Vector3(0.01f, 0.01f, 0.01f) );
			//modelnode->SetPosition( Math::Vector3( 0.0f, 0.0f, 30.0f ));
		}
	}
}

//------------------------------------------------------------------------------
/**
*/
void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//------------------------------------------------------------------------------
/**
*/
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}