#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnSceneComponentImpl.h>

#include "PhysXSampleImpl.h"

//--------------------------------------------------------------
//	Class Application
//--------------------------------------------------------------
CApplication::CApplication()
{
	m_pMeshDataGroup = CnNew CnMeshDataManager();
	m_pMaterialDataGroup = CnNew CnMaterialDataManager();
}
CApplication::~CApplication()
{
	m_PhysX.Release();

	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
}

//--------------------------------------------------------------
CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
												 nWidth, nHeight, usColorDepth,
												 usRefreshRate, bFullScreen,
												 bThreadSafe, bSetCurrentTarget );
	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	// Setup Scene..
	SetScene( _T("Scene") );

	m_spCamera = CnNew CnModelViewCamera( L"main " );

	m_spCamera->SetPosition( CnVector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 1000.0f );
	m_spCamera->SetRadius( 10.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( CnVector3( 0.0f, 10.0f, 0.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnNew CnLight( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	CnVector3 position( 0.0f, -1.0f, 0.0f );
	CnVector3 direction = -position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 0.8f, 0.8f, 0.8f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );

	CnSceneNode* lightNode = CnNew CnSceneNode( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );

	m_pSceneGraph->AttachChild( lightNode );

	// Data Setting
	SetData();

	// PhysX
	PhysXSample* sample = NULL;
	{
		sample = new SampleBoxNSphere();
		//sample = new SampleStaticNKinematicActor();
		//sample = new SampleFreezeNDamping();
		//sample = new SampleCollisionGroup();
		//sample = new SampleConvexShapeCreation();
	}
	sample->SetSceneGraph( m_pSceneGraph );

	m_PhysX.Register( sample );
	m_PhysX.Initialize();

	return true;
}

void CApplication::DeInitialize()
{
}

//--------------------------------------------------------------
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->CreateScene( L"Scene" );

	// 장면 그래프
	m_pSceneGraph = CnNew CnSceneGraph( L"MainSceneGraph" );

	SceneComponentImpl::UpdateScene* sceneUpdator = CnNew SceneComponentImpl::UpdateScene();
	sceneUpdator->Register( m_pSceneGraph );

	SceneComponentImpl::RenderModel* modelRenderer = CnNew SceneComponentImpl::RenderModel();
	modelRenderer->Link( sceneUpdator );

	SceneComponentImpl::RenderAABB* aabbRenderer = CnNew SceneComponentImpl::RenderAABB();
	aabbRenderer->Link( sceneUpdator );

	m_pWorld->Register( sceneUpdator );
	m_pWorld->Register( CnNew SceneComponentImpl::BeginScene() );
	m_pWorld->Register( modelRenderer );
	m_pWorld->Register( aabbRenderer );
	m_pWorld->Register( CnNew SceneComponentImpl::EndScene() );
}

//--------------------------------------------------------------
void CApplication::SetData()
{
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\" );

	m_pMeshDataGroup->AddDataPath( L"..\\Data\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\" );

	m_pMeshDataGroup->AddDataPath( L"..\\Data\\PhysXShape\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\PhysXShape\\" );
}

//--------------------------------------------------------------
void CApplication::Run()
{
	if (::GetAsyncKeyState('W'))
	{
		CnVector3 dir;
		m_spCamera->GetDirection( dir );
		
		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		CnVector3 dir;
		m_spCamera->GetDirection( dir );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		CnVector3 right;
		m_spCamera->GetRight( right );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		CnVector3 right;
		m_spCamera->GetRight( right );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}

	//--------------------------------------------------------------
	/**	순서..
		1. GetPhysicsResults()
		2. Input 처리 및 입력에 따른 Actor 들의 위치 이동
		3. Simulate
		4. Draw..
	*/
	//--------------------------------------------------------------
	m_PhysX.GetPhysicsResults();
	m_PhysX.Update();
	m_PhysX.Simulate();

	unsigned int numActors = m_PhysX.Scene()->getNbActors();
	NxActor** actors = m_PhysX.Scene()->getActors();
	while (numActors--)
	{
		NxActor* actor = *actors++;

		// DrawActor
		NxShape* const* shapes = actor->getShapes();
		unsigned int numShapes = actor->getNbShapes();
		while (numShapes--)
		{
			// 그리자..
			NxShape* shape = shapes[numShapes];

			m_PhysX.Draw( shape, actor );

		}
	}

	//
	CnFramework::Run();
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}