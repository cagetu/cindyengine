#include "stdafx.h"
#include "PhysXSample.h"
#include "Physic.h"

PhysXSample::PhysXSample()
: m_pOwner( 0 )
{
}

void PhysXSample::SetOwner( PhysX* pOwner )
{
	m_pOwner = pOwner;
}

//--------------------------------------------------------------
void PhysXSample::SetSceneGraph( const SceneGraphPtr& SceneGraph )
{
	m_spSceneGraph = SceneGraph;
}