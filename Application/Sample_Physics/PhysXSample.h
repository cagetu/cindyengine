#ifndef __PHYSX_SAMPLE_H__
#define __PHYSX_SAMPLE_H__

class PhysX;

class PhysXSample
{
protected:
	PhysX*	m_pOwner;

	SceneGraphPtr	m_spSceneGraph;

public:
	PhysXSample();
	virtual ~PhysXSample() {}

	void			SetOwner( PhysX* pOwner );
	void			SetSceneGraph( const SceneGraphPtr& SceneGraph );

	virtual void	Initialize() abstract;
	virtual void	Update() abstract;
	virtual void	Draw( NxShape* shape, NxActor* actor ) abstract;
};

#endif	// __PHYSX_SAMPLE_H__