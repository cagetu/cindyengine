#pragma once

#include <Cindy/CnFramework.h>

#include <Cindy/Scene/CnSceneComponent.h>
#include <Cindy/Scene/CnCameraImpl.h>
#include <Cindy/System/CnMouse.h>

#include "Physic.h"

// Application
class CApplication : public CnFramework
{
public:
	CnMouse		m_Mouse;
	short		m_nRotX;
	short		m_nRotY;

	//--------------------------------------------------------------
	//	Methods
	//--------------------------------------------------------------
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*	AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
								  int nWidth, int nHeight, ushort usColorDepth,
								  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
								  bool bSetCurrentTarget = false ) override;

	bool			Initialize() override;
	void			DeInitialize() override;

	void			SetData();
	void			Run() override;

	void			SetScene( const CnString& strName );

	// MsgProc
	virtual bool	MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) override;

private:
	CnRenderWindow*			m_pRenderWindow;

	//CnCamera*				m_pCamera;
	MdvCamPtr				m_spCamera;

	CnMeshDataManager*		m_pMeshDataGroup;
	CnMaterialDataManager*	m_pMaterialDataGroup;

	PhysX		m_PhysX;
};