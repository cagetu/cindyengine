#include "stdafx.h"
#include "PhysXSampleImpl.h"

#include "Physic.h"
#include "PhysXUserData.h"

#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Math/CnSphere.h>
#include <Cindy/System/CnString.h>

//==============================================================
//	Class SampleBoxNSphere
//==============================================================
SampleBoxNSphere::SampleBoxNSphere()
{
}
SampleBoxNSphere::~SampleBoxNSphere()
{
}

void SampleBoxNSphere::Initialize()
{
	//--------------------------------------------------------------
	// 장면에서 오브젝트들을 생성
	CnModelNode* modelnode = NULL;

	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"base_grid.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"base_grid.xml" );

		modelnode = CnNew CnModelNode( L"grid", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

			m_spSceneGraph->AttachChild( modelnode );
		}
	}

	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"testBox.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"testBox_mtl.xml" );

		modelnode = CnNew CnModelNode( L"box", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

			m_spSceneGraph->AttachChild( modelnode );
		}
	}

	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"testSphere.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"testSphere_mtl.xml" );

		modelnode = CnNew CnModelNode( L"sphere", 0xffff );
		//modelnode->SetPosition( CnVector3( 5.0f, 5.0f, 0.0f ) );
		modelnode->AttachMesh( spMesh );
		modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

		m_spSceneGraph->AttachChild( modelnode );
	}

	m_spSceneGraph->UpdateTransform();

	//--------------------------------------------------------------
	// 장면에서 오브젝트들을 생성

	{	// 1. 기본 충돌 Plane 만들기
		NxPlaneShapeDesc planeDesc;
		NxActorDesc actorDesc;

		actorDesc.shapes.pushBack( &planeDesc );

		m_pGroundPlane = m_pOwner->m_pScene->createActor( actorDesc );
	}
	
	{	// 2. 기본 충돌 박스 만들기
		// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box" );
		if (modelNode)
		{
			CnVector3 pos = modelNode->GetWorldPosition();

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();
			CnVector3 min = aabb.GetMin();
			CnVector3 max = aabb.GetMax();

			CnVector3 extent = max - min;
			extent *= 0.5f;

			float boxStartHeight = 10.0f;
			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, pos.y + boxStartHeight, pos.z );

			m_pBox = m_pOwner->CreateBox( "box", global, local, NxVec3( extent.x, extent.y, extent.z ) );
		}
	}

	{	// 3. 기본 충돌 구 만들기
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"sphere" );
		if (modelNode)
		{
			//CnVector3 pos = modelNode->GetWorldPosition();
			CnVector3 pos( 30.0f, 0.0f, 0.0f );

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();
			CnVector3 min = aabb.GetMin();
			CnVector3 max = aabb.GetMax();

			CnVector3 extent = max - cen;
			float radius = max.x - cen.x;

//			CnSphere sphere( &aabb );

			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, pos.y + 20.0f, pos.z );

			//float tmp = sphere.radius * 0.254f;

			m_pSphere = m_pOwner->CreateSphere( "sphere", global, local, radius );
		}
	}

	//m_pCurrentActor = m_pBox;
	m_pCurrentActor = m_pSphere;
}

void SampleBoxNSphere::Update()
{
	if (::GetAsyncKeyState('I'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, 1.0f), 5000000.0f, true );
	}
	if (::GetAsyncKeyState('K'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, -1.0f), 5000000.0f, true );
	}
	if (::GetAsyncKeyState('J'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(-1.0f, 0.0f, 0.0f), 5000000.0f, true );
	}
	if (::GetAsyncKeyState('L'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(1.0f, 0.0f, 0.0f), 5000000.0f, true );
	}
}

//--------------------------------------------------------------
void SampleBoxNSphere::Draw( NxShape* shape, NxActor* actor )
{
	NxShapeType type = shape->getType();
	switch (type)
	{
	case NX_SHAPE_PLANE:
		break;
	case NX_SHAPE_BOX:
		{
			NxVec3 localPos = shape->getLocalPosition();
			NxVec3 globalPos = shape->getGlobalPosition();

			NxMat34 pose = shape->getGlobalPose();
			NxVec3 boxDim = shape->isBox()->getDimensions();

			// draw...
			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( L"box" ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
				//node->SetScale( CnVector3( boxDim.x,boxDim.y, boxDim.z ) );
			}
		}
		break;
	case NX_SHAPE_SPHERE:
		{
			NxMat34 pose = shape->getGlobalPose();

			// draw...
			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( L"sphere" ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
			}
		}
		break;
	case NX_SHAPE_CAPSULE:
		break;
	case NX_SHAPE_CONVEX:
		break;
	case NX_SHAPE_MESH:
		break;
	case NX_SHAPE_WHEEL:
		break;
	case NX_SHAPE_HEIGHTFIELD:
		break;
	}
}

//==============================================================
//	Class SampleStaticNKinematicActor
//==============================================================
SampleStaticNKinematicActor::SampleStaticNKinematicActor()
: m_KT(0.0f)
{
}
SampleStaticNKinematicActor::~SampleStaticNKinematicActor()
{
}

//--------------------------------------------------------------
void SampleStaticNKinematicActor::Initialize()
{
	//--------------------------------------------------------------
	// 장면에서 오브젝트들을 생성
	CnModelNode* modelnode = NULL;
	
	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"base_grid.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"base_grid.xml" );

		modelnode = CnNew CnModelNode( L"grid", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

			m_spSceneGraph->AttachChild( modelnode );
		}
	}

	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"testBox.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"testBox_mtl.xml" );
		MtlDataPtr spMaterial2 = CnMaterialDataManager::Instance()->Load( L"testBox_mtl2.xml" );

		modelnode = CnNew CnModelNode( L"box1", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( -20.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"box2", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial2 );
			modelnode->SetPosition( CnVector3( 0.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"box3", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( 20.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		m_spSceneGraph->UpdateTransform();
	}

	//--------------------------------------------------------------
	{	// 1. 기본 충돌 Plane 만들기
		NxPlaneShapeDesc planeDesc;
		NxActorDesc actorDesc;

		actorDesc.shapes.pushBack( &planeDesc );

		m_pGroundPlane = m_pOwner->m_pScene->createActor( actorDesc );
	}

	{	// 2. 기본 충돌 박스 만들기
		// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box1" );

		CnAABB aabb;
		modelNode->GetBound( &aabb );

		CnVector3 cen = aabb.GetCenter();
		CnVector3 min = aabb.GetMin();
		CnVector3 max = aabb.GetMax();

		CnVector3 extent = max - min;
		extent *= 0.5f;

		CnVector3 pos = modelNode->GetWorldPosition();

		NxVec3 local( cen.x, cen.y, cen.z );
		NxVec3 global( pos.x, pos.y, pos.z );

		m_pBox1 = m_pOwner->CreateBox( "box1", global, local, NxVec3( extent.x, extent.y, extent.z ), 0 );
	}

	{	// 3. 기본 충돌 박스 만들기
		// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box2" );
		CnVector3 pos = modelNode->GetWorldPosition();

		CnAABB aabb;
		modelNode->GetBound( &aabb );

		CnVector3 cen = aabb.GetCenter();
		CnVector3 min = aabb.GetMin();
		CnVector3 max = aabb.GetMax();

		CnVector3 extent = max - min;
		extent *= 0.5f;

		NxVec3 local( cen.x, cen.y, cen.z );
		NxVec3 global( pos.x, pos.y, pos.z );

		m_pBox2 = m_pOwner->CreateBox( "box2", global, local, NxVec3( extent.x, extent.y, extent.z ) );
		m_pCurrentActor = m_pBox2;
	}

	{	// 4. 기본 충돌 박스 만들기
		// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box3" );
		CnVector3 pos = modelNode->GetWorldPosition();

		CnAABB aabb;
		modelNode->GetBound( &aabb );

		CnVector3 cen = aabb.GetCenter();
		CnVector3 min = aabb.GetMin();
		CnVector3 max = aabb.GetMax();

		CnVector3 extent = max - min;
		extent *= 0.5f;

		NxVec3 local( cen.x, cen.y, cen.z );
		NxVec3 global( pos.x, 0.0f, pos.z );

		m_pBox3 = m_pOwner->CreateBox( "box3", global, local, NxVec3( extent.x, extent.y, extent.z ) );
		m_pBox3->raiseBodyFlag( NX_BF_KINEMATIC );

		m_StartKinematic = global;
	}
}

//--------------------------------------------------------------
void SampleStaticNKinematicActor::Update()
{
	if (::GetAsyncKeyState('I'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, 1.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('K'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, -1.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('J'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(-1.0f, 0.0f, 0.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('L'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(1.0f, 0.0f, 0.0f), 75000000, true );
	}

	// updateMoving..
	NxReal x = 40*(NxMath::sin(m_KT*NxTwoPi)*NxMath::sin(m_KT*NxTwoPi) - 0.5) + 2.5;	
	m_pBox3->moveGlobalPosition( m_StartKinematic + NxVec3(x,0,0) );
	m_KT += (0.3f * m_pOwner->m_fDeltaTime);
}

//--------------------------------------------------------------
void SampleStaticNKinematicActor::Draw( NxShape* shape, NxActor* actor )
{
	NxShapeType type = shape->getType();
	switch (type)
	{
	case NX_SHAPE_PLANE:
		break;
	case NX_SHAPE_BOX:
		{
			NxMat34 pose = shape->getGlobalPose();
			NxVec3 boxDim = shape->isBox()->getDimensions();

			// draw...
			const char* name = shape->getName();
			CnString Name;
			CnStringUtil::ConvertCharToWChar( name, Name );

			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( Name ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
				//node->SetScale( CnVector3( boxDim.x,boxDim.y, boxDim.z ) );
			}
		}
		break;
	case NX_SHAPE_SPHERE:
		break;
	case NX_SHAPE_CAPSULE:
		break;
	case NX_SHAPE_CONVEX:
		break;
	case NX_SHAPE_MESH:
		break;
	case NX_SHAPE_WHEEL:
		break;
	case NX_SHAPE_HEIGHTFIELD:
		break;
	}
}

//==============================================================
//	Class SampleFreezeNDamping
//==============================================================
SampleFreezeNDamping::SampleFreezeNDamping()
{
}
SampleFreezeNDamping::~SampleFreezeNDamping()
{
}

void SampleFreezeNDamping::Initialize()
{
	//--------------------------------------------------------------
	// 장면에서 오브젝트들을 생성
	CnModelNode* modelnode = NULL;
	
	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"base_grid.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"base_grid.xml" );

		modelnode = CnNew CnModelNode( L"grid", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

			m_spSceneGraph->AttachChild( modelnode );
		}
	}

	{
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"testBox.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"testBox_mtl.xml" );
		MtlDataPtr spMaterial2 = CnMaterialDataManager::Instance()->Load( L"testBox_mtl2.xml" );

		modelnode = CnNew CnModelNode( L"box1", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( -10.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"box2", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial2 );
			modelnode->SetPosition( CnVector3( 0.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"box3", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( 10.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		m_spSceneGraph->UpdateTransform();
	}

	//--------------------------------------------------------------
	{	// 1. 기본 충돌 Plane 만들기
		NxPlaneShapeDesc planeDesc;
		NxActorDesc actorDesc;

		actorDesc.shapes.pushBack( &planeDesc );

		m_pGroundPlane = m_pOwner->m_pScene->createActor( actorDesc );
	}

	{	// 2. 기본 충돌 박스 만들기
		// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box1" );

		CnAABB aabb;
		modelNode->GetBound( &aabb );

		CnVector3 cen = aabb.GetCenter();
		CnVector3 min = aabb.GetMin();
		CnVector3 max = aabb.GetMax();

		CnVector3 extent = max - min;
		extent *= 0.5f;

		CnVector3 pos = modelNode->GetWorldPosition();

		NxVec3 local( cen.x, cen.y, cen.z );
		NxVec3 global( pos.x, pos.y, pos.z );

		//--------------------------------------------------------------
		//	Damping(제동)
		//	Linear와 angular damping은 중력이나 마찰력이 없는 곳에서 actor들에 힘을 멈춘다.
		//	이것을 추가하지 않으면, 무제한으로 구르는 것이 지속된다.
		//--------------------------------------------------------------
		m_pBox1 = m_pOwner->CreateBox( "box1", global, local, NxVec3( extent.x, extent.y, extent.z ), 5 );
		//m_pBox1->raiseBodyFlag( NX_BF_FROZEN_POS );
		//m_pBox1->raiseBodyFlag( NX_BF_FROZEN_ROT );
		//m_pBox1->raiseBodyFlag( NX_BF_FROZEN_POS_X );
		m_pOwner->Frozen( m_pBox1, NX_BF_FROZEN_POS_X );
		m_pOwner->Gravity( m_pBox1, false );
		m_pBox1->setLinearDamping( 0.5f );
	}

	{	// 3. 기본 충돌 박스 만들기
		// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box2" );
		CnVector3 pos = modelNode->GetWorldPosition();

		CnAABB aabb;
		modelNode->GetBound( &aabb );

		CnVector3 cen = aabb.GetCenter();
		CnVector3 min = aabb.GetMin();
		CnVector3 max = aabb.GetMax();

		CnVector3 extent = max - min;
		extent *= 0.5f;

		NxVec3 local( cen.x, cen.y, cen.z );
		NxVec3 global( pos.x, pos.y, pos.z );

		m_pBox2 = m_pOwner->CreateBox( "box2", global, local, NxVec3( extent.x, extent.y, extent.z ) );

		m_pCurrentActor = m_pBox2;
	}

	{	// 4. 기본 충돌 박스 만들기
		// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
		CnModelNode* modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box3" );
		CnVector3 pos = modelNode->GetWorldPosition();

		CnAABB aabb;
		modelNode->GetBound( &aabb );

		CnVector3 cen = aabb.GetCenter();
		CnVector3 min = aabb.GetMin();
		CnVector3 max = aabb.GetMax();

		CnVector3 extent = max - min;
		extent *= 0.5f;

		NxVec3 local( cen.x, cen.y, cen.z );
		NxVec3 global( pos.x, 0.0f, pos.z );

		m_pBox3 = m_pOwner->CreateBox( "box3", global, local, NxVec3( extent.x, extent.y, extent.z ) );
	}
}

void SampleFreezeNDamping::Update()
{
	if (::GetAsyncKeyState('I'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, 1.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('K'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, -1.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('J'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(-1.0f, 0.0f, 0.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('L'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(1.0f, 0.0f, 0.0f), 75000000, true );
	}
}

void SampleFreezeNDamping::Draw( NxShape* shape, NxActor* actor )
{
	NxShapeType type = shape->getType();
	switch (type)
	{
	case NX_SHAPE_PLANE:
		break;
	case NX_SHAPE_BOX:
		{
			NxMat34 pose = shape->getGlobalPose();
			NxVec3 boxDim = shape->isBox()->getDimensions();

			// draw...
			const char* name = shape->getName();
			CnString Name;
			CnStringUtil::ConvertCharToWChar( name, Name );

			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( Name ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
				//node->SetScale( CnVector3( boxDim.x,boxDim.y, boxDim.z ) );
			}
		}
		break;
	case NX_SHAPE_SPHERE:
		break;
	case NX_SHAPE_CAPSULE:
		break;
	case NX_SHAPE_CONVEX:
		break;
	case NX_SHAPE_MESH:
		break;
	case NX_SHAPE_WHEEL:
		break;
	case NX_SHAPE_HEIGHTFIELD:
		break;
	}
}

//==============================================================
//	Class SampleCollisionGroup
//==============================================================
SampleCollisionGroup::SampleCollisionGroup()
{
}
SampleCollisionGroup::~SampleCollisionGroup()
{
}

//--------------------------------------------------------------
/** @brief	초기화 */
//--------------------------------------------------------------
void SampleCollisionGroup::Initialize()
{
	SetDatas();
	SetActors();
	SetGroups();
}

//--------------------------------------------------------------
/** @brief	데이터 설정 */
//--------------------------------------------------------------
void SampleCollisionGroup::SetDatas()
{
	//--------------------------------------------------------------
	// 장면에서 오브젝트들을 생성
	CnModelNode* modelnode = NULL;
	
	{	// Ground
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"base_grid.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"base_grid.xml" );

		modelnode = CnNew CnModelNode( L"grid", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

			m_spSceneGraph->AttachChild( modelnode );
		}
	}

	{	// Boxes
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"testBox.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"testBox_mtl.xml" );
		MtlDataPtr spMaterial2 = CnMaterialDataManager::Instance()->Load( L"testBox_mtl2.xml" );

		modelnode = CnNew CnModelNode( L"box1", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( -30.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"box2", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial2 );
			modelnode->SetPosition( CnVector3( -20.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"box3", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( -10.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}
	}

	{	// Spheres
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"testSphere.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"testSphere_mtl.xml" );

		modelnode = CnNew CnModelNode( L"sphere1", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( 10.0f, 10.0f, -15.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"sphere2", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( 10.0f, 10.0f, 0.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}

		modelnode = CnNew CnModelNode( L"sphere3", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
			modelnode->SetPosition( CnVector3( 10.0f, 10.0f, 15.0f ) );
			m_spSceneGraph->AttachChild( modelnode );
		}
	}

	m_spSceneGraph->UpdateTransform();
}

//--------------------------------------------------------------
/** @brief	Actor 설정 */
//--------------------------------------------------------------
void SampleCollisionGroup::SetActors()
{
	//--------------------------------------------------------------
	{	// 1. 기본 충돌 Plane 만들기
		NxPlaneShapeDesc planeDesc;
		NxActorDesc actorDesc;

		actorDesc.shapes.pushBack( &planeDesc );

		m_pGroundPlane = m_pOwner->m_pScene->createActor( actorDesc );
	}

	//	충돌 박스 그룹
	CnModelNode* modelNode = NULL;
	{
		{	// 2. 기본 충돌 박스 만들기
			// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
			modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box1" );

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();

			CnVector3 extent;
			aabb.GetExtent( &extent );

			CnVector3 pos = modelNode->GetWorldPosition();

			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, pos.y, pos.z );

			m_pBox1 = m_pOwner->CreateBox( "box1", global, local, NxVec3( extent.x, extent.y, extent.z ), 5 );
		}

		{	// 3. 기본 충돌 박스 만들기
			// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
			modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box2" );
			CnVector3 pos = modelNode->GetWorldPosition();

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();

			CnVector3 extent;
			aabb.GetExtent( &extent );

			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, pos.y, pos.z );

			m_pBox2 = m_pOwner->CreateBox( "box2", global, local, NxVec3( extent.x, extent.y, extent.z ) );

			m_pCurrentActor = m_pBox2;
		}

		{	// 4. 기본 충돌 박스 만들기
			// 박스가 ground롤 떨어지도록 하기 위한 시작 높이를 3.5m로 설정한다
			modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"box3" );
			CnVector3 pos = modelNode->GetWorldPosition();

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();

			CnVector3 extent;
			aabb.GetExtent( &extent );

			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, 0.0f, pos.z );

			m_pBox3 = m_pOwner->CreateBox( "box3", global, local, NxVec3( extent.x, extent.y, extent.z ) );
		}

	}

	{	// Sphere 충돌 그룹
		{
			modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"sphere1" );
			CnVector3 pos = modelNode->GetWorldPosition();

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();
			CnVector3 max = aabb.GetMax();

			CnVector3 extent = max - cen;
			float radius = max.x - cen.x;

//			CnSphere sphere( &aabb );

			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, pos.y + 20.0f, pos.z );

			m_pSphere1 = m_pOwner->CreateSphere( "sphere1", global, local, radius );
		}
		{
			modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"sphere2" );
			CnVector3 pos = modelNode->GetWorldPosition();

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();
			CnVector3 max = aabb.GetMax();

			CnVector3 extent = max - cen;
			float radius = max.x - cen.x;

//			CnSphere sphere( &aabb );

			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, pos.y + 20.0f, pos.z );

			m_pSphere2 = m_pOwner->CreateSphere( "sphere2", global, local, radius );
		}
		{
			modelNode = (CnModelNode*)m_spSceneGraph->GetRoot()->GetChild( L"sphere3" );
			CnVector3 pos = modelNode->GetWorldPosition();

			CnAABB aabb;
			modelNode->GetBound( &aabb );

			CnVector3 cen = aabb.GetCenter();
			CnVector3 max = aabb.GetMax();

			CnVector3 extent = max - cen;
			float radius = max.x - cen.x;

//			CnSphere sphere( &aabb );

			NxVec3 local( cen.x, cen.y, cen.z );
			NxVec3 global( pos.x, pos.y + 20.0f, pos.z );

			m_pSphere3 = m_pOwner->CreateSphere( "sphere3", global, local, radius );
		}
	}

	AddUserDataToActors( m_pOwner->m_pScene );
}

//--------------------------------------------------------------
/** @brief	Group 설정 */
//--------------------------------------------------------------
void SampleCollisionGroup::SetGroups()
{
	// Group 설정
	m_pOwner->SetActorCollisionGroup( m_pGroundPlane, PLANE_GROUP );

	m_pOwner->SetActorCollisionGroup( m_pBox1, BOX_GROUP );
	m_pOwner->SetActorCollisionGroup( m_pBox2, BOX_GROUP );
	m_pOwner->SetActorCollisionGroup( m_pBox3, BOX_GROUP );

	m_pOwner->SetActorCollisionGroup( m_pSphere1, SPHERE_GROUP );
	m_pOwner->SetActorCollisionGroup( m_pSphere2, SPHERE_GROUP );
	m_pOwner->SetActorCollisionGroup( m_pSphere3, SPHERE_GROUP );

	// Group의 충돌 여부 설정
	m_pOwner->m_pScene->setGroupCollisionFlag( PLANE_GROUP, SPHERE_GROUP, true );
	m_pOwner->m_pScene->setGroupCollisionFlag( PLANE_GROUP, BOX_GROUP, true );
	m_pOwner->m_pScene->setGroupCollisionFlag( BOX_GROUP, SPHERE_GROUP, false );
}

//--------------------------------------------------------------
void SampleCollisionGroup::Update()
{
	if (::GetAsyncKeyState('I'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, 1.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('K'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(0.0f, 0.0f, -1.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('J'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(-1.0f, 0.0f, 0.0f), 75000000, true );
	}
	if (::GetAsyncKeyState('L'))
	{
		m_pOwner->ApplyForceToActor( m_pCurrentActor, NxVec3(1.0f, 0.0f, 0.0f), 75000000, true );
	}
}

void SampleCollisionGroup::Draw( NxShape* shape, NxActor* actor )
{
	NxShapeType type = shape->getType();
	switch (type)
	{
	case NX_SHAPE_PLANE:
		break;
	case NX_SHAPE_BOX:
		{
			NxMat34 pose = shape->getGlobalPose();
			NxVec3 boxDim = shape->isBox()->getDimensions();

			// draw...
			const char* name = shape->getName();
			CnString Name;
			CnStringUtil::ConvertCharToWChar( name, Name );

			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( Name ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
				//node->SetScale( CnVector3( boxDim.x,boxDim.y, boxDim.z ) );
			}
		}
		break;
	case NX_SHAPE_SPHERE:
		{
			NxVec3 localPos = shape->getLocalPosition();
			NxVec3 globalPos = shape->getGlobalPosition();

			NxMat34 pose = shape->getGlobalPose();

			// draw...
			const char* name = shape->getName();
			CnString Name;
			CnStringUtil::ConvertCharToWChar( name, Name );

			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( Name ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
				//node->SetScale( CnVector3( boxDim.x,boxDim.y, boxDim.z ) );
			}
		}
		break;
	case NX_SHAPE_CAPSULE:
		break;
	case NX_SHAPE_CONVEX:
		break;
	case NX_SHAPE_MESH:
		break;
	case NX_SHAPE_WHEEL:
		break;
	case NX_SHAPE_HEIGHTFIELD:
		break;
	}
}

//==============================================================
//	Class SampleConvexShapeCreation
//==============================================================
SampleConvexShapeCreation::SampleConvexShapeCreation()
{
}
SampleConvexShapeCreation::~SampleConvexShapeCreation()
{
}

void SampleConvexShapeCreation::SetDatas()
{
	//--------------------------------------------------------------
	// 장면에서 오브젝트들을 생성
	CnModelNode* modelnode = NULL;
	
	{	// Ground
		MeshDataPtr spMesh = CnMeshDataManager::Instance()->Load( L"base_grid.mmf" );
		MtlDataPtr spMaterial = CnMaterialDataManager::Instance()->Load( L"base_grid.xml" );

		modelnode = CnNew CnModelNode( L"grid", 0xffff );
		{
			modelnode->AttachMesh( spMesh );
			modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

			m_spSceneGraph->AttachChild( modelnode );
		}
	}

}

void SampleConvexShapeCreation::SetActors()
{
	//--------------------------------------------------------------
	{	// 1. 기본 충돌 Plane 만들기
		NxPlaneShapeDesc planeDesc;
		NxActorDesc actorDesc;

		actorDesc.shapes.pushBack( &planeDesc );

		m_pGroundPlane = m_pOwner->m_pScene->createActor( actorDesc );
	}

	// 메쉬를 Cook하기 전에 호출 되어야 한다.
	NxInitCooking();
	{
		// Cook
		NxConvexMeshDesc	meshDesc;
		
		meshDesc.numVertices = 0;		//!< mesh 버텍스 개수
		meshDesc.numTriangles = 0;		//!< mesh face 개수

		meshDesc.pointStrideBytes = 3 * sizeof(float);		//!< 버텍스 사이즈
		meshDesc.triangleStrideBytes = 3 * sizeof(WORD);	//!< Triangle 인덱스 사이즈

		meshDesc.points = 0;			//!< 버텍스 리스트 포인터
		meshDesc.triangles = 0;			//!< 인덱스 리스트 포인터

		meshDesc.flags = NX_CF_16_BIT_INDICES;
	}
	// Cooking library를 사용이 끝났을 때에 호출 되어야 한다.
	NxCloseCooking();
}

void SampleConvexShapeCreation::Initialize()
{
	SetDatas();
	SetActors();
}

void SampleConvexShapeCreation::Update()
{
}

void SampleConvexShapeCreation::Draw( NxShape* shape, NxActor* actor )
{
	NxShapeType type = shape->getType();
	switch (type)
	{
	case NX_SHAPE_PLANE:
		break;
	case NX_SHAPE_BOX:
		{
			NxMat34 pose = shape->getGlobalPose();
			NxVec3 boxDim = shape->isBox()->getDimensions();

			// draw...
			const char* name = shape->getName();
			CnString Name;
			CnStringUtil::ConvertCharToWChar( name, Name );

			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( Name ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
				//node->SetScale( CnVector3( boxDim.x,boxDim.y, boxDim.z ) );
			}
		}
		break;
	case NX_SHAPE_SPHERE:
		{
			NxVec3 localPos = shape->getLocalPosition();
			NxVec3 globalPos = shape->getGlobalPosition();

			NxMat34 pose = shape->getGlobalPose();

			// draw...
			const char* name = shape->getName();
			CnString Name;
			CnStringUtil::ConvertCharToWChar( name, Name );

			CnNode* node = m_spSceneGraph->GetRoot()->GetChild( Name ); 
			if (node)
			{
				CnVector3 tmp( pose.t.x, pose.t.y, pose.t.z );
				node->SetPosition( tmp );

				CnVector3 right = pose.M.getRow(0).get();
				CnVector3 up = pose.M.getRow(1).get();
				CnVector3 dir = pose.M.getRow(2).get();

				CnMatrix44 rot( right.x, right.y, right.z, 0.0f,
								 up.x, up.y, up.z, 0.0f,
								 dir.x, dir.y, dir.z, 0.0f,
								 0.0f, 0.0f, 0.0f, 1.0f );

				CnQuaternion quat;
				rot.GetRotation( quat );

				node->SetRotation( quat ); 
				//node->SetScale( CnVector3( boxDim.x,boxDim.y, boxDim.z ) );
			}
		}
		break;
	case NX_SHAPE_CAPSULE:
		break;
	case NX_SHAPE_CONVEX:
		break;
	case NX_SHAPE_MESH:
		break;
	case NX_SHAPE_WHEEL:
		break;
	case NX_SHAPE_HEIGHTFIELD:
		break;
	}
}