#ifndef __PHYSX_SAMPLE_IMPLEMENT_H__
#define __PHYSX_SAMPLE_IMPLEMENT_H__

#include "PhysXSample.h"

//==============================================================
//==============================================================
class SampleBoxNSphere : public PhysXSample
{
private:
	NxActor*		m_pGroundPlane;
	NxActor*		m_pBox;
	NxActor*		m_pSphere;

	NxActor*		m_pCurrentActor;
public:
	SampleBoxNSphere();
	virtual ~SampleBoxNSphere();

	void	Initialize() override;
	void	Update() override;
	void	Draw( NxShape* shape, NxActor* actor ) override;
};

//==============================================================
//==============================================================
class SampleStaticNKinematicActor : public PhysXSample
{
private:
	NxActor*		m_pGroundPlane;
	NxActor*		m_pBox1;
	NxActor*		m_pBox2;
	NxActor*		m_pBox3;

	NxActor*		m_pCurrentActor;

	NxVec3			m_StartKinematic;

	float			m_KT;

public:
	SampleStaticNKinematicActor();
	virtual ~SampleStaticNKinematicActor();

	void	Initialize() override;
	void	Update() override;
	void	Draw( NxShape* shape, NxActor* actor ) override;
};

//==============================================================
//==============================================================
class SampleFreezeNDamping : public PhysXSample
{
private:
	NxActor*		m_pGroundPlane;
	NxActor*		m_pBox1;
	NxActor*		m_pBox2;
	NxActor*		m_pBox3;

	NxActor*		m_pCurrentActor;

public:
	SampleFreezeNDamping();
	virtual ~SampleFreezeNDamping();

	void	Initialize() override;
	void	Update() override;
	void	Draw( NxShape* shape, NxActor* actor ) override;
};

//==============================================================
//==============================================================
class SampleCollisionGroup : public PhysXSample
{
	enum GROUPID
	{
		PLANE_GROUP = 0,
		BOX_GROUP,
		SPHERE_GROUP,
	};

private:
	NxActor*		m_pGroundPlane;

	NxActor*		m_pBox1;
	NxActor*		m_pBox2;
	NxActor*		m_pBox3;

	NxActor*		m_pSphere1;
	NxActor*		m_pSphere2;
	NxActor*		m_pSphere3;

	NxActor*		m_pCurrentActor;

	void	SetDatas();
	void	SetActors();
	void	SetGroups();
public:
	SampleCollisionGroup();
	virtual ~SampleCollisionGroup();

	void	Initialize() override;
	void	Update() override;
	void	Draw( NxShape* shape, NxActor* actor ) override;
};

//==============================================================
//==============================================================
class SampleConvexShapeCreation : public PhysXSample
{
private:
	NxActor*		m_pGroundPlane;

	
	void	SetDatas();
	void	SetActors();


public:
	SampleConvexShapeCreation();
	virtual ~SampleConvexShapeCreation();

	void	Initialize() override;

	void	Update() override;
	void	Draw( NxShape* shape, NxActor* actor ) override;
};

#endif	// __PHYSX_SAMPLE_IMPLEMENT_H__