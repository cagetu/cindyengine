#include "stdafx.h"
#include "Application.h"

#include <Cindy/Render/Base/CnRenderWindow.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd, pStrTitle,
													nWidth, nHeight, usColorDepth,
													usRefreshRate, bFullScreen,
													bThreadSafe, bSetCurrentTarget );
	m_pRenderWindow->Active(true);
	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	// Setup Scene..
	SetScene( _T("Scene") );

	// Set Data Path
	ShaderMgr->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	// Camera
	m_spCamera = CnModelViewCamera::Create();
	m_spCamera->SetName( L"main " );

	m_spCamera->SetPosition( Math::Vector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 10000.0f );
	m_spCamera->SetRadius( 1.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( CN_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnLight::Create();
	light->SetName( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	Math::Vector3 position( 10.0f, 10.0f, -10.0f );
	Math::Vector3 direction = position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

	CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );
	lightNode->EnableCulling( false );

	//SetData();
	SetData2();

	return true;
}

void CApplication::DeInitialize()
{
}

void CApplication::SetData()
{
	// 아툼
	MeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	MaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	SkeletonDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	TextureMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	// 반고
	MeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	MaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	SkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	AnimDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	TextureMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	// 엘프
	MeshDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	MaterialDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	SkeletonDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	AnimDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	TextureMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\Tex\\" );

	// ground
	MeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	MaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	TextureMgr->AddDataPath( L"..\\Data\\Ground\\" );

	MeshDataPtr spMesh;
	MtlDataPtr spMaterial;
	SkelDataPtr spSkeleton;

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
	m_pSceneGraph->AttachChild( modelnode );

	CnString skeleton( L"bango_rush_m.mbf" );
	CnString meshName( L"all.mmf" );
	CnString mtlName( L"newExpAll_mtl.xml" );
	CnString animName1( L"bango_m_rush_common_dance.maf" );
	CnString animName2( L"bango_m_rush_common_attack1.maf" );
	CnString animName3( L"bango_m_rush_common_run.maf" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"all.mmf", L"newExpAll_mtl.xml" );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model2" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( meshName, mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, -10.0f) );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model3" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"newExpAll.mmf", mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 10.0f) );

		// Animation
		//AnimDataPtr ani = m_pAnimationDataMgr->Load( animName3.c_str() );
		//Pointer<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, ani );
		//modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}
	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model4" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"newExpAll.mmf", L"newExpAll2_mtl.xml" );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 20.0f) );

		// Animation
		AnimDataPtr spAnimation1 = m_pAnimationDataMgr->Load( animName1.c_str() );
		//AnimDataPtr spAnimation2 = m_pAnimationDataMgr->Load( animName2.c_str() );

		Pointer<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, spAnimation1 );
		//channel1->GetMotion()->Lock( L"Bip01", L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 L Calf" );
		//channel1->GetMotion()->Lock( L"Bip01 R Calf" );
		//channel1->SetMixtureRatio( 0.001f );

		//Pointer<CnAnimChannel> channel2 = modelnode->AddAnimation( 1, spAnimation2 );
		//channel2->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel2->SetMixtureRatio( 0.2f );

		modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}

	//CnSceneNode* rootNode = m_pSceneGraph->GetRoot();
	//CnMaterialState* mtlState = CnMaterialState::Create();
	//mtlState->SetDiffuse( CnColor(1.0f, 0.5f, 0.0f) );
	//rootNode->AttachGlobalState( mtlState );
	//rootNode->UpdateRS();
}

void CApplication::SetData2()
{
	// 베룬
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\" );

	// 아크레시아
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bella\\bella\\TCROSSBOW\\" );

	CnModelNode* modelnode = NULL;

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"베룬" );
	{
		modelnode->AddMesh( L"berun.mmf", L"berun(UberShader).mtl");
		modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );
		m_pSceneGraph->AttachChild( modelnode );
	}

	//modelnode = CnModelNode::Create();
	//modelnode->SetName( L"rf" );
	//{
	//	SkelDataPtr skeleton = m_pSkeletonDataMgr->Load( L"mmBel.mbf" );
	//	modelnode->AddSkeleton( skeleton );
	//	modelnode->AddMesh( L"mmBel.mmf", L"mmBel_mtl.xml");
	//	//modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );

	//	// Animation
	//	AnimDataPtr animation1 = m_pAnimationDataMgr->Load( L"mmBel.maf" );

	//	Pointer<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, animation1 );
	//	modelnode->GetAnimController()->Play();

	//	m_pSceneGraph->AttachChild( modelnode );
	//}
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}