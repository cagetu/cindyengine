#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderGroup.h>
#include <Cindy/Material/CnTextureGroup.h>

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Scene/CnModelNode.h>

CApplication::CApplication()
{
	m_pMeshDataGroup = new CnMeshDataGroup();
	m_pMaterialDataGroup = new CnMaterialDataGroup();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	CnShaderGroup::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );
		
	CnRenderWindow* pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
															   nWidth, nHeight, usColorDepth,
															   usRefreshRate, bFullScreen,
															   bThreadSafe, bSetCurrentTarget );

	// Setup Scene..
	SetScene( _T("Scene") );

	m_pCamera = m_pCindy->CreateCamera( L"main" );
	m_pCamera->SetPosition( CnVector3( 0.0f, 7.0f, -14.0f ) );

	// viewport 1
	CnViewport* pViewport = pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_pCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	CnLight* light = NULL;
	CnVector3 position;

	// Ambient Light
	{
		light = CnNew CnLight( L"AmbientLight" );

		light->SetType( CnLight::Ambient );
		light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

		CnSceneNode* lightNode = CnNew CnSceneNode( L"AmbientLight", 0xffff );
		lightNode->AttachEntity( light );

		m_pSceneGraph->AttachChild( lightNode );
	}

	// Directional Light
	{
		light = CnNew CnLight( L"dirLight0" );
		light->SetType( CnLight::Directional );

		position = CnVector3( -3.0f, 10.f, -5.0f );

		light->SetDiffuse( CnColor( 0.5f, 0.5f, 0.5f, 0.1f ) );
		light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
		light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

		CnSceneNode* lightNode = CnNew CnSceneNode( L"dirLight0", 0xffff );
		lightNode->AttachEntity( light );
		lightNode->SetPosition( position );

		m_pSceneGraph->AttachChild( lightNode );
	}

	// Point Light
	{
		light = CnNew CnLight( L"pointLight0" );
		light->SetType( CnLight::Point );

		light->SetDiffuse( CnColor( 1.0f, 0.5f, 0.0f ) );
		light->SetRange( 10.0f );
		light->SetAttenuation( 1.0f, 0.0f, 0.0f );

		position = CnVector3( 3.0f, 10.0f, 0.0f );

		CnSceneNode* lightNode = CnNew CnSceneNode( L"pointLight0", 0xffff );
		lightNode->AttachEntity( light );
		lightNode->SetPosition( position );

		m_pSceneGraph->AttachChild( lightNode );
	}

	// Spot Light

	return pRenderWindow;
}

void CApplication::SetData()
{
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );	
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\" );	

	CnTextureGroup::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnTextureGroup::Instance()->AddDataPath( L"..\\Data\\" );

	MeshDataPtr spMesh = m_pMeshDataGroup->Load( L"atoom.mmf" );
	MtlDataPtr spMaterial = m_pMaterialDataGroup->Load( L"atoom_mtl.xml" );

	//SkeletonDataGroupPtr skeletonGroup = CnResourceManager::Instance().Get( CnSkeletonDataGroup::ms_Type.GetName() );
	//SkeletonDataPtr spSkeleton = skeletonGroup->Load( L"Data\\yumir_wind_m.mbf" );

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"atoom1", 0xffff );
	//////modelnode->AttachSkeleton( spSkeleton );
	modelnode->AttachMesh( spMesh );
	modelnode->AttachMaterial( L"atoom.mmf", spMaterial );

	m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	//modelnode = CnNew CnModelNode( L"test2", 0xffff );
	//modelnode->SetPosition( CnVector3( 5.0f, 5.0f, 0.0f ) );
	//modelnode->AttachMesh( spMesh );
	//m_pSceneGraph->GetRoot()->AttachChild( modelnode );

	//g_ShadowCaster.SetMaterial( L"Data\\ShadowMaterial.xml" );

	//// ground
	spMesh = CnMeshDataGroup::Instance()->Load( L"base_grid.mmf" );
	spMaterial = CnMaterialDataGroup::Instance()->Load( L"base_grid.xml" );

	modelnode = CnNew CnModelNode( L"grid", 0xffff );
	{
		modelnode->AttachMesh( spMesh );
		modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

		m_pSceneGraph->AttachChild( modelnode );
	}
}

void CApplication::Run()
{
	if (::GetAsyncKeyState('W'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.z += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('S'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.z -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('D'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.x += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );		
	}

	if (::GetAsyncKeyState('A'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.x -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState(VK_UP))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.y += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}
	if (::GetAsyncKeyState(VK_DOWN))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.y -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	CnFramework::Run();
}
