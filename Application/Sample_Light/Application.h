#pragma once

#include <Cindy/CnFramework.h>
#include <Cindy/Geometry/CnMeshDataGroup.h>
#include <Cindy/Material/CnMaterialDataGroup.h>

class CApplication : public CnFramework
{
private:
	CnCamera*		m_pCamera;

	CnMeshDataGroup*		m_pMeshDataGroup;
	CnMaterialDataGroup*	m_pMaterialDataGroup;

public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*		AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
									  int nWidth, int nHeight, ushort usColorDepth,
									  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
									  bool bSetCurrentTarget = false ) override;

	void		SetData();
	void		Run() override;
};