#pragma once

#include <Cindy/CnFramework.h>
#include <Cindy/Geometry/CnMeshDataManager.h>
#include <Cindy/Material/CnMaterialDataManager.h>
#include <Cindy/Animation/CnSkeletonDataManager.h>
#include <Cindy/Animation/CnAnimationDataManager.h>

#include <Cindy/Scene/CnCameraImpl.h>
#include <Cindy/System/CnMouse.h>

class CApplication : public CnFramework
{
public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*	AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
								  int nWidth, int nHeight, ushort usColorDepth,
								  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
								  bool bSetCurrentTarget = false ) override;

	bool			Initialize() override;
	void			DeInitialize() override;

	void			SetScene( const CnString& strName );

	void			SetData();
	void			Run() override;

	// MsgProc
	virtual bool	MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) override;

private:
	MdvCamPtr			m_spCamera;

	CnMouse				m_Mouse;
	short				m_nRotX;
	short				m_nRotY;

	CnRenderWindow*			m_pRenderWindow;

	CnMeshDataManager*		m_pMeshDataMgr;
	CnMaterialDataManager*	m_pMaterialDataMgr;
	CnSkeletonDataManager*	m_pSkeletonDataMgr;
	CnAnimationDataManager*	m_pAnimationDataMgr;
};
