#include "stdafx.h"
#include "Application.h"

#include <Cindy/Render/CnRenderWindow.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnSceneComponentImpl.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::NewObject();
	m_pMaterialDataMgr = CnMaterialDataManager::NewObject();
	m_pSkeletonDataMgr = CnSkeletonDataManager::NewObject();
	m_pAnimationDataMgr = CnAnimationDataManager::NewObject();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
											   nWidth, nHeight, usColorDepth,
											   usRefreshRate, bFullScreen,
											   bThreadSafe, bSetCurrentTarget );

	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	// Setup Scene..
	SetScene( _T("Scene") );

	// Camera
	m_spCamera = CnModelViewCamera::NewObject();
	m_spCamera->SetName( L"main " );

	m_spCamera->SetFar( 1000.0f );
	m_spCamera->SetRadius( 1.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );
	m_spCamera->SetPosition( Math::Vector3( 0.0f, 7.0f, -50.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnLight::NewObject();
	light->SetName( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	Math::Vector3 position( 10.0f, 10.0f, -10.0f );
	Math::Vector3 direction = position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 0.3f, 0.3f, 0.3f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

	CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );

	SetData();
	return true;
}

void CApplication::DeInitialize()
{
}

//================================================================
/** Set Scene
    @remarks      장면 설정
	@param        strName : 장면 이름
	@return       none
*/
//================================================================
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->CreateScene( strName.c_str() );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::NewObject();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	SceneComposite::UpdateScene* sceneUpdator = SceneComposite::UpdateScene::NewObject();
	sceneUpdator->Register( m_pSceneGraph );

	SceneComposite::RenderModel* modelRenderer = SceneComposite::RenderModel::NewObject();
	modelRenderer->Link( sceneUpdator );

	SceneComposite::RenderAABB* aabbRenderer = SceneComposite::RenderAABB::NewObject();
	aabbRenderer->Link( sceneUpdator );

	m_pWorld->Register( sceneUpdator );
	m_pWorld->Register( SceneComposite::BeginScene::NewObject() );
	m_pWorld->Register( modelRenderer );
	m_pWorld->Register( aabbRenderer );
	m_pWorld->Register( SceneComposite::RenderSkeleton::NewObject() );
	m_pWorld->Register( SceneComposite::EndScene::NewObject() );
}

//================================================================
/** Set Data
    @remarks      데이터 설정
*/
//================================================================
void CApplication::SetData()
{
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	m_pMeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	m_pMeshDataMgr->AddDataPath( L"..\\Data\\PhysXShape\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\PhysXShape\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\PhysXShape\\" );

	// IKTarget
	CnModelNode* ikTargetNode = CnModelNode::NewObject();
	{
		ikTargetNode->SetName(L"IKTarget");
		ikTargetNode->SetPosition( Math::Vector3(0.0f, 10.0f, -5.0f) );
		ikTargetNode->SetScale( Math::Vector3( 0.01f, 0.01f, 0.01f ) );
		ikTargetNode->UpdateTransform( false, false );

		ikTargetNode->AddMesh( L"testSphere.mmf", L"testSphere_mtl.xml" );
	}
	m_pSceneGraph->AttachChild( ikTargetNode );

	// Character
	MeshDataPtr spMesh;
	MtlDataPtr spMaterial;
	SkelDataPtr spSkeleton;

	CnString skeleton( L"bango_rush_m.mbf" );
	CnString meshName( L"all.mmf" );
	CnString mtlName( L"newExpAll_mtl.xml" );
	CnString animName1( L"bango_m_rush_common_dance.maf" );
	CnString animName2( L"bango_m_rush_common_attack1.maf" );

	//CnString skeleton( L"atoom.mbf" );
	//CnString meshName( L"atoom.mmf" );
	//CnString mtlName( L"atoom_mtl.xml" );

	CnModelNode* modelnode = NULL;
	modelnode = CnModelNode::NewObject();
	modelnode->SetName( L"atoom1" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AttachSkeleton( spSkeleton );
		modelnode->AddMesh( meshName, mtlName );

		SceneComPtr sceneCom = m_pWorld->Lookup( SceneComposite::RenderSkeleton::ms_Name );
		SceneComposite::RenderSkeleton* sceneComponent = static_cast<SceneComposite::RenderSkeleton*>(sceneCom.GetPtr());
		if (sceneComponent)
		{
			sceneComponent->AddSkeleton( modelnode->GetSkeleton() );
		}

		// IK 추가
		CnBoneNode* endEffector = (CnBoneNode*)modelnode->GetSkeleton()->GetNode( L"Bip01 L Hand" );
		if (endEffector)
		{
			CnIKController* ikCtrlr = CnIKController::NewObject();

			// 1개의 iksolver 설정
			IKSolverPtr solver = ikCtrlr->AddIKSolver( 0 );
			solver->goal = ikTargetNode;
			solver->endEffector = endEffector;
			solver->finish = (CnNode*)modelnode->GetSkeleton()->GetNode( L"Bip01 L UpperArm" );

			modelnode->SetIKController( ikCtrlr );
		}

		// Animation
		{
			AnimDataPtr spAnimation1 = m_pAnimationDataMgr->Load( animName1.c_str() );
			AnimDataPtr spAnimation2 = m_pAnimationDataMgr->Load( animName2.c_str() );

			Pointer<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, spAnimation1 );
			//channel1->GetMotion()->Lock( L"Bip01 Spine1" );
			//channel1->GetMotion()->Lock( L"Bip01 R Calf" );
			//channel1->SetMixtureRatio( 0.001f );

			//Pointer<CnAnimChannel> channel2 = modelnode->AddAnimation( 1, spAnimation2 );
			//channel2->GetMotion()->Lock( L"Bip01 Spine1" );
			//channel2->SetMixtureRatio( 0.2f );

			//modelnode->GetAnimCtrlr()->Play();
		}

		m_pSceneGraph->AttachChild( modelnode );
	}

	//CnSceneNode* rootNode = m_pSceneGraph->GetRoot();
	//CnMaterialState* mtlState = CnMaterialState::NewObject();
	//mtlState->SetDiffuse( CnColor(1.0f, 0.5f, 0.0f) );
	//rootNode->AttachGlobalState( mtlState );

	//modelnode = CnNew CnModelNode( L"all", 0xffff );
	//{
	//	spMesh = m_pMeshDataMgr->Load( L"all.mmf" );
	//	spMaterial = m_pMaterialDataMgr->Load( L"all_mtl.xml" );
	//	spSkeleton = m_pSkeletonDataMgr->Load( L"bango_rush_m.mbf" );

	//	//modelnode->SetPosition( Math::Vector3( 10.0f, 0.0f, 0.0f ) );
	//	modelnode->AttachMesh( spMesh );
	//	modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
	//	//modelnode->AttachSkeleton( spSkeleton );
	//	m_pSceneGraph->GetRoot()->AttachChild( modelnode );
	//}

	//// ground
	//MeshDataPtr spGound = meshGroup->Load( L"Data\\base_grid.mmf" );
	//modelnode = CnNew CnModelNode( L"ground", 0xffff );
	//modelnode->AttachMesh( spGound, false );

	//m_pSceneGraph->AttachChild( modelnode );

	//rootNode->UpdateRS();
}

void CApplication::Run()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}

	if (::GetAsyncKeyState(VK_NUMPAD4))
	{
		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"IKTarget" );
		node->Translate( Math::Vector3(-0.5f, 0.0f, 0.0f) );
	}
	if (::GetAsyncKeyState(VK_NUMPAD6))
	{
		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"IKTarget" );
		node->Translate( Math::Vector3(0.5f, 0.0f, 0.0f) );
	}
	if (::GetAsyncKeyState(VK_NUMPAD8))
	{
		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"IKTarget" );
		node->Translate( Math::Vector3(0.0f, 0.5f, 0.0f) );
	}
	if (::GetAsyncKeyState(VK_NUMPAD5))
	{
		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"IKTarget" );
		node->Translate( Math::Vector3(0.0f, -0.5f, 0.0f) );
	}
	if (::GetAsyncKeyState(VK_NUMPAD7))
	{
		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"IKTarget" );
		node->Translate( Math::Vector3(0.0f, 0.0f, -0.5f) );
	}
	if (::GetAsyncKeyState(VK_NUMPAD9))
	{
		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"IKTarget" );
		node->Translate( Math::Vector3(0.0f, 0.0f, 0.5f) );
	}

	CnFramework::Run();
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}