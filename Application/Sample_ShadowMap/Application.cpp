#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/Scene/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

#include <Cindy/Render/Base/CnRenderWindow.h>
#include <Cindy/Render/CnLisPSMProjector.h>
#include <Cindy/Render/CnPSMProjector.h>
#include <Cindy/Render/CnTSMProjector.h>

CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::New();
	m_pMaterialDataMgr = CnMaterialDataManager::New();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );
		
	m_pRenderWindow = Framework::Application::AttachWindow( hWnd, pStrTitle,
															nWidth, nHeight, usColorDepth,
															usRefreshRate, bFullScreen,
															bThreadSafe, bSetCurrentTarget );

	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	// Setup Scene..
	SetScene( _T("Scene") );

	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	m_pCamera = CnModelViewCamera::New();
	m_pCamera->SetName( L"main" );
	m_pCamera->SetPosition( Math::Vector3( 0.0f, 7.0f, -50.0f ) );
	m_pCamera->SetFar( 1000.0f );
	m_pCamera->SetRadius( 1.0f, 500.0f );
	m_pCamera->SetRadius( 50.0f );
	m_pCamera->SetLookAt( Math::Vector3( 0.0f, 10.0f, 0.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_pCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	{	// Ambient Light
		CnLight* ambientLight = CnNew CnLight( L"AmbientLight" );
		ambientLight->SetType( CnLight::Ambient );
		ambientLight->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

		CnSceneNode* lightNode = CnNew CnSceneNode( L"AmbientLight", 0xffff );
		lightNode->AttachEntity( ambientLight );

		m_pSceneGraph->AttachChild( lightNode );
	}

	{	// Directional Light
		CnLight* light = CnNew CnLight( L"dirLight0" );
		light->SetType( CnLight::Directional );

		Math::Vector3 direction( 1.0f, 1.0f, 1.0f );
		direction.Normalize();
		light->SetDirection( direction );

		light->SetDiffuse( CnColor( 0.5f, 0.5f, 0.5f, 0.1f ) );
		light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
		light->SetAmbient( CnColor( 0.0f, 0.0f, 0.0f, 0.0f ) );

		//Projector::LisPSM* projector = Projector::LisPSM::New();
		//projector->SetTexture( L"LispSM", 512, 512, 32, CnTexture::R32F );
		//Projector::PSM* projector = new Projector::PSM();
		//projector->SetTexture( L"PSM", 512, 512, 32 );
		Projector::TSM* projector = CnNew Projector::TSM();
		projector->SetTexture( L"TSM", 512, 512, 32 );
		projector->SetShader( L"ShadowMap.fx", L"RenderStaticDepthShadow", L"RenderSkinnedDepthShadow" );
		projector->Initialize();

		light->SetCaster( projector );

		CnSceneNode* lightNode = CnNew CnSceneNode( L"dirLight0", 0xffff );
		lightNode->AttachEntity( light );

		m_pSceneGraph->AttachChild( lightNode );
	}

	{	// Point Light
		Math::Vector3 position( 3.0f, 10.0f, 0.0f );

		CnLight* pointLight = CnNew CnLight( L"pointLight0" );
		pointLight->SetType( CnLight::Point );
		
		pointLight->SetDiffuse( CnColor( 1.0f, 0.5f, 1.0f ) );
		pointLight->SetRange( 10.0f );
		pointLight->SetAttenuation( 1.0f, 0.0f, 0.0f );

		CnSceneNode* lightNode = CnNew CnSceneNode( L"pointLight0", 0xffff );
		lightNode->AttachEntity( pointLight );
		lightNode->SetPosition( position );

		m_pSceneGraph->AttachChild( lightNode );
	}

	SetData();
	return true;
}

void CApplication::DeInitialize()
{
}

//================================================================
/** Set Scene
    @remarks      장면 설정
	@param        strName : 장면 이름
	@return       none
*/
//================================================================
void CApplication::SetScene( const CnString& strName )
{
	// Setup Scene..
	m_pWorld = m_pCindy->CreateScene( L"Scene" );

	// 장면 그래프
	m_pSceneGraph = CnSceneGraph::New();
	m_pSceneGraph->SetName( L"MainSceneGraph" );

	Scene::UpdateScene* sceneUpdator = Scene::UpdateScene::New();
	sceneUpdator->Register( m_pSceneGraph );

	Scene::RenderModel* modelRenderer = Scene::RenderModel::New();
	modelRenderer->Link( sceneUpdator );

	Scene::CastShadow* lightRenderer = Scene::CastShadow::New();
	lightRenderer->Link( sceneUpdator );

	Scene::DebugLight* lightDebug = Scene::DebugLight::New();
	lightDebug->Link( sceneUpdator );

	m_pWorld->Register( sceneUpdator );
	m_pWorld->Register( lightRenderer );
	m_pWorld->Register( Scene::BeginScene::New() );
	m_pWorld->Register( modelRenderer );
	m_pWorld->Register( lightDebug );
	m_pWorld->Register( Scene::EndScene::New() );
}

//================================================================
void CApplication::SetData()
{
	//m_pMeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	//m_pMaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );	
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	//MeshDataPtr spMesh = m_pMeshDataMgr->Load( L"atoom.mmf" );
	//MtlDataPtr spMaterial = m_pMaterialDataMgr->Load( L"atoom_mtl.xml" );

	//SkeletonDataGroupPtr skeletonGroup = CnResourceManager::Instance().Get( CnSkeletonDataGroup::ms_Type.GetName() );
	//SkeletonDataPtr spSkeleton = skeletonGroup->Load( L"Data\\yumir_wind_m.mbf" );

	CnModelNode* modelnode = NULL;
	modelnode = CnNew CnModelNode( L"model1", 0xffff );
	//modelnode->AttachSkeleton( spSkeleton );
	modelnode->AddMesh( L"newExpAll.mmf", L"newExpAll_mtl.xml", true );
	m_pSceneGraph->AttachChild( modelnode );

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\Ground\\" );

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
	m_pSceneGraph->AttachChild( modelnode );
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_pCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_pCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_pCamera->GetRight( right );

		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_pCamera->GetRight( right );

		Math::Vector3 lookAt = m_pCamera->GetLookAt();
		m_pCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_pCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_pCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_pCamera->GetRadius( min, max, radius );

			radius += delta;
			m_pCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}