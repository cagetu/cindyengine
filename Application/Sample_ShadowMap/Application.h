#pragma once

#include <Cindy/Framework/CnApplication.h>
#include <Cindy/Geometry/CnMeshDataManager.h>
#include <Cindy/Material/CnMaterialDataManager.h>

#include <Cindy/Scene/CnCameraImpl.h>
#include <Cindy/System/CnMouse.h>

class CApplication : public Framework::Application
{
public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*		AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
									  int nWidth, int nHeight, ushort usColorDepth,
									  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
									  bool bSetCurrentTarget = false ) override;

	bool		Initialize() override;
	void		DeInitialize() override;


	void		SetData();

	// MsgProc
	virtual bool	MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) override;

protected:
	virtual void	OnProcessInput() override;

private:
	MdvCamPtr			m_pCamera;

	CnMouse				m_Mouse;
	short				m_nRotX;
	short				m_nRotY;

	CnRenderWindow*		m_pRenderWindow;

	CnMeshDataManager*		m_pMeshDataMgr;
	CnMaterialDataManager*	m_pMaterialDataMgr;

	void	SetScene( const CnString& strName ) override;
};