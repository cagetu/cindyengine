#include "stdafx.h"
#include "Application.h"


#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnCamera.h>

CApplication::CApplication()
{
	m_pMeshDataGroup = CnNew CnMeshDataManager();
	m_pMaterialDataGroup = CnNew CnMaterialDataManager();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
}

bool CApplication::Initialize()
{
	SetLogFile( L"Log.txt", L"", true, true );

	// Setup Scene..
	SetScene( _T("Scene") );

	m_pCamera = m_pCindy->CreateCamera( L"main" );
	m_pCamera->SetPosition( CnVector3( 0.0f, 7.0f, -12.0f ) );

	m_pCamera2 = m_pCindy->CreateCamera( L"2nd" );
	m_pCamera2->SetPosition( CnVector3( 0.0f, 1.0f, -5.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 0.5f, 1.0f );
	pViewport->SetCamera( m_pCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 0, 0, 255, 0 ) );

	// viewport 2
	pViewport = m_pRenderWindow->AddViewport( 1, 0.0f, 0.5f, 0.5f, 1.0f );
	pViewport->SetCamera( m_pCamera2 );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 255, 0, 0, 0 ) );

	// SetData
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnShaderManager::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	MeshDataPtr spMesh = m_pMeshDataGroup->Load( L"atoom.mmf" );
	MtlDataPtr spMaterial = m_pMaterialDataGroup->Load( L"atoom_mtl.xml" );

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"atoom1", 0xffff );
	modelnode->AttachMesh( spMesh );
	modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

	m_pSceneGraph->AttachChild( modelnode );

	modelnode = CnNew CnModelNode( L"atoom2", 0xffff );
	//modelnode->SetPosition( CnVector3( 5.0f, 5.0f, 0.0f ) );
	modelnode->AttachMesh( spMesh );
	modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

	m_pSceneGraph->AttachChild( modelnode );
	return true;
}
void CApplication::DeInitialize()
{
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	m_pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
											   nWidth, nHeight, usColorDepth,
											   usRefreshRate, bFullScreen,
											   bThreadSafe, bSetCurrentTarget );
	return m_pRenderWindow;
}

void CApplication::Run()
{
	if (::GetAsyncKeyState('W'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.z += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('S'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.z -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('D'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.x += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );		
	}

	if (::GetAsyncKeyState('A'))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.x -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState(VK_UP))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.y += 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}
	if (::GetAsyncKeyState(VK_DOWN))
	{
		CnVector3 pos = m_pCamera->GetPosition();
		pos.y -= 1.0f;
		m_pCamera->SetPosition( pos );
		m_pCamera->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	// Camera 2
	if (::GetAsyncKeyState('I'))
	{
		CnVector3 pos = m_pCamera2->GetPosition();
		pos.z += 1.0f;
		m_pCamera2->SetPosition( pos );
		m_pCamera2->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('K'))
	{
		CnVector3 pos = m_pCamera2->GetPosition();
		pos.z -= 1.0f;
		m_pCamera2->SetPosition( pos );
		m_pCamera2->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	if (::GetAsyncKeyState('L'))
	{
		CnVector3 pos = m_pCamera2->GetPosition();
		pos.x += 1.0f;
		m_pCamera2->SetPosition( pos );
		m_pCamera2->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );		
	}

	if (::GetAsyncKeyState('J'))
	{
		CnVector3 pos = m_pCamera2->GetPosition();
		pos.x -= 1.0f;
		m_pCamera2->SetPosition( pos );
		m_pCamera2->SetLookAt( pos + CnVector3( 0.0f, 0.0f, 1.0f ) );
	}

	CnFramework::Run();
}
