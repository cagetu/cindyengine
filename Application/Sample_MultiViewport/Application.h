#pragma once

#include <Cindy/CnFramework.h>

#include <Cindy/Geometry/CnMeshDataManager.h>
#include <Cindy/Material/CnMaterialDataManager.h>

class CApplication : public CnFramework
{
public:
	CApplication();
	virtual ~CApplication();

	bool				Initialize() override;
	void				DeInitialize() override;

	CnRenderWindow*		AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
									  int nWidth, int nHeight, ushort usColorDepth,
									  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
									  bool bSetCurrentTarget = false ) override;

	void		Run() override;

private:
	CnRenderWindow*			m_pRenderWindow;

	CnCamera*				m_pCamera;
	CnCamera*				m_pCamera2;

	CnMeshDataManager*		m_pMeshDataGroup;
	CnMaterialDataManager*	m_pMaterialDataGroup;

};