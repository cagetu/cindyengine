#include "stdafx.h"
#include "Application.h"

#include <Cindy/Graphics/Base/CnRenderWindow.h>
#include <Cindy/Graphics/Base/CnRenderTexture.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Lighting/CnLight.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/Component/CindySceneComponents.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>
#include <Cindy/Material/Fragment/CnFragmentManager.h>

#include <Cindy/Lighting/ShadowMap/CindyShadowMaps.h>

#include <Cindy/Util/CnRandomNumberTable.h>
#include <Cindy/Util/CnString.h>

//#define TEST_LPP
//#define _TEST_SHADOW
#define TEST_MULTILIGHT

//----------------------------------------------------------------
/**
*/
CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pAnimationDataMgr = CnAnimationDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
	SAFEDEL( m_pAnimationDataMgr );
}

//----------------------------------------------------------------
/**
*/
CnRenderWindow* CApplication::AttachWindow( HWND hWnd,
											const wchar_t* pStrTitle,
											int nWidth,
											int nHeight,
											ushort usColorDepth, 
										    ushort usRefreshRate,
											bool bFullScreen,
											bool bThreadSafe,
											bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd,
												   pStrTitle,
												   nWidth,
												   nHeight,
												   usColorDepth,
												   usRefreshRate,
												   bFullScreen,
												   bThreadSafe,
												   bSetCurrentTarget );
	m_pRenderWindow->Active(true);
	return m_pRenderWindow;
}

//----------------------------------------------------------------
/**	@brief	초기화
*/
bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();
	if (false == result)
		return false;

	// Set Data Path
	FrameMgr->SetDataPath(L"..\\..\\sdk\\Frame\\");
	ShaderMgr->AddDataPath( L"..\\..\\sdk\\Shader\\" );
	TextureMgr->AddDataPath( L"..\\Data\\" );
	/// material Path
	TextureMgr->AddDataPath( L"..\\..\\sdk\\Material\\" );
	MaterialDataMgr->AddDataPath( L"..\\..\\sdk\\Material\\" );

#ifdef TEST_LPP
	RenderDevice.SetShadeType( CnRenderer::Deferred );
	m_Frame = CnFrameLoader::Load( L"F_LightPrePass.xml" );
#else
	m_Frame = CnFrameLoader::Load( L"F_PostEffect.xml" );
#endif
	if (m_Frame.IsNull())
		return false;

	m_pSceneGraph = CnCindy::Instance()->GetSceneGraph(L"DefaultSceneGraph");
	m_spCamera = CnCindy::Instance()->GetCamera(L"DefaultCamera");

	SetSceneGraph();

	return true;
}

//----------------------------------------------------------------
/**	@brief
*/
void CApplication::SetSceneGraph()
{
	// Light
	CnLight* light = CnLight::Create();
	light->SetName( L"GlobalLight" );
	light->SetType( CnLight::Global );

	Math::Vector3 position( 0.0f, 500.0f, -500.0f );
	Math::Vector3 direction = position; //Math::Vector3( -1.0f, 1.0f, -1.0f );//-position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 1.0f, 1.0f, 1.0f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	//light->SetAmbient( CnColor( 0.5f, 0.5f, 0.5f, 0.0f ) );

	CnSceneNode* lightNode = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );
	lightNode->EnableCulling( false );

#ifdef _TEST_SHADOW
	//Ptr<ShadowMap::SSM> sm = ShadowMap::SSM::Create();
	//Ptr<ShadowMap::PSM> sm = ShadowMap::PSM::Create();
	Ptr<ShadowMap::LisPSM> sm = ShadowMap::LisPSM::Create();
	//Ptr<ShadowMap::TSM> sm = ShadowMap::TSM::Create();
	//Ptr<ShadowMap::PSSM> sm = ShadowMap::PSSM::Create();

	Ptr<CnFrame> renderShadowMap = CnFrameLoader::Load( L"F_ShadowMap.xml" );
	sm->SetFrame( renderShadowMap );

	light->SetShadowBuilder( sm.Cast<ShadowMap::Builder>() );
#endif

	//Ptr<CnMesh> skybox = CnMesh::CreateBox(L"skybox", 1000, 1000, 1000);

	/// pointLights
#ifdef TEST_MULTILIGHT
	AddPointLight(0, Math::Vector3(-10.0f, 10.0f, 0.0f), CnColor(1.0f, 0.0f, 0.0f, 1.0f), 50.0f, 1.0f);
	AddPointLight(1, Math::Vector3(50.0f, 10.0f, 0.0f), CnColor(0.0f, 0.0f, 1.0f, 1.0f), 50.0f, 1.0f);
#endif

#ifdef TEST_LPP
	unsigned randomKey = 0;
	for (int p=0; p<MaxLights; ++p)
	{
		CnString lightName(L"PointLight");
		CnString lightNumber = unicode::ToString(p);
		lightName += lightNumber;

		CnLight* pointLight = CnLight::Create();
		{
			pointLight->SetName( lightName );
			pointLight->SetType( CnLight::Point );

			pointLight->SetAttenuation( 1.0f, 0.0f, 0.0f );

			float r = CnRandomNumberTable::Rand(randomKey++, 0.0f, 1.0f);
			float g = CnRandomNumberTable::Rand(randomKey++, 0.0f, 1.0f);
			float b = CnRandomNumberTable::Rand(randomKey++, 0.0f, 1.0f);
			pointLight->SetDiffuse( CnColor( r, g, b, 1.0f ) );

			float range = CnRandomNumberTable::Rand(randomKey++, 3.0f, 10.0f);
			pointLight->SetRange( range );

			//pointLight->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
			//pointLight->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );
		}
		m_PointLights[p] = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( lightName, 0xffff );
		m_PointLights[p]->AttachEntity( pointLight );
		m_PointLights[p]->EnableCulling( false );

		float x = CnRandomNumberTable::Rand(randomKey++, -10.0f, 10.0f);
		float y = CnRandomNumberTable::Rand(randomKey++, 3.0f, 20.0f);
		float z = CnRandomNumberTable::Rand(randomKey++, -10.0f, 10.0f);
		m_PointLights[p]->SetPosition(Math::Vector3( x, y, z ));
	}
#endif

//	SetData();
	SetData2();
}

void CApplication::AddPointLight(int index, const Math::Vector3& pos, const CnColor& color, float range, float intensity)
{
	CnString lightName(L"PointLight");
		
	wchar_t buff[256];
	_itow( index, buff, 10 );
	lightName += buff;

	CnLight* pointLight = CnLight::Create();
	{
		pointLight->SetName( lightName );
		pointLight->SetType( CnLight::Point );
		pointLight->SetAttenuation( 1.0f, 0.0f, 0.0f );
		pointLight->SetDiffuse( color );
		pointLight->SetRange( range );
		//pointLight->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
		//pointLight->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );
	}
	m_PointLights[index] = (CnSceneNode* )m_pSceneGraph->GetRoot()->CreateChild( lightName, 0xffff );
	m_PointLights[index]->AttachEntity( pointLight );
	m_PointLights[index]->EnableCulling( false );
	m_PointLights[index]->SetPosition(pos);
	
#if 0
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\TestPrimitive\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\TestPrimitive\\" );
	TextureMgr->AddDataPath( L"..\\Data\\TestPrimitive\\" );

	CnModelNode* modelnode = CnModelNode::Create();
	modelnode->SetName( L"sphere" );
	{
		modelnode->AddMesh( L"testSphere.mmf", L"testSphere_mtl.xml", true );
		m_PointLights[index]->AttachChild( modelnode );
		m_PointLights[index]->SetScale(Math::Vector3(10.0f, 10.0f, 10.0f));
	}
#endif
}

//----------------------------------------------------------------
/**	@brief	Data 설정
*/
void CApplication::SetData()
{
	// 아툼
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	TextureMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	// 반고
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	TextureMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	// 엘프
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	m_pAnimationDataMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\export\\" );
	TextureMgr->AddDataPath( L"..\\Data\\엘프몬스터애니 리소스\\Elf_Wizard_female\\Tex\\" );

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	TextureMgr->AddDataPath( L"..\\Data\\Ground\\" );

	MeshDataPtr spMesh;
	MtlDataPtr spMaterial;
	SkelDataPtr spSkeleton;

	CnModelNode* modelnode = NULL;

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
	m_pSceneGraph->AttachChild( modelnode );

	CnString skeleton( L"bango_rush_m.mbf" );
	CnString meshName( L"all.mmf" );
	CnString mtlName( L"newExpAll_mtl.xml" );
	CnString animName1( L"bango_m_rush_common_dance.maf" );
	CnString animName2( L"bango_m_rush_common_attack1.maf" );
	CnString animName3( L"bango_m_rush_common_run.maf" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"all.mmf", L"newExpAll_mtl.xml" );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model2" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( meshName, mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, -10.0f) );

		m_pSceneGraph->AttachChild( modelnode );
	}

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model3" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"newExpAll.mmf", mtlName );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 10.0f) );

		// Animation
		//Ptr<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, animName3.c_str() );
		//modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}
	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model4" );
	{
		spSkeleton = m_pSkeletonDataMgr->Load( skeleton.c_str() );
		modelnode->AddSkeleton( spSkeleton );
		modelnode->AddMesh( L"newExpAll.mmf", L"newExpAll2_mtl.xml" );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 20.0f) );

		// Animation
		Ptr<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, animName1.c_str() );
		//channel1->GetMotion()->Lock( L"Bip01", L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel1->GetMotion()->Lock( L"Bip01 L Calf" );
		//channel1->GetMotion()->Lock( L"Bip01 R Calf" );
		//channel1->SetMixtureRatio( 0.001f );

		//Ptr<CnAnimChannel> channel2 = modelnode->AddAnimation( 1, spAnimation2 );
		//channel2->GetMotion()->Lock( L"Bip01 Spine1" );
		//channel2->SetMixtureRatio( 0.2f );

		modelnode->GetAnimController()->Play();

		m_pSceneGraph->AttachChild( modelnode );
	}

	//CnSceneNode* rootNode = m_pSceneGraph->GetRoot();
	//CnMaterialState* mtlState = CnMaterialState::Create();
	//mtlState->SetDiffuse( CnColor(1.0f, 0.5f, 0.0f) );
	//rootNode->AttachGlobalState( mtlState );
	//rootNode->UpdateRS();
}

//----------------------------------------------------------------
/**	@brief	Data 설정
*/
void CApplication::SetData2()
{
	CnModelNode* modelnode = NULL;

#if 1
	// 마커스
	{
		m_pMeshDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		m_pMaterialDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		m_pAnimationDataMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );
		TextureMgr->AddDataPath( L"..\\Data\\DemoContent_Marcus_RigMax\\" );

		modelnode = CnModelNode::Create();
		modelnode->SetName( L"Marcus" );
		{
			//modelnode->AddSkeleton( L"marcus.mbf" );
			modelnode->AddMesh( L"marcus.mmf", L"marcus_.mtl", true);
			m_pSceneGraph->AttachChild( modelnode );

			// Animation
			//Ptr<CnAnimChannel> channel1 = modelnode->AddAnimation( 0, L"marcus.maf" );
			//modelnode->GetAnimController()->Play();
			//modelnode->SetScale( Math::Vector3(0.01f, 0.01f, 0.01f) );
			//modelnode->SetPosition( Math::Vector3( 0.0f, 0.0f, 30.0f ));

			modelnode->SetScale(Math::Vector3(0.2f, 0.2f, 0.2f));

			m_Model = modelnode;
		}
	}

	// 베룬
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\export\\" );
	TextureMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\" );
	//TextureMgr->AddDataPath( L"..\\Data\\베룬_엔진테스트용\\Texture\\2k\\" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"베룬" );
	{
#ifdef TEST_LPP
		modelnode->AddMesh( L"berun.mmf", L"berun(lightprepass).mtl", true);
#else
		modelnode->AddMesh( L"berun.mmf", L"berun(UberShader).mtl", true);
#endif
		modelnode->SetScale( Math::Vector3(0.1f, 0.1f, 0.1f) );
		modelnode->SetPosition( Math::Vector3(0.0f, 0.0f, 30.0f) );
		m_pSceneGraph->AttachChild( modelnode );
	}
#endif

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	TextureMgr->AddDataPath( L"..\\Data\\Ground\\" );

	modelnode = CnNew CnModelNode( L"ground", 0xffff );
#ifdef TEST_LPP
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid(lightprepass).xml", true );
#else
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml", true );
#endif
	m_pSceneGraph->AttachChild( modelnode );

#if 0
	// Skin
	//modelnode = CnModelNode::Create();
	//modelnode->SetName( L"skin" );
	//{
	//	modelnode->AddMesh( L"sphere.mmf", L"sphere.mtl", true );
	//	modelnode->SetPosition( Math::Vector3(0.0f, 10.0f, 0.0f) );
	//	m_pSceneGraph->AttachChild( modelnode );
	//}
	// 반고
	//m_pMeshDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//m_pMaterialDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//m_pAnimationDataMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );
	//TextureMgr->AddDataPath( L"..\\Data\\bango\\mesh_lv02\\" );

	//modelnode = CnModelNode::Create();
	//modelnode->SetName( L"model1" );
	//{
	//	SkelDataPtr spSkeleton = m_pSkeletonDataMgr->Load( L"bango_rush_m.mbf" );
	//	modelnode->AddSkeleton( spSkeleton );
	//	modelnode->AddMesh( L"all.mmf", L"newExpAll_mtl.xml" );

	//	m_pSceneGraph->AttachChild( modelnode );
	//}


	// TeamFortress2 - medic
	//m_pMeshDataMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );
	//m_pMaterialDataMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );
	//m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );
	//TextureMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );

	//modelnode = CnModelNode::Create();
	//modelnode->SetName( L"model1" );
	//{
	//	modelnode->AddSkeleton( L"medic.mbf" );
	//	modelnode->AddMesh( L"medic.mmf", L"medic.mtl" );
	//	modelnode->SetPosition(Math::Vector3(0.0f, 0.0f, -10.0f));
	//	modelnode->SetScale( Math::Vector3(0.5f, 0.5f, 0.5f) );
	//	m_pSceneGraph->AttachChild( modelnode );
	//}
	
	// Hair
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\shaderTest\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\shaderTest\\" );
	TextureMgr->AddDataPath( L"..\\Data\\shaderTest\\" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"hair" );
	{
		modelnode->AddMesh( L"hair.mmf", L"hair.mtl", true );

		m_pSceneGraph->AttachChild( modelnode );
	}
#endif
}

//----------------------------------------------------------------
/**	@brief	
*/
void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
	if (::GetAsyncKeyState(VK_LEFT))
	{
		Math::Vector3 p = m_Model->GetLocalPosition();
		m_Model->SetPosition(p + Math::Vector3(-1.0f, 0.0f, 0.0f));
	}
	if (::GetAsyncKeyState(VK_RIGHT))
	{
		Math::Vector3 p = m_Model->GetLocalPosition();
		m_Model->SetPosition(p + Math::Vector3(1.0f, 0.0f, 0.0f));
	}
	if (::GetAsyncKeyState(VK_UP))
	{
		Math::Vector3 p = m_Model->GetLocalPosition();
		m_Model->SetPosition(p + Math::Vector3(0.0f, 1.0f, 0.0f));
	}
	if (::GetAsyncKeyState(VK_DOWN))
	{
		Math::Vector3 p = m_Model->GetLocalPosition();
		m_Model->SetPosition(p + Math::Vector3(0.0f, -1.0f, 0.0f));
	}
}

//----------------------------------------------------------------
/** @brief	Application(게임에서 Logic)을 갱신한다. 
*/
void CApplication::OnUpdateFrame()
{
	//return;

#ifdef TEST_LPP
	static ulong time = 0;
	static ulong prev = 0;

	time = timeGetTime();
	if (time - prev > 2000)
	{
		for (int p=0; p<MaxLights; ++p)
		{
			float x = Math::Rand(-15.0f, 15.0f);
			float y = Math::Rand(10.0f, 50.0f);
			float z = Math::Rand(-15.0f, 15.0f);
			m_PointLights[p]->SetPosition(Math::Vector3( x, y, z ));
		}

		prev = time;
	}
#endif
}

//----------------------------------------------------------------
/**	@brief	
*/
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_SIZE:
		{
			int width = LOWORD(lParam);
			int height = HIWORD(lParam);

			//CnRenderer::Instance()->GetCurrentRenderWindow()->Resize(width, height);
		}
		break;

	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}