#pragma once

#include <Cindy/Application/CnApplication.h>
#include <Cindy/Geometry/CnMeshDataManager.h>
#include <Cindy/Material/CnMaterialDataManager.h>
#include <Cindy/Animation/CnSkeletonDataManager.h>
#include <Cindy/Animation/CnAnimationDataManager.h>

#include <Cindy/Scene/CnCameraImpl.h>
#include <Cindy/Util/CnMouse.h>
#include <Cindy/Foundation/CnMemObject.h>

class CApplication : public CnApplication
{
public:
	CApplication();
	virtual ~CApplication();

	CnRenderWindow*	AttachWindow( HWND hWnd, const wchar_t* pStrTitle,
								  int nWidth, int nHeight, ushort usColorDepth,
								  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
								  bool bSetCurrentTarget = false ) override;

	bool			Initialize() override;

	void			SetSceneGraph();

	void			SetData();
	void			SetData2();

	// MsgProc
	virtual bool	MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) override;

protected:
	virtual void	OnProcessInput() override;
	virtual void	OnUpdateFrame() override;

private:
	void	AddPointLight(int index, const Math::Vector3& pos, const CnColor& color, float range, float intensity);

	static const int	MaxLights = 32;

	MdvCamPtr			m_spCamera;

	CnMouse				m_Mouse;
	short				m_nRotX;
	short				m_nRotY;

	CnRenderWindow*			m_pRenderWindow;

	CnMeshDataManager*		m_pMeshDataMgr;
	CnMaterialDataManager*	m_pMaterialDataMgr;
	CnSkeletonDataManager*	m_pSkeletonDataMgr;
	CnAnimationDataManager*	m_pAnimationDataMgr;

	CnSceneNode*	m_PointLights[MaxLights];

	CnSceneNode*	m_Model;
};
