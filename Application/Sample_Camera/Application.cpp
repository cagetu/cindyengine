#include "stdafx.h"
#include "Application.h"

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnLight.h>
#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderGroup.h>
#include <Cindy/Material/CnTextureGroup.h>

#include <Cindy/Render/CnRenderWindow.h>
#include <Cindy/Scene/CnModelNode.h>


CApplication::CApplication()
{
	m_pMeshDataGroup = new CnMeshDataGroup();
	m_pMaterialDataGroup = new CnMaterialDataGroup();
	m_pSkeletonDataGroup = new CnSkeletonDataGroup();

}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataGroup );
	SAFEDEL( m_pMaterialDataGroup );
	SAFEDEL( m_pSkeletonDataGroup );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	CnRenderWindow* pRenderWindow = CnFramework::AttachWindow( hWnd, pStrTitle,
															   nWidth, nHeight, usColorDepth,
															   usRefreshRate, bFullScreen,
															   bThreadSafe, bSetCurrentTarget );

	// Setup Scene..
	SetScene( _T("Scene") );

	// Camera
	m_spCamera = CnNew CnModelViewCamera( L"main " );

	m_spCamera->SetPosition( CnVector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 1000.0f );
	m_spCamera->SetRadius( 10.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( CnVector3( 0.0f, 0.0f, 0.0f ) );
	m_spCamera->SetRestrictAngle( 0.1f );

	// viewport 1
	CnViewport* pViewport = pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnNew CnLight( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	CnVector3 position( 10.0f, 10.0f, -10.0f );
	CnVector3 direction = position;
	direction.Normalize();

	light->SetDirection( direction );
	light->SetDiffuse( CnColor( 0.8f, 0.8f, 0.8f, 1.0f ) );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.0f, 0.0f, 0.0f, 0.0f ) );

	CnSceneNode* lightNode = CnNew CnSceneNode( L"globalLight", 0xffff );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );

	m_pSceneGraph->AttachChild( lightNode );

	SetData();

	return pRenderWindow;
}

void CApplication::SetData()
{
	CnShaderGroup::Instance()->AddDataPath( L"..\\..\\sdk\\Shader\\" );
	
	CnTextureGroup::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnTextureGroup::Instance()->AddDataPath( L"..\\Data\\" );

	m_pMeshDataGroup->AddDataPath( L"..\\Data\\" );
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\PhysXShape\\" );
	m_pMeshDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\PhysXShape\\" );
	m_pMaterialDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	m_pSkeletonDataGroup->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	MeshDataPtr spMesh;
	MtlDataPtr spMaterial;
	SkelDataPtr spSkeleton;
	CnModelNode* modelnode = NULL;

	// model
	modelnode = CnNew CnModelNode( L"atoom1", 0xffff );
	{
		spMesh = m_pMeshDataGroup->Load( L"atoom.mmf" );
		spMaterial = m_pMaterialDataGroup->Load( L"atoom_mtl.xml" );
		spSkeleton = m_pSkeletonDataGroup->Load( L"atoom.mbf" );

		modelnode->AttachMesh( spMesh );
		modelnode->AttachMaterial( spMesh->GetName(), spMaterial );
		modelnode->AttachSkeleton( spSkeleton );

		m_pSceneGraph->GetRoot()->AttachChild( modelnode );
	}


	modelnode = CnNew CnModelNode( L"sphere", 0xffff );
	{
		spMesh = CnMeshDataGroup::Instance()->Load( L"testSphere.mmf" );
		spMaterial = CnMaterialDataGroup::Instance()->Load( L"testSphere_mtl.xml" );
		
		modelnode->AttachMesh( spMesh );
		modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

		m_pSceneGraph->AttachChild( modelnode );

		modelnode->Scale( CnVector3( 0.1f, 0.1f, 0.1f ) );
	}

	// Ground Grid
	modelnode = CnNew CnModelNode( L"grid", 0xffff );
	{
		spMesh = CnMeshDataGroup::Instance()->Load( L"base_grid.mmf" );
		spMaterial = CnMaterialDataGroup::Instance()->Load( L"base_grid.xml" );

		modelnode->AttachMesh( spMesh );
		modelnode->AttachMaterial( spMesh->GetName(), spMaterial );

		m_pSceneGraph->AttachChild( modelnode );
	}
}

#define GAP_PLAYER_CAMERA	3.0f

void CApplication::Run()
{
	if (::GetAsyncKeyState('W'))
	{
		CnVector3 dir;
		m_spCamera->GetDirection( dir );
		
		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );

		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"sphere" );
		if (node)
		{
			CnVector3 pos = lookAt + dir;
			pos.y -= GAP_PLAYER_CAMERA;
			node->SetPosition( pos );
		}
	}
	if (::GetAsyncKeyState('S'))
	{
		CnVector3 dir;
		m_spCamera->GetDirection( dir );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );

		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"sphere" );
		if (node)
		{
			CnVector3 pos = lookAt - dir;
			pos.y -= GAP_PLAYER_CAMERA;
			node->SetPosition( pos );
		}
	}
	if (::GetAsyncKeyState('A'))
	{
		CnVector3 right;
		m_spCamera->GetRight( right );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );

		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"sphere" );
		if (node)
		{
			CnVector3 pos = lookAt + right;
			pos.y -= GAP_PLAYER_CAMERA;
			node->SetPosition( pos );
		}
	}
	if (::GetAsyncKeyState('D'))
	{
		CnVector3 right;
		m_spCamera->GetRight( right );

		CnVector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );

		CnNode* node = m_pSceneGraph->GetRoot()->GetChild( L"sphere" );
		if (node)
		{
			CnVector3 pos = lookAt - right;
			pos.y -= GAP_PLAYER_CAMERA;
			node->SetPosition( pos );
		}
	}

	CnFramework::Run();
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.02f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}