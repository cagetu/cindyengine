#include "stdafx.h"
#include "Application.h"

#include <Cindy/Render/Base/CnRenderWindow.h>

#include <Cindy/Scene/CnSceneGraph.h>
#include <Cindy/Scene/CnModelNode.h>
#include <Cindy/Scene/CnLight.h>

#include <Cindy/Material/CnTexture.h>
#include <Cindy/Material/CnShaderManager.h>
#include <Cindy/Material/CnTextureManager.h>

CApplication::CApplication()
{
	m_pMeshDataMgr = CnMeshDataManager::Create();
	m_pSkeletonDataMgr = CnSkeletonDataManager::Create();
	m_pMaterialDataMgr = CnMaterialDataManager::Create();
}
CApplication::~CApplication()
{
	SAFEDEL( m_pMeshDataMgr );
	SAFEDEL( m_pMaterialDataMgr );
	SAFEDEL( m_pSkeletonDataMgr );
}

CnRenderWindow* CApplication::AttachWindow( HWND hWnd, const wchar_t* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
										    ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
{
	SetLogFile( L"Log.txt", L"", true, true );

	m_pRenderWindow = CnApplication::AttachWindow( hWnd, pStrTitle,
												   nWidth, nHeight, usColorDepth,
												   usRefreshRate, bFullScreen,
												   bThreadSafe, bSetCurrentTarget );

	return m_pRenderWindow;
}

bool CApplication::Initialize()
{
	bool result = CnApplication::Initialize();

	// Setup Scene..
	SetScene( _T("Scene") );

	// Camera
	m_spCamera = CnModelViewCamera::Create();
	m_spCamera->SetName( L"main " );

	m_spCamera->SetPosition( Math::Vector3( 0.0f, 7.0f, -50.0f ) );
	m_spCamera->SetFar( 1000.0f );
	m_spCamera->SetRadius( 1.0f, 500.0f );
	m_spCamera->SetRadius( 50.0f );
	m_spCamera->SetLookAt( Math::Vector3( 0.0f, 7.0f, 0.0f ) );

	// viewport 1
	CnViewport* pViewport = m_pRenderWindow->AddViewport( 0, 0.0f, 0.0f, 1.0f, 1.0f );
	pViewport->SetCamera( m_spCamera.Cast<CnCamera>() );
	pViewport->SetScene( m_pWorld );
	pViewport->SetBackgroundColor( D3DCOLOR_RGBA( 100, 100, 100, 0 ) );

	// Light
	CnLight* light = CnLight::Create();
	light->SetName( L"GlobalLight" );
	light->SetType( CnLight::Directional );

	Math::Vector3 position( 5.0f, 20.0f, -10.0f );
	Math::Vector3 direction = position;
	direction.Normalize();

	CnColor diffuse( 1.0f, 200.0f/255.f, 100.0f/255.0f, 0.1f );

	light->SetDirection( direction );
	light->SetDiffuse( diffuse );
	light->SetSpecular( CnColor( 1.0f, 1.0f, 1.0f, 0.0f ) );
	light->SetAmbient( CnColor( 0.1f, 0.1f, 0.1f, 0.0f ) );

	CnSceneNode* lightNode = CnSceneNode::Create();
	lightNode->SetName( L"globalLight" );
	lightNode->SetID( INVALID_NODE_ID );
	lightNode->SetPosition( position );
	lightNode->AttachEntity( light );

	m_pSceneGraph->AttachChild( lightNode );

	SetData();
	return true;
}

void CApplication::SetData()
{
	ShaderMgr->AddDataPath( L"..\\..\\sdk\\Shader\\" );

	m_pMeshDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\creation_sh_f_lv00\\" );

	Pointer<CnModelNode> modelnode = NULL;

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model1" );
	modelnode->AddMesh( L"newAtoom.mmf", L"newAtoom_mtl.xml" );
	modelnode->SetPosition( Math::Vector3( 0.0f, 0.0f, 1.0f ) );
	//m_pSceneGraph->AttachChild( modelnode );

	// TeamFortress2 - medic
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );
	m_pSkeletonDataMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );
	TextureMgr->AddDataPath( L"..\\Data\\TF2\\medic\\" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"model2" );
	{
		modelnode->AddSkeleton( L"medic.mbf" );
		modelnode->AddMesh( L"medic.mmf", L"medic.mtl" );
		modelnode->SetPosition( Math::Vector3(10.0f, 0.0f, 0.0f) );
		modelnode->SetScale( Math::Vector3(0.3f, 0.3f, 0.3f) );
		m_pSceneGraph->AttachChild( modelnode );
	}

	// ground
	m_pMeshDataMgr->AddDataPath( L"..\\Data\\Ground\\" );
	m_pMaterialDataMgr->AddDataPath( L"..\\Data\\Ground\\" );	
	CnTextureManager::Instance()->AddDataPath( L"..\\Data\\Ground\\" );

	modelnode = CnModelNode::Create();
	modelnode->SetName( L"ground" );
	modelnode->AddMesh( L"base_grid.mmf", L"base_grid.xml" );
	//m_pSceneGraph->AttachChild( modelnode );
}

void CApplication::OnProcessInput()
{
	if (::GetAsyncKeyState('W'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );
		
		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + dir );
	}
	if (::GetAsyncKeyState('S'))
	{
		Math::Vector3 dir;
		m_spCamera->GetDirection( dir );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - dir );
	}
	if (::GetAsyncKeyState('A'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt + right );
	}
	if (::GetAsyncKeyState('D'))
	{
		Math::Vector3 right;
		m_spCamera->GetRight( right );

		Math::Vector3 lookAt = m_spCamera->GetLookAt();
		m_spCamera->SetLookAt( lookAt - right );
	}
}

//================================================================
bool CApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		{
			m_nRotX = LOWORD(lParam);
			m_nRotY = HIWORD(lParam);

			m_Mouse.Press( CnMouse::Left );
		}
		break;
	case WM_LBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Left );
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Right );
		}
		break;

	case WM_RBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Right );
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_Mouse.Press( CnMouse::Middle );
		}
		break;
	case WM_MBUTTONUP:
		{
			m_Mouse.Release( CnMouse::Middle );
		}
		break;

	case WM_MOUSEMOVE:
		{
			short x = LOWORD(lParam);
			short y = HIWORD(lParam);

			static const float CAM_DELTA = 0.09f;
			static const float MOUSE_VALUE = 0.0003f;

			if (m_Mouse.IsPressed( CnMouse::Left ))
			{
				float xRot = (float)(x - m_nRotX) * MOUSE_VALUE;
				float yRot = (float)(y - m_nRotY) * MOUSE_VALUE;

				float angle = yRot * m_Mouse.GetSensitive();
				if (angle < -CAM_DELTA)
				{
					angle = -CAM_DELTA;
				}
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::X );

				angle = xRot * m_Mouse.GetSensitive();
				m_spCamera->Rotate( angle, CnModelViewCamera::Axis::Y );

				m_nRotX = x;
				m_nRotY = y;
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			float delta = (short)HIWORD(wParam) * 0.01f;

			float radius, min, max;
			m_spCamera->GetRadius( min, max, radius );

			radius += delta;
			m_spCamera->SetRadius( radius );
		}
		break;
	}

	return false;
}