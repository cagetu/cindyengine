#line 1 "pe_lightrays.fx"
//-------------------------------------------------------------
/**	pe_lightrays.fx : sunlight volumic light rays

	reference : ShaderX6 - 5.2. Sunlight with Volumetric Light Rays

	Copyright ?2008 cagetu, Inc. All Rights Reserved.
*/
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"

//-------------------------------------------------------------
//	shader variables
//-------------------------------------------------------------
float4x4 gWorldViewProj		: ModelViewProjection;
float4x4 gViewProj			: ViewProjection;

float4 gSunLightPosition	: SunLightPosition;
float4 gSunLightColor		: SunLightColor = {1.0f, 1.0f, 1.0f, 1.0f};
float gLightRayIntensity	: LightRayIntensity = 0.5f;
float gTimeValue			: TimeValue;

#define NumSamples_2	8
#define NumSamples		(NumSamples_2 * 2)
#define Step			(1.0f/NumSamples)	/// radial glow step
#define ScaleFactor		-2.5f				/// texcoord scale factor

//-------------------------------------------------------------
//	Texture Samplers
//-------------------------------------------------------------
texture gDiffuseMap0 : DiffuseMap0;
sampler DiffuseSampler = sampler_state
{
   Texture = <gDiffuseMap0>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   //MipMapLodBias = <MipLodBias>;
};

texture gDiffuseMap1 : DiffuseMap1;
sampler GlowSampler = sampler_state
{
   Texture = <gDiffuseMap1>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   //MipMapLodBias = <MipLodBias>;
};

//-------------------------------------------------------------
//	structures
//-------------------------------------------------------------
struct VertexInput
{
    float4 position : POSITION;
    float2 uv : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
};

//------------------------------------------------------------------------------
/**
	scale texcoords
*/
float2 NTexcoord( float2 Texcoord, int index, float3 c )
{
	/// calculate scale factor
	float Scale = sqrt(index) * c.z + 1.0;
	/// perform texcoords scaling
    return ( Texcoord.xy - c.xy ) * Scale + c.xy;
}

//------------------------------------------------------------------------------
/**
*/
VertexOutput vsRadialGlowMask( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;

	output.position = mul(Input.position, gWorldViewProj);
	// screen space[-1, 1]로 sun 포지션 변환
	float4 sunPos = mul(gSunLightPosition, gViewProj);
	float2 ts = float2(sunPos.x / sunPos.w, -sunPos.y / sunPos.w);

	output.uv0 = Input.uv;

	// glow mask에 대한 texture coordinates를 결정하기 위해
	// screen space안에서 sun position을 사용한다.
	output.uv1 = Input.uv - 0.5f * ts.xy;
	return output;
}

//------------------------------------------------------------------------------
/**
*/
float4 psRadialGlowMask( VertexOutput Input ) : COLOR
{
	float4 output = (float4)0;
	
	float4 sceneColor = tex2D( DiffuseSampler, Input.uv0 );
	float4 glowMaskColor = tex2D( GlowSampler, Input.uv1 );
	
	// compute pixel luminance
	//float lum = dot( sceneColor.xyz, 1.0f/3 );
	float lum = dot( sceneColor.xyz, luminance.xyz );
	
	// lighting contrast 적용
	sceneColor *= lum;

	// rendertarget과 mask의 조합
	output = sceneColor * glowMaskColor;
	return output;
}

//-------------------------------------------------------------
//	structures
//-------------------------------------------------------------
struct VS2PS
{
    float4 position : POSITION;
    float4 uv0[NumSamples_2] : TEXCOORD0;
};

//------------------------------------------------------------------------------
/**
*/
VS2PS vsRadialGlowLuminance( const VertexInput Input )
{
	VS2PS output = (VS2PS)0;
	output.position = mul(Input.position, gWorldViewProj);

	// screen space[-1, 1]로 sun 포지션 변환
	float4 sunPos = mul(gSunLightPosition, gViewProj);

	// screen space에서 sun position을 결정하고 texcoord scale factor
	float3 ts = float3( (sunPos.x / sunPos.w) * 0.5f + 0.5f,
						(-sunPos.y / sunPos.w) * 0.5f + 0.5f,
						ScaleFactor / NumSamples );

	// n = NumSamples, sun radial glow를 위해 매핑들을 차이를 계산
	// (pixel과 sun position과의 선을 만든다?!)
	int j = 0;
	for (int i = 0; i < NumSamples_2; i++)
	{
		output.uv0[i].xy = NTexcoord(Input.uv.xy, j, ts);
		j++;
		output.uv0[i].zw = NTexcoord(Input.uv.xy, j, ts);
		j++;
	}
	return output;
}

//------------------------------------------------------------------------------
/**
*/
float4 psRadialGlowLuminance( VS2PS Input ) : COLOR
{
	float4 output = (float4)0;
	
	float lum = 1.0f;
	
	// mapping들의 차이를 이용하여 radial glow buffer를 샘플링하고,
	// luminance를 감소 시킨다.
	for (int i = 0; i < NumSamples_2; i++)
	{
		output += tex2D( DiffuseSampler, Input.uv0[i].xy ) * lum;
		lum -= Step;
		output += tex2D( DiffuseSampler, Input.uv0[i].zw ) * lum;
		lum -= Step;		
	}
	output *= 0.25f;
	return output;
}

//------------------------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
VertexOutput vsFinal( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;
	
	output.position = mul(Input.position, gWorldViewProj);
	output.uv0 = Input.uv;
	
	return output;
}

//------------------------------------------------------------------------------
/** Final Scene
	@brief	장면을 랜더링한다.
*/
float4 psFinal( VertexOutput Input ) : COLOR
{
	float4 color = tex2D( DiffuseSampler, Input.uv0 );
	float4 glow = tex2D( GlowSampler, Input.uv0 );
	glow *= gSunLightColor;
	
#ifdef _SRGB_
	//color = sRGBtolinearRGB( color );
#endif
	
	float flicker = gTimeValue * 0.001f;	// 0.5f;	// 
	float f = 0.5f * (1.0f + sin(flicker)) * gLightRayIntensity;
	
	//lerp을 하면, lightRay가 드리워지지 않는 부분은 어두워진다.
	//add 연산을 하면, 비유에 따라 색상을 추가해줄 수 있다. -> HDR 텍스쳐를 이용해야 할 듯...
	color = lerp(color, glow, f);
	//color += glow * f;
	color = glow;
#ifdef _SRGB_
	//color = linearRGBtosRGB(color);
#endif
	return color;
}

//-------------------------------------------------------------
//	Techniques
//-------------------------------------------------------------
technique RadialGlowMask
{
	Pass p0
	{
		VertexShader = compile vs_3_0 vsRadialGlowMask();
		PixelShader = compile ps_3_0 psRadialGlowMask();
	}
}

technique RadialGlowLuminance
{
	Pass p0
	{
		VertexShader = compile vs_3_0 vsRadialGlowLuminance();
		PixelShader = compile ps_3_0 psRadialGlowLuminance();
	}
}

// scene
technique FinalScene
{
	Pass Default
	{
		VertexShader = compile vs_3_0 vsFinal();
		PixelShader = compile ps_3_0 psFinal();
	}
}