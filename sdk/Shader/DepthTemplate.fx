#line 1 "DepthTemplate.fx"
//-------------------------------------------------------------
//	Depth.fx : ���� ������
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Skinning.fxh"
#include "..\\..\\sdk\\Shader\\DefaultSamplers.fxh"

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
float4x4 gWorldViewProj : ModelViewProjection;
float4x4 gWorldView : ModelView;
float4 gViewInfo : ViewInfo;

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
struct DepthVertexInput
{
    float4 position : POSITION0;
#ifdef _DIFFUSE_MAP_
    float2 uv0      : TEXCOORD0;
#endif
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif    
};

struct DepthVertexOutput
{
	float4 position		: POSITION0;
	float2 viewDepth	: TEXCOORD0;
	float2 posDepth		: TEXCOORD1;
#ifdef _DIFFUSE_MAP_
	float2 uv0			: TEXCOORD2;
#endif	
};

//------------------------------------------------------------------------------
/**
    Vertex shader function for the depth pass.
    Passes depth to pixelshader.    
*/
DepthVertexOutput DepthVertexShader( DepthVertexInput Input )
{
    DepthVertexOutput Output;

	float4 localPos = 0;
#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
#else
	localPos = Input.position;
#endif
	float4 posClip = TransformPosition(localPos, gWorldViewProj);
	Output.position = posClip;	// / posClip.w;

	// Depth
	float4 posView = TransformPosition(localPos, gWorldView);
	Output.viewDepth.x = posView.z;
	Output.viewDepth.y = posView.w;
	Output.posDepth.x = posClip.z;
	Output.posDepth.y = posClip.w;

#ifdef _DIFFUSE_MAP_
	Output.uv0 = Input.uv0;	//UnpackUv(Input.uv0);
#endif

    return Output;
}

//------------------------------------------------------------------------------
/**
*/
float4 DepthPixelShader( const DepthVertexOutput Input ) : COLOR
{
	float4 Output = float4(1,1,1,1);
	
	//float sceneDepth = NormalizedLinearDepth(Input.viewDepth.x, gViewInfo.z, gViewInfo.w);	// Input.depth.x
	//Output = float4(EncodeDepth(sceneDepth), Input.posDepth.x, Input.posDepth.y);	// float4(sceneDepth, sceneDepth, Input.posDepth.x, 1.0f);
	//Output = float4(EncodeDepth(Input.viewDepth.y), 0.0f, 1.0f);	// float4(sceneDepth, sceneDepth, Input.posDepth.x, 1.0f);

	//Output = float4(EncodeDepth(Input.posDepth.x/Input.posDepth.y), Input.posDepth.x, ComputeDepthBlur(Input.viewDepth.x));
	//Output = float4(EncodeDepth(Input.posDepth.x/Input.posDepth.y), EncodeDepth(Input.viewDepth.x));
	Output = float4(EncodeDepth(Input.viewDepth.x), 0.0f, 1.0f);

#ifdef _DIFFUSE_MAP_	
	float4 albedo = tex2D( DiffuseMapSampler0, Input.uv0 );
	Output.a = albedo.a;
#endif

	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p0
	{
		VertexShader = compile vs_3_0 DepthVertexShader();
		PixelShader	= compile ps_3_0 DepthPixelShader();
	}
}

