#line 1 "Toon.fx"
//-------------------------------------------------------------
//	Toon.fx : Toon ������
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\lib.fx"
#include "..\\..\\sdk\\Shader\\lib_lights.fx"

//-------------------------------------------------------------
// vertex shader
//-------------------------------------------------------------
float4x4  matWorldViewProjection 	: ModelViewProjection;

texture diffuseMap			: DiffuseMap0;
texture toonMap				: ToonMap;

float4 lightPosition : LightPosition;
//float4 lightDiffuse : LightDiffuse;
//float4 globalAmbient : GlobalAmbient;

float3 eyePosition : EyePosition;

struct VS_INPUT 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
};

struct VS_OUTPUT 
{
	float4 pos : POSITION0;
	float2 tex0 : TEXCOORD0; 
	float2 tex1 : TEXCOORD1;	
	float4 rim : TEXCOORD2;
};

VS_OUTPUT vsToonShade( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );
	Output.tex0 = Input.tex0;

	float3 lightDir = GetLightDir( Input.pos, lightPosition );
	float intensity = Lambert( Input.norm, lightDir );
	Output.tex1.x = intensity;
	Output.tex1.y = 0.5f;
	
	return( Output );
}

VS_OUTPUT vsToonShade_RimLight( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );
	Output.tex0 = Input.tex0;

	float3 lightDir = GetLightDir( Input.pos, lightPosition );
	float intensity = Lambert( Input.norm, lightDir );
	Output.tex1.x = intensity;
	Output.tex1.y = 0.5f;

	// rim
	float3 eyeVec = eyePosition - Input.pos.xyz;
	eyeVec = normalize( eyeVec );
	float4 rimLight = Rim( Input.norm, eyeVec, lightDir, 5.0f, float4( 1.0f, 1.0f, 1.0f, 0.0f ) );
	Output.rim = rimLight;
	
	return( Output );
}

VS_OUTPUT vsEdgeStretch( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	float4 pos = mul( Input.pos, matWorldViewProjection );
	
	float4 n = float4( Input.norm, 1.0f );
	n = mul( n, matWorldViewProjection );
	n.zw = 0;
	
	pos = pos + 0.01f*n;
	Output.pos = pos;
	Output.tex0 = Input.tex0;
	
	return Output;
}

//-------------------------------------------------------------
// Pixel Shader
//-------------------------------------------------------------
sampler diffuseMap_Sampler = sampler_state
{
   Texture = <diffuseMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

sampler toonMap_Sampler = sampler_state
{
	Texture = <toonMap>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

struct PS_INPUT 
{
	float2	tex0 : TEXCOORD0;
	float2	tex1 : TEXCOORD1;
	float4	rim : TEXCOORD2;
};

float4 psToonShade( PS_INPUT Input ) : COLOR0
{
	float4 mapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	float4 result = mapColor;

	float4 toonMap = tex1D( toonMap_Sampler, Input.tex1 );
	result *= toonMap;
	
	return result;
}

float4 psToonShade_RimLight( PS_INPUT Input ) : COLOR0
{
	float4 mapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	float4 result = mapColor;

	float4 toonMap = tex1D( toonMap_Sampler, Input.tex1 );
	result *= toonMap;

	result += Input.rim;
	
	return result;
}

float4 psEdge( PS_INPUT Input ) : COLOR0
{
	float4 mapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	return float4(0, 0, 0, mapColor.a);
}

//--------------------------------------------------------------//
// Technique Section for Effect Workspace.
//--------------------------------------------------------------//
technique Geometry
{
	pass Default
	{
		VertexShader = compile vs_2_0 vsToonShade();
		PixelShader	= compile ps_2_0 psToonShade();
	}

	pass Edge
	{
		CULLMODE = CW;	
		
		VertexShader = compile vs_2_0 vsEdgeStretch();
		PixelShader	= compile ps_2_0 psEdge();
	}
}

// ToonShade + RimLighting
technique Toon_Rim
{
	pass Default
	{
		VertexShader = compile vs_2_0 vsToonShade_RimLight();
		PixelShader	= compile ps_2_0 psToonShade_RimLight();
	}

	pass Edge
	{
		CULLMODE = CW;	
		
		VertexShader = compile vs_2_0 vsEdgeStretch();
		PixelShader	= compile ps_2_0 psEdge();
	}
}

