#ifndef __SAMPLING_FXH__
#define __SAMPLING_FXH__

//-------------------------------------------------------------
//	Sampling.fxh : 샘플링 하는 함수 모음
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

float2 g_aSampleOffsets[16] : SampleOffsets;
float2 g_aSampleWeights[16] : SampleWeights;

#endif  // __SAMPLING_FXH__