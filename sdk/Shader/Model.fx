#line 1 "Model.fx"
//-------------------------------------------------------------
//	Model.fx : 기본 Gemetry
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"

//-------------------------------------------------------------
// vertex shader
//-------------------------------------------------------------
float4x4  matWorldViewProjection 	: ModelViewProjection;
float4x4  matWorldView			: ModelView;

texture diffuseMap			: DiffuseMap0;
texture shadowMap			: ShadowMap;
texture cubeMap0			: CubeMap0;

float4 lightPosition : LightPosition;
float4 lightDiffuse : LightDiffuse;

float3 eyePosition : EyePosition;

float4 globalAmbient : GlobalAmbient;

// direction of light from sky (view space)
float3 DirFromSky < string UIDirectional = "Direction from Sky"; > = { 0.0f, -1.0f, 0.0f };            

//-------------------------------------------------------------
struct VS_INPUT 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;	
};

struct VS_OUTPUT 
{
	float4 pos : POSITION0;
	float4 diff : COLOR0; 
	float2 tex0 : TEXCOORD0; 
	float3 normal : TEXCOORD1;
	float3 eyeVec : TEXCOORD2;
	float3 lightDir : TEXCOORD3;
};

//-------------------------------------------------------------
VS_OUTPUT vsModel( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	float4 position = TransformPosition( Input.pos, matWorldViewProjection );
	float3 normal = TransformNormal( Input.norm, matWorldViewProjection );

	Output.pos = position;
	Output.tex0 = UnpackUv(Input.tex0);
	//Output.tex0 = Input.tex0;

	float3 inputNormal = normalize( UnpackNormal(Input.norm) );	// Input.norm;	// 
	Output.normal = inputNormal;

	// 
	float3 eye = normalize(eyePosition - Input.pos.xyz);
	Output.eyeVec = eye;
	
	// 
	float3 lightDir = LightDirection( Input.pos, lightPosition );
	Output.lightDir = lightDir;

	// diffuse
	float diffuseIntensity = HalfLambert( inputNormal, lightDir, 2 );

	// 그림자 색상과 보간을 하여, 부드럽게 넘어가도록 하는 방법...
	//static float4 GlobalBackLightColor = { 0.0f, 0.0f, 0.0f, 0.0f };    // ground	
	//float4 diffuse = lerp( GlobalBackLightColor, lightDiffuse, diffuseIntensity );
	//diffuse *= Input.diff * matDiffuse;

	float4 diffuse = lightDiffuse * Input.diff * matDiffuse * diffuseIntensity;

	// ambient
	float4 ambient = globalAmbient * matAmbient;

	// emissive
	float4 emissive = matEmissive;

	// 반구 조명..
	float4 hemisphere = Hemisphere( normal, -DirFromSky, 0.0f );
	hemisphere = 0.0f;

	// result
	Output.diff = diffuse + ambient;// + ambient + specular + emissive + hemisphere;
	//Output.diff = hemisphere;
	Output.diff.a = Input.diff.a;

	return( Output );
}

VS_OUTPUT vsUnlit( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );
	Output.tex0 = Input.tex0;

	return( Output );
}

//-------------------------------------------------------------
// Pixel Shader
//-------------------------------------------------------------
sampler diffuseMap_Sampler = sampler_state
{
   Texture = <diffuseMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler shadowMap_Sampler = sampler_state
{
   Texture = <shadowMap>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   ADDRESSW = CLAMP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler cubeMap0_Sampler = sampler_state
{
   Texture = <cubeMap0>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   ADDRESSW = CLAMP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

//-------------------------------------------------------------
struct PS_INPUT 
{
	float4 diff : COLOR0;
	float2 tex0 : TEXCOORD0;
	float3 normal : TEXCOORD1;
	float3 eyeVec : TEXCOORD2;
	float3 lightDir : TEXCOORD3;
};

float4 psUnlit( PS_INPUT Input ) : COLOR
{
	float4 result = (float4)0;
	
	float4 diffuseMapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	
	result = diffuseMapColor;
	return result;
}

//-------------------------------------------------------------
float4 psModel( PS_INPUT Input ) : COLOR
{
	float4 result = (float4)0;

	//float3 N = normalize( Input.normal );	
	//float3 V = normalize( Input.eyeVec );
	float3 N = Input.normal;
	float3 V = Input.eyeVec;
	float3 L = Input.lightDir;

	// Rim Light
	float4 rimLight = Rim( N, V, L, 16 );

	// specular
	float specIntensity = Blinn( V, L, N, 16 );
	float4 specular = float4( specIntensity * matSpecular.rgb, 0 );
	
	// Ambient
	//float4 envMap = texCUBE( cubeMap0_Sampler, N );
	//float3 ambient = AmbientLight( N, envMap );

	//float3 diffuseLight = saturate( Input.diff.xyz + rimLight ) + envMap.xyz;
	float3 diffuseLight = Input.diff.xyz + rimLight;// + envMap.xyz;

	// Diffuse
	float4 diffuseMapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	float4 diffuse = diffuseMapColor * float4( diffuseLight, Input.diff.a );
	//float4 diffuse = diffuseMapColor * float4( Input.diff.xyz, Input.diff.a );

	// Composite
	//result = diffuse + globalAmbient + rimLight;
	//result = diffuse + rimLight;
	result = diffuse + specular;

	return result;
}

//--------------------------------------------------------------//
// Technique Section for Effect Workspace.
//--------------------------------------------------------------//
technique Geometry < string Mask = "Solid"; >
{
	pass NormalPass
	{
		VertexShader = compile vs_2_0 vsModel();
		PixelShader = compile ps_2_0 psModel();
	}
}

technique NoShade < string Mask = "Solid"; >
{
	pass NormalPass
	{
		VertexShader = compile vs_1_1 vsUnlit();
		PixelShader	= compile ps_2_0 psUnlit();
	}
}
