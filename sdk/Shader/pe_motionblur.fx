//******************************************************************************
/**	@author		cagetu
	@brief		Object Motion Blur Shader
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
shared float4x4 gWorldViewProj : ModelViewProjection;
float4x4 gInvViewProj : ViewProjInv;
float4x4 gPrevViewProj : PrevViewProj;

texture gDiffuseMap0 : DiffuseMap0;
sampler DepthSampler = sampler_state
{
   Texture = <gDiffuseMap0>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   //MipMapLodBias = <MipLodBias>;
};

texture gDiffuseMap1 : DiffuseMap1;
sampler SceneSampler = sampler_state
{
   Texture = <gDiffuseMap1>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   //MipMapLodBias = <MipLodBias>;
};

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// 입력
struct VertexInput
{
	float4 position : POSITION0;
	float2 texCoord : TEXCOORD0;
};
/// 출력
struct VertexOutput
{
	float4 position : POSITION0;
	float2 texCoord : TEXCOORD0;
};

//------------------------------------------------------------------------------
/**
*/
VertexOutput vsMain( const VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;
	Output.position = mul(Input.position, gWorldViewProj);
	Output.texCoord = Input.texCoord;

	return Output;
}

//------------------------------------------------------------------------------
/**	GPUGems3권
*/
float4 psMain( const VertexOutput Input ) : COLOR0
{
	float4 Output = float4(1.0f, 1.0f, 1.0f, 1.0f);
	
	float2 texCoord = Input.texCoord;

	/** depthBuffer
		x, y : viewSpaceDepth
		z : posClip.z / posClip.w (0~1)
	*/
	float4 depthBuffer = tex2D(DepthSampler, texCoord);
	float zOverW = DecodeDepth(depthBuffer);

	/// H 는 -1에서 1 사이에서 이 Pixel에 viewport 위치이다. (non-homogeneous)
	float4 H = float4(texCoord.x * 2 - 1, (1 - texCoord.y) * 2 - 1, zOverW, 1);  

	/// worldPos * viewProj = hPos;
	/// depth = hPos.z / hPos.w;	(0.0f ~ 1.0f)
	/// worldPos = hPos * viewProjInv;

	/// view-Projection inverse에 의해 변환
	float4 D = mul(H, gInvViewProj);

	/// world 위치를 얻기 위해, w로 나눈다.
	float4 worldPos = D / D.w;

	/// 현재 viewport position
	float4 currentScreenPos = H;

	// world position을 사용하여, 이전의 view-projection matrix에 의해 변환.
	// w로 나누어서 [-1,1]의 nonhomogeneous 좌표로 변환한다.
	float4 previousScreenPos = mul(worldPos, gPrevViewProj);
	previousScreenPos /= previousScreenPos.w;

	// 이 프레임에 위치와 마지막 프레임의 위치를 사용하여, pixel velocity를 계산한다.
	// velocity는 [-2, 2]의 값이다. 따라서, [-1, 1]사이의 값을 만들어주려면, 2로 나눈다.
	float2 velocity = currentScreenPos - previousScreenPos;
	velocity /= 2.0f;

	return float4(velocity, 0.0f, 1.0f);
	
	const int numSamples = 2;

	float4 color = tex2D(SceneSampler, texCoord);
	texCoord += velocity;
	for (int i=1; i < numSamples; i++, texCoord += velocity)
	{
		/// 색상의 총합을 구한다.
		float4 currentColor = tex2D(SceneSampler, texCoord);
		/// 색상 누적...
		color += currentColor;
	}

	/// 최족 Blur 색상을 얻기 위해 샘플의 평균을 구한다.
	Output = color / numSamples;
	return Output;
}

//------------------------------------------------------------------------------
// Technique Section for Effect Workspace.
//------------------------------------------------------------------------------
technique t0
{
	pass p0
	{
		VertexShader = compile vs_3_0 vsMain();	// null;
		PixelShader = compile ps_3_0 psMain();
	}
}