#line 1 "Hdr.fx"
//-------------------------------------------------------------
//	Hdr.fx : 기본 Gemetry
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

texture diffuseMap : DiffuseMap;

sampler diffuseMap_Sampler = sampler_state
{
	Texture = <diffuseMap>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
};

float2 g_aSampleOffsetsWeights[16];

//==================================================================
/** DownFilter
	@remarks	4x4만큼 축소 필터링 한다.
*/
//==================================================================
float4 psDownFilter4x4( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;

	for (int i=0; i<16; ++i)
		color += tex2D( diffuseMap_Sampler, inTex + g_aSampleOffsetsWeights[i] );
		
	return color/16;
}

float brightPassThreshold;
float brightPassOffset;

//==================================================================
/** BrightPassFilter
	@remarks	밝은 부분을 추출한다.
*/
//==================================================================
float4 psBrightPassFilter( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 sample = tex2D( diffuseMap_Sampler, inTex );

	// 어두운 픽셀들을 제외 시킨다.
	sample.rgb -= brightPassThreshold;

	// 하한값을 0으로
	sample.rgb = max(sample.rgb, 0.0f);

	// map value 0~1
	sample.rgb /= (brightPassOffset+sample.rgb);

	return sample;
}

// The per-color weighting to be used for luminance calculations in RGB order.
static const float3 luminanceVector = float3(0.2125f, 0.7154f, 0.0721f);

//==================================================================
/** SampleLuminanceAverageLog
	@remarks	소스 이미지의 밝은 부분을 추출하고, log()의 평균이
				포함된 scaled image를 반환한다.
*/
//==================================================================
float4 psSampleLuminanceAverageLog( in float2 inTex : TEXCOORD0 ) :COLOR
{
	float3 sample = 0;
	float logLuminanceSum = 0;

	// sample Point를 통해, log(luminace)의 합을 계산한다.
	for (int i=0; i < 9; i++)
	{
		sample = tex2D( diffuseMap_Sampler, inTex + g_aSampleOffsetsWeights[i] );
		logLuminanceSum += log( dot(sample, luminanceVector) + 0.0001f );
	}
	
	// 평균 산출
	logLuminanceSum /= 9;
	
	return float4( logLuminanceSum, logLuminanceSum, logLuminanceSum, 1.0f );
}

//==================================================================
/** ReSampleLuminanceDown4x4
	@remarks	sample point들의 blending에 의해 밝은 부분 텍스쳐를
				down scale.
*/
//==================================================================
float4 psSampleLuminanceIterative( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float reSampleSum = 0.0f;

	// sample point들에 대해 luminance들을 합한다.
	for (int i=0; i < 16; i++)
	{
		reSampleSum += tex2D( diffuseMap_Sampler, inTex + g_aSampleOffsetsWeights[i] );
	}
	
	reSampleSum /= 16;
	
	return float4( reSampleSum, reSampleSum, reSampleSum, 1.0f );
}

//==================================================================
/** ReSampleLuminanceExp
	@remarks	평균 휘도를 구하고, exp() 연산을 하여, 평균 luminance
				구하기를 완료
*/
//==================================================================
float4 psSampleLuminanceExp( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float2 reSampleSum = 0.0f;

	// sample point들에 대해 luminance들을 합한다.
	for (int i=0; i < 16; i++)
	{
		reSampleSum += tex2D( diffuseMap_Sampler, inTex + g_aSampleOffsetsWeights[i] );
	}
	
	// 평균을 구하고, 평균 luminance 계산을 완료하기 위해, exp() 연산을 한다.
	float avg = exp(reSampleSum/16);

	return float4( avg, avg, avg, 1.0f );
}

technique DownFilter4x4
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psDownFilter4x4();
	}
}
