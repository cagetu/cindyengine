//------------------------------------------------------------------------------
//  p_gbuffer.fx : Pass-shader for the Light Pre G-Buffer
//	[참고] Nebula3 - Radon Labs GmbH
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//------------------------------------------------------------------------------
technique t0
{
    pass p0
    {
        // Disable writing to depth buffer
		CullMode = CCW;

		ZEnable	= True;
		ZWriteEnable = false;
        ZFunc             = Equal;
		// 첫 패스로 할 때는 사용하지 않는다.
		//AlphaBlendEnable = true;
		//SrcBlend = One;
        //DestBlend = One;
    }
}
