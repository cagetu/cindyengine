#line 1 "template_depth.fxh"
//-------------------------------------------------------------
//	DepthFunc.fxh : ���� ������
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\DepthBlur.fxh"
#include "..\\..\\sdk\\Shader\\Util_Skinning.fxh"
#include "..\\..\\sdk\\Shader\\sampler_default.fxh"
#ifdef _SMDEPTH_
#include "..\\..\\sdk\\Shader\\Util_ShadowMap.fxh"
#endif

float4 gViewInfo : ViewInfo;

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
struct VertexInput
{
    float4 position		: POSITION0;
#ifdef _DIFFUSE_MAP_
    float2 uv0			: TEXCOORD0;
#endif
	float3 normal		: NORMAL0;
#ifdef _NORMAL_MAP_
	float4 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
#endif
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif    
};

struct VertexOutput
{
	float4 position		: POSITION0;
	float2 depth		: TEXCOORD0;
#ifdef _DIFFUSE_MAP_
	float2 uv0			: TEXCOORD2;
#endif
	float3 normal		: TEXCOORD1;
#ifdef _NORMAL_MAP_
	float3 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
#endif
};

//------------------------------------------------------------------------------
/**
    Vertex shader function for the depth pass.
    Passes depth to pixelshader.    
*/
VertexOutput vs_depth( VertexInput Input )
{
    VertexOutput Output = (VertexOutput)0;

	float4 localPos = 0;
	float3 localNor = 0;
	float3 inputNormal = Input.normal;	//normalize(UnpackNormal(Input.normal));

#ifdef _NORMAL_MAP_
	float3 inputTangent = Input.tangent.xyz;	//normalize(UnpackNormal(Input.tangent));
	float3 inputBinormal = Input.binormal;		//normalize(UnpackNormal(Input.binormal));
	float3 localTan = 0;
	float3 localBir = 0;
#endif //_NORMAL_MAP_

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
  #ifdef _NORMAL_MAP_
	localTan = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, gJoints );
	localBir = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, gJoints );
  #endif //_NORMAL_MAP_
#else
	localPos = Input.position;
	localNor = inputNormal;
  #ifdef _NORMAL_MAP_
	localTan = inputTangent;
	localBir = inputBinormal;
  #endif
#endif

	const float4 HPos = TransformPosition(localPos, gWorldViewProj);
	Output.position = HPos;	// / HPos.w;

#ifdef _SMDEPTH_
	Output.depth.x = HPos.z;
#else
	// Depth
	float4 worldviewPos = TransformPosition(localPos, gWorldView);
	Output.depth.x = worldviewPos.z;
#endif

#ifdef _DIFFUSE_MAP_
	Output.uv0 = Input.uv0;	//UnpackUv(Input.uv0);
#endif

	// tangent, binormal
#ifdef _NORMAL_MAP_
	Output.tangent = localTan;
	Output.binormal = localBir;
#endif // _NORMAL_MAP_
	Output.normal = localNor;

    return Output;
}

//------------------------------------------------------------------------------
/**
*/
float4 ps_depth( const VertexOutput Input ) : COLOR
{
	float4 Output = float4(1,1,1,1);

#ifdef _SMDEPTH_
	float d = Input.depth.x;
	float2 moments = ComputeMoments(d);
	Output = float4(moments, 0.0f, 1.0f);
	//Output = EncodeVSMDepth(depth);
#else
  #ifdef _DIFFUSE_MAP_
	float4 albedo = tex2D( DiffuseMapSampler0, Input.uv0 );
	Output.a = albedo.a;
  #endif
  #ifdef _ALPHA_MAP_
	Output.a = tex2D( AlphaMapSampler, Input.uv0 );
  #endif

	/// normal
	float3 normal = 0;
  #ifdef _NORMAL_MAP_
	normal = psNormalFromBumpMap(NormalMapSampler, Input.uv0, Input.tangent, Input.binormal, Input.normal );
  #else
	normal = Input.normal;
  #endif
	normal = normalize(mul(normal, (float3x3)gWorldView));

	float normDepth = NormalizeDepth(Input.depth.x, (gViewInfo.w-gViewInfo.z));
	Output = float4(EncodeNormal(normal), EncodeDepth(normDepth));
#endif
	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p0
	{
		AlphaBlendEnable = false;

		VertexShader = compile vs_3_0 vs_depth();
		PixelShader = compile ps_3_0 ps_depth();	
	}

}