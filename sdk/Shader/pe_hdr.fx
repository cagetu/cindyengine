#line 1 "Hdr.fx"
//-------------------------------------------------------------
//	Hdr.fx : HDR 랜더링
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
//#include "..\\..\\sdk\\Shader\\Filter.fxh"
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"

//-------------------------------------------------------------
//	shader variables
//-------------------------------------------------------------

// filter
float2 filterSampleOffsets[16] : FilterSampleOffsets;
float4 filterSampleWeights[16] : FilterSampleWeights;

float4 hdrBloomColor = {1.0f, 1.0f, 1.0f, 1.0f};
float hdrBloomScale = 1.0f;

float g_fMiddleGray = 0.18f;	// 0.18, 0.36, 0.72...
static const float  BRIGHT_PASS_THRESHOLD  = 0.5f;  // Threshold for BrightPass filter
static const float  BRIGHT_PASS_OFFSET     = 0.5f;	// Offset for BrightPass filter

// The per-color weighting to be used for luminance calculations in RGB order.
static const float3 luminanceVector = float3(0.2125f, 0.7154f, 0.0721f);

const bool g_EnableToneMap = false;
const float g_WhiteCutOff = 0.5f;	// 0.5, 1.0f, 1.5f, 3, ... 

float g_fElapsedTime : TimeValue;

//-------------------------------------------------------------
//	Texture Samplers
//-------------------------------------------------------------
texture diffuseMap0 : DiffuseMap0;
texture diffuseMap1 : DiffuseMap1;
texture diffuseMap2 : DiffuseMap2;
texture diffuseMap3 : DiffuseMap3;
texture diffuseMap4 : DiffuseMap4;
texture diffuseMap5 : DiffuseMap5;
texture diffuseMap6 : DiffuseMap6;

sampler diffuseMap0_Sampler = sampler_state
{
    Texture = <diffuseMap0>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = Point;
};

sampler brightPass_Sampler = sampler_state
{
    Texture = <diffuseMap0>;
    MinFilter = Point;
};

sampler bloom_Sampler = sampler_state
{
    Texture = <diffuseMap1>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

sampler diffuseMap1_Sampler = sampler_state
{
    Texture = <diffuseMap1>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

sampler diffuseMap2_Sampler = sampler_state
{
    Texture = <diffuseMap2>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

sampler diffuseMap3_Sampler = sampler_state
{
    Texture = <diffuseMap3>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

sampler diffuseMap4_Sampler = sampler_state
{
    Texture = <diffuseMap4>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

sampler diffuseMap5_Sampler = sampler_state
{
    Texture = <diffuseMap5>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

sampler diffuseMap6_Sampler = sampler_state
{
    Texture = <diffuseMap6>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

//-------------------------------------------------------------
//	structures
//-------------------------------------------------------------
struct VertexInput
{
    float4 position : POSITION;
    float2 uv : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 uv : TEXCOORD0;
};

VertexOutput vsQuad( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;
	output.position = Input.position;
	output.uv = Input.uv;
	
	return output;
}

//------------------------------------------------------------------------------
/**
    UpdateSamplesBloom
    
    Get sample offsets and weights for a horizontal or vertical bloom filter.
    This is normally executed in the pre-shader.

void
UpdateSamplesBloom(in bool horizontal, in int texSize, in float deviation, in float multiplier, out float3 sampleOffsetsWeights[MaxBloomSamples])
{
    float tu = 1.0f / (float) texSize;
    
    // fill center texel
    float weight = multiplier * GaussianDistribution(0.0f, 0.0f, deviation);
    sampleOffsetsWeights[0]  = float3(0.0f, 0.0f, weight);
    sampleOffsetsWeights[15] = float3(0.0f, 0.0f, 0.0f);

    // fill first half
    int i;
    for (i = 1; i < 8; i++)
    {
        if (horizontal)
        {
            sampleOffsetsWeights[i].xy = float2(i * tu, 0.0f);
        }
        else
        {
            sampleOffsetsWeights[i].xy = float2(0.0f, i * tu);
        }
        weight = multiplier * GaussianDistribution((float)i, 0, deviation);
        sampleOffsetsWeights[i].z = weight;
    }

    // mirror second half
    for (i = 8; i < 15; i++)
    {
        sampleOffsetsWeights[i] = sampleOffsetsWeights[i - 7] * float3(-1.0f, -1.0f, 1.0f);
    }
}
*/

//==================================================================
/** Final Scene
	@brief	장면을 랜더링한다.
*/
//==================================================================
float4 psFinalScene( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = tex2D( diffuseMap0_Sampler, inTex );

	#ifdef _SRGB_
	//color = sRGBtolinearRGB( color );
	#endif
	
    //
    //      (      (   Lp    ))
    // Lp * (1.0f +(---------))
    //      (      ((Lm * Lm)))
    // -------------------------
    //         1.0f + Lp
    //
    // Map the high range of color values into a range appropriate for
    // display, taking into account the user's adaptation level, and selected
    // values for for middle gray and white cutoff.
	float4 luminance = tex2D( diffuseMap2_Sampler, float2(0.5f, 0.5f) );
	float adaptedLuminance = luminance.r;
	if (g_EnableToneMap)
	{
		float3 pixelLuminance = color.xyz;	//dot(color, luminanceVector);
		float4 scaledLuminance;
		scaledLuminance.xyz = pixelLuminance * (g_fMiddleGray / (adaptedLuminance + 0.001f));
		scaledLuminance.w = color.w;

		// 1. 가장 간단한 tone mapping operation
		color.rgb = scaledLuminance.rgb / (1.0f+scaledLuminance);

		// 2. white cutoff는 장면의 일단 장면의 가장 밝은 부분을 기본값으로 한다.
		//float Lm = luminance.g;	// g_WhiteCutOff;
		//float Lm = 0.5f;
		//color.rgb = scaledLuminance * (1.0f + scaledLuminance/(Lm*Lm) ) / (1.0f + scaledLuminance);
	}

	//float4 star = tex2D( diffuseMap1_Sampler, inTex );
	//color += star;
	
	float4 bloom = tex2D( bloom_Sampler, inTex );
	color += bloom * hdrBloomScale;

	#ifdef _SRGB_
	//color = linearRGBtosRGB(color);
	#endif

	return color;
}

//==================================================================
/** SampleLuminanceAverageLog
	@brief	소스 이미지의 밝은 부분을 추출하고, log()의 평균이
				포함된 scaled image를 반환한다.
*/
//==================================================================
float4 psSampleLumInitial( in float2 inTex : TEXCOORD0 ) :COLOR
{
	float3 sample = 0;
	float logLuminanceSum = 0;

	float maximum = -1e20;

	// sample Point를 통해, log(luminace)의 합을 계산한다.
	for (int i=0; i < 9; i++)
	{
		sample = tex2D( diffuseMap0_Sampler, inTex + filterSampleOffsets[i] );
		logLuminanceSum += log( dot(sample, luminanceVector) + 0.0001f );

		// 3. Take the maximum value of the incoming, same as computing the
		//    brightness/value for an HSV/HSB conversion:
		float GreyValue = max( sample.r, max( sample.g, sample.b ) );
		maximum = max( maximum, GreyValue );
	}

	// 평균 산출
	logLuminanceSum /= 9;

	return float4( logLuminanceSum, maximum, 0.0f, 1.0f );
}

//==================================================================
/** ReSampleLuminanceDown4x4
	@brief	sample point들의 blending에 의해 밝은 부분 텍스쳐를
				down scale.
*/
//==================================================================
float4 psSampleLumIterative( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float reSampleSum = 0.0f;
	float maximum = -1e20;

	float4 sample;
	// sample point들에 대해 luminance들을 합한다.
	for (int i=0; i < 16; i++)
	{
		sample = tex2D( diffuseMap0_Sampler, inTex + filterSampleOffsets[i] );
		maximum = max( maximum, sample.g );

		reSampleSum += sample.r;
	}
	
	reSampleSum /= 16;
	
	return float4( reSampleSum, maximum, 0.0f, 1.0f );
}

//==================================================================
/** ReSampleLuminanceExp
	@brief	평균 휘도를 구하고, exp() 연산을 하여, 평균 luminance
				구하기를 완료
*/
//==================================================================
float4 psSampleLumFinal( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float reSampleSum = 0.0f;
	float maximum = -1e20;

	float4 sample;
	// sample point들에 대해 luminance들을 합한다.
	for (int i=0; i < 16; i++)
	{
		sample = tex2D( diffuseMap0_Sampler, inTex + filterSampleOffsets[i] );
		maximum = max( maximum, sample.g );

		reSampleSum += sample.r;
	}

	// 평균을 구하고, 평균 luminance 계산을 완료하기 위해, exp() 연산을 한다.
	reSampleSum = exp(reSampleSum/16);

	return float4( reSampleSum, maximum, 0.0f, 1.0f );
}

//==================================================================
/** BrightPassFilter
	@brief	마지막에 계산된 luminance를 가지고 시간의 경과와 현재 장면의 luminance,
			가장 최근에 사용된 adaptaion level을 가지고 카메라를 적응시키는 luminance를 계산
*/
//==================================================================
float4 psCalculateAdaptedLum( in float2 inTex : TEXCOORD0 ) : COLOR0
{
    float fAdaptedLum = tex2D(diffuseMap0_Sampler, float2(0.5f, 0.5f));
    float fCurrentLum = tex2D(diffuseMap1_Sampler, float2(0.5f, 0.5f));

    // The user's adapted luminance level is simulated by closing the gap between
    // adapted luminance and current luminance by 2% every frame, based on a
    // 30 fps rate. This is not an accurate model of human adaptation, which can
    // take longer than half an hour.
    float fNewAdaptation = fAdaptedLum + (fCurrentLum - fAdaptedLum) * ( 1 - pow( 0.98f, 30 * g_fElapsedTime ) );
    return float4(fNewAdaptation, fNewAdaptation, fNewAdaptation, 1.0f);
}

//==================================================================
/** BrightPassFilter
	@brief	밝은 부분을 추출한다.
*/
//==================================================================
float4 psBrightPassFilter( in float2 inTex : TEXCOORD0 ) :COLOR
{
	/// down-scaled scene
	float4 sample = tex2D( diffuseMap0_Sampler, inTex );

	//@< 임시...
	//sample.rgb -= 1.0f;
	//sample.rgb = 3.0f * max(sample, 0.0f);
	//return sample;
	//@>

	//@< Nebula3의 방법.
	//float3 brightColor = max(sample.rgb - BRIGHT_PASS_THRESHOLD, 0);
	//bool isBright = any(brightColor);
 //   float luminance = dot(brightColor, float3(0.299, 0.587, 0.114));
 //   return hdrBloomColor * sample * luminance * isBright;
	//@>

	////<@ tonemap
	// LumScaled = g_fMiddleGray * sample.rgb * ("world" luminance for pixel) / luminance average;
	// Lum_Thresold = max(LumScaled * (1.0f + LumScaled / (White*White)_brightpas) - BRIGHT_PASS_THRESHOLD, 0.0f);
	// Lum_BrightPass = Lum_Thresold / (BRIGHT_PASS_OFFSET + Lum_Thresold);
	float adaptedLuminance = tex2D( diffuseMap1_Sampler,  float2(0.5f, 0.5f) ).r;

	// tone-mapping의 후에 pixel의 값을 결정한다.
	//float3 pixelLuminance = sample.xyz;	//dot(sample, luminanceVector);	//
	//sample.rgb = pixelLuminance * (g_fMiddleGray / (adaptedLuminance + 0.001f));
	sample.rgb *= g_fMiddleGray / (adaptedLuminance + 0.001f);

	// 어두운 픽셀들을 제외
	sample.rgb -= BRIGHT_PASS_THRESHOLD;

	// Clamp to 0
	sample = max(sample, 0.0f);

	// 0~1사이로 결과값을 매핑한다. 
	// BRIGHT_PASS_OFFSET보다 높은 값은 빛이 비추어진 장면 오브젝트들로 부터 분리된 라이트들일 것이다.
	sample.rgb /= (BRIGHT_PASS_OFFSET+sample);
	return sample;
	//@>
}

//==================================================================
/** Bloom_Horizontal
	@brief	발광 효과 수평으로 늘리기
*/
//==================================================================
float4 psBloom_Horizontal( in float2 inTex : TEXCOORD ) : COLOR
{
	float4 color = 0;
	
	for (int i=0; i<15; ++i)
	{
		color += tex2D( diffuseMap0_Sampler, float2(inTex.x + filterSampleOffsets[i].x, inTex.y) ) * filterSampleWeights[i];
	}
	
	return color;
}
//==================================================================
/** Bloom_Vertical
	@brief	발광 효과 수직으로 늘리기
*/
//==================================================================
float4 psBloom_Vertical( in float2 inTex : TEXCOORD ) : COLOR
{
	float4 color = 0;
	
	for (int i=0; i<15; ++i)
	{
		color += tex2D( diffuseMap0_Sampler, float2(inTex.x, inTex.y + filterSampleOffsets[i].y) ) * filterSampleWeights[i];
	}
	
	return color;
}

//==================================================================
/** StarEffect
	@brief	별 효과를 만든다.
*/
//==================================================================
float4 psStarEffect( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 sample = 0;
	float4 color = 0;
	
	float2 samplePosition = 0;
	for (int i=0; i<8; ++i)
	{
		samplePosition = inTex + filterSampleOffsets[i];
		sample = tex2D( diffuseMap0_Sampler, samplePosition );
		color += filterSampleWeights[i] * sample;
	}
	
	return color;
}

//==================================================================
/** MergeTexture
	@brief	텍스쳐를 합친다.
*/
//==================================================================
float4 psMergeStarTextures( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;
	
	color += tex2D( diffuseMap0_Sampler, inTex );
	color += tex2D( diffuseMap1_Sampler, inTex );
	color += tex2D( diffuseMap2_Sampler, inTex );
	color += tex2D( diffuseMap3_Sampler, inTex );
	color += tex2D( diffuseMap4_Sampler, inTex );
	color += tex2D( diffuseMap5_Sampler, inTex );
	
	return color/6.0f;
}

//==================================================================
/** Copy To 
	@brief	장면을 랜더링한다.
*/
//==================================================================
float4 psCopyTo( in float2 inTex : TEXCOORD0 ) :COLOR
{
    float4 sample = tex2D(diffuseMap0_Sampler, inTex);
	return sample;
}

//-------------------------------------------------------------
//	Techniques
//-------------------------------------------------------------
// Luminance Sample 평균
technique LuminanceSampleAvg
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psSampleLumInitial();
	}
}

technique LuminanceResampleAvg
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psSampleLumIterative();
	}
}

technique LuminanceResampleAvgExp
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psSampleLumFinal();
	}
}

//
technique CalculateAdaptedLum
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psCalculateAdaptedLum();
	}
}

// 밝은 부분
technique BrightPassFilter
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psBrightPassFilter();
	}
}

// Bloom Effect
technique Bloom_Vertical
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psBloom_Vertical();
	}
}
technique Bloom_Horizontal
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psBloom_Horizontal();
	}
}

// Star Effect
technique StarEffect
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psStarEffect();
	}
}

// Star Effect Merge Texture
technique MergeStarTexture
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psMergeStarTextures();
	}
}

// scene
technique FinalScene
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psFinalScene();
	}
}

technique CopyTo
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psCopyTo();
	}
}