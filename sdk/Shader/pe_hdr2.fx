#line 1 "pe_hdr2.fx"

//-------------------------------------------------------------
//	Hdr.fx : HDR 랜더링
//	Copyright ?2011 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"
#include "..\\..\\sdk\\Shader\\Filter.fxh"

//-------------------------------------------------------------
// Shader variables
//-------------------------------------------------------------
// Overlay
float4 gScreenSize : DisplayResolution;
float4 gScreenProj[2] : ScreenProj;

float gConstAvgLuminance : AverageLuminance = 0.01;

float gMiddleGrey : MiddleGrey = 0.18f;		// (MiddleGrey / AverageLumiance) * SceneLuminance
float gMaxLuminance : MaxLuminance = 16.0f;

float gBloomThreshold : BloomThreshold = 0.8f;
float gBloomScale : BloomScale = 1.0f;
float4 gBloomTint : BloomTint = 1.0f;
float gBloomScreenBlendThreshold : BloomScreenBlendThreshold = 10.0f;

float gElapsedTime : ElapsedTime = 1.0f;
float gTau : Tau = 1.0f;

//-------------------------------------------------------------
//	Texture Samplers
//-------------------------------------------------------------
texture colorbuffer : DiffuseMap0;
sampler ColorBufferSampler = 
sampler_state
{
    Texture = <colorbuffer>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
};
sampler ColorPointSampler = 
sampler_state
{
    Texture = <colorbuffer>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

texture bloombuffer : BloomBuffer;
sampler BloomBufferSampler =
sampler_state
{
    Texture = <bloombuffer>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
};

texture luminancebuffer : LuminanceBuffer;
sampler LuminanceBufferSampler =
sampler_state
{
    Texture = <luminancebuffer>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};
//-------------------------------------------------------------
/**	Vertex Shader 구조체
*/
//-------------------------------------------------------------
struct VertexInput
{
    float3 position : POSITION;
    float2 texcoord : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};

//-------------------------------------------------------------
VertexOutput vs_main( VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;

	output.position = float4(Input.position.xy, 0, 1) * gScreenProj[0] + gScreenProj[1];
	output.texcoord = Input.texcoord;

	return output;
}

//// TONEMAPPING

#define _TONEMAP_FILMICU2
//#define _TONEMAP_EXPONENTIAL
//#define _TONEMAP_REINHARDADV
//#define _TONEMAP_REINHARD

#ifdef _TONEMAP_FILMICU2
float A : SHOULDERSTRENGTH = 0.22;	// ShoulderStrength
float B : LINEARSTRENGTH = 0.5;		// LinearStrength
float C : LINEARANGLE = 0.10;		// LinearAngle
float D : TOESTRENGTH = 0.20;		// ToeStrength
float E : TOENUMERATOR = 0.02;		// ToeNumerator
float F : TOEDENOMINATOR = 0.30;	// ToeDenominator
float W : LINEARWHITE = 11.2;		// LinearWhite

float3 U2Func(float3 x)
{
	return ((x*(A*x+C*B)+D*E) / (x*(A*x+B)+D*F))-E/F;
}
#endif

float GetMiddleGrey(float lumAvg)
{
	float exposure = 0;

 #ifdef _AUTOMIDDLEGREY
	exposure = max(0, 1.03 - 2.0/(2.0 + log10(lumAvg+1.0f)));		// function 1
	//exposure = max(0, 1.5 - (1.5 / (1.0 + lumAvg*0.1))) + 0.1;	// function 2
 #else
	exposure = gMiddleGrey;
 #endif
	return exposure;
}


float4 ps_composite( VertexOutput Input ) : COLOR
{
	const float4 HDRColor = tex2D(ColorBufferSampler, Input.texcoord);

	float4 BloomColor = tex2D(BloomBufferSampler, Input.texcoord) * gBloomScale * gBloomTint;

	// Use an exponential function that converges on 0 slowly
	// This minimizes the halo creates by the screen blend when the source image is a bright gradient

	// RGB scale factor to calculated pixel luminance using a weight average
	float3 LuminanceFactor = float3( 0.3, 0.59, 0.11 );
	float pixelLuminance = dot( LuminanceFactor, BloomColor );

	float brightness = saturate(gBloomScreenBlendThreshold - exp(-3 * pixelLuminance));
	BloomColor.rgb *= brightness;

	float3 SceneColor = HDRColor.xyz + BloomColor.xyz;

	return float4(SceneColor, 1);
}


#define _NUM_BLURSAMPLES	9

// Calculates the gaussian blur weight for a given distance and sigmas
float CalcGaussianWeight(int sampleDist, float sigma)
{
	float g = 1.0f / sqrt(2.0f * 3.14159 * sigma * sigma);  
	return (g * exp(-(sampleDist * sampleDist) / (2 * sigma * sigma)));
}

float4 ps_gaussianblur( VertexOutput Input, uniform bool vertical ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	float2 gaussianDir = (vertical==true) ? float2(0,1) : float2(1, 0);
	
	// Gaussian Blur 테스트
	float4 totalColor = 0;
	for(int i=0; i<_NUM_BLURSAMPLES; i++)
	{
		float2 gaussianOffsets = (i-4) / gScreenSize * gaussianDir;
		float gaussianweight = CalcGaussianWeight((i-4), 2.0f);

		float4 sampleColor = tex2D(ColorBufferSampler, Input.texcoord+gaussianOffsets);
		totalColor += sampleColor * gaussianweight;
	}
	return totalColor;
}

#define _MAX_SCENECOLOR 4

// ---------------------------------------------

/** Computes a pixel's luminance for bloom */
float ComputeLuminanceForBloom( float3 InSceneColor )
{
	// Compute the luminance for this pixel
	float TotalLuminance;
	if( 1 )
	{
		// Compute luminance as the maximum of RGB.  This is a bit more intuitive for artists as they know
		// that any pixel with either of the RGB channels above 1.0 will begin to bloom.
		TotalLuminance = max( InSceneColor.r, max( InSceneColor.g, InSceneColor.b ) );
	}
	else
	{
		// RGB scale factor to calculated pixel luminance using a weight average
		half3 LuminanceFactor = half3( 0.3, 0.59, 0.11 );

		// Compute true luminance
		TotalLuminance = dot( LuminanceFactor, InSceneColor );
	}

	return TotalLuminance;
}


/** Computes bloomed amount for the specified scene color */
float ComputeBloomAmount( float3 InSceneColor, float InLuminance, float Threshold )
{
	// Size of the bloom "ramp".  This value specifies the amount of light beyond the bloom threshold required
	// before a pixel's bloom will be 100% of the original color.
	// NOTE: Any value above 0.8 looks pretty good here (and 1.0 is often fastest), but a value of 2.0 here
	//	     minimizes artifacts: the bloom ramp-up will closely match the linear ascent of additive color
	float BloomRampSize = 2.0f;

	// Figure out how much luminance is beyond the bloom threshold.  Note that this value could be negative but
	// we handle that in the next step.
	float BloomLuminance = InLuminance - Threshold;

	// Note that we clamp the bloom amount between 0.0 and 1.0, but pixels beyond our bloom ramp will still 
	// bloom brighter because we'll use 100% of the original scene color as bloom
	float BloomAmount = saturate( BloomLuminance / BloomRampSize );
	
	return BloomAmount;
}

float4 ps_bloomthresold( VertexOutput Input ) : COLOR0
{
	const float4 HDRSceneColor = tex2D(ColorBufferSampler, Input.texcoord);

	/*	Threshold 처리를 어떻게?
		1. 그냥 빼준다.
		2. log based 
		
		3. Threshold 이후의 결과에 대한 범위 설정 (0~1)?? 
	*/

	float3 BloomColor = 0.0f;

	/*	#test1
		밝은 부분에 대한 범위를 [0, 1]로 Scale하였음.
	*/ 
	//const float _BloomLuminanceScale = 4.0f;
	//float t = saturate(max(0, SceneLuminance - gBloomThreshold)/SceneLuminance) * _BloomLuminanceScale;
	//BloomColor = SceneColor/SceneLuminance * t;
	//BloomColor = clamp(BloomColor, 0, _MAX_SCENECOLOR);
	
	const float3 SceneColor = HDRSceneColor.xyz;
	const float SceneLuminance = ComputeLuminanceForBloom(SceneColor);
#if 1
	float BloomAmount = ComputeBloomAmount(SceneColor, SceneLuminance, gBloomThreshold);
#else
	float BloomLuminance = SceneLuminance - gBloomThreshold;
	const float BloomBrightOffset = 4.0f;
	float BloomAmount = saturate(BloomLuminance / (BloomLuminance+BloomBrightOffset));
#endif
	BloomColor = SceneColor * BloomAmount;

	// #test2
	//float t = log2(SceneLuminance) - 2;
	//float3 bloomColor = HDRColor.rgb/SceneLuminance * max(0, exp2(t));

	return float4(BloomColor, 1);
}

//// 톤매핑

float RGBToLuminance(float3 color)
{
	const float3 LUMINANCE_ = {0.299f, 0.587f, 0.114f};
	return max(0, dot(LUMINANCE_, color));
}

float4 ps_beginluminance(VertexOutput Input) : COLOR
{
	const float4 HDRSceneColor = tex2D(ColorBufferSampler, Input.texcoord);
	const float luminance = RGBToLuminance(HDRSceneColor.rgb);
	
	const float logluminance = log(luminance+1e-5);
	return float4(logluminance, 1, 1, 1);
}

float4 ps_endluminance(VertexOutput Input) : COLOR
{
    const float SumLogLuminance = psDownFilter4x4(Input.texcoord).x;
	
	const float luminance = exp(SumLogLuminance);
	return float4(luminance, 1, 1, 1);
}

float4 ps_adaptluminance(VertexOutput Input) : COLOR
{
	const float currentluminance = tex2D(ColorPointSampler, float2(0.5, 0.5)).r;
	const float lastluminance = tex2D(LuminanceBufferSampler, float2(0.5, 0.5)).r;

    // Adapt the luminance using Pattanaik's technique
	const float luminance = lastluminance + (currentluminance - lastluminance) * (1 - exp(-gElapsedTime*gTau));
	return float4(luminance, 1, 1, 1);
};

float4 ps_storeluminance(VertexOutput Input) : COLOR
{
	const float luminance = tex2D(LuminanceBufferSampler, float2(0.5, 0.5)).r;
	return float4(luminance, 1, 1, 1);
};

#define _AUTOEXPOSURE	1

float LinearToe : LINEARTOE = 1.0f;
float LinearScale : LINEARSCALE = 1.0f;
//-------------------------------------------------------------
float4 ps_tonemapping( VertexOutput Input ) : COLOR
{
	const float4 HDRColor = tex2D(ColorBufferSampler, Input.texcoord);

#if _AUTOEXPOSURE
	// Scale luminance
	const float AverageLuminance = max(tex2D(LuminanceBufferSampler, float2(0.5f, 0.5f)).x, 0.0001f);
	const float linearExposure = GetMiddleGrey(AverageLuminance) / AverageLuminance;
	float exposure = log2(max(linearExposure, 0.0001f));
#else
	float exposure = 0.0f;
#endif

	// 현재 장면의 밝기랑 비교해서, 제한을 해보자.. 
	const float3 exposedColor = HDRColor.rgb * exposure;

	float3 LDRColor;
#if defined(_TONEMAP_FILMICU2)
	{
		const float ExposureBias = 2.0f;
		LDRColor = U2Func(exposedColor*ExposureBias) / U2Func(W);
	}
#elif defined(_TONEMAP_EXPONENTIAL)
	{
		LDRColor = 1.0 - exp(-exposedColor);
	}
#elif defined(_TONEMAP_REINHARDADV)
	{
		LDRColor = exposedColor * (1.0 + (exposedColor/max(0.0001f,Square(gMaxLuminance)))) / (1.0+exposedColor);
	}
#elif defined(_TONEMAP_REINHARD)
	{
		LDRColor = exposedColor / abs(LinearToe+exposedColor) * LinearScale;
	}
#else
	{
		LDRColor = exposedColor;
	}
#endif
	return float4(saturate(LDRColor), 1);
}

// BLOOM THRESHOLD
technique BloomThreshold
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_bloomthresold();
	}
}
// 2x2 축소 샘플링
technique DownFilter2x2
{
	Pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 psDownFilter2x2();
	}
}
technique DownFilter4x4
{
	Pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 psDownFilter4x4();
	}
}

// GAUSSIAN BLUR
technique GaussianHori
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_gaussianblur(false);
	}
}

technique GaussianVert
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_gaussianblur(true);
	}
}

// LUMINANCE
technique BeginLuminance
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_beginluminance();
	}
}

technique EndLuminance
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_endluminance();
	}
}

technique AdaptLuminance
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_adaptluminance();
	}
}

technique StoreLuminance
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_storeluminance();
	}
}

// TONE MAPPING
technique Tonemapping
{
	Pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_tonemapping();
	}
}

technique FinalScene
{
	Pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_composite();
	}
}
