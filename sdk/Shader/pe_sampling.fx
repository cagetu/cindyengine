#line 1 "pe_sampling.fx"
//-------------------------------------------------------------
//	pe_sampling.fx : post-processing sampling 관련 Shader 모음
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Filter.fxh"

//-------------------------------------------------------------
//	Techniques
//-------------------------------------------------------------
// 2x2 축소 샘플링
technique DownFilter2x2
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psDownFilter2x2();
	}
}

// 4x4 축소 샘플링
technique DownFilter4x4
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psDownFilter4x4();
	}
}

// GaussBlur5x5
technique GaussBlur5x5
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psGaussBlur5x5();
	}
}

technique GaussBlur3x3
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psGaussBlur3x3();
	}
}