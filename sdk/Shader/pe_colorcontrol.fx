#line 1 "pe_colorcontrol.fx"
//-------------------------------------------------------------
//	pe_colorcontrol.fx : Color Control 
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"

//-------------------------------------------------------------
// Constants
//-------------------------------------------------------------
float4x4 gWorldViewProj : ModelViewProjection;

// texture
texture colorTexture : DiffuseMap0;

//-------------------------------------------------------------
// Sampler
//-------------------------------------------------------------
sampler colorSampler = sampler_state
{
    Texture = <colorTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

//-------------------------------------------------------------
/**	Vertex Shader 구조체
*/
//-------------------------------------------------------------
struct VertexInput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
VertexOutput vsMain( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;
	
	output.position = mul(Input.position, gWorldViewProj);
	output.texcoord = Input.texcoord;
	
	return output;
}

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
float4 psMain( VertexOutput Input ) : COLOR
{
	float4 color = tex2D( colorSampler, Input.texcoord );
	
	#ifdef _SRGB_
	//color = sRGBtolinearRGB( color );
	#endif

    //float3 n;
    //n.xy = (tex2D(colorSampler, Input.texcoord).ag * 2.0) - 1.0;    
    //n.z = sqrt(1.0 - dot(n.xy, n.xy));
	//float4 c = float4(n, 1.0f);

	/// color control
	color = AdjustLevel( color );
	color = GrayScaleConversion( color );
	color = PhotoFilter( color );
	//color = SeletiveColorCorrection( color );
	
	#ifdef _SRGB_
	//color = linearRGBtosRGB(color);
	#endif

	return color;
}

//-------------------------------------------------------------
/**	Technique
*/
//-------------------------------------------------------------
technique RenderScreenQuad
{
	Pass Default
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}

// scene
technique FinalScene
{
	Pass Default
	{
/*
        ZWriteEnable     = False;
        ZEnable          = False;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        AlphaBlendEnable = False;
        AlphaTestEnable  = False;
        CullMode         = None;
        StencilEnable    = False;
*/
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}