#ifndef __UTIL_SHADOWMAP_FXH__
#define __UTIL_SHADOWMAP_FXH__

//==============================================================================
//	util_shadowmap.fxh : shader shadowmap utility functions.
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//==============================================================================
#include "..\\..\\sdk\\Shader\\config.fxh"

#define _VSM_	(1)
#define _PSSM_	(0)

static const float LightVSMEpsilon = 0.0001;
static const float LBRAmount = 0.18f;

float gShadowMapBias	: ShadowMapBias = -0.002f;
float gMinVariance		: MinVariance = 1.0f;

#if _PSSM_
	#define PSSMNumSplits 4
	
	float gGlobalPSSMDistances[PSSMNumSplits+1]		: GlobalPSSMDistances;
	float4x4 gGlobalPSSMTransforms[PSSMNumSplits]	: GlobalPSSMTransforms;

	texture pssm0 : GlobalPSSMBuffer0;
	texture pssm1 : GlobalPSSMBuffer1;
	texture pssm2 : GlobalPSSMBuffer2;
	texture pssm3 : GlobalPSSMBuffer3;

	sampler gGlobalPSSMBuffers[PSSMNumSplits] = 
	{
		sampler_state
		{
			Texture = <pssm0>;
			MinFilter = Linear;  
			MagFilter = Linear;
			MipFilter = None;
			AddressU  = Border;
			AddressV  = Border;   
			BorderColor = 0xFFFFFFFF;
		},
		sampler_state
		{
			Texture = <pssm1>;
			MinFilter = Linear;  
			MagFilter = Linear;
			MipFilter = None;
			AddressU  = Border;
			AddressV  = Border;   
			BorderColor = 0xFFFFFFFF;
		},
		sampler_state
		{
			Texture = <pssm2>;
			MinFilter = Linear;  
			MagFilter = Linear;
			MipFilter = None;
			AddressU  = Border;
			AddressV  = Border;   
			BorderColor = 0xFFFFFFFF;
		},
		sampler_state
		{
			Texture = <pssm3>;
			MinFilter = Linear;  
			MagFilter = Linear;
			MipFilter = None;
			AddressU  = Border;
			AddressV  = Border;   
			BorderColor = 0xFFFFFFFF;
		},
	};

#else
	float4x4 gWorldToTexProj	: ModelLightProjTexture;

	texture gShadowMap : ShadowMap;
	sampler ShadowMapSampler = sampler_state
	{
		Texture = <gShadowMap>;
		MinFilter = Linear;  
		MagFilter = Linear;
		MipFilter = None;
		AddressU  = Clamp;
		AddressV  = Clamp;   
	};
#endif

//------------------------------------------------------------------------------
/**
*/
float linstep( float min, float max, float v )
{
	return clamp((v-min) / (max - min), 0, 1);
}

//------------------------------------------------------------------------------
/**
*/
float LightBleedingReduction( float p_max, float amount )
{
	// remove the [0, amount] tail and linearly rescale(amount, 1).
	return linstep(amount, 1, p_max);
}

//------------------------------------------------------------------------------
/**
	- t <= moments.x 이고, pmax가 1이면, full lit!!!
	- t 는 distToLight 라이트에서의 거리
*/
float ChebyshevUpperBound(in float2 moments, in float t)
{
	// One-tiled inequality valid if t > moments.x
	float p = (t <= moments.x);

	float E_x2 = moments.y;
	float Ex_2 = moments.x * moments.x;

	// Compute variance
	//float variance = E_x2 - Ex_2;
	float variance = min(max(E_x2 - Ex_2, 0.0) + LightVSMEpsilon, gMinVariance);
	
	// Compute probabilistic upper bound.
	float d = t - moments.x;
	float p_max = variance / (variance + d*d);
	
	p_max = LightBleedingReduction(p_max, LBRAmount);
	return max(p, p_max);
}

//------------------------------------------------------------------------------
/**	Biasing
	- GPUGems3권 Biasing 참고!!!
*/
float2 ComputeMoments( float depth )
{
	float2 moments;
	
	// 첫번째 moment는 depth
	moments.x = depth;
	
	// depth의 부분적 도함수를 구한다.
	float dx = ddx(depth);
	float dy = ddy(depth);
	
	// pixel 면적에 대한 두번째 모멘트를 구한다.
	moments.y = depth * depth + 0.25 * (dx*dx + dy*dy);
	return moments;
}

//------------------------------------------------------------------------------
/**
*/
float
GetShadow( sampler shadowMap, float4 shadowProjPos )
{
	float lit = 1.0f;
	
	float depth = shadowProjPos.z;
	float distToLight = depth + gShadowMapBias;
#if _VSM_
	if (depth > 0.0f)
	{
		float2 moments = tex2Dproj(shadowMap, shadowProjPos).xy;
		lit = ChebyshevUpperBound( moments, distToLight );
	}
#else
	float depthSM = tex2Dproj(shadowMap, shadowProjPos).r;	
	lit = (distToLight < depthSM) ? 1.0f : 0.5f/*depthSM*/;
#endif
	return lit;
}

#endif	// __UTIL_SHADOWMAP_FXH__