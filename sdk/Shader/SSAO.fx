//******************************************************************************
/**	@author		cagetu
	@brief		Screen-Space Ambient Occlusion
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
float4x4 gWorldViewProj : ModelViewProjection;

texture gDepthTexture : DiffuseMap0;
texture gRotationTexture : DiffuseMap1;

float2 gScreenSize : DisplayResolution;
float4 gViewInfo : ViewInfo;	// cameraAspect, tanFOV, cameraNear, cameraFar

// x = default occlusion value
// y = kernel sphere radius 
// z = multiple of kernel radius to use for range truncation
// w = delta scale multiplier
float4 gSSAOParams = float4( 0.5f, 0.5f, 3.0f, 200.0f );	

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// 입력
struct VertexInput
{
	float4 position : POSITION0;
	float2 texCoord : TEXCOORD0;
};
/// 출력
struct VertexOutput
{
	float4 position : POSITION0;
	float2 texCoord : TEXCOORD0;
};

//------------------------------------------------------------------------------
// Samplers
//------------------------------------------------------------------------------
sampler DepthSampler = sampler_state
{
    Texture = <gDepthTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};
sampler RotationSampler = sampler_state
{
    Texture = <gRotationTexture>;
	AddressU = Wrap;
	AddressV = Wrap;
	AddressW = Wrap;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = Point;
};

//------------------------------------------------------------------------------
/**
*/
VertexOutput vsMain( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;
	Output.position = mul(Input.position, gWorldViewProj);
	Output.texCoord = Input.texCoord;
	return Output;
}


//------------------------------------------------------------------------------
/**
*/
float ReadDepth(in float2 texCoord)
{
	half pixelDepth = DecodeDepth(tex2D(DepthSampler, texCoord));

	half nearZ = gViewInfo.z;
	half farZ = gViewInfo.w;
	
	return (2.0f * nearZ) / (nearZ + farZ - pixelDepth * (farZ - nearZ));
}


//------------------------------------------------------------------------------
/**
*/
float4 psMain( VertexOutput Input ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	const half3 avKernel[8] =
	{
		normalize( half3(  1, 1, 1 ) ) * 0.125f,
		normalize( half3( -1,-1,-1 ) ) * 0.250f,
		normalize( half3( -1,-1, 1 ) ) * 0.375f,
		normalize( half3( -1, 1,-1 ) ) * 0.500f,
		normalize( half3( -1, 1 ,1 ) ) * 0.625f,
		normalize( half3(  1,-1,-1 ) ) * 0.750f,
		normalize( half3(  1,-1, 1 ) ) * 0.875f,
		normalize( half3(  1, 1,-1 ) ) * 1.000f
	};

	// 현재 pixel의 깊이를 얻어서 meter로 변환한다. (meter로 far clipping plane 거리)
	//half pixelDepth = tex2D(DepthSampler, Input.texCoord).r; 
	half pixelDepth = DecodeDepth(tex2D(DepthSampler, Input.texCoord));

	half nearFarClipDist = gViewInfo.w-gViewInfo.z;
	half fDepth = pixelDepth * nearFarClipDist;
	//half fDepth = ReadDepth(Input.texCoord);
	
	float radius = gSSAOParams.y;

	half3 kernelScale = half3(radius/fDepth, radius/fDepth, radius/nearFarClipDist);

#define NUM_PASSES 2
	float occlusion = 0.0f;
	for (int j = 0; j < NUM_PASSES; j++)
	{
		// Get Rotation Vector
		half2 rotateUV = Input.texCoord * (7.0f + (float)j);
		half3 rotation = tex2D(RotationSampler, rotateUV).xyz * 2.0f - 1.0f; 

		for (int i = 0; i < 8; i++)
		{
			half3 rotatedKernel = reflect(avKernel[i], rotation) * kernelScale;

			//half sampleDepth = tex2D(DepthSampler, rotatedKernel.xy + Input.texCoord.xy).r;
			half sampleDepth = DecodeDepth(tex2D(DepthSampler, rotatedKernel.xy + Input.texCoord.xy));
			//half sampleDepth = ReadDepth(rotatedKernel.xy + Input.texCoord.xy);
			half delta = max(sampleDepth - pixelDepth + rotatedKernel.z, 0.0f);
			half range = abs(delta) / (kernelScale.z * gSSAOParams.z);
			occlusion += lerp( /*saturate*/(delta * gSSAOParams.w), gSSAOParams.x, saturate(range) );
		}
	}
	
	occlusion = occlusion / ( NUM_PASSES * 8.0f );
	Output = occlusion;
	Output = lerp( 0.1f, 0.6f, saturate(Output.x) );
	
	//Output = pixelDepth;
	//Output = tex2D(DepthSampler, Input.texCoord).z;
	//Output = float4(pixelDepth, pixelDepth, pixelDepth, 1.0f);
	//Output = DecodeDepth(tex2D(DepthSampler, Input.texCoord));
	return Output;
}

//------------------------------------------------------------------------------
/**
*/
float4 psMain2( VertexOutput Input ) : COLOR0
{
	float4 Output = float4(1,1,1,1);
	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p1
	{
		VertexShader = compile vs_2_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}