#line 1 "NPR_TF2.fx"
//-------------------------------------------------------------
//	NormalGeometry.fx : 기본 Gemetry
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"

//-------------------------------------------------------------
// vertex shader
//-------------------------------------------------------------
float4x4  matWorldViewProjection 	: ModelViewProjection;
//float4x4  matWorldView				: ModelView;
//float4x4  matWorldToTexProj			: ModelViewProjTexture;

// textures
texture diffuseMap			: DiffuseMap0;
texture toonMap				: ToonMap;
texture normalMap			: NormalMap;
texture cubeMap				: CubeMap0;

float4 lightPosition : LightPosition;
float4 lightDiffuse : LightDiffuse;

float4 globalAmbient : GlobalAmbient;
float3 eyePosition : EyePosition;

struct VS_INPUT 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;	
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;	
};

struct VS_OUTPUT 
{
	float4 pos : POSITION0;
	float4 diff : COLOR0; 
	float2 tex0 : TEXCOORD0; 
	float3 norm : TEXCOORD1;
	float3 eyeVec : TEXCOORD2;
	float3 lightDir : TEXCOORD3;
};

VS_OUTPUT vsNormalGeometry( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	float4 position = TransformPosition( Input.pos, matWorldViewProjection );
	float3 normal = TransformNormal( Input.norm, matWorldViewProjection );

	Output.pos = position;
	Output.tex0 = Input.tex0;

	//build object to tangent space transform matrix 
	float3x3 tangentXForm;
	tangentXForm[0] = Input.tangent;
	tangentXForm[1] = Input.norm;
	tangentXForm[2] = Input.binormal;

	float3 lightDir = LightDirection( Input.pos, lightPosition );
	Output.lightDir = lightDir;

	float3 eye = eyePosition - Input.pos.xyz;
	
	Output.norm = Input.norm;
	Output.diff = Input.diff;
	Output.eyeVec = normalize( eye );	
	Output.lightDir = lightDir;

	return( Output );
}

//-------------------------------------------------------------
// Pixel Shader
//-------------------------------------------------------------
sampler diffuseMap_Sampler = sampler_state
{
   Texture = <diffuseMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler normalMap_Sampler = sampler_state
{
   Texture = <normalMap>;
   //ADDRESSU = WRAP;
   //ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler toonMap_Sampler = sampler_state
{
	Texture = <toonMap>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
sampler cubeMap0_Sampler = sampler_state
{
   Texture = <cubeMap>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   ADDRESSW = CLAMP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

//==================================================================
/**	Wrap
	@remarks	0~1의 범위안에 scalar를 RGB COLOR로 매핑하는 함수
	
	@param Intensity	: HalfLambert 결과
	@result float4		: RGB ( 0~2의 범위를 가진다. )
*/
//==================================================================
float4 Wrap( in float Intensity )
{
	float4 shadeColor = tex1D( toonMap_Sampler, Intensity );
	
	return shadeColor;
}

//==================================================================
/** Gooch Shading
	I : Surface RGB Color
	
	I = (1 + dot(n,l))*0.5f * coolColor + (1- (1+dot(n,l))*0.5f) * warmColor
	
	coolColor = blueColor + A*Kd(SurfaceColor);
	warmColor = yellowColor + B*Kd(SurfaceColor);
*/
//==================================================================
float3 GoochShading( in float Intensity )
{
	return float3(1.0f, 1.0f, 1.0f);
}

//==================================================================
/** 
*/
//==================================================================
float4 ps( VS_OUTPUT Input ) : COLOR
{
	float4 output;

	// View Independent Lighting Terms
		
	float3 normal = Input.norm;
	float3 view = Input.eyeVec;
	float3 lightDir = normalize( Input.lightDir );

	// Diffuse	
	float4 diffuseMapColor = tex2D( diffuseMap_Sampler, Input.tex0 );

	float diffuseIntensity = HalfLambert( normal, lightDir, 1 );
	float4 warp = Wrap( diffuseIntensity );
	float4 warpedDiffuse = warp * lightDiffuse;
	warpedDiffuse.w = Input.diff.a;

	float4 ambientCube = texCUBE( cubeMap0_Sampler, Input.norm );
	
	float4 diffuse = diffuseMapColor * (warpedDiffuse + ambientCube);

	// Rim Light
	//float4 rimLight = Rim( normal, view, lightDir, 5.0f, lightDiffuse );
	//output = diffuse + rimLight;

	// View Dependent Lighting Terms
	
	// Fresnel
	float NdotV = dot( normal, view );
	float fs = pow( 1.0f - NdotV, 0.1 );	// artist tuned Frsnel term
	float fr = pow( 1.0f - NdotV, 4 );		// rim Fresnel term

	// Multiple Phong terms Per Light
	float spec = fs * Phong( view, lightDir, normal, 16 );
	float rim = fr * Phong( view, lightDir, normal, 2 );

	float3 phongTerm = max( spec, rim ) * lightDiffuse.xyz;
	
	// Dedicated rim lighting
	const float3 up = float3( 0.0, 1.0f, 0.0f );
	float NdotU = dot( normal, up );
	
	float4 ambientView = texCUBE( cubeMap0_Sampler, view );
	float3 dedicatedRim = ambientView * NdotU * fr;

	float3 specular = phongTerm + dedicatedRim;
	
	output = diffuse + float4( specular, 0.0f );

	return output;
}

//==================================================================
//==================================================================
float4 psNormalGeometry( VS_OUTPUT Input ) : COLOR
{
	float4 result;

	//1. HalfLambert
	float3 normal = Input.norm;
	normal = normalize(normal);

	float3 lightDir = normalize(Input.lightDir);

	float intensity = HalfLambert( normal, lightDir, 1 );
	result = intensity;

	//2. warp function
/*
	const float3 darkColor = float3(0.0f, 0.0f, 0.0f);
	const float3 liteColor = float3(0.8f, 0.5f, 0.1f);

	const float3 warmColor = float3(0.5f, 0.5f, 0.05f);
	const float3 coolColor = float3(0.05f, 0.05f, 0.05f);
	
	float3 mixer = 0.5f * (intensity+1.0f);	
	float3 surfColor = lerp( darkColor, liteColor, mixer );
	float3 toneColor = lerp( coolColor, warmColor, mixer );

	float3 mixColor = surfColor + toneColor;

	result = float4( mixColor, 1.0f );
*/
	float4 mapColor = tex2D( diffuseMap_Sampler, Input.tex0 );	
	float4 shadeColor = tex1D( toonMap_Sampler, intensity );

	// Gooch Shading
	const float3 kBlue = float3(0.0f, 0.0f, 1.0f);
	const float3 kYellow = float3(0.4f, 0.4f, 0.0f);

	//float3 surfDiffuse = float3( 1.0f, 0.7f, 0.5f );
	float3 surfDiffuse = float3( 1.0f, 1.0f, 1.0f );

	float3 coolColor = kBlue;// + (0.1f * surfDiffuse);
	float3 warmColor = kYellow + (0.8f * surfDiffuse);

	float3 mixer = 0.5f * (intensity+1.0f);	

	float3 toneColor = lerp( coolColor, warmColor, mixer );
	result = float4( toneColor, 1.0f );

	float4 lambert = intensity;
	//result = lambert;
	//result = saturate( shadeColor * 2.0f );
	//result += lambert;
	
	//result = saturate( result );
	
	//result = intensity + (shadeColor*2.0f);
	result *= lightDiffuse;	
		
	//result = intensity + shadeColor;
	//result *= 2.0f;

	float4 diffuse = float4( 1.0f, 200.0f/255.f, 100.0f/255.0f, 1.0f );
	float4 srcBlend = intensity*0.8f;
	float4 destBlend = (shadeColor*2.0f)*0.2f;
	//result = srcBlend + destBlend;
	//result *= lightDiffuse;

	//3. color
	//4. ambientOcclusion
//	float4 ambient = tex2D( ambientOcclusionMap_Sampler, Input.tex0 );
//	result.xyz = result.xyz + ambient * 0.5f;
	float3 ambient = float3( 0.6f, 0.25f, 0.5f );
	result.xyz += ambient;
	
	result.xyz = saturate( result.xyz );
	
	result.w = 1.0f;

	result *= mapColor;

	return result;
}

//--------------------------------------------------------------//
// Technique Section for Effect Workspace.
//--------------------------------------------------------------//
technique Geometry
{
	pass NormalPass
	{
		VertexShader = compile vs_2_0 vsNormalGeometry();
		PixelShader	= compile ps_2_0 psNormalGeometry();
	}
}
