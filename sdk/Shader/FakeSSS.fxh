//******************************************************************************
/**	@author		cagetu
	@brief		FakeSSS Shader 만들기
				마비노기 영웅전에서 사용되었다고 알려진 방법
	@see		http://developer.download.nvidia.com/SDK/9.5/Samples/3dgraphics_effects.html
				에서 lambSkin.fxproj를 참고한다.
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
float4 gSurfaceColor  = {0.9f, 1.0f, 0.9f, 1.0f};
float4 gSubColor  = {1.0f, 0.2f, 0.2f, 1.0f};
float gRollOff = 0.4f;

//------------------------------------------------------------------------------
/**
	Subsurface 의 색상을 얻어온다.
*/
float4
GetSubsurfaceLite(float DiffuseIntensity)
{
	//float LdotN = dot(Light, Normal);
	//float diffuseComponent = max(0, LdotN);
	//Diffuse = float4((diffuseComponent * gSurfaceColor).xyz, 1.0f);

	float subLamb = smoothstep(-gRollOff, 1.0f, DiffuseIntensity) - smoothstep(0.0f, 1.0f, DiffuseIntensity);
	subLamb = max(0.0, subLamb);

	return subLamb * gSubColor;
}

