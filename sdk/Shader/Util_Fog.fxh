#ifndef __UTIL_FOG_FXH__
#define __UTIL_FOG_FXH__

//-------------------------------------------------------------
//	util_fog.fxh : 장면의 fog에 대한 처리
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

float cHeightFalloff : 1.0f;

//-------------------------------------------------------------
/**
	Developed a simpler method to compute height/distance based fog with exponential fall-off
*/
float ComputeVolumetricFog( float3 cameraToWorldPos )
{
	// NOTE: cVolFogHeightDensityAtViewer = exp( -cHeightFalloff * cViewPos.z );
	float cVolFogHeightDensityAtViewer = 1.0f;
	
	float fogInt = length(cameraToWorldPos) * cVolFogHeightDensityAtViewer;
	
	const float cSlopeThreshold = 0.01;
	if (abs(cameraToWorldPos.z) > cSlopeThreshold)
	{
		float t = cHeightFalloff * cameraToWorldPos.z;
		fogInt *= (1.0 - exp(-t)) / t;
	}

	return exp( -cGlobalDentisy * fogInt );
}

#endif	// __UTIL_FOG_FXH__