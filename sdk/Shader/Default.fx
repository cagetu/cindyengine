#line 1 "Default.fx"
//-------------------------------------------------------------
//	NormalGeometry.fx : �⺻ Gemetry
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
#include "..\\..\\sdk\\Shader\\DepthBlur.fxh"

//-------------------------------------------------------------
// vertex shader
//-------------------------------------------------------------
float4x4  matWorldViewProjection 	: ModelViewProjection;
float4x4  matWorldView				: ModelView;
//float4x4  matWorldToTexProj			: ModelViewProjTexture;

texture diffuseMap			: DiffuseMap0;
texture shadowMap			: ShadowMap;

float4 lightPosition : LightPosition;
float4 lightDiffuse : LightDiffuse;

float4 globalAmbient : GlobalAmbient;

struct VS_INPUT 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;	
};

struct VS_OUTPUT 
{
	float4 pos : POSITION0;
	float4 diff : COLOR0; 
	float2 tex0 : TEXCOORD0; 
	float depth : TEXCOORD1;
	float4 texProj : TEXCOORD2;
};

VS_OUTPUT vsNormalGeometry( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	float4 position = TransformPosition( Input.pos, matWorldViewProjection );
	float3 normal = TransformNormal( Input.norm, matWorldViewProjection );

	Output.pos = position;
	//Output.tex0 = Input.tex0;
	Output.tex0 = UnpackUv(Input.tex0);	

	float3 lightDir = LightDirection( Input.pos, lightPosition );
	
	//float diffuseIntensity = Lambert( normalize, lightDir );
	float diffuseIntensity = HalfLambert( normal, lightDir, 2 );

	float4 diffuse = lightDiffuse * matDiffuse * Input.diff * diffuseIntensity;

	Output.diff = diffuse + globalAmbient;
	Output.diff.a = Input.diff.a;

	//float3 view = mul( Input.pos, (float4x3)matWorldView );
	//Output.depth = view.z;

	//Output.texProj = mul( Input.pos, matWorldToTexProj );
	
	return( Output );
}

VS_OUTPUT vsNoShade( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );
	Output.tex0 = Input.tex0;
	//Output.tex0 = UnpackUv(Input.tex0);	

	return( Output );
}

struct vsNormalInput
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
};

VS_OUTPUT vsDefault( vsNormalInput Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );
	Output.tex0 = Input.tex0;
	Output.diff = matDiffuse;

	return( Output );
}

//-------------------------------------------------------------
// Pixel Shader
//-------------------------------------------------------------
sampler diffuseMap_Sampler = sampler_state
{
   Texture = <diffuseMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler shadowMap_Sampler = sampler_state
{
   Texture = <shadowMap>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   ADDRESSW = CLAMP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

struct PS_INPUT 
{
	float4	diff : COLOR0;
	float2	tex0 : TEXCOORD0;
	float depth : TEXCOORD1;
	float4 texProj : TEXCOORD2;
};

struct psOutput
{
	float4 color0 : COLOR0;
	float4 color1 : COLOR1;
};

float4 psNoShade( PS_INPUT Input ) : COLOR
{
	float4 result = (float4)0;
	
	float4 diffuseMapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	
	result = diffuseMapColor;
	return result;
}

float4 psDefault( PS_INPUT Input ) : COLOR
{
	float4 result = (float4)0;
	
	float4 diffuseMapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	
	result = diffuseMapColor * Input.diff;
	return result;
}

psOutput psNormalGeometry( PS_INPUT Input )
{
	psOutput Output;
	
	float4 mapColor = tex2D( diffuseMap_Sampler, Input.tex0 );

	float4 diffuse = mapColor;

	float2 shadowTexCoord = Input.texProj.xy / Input.texProj.w;
	float  shadowTestDepth = Input.texProj.z / Input.texProj.w;

	//float  shadowDepth = tex2D(shadowMap_Sampler, shadowTexCoord);
	//float  shadow      = (shadowTestDepth <= shadowDepth);
	//diffuse.rgb = ((shadow*0.2+0.8)*mapColor.rgb)*0.9 + 0.1;

	//float3 shadow = tex2D(shadowMap_Sampler, Input.texProj).rgb;
	//diffuse.rgb = ((shadow*0.2+0.8) * diffuse.rgb)*0.9 + 0.1;

	//float4 shadowColor = Input.texProj.w < 0 ? 1 : tex2Dproj(shadowMap_Sampler, Input.texProj );
	//diffuse.rgb *= shadowColor.rgb;

	float4 result = diffuse * Input.diff;

	Output.color0 = result;
	Output.color1 = ComputeDepthBlur( Input.depth );

	return Output;
}

//--------------------------------------------------------------//
// Technique Section for Effect Workspace.
//--------------------------------------------------------------//
technique Geometry
{
	pass NormalPass
	{
		VertexShader = compile vs_2_0 vsNormalGeometry();
		//PixelShader	= compile ps_2_0 psNormalGeometry();
		PixelShader	= compile ps_2_0 psDefault();
	}
}

technique Default
{
	pass NormalPass
	{
		VertexShader = compile vs_2_0 vsDefault();
		PixelShader	= compile ps_2_0 psDefault();
	}
}
technique NoShade
{
	pass NormalPass
	{
		VertexShader = compile vs_1_1 vsNoShade();
		PixelShader	= compile ps_2_0 psNoShade();
	}
}
