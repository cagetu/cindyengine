#line 1 "Hdr.fx"
//-------------------------------------------------------------
//	Hdr.fx : 기본 Gemetry
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

sampler s0 : register(s0);
sampler s1 : register(s1);
sampler s2 : register(s2);
sampler s3 : register(s3);
sampler s4 : register(s4);
sampler s5 : register(s5);

float2 g_aDownSample2x2Offsets[4] : Down2x2SampleOffsets;
//==================================================================
/** DownFilter2x2
	@remarks	2x2만큼 축소 필터링 한다.
*/
//==================================================================
float4 psDownFilter2x2( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;

	for (int i=0; i<4; ++i)
		color += tex2D( s0, inTex + g_aDownSample2x2Offsets[i] );
		
	return color/4;
}

float2 g_aSampleOffsets[16] : Down4x4SampleOffsets;
//==================================================================
/** DownFilter4x4
	@remarks	4x4만큼 축소 필터링 한다.
*/
//==================================================================
float4 psDownFilter4x4( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;

	for (int i=0; i<16; ++i)
		color += tex2D( s0, inTex + g_aSampleOffsets[i] );
		
	return color/16;
}

float2 g_aLuminanceSampleOffsets[16] : LuminanceSampleOffsets;

// The per-color weighting to be used for luminance calculations in RGB order.
static const float3 luminanceVector = float3(0.2125f, 0.7154f, 0.0721f);

//==================================================================
/** SampleLuminanceAverageLog
	@remarks	소스 이미지의 밝은 부분을 추출하고, log()의 평균이
				포함된 scaled image를 반환한다.
*/
//==================================================================
float4 psSampleLuminanceAverageLog( in float2 inTex : TEXCOORD0 ) :COLOR
{
	float3 sample = 0;
	float logLuminanceSum = 0;

	// sample Point를 통해, log(luminace)의 합을 계산한다.
	for (int i=0; i < 9; i++)
	{
		sample = tex2D( s0, inTex + g_aLuminanceSampleOffsets[i] );
		logLuminanceSum += log( dot(sample, luminanceVector) + 0.0001f );
	}
	
	// 평균 산출
	logLuminanceSum /= 9;
	
	return float4( logLuminanceSum, logLuminanceSum, logLuminanceSum, 1.0f );
}

//==================================================================
/** ReSampleLuminanceDown4x4
	@remarks	sample point들의 blending에 의해 밝은 부분 텍스쳐를
				down scale.
*/
//==================================================================
float4 psResampleLuminanceAverage( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float reSampleSum = 0.0f;

	// sample point들에 대해 luminance들을 합한다.
	for (int i=0; i < 16; i++)
	{
		reSampleSum += tex2D( s0, inTex + g_aLuminanceSampleOffsets[i] );
	}
	
	reSampleSum /= 16;
	
	return float4( reSampleSum, reSampleSum, reSampleSum, 1.0f );
}

//==================================================================
/** ReSampleLuminanceExp
	@remarks	평균 휘도를 구하고, exp() 연산을 하여, 평균 luminance
				구하기를 완료
*/
//==================================================================
float4 psSampleLuminanceExp( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float reSampleSum = 0.0f;

	// sample point들에 대해 luminance들을 합한다.
	for (int i=0; i < 16; i++)
	{
		reSampleSum += tex2D( s0, inTex + g_aLuminanceSampleOffsets[i] );
	}
	
	// 평균을 구하고, 평균 luminance 계산을 완료하기 위해, exp() 연산을 한다.
	reSampleSum = exp(reSampleSum/16);

	return float4( reSampleSum, reSampleSum, reSampleSum, 1.0f );
}

float g_fMiddleGray = 0.18f;
static const float  BRIGHT_PASS_THRESHOLD  = 5.0f;  // Threshold for BrightPass filter
static const float  BRIGHT_PASS_OFFSET     = 10.0f; // Offset for BrightPass filter

//==================================================================
/** BrightPassFilter
	@remarks	밝은 부분을 추출한다.
*/
//==================================================================
float4 psBrightPassFilter( in float2 inTex : TEXCOORD0 ) :COLOR
{
	float4 sample = tex2D( s0, inTex );
	float adaptedLuminance = 0;
	
	// tone-mapping의 후에 pixel의 값을 결정한다.
	//sample.rgb *= g_fMiddleGray / (adaptedLuminance + 0.001f);
	
	// 어두운 픽셀들을 제외
	//sample.rgb -= BRIGHT_PASS_THRESHOLD;
	
	// 0~1사이로 결과값을 map한다. 특정값보다 높은 값은 빛이 비추어진 장면 오브젝트들
	// 로 부터 분리된 라이트들일 것이다.
	//sample.rgb /= (BRIGHT_PASS_OFFSET+sample);

	sample.rgb -= 1.0f;
	sample.rgb = 3.0f * max(sample, 0.0f);

	return sample;
}

float2 g_aGaussSampleOffsets[16] : GauseBlur5x5SampleOffsets;
float4 g_aGaussSampleWeights[16] : GauseBlur5x5SampleWeights;

//==================================================================
/** GaussBlur5x5
	@remarks	중심 점에서 가까운 12개의 point를 표본으로 5x5 gaussian blur를 
				계산한다.
*/
//==================================================================
float4 psGaussBlur5x5( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 sample = 0;
	
	for (int i=0; i<13; ++i)
		sample += g_aGaussSampleWeights[i] * tex2D( s0, inTex + g_aGaussSampleOffsets[i] );

	return sample;
}

float2 g_aStarSampleOffsets[16] : HDRStarSampleOffsets;
float4 g_aStarSampleWeights[16] : HDRStarSampleWeights;

//==================================================================
/** StarEffect
	@remarks	별 효과를 만든다.
*/
//==================================================================
float4 psStarEffect( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 sample = 0;
	float4 color = 0;
	
	float2 samplePosition = 0;
	for (int i=0; i<8; ++i)
	{
		samplePosition = inTex + g_aStarSampleOffsets[i];
		sample = tex2D( s0, samplePosition );
		color += g_aStarSampleWeights[i] * sample;
	}
	
	return color;
}

//==================================================================
/** MergeTexture
	@remarks	텍스쳐를 합친다.
*/
//==================================================================
float4 psMergeStarTextures( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;
	
	color += tex2D( s0, inTex );
	color += tex2D( s1, inTex );
	color += tex2D( s2, inTex );
	color += tex2D( s3, inTex );
	color += tex2D( s4, inTex );
	color += tex2D( s5, inTex );
	
	return color/6.0f;
}

//==================================================================
/** Final Scene
	@remarks	장면을 랜더링한다.
*/
//==================================================================
float4 psFinalScene( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = tex2D( s0, inTex );
	float4 star = tex2D( s1, inTex );
	
	color += star;
	
	return color;
}

// 2x2 축소 샘플링
technique DownFilter2x2
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psDownFilter2x2();
	}
}

// 4x4 축소 샘플링
technique DownFilter4x4
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psDownFilter4x4();
	}
}

// Luminance Sample 평균
technique LuminanceSampleAvg
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psSampleLuminanceAverageLog();
	}
}

technique LuminanceResampleAvg
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psResampleLuminanceAverage();
	}
}

technique LuminanceResampleAvgExp
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psSampleLuminanceExp();
	}
}

// 밝은 부분
technique BrightPassFilter
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psBrightPassFilter();
	}
}

// GaussBlur5x5
technique GaussBlur5x5
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psGaussBlur5x5();
	}
}

// Star Effect
technique StarEffect
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psStarEffect();
	}
}

// Star Effect Merge Texture
technique MergeStarTexture
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psMergeStarTextures();
	}
}

// scene
technique FinalScene
{
	Pass Default
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psFinalScene();
	}
}