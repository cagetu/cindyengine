#line 1 "NormalSpecMap.fx"
//-------------------------------------------------------------
//	NormalGeometry.fx : 기본 Gemetry
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"

//-------------------------------------------------------------
// vertex shader
//-------------------------------------------------------------
float4x4  matWorldViewProjection 	: ModelViewProjection;
float4x4  matWorldView				: ModelView;

float4 lightDiffuse : LightDiffuse;
float4 lightSpecular : LightSpecular;
float3 lightPosition : LightPosition;


float4 g_GlobalAmbient :GlobalAmbient;

float3 eyePosition : EyePosition;

texture diffuseMap			: DiffuseMap0;
texture normalMap			: NormalMap;
texture specularMap			: SpecularMap;
texture specularColorMap	: SpecularColorMap;
texture emissiveMap			: EmissiveMap;
texture occlusionMap		: OcclusionMap;

struct VS_INPUT 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;	
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
};

struct vsSkinnedInput 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;	
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
};

struct VS_OUTPUT 
{
	float4 pos : POSITION0;
	float4 diff : COLOR0; 
	float2 tex0 : TEXCOORD0; 
	float3 lightVec : TEXCOORD1;
	float3 eyeVec : TEXCOORD2;
	float  depth : TEXCOORD3;	
};

//------------------------------------------------------------------------------
/**
*/
VS_OUTPUT vsNormalGeometry( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );

#ifdef __PACK_VERTEX__
	Output.tex0 = UnpackUv(Input.tex0);
#else
	Output.tex0 = Input.tex0;
#endif	// __PACK_VERTEX__
	Output.diff = Input.diff;

	//build object to tangent space transform matrix 
	float3x3 tangentXForm;

#ifdef __PACK_VERTEX__
	tangentXForm[0] = normalize(UnpackNormal(Input.tangent));
	tangentXForm[1] = normalize(UnpackNormal(Input.binormal));
	tangentXForm[2] = normalize(UnpackNormal(Input.norm));
#else
	tangentXForm[0] = Input.tangent;
	tangentXForm[1] = Input.binormal;
	tangentXForm[2] = Input.norm;
#endif	// __PACK_VERTEX__
	tangentXForm[0] = normalize(UnpackNormal(Input.tangent));
	tangentXForm[1] = normalize(UnpackNormal(Input.binormal));
	tangentXForm[2] = normalize(UnpackNormal(Input.norm));

	float3 lightDir = normalize(lightPosition - Input.pos.xyz); 
	Output.lightVec = mul( tangentXForm, lightDir );

	float3 eyeVec = eyePosition - Input.pos.xyz;	//normalize(eyePosition - Input.pos.xyz);
	Output.eyeVec = mul( tangentXForm, eyeVec );

	//Output.diff = dot(Input.norm, lightDir)*0.5f + 0.5f;
	Output.diff = float4(Input.tangent, 1.0);
	
	// depth
//	float3 view = mul( Input.pos, (float4x3)matWorldView );
//	float4 focalPlane = float4(0.0f, 0.0f, 1.0f, -8.0f);
//	float blurFactor = dot( float4(view, 1.0f), focalPlane) * 0.4f;
//	Output.depth = blurFactor*blurFactor;
//	float MaxBlurFactor = 3.0f / 4.0f;
//	Output.depth = min( Output.depth, MaxBlurFactor );
	
	float3 view = mul( Input.pos, (float4x3)matWorldView );
	Output.depth = view.z;

	return( Output );
}

//-------------------------------------------------------------
// Pixel Shader
//-------------------------------------------------------------
sampler diffuseMap_Sampler = sampler_state
{
   Texture = <diffuseMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler normalMap_Sampler = sampler_state
{
   Texture = <normalMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler specularMap_Sampler = sampler_state
{
   Texture = <specularMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler specularColorMap_Sampler = sampler_state
{
   Texture = <specularColorMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

sampler emissiveMap_Sampler = sampler_state
{
	Texture = <emissiveMap>;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;	
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

sampler occlusionMap_Sampler = sampler_state
{
	Texture = <occlusionMap>;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;	
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

struct PS_INPUT 
{
	float4	diff : COLOR0;
	float2	tex0 : TEXCOORD0;
	float3	lightVec : TEXCOORD1;
	float3 	eyeVec : TEXCOORD2;
	float	depth : TEXCOORD3;
};

struct psOutput
{
	float4 color0 : COLOR0;
	float4 color1 : COLOR1;
};

//------------------------------------------------------------------------------
/**
*/
psOutput psGeometry( PS_INPUT Input )
{
	psOutput Output = (psOutput)0;

	// normal
	float4 normalMap = tex2D( normalMap_Sampler, Input.tex0 );
	float3 normal = normalize( Expand( normalMap.xyz ) );

	// lightDir / eye
	float3 lightDir = Input.lightVec;
	float3 eye = Input.eyeVec;

	// emissive
	float4 emissive = tex2D( emissiveMap_Sampler, Input.tex0 );

	// ambient
	float4 ambientOcclusion = tex2D( occlusionMap_Sampler, Input.tex0 );
	float3 globalAmbient = g_GlobalAmbient.xyz * ambientOcclusion.rgb;
	float3 ambient = globalAmbient * matAmbient.rgb;

	// diffuses
	float4 diffuseMap = tex2D( diffuseMap_Sampler, Input.tex0 );

	float diffuseIntensity = HalfLambert( normal, lightDir, 2 );
	float4 diffuse = float4( lightDiffuse.rgb*matDiffuse.rgb*diffuseIntensity, matDiffuse.a );

	float4 viewInDependentColor = diffuseMap * float4( emissive.rgb + ambient + diffuse.rgb, diffuse.a );

	// specular
	float4 specMap = tex2D( specularMap_Sampler, Input.tex0 );
	float4 specColorMap = tex2D( specularColorMap_Sampler, Input.tex0 );

	float3 specColor = specMap.rgb * specColorMap.rgb;// * diffuse.a;
	float3 specular = specColor * lightSpecular.rgb * matSpecular.rgb;

	float specIntensity = Blinn( eye, lightDir, normal, 32 );
	//float specIntensity = Phong( eye, lightDir, normal, matSpecular.w );

	float3 spec = specIntensity * specular * diffuseIntensity;

	// Rim Light
	float4 rimLight = 0;	//Rim( normal, eye, lightDir, 32 );
	float4 viewDependentColor = float4( spec + rimLight.rgb, 0.0f );

	// Output
	Output.color0 = viewInDependentColor + viewDependentColor;
	//Output.color1 = ComputeDepthBlur( Input.depth );

	//Output.color0 = Input.diff;
	return Output;
}


//--------------------------------------------------------------//
// Technique Section for Effect Workspace.
//--------------------------------------------------------------//
technique Geometry
{
	pass NormalPass
	{
		VertexShader = compile vs_1_1 vsNormalGeometry();
		PixelShader	= compile ps_2_0 psGeometry();
	}
}

technique Depth
{
	// 알파테스트 완료..
	pass Default
	{
		VertexShader = compile vs_1_1 vsNormalGeometry();
		PixelShader	= compile ps_2_0 psGeometry();	
	}
}
