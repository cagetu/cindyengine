#line 1 "pe_radialblur.fx"
//------------------------------------------------------------------------------
/** pe_radialblur.fx : Radial Blur Post-Effect.
	Copyright ?2008 cagetu, Inc. All Rights Reserved.

	[참고자료]	
		http://www.gamerendering.com/category/special-effects/page/2/
*/
//------------------------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"

//-------------------------------------------------------------
// Constants
//-------------------------------------------------------------
float4x4 gWorldViewProj : ModelViewProjection;

// texture
texture gColorTexture : DiffuseMap0;

const float sampleDist = 1.0;
const float sampleStrength = 2.2;

//-------------------------------------------------------------
// Sampler
//-------------------------------------------------------------
sampler ColorSampler = sampler_state
{
    Texture = <gColorTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

//-------------------------------------------------------------
/**	Vertex Shader 구조체
*/
//-------------------------------------------------------------
struct VertexInput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
VertexOutput vsMain( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;
	
	output.position = mul(Input.position, gWorldViewProj);
	output.texcoord = (float2(output.position.x, -output.position.y) + float2(1.0, 1.0)) / 2.0;

	return output;
}

// some sample positions
float samples[10] = {-0.08, -0.05, -0.03, -0.02, -0.01, 0.01, 0.02, 0.03, 0.05, 0.08};

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
float4 psMain( VertexOutput Input ) : COLOR
{
    // 0.5, 0.5 is the center of the screen
    // so substracting uv from it will result in
    // a vector pointing to the middle of the screen
	float2 dir = 0.5 - Input.texcoord;
	
    // calculate the distance to the center of the screen
	float2 dist = sqrt(dir.x*dir.x + dir.y*dir.y);
	
    // normalize the direction (reuse the distance)
    dir = dir/dist; 
    
    // this is the original colour of this fragment
    // using only this would result in a nonblurred version
    float4 color = tex2D(ColorSampler, Input.texcoord); 
    
	#ifdef _SRGB_
	//color = sRGBtolinearRGB( color );
	#endif
	
    float4 sum = color;
 
    // take 10 additional blur samples in the direction towards
    // the center of the screen
    for (int i = 0; i < 10; i++)
    {
		sum += tex2D( ColorSampler, Input.texcoord + dir * samples[i] * sampleDist );
    }
 
    // we have taken eleven samples
    sum *= 1.0/11.0;
 
    // weighten the blur effect with the distance to the
    // center of the screen ( further out is blurred more)
    float t = dist * sampleStrength;
    t = clamp( t, 0.0, 1.0); //0 &lt;= t &lt;= 1
 
    //Blend the original color with the averaged pixels
    color = lerp( color, sum, t );
    
	#ifdef _SRGB_
	//color = linearRGBtosRGB(color);
	#endif
	
	return color;
}

//-------------------------------------------------------------
/**	Technique
*/
//-------------------------------------------------------------
technique RenderScreenQuad
{
	Pass Default
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}

// scene
technique FinalScene
{
	Pass Default
	{
/*
        ZWriteEnable     = False;
        ZEnable          = False;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        AlphaBlendEnable = False;
        AlphaTestEnable  = False;
        CullMode         = None;
        StencilEnable    = False;
*/
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}
