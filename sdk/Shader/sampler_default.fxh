//******************************************************************************
/**	@author		cagetu
	@brief		�⺻ Texture Sampler Shader
*/
//******************************************************************************

#ifndef MipLodBias
#define MipLodBias -0.5f
#endif

#ifdef _DIFFUSE_MAP_ 
texture gDiffuseMap0 : DiffuseMap0;
sampler DiffuseMapSampler0 = sampler_state
{
   Texture = <gDiffuseMap0>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _ALPHA_MAP_
texture gAlphaMap : AlphaMap;
sampler AlphaMapSampler = sampler_state
{
   Texture = <gAlphaMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _NORMAL_MAP_
texture gNormalMap : NormalMap;
sampler NormalMapSampler = sampler_state
{
   Texture = <gNormalMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _SPEC_MAP_
texture gSpecularMap : SpecularMap;
sampler SpecularMapSampler = sampler_state
{
   Texture = <gSpecularMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _SPEC_EXP_MAP_
texture gSpecularExpMap : SpecularExpMap;
sampler SpecularExpMapSampler = sampler_state
{
   Texture = <gSpecularExpMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _TOON_MAP_
texture gToonMap : ToonMap;
sampler ToonMapSampler = sampler_state
{
	Texture = <gToonMap>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _GLOW_MAP_
texture gGlowMap : GlowMap;
sampler GlowMapSampler = sampler_state
{
	Texture = <gGlowMap>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
    MIPFILTER = LINEAR;
	MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _EMISSIVE_MAP_
texture gEmissiveMap : EmissiveMap;
sampler EmissiveMapSampler = sampler_state
{
	Texture = <gEmissiveMap>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
    MIPFILTER = LINEAR;
	MipMapLodBias = <MipLodBias>;
};
#endif

#ifdef _REFLECT_MAP_
texture gReflectMap : ReflectMap;
sampler ReflectMapSampler = sampler_state
{
	Texture = <gReflectMap>;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
    MIPFILTER = Point;
	MipMapLodBias = <MipLodBias>;
};
#endif