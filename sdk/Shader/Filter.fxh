#line 1 "Filter.fxh"
//-------------------------------------------------------------
//	Filter.fxh : Filter(Sampling) 기능
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

texture sourceTexture : SourceBuffer;
sampler SourceSampler = sampler_state
{
	Texture = <sourceTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;	
};

float2 filterSampleOffsets[16] : FilterSampleOffsets;
float4 filterSampleWeights[16] : FilterSampleWeights;

//==================================================================
/** DownFilter2x2
	@brief	2x2만큼 축소 필터링 한다.
*/
//==================================================================
float4 psDownFilter2x2( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;

	for (int i=0; i<4; ++i)
		color += tex2D( SourceSampler, inTex + filterSampleOffsets[i] );

	return color/4;
}

//==================================================================
/** DownFilter4x4
	@brief	4x4만큼 축소 필터링 한다.
*/
//==================================================================
float4 psDownFilter4x4( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;

	for (int i=0; i<16; ++i)
		color += tex2D( SourceSampler, inTex + filterSampleOffsets[i] );
		
	return color/16;
}


//==================================================================
/** GaussBlur5x5
	@brief	중심 점에서 가까운 12개의 point를 표본으로 5x5 gaussian blur를 
				계산한다.
*/
//==================================================================
float4 psGaussBlur5x5( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 sample = 0;
	
	for (int i=0; i<13; ++i)
		sample += filterSampleWeights[i] * tex2D( SourceSampler, inTex + filterSampleOffsets[i] );

	return sample;
}

//==================================================================
/**	GaussBlur3x3
	@brief	중심 점에서 가까운 8개의 point를 표본으로 3x3 gaussian blur를 
				계산한다.
*/
//==================================================================
float4 psGaussBlur3x3( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 sample = 0;
	
	for (int i=0; i<9; ++i)
		sample += filterSampleWeights[i] * tex2D( SourceSampler, inTex + filterSampleOffsets[i] );

	return sample;	
}