#line 1 "LightPrePass.fxh"
//*************************************************************
//	LightPrePass.fxh : Light Pre Pass Header
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//*************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"
//#define _SKIN_LIGHTING_
#ifdef _SKIN_LIGHTING_
#include "..\\..\\sdk\\Shader\\FakeSSS.fxh"
#endif

//--------------------------------------------------------------//
// Constants
//--------------------------------------------------------------//
shared float4 gViewInfo : ViewInfo;
shared float2 gScreenSize : DisplayResolution;
shared float2 gUVAdjust : ScreenUVAdjust;

/// 카메라 공간으로 변환된 라이트의 변수들이다...
shared float3 gLightPosition : LightPosition;
shared float3 gLightAttenuation : LightAttenuation = {1.0f, 1.0f, 1.0f};
shared float4 gLightDiffuse : LightDiffuse = {1.0f, 1.0f, 1.0f, 1.0f};
shared float  gInvSqrLightRange : LightRange = 1.0f;
shared float  gSpecularPower = 2.0f;

shared float4x4 gWorldViewProj : ModelViewProjection;
shared float4x4 gWorldView : ModelView;
shared float4x4 gInvView : InvView;

//--------------------------------------------------------------//
// Texture & Sampler
//--------------------------------------------------------------//
texture gDepthBuffer : DiffuseMap0;
sampler DepthSampler = sampler_state
{
   Texture = <gDepthBuffer>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MINFILTER = POINT;
   MAGFILTER = POINT;
   MIPFILTER = NONE;
};

texture gNormalBuffer : DiffuseMap1;
sampler NormalSampler = sampler_state
{
   Texture = <gNormalBuffer>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MINFILTER = POINT;
   MAGFILTER = POINT;
   MIPFILTER = NONE;
};

//--------------------------------------------------------------//
// Structures
//--------------------------------------------------------------//
/// 입력
struct VertexInput
{
    float4 position : POSITION0;
    float2 texcoord	: TEXCOORD0;
};
/// 출력
struct VertexOutput
{
    float4 position	: POSITION0;
    float2 texcoord	: TEXCOORD0;
    float3 eyeToScreenRay : TEXCOORD1;
};

//--------------------------------------------------------------//
// Functions
//--------------------------------------------------------------//
/// VertexShader
/**
*/
VertexOutput vsMain( VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;
	
	float4 hPos = mul(Input.position, gWorldViewProj);
	output.position = hPos;
	output.texcoord = Input.texcoord;

	float viewAspect = gViewInfo.x;
	float tanFOV = gViewInfo.y;
	hPos.xy /= hPos.w;

	output.eyeToScreenRay = GetEyeToScreenRay(hPos, viewAspect, tanFOV);

	return output;
}
