#line 1 "template_solid.fx"
//******************************************************************************
/**	@author		cagetu
	@brief		SolidTemplate Shader 만들기
*/
//******************************************************************************
#if defined(_DEPTH_) || defined(_SMDEPTH_)
#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\template_depth.fxh"
#else
//------------------------------------------------------------------------------
// DEFINES
//------------------------------------------------------------------------------
#define _RIM_LIGHT_
#define _LIGHTINGONLY_
//#define _NOSPECULAR_
//#define _FASTSPECULAR
#define _PHYSICBASEDLIGHTING_
#define _AMBIENTCUBE

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
#include "..\\..\\sdk\\Shader\\Util_Skinning.fxh"
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"
#ifdef _SHADOW_MAP_
#include "..\\..\\sdk\\Shader\\Util_ShadowMap.fxh"
#endif
#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\sampler_default.fxh"
#ifdef _HAIR_LIGHTING_
#include "..\\..\\sdk\\Shader\\HairLighting.fxh"
#endif
#ifdef _SKIN_LIGHTING_
#include "..\\..\\sdk\\Shader\\FakeSSS.fxh"
#endif

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
/// Global Light
float3 lightDirection	: LightDirection;
//float3 lightPosition	: LightPosition;
float4 lightDiffuse		: LightDiffuse;
//float4 lightSpecular	: LightSpecular;

#ifdef _MULTIVERTEXLIGHTS_
int LocalLightCount : LightCount;
float4 LightPosArray[8] : LightPosArray;
float4 LightColorArray[8] : LightColorArray;
//float LightRangeArray[8] : LightRangeArray;
#endif

#ifdef _PHYSICBASEDLIGHTING_
float MatReflectiveIndex : MATREFLECTIVEINDEX = 1.3f; // 
#endif

float MatEmissiveIntensity : MatEmissiveIntensity = 0.2f;
float MatSpecularIntensity : MatSpecularIntensity = 1.0f;
float MatAmbientIntensity : MatAmbientIntensity = 1.0f;
float MatGlowIntensity : MatGlowIntensity = 4.0f;
float SpecularPower : SpecPower = 32.0f;

const float _PI = 3.141592654f;

#if defined(_SPEC_MAP_) || defined(_REFLECT_MAP_) || defined(_RIM_LIGHT_)
float3 gEyePosition : EyePosition;
#endif

#ifdef _REFLECT_MAP_
float gReflectivity : Reflectivity = 0.1f;
#endif

#ifdef _BLEND_ALPHA_
float gAlphaBlendFactor : AlphaBlendFator = 1.0f;
#endif

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// 입력
struct VertexInput
{
	float4 position		: POSITION0;
	float2 uv0			: TEXCOORD0;
	float3 normal		: NORMAL0;
#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
#endif
#ifdef _NORMAL_MAP_
	float4 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
#endif
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif
};

/// 출력
struct VertexOutput
{
	float4 position		: POSITION0;
	float2 uv0			: TEXCOORD0;
#ifdef _NORMAL_MAP_
	float3 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
	float3 normal		: TEXCOORD1;
#else
	float3 normal		: TEXCOORD1;
#endif
	float3 lightVec		: TEXCOORD2;
#if defined(_SPEC_MAP_) || defined(_REFLECT_MAP_) || defined(_RIM_LIGHT_)
	float3 eyeVec		: TEXCOORD3;	// (eyePos - localPos)
#endif
#ifdef _SHADOW_MAP_
  #if _PSSM_
	float4 localPos		: TEXCOORD4;
  #else
	float4 shadowProjPos : TEXCOORD4;
  #endif
#endif
#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
#endif
#ifdef _MULTIVERTEXLIGHTS_
	float3 lightPos		: TEXCOORD5;
	float4 lightColor	: TEXCOORD6;
	float4 localPos		: TEXCOORD7;
#endif
};

//------------------------------------------------------------------------------
/**
*/
VertexOutput vs_main( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	/// position, normal
	float4 localPos = 0;
	float3 localNor = 0;
	float3 inputNormal = Input.normal;	//normalize(UnpackNormal(Input.normal));

#ifdef _NORMAL_MAP_
	float3 inputTangent = Input.tangent.xyz;	//normalize(UnpackNormal(Input.tangent));
	float3 inputBinormal = Input.binormal;		//normalize(UnpackNormal(Input.binormal));
	float3 localTan = 0;
	float3 localBir = 0;
#endif //_NORMAL_MAP_

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
  #ifdef _NORMAL_MAP_
	localTan = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, gJoints );
	localBir = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, gJoints );
  #endif //_NORMAL_MAP_
#else
	localPos = Input.position;
	localNor = inputNormal;
  #ifdef _NORMAL_MAP_
	localTan = inputTangent;
	localBir = inputBinormal;
  #endif
#endif

	//
	float4 posClip = TransformPosition(localPos, gWorldViewProj);
	Output.position = posClip;	// / posClip.w;

	// tangent, binormal
  #ifdef _NORMAL_MAP_
	Output.tangent = localTan;
	Output.binormal = localBir;
  #endif // _NORMAL_MAP_

	/// normal
	//localNor = TransformNormal(localNor, gWorldViewProj);
	Output.normal = localNor;

	/// lightVec
	//float3 lightVec = lightPosition - localPos.xyz;
	//lightVec = normalize(lightVec);
	Output.lightVec = normalize(lightDirection);

	// uv
	Output.uv0 = Input.uv0;	//UnpackUv(Input.uv0);

#ifdef _VERTEXCOLOR_
	Output.diff = Input.diff;
#endif
	
#if defined(_SPEC_MAP_) || defined(_REFLECT_MAP_) || defined(_RIM_LIGHT_)
	float3 eyeVec = gEyePosition - localPos.xyz;
	eyeVec = normalize(eyeVec);
	Output.eyeVec = eyeVec;
#endif

#ifdef _SHADOW_MAP_
  #if _PSSM_
	Output.localPos = localPos;
  #else
	Output.shadowProjPos = TransformPosition(localPos, gWorldToTexProj);
  #endif
#endif

#ifdef _MULTIVERTEXLIGHTS_
	//// 여기에서 구현합시다...

	float3 AggLightPos = 0;
	float4 AggLightColor = 0;

	float totalWeight = 0.0f;
	float3 totalLightVector = 0.0f;

	// 전부 로컬좌표계에서 계산됩니다.
	int i=0;
	for (i=0; i<LocalLightCount; i++)
	{
		// weight는 intensity에 따른 가중치가 되어야 함.
		const float3 lightVector = LightPosArray[i].xyz - localPos.xyz;
		const float3 lightDir = normalize(lightVector);
		const float len = length(lightVector);

		/*	comments
			2011-12-29 : 창희
			1. FallOff는 어디에 적용해주는 것일까요??? -> Intensity

			2011-12-31 : 재희
			1. 범위에 따라 Intensity 줄이는 공식일 걸.. 
			R/R0에서 R은 포인트라이트 범위이고, R0는 사이의 거리임.
			그래서 R0가 0일 때는무한대가 되고 R이 되는 순간부터 0이 된다. 
			0이면 무한대인데 min값 처리 안해도 되나?
			근데 왜 안되지? 뭣이 이상해.. 파란색, 빨간색 조명이 보이질 않아..ㅠㅠ

			2012-01-01 : 창희
			1. 나는 그냥 r0는 상수라고 생각했음... 마치 "이 범위까지는 일정하게 강하다" 라는 의미로... 따라서, 라이트 범위의 절반 정도는 최대 밝기하는 의미 아닐까?
			2. weight가 1/len 이 아니고, falloff로만 처리하는건가요??? falloff는 감쇠니까, light와 vertex의 거리에 따른 강도의이니, weight * falloff 해주어야 하는거 아닌가??
			3. 라이트는 양쪽에 배치가 되어 있고, <-, -> 버튼으로 캐릭터를 양쪽으로 이동시킬 수 있어요.. 그렇게 보시면 될 듯... (현재 라이팅 메쉬가 렌더링 되지는 않아욤... ㅡㅡ;)

			2012-01-02 : 재희
			1. 자네 식처럼 하면 falloff는 항상 1이 되버리는데... 
			2. falloff가 거리에 따른 감쇠식이니 이걸 weight라고 본 것임. 문서에 보면 쉽게 계산하려고 weight를 1로 뒀다고 계속 강조함.

			2012-01-02 : 창희
			1. falloff를 구하는 것은 "재희"님의 코드가 맞는 것 같습니다요~
			2. 뒷 부분에 상대적인 light vector의 가중치를 위해서, 거리로 나눈다고 하는 부분이 나오는데, 저는 그냥 그대로 믿고 (1/len)을 weight로 보고 적용해보았습니다.
			3. light의 Intensity(강도)를 상수로 적용한다면, 어떻게 해야 될까요?
			 - 일단 lightColor에는 적용을 해주어야 할 것 같습니다.
			 - position을 구하는데, weight에 영향을 주어야 할지는 잘 모르겠네요...

			2012-01-05 : 창희
			1. weight는 (1.0/len)으로 거리에 따라서, 적용해주는 것이 맞는 듯. 기본 weight를 1로 했더니, 거리와 상관없이 값이 섞여버리는 듯...
			  (테스트는 float weight = 1.0f vs float weight = (1.0f/len))

			2012-01-08 : 창희
			1. weight를 = (1.0.len) * falloff 와 weight = falloff 와 비교해보았음.
			   의미적으로 본다면, falloff는 라이트범위와 관련있는 것이고, (1.0/len)은 거리와 관련되어 있는 것이니, 두 factor 전부 고려되어야 한다고 생각함...
		*/
		const float lightIntensity = LightColorArray[i].w;
		const float lightRange = LightPosArray[i].w;

#if 1	// 창희꺼!
		float falloff = 0.25f * pow(cos(max(1, lightRange / len) * _PI)+1, 2);
		float weight = (1.0f / len) * saturate(dot(localNor, lightDir)) * falloff * lightIntensity;
		totalLightVector += weight * lightDir;;
		totalWeight += weight/len;
#else
		// 재희꺼!
		float falloff = 0.25f * pow(cos(max(1, lightRange / len ) * _PI)+1, 2);
		float weight = falloff * saturate(dot(localNor, lightDir));
		totalLightVector += weight * lightDir;
		totalWeight += weight/len;
#endif
	}

	AggLightPos = (totalLightVector / totalWeight) + localPos.xyz;
	
	float3 AggLightDir = AggLightPos.xyz - localPos.xyz;

	for(i=0; i<LocalLightCount; i++)
	{
		const float3 lightColor = LightColorArray[i].xyz;
		const float lightIntensity = LightColorArray[i].w;

		float3 lightVector = LightPosArray[i].xyz - localPos.xyz;
		AggLightColor.xyz += saturate(dot(lightVector, AggLightDir)) * lightColor * lightIntensity;
	}

	Output.lightPos = AggLightPos;
	Output.lightColor = AggLightColor;
	Output.localPos = localPos;
#endif

	return Output;
}

//------------------------------------------------------------------------------
/**
*/
float4 ps_main( VertexOutput Input ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	/// normal
	float3 normal = 0;
#ifdef _NORMAL_MAP_
	normal = psNormalFromBumpMap(NormalMapSampler, Input.uv0, Input.tangent, Input.binormal, Input.normal );
#else
	normal = Input.normal;
#endif
	normal = normalize(normal);

	float4 diffuseTerm = 1;
	float4 specularTerm = 0;
	float4 ambientTerm = 0;
	
	// ambient
#ifdef _AMBIENTCUBE
	ambientTerm.rgb += GetAmbientCube( normal );
#endif
	ambientTerm.rgb *= MatAmbientIntensity;

#ifdef _TOON_MAP_
	float4 shadeColor = tex1D(ToonMapSampler, diffuseIntensity);
	Output = shadeColor;
#endif

	// lighting
	// diffuseTerm 색상...
	//float diffuseIntensity = HalfLambert( normal, Input.lightVec, 1 );	// Dot3Clamp(normal, Input.lightVec); // 
	float diffuseIntensity = Lambert(normal, Input.lightVec);
#if 0
	float f0 = Square( (1.0-MatReflectiveIndex) / (1.0+MatReflectiveIndex) );
	float Fdiff = f0 + (1.0-f0)*pow(diffuseIntensity, 5);
	diffuseIntensity = 1.0f - Fdiff;
#endif

	float4 lightColor = lightDiffuse;
#ifdef _MULTIVERTEXLIGHTS_
	/// 픽셀 셰이더에서의 계산은 여기서!!!
	float4 PointLightColor = 0;
	
	const float _INTERTHRESHOLD = 1.0f;

	float3 PointLightVec = Input.lightPos - Input.localPos.xyz;
	float3 PointLightDir = length(PointLightVec) < _INTERTHRESHOLD ? PointLightVec : normalize(PointLightVec);

	PointLightColor = Input.lightColor * saturate(dot(normal, PointLightDir));
	lightColor += PointLightColor;
#endif

	float4 diffColor = lightColor;
	diffColor *= matDiffuse;
#ifdef _VERTEXCOLOR_
	diffColor *= Input.diff;
#endif

	float4 rimLight = 0;
#ifdef _RIM_LIGHT_
	/// Nebula3의 rimLighting
	//float rimIntensity = RimLightIntensity(normal, Input.eyeVec);
	//diffuseTerm = float4(rimIntensity, rimIntensity, rimIntensity, 0.0f);
	//diffuseTerm += lightDiffuse * diffuseIntensity;
	//diffuseTerm += lightDiffuse * diffuseIntensity * rimIntensity;

	rimLight = Rim2(normal, Input.eyeVec, Input.lightVec, 4.0f) * RimIntensity * lightColor;
#endif

	float4 albedo = (float4)1;
#ifdef _DIFFUSE_MAP_
	albedo = tex2D( DiffuseMapSampler0, Input.uv0 );
  #ifdef _SRGB_
	albedo = sRGBtolinearRGB( albedo );
  #endif
/*
	#ifdef _EMISSIVE_MAP_
	float4 illumination = tex2D( EmissiveMapSampler, Input.uv0 ) * MatEmissiveIntensity;
	albedo = albedo * diffuseTerm * (1 - illumination) + albedo * illumination;
	//albedo = albedo * diffuseTerm + (illumination * 1.5f);
	#endif
*/
#endif

	float4 subSurface = 0.0f;
#ifdef _SKIN_LIGHTING_
	subSurface = GetSubsurfaceLite( diffuseIntensity );
#endif

	/// Shadow Map
	float shadowlit = 1.0f;
#ifdef _SHADOW_MAP_
  #if _PSSM_
	float distance = TransformPosition(Input.localPos, gWorldView).z;

	if (distance < gGlobalPSSMDistances[1])
	{
		float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[0]);
		shadowlit = GetShadow(gGlobalPSSMBuffers[0], projLightPos);
		
		//Output.rgb += float4(1.0f, 0.0f, 0.0f, 0.0f);
	}
	else if (distance < gGlobalPSSMDistances[2])
	{
		float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[1]);
		shadowlit = GetShadow(gGlobalPSSMBuffers[1], projLightPos);
		
		//Output.rgb += float4(0.0f, 1.0f, 0.0f, 0.0f);
	}
	else if (distance < gGlobalPSSMDistances[3])
	{
		float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[2]);
		shadowlit = GetShadow(gGlobalPSSMBuffers[2], projLightPos);
		
		//Output.rgb += float4(0.0f, 0.0f, 1.0f, 0.0f);
	}
	else if (distance < gGlobalPSSMDistances[4])
	{
		float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[3]);
		shadowlit = GetShadow(gGlobalPSSMBuffers[3], projLightPos);		
	}
  #else
	shadowlit = GetShadow(ShadowMapSampler, Input.shadowProjPos);
  #endif
#endif

	diffuseTerm = (diffuseIntensity * diffColor + ambientTerm + rimLight + subSurface);
#ifndef _LIGHTINGONLY_
	diffuseTerm *= albedo;
#endif
	Output = diffuseTerm;

	float3 specColor = 0;
#ifndef _NOSPECULAR_
 #ifdef _HAIR_LIGHTING_
	// specular lighting
	specColor = HairSpecular(Input.binormal, normal, lightVec, viewVec, uv ) * lightColor.xyz;
 #else
  #ifdef _SPEC_MAP_
	float4 specMap = tex2D( SpecularMapSampler, Input.uv0 );
	float3 specular = specMap.rgb * lightColor.rgb; // * matSpecular.rgb;
	
	const float3 halfVec = normalize(Input.lightVec + Input.eyeVec);

   #ifdef _FASTSPECULAR
	float BaseSpec = max(0, dot(halfVec, normal));

	// Fast specular approximation (http://www.gamasutra.com/view/feature/2972/a_noninteger_power_function_on_.php)
	// Basically pow( N, P ) can be approximated by pow( max( A * N + B ), M )
	//      - A and B are constants that must be tweaked to get artifact-free results
	//      - M can be really small in practice (2 looks good, can be replaced by single multiply)
	// This should result in a mad_sat instruction plus one multiply (2 instructions total!)
	#define SpecApproxA 6.645
	#define SpecApproxB -5.645
	float specIntensity = clamp( SpecApproxA * BaseSpec + SpecApproxB, 0.0, 1.0 );
	specIntensity *= specIntensity;   // M = 2
   #else
	// Blinn-Phong
	const float Shininess = 32.0f; //SpecularPower;
	float specIntensity = pow( max(0, dot(halfVec, normal)), Shininess );
    #ifdef _PHYSICBASEDLIGHTING_
	 // Normalized Distribute Function(NDF)
	 specIntensity *= ((2.0 + Shininess) / 2.0*_PI);// * (_PI/4.0f);
	 //specIntensity *= (Shininess+2.04f)/(8.0f*_PI);

	 // Schlick-Fresnel Function
	 float F0 = Square( (1.0-MatReflectiveIndex) / (1.0+MatReflectiveIndex) );
	 float base = 1.0-max(0, dot(Input.lightVec, halfVec));
	 float Fresnel = F0 + (1.0-F0) * pow(base, 5);
	 specIntensity *= Fresnel;
	 
	#if 0
	 // visibility function (Kelemon-Szirmay-Kalos)
	 float Visibility = 4.0f / max(0.0001f, Square(Input.eyeVec + Input.lightVec));
	#else
	 // visibility function (Schlick-Smith's Shadowing Function)
	 const float a = 1.0/sqrt((_PI/4.0)*Shininess + (_PI/2.0));
	 float Visibility = 1.0 / ((max(0, dot(normal, Input.lightVec))*(1.0-a) + a) * (max(0, dot(normal, Input.eyeVec))*(1.0-a) + a));
	#endif
	 specIntensity *= Visibility;
    #endif
   #endif
 	
	specColor = specIntensity * specular;
  #ifdef _SPEC_EXP_MAP_
	float4 specularMask = tex2D( SpecularExpMapSampler, Input.uv0 );
	specColor *= specularMask;
  #endif
	
	specColor *= MatSpecularIntensity;
  #endif
 #endif
#endif
	Output += float4(specColor, 0.0f);

	//Output += ambientTerm;

	///@cagetu -09/11/06 : Alpha값 설정에 대해서는 다시 설정하자. (현재 EarlyZ 공사 관계로 일단 이렇게...완료 후 수정요망!!!)
#ifdef _ALPHA_MAP_
	Output.a = tex2D(AlphaMapSampler, uv);
#else
	Output.a = albedo.a;
#endif

#ifdef _BLEND_ALPHA_
	Output.a *= gAlphaBlendFactor;
#endif

/*
#ifdef _GLOW_MAP_
	float4 glowMap = tex2D( GlowMapSampler, Input.uv0 );
	//float ldotn = dot(normalize(normal), normalize(Input.lightVec));
	//float4 glow = glowMap * saturate(1-ldotn);
	float4 glow = glowMap * saturate(1-diffuseIntensity);
	Output.rgb += glow.rgb * MatGlowIntensity * glowMap.a;
#endif
*/

#ifdef _EMISSIVE_MAP_
	float4 emissive = tex2D( EmissiveMapSampler, Input.uv0 );
	Output.rgb += (emissive.rgb * MatEmissiveIntensity);
#endif

#ifdef _REFLECT_MAP_
	float3 reflectVec = reflect(normal, Input.eyeVec);	// CalculateReflect(normal, Input.eyeVec);	//
	float reflectIntensity = gReflectivity;

  #ifdef _SPEC_MAP_
	reflectIntensity *= specMap;
  #endif // _SPEC_MAP_
/*
  #ifdef _SPEC_EXP_MAP_
	float4 reflect = reflectIntensity * specularMask;
	reflectIntensity = reflect.x;
  #endif
*/
	float4 reflectColor = texCUBE( ReflectMapSampler, reflectVec );
	Output.rgb = Output.rgb * (1.0f - reflectIntensity) + reflectColor.xyz * reflectIntensity;
#endif

	/// Shadow
	Output.rgb *= shadowlit;

#ifdef _GLOW_MAP_
	float4 glowMap = tex2D( GlowMapSampler, Input.uv0 );
	//float ldotn = dot(normalize(normal), normalize(Input.lightVec));
	//float4 glow = glowMap * saturate(1-ldotn);
	float4 glow = glowMap * saturate(1-diffuseIntensity);
	Output.rgb += glow.rgb * MatGlowIntensity * glowMap.a;
#endif

#ifdef _BLEND_ALPHA_
	// premultiplied alpha
	Output.rgb *= Output.a;
#endif

#ifdef _SRGB_
	//Output = linearRGBtosRGB(Output);
	//Output = sRGBtolinearRGB(Output);
#endif

	//Output = float4(rimLight.xyz, 1.0f);
	//Output = ambientTerm * albedo;
	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_main();
	}
}

#endif // 