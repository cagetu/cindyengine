//******************************************************************************
/**	@author		cagetu
	@brief		Object Motion Blur Shader
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
float4x4 gWorld : Model;
float4x4 gView : View;
float4x4 gViewProj : ViewProjection;
float4x4 gPrevViewProj : PrevViewProjection;
float4x4 gPrevView : PrevView;

#ifdef _SKINNED_
float4x3 gJoints[27] : JointPalette;
#endif

texture gDiffuseMap0 : DiffuseMap0;
sampler SceneSampler = sampler_state
{
   Texture = <gDiffuseMap0>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
   //MipMapLodBias = <MipLodBias>;
};

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// 입력
struct VertexInput
{
	float4 position : POSITION0;
	float3 normal : NORMAL0;
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif
};
/// 출력
struct VertexOutput
{
	float4 position : POSITION0;
	float3 velocity : TEXCOORD0;
};

struct PixelInput
{
	float4 wpos : VPOS;
	float3 velocity : TEXCOORD0;
};

//------------------------------------------------------------------------------
/**
*/
VertexOutput vsMain( const VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	/// position, normal
	float4 localPos = 0;
	float3 localNor = 0;
	
	float3 inputNormal = normalize(UnpackNormal(Input.normal));	// Input.normal;

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
#else
	localPos = Input.position;
	localNor = inputNormal;
#endif

	// eye space로 Position 변환
	float4x4 worldView = gWorld * gView;
	float4x4 prevWorldView = gWorld * gPrevView;
	
	float4 P = mul(worldView, localPos);
	float4 prevP = mul(prevWorldView, localPos);

	// eye space로 normal 변환
	float3 N = TransformNormal(localNor, worldView);
	
	// eye space motion vector 계산
	float3 motionVector = P.xyz - prevP.xyz;
	
	// clip space로 변환
	float4x4 worldViewProj = mul(gWorld, gViewProj);
	float4x4 prevWorldViewProj = mul(gWorld, gPrevViewProj);

	P = TransformPosition(localPos, worldViewProj);
	prevP = TransformPosition(localPos, prevWorldViewProj);

	// motionVector와 normal Vector를 기준으로 선택
	float flag = dot(motionVector, N) > 0;
	float4 Pstretch = flag ? P : prevP;
	Output.position = Pstretch;

	// do divide by W => NDC Coordinates
	P.xyz = P.xyz / P.w;
	prevP.xyz = prevP.xyz / prevP.w;
	Pstretch.xyz = Pstretch.xyz / Pstretch.w;

	float3 dP= P.xyz - prevP.xyz;
	Output.velocity = dP;
	return Output;
}

const half blurScale = 1.0f;
const float SAMPLES = 8;

//------------------------------------------------------------------------------
/**
*/
float4 psMain( const PixelInput Input ) : COLOR0
{
	float4 Output = float4(1.0f, 1.0f, 1.0f, 1.0f);

	// read velocity from texture coordnates
	half2 velocity = Input.velocity.xy * blurScale;

	// sample scene texture along direction of motion
	const float samples = SAMPLES;
	const float w = 1.0f / samples;	// sample weight
	
	float4 a = 0;
	for (int i=0; i<4; i++)
	{
		float t = 1 / (samples-1);
		a = a + tex2D(SceneSampler, Input.wpos + velocity*t) * w;
	}
	Output = a;
	return Output;
}

//------------------------------------------------------------------------------
// Technique Section for Effect Workspace.
//------------------------------------------------------------------------------
technique t0
{
	pass p0
	{
		VertexShader = compile vs_2_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}