// 変換行列
const static float3x3	M01 = float3x3(
	0.4124, 0.3576, 0.1805,
	0.2126, 0.7152, 0.0722,
	0.0193, 0.1192, 0.9505 );
const static float3x3	M02 = float3x3(
	0.2209, 0.3390, 0.4184,
	0.1138, 0.6780, 0.7319,
	0.0102, 0.1130, 0.2969 );

// 逆変換行列
const static float3x3	InverseM01 = float3x3(
	 3.2406, -1.5372, -0.4986,
	-0.9689,  1.8758,  0.0415,
	 0.0557, -0.2040,  1.0570 );
const static float3x3	InverseM02 = float3x3(
	 6.0013, -2.7000, -1.7995,
	-1.3320,  3.1029, -5.7720,
	 0.3007, -1.0880,  5.6268 );

// LUVへの変換
float3	Rgb2Luv( float3 rgb )
{
	float3	XYZ = mul( M01, rgb );
    XYZ = max( XYZ, float3(1e-6, 1e-6, 1e-6) );
	float2	xy = XYZ.xz / (XYZ.x + XYZ.y + XYZ.z);
	float	L = XYZ.y;
	float2	uvd = float2(4 * xy.x, 9 * xy.y) / (-2 * xy.x + 12 * xy.y + 3);
	float2	uve = uvd / 0.62;
	return float3( uve, L );
}
float3	Rgb2EricsonLuv( float3 rgb )
{
	float3	Xp_Y_XYZp = mul( rgb, M02 );
	Xp_Y_XYZp = max( Xp_Y_XYZp, float3(1e-6, 1e-6, 1e-6) );
	float2	uv = Xp_Y_XYZp.xy / Xp_Y_XYZp.z;
	return float3( uv, Xp_Y_XYZp.y );
}
float4	Rgb2LogLuv( float3 rgb )
{
	float4	vResult;
	float3	Xp_Y_XYZp = mul( rgb, M02 );
	Xp_Y_XYZp = max( Xp_Y_XYZp, float3(1e-6, 1e-6, 1e-6) );
	vResult.xy = Xp_Y_XYZp.xy / Xp_Y_XYZp.z;
	float Le = 2 * log2( Xp_Y_XYZp.y ) + 127;
	vResult.w = frac( Le );
	vResult.z = (Le - (floor(vResult.w * 255.0f)) / 255.0f) / 255.0f;
	return vResult;
}

// RGBへの変換
float3	Luv2Rgb( float3 luv )
{
	float2	uvd = luv.xy * 0.62;
	float2	xy = float2(9 * uvd.x, 4 * uvd.y) / (6 * uvd.x - 16 * uvd.y + 12);
#if 0
	float3	XYZ = { luv.z * xy.x / xy.y, luv.z, luv.z * (1 - xy.x - xy.y) / xy.y };
#else
	float	div = luv.z / (1 - xy.x - xy.y);
	float3	XYZ = { xy.x * div, luv.z, xy.y * div };
#endif
	float3	rgb = mul( InverseM01, XYZ );
	return rgb;
}
float3	EricsonLuv2Rgb( float3 luv )
{
	float3	XYZ;
	XYZ.y = luv.z;
	XYZ.z = XYZ.y / luv.y;
	XYZ.x = luv.x * XYZ.z;
	float3	rgb = mul( XYZ, InverseM02 );
	return max( rgb, 0 );
}
float3	LogLuv2Rgb( float4 luv )
{
	float	Le = luv.z * 255 + luv.w;
	float3	Xp_Y_XYZp;
	Xp_Y_XYZp.y = exp2((Le - 127) / 2);
	Xp_Y_XYZp.z = Xp_Y_XYZp.y / luv.y;
	Xp_Y_XYZp.x = luv.x * Xp_Y_XYZp.z;
	float3	rgb = mul( Xp_Y_XYZp, InverseM02 );
	return max( rgb, 0 );
}

// LUV加算ブレンド
float3	BlendLuv( float3 src, float3 dest )
{
	return float3(
		lerp( dest.rg, src.rg, saturate( (src.b / dest.b) * 0.5f ) ),
		dest.b + src.b
	);
}
