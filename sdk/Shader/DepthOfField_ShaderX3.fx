#line 1 "DepthOfField_ShaderX3.fx"
//-------------------------------------------------------------
//	Depth Of Field.fx : 피사계 심도 구현
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"

// texture
texture diffuseMap : DiffuseMap0;
texture diffuseMap1 : DiffuseMap1;
texture diffuseMap2 : DiffuseMap2;

float2 filterSampleOffsets[16] : FilterSampleOffsets;
float4 filterSampleWeights[16] : FilterSampleWeights;

//-------------------------------------------------------------
/**	Sampler
*/
//-------------------------------------------------------------
sampler gDepthSampler = sampler_state
{
    Texture = <diffuseMap>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

sampler gLowSceneSampler = sampler_state
{
    Texture = <diffuseMap1>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

sampler gSceneSampler = sampler_state
{
    Texture = <diffuseMap2>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

//==================================================================
/** DownFilter4x4
	@brief	4x4만큼 축소 필터링 한다.
*/
//==================================================================
float4 psDownFilter2x2( in float2 inTex : TEXCOORD0 ) : COLOR
{
	float4 color = 0;

	for (int i=0; i<4; ++i)
	{
		float2 texcoord = inTex + filterSampleOffsets[i];
		color.rgb += tex2D( gSceneSampler, texcoord ).rgb;
		color.a += tex2D( gDepthSampler, texcoord ).a;
	}

	return color/4;
}
// 4x4 축소 샘플링
technique Downsample2x2
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psDownFilter2x2();
	}
}

//==================================================================
//==================================================================
float2 g_ScreenRes : Resolution = float2(1024, 768);
float2 g_MaxCoC = float2(5.0f, 10.0f);				// 픽셀안에서 confusion(CoC) radius의 최대 circle과 지름
float g_RadiusScale = 0.4f;							// scale factor for maximum CoC size on low res. image

// contains poisson-distributed positions on the unit circle
static float2 poisson[8] = {  
  float2( 0.0,      0.0),
  float2( 0.527837,-0.085868),
  float2(-0.040088, 0.536087),
  float2(-0.670445,-0.179949),
  float2(-0.419418,-0.616039),
  float2( 0.440453,-0.639399),
  float2(-0.757088, 0.349334),
  float2( 0.574619, 0.685879)
};

//-------------------------------------------------------------
/** @brief		Poisson Filter 
	@remarks	이 방법은 장면 텍스쳐의 Alpha 채널에 Depth를 저장하여, 블러된 장면과 기본 장면을
				블랜딩하여, 그 차이의 깊이값 만큼 보간하는 방법인데,
				Depth를 따로 기록하는 방식에서는 사용하기가 복잡해 진다..
				다른 방법을 이용해야 한다!!!
*/
float4 
psDof_ShaderX3_Pass1( in float2 inTexcoord : TEXCOORD0 ) : COLOR
{
	// Depth
	float4 depthBuffer = tex2D( gDepthSampler, inTexcoord );
	float centerDepth = depthBuffer.a;

	// convert depth into blur radius in pixels
	float discRadius = abs( centerDepth * g_MaxCoC.y - g_MaxCoC.x );

	// compute disc radius on low-res image
	float2 inverseViewportDimensions = 1.0f / g_ScreenRes;
	float2 pixelSizeLow = 4.0f * inverseViewportDimensions;
	float discRadiusLow = discRadius * g_RadiusScale;

	float4 result = 0;

	for (int t=0; t<4; t++)
	{
		// fetch low-res tap
		float2 coordLow = inTexcoord + (pixelSizeLow * poisson[t] * discRadiusLow);
		float4 tapLow = tex2D( gLowSceneSampler, coordLow );

		// fetch high-res tap
		float2 coordHigh = inTexcoord + (inverseViewportDimensions * poisson[t] * discRadius);
		float4 tapHigh = tex2D( gSceneSampler, coordHigh );
		tapHigh.a = centerDepth;
		
		// put tap blurriness into [0, 1] range
		float tapBlur = abs( tapHigh.a * 2.0f - 1.0f );

		// mix low- and hi-res taps based on tap blurriness
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		// apply leaking reduction: lower weight for taps that are
		// closer than the center tap and in focus
		tap.a = (tap.a >= centerDepth) ? 1.0f : abs(tap.a*2.0f - 1.0);

		// accumulate
		result.rgb += tap.rgb * tap.a;
		result.a += tap.a;

/*
		/// DepthBuffer를 이용하려면, blur 처리만 하고, leaking reduction은 할 수가 없을 듯!!!
		float tapBlur = abs( centerDepth * 2.0f - 1.0f );
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		result.rgb += (tap.rgb * centerDepth);
		result.a += centerDepth;
*/		
	}

	// normalize and return result
	return result/result.a;
}

//-------------------------------------------------------------
/** @brief		Poisson Filter 
	@remarks	이 방법은 장면 텍스쳐의 Alpha 채널에 Depth를 저장하여, 블러된 장면과 기본 장면을
				블랜딩하여, 그 차이의 깊이값 만큼 보간하는 방법인데,
				Depth를 따로 기록하는 방식에서는 사용하기가 복잡해 진다..
				다른 방법을 이용해야 한다!!!
*/
float4 
psDof_ShaderX3_Pass2( in float2 inTexcoord : TEXCOORD0 ) : COLOR
{
	// Depth
	float4 depthBuffer = tex2D( gDepthSampler, inTexcoord );
	float centerDepth = depthBuffer.a;

	// convert depth into blur radius in pixels
	float discRadius = abs( centerDepth * g_MaxCoC.y - g_MaxCoC.x );

	// compute disc radius on low-res image
	float2 inverseViewportDimensions = 1.0f / g_ScreenRes;
	float2 pixelSizeLow = 4.0f * inverseViewportDimensions;
	float discRadiusLow = discRadius * g_RadiusScale;

	float4 result = 0;

	for (int t=4; t<8; t++)
	{
		// fetch low-res tap
		float2 coordLow = inTexcoord + (pixelSizeLow * poisson[t] * discRadiusLow);
		float4 tapLow = tex2D( gLowSceneSampler, coordLow );

		// fetch high-res tap
		float2 coordHigh = inTexcoord + (inverseViewportDimensions * poisson[t] * discRadius);
		float4 tapHigh = tex2D( gSceneSampler, coordHigh );
		tapHigh.a = centerDepth;
				

		// put tap blurriness into [0, 1] range
		float tapBlur = abs( tapHigh.a * 2.0f - 1.0f );

		// mix low- and hi-res taps based on tap blurriness
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		// apply leaking reduction: lower weight for taps that are
		// closer than the center tap and in focus
		tap.a = (tap.a >= centerDepth) ? 1.0f : abs(tap.a*2.0f - 1.0);

		// accumulate
		result.rgb += tap.rgb * tap.a;
		result.a += tap.a;

/*
		/// DepthBuffer를 이용하려면, blur 처리만 하고, leaking reduction은 할 수가 없을 듯!!!
		float tapBlur = abs( centerDepth * 2.0f - 1.0f );
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		result.rgb += (tap.rgb * centerDepth);
		result.a += centerDepth;
*/		
	}

	result = result/result.a;
	result.a = 0.5f;
	
	// normalize and return result
	return result;
}

//-------------------------------------------------------------
/**
*/
float4 
psDofPass1(in float2 inTexcoord : TEXCOORD0) : COLOR0
{
	float4 output = psDof_ShaderX3_Pass1(inTexcoord);
	return output;	
}
float4 
psDofPass2(in float2 inTexcoord : TEXCOORD0) : COLOR0
{
	float4 output = psDof_ShaderX3_Pass2(inTexcoord);
	return output;	
}

//-------------------------------------------------------------
//	Techniques...
//-------------------------------------------------------------
technique DoF_ShaderX3
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psDofPass1();
	}
	Pass p1
	{
		AlphaBlendEnable = True;
		BlendOp = Add;
		DestBlend = InvSrcAlpha;
		SrcBlend = SrcAlpha;

		VertexShader = null;
		PixelShader = compile ps_3_0 psDofPass2();
	}
}