#ifndef __UTIL_LIGHTING_FXH__
#define __UTIL_LIGHTING_FXH__

//-------------------------------------------------------------
//	util_lighting.fxh : lighting 계산을 위한 utility functions
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

//==================================================================
/** LightDirection
*/
//==================================================================
float3 LightDirection( float4 localPos, float3 localLightPos )
{
	return normalize(localLightPos - localPos.xyz);
}

//==================================================================
/** Lambert 연산
	@param Normal		: 버텍스 노말
	@param LightDir		: Light Direction

	LightIntensity = max(0, dot(n,l));

	최종 라이트 = Material_ems + Global_amb*Material_amb + sum( Material_dif*diffuse_Color + Material_Amb*Light_Amb )
*/
//==================================================================
float Lambert( float3 Normal, float3 LightDir )
{
	return max(0, dot(Normal, LightDir) );
}

//==================================================================
/** Half Lambert
	Valve의 Half Lambert
	cf) Team Fortress2에서는 제곱을 사용하지 않음.
*/
//==================================================================
float HalfLambert( float3 Normal, float3 LightDir, uniform int E )
{
	return pow( (0.5f * dot(Normal, LightDir)) + 0.5f, E );
}

//==================================================================
/**	언리얼에서 Power(dot(n,l), GetDiffusePower()) 식으로 이용
*/
//==================================================================
float GetDiffusePower()
{
	return 1.0f;
}

//==================================================================
/**	Wrap-Diffuse Lighting
 */
//==================================================================
float WrapDiffuse( float WrapFactor, float3 Normal, float3 LightDir  )
{
	return max( 0, (dot(LightDir, Normal) + WrapFactor) / (1+WrapFactor) );
}

//	Ambient Cube용
float3	g_aAmbientCube[6] : AmbientCubeColor =
{
	{	0.55F, 0.59F, 0.65F	},	// +X
	{	0.55F, 0.59F, 0.65F	},	// -X
	{	0.6F, 0.69F, 0.83F	},	// +Y위
	{	0.45F, 0.64F, 0.58F	},	// -Y아래
	{	0.52F, 0.56F, 0.62F	},	// +Z
	{	0.52F, 0.56F, 0.62F	}	// -Z
};

//==================================================================
/** Unbumped Ambient Cube Math
	    Valve의 범프 노말이 적용되지 않은 Ambient Cube
*/
//==================================================================
float3 GetAmbientCube( const float3 WorldNormal )
{
	float3 squared = WorldNormal * WorldNormal;
	int3 isNegative = (WorldNormal < 0.0f);
	
	float3 linearColor;
	linearColor = squared.x * g_aAmbientCube[isNegative.x] +
				  squared.y * g_aAmbientCube[isNegative.y+2] +
				  squared.z * g_aAmbientCube[isNegative.z+4];
	return linearColor;
}

//==================================================================
/**	Attenuation
*/
//==================================================================
float GetAttenuation( half3 lightVec, half oneOverSqrLightRadius)
{
	return saturate(1 - dot(lightVec, lightVec) * oneOverSqrLightRadius);
}

//==================================================================
/** Point Light Diffuse 연산
  	@param Pos		: 버텍스 포지션
  	@param Normal		: 버텍스 노말
  	@param LightPos		: Light 위치
  	@param LightDiffuse	: Light Diffuse Color
  	@param LightAtten	: Light Attennuate
  
  	Atten - 1/( att0i + att1i * d + att2i * d2)
  
  	diffuse_Color - max(0, dot(n,l)) * light diffuse * Atten
  
  	최종 라이트 - Material_ems + Global_amb*Material_amb + sum( Material_dif*diffuse_Color + Material_Amb*Light_Amb )
*/
//==================================================================
float4 PointLightDiffuse( float4 Pos, float3 Normal, float4 LightPos, float4 LightDiffuse, float4 LightAtten )
{
	float3 delta = LightPos - Pos;
	float distance = length(delta);
	float3 direction = normalize(delta);

	float atten = 1/( LightAtten.x + LightAtten.y * distance + LightAtten.z * distance*distance);
	return ( max(0, dot(direction, Normal) ) * LightDiffuse * atten );
}

//==================================================================
/** Phong 반영 반사광
  	@brief	반사 벡터
  		R = -Eye + 2 * NdotV * N
  		
  	@param Pos		: 버텍스 포지션
  	@param Normal	: 버텍스 노말
  	@param LightDir	: Light 방향
  	@param EyePos	: 시점 벡터(로컬 좌표계상의)
  	@param Shininess: 밝기
*/
//==================================================================
float Phong( in float3 Eye,
			 in float3 LightDir,
			 in float3 Normal,
			 in float Shininess )
{
	float3 R = -Eye + 2.0f * dot( Normal, Eye ) * Normal;
	// == float3 R = -reflect( Eye, Normal );

	float spec = pow( Dot3Clamp( LightDir, R ), Shininess );
	//float spec = pow( saturate(dot(LightDir, R)), Shininess );
	
	return spec;
}

//==================================================================
/** Blinn-Phong 연산
    @param Pos		: 버텍스 포지션
    @param Normal	: 버텍스 노말
    @param LightDir	: Light 방향
    @param EyePos	: 시점 벡터(로컬 좌표계상의)
    @param Shininess: 밝기
*/
//==================================================================
float Blinn( in float3 Eye,
			  in float3 LightDir,
			  in float3 Normal,
			  in float Shininess )
{
	float3 halfVec = normalize(LightDir.xyz + Eye);
	//float specIntensity = pow( Dot3Clamp( halfVec, Normal ), Shininess );
	return pow( saturate(dot(halfVec,Normal)), Shininess );
}

//==================================================================
/** Hemispheric Lighting
	  DXSDK에 소개된 hemispheric lighting term
*/
//==================================================================
float4 Hemisphere( float3 Normal, float3 DirToSky, float OcclusionFactor )
{
	// 외부에서 material ambient를 설정해줘야 하지 않을까?!!?
	static float4 MaterialAmbientIntensity = { 0.5f, 0.5f, 0.5f, 1.0f };    // ambient
	static float4 GroundColor              = { 0.1f, 0.4f, 0.0f, 1.0f };    // ground
	static float4 SkyColor                 = { 0.5f, 0.5f, 0.9f, 1.0f };    // sky

	float4 hemi = MaterialAmbientIntensity;

	//------------------------------------------------------------------
	//	반구 항을 곱하기 위해서, 정점당 저장된 occlusion 항을 사용한다.
	//	
	//	Occlusion term?
	//		오클루전 항은 가중치인데, 오프라인 패스에서 계산되었던 것이다.
	//		이 값은 모델의 다른 부분에 의해 가로 막히는 반구의 퍼센트이다.
	//		제공된 항에서 0 값은 정점이 기하도형에 의해 차단되지 않음을 의미하며(모든 빛을 받는다),
	//		반면 1은 정점이 빛으로부터 완전히 가려진다는 것을 의미한다
	//	
	//	Suggestions:
	//		1 - Occ 라는 결과를 곱하는 것은 이 정점이 반구 조명에 의해 얼마나 영향을 받는지와 관련해서
	//		반구조명을 어둡게 만들 것이다.
	//
	//------------------------------------------------------------------
	hemi *= 1 - OcclusionFactor;

	//------------------------------------------------------------------
	//	반구 조명값을 계산한다.
	//	What is a hemisphere light:
	//		반구 조명은 하늘과 땅이라는 두 개의 색상으로 구성되어 있으며, 이 색상들은 법선에 기반에 서로 보간된다;
	//		즉 하늘쪽을 가리키는 법선을 가진 영역은 "하늘" 색상을 가지며, 땅쪽을 가리키는 영역은 "땅"의 색상을 더 많이 가진다.
	//	
	//	Suggestions:
	//		반구 조명을 구현하는 쉬운 방법은 하늘 색상과 땅 색상을 법선과 하늘 방향 사이각에 기반해서 선형적으로 보간하는 것이다.
	//		이 각은 법선과 하늘 방향을 내적함으로써 간단히 계산될 수 있다.
	//		이 값은 -1 부터 1 까지의 결과를 산출하는데, 이 값들을 0 에서 1 까지로 다시 매핑할 때 두 색상 사이를 선형적으로 보간하기 좋다.
	//	
	//------------------------------------------------------------------
	float lerpFactor = (dot( Normal, DirToSky ) + 1) / 2;

	hemi *= lerp( GroundColor, SkyColor, lerpFactor );

	return hemi;
}

const float RimIntensity = 1.0f;//3.0;//
const float RimPower = 4.0f;

//==================================================================
/** Rim Light
*/
//==================================================================
float4 Rim( in float3 Normal,
			in float3 View,
			in float3 LightDir,
			in float Power )
{
	float NdotV = Lambert( Normal, View );
	float VdotL = Lambert( -LightDir, View );

	float fresnel = (1.0f - NdotV);
	float rim = pow( fresnel, Power ) * VdotL;

	return float4( rim, rim, rim, 1.0f );
}

//==================================================================
/** Rim Light - by narew3D
	float3 diffuse = saturate((RimIntensity+diffuseIntensity) * lightColor);
*/
//==================================================================
float4 Rim2( in float3 Normal,
			 in float3 View,
			 in float3 LightDir,
			 in float Power )
{
	float NdotV = abs( dot( Normal, View ) );
	float VdotL = max( 0, -dot( LightDir, View ) );	// backlight 값

	float fresnel = (1.0f - NdotV);
	float rim = pow( fresnel, Power ) * VdotL;

	return float4( rim, rim, rim, 1.0f );
}

//==================================================================
/** Rim Intensity
*/
//==================================================================
float RimLightIntensity( float3 Normal, float3 EyeVec )
{
	float fresnel = 1.0f - abs(dot(Normal, EyeVec));
	float rimIntensity = pow(fresnel, RimPower) * RimIntensity;
	return rimIntensity;
}

#endif  // __UTIL_LIGHTING_FXH__