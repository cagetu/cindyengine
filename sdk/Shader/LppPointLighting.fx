#line 1 "LightPrePassPointLighting.fx"
//*************************************************************
//	LightPrePassPointLighting.fx : Light Pre Pass Lighting
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//*************************************************************
#include "..\\..\\sdk\\Shader\\LightPrePass.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
//#ifdef _SHADOW_MAP_
#include "..\\..\\sdk\\Shader\\Util_ShadowMap.fxh"
//#endif

/// Pixel 입력
struct PixelInput
{
    float4 vPos	: VPOS;
    float3 eyeToScreenRay : TEXCOORD1;
};

//--------------------------------------------------------------//
/**
	LightColor.r * N.L * Att
	LightColor.g * N.L * Att
	LightColor.b * N.L * Att
	R.V^n * N.L * Att
*/
float4 psMain( const PixelInput Input ) : COLOR0
{
	float4 output = (float4)0;
	
	// Co-ords for texture lookups = pixel rasterisation pos / screen dimensions (e.g. 512/1024 = 0.5f)
	// But due to D3D's weird sampling rules, we have to correct the texture co-ordinate by offsetting it by a predefined amount
	float2 coords = Input.vPos.xy / gScreenSize.xy;		
	coords += gUVAdjust;

	float4 depthBuffer = tex2D( DepthSampler, coords );
	float4 normalBuffer = tex2D( NormalSampler, coords );
	
	/// 카메라 공간의 Pos
	float depth = DecodeDepth(depthBuffer.xy);
	float3 viewSpacePos = GetPixelPos( Input.eyeToScreenRay, depth );
	//float3 viewSpacePos = depthBuffer;

	/// 카메라 공간의 노말
	float3 viewSpaceNormal = UnpackNormal( normalBuffer.xyz );

	// Computes light attenuation and direction
	float3 viewSpaceLightPos = gLightPosition;
	float3 pixelToLight = viewSpaceLightPos - viewSpacePos;
	float3 lightDir = normalize(pixelToLight);

	/// diffuse
	float diffIntensity = HalfLambert(viewSpaceNormal, lightDir, 2);

	// R.V == Phong
	//float3 eyeDir = normalize( viewSpacePos );
	//float specIntensity = pow(saturate(dot(reflect(eyeDir, viewSpaceNormal), lightDir)), 32);
	float3 eyeDir = normalize( -viewSpacePos );
	float specIntensity = Blinn( eyeDir, lightDir, viewSpaceNormal, 32 );
 
	// rimLight
	float4 rimIntensity = Rim2(viewSpaceNormal, eyeDir, lightDir, 4.0f) * RimIntensity;

	// color
	float4 ambientTerm = 0;
	float4 diffuseTerm = 0;

	//ambientTerm.rgb = GetAmbientCube( viewSpaceNormal );

	/**	1. view space pos * inv view matrix = world pos
		2. world pos -> light view projection space
	*/

	/// viewSpacePos * InvView * LightViewProj * TexBias
	/// gWorldToTexProj : InvView * LightViewProj * TexBias
	float4 pos = float4(viewSpacePos.xyz, 1.0f);
	float4 worldPos = TransformPosition( pos, gInvView );
	float4 lightSpacePos = TransformPosition( worldPos, gWorldToTexProj );

	//float NdotL = max( 0.0f, dot(lightDir, viewSpaceNormal) );
	if (diffIntensity > 0.0f)
	{
		/// attenuation
		float distanceToLight = length( pixelToLight );
		half attenuation = max( 0, 1 - ( distanceToLight / gInvSqrLightRange ));

		//diffuseTerm = gLightDiffuse * (diffIntensity +rimIntensity.x) * attenuation;
		//diffuseTerm = gLightDiffuse * (diffIntensity * attenuation+rimIntensity.x);
		diffuseTerm = gLightDiffuse * saturate(diffIntensity+rimIntensity.x);
#ifdef _SKIN_LIGHTING_
		diffuseTerm += GetSubsurfaceLite( diffIntensity );
#endif
		diffuseTerm *= attenuation;
		specIntensity *= attenuation;
	}

	output = float4(diffuseTerm.xyz, specIntensity); 
	return output;
}

//--------------------------------------------------------------//
//	Techniques
//--------------------------------------------------------------//

// PointStencil은 stencil volume을 처리하고 불필요한 shading을 피하는 pass 1/2이다.
// 
// shadow volume 알고리즘에서 발견한 것처럼 two-side stencil 구현을 기초했다.
// 하나의 pass에서 stencil buffer 안에 light volume을 랜더링한다.
// * Initial stencil buffer value = 1
// * Front facing polys drawn first.  If the depth test fails, increment the stencil value
// * Back facing polys drawn second.  If the depth test fails, decrement the stencil value
//
// So we have several cases:

technique PointLight
{
	// Stencil
	pass p0
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = null;

        CullMode			= none;
        ColorWriteEnable	= 0x0;        
        
        // Disable writing to the frame buffer
        AlphaBlendEnable	= true;
        SrcBlend			= Zero;
        DestBlend			= One;
        
        // Disable writing to depth buffer
        ZWriteEnable		= false;
        ZEnable				= true;
        ZFunc				= Less;

        // Setup stencil states
        StencilEnable		= true;
        TwoSidedStencilMode = true;
        
        StencilRef			= 1;
        StencilMask			= 0xFFFFFFFF;
        StencilWriteMask	= 0xFFFFFFFF;
        
        // stencil settings for front facing triangles
        StencilFunc			= Always;
        StencilZFail		= Incr;
        StencilPass			= Keep;

        // stencil settings for back facing triangles
        Ccw_StencilFunc		= Always;
        Ccw_StencilZFail	= Decr;
        Ccw_StencilPass		= Keep;
	}
	// Calculate PointLight
	pass p1
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader	= compile ps_3_0 psMain();

		ZEnable			= false;
		ZWriteEnable	= false;
							
		AlphaBlendEnable = true;
		SrcBlend		= One;
        DestBlend		= One;

        // draw backfaces so that we're always guaranteed to get the right behaviour when inside the light volume
        CullMode		= CW;	        

        ColorWriteEnable = 0xFFFFFFFF;

  		StencilEnable	= true;
  		TwoSidedStencilMode = false;
        StencilFunc		= Equal;

		StencilFail		= Keep;
		StencilZFail	= Keep;
		StencilPass		= Keep;

		StencilRef		= 0;
		StencilMask		= 0xFFFFFFFF;
        StencilWriteMask = 0xFFFFFFFF;
	}
}