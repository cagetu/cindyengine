#ifndef __UTIL_SKINNING_FXH__
#define __UTIL_SKINNING_FXH__

//-------------------------------------------------------------
//	util_skinning.fxh : skinning 계산을 위한 utility functions
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\config.fxh"

#define MAX_JOINTS	27

#ifdef _SKINNED_
float4x3 gJoints[MAX_JOINTS] : JointPalette;
#endif

//------------------------------------------------------------------------------
/** Skinned Position
*/
float4 SkinnedPosition( const float4 Pos, const float4 Weights, const float4 Indices, const float4x3 Joints[MAX_JOINTS] )
{
	float3 pos[4];
#ifdef __PACK_VERTEX__
	float4 normWeights = Weights / dot(Weights, float4(1.0, 1.0, 1.0, 1.0));
	pos[0] = mul( Pos, Joints[Indices[0]] ) * normWeights[0];
	pos[1] = mul( Pos, Joints[Indices[1]] ) * normWeights[1];
	pos[2] = mul( Pos, Joints[Indices[2]] ) * normWeights[2];
	pos[3] = mul( Pos, Joints[Indices[3]] ) * normWeights[3];
#else
	pos[0] = mul( Pos, Joints[Indices[0]] ) * Weights[0];
	pos[1] = mul( Pos, Joints[Indices[1]] ) * Weights[1];
	pos[2] = mul( Pos, Joints[Indices[2]] ) * Weights[2];
	pos[3] = mul( Pos, Joints[Indices[3]] ) * Weights[3];
#endif // __PACK_VERTEX__

	return float4( pos[0]+pos[1]+pos[2]+pos[3], 1.0f );
}

//------------------------------------------------------------------------------
/** Skinned Normal 
*/
float3 SkinnedNormal( const float3 Normal, const float4 Weights, const float4 Indices, const float4x3 Joints[MAX_JOINTS] )
{
	float3 normal[4];
	//normal[0] = mul( Normal, Joints[Indices[0]]) * Weights[0];
	//normal[1] = mul( Normal, Joints[Indices[1]]) * Weights[1];
	//normal[2] = mul( Normal, Joints[Indices[2]]) * Weights[2];
	//normal[3] = mul( Normal, Joints[Indices[3]]) * Weights[3];
	//return normalize(normal[0]+normal[1]+normal[2]+normal[3]);

	normal[0] = mul( Normal, (matrix<float,3,3>)Joints[Indices[0]]) * Weights[0];
	normal[1] = mul( Normal, (matrix<float,3,3>)Joints[Indices[1]]) * Weights[1];
	normal[2] = mul( Normal, (matrix<float,3,3>)Joints[Indices[2]]) * Weights[2];
	normal[3] = mul( Normal, (matrix<float,3,3>)Joints[Indices[3]]) * Weights[3];
	return normalize(normal[0]+normal[1]+normal[2]+normal[3]);
}

#endif // __UTIL_SKINNING_FXH__