//******************************************************************************
/**	@author		cagetu
	@brief		Airbrush ȿ�� 
*/
//******************************************************************************
#ifndef __CN_AIRBRUSH_FXH__
#define __CN_AIRBRUSH_FXH__

float2	gScreenSize : DisplayResolution = float2(1024, 768);
float	gTexSize = 64.0f;
float	gDistribution = 0.6f;
float	gScattering = 0.6;
float	gGradient = 0.78f;

texture RamdomValue : RandomValueMap;
sampler RamdomValueSampler = sampler_state
{
    Texture = <RamdomValue>;
	ADDRESSV = WRAP;
	ADDRESSW = WRAP;
	ADDRESSU = WRAP;
	MIPFILTER = NONE;
	MINFILTER = POINT;
	MAGFILTER = POINT;   
    //MipMapLodBias = <MipLodBias>;
};

//------------------------------------------------------------------------------
/**
*/
float
SprayAdaptiveHermite(float intensity, float sprayParameter)
{
   // adaptive correction, Hermite-Interpolation
   float Localintensity = intensity - sprayParameter;
   float correction = 2.0f * saturate(min(intensity,1.0f-intensity))*(1.0f-gScattering);
   return smoothstep(-gGradient*correction, gGradient*correction, Localintensity-0.5);
}

//------------------------------------------------------------------------------
/**
*/
float 
Spray(float intensity, float sprayParameter)
{
	return SprayAdaptiveHermite(intensity, sprayParameter);
}

#endif // __CN_AIRBRUSH_FXH__
