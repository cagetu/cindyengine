//******************************************************************************
/**	@author		cagetu
	@brief		HairLighting Shader �����
*/
//******************************************************************************

//------------------------------------------------------------------------------
// Variables
//------------------------------------------------------------------------------
texture hairShiftMap : HairShiftMap;
sampler hairShiftMapSampler = sampler_state
{
    Texture     = <hairShiftMap>;
    AddressU    = Wrap;
    AddressV    = Wrap;
    MinFilter   = Linear;
    MagFilter   = Linear;
    MipFilter   = Point;
    //MipMapLodBias = <MipLodBias>;
};
texture hairSpecMaskMap : HairSpecMaskMap;
sampler hairSpecMaskMapSampler = sampler_state
{
    Texture     = <hairSpecMaskMap>;
    AddressU    = Wrap;
    AddressV    = Wrap;
    MinFilter   = Linear;
    MagFilter   = Linear;
    MipFilter   = Point;
    //MipMapLodBias = <MipLodBias>;
};

// 0: primartyShift
// 1: secondShift
// 2: specExp1
// 3: specExp2
float4 gHairLightingParam : HairLightingParam = {0.08f, 0.54f, 200.0f, 15.0f};
float4 gHairSpecularColor1 = float4(0.7f, 0.6f, 0.4f, 1.0f);
float4 gHairSpecularColor2 = float4(0.7f, 0.4f, 0.1f, 1.0f);

//------------------------------------------------------------------------------
/**
*/
float3
ShiftTangent(float3 T, float3 N, float shift)
{
	float3 shiftedT = T + shift * N;
	return normalize(shiftedT);
}

//------------------------------------------------------------------------------
/**
*/
float
StrandSpecular(float3 T, float3 H, float exponent)
{
	float dotTH = dot(T, H);
	float sinTH = sqrt(1.0f - dotTH*dotTH);
	float dirAtten = smoothstep(-1.0f, 0.0f, dotTH);
	return dirAtten * pow(sinTH, exponent);
}

float 
GetHairDiffuse(float3 T, float3 lightDir)
{
	const float dotTL = max(0, dot(T, lightDir));
	const float sinTL = sqrt(1.0f - dotTL*dotTL);
	return sinTL;
}

//------------------------------------------------------------------------------
/**
*/
float3
HairSpecular(float3 tangent, float3 normal, float3 lightVec, float3 viewVec, float2 uv)
{
	float shiftTex = tex2D(hairShiftMapSampler, uv) - 0.5f;
	float3 t1 = ShiftTangent(tangent, normal, gHairLightingParam[0] + shiftTex);
	float3 t2 = ShiftTangent(tangent, normal, gHairLightingParam[1] + shiftTex);

	float3 H = normalize(lightVec + viewVec);
	
	// specular lighiting
	float3 specular = gHairSpecularColor1 * StrandSpecular(t1, H, gHairLightingParam[2]);
	float3 specular2 = gHairSpecularColor2 * StrandSpecular(t2, H, gHairLightingParam[3]);

	// add second specular term, modulated with noise texture
	float specMask = tex2D(hairSpecMaskMapSampler, uv * 10.0f);
	specular2 *= specMask;

    // specular attenuation for hair facing away from light
    float specularAttenuation = saturate(1.75 * dot(normal, lightVec) + 0.25);

	specular = (specular + specular2) * specularAttenuation;

	return specular;
}

//------------------------------------------------------------------------------
/**
*/
float4
HairLighting(float3 tangent, float3 normal, float2 uv,
			 float3 lightVec, float3 viewVec,
			 float3 diffColor, float3 lightColor,
			 float ambOcc, sampler baseMap )
{
	// diffuse lighting : the lerp shifts the shadow boundary for a softer look
	float3 diffuse = saturate( lerp(0.25, 1.0, dot(normal, lightVec)) );
	diffuse *= diffColor * lightColor;

	// specular lighting
	float3 specular = HairSpecular( tangent, normal, lightVec, viewVec, uv );

	// final color
	float4 output = tex2D(baseMap, uv);
	output.rgb = (diffuse * specular) * tex2D(baseMap, uv) * lightColor;
	output.rgb *= ambOcc;	// modulate color by ambient occlusion term

	return output;
}