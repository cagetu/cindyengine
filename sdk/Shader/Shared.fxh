#ifndef __SHAREDED_FXH__
#define __SHAREDED_FXH__

//-------------------------------------------------------------
//	공유 데이터 
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

//-------------------------------------------------------------
float4x4 gWorldViewProj : ModelViewProjection;
float4x4 gWorld			: Model;
float4x4 gWorldView		: ModelView;

shared float4 matAmbient : MatAmbient;
shared float4 matDiffuse : MatDiffuse;
shared float4 matSpecular : MatSpecular;
shared float4 matEmissive : MatEmissive;

#endif  // __SHAREDED_FXH__