//******************************************************************************
/**	@author		cagetu
	@brief		Skin 재질 Shader
*/
//******************************************************************************

#if defined(_DEPTH_) || defined(_SMDEPTH_)

#include "..\\..\\sdk\\Shader\\DefaultSamplers.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\DepthFunc.fxh"

#else

#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\DefaultSamplers.fxh"
#include "..\\..\\sdk\\Shader\\Util_Skinning.fxh"
#include "..\\..\\sdk\\Shader\\FakeSSS.fxh"
#ifdef _SHADOW_MAP_
#include "..\\..\\sdk\\Shader\\Util_ShadowMap.fxh"
#endif

//------------------------------------------------------------------------------
//	Variables
//------------------------------------------------------------------------------
float3 lightPosition	: LightPosition;
float4 lightDiffuse		: LightDiffuse;
float4 lightSpecular	: LightSpecular;

float3 gEyePosition		: EyePosition;

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// 입력
struct VertexInput
{
	float4 position		: POSITION0;
	float2 uv0			: TEXCOORD0;
	float3 normal		: NORMAL0;
#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
#endif
	float4 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif
};

/// 출력
struct VertexOutput
{
	float4 position		: POSITION0;
	float2 uv0			: TEXCOORD0;
	#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
	#endif
	float3 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
	float3 normal		: TEXCOORD1;
	float3 lightVec		: TEXCOORD2;
	float3 eyeVec		: TEXCOORD3;	// (eyePos - localPos)
	#ifdef _SHADOW_MAP_
		#if _PSSM_
		float4 localPos	: TEXCOORD4;
		#else
		float4 shadowProjPos : TEXCOORD4;
		#endif
	#endif
};

//------------------------------------------------------------------------------
/**
*/
VertexOutput vsMain( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	/// position, normal
	float4 localPos = 0;
	float3 localNor = 0;
	float3 inputNormal = Input.normal;	//normalize(UnpackNormal(Input.normal));

	float3 inputTangent = Input.tangent.xyz;		//normalize(UnpackNormal(Input.tangent));
	float3 inputBinormal = Input.binormal.xyz;		//normalize(UnpackNormal(Input.binormal));
	float3 localTan = 0;
	float3 localBir = 0;

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
	#ifdef _NORMAL_MAP_
		localTan = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, gJoints );
		localBir = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, gJoints );
	#endif //_NORMAL_MAP_
#else
	localPos = Input.position;
	localNor = inputNormal;
	localTan = inputTangent;
	localBir = inputBinormal;
#endif

	//
	float4 posClip = TransformPosition(localPos, gWorldViewProj);
	Output.position = posClip;	// / posClip.w;

	// tangent, binormal
	Output.tangent = inputTangent;
	Output.binormal = localBir;

	/// normal
	//localNor = TransformNormal(localNor, gWorldViewProj);
	Output.normal = localNor;

	/// lightVec
	float3 lightVec = lightPosition - localPos.xyz;
	lightVec = normalize(lightVec);
	Output.lightVec = lightVec;

	// uv
	Output.uv0 = Input.uv0;	//UnpackUv(Input.uv0);

#ifdef _VERTEXCOLOR_
	Output.diff = Input.diff;
#endif

	float3 eyeVec = gEyePosition - localPos.xyz;
	eyeVec = normalize(eyeVec);
	Output.eyeVec = eyeVec;

#ifdef _SHADOW_MAP_
	#if _PSSM_
	Output.localPos = localPos;
	#else
	Output.shadowProjPos = TransformPosition(localPos, gWorldToTexProj);
	#endif
#endif

	return Output;
}

//------------------------------------------------------------------------------
/**
*/
float4 psMain( VertexOutput Input, uniform int NumLights ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	float4 diffColor = (float4)1.0f;
#ifdef _VERTEXCOLOR_
	diffColor = Input.diff;
#endif
	diffColor *= matDiffuse;

	/// normal
	float3 normal = 0;
#ifdef _NORMAL_MAP_
	normal = psNormalFromBumpMap(NormalMapSampler, Input.uv0, Input.tangent, Input.binormal, Input.normal );
#else
	normal = Input.normal;
#endif
	normal = normalize(normal);
	
	// lighting
	float4 diffuse = 0;
	
	// lighting
	float diffuseIntensity = HalfLambert( normal, Input.lightVec, 2 );	// Dot3Clamp(normal, Input.lightVec); // 

#ifdef _RIM_LIGHT_
	/// Nebula3의 rimLighting
	//float rimIntensity = RimLightIntensity(normal, Input.eyeVec);
	//diffuse = float4(rimIntensity, rimIntensity, rimIntensity, 0.0f);
	//diffuse += lightDiffuse * diffuseIntensity;
	//diffuse += lightDiffuse * diffuseIntensity * rimIntensity;

	float4 rimLight = Rim(normal, Input.eyeVec, Input.lightVec, 2.0f);
	diffuse = (diffuseIntensity) * lightDiffuse + rimLight;
#else
	diffuse = lightDiffuse * diffuseIntensity;
#endif

	float4 subsurface = GetSubsurfaceLite( diffuseIntensity );
	diffuse += subsurface;

	float4 albedo = (float4)1;
#ifdef _DIFFUSE_MAP_
	albedo = tex2D( DiffuseMapSampler0, Input.uv0 );
	#ifdef _SRGB_
	albedo = sRGBtolinearRGB( albedo );
	#endif

	#ifdef _EMISSIVE_MAP_
	//float4 illumination = tex2D( EmissiveMapSampler, Input.uv0 ) * MatEmissiveIntensity;
	//Output = albedo * diffuse * (1 - illumination) + albedo * illumination;
	//Output = albedo * diffuse + (illumination * 1.5f);
	Output = Output * diffuse * albedo;
	#else
	Output = Output * diffuse * albedo;
	#endif
#else
	Output = diffuse;
#endif

#ifdef _VERTEXCOLOR_
	Output = Output * Input.diff;
#endif
	
#ifdef _ALPHA_MAP_
	Output.a = tex2D(AlphaMapSampler, uv);
#endif

#ifdef _BLEND_ALPHA_
	Output.a *= gAlphaBlendFactor;
#endif

#ifdef _SHADOW_MAP_
	float lit = 0.0f;
	#if _PSSM_
		float distance = TransformPosition(Input.localPos, gWorldView).z;

		if (distance < gGlobalPSSMDistances[1])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[0]);
			lit = GetShadow(gGlobalPSSMBuffers[0], projLightPos);
			
			//Output.rgb += float4(1.0f, 0.0f, 0.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[2])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[1]);
			lit = GetShadow(gGlobalPSSMBuffers[1], projLightPos);
			
			//Output.rgb += float4(0.0f, 1.0f, 0.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[3])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[2]);
			lit = GetShadow(gGlobalPSSMBuffers[2], projLightPos);

			//Output.rgb += float4(0.0f, 0.0f, 1.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[4])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[3]);
			lit = GetShadow(gGlobalPSSMBuffers[3], projLightPos);		
		}
	#else
		lit = GetShadow(ShadowMapSampler, Input.shadowProjPos);
	#endif

	Output.rgb *= lit;
#endif

#ifdef _BLEND_ALPHA_
	// premultiplied alpha
	Output.rgb *= Output.a;
#endif

	#ifdef _SRGB_
	//Output = linearRGBtosRGB(Output);
	//Output = sRGBtolinearRGB(Output);
	#endif

	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p0
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain(NUM_LIGHTS);
	}
}

#endif // Depth