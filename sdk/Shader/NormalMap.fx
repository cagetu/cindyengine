#line 1 "NormalMap.fx"
//-------------------------------------------------------------
//	NormalGeometry.fx : 기본 Gemetry
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\lib.fx"
#include "..\\..\\sdk\\Shader\\lib_lights.fx"

//-------------------------------------------------------------
// vertex shader
//-------------------------------------------------------------
float4x4  matWorldViewProjection 	: ModelViewProjection;
float4x4  matWorldView				: ModelView;

float4 lightDiffuse : LightDiffuse;
float4 lightSpecular : LightSpecular;
float3 lightPosition : LightPosition;

float4 g_GlobalAmbient :GlobalAmbient;

float3 eyePosition : EyePosition;

texture diffuseMap		: DiffuseMap0;
texture normalMap		: NormalMap;
texture specularMap		: SpecularMap;
texture occlusionMap	: OcclusionMap;

struct VS_INPUT 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;	
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
};

struct VS_OUTPUT 
{
	float4 pos : POSITION0;
	float4 diff : COLOR0; 
	float2 tex0 : TEXCOORD0; 
	float3 lightVec : TEXCOORD1;
	float3 eyeVec : TEXCOORD2;
	float  depth : TEXCOORD3;	
};

VS_OUTPUT vsNormalGeometry( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );
	Output.tex0 = Input.tex0;
	Output.diff = Input.diff;
	
	//build object to tangent space transform matrix 
	float3x3 tangentXForm;
	tangentXForm[0] = Input.tangent;
	tangentXForm[1] = Input.binormal;
	tangentXForm[2] = Input.norm;

	float3 lightDir = lightPosition - Input.pos;
	Output.lightVec = mul( tangentXForm, lightDir );

	float3 eyeVec = eyePosition - Input.pos;
	Output.eyeVec = mul( tangentXForm, eyeVec );

	Output.diff = dot(Input.norm, lightDir)*0.5f + 0.5f;
	
	// depth
//	float3 view = mul( Input.pos, (float4x3)matWorldView );
//	float4 focalPlane = float4(0.0f, 0.0f, 1.0f, -8.0f);
//	float blurFactor = dot( float4(view, 1.0f), focalPlane) * 0.4f;
//	Output.depth = blurFactor*blurFactor;
//	float MaxBlurFactor = 3.0f / 4.0f;
//	Output.depth = min( Output.depth, MaxBlurFactor );

	float3 view = mul( Input.pos, (float4x3)matWorldView );
	Output.depth = view.z;
	
	return( Output );
}

//-------------------------------------------------------------
// Pixel Shader
//-------------------------------------------------------------
sampler diffuseMap_Sampler = sampler_state
{
   Texture = <diffuseMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler normalMap_Sampler = sampler_state
{
   Texture = <normalMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler specularMap_Sampler = sampler_state
{
   Texture = <specularMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler occlusionMap_Sampler = sampler_state
{
	Texture = <occlusionMap>;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;	
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
struct PS_INPUT 
{
	float4	diff : COLOR0;
	float2	tex0 : TEXCOORD0;
	float3	lightVec : TEXCOORD1;
	float3 	eyeVec : TEXCOORD2;
	float	depth : TEXCOORD3;
};

struct psOutput
{
	float4 color0 : COLOR0;
	//float4 color1 : COLOR1;
};

psOutput psNormalGeometry( PS_INPUT Input )
{
	psOutput Output = (psOutput)0;

	float4 result;
	
	// normal
	float4 normalMap = tex2D( normalMap_Sampler, Input.tex0 );
	float3 normal = Expand( normalMap.xyz );
	normal = normalize( normal );

	// diffuse
	float4 diffuseMap = tex2D( diffuseMap_Sampler, Input.tex0 );

	float3 lightDir = normalize(Input.lightVec);

	float3 lightDiff = HalfLambert(lightDir, normal, 2);
	lightDiff *= lightDiffuse.xyz;

	float4 diffuse = float4( lightDiff, 1.0f );
	result = diffuse;
	result *= diffuseMap;
	
	//result.xyz += g_GlobalAmbient.xyz;

	// Output
	Output.color0 = result;
	//Output.color1 = ComputeDepthBlur( Input.depth );

	return Output;
}

float4 psOnlyLight( PS_INPUT Input ) : COLOR
{
	// normal
	float4 normalMap = tex2D( normalMap_Sampler, Input.tex0 );
	float3 normal = Expand( normalMap.xyz );
	normal = normalize( normal );

	float3 lightDir = normalize(Input.lightVec);

	// diffuse
	float4 lightDiff = Lambert( normal, lightDir ) * lightDiffuse;
	// 최대값을 1로 해주어야 하지 않겠어???

	float4 result = float4( lightDiff.xyz, 1.0f );
	//result.xyz += g_GlobalAmbient.xyz;

	return result;
}

//--------------------------------------------------------------//
// Technique Section for Effect Workspace.
//--------------------------------------------------------------//
technique Geometry
{
	pass NormalPass
	{
		VertexShader = compile vs_1_1 vsNormalGeometry();
		PixelShader	= compile ps_2_0 psNormalGeometry();
	}
}

technique NoTexture
{
	pass Default
	{
		VertexShader = compile vs_1_1 vsNormalGeometry();
		PixelShader = compile ps_2_0 psOnlyLight();
	}
}

technique Depth
{
	// 알파테스트 완료..
	pass Default
	{
		VertexShader = compile vs_1_1 vsNormalGeometry();
		PixelShader	= compile ps_2_0 psNormalGeometry();	
	}
}
