//******************************************************************************
/**	@author		cagetu
	@brief		Light Pre Pass GBuffer Shader 만들기
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"

//--------------------------------------------------------------//
// ShaderVariables
//--------------------------------------------------------------//
#ifdef _SKINNED_
float4x3 gJoints[27] : JointPalette;
#endif

shared float4 gViewInfo : ViewInfo;

//--------------------------------------------------------------//
// Structures
//--------------------------------------------------------------//
/// 입력
struct VertexInput
{
	float4 position : POSITION0;
	float3 normal : NORMAL0;
	float2 uv0 : TEXCOORD0;
#ifdef _VERTEXCOLOR_
	float4 diff : COLOR0;
#endif
#ifdef _NORMAL_MAP_
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
#endif
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif
};
/// 출력
struct VertexOutput
{
	float4 position : POSITION0; 
   	float4 posView	: TEXCOORD0;
	float3 eyeScreenRay  :TEXCOORD1;
	float2 uv0 : TEXCOORD2;
	float3 normal : TEXCOORD3;
#ifdef _NORMAL_MAP_	
	float3 tangent : TEXCOORD4;
	float3 binormal : TEXCOORD5;
#endif
};

/// 출력
struct PixelOutput
{
	float4 MRT0		: COLOR0;	// Depth (view space)
	float4 MRT1		: COLOR1;	// Normal
};

//--------------------------------------------------------------//
//	Functions
//--------------------------------------------------------------//
/**	GBuffer VertexShader
*/
VertexOutput vsFillGBuffer( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	/// position, normal
	half4 localPos = 0;
	half3 localNor = 0;
	half3 inputNormal = Input.normal;	//normalize(UnpackNormal(Input.normal));	// Input.normal;

#ifdef _NORMAL_MAP_
	half3 inputTangent = Input.tangent.xyz;	//normalize(UnpackNormal(Input.tangent));
	half3 inputBinormal = Input.binormal;	//normalize(UnpackNormal(Input.binormal));
	half3 localTan = 0;
	half3 localBir = 0;
#endif

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
	#ifdef _NORMAL_MAP_
	localTan = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, gJoints );
	localBir = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, gJoints );
	#endif
#else
	localPos = Input.position;
	localNor = inputNormal;
	#ifdef _NORMAL_MAP_
	localTan = inputTangent;
	localBir = inputBinormal;
	#endif
#endif
	float4 position = TransformPosition( localPos, gWorldViewProj );
	float4 posView = TransformPosition( localPos, gWorldView );

	// Result
	Output.position = position;
	Output.posView = posView;

	float viewAspect = gViewInfo.x;
	float tanFOV = gViewInfo.y;

	position.xy /= position.w;
	Output.eyeScreenRay = GetEyeToScreenRay(position, viewAspect, tanFOV);

#ifdef _DIFFUSE_MAP_
	Output.uv0 = Input.uv0;	//UnpackUv(Input.uv0);
#endif

#ifdef _NORMAL_MAP_
	float3x3 TBN = GetTangentToObject( Input.tangent, Input.binormal, Input.normal );
	float3x3 tangentToViewSpace = mul( TBN, (float3x3)gWorldView );

	Output.tangent = tangentToViewSpace[0];
	Output.binormal = tangentToViewSpace[1];
	Output.normal = tangentToViewSpace[2];
#else
	Output.normal = mul( localNor, (float3x3)gWorldView );
#endif

	return Output;
}

//--------------------------------------------------------------//
/** GBuffer PixelShader
	MRT를 사용하지 않고, 하나의 GBuffer에 Normal.xy, Depth를 기록하고 싶은데,
	AlphaTest를 사용하려면 Depth를 어떻게 해야 할지가 궁금...
*/
PixelOutput psFillGBuffer( const VertexOutput Input )
{
	PixelOutput Output = (PixelOutput)0;

	// position(depth)
	//float depth = Input.posView.z/Input.posView.w;
	//float3 worldPos = Input.posView.xyz / Input.posView.w;
	//float depth = length(worldPos) / length(Input.eyeScreenRay);
	//float depth = NormalizedLinearDepth(Input.posView.x, gViewInfo.z, gViewInfo.w);
	float depth = length(Input.posView.xyz);
	Output.MRT0 = float4(EncodeDepth(depth), 0.0f, 1.0f);
	//Output.MRT0 = Input.posView;

#ifdef _DIFFUSE_MAP_
	float4 albedo = tex2D( DiffuseMapSampler0, Input.uv0 );
	Output.MRT0.a = albedo.a;
#endif
#ifdef _ALPHA_MAP_
	Output.MRT0.a = tex2D( AlphaMapSampler, Input.uv0 );
#endif

	// view space normal
	float3 normal;
#ifdef _NORMAL_MAP_
	normal = psNormalFromBumpMap(NormalMapSampler, Input.uv0, Input.tangent, Input.binormal, Input.normal );
#else
	normal = Input.normal;
#endif
	normal = normalize(normal);

#ifdef LDR
	// pack
	Output.MRT1.a = normal.y;
	Output.MRT1.g = normal.x;
#endif
	normal = PackNormal(normal);
	Output.MRT1 = half4(normal, 0.0f);
	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique GBuffer
{
	pass p0
	{
		VertexShader = compile vs_3_0 vsFillGBuffer();
		PixelShader = compile ps_3_0 psFillGBuffer();
	}
}
