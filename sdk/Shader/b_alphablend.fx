//------------------------------------------------------------------------------
//  b_alphablend.fx : Pass-shader for the color pass.
//	[����] Nebula3 - Radon Labs GmbH
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//------------------------------------------------------------------------------
technique t0
{
    pass p0
    {
        ZEnable           = True;
        ZWriteEnable      = False;
        ZFunc             = LessEqual;
        StencilEnable     = False;
        //FogEnable         = False;
        AlphaBlendEnable  = True;
        AlphaTestEnable   = False;
        AlphaFunc         = GreaterEqual;
        DestBlend         = InvSrcAlpha;
        SrcBlend          = One; // Premultiplied in pixel shader
        AlphaRef		  = 120;
        ScissorTestEnable = False;
        CullMode          = CCW;
    }
}
