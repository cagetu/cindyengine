#line 1 "pe_compose.fx"
//-------------------------------------------------------------
//	PostProcess.fx : 기본 Gemetry
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"

//-------------------------------------------------------------
// Constants
//-------------------------------------------------------------
// Overlay
float4 gScreenSize : DisplayResolution;
float4 gScreenProj[2] : ScreenProj;

//-------------------------------------------------------------
// Sampler
//-------------------------------------------------------------
texture colorTexture : DiffuseMap0;
sampler colorSampler = sampler_state
{
    Texture = <colorTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

//-------------------------------------------------------------
/**	Vertex Shader 구조체
*/
//-------------------------------------------------------------
struct VertexInput
{
    float3 position : POSITION;
    float2 texcoord : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
VertexOutput vs_main( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;

	output.position = float4(Input.position.xy, 0, 1) * gScreenProj[0] + gScreenProj[1];
	output.texcoord = Input.texcoord;

	return output;
}

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
float4 ps_main( VertexOutput Input ) : COLOR
{
	float4 color = tex2D( colorSampler, Input.texcoord );	
#ifdef _SRGB_
	//color = sRGBtolinearRGB( color );
#endif

    //float3 n;
    //n.xy = (tex2D(colorSampler, Input.texcoord).ag * 2.0) - 1.0;    
    //n.z = sqrt(1.0 - dot(n.xy, n.xy));
	//float4 c = float4(n, 1.0f);

	/// color control
	//color = AdjustLevel( color );
	//color = GrayScaleConversion( color );
	//color = PhotoFilter( color );

#ifdef _SRGB_
	color = linearRGBtosRGB(color);
#endif
	color.a = 1.0f;
	return color;
}

//-------------------------------------------------------------
/**	Technique
*/
//-------------------------------------------------------------
technique PostProcess
{
	Pass Default
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_main();
	}
}