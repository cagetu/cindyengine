#line 1 "Uber-LightPrePassTemplate.fx"
//******************************************************************************
/**	@author		cagetu
	@brief		UberShaderDeferred Shader 만들기
*/
//******************************************************************************

#if defined(_DEPTH_) || defined(_SMDEPTH_)
	#include "..\\..\\sdk\\Shader\\Shared.fxh"
	#include "..\\..\\sdk\\Shader\\DepthFunc.fxh"
#else
	#include "..\\..\\sdk\\Shader\\DefaultSamplers.fxh"

	#ifdef _GBUFFER_
		#include "..\\..\\sdk\\Shader\\LppGBuffer.fx"
	#else
		#include "..\\..\\sdk\\Shader\\Shared.fxh"
		#include "..\\..\\sdk\\Shader\\Util.fxh"
		#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
		#include "..\\..\\sdk\\Shader\\Util_Color.fxh"
		//#ifdef _SHADOW_MAP_
		//#include "..\\..\\sdk\\Shader\\Util_ShadowMap.fxh"
		//#endif

texture gLightBuffer : LightBuffer;
sampler LightBufferSampler = sampler_state
{
	Texture = <gLightBuffer>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = NONE;
	MipMapLodBias = <MipLodBias>;
};

texture gNormalBuffer : NormalBuffer;
sampler NormalBufferSampler = sampler_state
{
	Texture = <gNormalBuffer>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = NONE;
	MipMapLodBias = <MipLodBias>;
};

//--------------------------------------------------------------//
// ShaderVariables
//--------------------------------------------------------------//
const float2 gScreenSize : DisplayResolution;
const float2 gUVAdjust : ScreenUVAdjust;

/// view space normal -> world normal
float4x4 gInvView : InvTransposedView;

#if defined(_SPEC_MAP_) || defined(_REFLECT_MAP_) || defined(_RIM_LIGHT_)
float3 gEyePosition : EyePosition;
#endif

#ifdef _REFLECT_MAP_
float gReflectivity : Reflectivity = 0.1f;
#endif

#ifdef _BLEND_ALPHA_
float gAlphaBlendFactor : AlphaBlendFator = 1.0f;
#endif

float MatEmissiveIntensity : MatEmissiveIntensity = 0.2f;
float MatSpecularIntensity : MatSpecularIntensity = 1.0f;
float MatAmbientIntensity : MatAmbientIntensity = 0.5f;
float MatGlowIntensity : MatGlowIntensity = 4.0f;

//--------------------------------------------------------------//
// Structures
//--------------------------------------------------------------//
/// 입력
struct VertexInput
{
	float4 position		: POSITION0;
	float2 uv0			: TEXCOORD0;
#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
#endif
//	float3 normal		: NORMAL0;
//#ifdef _NORMAL_MAP_
//	float3 tangent		: TANGENT;
//	float3 binormal		: BINORMAL;
//#endif
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif
};
/// 출력
struct VertexOutput
{
	float4 position		: POSITION0; 
	float2 uv0			: TEXCOORD0;
//#ifdef _NORMAL_MAP_	
//	float3 tangent		: TANGENT;
//	float3 binormal		: BINORMAL;
//#endif
//	float3 normal		: TEXCOORD1;
#if defined(_SPEC_MAP_) || defined(_REFLECT_MAP_) || defined(_RIM_LIGHT_)
	float3 eyeVec		: TEXCOORD2;	// (eyePos - localPos)
#endif
//#ifdef _SHADOW_MAP_
//	#if _PSSM_
//	float4 localPos		: TEXCOORD3;
//	#else
//	float4 shadowProjPos : TEXCOORD3;
//	#endif
//#endif
#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
#endif
};

/// 출력
struct PixelInput
{
	float2 hPos			: VPOS;
	float2 uv0			: TEXCOORD0;
//#ifdef _NORMAL_MAP_	
//	float3 tangent		: TANGENT;
//	float3 binormal		: BINORMAL;
//#endif
//	float3 normal		: TEXCOORD1;
#if defined(_SPEC_MAP_) || defined(_REFLECT_MAP_) || defined(_RIM_LIGHT_)
	float3 eyeVec		: TEXCOORD2;	// (eyePos - localPos)
#endif
//#ifdef _SHADOW_MAP_
//	#if _PSSM_
//	float4 localPos		: TEXCOORD3;
//	#else
//	float4 shadowProjPos : TEXCOORD3;
//	#endif
//#endif
#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
#endif
};

//------------------------------------------------------------------------------
/**
	Forword Rendering에 사용하는 VertexShader
*/
VertexOutput vsMain( const VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	/// position, normal
	half4 localPos = 0;
	half3 localNor = 0;
	//half3 inputNormal = Input.normal;	//normalize(UnpackNormal(Input.normal));	// Input.normal;

//#ifdef _NORMAL_MAP_
//	half3 inputTangent = Input.tangent.xyz;	//normalize(UnpackNormal(Input.tangent));
//	half3 inputBinormal = Input.binormal;	//normalize(UnpackNormal(Input.binormal));
//	half3 localTan = 0;
//	half3 localBir = 0;
//#endif

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	//localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
	//#ifdef _NORMAL_MAP_
	//localTan = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, gJoints );
	//localBir = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, gJoints );
	//#endif
#else
	localPos = Input.position;
	//localNor = inputNormal;
	//#ifdef _NORMAL_MAP_
	//localTan = inputTangent;
	//localBir = inputBinormal;
	//#endif
#endif
	// Result
	Output.position = TransformPosition( localPos, gWorldViewProj );
	//Output.posView = TransformPosition( localPos, gWorldView );

#ifdef _DIFFUSE_MAP_
	Output.uv0 = Input.uv0;	//UnpackUv(Input.uv0);
#endif

#ifdef _VERTEXCOLOR_
	Output.diff = Input.diff;
#endif

#if defined(_SPEC_MAP_) || defined(_REFLECT_MAP_) || defined(_RIM_LIGHT_)
	float3 eyeVec = gEyePosition - localPos.xyz;
	eyeVec = normalize(eyeVec);
	Output.eyeVec = eyeVec;
#endif

//	Output.normal = localNor;
//#ifdef _NORMAL_MAP_
//	Output.tangent = localTan;
//	Output.binormal = localBir;
//#endif

//#ifdef _SHADOW_MAP_
//	#if _PSSM_
//	Output.localPos = localPos;
//	#else
//	Output.shadowProjPos = TransformPosition(localPos, gWorldToTexProj);
//	#endif
//#endif
	return Output;
}

//------------------------------------------------------------------------------
/**
	Forword Rendering에 사용하는 PixelShader
*/
float4 psMain( const PixelInput Input ) : COLOR0
{
	float4 Output = (float4)0;

	// Co-ords for texture lookups = pixel rasterisation pos / screen dimensions (e.g. 512/1024 = 0.5f)
	// But due to D3D's weird sampling rules, we have to correct the texture co-ordinate by offsetting it by a predefined amount
	float2 coords = Input.hPos.xy / gScreenSize.xy;		
	coords += gUVAdjust;

	//! Cell Shading에 사용할, N dot L의 값을 어디서 얻어온단 말이냐?!!
#ifdef _TOON_MAP_
	//float4 shadeColor = tex1D(ToonMapSampler, diffuseIntensity);
	//Output = shadeColor;
#endif

	/// normal
	float3 normal = 0;

	//@< G-Buffer에서 만든 view space normal Buffer를 재사용할 수는 없을까?! 
	//	World Space Normal = View Space Normal * Invert( Transpose(View Matrix) )
	// view space normal -> world normal
	float4 normalBuffer = tex2D( NormalBufferSampler, coords );
	float3 viewSpaceNormal = UnpackNormal( normalBuffer.xyz );
	normal = mul( viewSpaceNormal, (float3x3)gInvView );
	normal = normalize(normal);

//#ifdef _NORMAL_MAP_
//	normal = psNormalFromBumpMap(NormalMapSampler, Input.uv0, Input.tangent, Input.binormal, Input.normal );
//#else
//	normal = Input.normal;
//#endif
//	normal = normalize(normal);
	//@>

	float4 albedo = (float4)1;
#ifdef _DIFFUSE_MAP_
	albedo = tex2D( DiffuseMapSampler0, Input.uv0 );
	#ifdef _SRGB_
	albedo = sRGBtolinearRGB( albedo );
	#endif
#endif

	/// Shadow Map
	float shadowlit = 1.0f;
//#ifdef _SHADOW_MAP_
//	#if _PSSM_
//		float distance = TransformPosition(Input.localPos, gWorldView).z;
//
//		if (distance < gGlobalPSSMDistances[1])
//		{
//			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[0]);
//			shadowlit = GetShadow(gGlobalPSSMBuffers[0], projLightPos);
//			
//			//Output.rgb += float4(1.0f, 0.0f, 0.0f, 0.0f);
//		}
//		else if (distance < gGlobalPSSMDistances[2])
//		{
//			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[1]);
//			shadowlit = GetShadow(gGlobalPSSMBuffers[1], projLightPos);
//			
//			//Output.rgb += float4(0.0f, 1.0f, 0.0f, 0.0f);
//		}
//		else if (distance < gGlobalPSSMDistances[3])
//		{
//			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[2]);
//			shadowlit = GetShadow(gGlobalPSSMBuffers[2], projLightPos);
//			
//			//Output.rgb += float4(0.0f, 0.0f, 1.0f, 0.0f);
//		}
//		else if (distance < gGlobalPSSMDistances[4])
//		{
//			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[3]);
//			shadowlit = GetShadow(gGlobalPSSMBuffers[3], projLightPos);		
//		}
//	#else
//		shadowlit = GetShadow(ShadowMapSampler, Input.shadowProjPos);
//	#endif
//#endif

	/** lightBuffer에서 계산된 값을 찾아온다.
		LightColor.r * N.L * Att
		LightColor.g * N.L * Att
		LightColor.b * N.L * Att
		R.V^n * N.L * Att
	*/
	float4 lightBuffer = tex2D( LightBufferSampler, coords );

	// ambient
	float4 ambientTerm = 0;
	ambientTerm.rgb = GetAmbientCube( normal );

	/// diffuse
	float4 diffuseTerm = 1;

	float4 diffColor = 0;
	diffColor = matDiffuse;
#ifdef _VERTEXCOLOR_
	diffColor *= Input.diff;
#endif

	/// lightBuffer = lightDiffuse * diffuseIntensity;// * shadowLit + rimLight
	diffuseTerm = diffColor;
	diffuseTerm.rgb *= lightBuffer.rgb;
	diffuseTerm += ambientTerm * MatAmbientIntensity;
	diffuseTerm = saturate(diffuseTerm);	// 0~1사이의 값으로 보정 : saturate
	diffuseTerm *= albedo;
	Output = diffuseTerm;

	/// specular
	float4 specularTerm = 0;
#ifdef _SPEC_MAP_
	float4 specMap = tex2D( SpecularMapSampler, Input.uv0 );
	float3 specular = specMap.rgb;	// * lightSpecular.rgb; // * matSpecular.rgb;

	float specIntensity = lightBuffer.a;

	specularTerm.rgb = specIntensity * specular;
	#ifdef _SPEC_EXP_MAP_
		float4 specularMask = tex2D( SpecularExpMapSampler, Input.uv0 );
		specularTerm.rgb *= specularMask;
	#endif

	/// Glossiness
	specularTerm.rgb *= MatSpecularIntensity;
#endif
	Output += specularTerm;

	/// Reflect
	float3 reflectTerm = 0.0f;
#ifdef _REFLECT_MAP_
	float3 reflectVec = reflect(normal, Input.eyeVec);	// CalculateReflect(normal, Input.eyeVec);	//
	float reflectIntensity = gReflectivity;
/*
	#ifdef _SPEC_EXP_MAP_
		float4 reflect = reflectIntensity * specularMask;
		reflectIntensity = reflect.x;
	#endif
*/
	float4 reflectColor = texCUBE( ReflectMapSampler, reflectVec );
	reflectTerm = lerp(Output.xyz, reflectColor.xyz, reflectIntensity);
	//reflectTerm = Output.xyz * (1.0f - reflectIntensity) + reflectMap.xyz * reflectIntensity;
	Output.xyz = reflectTerm;
#endif

	///@cagetu -09/11/06 : Alpha값 설정에 대해서는 다시 설정하자. (현재 EarlyZ 공사 관계로 일단 이렇게...완료 후 수정요망!!!)
#ifdef _ALPHA_MAP_
	Output.a = tex2D(AlphaMapSampler, uv);
#else
	Output.a = albedo.a;
#endif
	
#ifdef _BLEND_ALPHA_
	Output.a *= gAlphaBlendFactor;
#endif

#ifdef _EMISSIVE_MAP_
	float4 emissive = tex2D( EmissiveMapSampler, Input.uv0 );
	Output.rgb += (emissive.rgb * MatEmissiveIntensity);
#endif

	//Output.rgb *= shadowlit;

#ifdef _GLOW_MAP_
	float4 glowMap = tex2D( GlowMapSampler, Input.uv0 );
	//float ldotn = dot(normalize(normal), normalize(Input.lightVec));
	//float4 glow = glowMap * saturate(1-ldotn);
	float4 glow = glowMap;// * saturate(1-diffuseIntensity);
	Output.rgb += glow.rgb * MatGlowIntensity * glowMap.a;
#endif

#ifdef _BLEND_ALPHA_
	// premultiplied alpha
	Output.rgb *= Output.a;
#endif
	/// 그림자!!!
	//Output.rgb = shadowlit;
	//Output.rgb = lightBuffer.rgb;
	//Output.rgb = GetAmbientCube( normal );

	return Output;
};

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p0
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}

#endif	/// GBuffer
#endif	/// ShadowMap