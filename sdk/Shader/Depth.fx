#line 1 "Depth.fx"
//-------------------------------------------------------------
//	Depth.fx : ���� ������
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\DepthBlur.fxh"

/// Constants

float4x4  matWorldViewProjection 	: ModelViewProjection;
float4x4  matWorldView : ModelView;

float4x3  matJoints[27]	: JointPalette;

/// Structs

struct vsInputStatic
{
	float4 pos : POSITION0;
	float3 nor : NORMAL0;
	float4 col : COLOR0;
};

struct vsInputSkinned
{
	float4 pos : POSITION0;
	float3 nor : NORMAL0;
	float4 col : COLOR0;
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES;
};

struct vsOutput
{
	float4 pos : POSITION0;
	float4 col : COLOR0;
	float depth : TEXCOORD0;	
};

struct psInput
{
	float4 color : COLOR0;
	float depth : TEXCOORD0;
};

/// Shader Implements

//-------------------------------------------------------------
/** @brief ���� �� ���
*/
vsOutput vsStaticDepth( vsInputStatic Input )
{
	vsOutput Output = (vsOutput)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );

	// Compute position in view space:
	Output.depth = mul( Input.pos, matWorldView ).z;

	return Output;
}

//-------------------------------------------------------------
/** @brief ���� �� ���
*/
vsOutput vsSkinnedDepth( vsInputSkinned Input )
{
	vsOutput Output = (vsOutput)0;

	float4 position = SkinnedPosition( Input.pos, Input.blendWeights, Input.blendIndices, matJoints );
	Output.pos = TransformPosition( position, matWorldViewProjection );

	// Compute position in view space:
	float4 posView = mul( position, matWorldView );
	Output.depth = posView .z / posView.w;

	return Output;
}

//-------------------------------------------------------------
/** @brief ���� �� ���
*/
float4 psDepth( psInput Input ) : COLOR0
{
	return ComputeDepthBlur( Input.depth ).xxxx;
}

///	Techniques

/// ���̰� ������
technique RenderStaticDepth < string Mask = "Depth"; >
{
	Pass p0
	{
		VertexShader = compile vs_2_0 vsStaticDepth();
		PixelShader = compile ps_2_0 psDepth();
	}
}

/// ���̰� ������
technique RenderSkinnedDepth < string Mask = "Depth|Skinned"; >
{
	Pass p0
	{
		VertexShader = compile vs_2_0 vsSkinnedDepth();
		PixelShader = compile ps_2_0 psDepth();
	}
}

/// ���̰� ������
technique DeferredStaticDepth < string Mask = "Deferred|Depth"; >
{
	Pass p0
	{
		VertexShader = compile vs_2_0 vsStaticDepth();
		PixelShader = compile ps_2_0 psDepth();
	}
}

/// ���̰� ������
technique DeferredSkinnedDepth < string Mask = "Deferred|Depth|Skinned"; >
{
	Pass p0
	{
		VertexShader = compile vs_2_0 vsSkinnedDepth();
		PixelShader = compile ps_2_0 psDepth();
	}
}

