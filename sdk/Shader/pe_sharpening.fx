#line 1 "pe_sharpening.fx"
//-------------------------------------------------------------
//	pe_sharpening.fx : Image Sharpening
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"

//-------------------------------------------------------------
// Constants
//-------------------------------------------------------------
float4x4 gWorldViewProj : ModelViewProjection;

// texture
texture gColorTexture : DiffuseMap0;
texture gBlurredTexture : DiffuseMap1;

//-------------------------------------------------------------
// Sampler
//-------------------------------------------------------------
sampler ColorSampler = sampler_state
{
    Texture = <gColorTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};
sampler BlurredSampler = sampler_state
{
    Texture = <gBlurredTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

//-------------------------------------------------------------
/**	Vertex Shader 구조체
*/
//-------------------------------------------------------------
struct VertexInput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
VertexOutput vsMain( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;
	
	output.position = mul(Input.position, gWorldViewProj);
	output.texcoord = Input.texcoord;
	
	return output;
}

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
float4 psMain( VertexOutput Input ) : COLOR
{
	float4 color = tex2D( ColorSampler, Input.texcoord );
	#ifdef _SRGB_
	//color = sRGBtolinearRGB( color );
	#endif
	
	float4 blurred = tex2D( BlurredSampler, Input.texcoord );

	color = lerp(blurred, color, 1.2);
	
	#ifdef _SRGB_
	//color = linearRGBtosRGB(color);
	#endif
	
	return color;
}

//-------------------------------------------------------------
/**	Technique
*/
//-------------------------------------------------------------
technique RenderScreenQuad
{
	Pass Default
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}

// scene
technique FinalScene
{
	Pass Default
	{
/*
        ZWriteEnable     = False;
        ZEnable          = False;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        AlphaBlendEnable = False;
        AlphaTestEnable  = False;
        CullMode         = None;
        StencilEnable    = False;
*/
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}