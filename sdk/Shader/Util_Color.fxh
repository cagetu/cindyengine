#ifndef __UTIL_COLOR_FXH__
#define __UTIL_COLOR_FXH__

//-------------------------------------------------------------
//	util_color.fxh : 장면의 색상에 대한 처리
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

#define _SRGB_

// color vars
shared float saturation : Saturation = {1.0};
shared float4 balance   : Balance    = {1.0f, 1.0f, 1.0f, 1.0f};
shared float4 luminance : Lumincance = {0.299f, 0.587f, 0.114f, 0.0f};
shared float fadeValue  : FadeValue  = {1.0f};

// color level
float4 inputLevel : InputLevel = {0.0f, 1.0f, 255.0f, 0.0f};	// black, gamma, white
float2 outputLevel : OutputLevel = {0.0f, 255.0f};				// black, white

// photo filter
float4 moodColor : MoodColor = {0.9f, 0.47f, 0.0f, 1.0f};
float ratio = 0.0f;

// seletive color
float4 gSeletiveColor : SeletiveColor = {1.0f, 1.0f, 1.0f, 1.0f};
float4 gSeletiveDestColor : SeletiveDestColor = {0.0f, 0.0f, 0.0f, 0.0f};

//
float4 ColorScale;
float4 OverlayColor;
float InvertGamma;

//-------------------------------------------------------------
/**
	UDK - GammaCorrection
*/
float4 GammaCorrection( in float4 sceneColor )
{
	half3 linearColor = lerp(sceneColor.rgb * ColorScale, OverlayColor.rgb, OverlayColor.a );
	return float4( pow(saturate(linearColor), InvertGamma), 1.0f);
}

//-------------------------------------------------------------
/**
	이미지 level 조정
*/
float4 AdjustLevel( in float4 sceneColor )
{
	float4 output = sceneColor;
	
	float inBlack = inputLevel.x;
	float inGamma = inputLevel.y;
	float inWhite = inputLevel.z;
	float outBlack = outputLevel.x;
	float outWhite = outputLevel.y;

	output = (pow(((sceneColor * 255.0) - inBlack) / (inWhite - inBlack), inGamma) * (outWhite - outBlack) + outBlack) / 255.0;
	return output;
}

//-------------------------------------------------------------
/**
	Used to output a scale value based on scene color luminance when additively blending.
*/
half4 AccumulateSceneColor( in half4 sceneColor )
{
	// Tweakable luminance scale
	const half SceneColorAccumulationFactor = 0.1f;
	const half MaxLuminanceScale = 0.1f;
	const half3 LuminanceWeights = half3(.3f, .59f, .11f) * SceneColorAccumulationFactor;
	half ScaledLuminance = clamp(dot(sceneColor.rgb, LuminanceWeights), 0.0f, MaxLuminanceScale);
	return half4(sceneColor.rgb, ScaledLuminance * ScaledLuminance);
}

//-------------------------------------------------------------
/**
	grayscale 적용
*/
float4 GrayScaleConversion( in float4 sceneColor )
{
    float4 grey = dot(sceneColor.xyz, luminance.xyz);
    float4 color = balance * lerp(grey, sceneColor, saturation);
    color.rgb *= fadeValue;

	return color;
}

//-------------------------------------------------------------
/**
	- moodColor: 아티스트가 지정한 분위기 연출 색상
	- luminance: 현재 장면의 명도인것 같은데, 자세히는 모르겠음
	- ratio: 블랜딩 정도
*/
float4 PhotoFilter( in float4 sceneColor )
{
	float4 cMood = moodColor;

	cMood = lerp(0, cMood, saturate(luminance*2.0f));
	cMood = lerp(cMood, 1, saturate(luminance-0.5f)*2.0f);

	return lerp(sceneColor, cMood, saturate(luminance * ratio));
}

//-------------------------------------------------------------
/**
*/
float rgbToRGB(float component)
{
	float result = 0;
	
	if (component <= 0.04045)
	{
		result = component/12.92;	
	}
	else
	{
		result = pow((component+0.055)/1.055, 2.4);
	}
	return result;
}

//-------------------------------------------------------------
/**
*/
float RGBTorgb(float component)
{
	float result = 0;
	
	if (component <= 0.0031308)
	{
		result = 12.92 * component;
	}
	else
	{
		result = 1.055 * pow(component, 1.0/2.4) - 0.055;
	}
	return result;
}

//-------------------------------------------------------------
/**
*/
float4 sRGBtolinearRGB( float4 rgb )
{
	float4 RGB;

	float3 diffuseCol = pow(rgb, 2.2);	// rgb;
	RGB.rgb = diffuseCol;// * diffuseCol;
	RGB.a = rgb.a;
	return RGB;
}

//-------------------------------------------------------------
/**
*/
float4 linearRGBtosRGB( float4 RGB )
{
	float4 rgb;
	
	rgb = pow(RGB, 1.0/2.2);
	rgb.a = RGB.a;
	return rgb;
}

//-------------------------------------------------------------
/**
*/
float4 RGBtoCMYK( float3 RGB )
{
	float4 CMYK;
	CMYK.xyz = 1.0f - RGB;
	CMYK.w = min( CMYK.x, min(CMYK.y, min(1.0f, CMYK.z)) );
	CMYK.xyz = (CMYK.xyz - CMYK.www) / (1.0f - CMYK.www);
	return CMYK;
}

//-------------------------------------------------------------
/**
*/
float3 CMYKtoRGB( float4 CMYK )
{
	float3 RGB;
	RGB.xyz = CMYK.xyz * (1.0f - CMYK.w) + CMYK.w;
	return RGB;
}

//-------------------------------------------------------------
/**
	@param srcColor : 장면 색상
	@param gSeletiveColor : 선택된 색상 값
	@param gSeletiveDestColor : 실제 조정하게 되는 CMYK
*/
float4 SeletiveColorCorrection( float4 srcColor )
{
	/// 변형할 색상의 영역치
	float colorRange = saturate( 1.0f - length(srcColor.xyz - gSeletiveColor.xyz) );
	
	/// CMYK로 변환
	float4 CMYK = RGBtoCMYK( srcColor.xyz );
	
	/// CMYK 공간에서 색 변형
	CMYK = lerp( CMYK, clamp(CMYK + gSeletiveDestColor, - 1, 1), colorRange );

	/// 최종 변형
	float4 result = 0;
	result.rgb = lerp( srcColor.rgb, CMYKtoRGB( CMYK ), colorRange );
	result.a = srcColor.a;
	return result;
}

#endif	// __UTIL_COLOR_FXH__