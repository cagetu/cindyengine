#ifndef __UTIL_FXH__
#define __UTIL_FXH__

//==============================================================================
//	util.fxh : shader utility functions.
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//==============================================================================
#include "..\\..\\sdk\\Shader\\config.fxh"

//------------------------------------------------------------------------------
/** Modelview-Projection 공간으로 변환 연산
*/
float4 
TransformPosition( const float4 Pos, const float4x4 WorldViewProjection )
{
	return mul( Pos, WorldViewProjection );
}
//------------------------------------------------------------------------------
float3 
TransformNormal( const float3 Normal, const float4x4 WorldViewProjection )
{
	return normalize( mul( Normal, (float3x3)WorldViewProjection ) );
}

//------------------------------------------------------------------------------
/** Dot3 Clamp 
*/
float 
Dot3Clamp( float3 v1, float3 v2 )
{
	return max(0, dot(v1, v2) );
}

//------------------------------------------------------------------------------
/** Expand
	@param v : 확장값
	@result float3 : 확장된 값
*/
float3 
Expand( float3 v )
{
	return (2.0f * v) - 1.0f;
}

//------------------------------------------------------------------------------
/** Reflect Vector
*/
float3
CalculateReflect( float3 Normal, float3 EyeVec )
{
	return 2.0f * dot(EyeVec, Normal) * Normal - EyeVec;
}

//------------------------------------------------------------------------------
/**
    Sample a texel with manual bilinear filtering (for texture formats which
    don't support filtering).
    
    FIXME: on 360 we can actually do the same without knowing texSize, saves
    the coordinate computations!
*/
float4 
SampleBilinear(sampler texSampler, float2 uv, float2 texSize)
{
#if USE_HARDWARE_SAMPLING
    return tex2D(texSampler, uv);
#else
    float2 invTexSize = float2(1.0, 1.0) / texSize;
    float2 uv01 = float2(uv.x + invTexSize.x, uv.y);
    float2 uv10 = float2(uv.x, uv.y + invTexSize.y);
    float2 uv11 = uv + invTexSize;
    
    float4 c00 = tex2D(texSampler, uv);
    float4 c01 = tex2D(texSampler, uv01);
    float4 c10 = tex2D(texSampler, uv10);
    float4 c11 = tex2D(texSampler, uv11);

    float2 ratios = frac(uv * texSize);
    float4 c = lerp(lerp(c00, c01, ratios.x), lerp(c10, c11, ratios.x), ratios.y);
    return c;    
#endif    
}

//------------------------------------------------------------------------------
/**
    Encode 2 values in the range 0..1 into a 4-channel vector. Used
    for encoding PSSM-depth values into a 32-bit-rgba value.
*/
float4 
Encode2(float2 inVals)
{
    return float4(inVals.x, frac(inVals.x * 256.0), inVals.y, frac(inVals.y * 256.0));
}

//------------------------------------------------------------------------------
/**
    Decode 2 values encoded by Encode2().
*/
float2 
Decode2(float4 inVals)
{
    return float2(inVals.x + (inVals.y / 256.0), inVals.z + (inVals.w / 256.0));
}

//------------------------------------------------------------------------------
/**
    Encode VSM depth values from the projection space depth (0..1)
*/
float4 
EncodeVSMDepth(float depth)
{
    return float4(depth, depth*depth, 0.0, 0.0);
}

//------------------------------------------------------------------------------
/**
    Decode VSM depth values from a shadow map texture.
*/
float2 
DecodeVSMDepth(sampler texSampler, float2 uv, float2 texSize)
{
    return SampleBilinear(texSampler, uv, texSize).xy;
}

//------------------------------------------------------------------------------
/**
    Sample a surface normal from a DXT5nm-compressed texture, return 
    TangentSurfaceNormal.
*/
float3 
SamplePackedNormal(sampler bumpSampler, float2 uv)
{
    float3 n;
    n.xy = (tex2D(bumpSampler, uv).ag * 2.0) - 1.0;    
    n.z = sqrt(1.0 - dot(n.xy, n.xy));
    return n;
}

//------------------------------------------------------------------------------
/**
    Sample a surface normal from a texture, return 
    TangentSurfaceNormal.
*/
float3 
SampleNormal(sampler bumpSampler, float2 uv)
{
#if USB_DXT5NM
	return SamplePackedNormal( bumpSampler, uv );
#else
	return (tex2D(bumpSampler, uv).xyz * 2.0) - 1.0;
#endif
}

//------------------------------------------------------------------------------
/**
    transform into world(object) space.
*/
float3x3
GetTangentToObject( float3 vTangent, float3 vBinormal, float3 vNormal )
{
	return float3x3(vTangent, vBinormal, vNormal);
}

//------------------------------------------------------------------------------
/**
    Sample tangent surface normal from bump map and transform into 
    world(object) space.
*/
float3
psNormalFromBumpMap(sampler bumpMapSampler, float2 uv, float3 vTangent, float3 vBinormal, float3 vNormal )
{
    float3x3 tangentFrame = float3x3(vTangent, vBinormal, vNormal);
    float3 tangentNormal = SampleNormal(bumpMapSampler, uv);
    return mul(tangentNormal, tangentFrame);
}

//------------------------------------------------------------------------------
/**
    Pack a normal [-1,1] -> [0->1]
*/
float3
PackNormal(float3 Normal)
{
	return Normal * 0.5f + 0.5f;
}

//------------------------------------------------------------------------------
/**
    Unpack a UB4N packed normal.
*/
float3
UnpackNormal(float3 packedNormal)
{
    return (packedNormal * 2.0) - 1.0;
}

//------------------------------------------------------------------------------
/**
    Unpack a 4.12 packed texture coord.
*/
float2
UnpackUv(float2 packedUv)
{
    return (packedUv / 4096.0);
}   

//------------------------------------------------------------------------------
/**
    Unpack a skin weight vertex component. Since the packing looses some
    precision we need to re-normalize the weights.
*/
float4
UnpackWeights(float4 weights)
{
    return (weights / dot(weights, float4(1.0, 1.0, 1.0, 1.0)));
}

//------------------------------------------------------------------------------
/**
    Scale down pseudo-HDR-value into RGB8.
    A16B16G16R16F 텍스쳐 대신 A8R8G8B8
*/
float4
EncodeHDR(in float4 rgba)
{
    return rgba * float4(0.5, 0.5, 0.5, 1.0);
}

/*
float4 EncodeHDR(in float4 rgba)
{
    const float colorSpace = 0.8;
    const float maxHDR = 8;
    const float hdrSpace = 1 - colorSpace;
    const float hdrPow = 10;
    const float hdrRt = 0.1;
    
    float3 col = clamp(rgba.rgb,0,1) * colorSpace;
    float3 hdr = pow(clamp(rgba.rgb,1,10),hdrRt)-1;
    float4 result;
    hdr = clamp(hdr, 0, hdrSpace);
    result.rgb = col + hdr;
    result.a = rgba.a;
    return result;
}
*/

//------------------------------------------------------------------------------
/**
    Scale up pseudo-HDR-value encoded by EncodeHDR() in shaders.fx.
*/   
float4 
DecodeHDR(in float4 rgba)
{
    return rgba * float4(2.0f, 2.0f, 2.0f, 1.0f);
}   

/*
float4 DecodeHDR(in float4 rgba)
{
    const float colorSpace = 0.8;
    const float maxHDR = 8;
    const float hdrSpace = 1 - colorSpace;
    //const float hdrPow = log(maxHDR)/log(hdrSpace);
    const float hdrPow = 10;
    //const float hdrRt = 1/hdrPow;
    const float hdrRt = 0.1;
        
    float3 col = clamp(rgba.rgb,0,colorSpace) * (1/colorSpace);
    float3 hdr = pow(clamp(rgba.rgb - colorSpace, 0, hdrSpace)+1,hdrPow)-1;
    float4 result;
    result.rgb = col + hdr;
    result.a = rgba.a;
    return result;
}
*/

//------------------------------------------------------------------------------
/**	view space depth 값을 0에서 1사이의 값으로 변환해준다.

	z = (Z-Zn) / (Zf-Zn)
*/
float
NormalizedLinearDepth(float depth, float nearPlane, float farPlane)
{
	//return (depth-nearPlane) / (farPlane-nearPlane);
	return depth / (farPlane-nearPlane);
}

// Normalize/Denormalize Depth [0, X] <-> [0, 1]
float NormalizeDepth(float d, float range)
{
	return d / range;
}
float DenormalizeDepth(float d, float range)
{
	return d * range;
}

//------------------------------------------------------------------------------
/**
	worldPos * viewProj = hPos;
	depth = hPos.z / hPos.w;	(0.0f ~ 1.0f)
	
	worldPos = hPos * viewProjInv;
*/
float4
GetDepthToWorldPos( float zOverW, float4x4 invViewProj, float2 texCoord )
{
	/// H 는 -1에서 1 사이에서 이 Pixel에 viewport 위치이다. (non-homogeneous)
	float4 H = float4(texCoord.x * 2 - 1, (1 - texCoord.y) * 2 - 1, zOverW, 1);  
	//float4 H = float4(texCoord.x, texCoord.y, zOverW, 1.0f);

	/// view-Projection inverse에 의해 변환
	float4 D = mul(H, invViewProj);

	/// world 위치를 얻기 위해, w로 나눈다.
	float4 worldPos = D / D.w;

	return worldPos;
}

//------------------------------------------------------------------------------
/**
	카메라 공간에서의 Screen으로 Ray
*/
float3
GetEyeToScreenRay( float4 screenPos, float viewAspect, float invTanHalfFOV )
{
	return float3(screenPos.x * viewAspect, screenPos.y, invTanHalfFOV);  
}

//------------------------------------------------------------------------------
/**
	카메라 공간에서의 Pos
*/
float3
GetPixelPos( float3 eyeScreenRay, float depth )
{
	return normalize(eyeScreenRay) * depth;
}

//------------------------------------------------------------------------------
/**
    EncodeDepth()

    Encode depth into 2 8-bit values. d:[0,1]
*/
float2
EncodeDepth(float d)
{
    return float2(floor(d) / 256.0, frac(d));
}

//------------------------------------------------------------------------------
/**
   DecodeDepth()
    
    Decode depth from 2 8-bit values.
*/
float
DecodeDepth(float2 codedDepth)
{
    return codedDepth.x * 256.0 + codedDepth.y;
}

//------------------------------------------------------------------------------
/**
*/
float2 PackFloat16(float depth)
{
    depth /= 4;
    float Integer = floor(depth);
    float fraction= frac(depth);
    
    return float2( Integer/256, fraction);    
}

//------------------------------------------------------------------------------
/**
*/
float UnpackFloat16( half2 depth )
{
   const float2 unpack = {1024.0f, 4.0f};

   return dot(unpack, depth);
}

//------------------------------------------------------------------------------
/**
*/
float3 
ReconstructNormal( float x, float y )
{
	float3 normal;
	normal.x = x*2-1;
	normal.y = y*2-1;
	normal.z = -sqrt(1-dot(normal.xy,normal.xy));
	return normal;
}

// Encode/Decode Normal
float2 EncodeNormal(float3 n)
{
	float p = sqrt(n.z*8+8);
	return n.xy/p + 0.5;
}

float3 DecodeNormal(float2 encodedNormal)
{
	float2 fenc = encodedNormal*4 - 2;
	float f = dot(fenc, fenc);
	float g = sqrt(1 - f/4);
	float3 n;
	n.xy = fenc*g;
	n.z = 1 - f/2;
	return n;
}

//------------------------------------------------------------------------------
/**
	Light Pre Pass의 Depth와 Normal을 한번에 압축
	cf) http://drilian.com/category/development/graphics/light-pre-pass-render/
*/
float4
PackDepthNormal( float Depth, float3 Normal )
{
	float4 output;  

	// High depth (currently in the 0..127 range   
	Depth = saturate(Depth);  
	output.z = floor(Depth*127);  

	// Low depth 0..1   
	output.w = frac(Depth*127);  

	// Normal (xy)   
	output.xy = Normal.xy*.5+.5;  

	// Encode sign of 0 in upper portion of high Z   
	if(Normal.z < 0)  
		output.z += 128;  

	// Convert to 0..1   
	output.z /= 255;  

	return output;
}

//------------------------------------------------------------------------------
/**
	Light Pre Pass의 Depth와 Normal을 해제
	cf) http://drilian.com/category/development/graphics/light-pre-pass-render/
*/
float4
UnpackDepthNormal( in float4 Input, out float Depth, out float3 Normal )
{
	// Read in the normal xy   
	Normal.xy = Input.xy*2-1;  

	// Compute the (unsigned) z normal
	Normal.z = 1.0 - sqrt(dot(Normal.xy, Normal.xy));  
	float hiDepth = Input.z*255;  

	// Check the sign of the z normal component   
	if(hiDepth >= 128)  
	{  
		Normal.z = -Normal.z;  
		hiDepth -= 128;  
	}  

	Depth = (hiDepth + Input.w)/127.0;  
}

float Square(float A)
{
	return A * A;
}
float2 Square(float2 A)
{
	return A * A;
}
float3 Square(float3 A)
{
	return A * A;
}
float4 Square(float4 A)
{
	return A * A;
}

#endif  // __UTIL_FXH__