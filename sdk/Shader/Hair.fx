//******************************************************************************
/**	@author		cagetu
	@brief		Hair 재질 Shader
*/
//******************************************************************************

#if defined(_DEPTH_) || defined(_SMDEPTH_)

#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\DepthFunc.fxh"

#else

#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"
#include "..\\..\\sdk\\Shader\\DefaultSamplers.fxh"
#include "..\\..\\sdk\\Shader\\Util_Skinning.fxh"
#include "..\\..\\sdk\\Shader\\HairLighting.fxh"
#ifdef _SHADOW_MAP_
#include "..\\..\\sdk\\Shader\\Util_ShadowMap.fxh"
#endif

//------------------------------------------------------------------------------
//	Variables
//------------------------------------------------------------------------------
float3 lightPosition	: LightPosition;
float4 lightDiffuse		: LightDiffuse;
float4 lightSpecular	: LightSpecular;

float3 gEyePosition		: EyePosition;

#ifdef _BLEND_ALPHA_
float gAlphaBlendFactor : AlphaBlendFator = 1.0f;
#endif

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// 입력
struct VertexInput
{
	float4 position		: POSITION0;
	float2 uv0			: TEXCOORD0;
	float3 normal		: NORMAL0;
#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
#endif
	float4 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif
};

/// 출력
struct VertexOutput
{
	float4 position		: POSITION0;
	float2 uv0			: TEXCOORD0;
	#ifdef _VERTEXCOLOR_
	float4 diff			: COLOR0;
	#endif
	float3 tangent		: TANGENT;
	float3 binormal		: BINORMAL;
	float3 normal		: TEXCOORD1;
	float3 lightVec		: TEXCOORD2;
	float3 eyeVec		: TEXCOORD3;	// (eyePos - localPos)
	#ifdef _SHADOW_MAP_
		#if _PSSM_
		float4 localPos	: TEXCOORD4;
		#else
		float4 shadowProjPos : TEXCOORD4;
		#endif
	#endif
};

//------------------------------------------------------------------------------
/**
*/
VertexOutput vsMain( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	/// position, normal
	float4 localPos = 0;
	float3 localNor = 0;
	float3 inputNormal = Input.normal;	//normalize(UnpackNormal(Input.normal));

	float3 inputTangent = Input.tangent.xyz;		//normalize(UnpackNormal(Input.tangent));
	float3 inputBinormal = Input.binormal.xyz;		//normalize(UnpackNormal(Input.binormal));
	float3 localTan = 0;
	float3 localBir = 0;

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
	#ifdef _NORMAL_MAP_
		localTan = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, gJoints );
		localBir = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, gJoints );
	#endif //_NORMAL_MAP_
#else
	localPos = Input.position;
	localNor = inputNormal;
	localTan = inputTangent;
	localBir = inputBinormal;
#endif

	//
	float4 posClip = TransformPosition(localPos, gWorldViewProj);
	Output.position = posClip;	// / posClip.w;

	// tangent, binormal
	Output.tangent = localTan;
	Output.binormal = localBir;

	/// normal
	//localNor = TransformNormal(localNor, gWorldViewProj);
	Output.normal = localNor;

	/// lightVec
	float3 lightVec = lightPosition - localPos.xyz;
	lightVec = normalize(lightVec);
	Output.lightVec = lightVec;

	// uv
	Output.uv0 = Input.uv0;	//UnpackUv(Input.uv0);

#ifdef _VERTEXCOLOR_
	Output.diff = Input.diff;
#endif

	float3 eyeVec = gEyePosition - localPos.xyz;
	eyeVec = normalize(eyeVec);
	Output.eyeVec = eyeVec;

#ifdef _SHADOW_MAP_
	#if _PSSM_
	Output.localPos = localPos;
	#else
	Output.shadowProjPos = TransformPosition(localPos, gWorldToTexProj);
	#endif
#endif

	return Output;
}

//------------------------------------------------------------------------------
/**
*/
float4 psMain( VertexOutput Input, uniform int NumLights ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	float4 diffColor = (float4)1.0f;
#ifdef _VERTEXCOLOR_
	diffColor = Input.diff;
#endif
	diffColor *= matDiffuse;

	float3 normal = Input.normal;
	float3 tangent = Input.binormal;
	float3 lightVec = Input.lightVec;
	float3 viewVec = Input.eyeVec;
	float2 uv = Input.uv0;

	float ambientOcculsion = 1.0f;
	
	// diffuse lighting : the lerp shifts the shadow boundary for a softer look
	float3 diffuse = saturate( lerp(0.25, 1.0, dot(normal, lightVec)) );
	diffuse *= diffColor * matDiffuse.xyz;

	// specular lighting
	float3 specular = HairSpecular( tangent, normal, lightVec, viewVec, uv ) * lightSpecular.xyz;

	// final color
	Output.rgb = (diffuse + specular) * tex2D(DiffuseMapSampler0, uv) * lightDiffuse.xyz;
	Output.rgb *= ambientOcculsion;	// modulate color by ambient occlusion term

#ifdef _ALPHA_MAP_
	Output.a = tex2D(AlphaMapSampler, uv);
#endif

#ifdef _BLEND_ALPHA_
	Output.a *= gAlphaBlendFactor;
#endif

#ifdef _SHADOW_MAP_
	float lit = 0.0f;
	#if _PSSM_
		float distance = TransformPosition(Input.localPos, gWorldView).z;

		if (distance < gGlobalPSSMDistances[1])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[0]);
			lit = GetShadow(gGlobalPSSMBuffers[0], projLightPos);
			
			//Output.rgb += float4(1.0f, 0.0f, 0.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[2])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[1]);
			lit = GetShadow(gGlobalPSSMBuffers[1], projLightPos);
			
			//Output.rgb += float4(0.0f, 1.0f, 0.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[3])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[2]);
			lit = GetShadow(gGlobalPSSMBuffers[2], projLightPos);

			//Output.rgb += float4(0.0f, 0.0f, 1.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[4])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[3]);
			lit = GetShadow(gGlobalPSSMBuffers[3], projLightPos);		
		}
	#else
		lit = GetShadow(ShadowMapSampler, Input.shadowProjPos);
	#endif

	Output.rgb *= lit;
#endif

#ifdef _BLEND_ALPHA_
	// premultiplied alpha
	Output.rgb *= Output.a;
#endif

	#ifdef _SRGB_
	//Output = linearRGBtosRGB(Output);
	//Output = sRGBtolinearRGB(Output);
	#endif

	return Output;
}



//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
/*	//@cagetu : EarylZ와 곂친다... 정리가 필요할 듯...
	pass p0
	{
        ZEnable           = True;
        ZWriteEnable      = True;
        ZFunc             = LessEqual;
        AlphaBlendEnable  = False;
        AlphaFunc         = LessEqual;
        AlphaTestEnable   = True;
        AlphaRef		  = 0x2;
        CullMode          = None;

		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain(NUM_LIGHTS);
	}
*/
	pass p1
	{
        ZEnable           = True;
        ZWriteEnable      = False;
        ZFunc             = Equal;
        AlphaBlendEnable  = False;
        AlphaTestEnable   = False;
        CullMode          = None;

		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain(NUM_LIGHTS);
	}
/*	
	pass p2
	{
        ZEnable           = True;
        ZWriteEnable      = False;
        ZFunc             = Less;
        AlphaBlendEnable  = True;
        AlphaTestEnable   = False;
        AlphaFunc         = LessEqual;
        AlphaRef		  = 0x2;
        CullMode          = CW;
        DestBlend		  = SrcAlpha;
        SrcBlend		  = InvSrcAlpha;

		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain(NUM_LIGHTS);
	}
	pass p3
	{
        ZEnable           = True;
        ZWriteEnable      = True;
        AlphaTestEnable   = False;
        CullMode          = CCW;
	
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain(NUM_LIGHTS);
	}
*/	
}

#endif // Depth