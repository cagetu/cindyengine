#line 1 "DepthBlur.fx"
//-------------------------------------------------------------
//	"DepthBlur.fx" : 깊이 값을 구한다.
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------

//const float d_focus = 360.0f;
//const float d_near = 80.0f;
//const float d_far = 1000.0f;
//const float clamp_far = 1.0f;

/// d_foucus, d_near, d_far, clamp_far
float4 gDofParam : DOFParameter  = { 10.0f, 5.0f, 100.0f, 1.0f };

//==================================================================
/** 깊이 blur값을 구한다.
	@param depth : pixel의 깊이값
	@result float : focus
	@remarks ATI DOF 문서에 있는 ComputeDepthBlur가 잘못 되어 있는것이 
		 함수 전 페이지의 설명하고 함수 동작이 좀 틀리더군요 
		 분기에서 f가 -1에서 1사이로 리니어 하게 증가 되게 되는데 
		 이를 최종적으로 0~1로 만들어 버립니다. 
		 결국 최종값이 카메라와의 거리에 비례하여 리니어 하게 증가합니다. 
		 포커스 기준으로 거리에 따라 증가를 해야 하는데 말이죠 
		 depth < vDofParams.y 조건에서 
		 f의 값을 절대값으로 사용 하고 분기 밖 f * 0.5f + 0.5f를 하지 말아야 합니다 
		 최종적으로 1~0 0~1로 나오게요
*/
//==================================================================
float ComputeDepthBlur( float depth )
{
	float focus = 0.0f;

	//const float d_focus = 10.0f;
	//const float d_near = 5.0f;
	//const float d_far = 15.0f;
	//const float clamp_far = 1.0f;

	const float d_focus = gDofParam.x;
	const float d_near = gDofParam.y;
	const float d_far = gDofParam.z;
	const float clamp_far = gDofParam.w;
  
	if (depth < d_focus)
	{
		// scale depth value between near blur distance and focal distance to [-1, 0] range
		focus = (depth - d_focus) / (d_focus - d_near);
	}
	else
	{
		// scale depth value between focal distance and far blur
		// distance to [0, 1] range
		focus = (depth - d_focus) / (d_far - d_focus);

		// clamp the far blur to a maximum blurriness
		focus = clamp( focus, 0, clamp_far );
	}

	// scale and bias into [0, 1] range
	return focus*0.5f + 0.5f;
}
