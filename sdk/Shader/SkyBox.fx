//******************************************************************************
/**	@author		cagetu
	@brief		Sky Box Rendering
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
float4x4 gInvWorldViewProj : InvModelViewProjection;

//------------------------------------------------------------------------------
// Samplers
//------------------------------------------------------------------------------
texture gCubeMap : CubeMap0;
sampler CubeMapSampler = sampler_state
{
    Texture = <gCubeMap>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// �Է�
struct VertexInput
{
	float4 position : POSITION0;
};
/// ���
struct VertexOutput
{
	float4 position : POSITION0;
	float3 texCoord : TEXCOORD0;
};

//------------------------------------------------------------------------------
/**
*/
VertexOutput vsMain( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;
	Output.position = Input.position;
    Output.texCoord = normalize( mul(Input.position, gInvWorldViewProj) );
	return Output;
}

//------------------------------------------------------------------------------
/**
*/
float4 psMain( VertexOutput Input ) : COLOR0
{
    //float4 Output = texCUBE( CubeMapSampler, Input.texCoord );
    float4 Output = float4(1.0f, 1.0f, 1.0f, 1.0f);
	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p1
	{
		VertexShader = compile vs_2_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}