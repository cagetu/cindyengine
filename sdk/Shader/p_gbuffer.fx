//------------------------------------------------------------------------------
//  p_gbuffer.fx : Pass-shader for the Light Pre G-Buffer
//	[����] Nebula3 - Radon Labs GmbH
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//------------------------------------------------------------------------------
technique t0
{
    pass p0
    {
        // Disable writing to depth buffer
  //      ZWriteEnable		= true;
  //      ZEnable				= true;
  //      ZFunc				= LessEqual;

		//AlphaTestEnable		= false;
		//AlphaBlendEnable	= false;
        ColorWriteEnable  = RED|GREEN|BLUE|ALPHA; // write depth as color for later use in other shaders	// 0
        ZEnable           = True;
        ZWriteEnable      = True;
        ZFunc             = LessEqual;
        StencilEnable     = False;
        FogEnable         = False;
        AlphaBlendEnable  = False;
        AlphaTestEnable   = False;
        AlphaFunc         = GreaterEqual;
        ScissorTestEnable = False;
        CullMode          = CCW;
    }
}
