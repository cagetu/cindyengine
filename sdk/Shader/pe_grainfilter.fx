#line 1 "pe_radialblur.fx"
//------------------------------------------------------------------------------
//  pe_FILMGRAIN.fx : Film Grain Filter Post-Effect.
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//------------------------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util_Color.fxh"

//-------------------------------------------------------------
// Constants
//-------------------------------------------------------------
float4x4 gWorldViewProj : ModelViewProjection;

// texture
texture gColorTexture : DiffuseMap0;
texture gRandomTexture : RandomMap;

//
float interference = 0.01f;
float density = 4.0;
float timeCounter : TimeValue = 0;
float offset = 0.0;

//-------------------------------------------------------------
// Sampler
//-------------------------------------------------------------
sampler ColorSampler = sampler_state
{
    Texture = <gColorTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

//-------------------------------------------------------------
// Sampler
//-------------------------------------------------------------
sampler RandomSampler = sampler_state
{
    Texture = <gRandomTexture>;
    AddressU = WRAP;
    AddressV = WRAP;
    AddressW = WRAP;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = Point;
    //MipMapLodBias = <MipLodBias>;
};

//-------------------------------------------------------------
/**	Vertex Shader 구조체
*/
//-------------------------------------------------------------
struct VertexInput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
};
struct VertexOutput
{
    float4 position : POSITION;
    float2 texcoord : TEXCOORD0;
    float2 pos : TEXCOORD1;
};

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
VertexOutput vsMain( const VertexInput Input )
{
	VertexOutput output = (VertexOutput)0;
	
	output.position = mul(Input.position, gWorldViewProj);
	output.texcoord = Input.texcoord;
	output.pos = float2(output.position.x, -output.position.y);

	return output;
}

//-------------------------------------------------------------
/**	Screen Quad 처리 함수
*/
float4 psMain( VertexOutput Input ) : COLOR
{
    float4 color = tex2D(ColorSampler, Input.texcoord); 
    
	#ifdef _SRGB_
	//color = sRGBtolinearRGB( color );
	#endif
	
	float rand = tex3D(RandomSampler, float3(Input.pos * density, timeCounter)) - offset;

	color = color + (interference * rand);
	#ifdef _SRGB_
	//color = linearRGBtosRGB(color);
	#endif
	
	return color;
}

//-------------------------------------------------------------
/**	Technique
*/
//-------------------------------------------------------------
technique RenderScreenQuad
{
	Pass Default
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}

// scene
technique FinalScene
{
	Pass Default
	{
/*
        ZWriteEnable     = False;
        ZEnable          = False;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        AlphaBlendEnable = False;
        AlphaTestEnable  = False;
        CullMode         = None;
        StencilEnable    = False;
*/
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}

