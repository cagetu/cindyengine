#line 1 "ShadowMap.fx"
//-------------------------------------------------------------
//	ShadowMap.fx : 그림자 Depth 얻어오기
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Skinning.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"

/// Constants

//float4 diffuse : DirLightDiffuse;

/// Structs

struct vsInputStatic
{
	float4 pos : POSITION0;
	float3 nor : NORMAL0;
	float4 col : COLOR0;
};

struct vsInputSkinned
{
	float4 pos : POSITION0;
	float3 nor : NORMAL0;
	float4 col : COLOR0;
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES;
};

struct vsOutput
{
	float4 pos		: POSITION0;
	float4 depth	: TEXCOORD0;
};

struct psInput
{
	float4 depth : TEXCOORD0;
};

//------------------------------------------------------------------
//	Static Mesh
//------------------------------------------------------------------
vsOutput vsStaticMesh( vsInputStatic Input )
{
	vsOutput Output = (vsOutput)0;

	Output.pos = mul( Input.pos, gWorldViewProj );
	Output.depth = float4( Output.pos.zzz, Output.pos.w );

	return( Output );
}

//------------------------------------------------------------------
//	Skinned Mesh
//------------------------------------------------------------------
vsOutput vsSkinnedMesh( vsInputSkinned Input )
{
	vsOutput Output = (vsOutput)0;
	
#ifdef _SKINNED_
	float4 position = SkinnedPosition( Input.pos, Input.blendWeights, Input.blendIndices, gJoints );
	Output.pos = TransformPosition( position, gWorldViewProj );
#endif

	Output.depth = float4( Output.pos.zzz, Output.pos.w );
	
	return( Output );
}

//==================================================================
//	pixel shader
//==================================================================

//------------------------------------------------------------------
float4 psDepthShadow( vsOutput Input ) : COLOR0
{
	float depth = Input.depth.b/Input.depth.a;
	return float4( depth.xxx, 1.0 );
	//return float4(0.0f, 0.0f, 0.0f, 1.0f);
}

//==================================================================
// Technique Section for Effect Workspace.
//==================================================================

/// 정적 메쉬의 그림자 깊이 
technique RenderStaticDepthShadow
{
	pass Default
	{
		VertexShader = compile vs_2_0 vsStaticMesh();
		PixelShader = compile ps_2_0 psDepthShadow();
	}
}

/// 스킨 메쉬의 그림자 깊이 
technique RenderSkinnedDepthShadow
{
	pass Default
	{
		VertexShader = compile vs_2_0 vsSkinnedMesh();
		PixelShader = compile ps_2_0 psDepthShadow();
	}
}