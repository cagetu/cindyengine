#line 1 "LightPrePassDirectionalLighting.fx"
//*************************************************************
//	LightPrePassDirectionalLighting.fx : Light Pre Pass Lighting
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//*************************************************************
#include "..\\..\\sdk\\Shader\\LightPrePass.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
//#ifdef _SHADOW_MAP_
#include "..\\..\\sdk\\Shader\\Util_ShadowMap.fxh"
//#endif

//--------------------------------------------------------------//
/**
	LightColor.r * N.L * Att
	LightColor.g * N.L * Att
	LightColor.b * N.L * Att
	R.V^n * N.L * Att
*/
float4 psMain( const VertexOutput Input ) : COLOR0
{
	float4 output = (float4)0;

	// Co-ords for texture lookups = pixel rasterisation pos / screen dimensions (e.g. 512/1024 = 0.5f)
	// But due to D3D's weird sampling rules, we have to correct the texture co-ordinate by offsetting it by a predefined amount
	//float2 coords = Input.vPos.xy / gScreenSize.xy;		
	//coords += gUVAdjust;

	float2 coords = Input.texcoord;

	float4 depthBuffer = tex2D( DepthSampler, coords );
	float4 normalBuffer = tex2D( NormalSampler, coords );

	/// 카메라 공간의 Pos
	float depth = DecodeDepth(depthBuffer.xy);
	float3 viewSpacePos = GetPixelPos( Input.eyeToScreenRay, depth );
	//float3 viewSpacePos = depthBuffer;

	/// 카메라 공간의 노말
	float3 viewSpaceNormal = UnpackNormal( normalBuffer.xyz );

	// Computes light attenuation and direction
	float3 viewSpaceLightPos = gLightPosition;
	float3 pixelToLight = viewSpaceLightPos - viewSpacePos;
	float3 lightDir = normalize(pixelToLight);

	/// attenuation
	float attenuation = 1.0f;

	/// diffuse
	float diffIntensity = HalfLambert(viewSpaceNormal, lightDir, 2);	// dot(lightDir, normal)

	// R.V == Phong
	//float3 eyeDir = normalize( viewSpacePos );
	//float specIntensity = pow(saturate(dot(reflect(eyeDir, viewSpaceNormal), lightDir)), 32);
	float3 eyeDir = normalize( -viewSpacePos );
	float specIntensity = Blinn( eyeDir, lightDir, viewSpaceNormal, 32 );
 
	// rimLight
	float4 rimIntensity = Rim2(viewSpaceNormal, eyeDir, lightDir, 4.0f) * RimIntensity;

	// color
	float4 ambientTerm = 0;
	float4 diffuseTerm = 0;

	//ambientTerm.rgb = GetAmbientCube( viewSpaceNormal );

	/**	1. view space pos * inv view matrix = world pos
		2. world pos -> light view projection space
	*/

	/// viewSpacePos * InvView * LightViewProj * TexBias
	/// gWorldToTexProj : InvView * LightViewProj * TexBias
	float4 pos = float4(viewSpacePos.xyz, 1.0f);
	float4 worldPos = TransformPosition( pos, gInvView );
	float4 lightSpacePos = TransformPosition( worldPos, gWorldToTexProj );

	// shadow
	float shadowLit = 1.0f;
//#ifdef _SHADOW_MAP_
	#if _PSSM_
		float distance = TransformPosition(Input.localPos, gWorldView).z;

		if (distance < gGlobalPSSMDistances[1])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[0]);
			shadowLit = GetShadow(gGlobalPSSMBuffers[0], projLightPos);
			
			//Output.rgb += float4(1.0f, 0.0f, 0.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[2])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[1]);
			shadowLit = GetShadow(gGlobalPSSMBuffers[1], projLightPos);
			
			//Output.rgb += float4(0.0f, 1.0f, 0.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[3])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[2]);
			shadowLit = GetShadow(gGlobalPSSMBuffers[2], projLightPos);
			
			//Output.rgb += float4(0.0f, 0.0f, 1.0f, 0.0f);
		}
		else if (distance < gGlobalPSSMDistances[4])
		{
			float4 projLightPos = TransformPosition(Input.localPos, gGlobalPSSMTransforms[3]);
			shadowLit = GetShadow(gGlobalPSSMBuffers[3], projLightPos);		
		}
	#else
		shadowLit = GetShadow(ShadowMapSampler, lightSpacePos);
	#endif
//#endif // _SHADOW_MAP_

	diffuseTerm = gLightDiffuse * diffIntensity * attenuation + gLightDiffuse * rimIntensity;
#ifdef _SKIN_LIGHTING_
	diffuseTerm += GetSubsurfaceLite( diffIntensity );
#endif
	diffuseTerm *= shadowLit;

	output = float4(diffuseTerm.xyz, specIntensity * attenuation); 
	return output;
}

//--------------------------------------------------------------//
//	Techniques
//--------------------------------------------------------------//
technique DirectionLight
{
	pass p0
	{
		VertexShader = compile vs_3_0 vsMain();
		PixelShader = compile ps_3_0 psMain();
	}
}