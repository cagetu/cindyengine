#ifndef __SKINNED_FX__
#define __SKINNED_FX__

#line 1 "Skinned.fx"

//-------------------------------------------------------------
//	Skinned.fx : 기본 Gemetry
//	Copyright ?2007 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"

//-------------------------------------------------------------
// vertex shader
//-------------------------------------------------------------
float4x4  matWorldViewProjection : ModelViewProjection;
float4x4  matWorldView : ModelView;

float4x3  matJoints[27]	: JointPalette;

float3 lightPosition : LightPosition;
float4 lightDiffuse : LightDiffuse;
float4 lightSpecular : LightSpecular;

float3 eyePosition : EyePosition;

float4 globalAmbient : GlobalAmbient;

// texture
texture diffuseMap	: DiffuseMap0;
texture normalMap	: NormalMap;
texture specularMap	: SpecularMap;

// direction of light from sky (view space)
float3 DirFromSky < string UIDirectional = "Direction from Sky"; > = { 0.0f, -1.0f, 0.0f };            

//
struct VS_INPUT 
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES;
};

struct vsInput
{
	float4 pos : POSITION0;   
	float3 norm : NORMAL0;
	float2 tex0 : TEXCOORD0;
	float4 diff : COLOR0;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES;
};

struct VS_OUTPUT 
{
	float4 pos : POSITION0;
	float4 diff : COLOR0; 
	float2 tex0 : TEXCOORD0; 
	float3 normal : TEXCOORD1;
	float3 eyeVec : TEXCOORD2;
	float3 lightDir : TEXCOORD3;
};

VS_OUTPUT vsSkinned( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	int i = 0;

	float3 inputNormal = normalize(UnpackNormal(Input.norm));	// Input.norm;
	float4 skinnedPos = SkinnedPosition( Input.pos, Input.blendWeights, Input.blendIndices, matJoints );
	float3 skinnedNormal = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, matJoints );

	float4 position = TransformPosition( skinnedPos, matWorldViewProjection );
	float3 normal = TransformNormal( skinnedNormal, matWorldViewProjection );

	float3 eye = eyePosition - skinnedPos.xyz; //Input.pos.xyz;
	Output.pos = position;
	Output.normal = skinnedNormal;
	Output.tex0 = UnpackUv(Input.tex0);	
	Output.eyeVec = eye;

	// diffuse
	float3 lightDir = normalize( lightPosition - skinnedPos.xyz );//LightDirection(skinnedPos, lightPosition);
	float diffuseIntensity = HalfLambert( skinnedNormal, lightDir, 1 );	//Lambert(skinnedNormal, lightDir);

	// 그림자 색상과 보간을 하여, 부드럽게 넘어가도록 하는 방법...
	//static float4 GlobalBackLightColor = { 0.0f, 0.0f, 0.0f, 0.0f };    // ground	
	//float4 diffuse = lerp( GlobalBackLightColor, lightDiffuse, diffuseIntensity );
	//diffuse *= Input.diff * matDiffuse;

	float4 diffuse = diffuseIntensity * lightDiffuse;// * Input.diff * matDiffuse;

	// ambient
	float4 ambient = globalAmbient * matAmbient;

	// specular
	//float4 specular = float4( Blinn( eye, lightDir, normal, matSpecular.w) * matSpecular.rgb, 0 );
	float4 specular = 0.0f;

	// emissive
	float4 emissive = matEmissive;
	emissive = 0;
	
	// 반구 조명..
	float4 hemisphere = Hemisphere( normal, -DirFromSky, 0.0f );

	// result
	Output.diff = diffuse;// + ambient + specular + emissive;// + hemisphere;
	//Output.diff = hemisphere;
	Output.diff.a = Input.diff.a;

	// light Direction
	Output.lightDir = lightDir;

	return( Output );
}

//------------------------------------------------------------------------------
/**	Light 없는 VertexShader
*/

VS_OUTPUT vsUnlit( VS_INPUT Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	Output.pos = mul( Input.pos, matWorldViewProjection );
	Output.tex0 = Input.tex0;

	return( Output );
}

//------------------------------------------------------------------------------
/**	Unreal-Marcus 데이터를 활용한 Light 테스트
*/
VS_OUTPUT vsSkinnedTangent( vsInput Input )
{
	VS_OUTPUT Output = (VS_OUTPUT)0;

	int i = 0;

	//float3 inputNormal = Input.norm;
	//float3 inputTangent = Input.tangent;
	//float3 inputBinormal = Input.binormal;
	float3 inputNormal = normalize(UnpackNormal(Input.norm));
	float3 inputTangent = normalize(UnpackNormal(Input.tangent));
	float3 inputBinormal = normalize(UnpackNormal(Input.binormal));

	float4 skinnedPos = SkinnedPosition( Input.pos, Input.blendWeights, Input.blendIndices, matJoints );
	float3 skinnedNormal = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, matJoints );
	float3 skinnedTangent = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, matJoints );
	float3 skinnedBinormal = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, matJoints );

	float4 position = TransformPosition( skinnedPos, matWorldViewProjection );
	//float3 normal = TransformNormal( skinnedNormal, matWorldViewProjection );

	Output.pos = position;
	Output.normal = skinnedNormal;
	//Output.tex0 = Input.tex0;	
	Output.tex0 = UnpackUv(Input.tex0);	

	//build object to tangent space transform matrix 
	float3x3 tangentXForm = float3x3(skinnedTangent, skinnedBinormal, skinnedNormal);

	// diffuse
	float3 eye = normalize( eyePosition - skinnedPos.xyz );
	Output.eyeVec = mul( tangentXForm, eye );

	float3 lightDir = normalize( lightPosition - skinnedPos.xyz );
	Output.lightDir = mul( tangentXForm, lightDir );

	Output.diff = Input.diff;
	return( Output );
}

//-------------------------------------------------------------
// Pixel Shader
//-------------------------------------------------------------
sampler diffuseMap_Sampler = sampler_state
{
   Texture = <diffuseMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
/*
sampler shadowMap_Sampler = sampler_state
{
   Texture = <shadowMap>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   ADDRESSW = CLAMP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
*/

sampler normalMap_Sampler = sampler_state
{
   Texture = <normalMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
sampler specularMap_Sampler = sampler_state
{
   Texture = <specularMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

texture ambCubeTexture : ENVIRONMENT
<
    string ResourceName = "cubeCathDiff.dds";
    string ResourceType = "Cube";
>;

struct PS_INPUT 
{
	float4 diff : COLOR0;
	float2 tex0 : TEXCOORD0;
	float3 normal : TEXCOORD1;
	float3 eyeVec : TEXCOORD2;
	float3 lightDir : TEXCOORD3;
};

//------------------------------------------------------------------------------
/**
*/
float4 psUnlit( PS_INPUT Input ) : COLOR
{
	float4 result = (float4)0;
	
	float4 diffuseMapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	
	result = diffuseMapColor;
	return result;
}

//------------------------------------------------------------------------------
/**
*/
float4 psModel( PS_INPUT Input ) : COLOR
{
	float4 result = (float4)0;

	float3 N = normalize( Input.normal );	
	float3 V = normalize( Input.eyeVec );

	// Rim Light
	float4 rimLight = Rim( N, V, Input.lightDir, 1.0f );

	// Diffuse
	float4 diffuseMapColor = tex2D( diffuseMap_Sampler, Input.tex0 );
	//float3 diffColor = (rimLight.rgb + Input.diff.rgb) * diffuseMapColor.rgb;
	//float4 diffuse = float4(diffColor, Input.diff.a * diffuseMapColor.a);
	//float4 diffuse = (rimLight + Input.diff) * diffuseMapColor;
	float4 diffuse = diffuseMapColor * Input.diff;

	// Specular
	float4 specular = float4( Blinn( V, Input.lightDir, N, matSpecular.w ) * matSpecular.rgb, 1 );

	// Composite
	//result = diffuse + globalAmbient + rimLight;
	//result = diffuse + rimLight;// + specular;
	result = diffuse;
	return result;
}

//------------------------------------------------------------------------------
/**	Unreal-Marcus 데이터를 활용한 Light 테스트
*/
float4 psNormalSpec( PS_INPUT Input ) : COLOR
{
	float4 result = (float4)0;

	// normal
	float4 normalMap = tex2D( normalMap_Sampler, Input.tex0 );
	//float3 normal = Expand( normalMap.xyz );
	float3 normal = SampleNormal( normalMap_Sampler, Input.tex0 );

	float3 N = normalize(normal);
	float3 V = Input.eyeVec;
	float3 L = Input.lightDir;

	// Rim Light
	float4 rimIntensity = Rim( N, V, L, 2 );
	//float rimIntensity = RimLightIntensity( N, V );

	// Diffuse
	float4 diffuseMap = tex2D( diffuseMap_Sampler, Input.tex0 );
	float diffuseIntensity = HalfLambert( normal, L, 2 );
	float4 diffuse = float4( ((globalAmbient.rgb + rimIntensity.xyz) + (lightDiffuse.rgb*matDiffuse.rgb*diffuseIntensity)), matDiffuse.a);
	//diffuse += float4(rimIntensity, rimIntensity, rimIntensity, 0 );
	diffuse *= Input.diff;	
	diffuse *= diffuseMap;

	// Specular
	float4 specularMap = tex2D( specularMap_Sampler, Input.tex0 );
	float specularIntensity = Blinn( V, L, N, 32 );
	float4 specular = float4( specularIntensity * specularMap.a * specularMap.rgb * matSpecular.rgb * lightSpecular.rgb * diffuseIntensity, 0.0 );

	// Composite
	result = diffuse + specular;
	result = AccumulateSceneColor(result);
	return result;
}

//--------------------------------------------------------------//
// Technique Section for Effect Workspace.
//--------------------------------------------------------------//
technique Geometry < string Mask = "Skinned"; >
{
	pass NormalPass
	{
		VertexShader = compile vs_2_0 vsSkinned();
		PixelShader	= compile ps_2_0 psModel();
	}
}

technique NoShade < string Mask = "Unlit"; >
{
	pass NormalPass
	{
		VertexShader = compile vs_1_1 vsUnlit();
		PixelShader	= compile ps_2_0 psUnlit();
	}
}

technique NormalSpec < string Mask = "Skinned|Tangent"; >
{
	pass p0
	{
		VertexShader = compile vs_2_0 vsSkinnedTangent();
		PixelShader	= compile ps_2_0 psNormalSpec();	
	}
}

#endif	//  __SKINNED_FX__