#line 1 "DepthOfField.fx"
//-------------------------------------------------------------
//	Depth Of Field.fx : 피사계 심도 구현
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//-------------------------------------------------------------
#include "..\\..\\sdk\\Shader\\Util.fxh"

// texture
texture diffuseMap : DiffuseMap0;
texture diffuseMap1 : DiffuseMap1;
texture diffuseMap2 : DiffuseMap2;

//==================================================================
/**	Sampler
*/
//==================================================================
sampler gDepthSampler = sampler_state
{
    Texture = <diffuseMap>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

sampler gLowSceneSampler = sampler_state
{
    Texture = <diffuseMap1>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

sampler gSceneSampler = sampler_state
{
    Texture = <diffuseMap2>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

//==================================================================
//==================================================================
float2 g_ScreenRes : DisplayResolution = float2(1024, 768);
float2 g_MaxCoC = float2(5.0f, 10.0f);				// 픽셀안에서 confusion(CoC) radius의 최대 circle과 지름
float g_RadiusScale = 0.4f;							// scale factor for maximum CoC size on low res. image

// contains poisson-distributed positions on the unit circle
static float2 poisson[8] = {  
  float2( 0.0,      0.0),
  float2( 0.527837,-0.085868),
  float2(-0.040088, 0.536087),
  float2(-0.670445,-0.179949),
  float2(-0.419418,-0.616039),
  float2( 0.440453,-0.639399),
  float2(-0.757088, 0.349334),
  float2( 0.574619, 0.685879)
};

//-------------------------------------------------------------
/** @brief		Poisson Filter 
	@remarks	이 방법은 장면 텍스쳐의 Alpha 채널에 Depth를 저장하여, 블러된 장면과 기본 장면을
				블랜딩하여, 그 차이의 깊이값 만큼 보간하는 방법인데,
				Depth를 따로 기록하는 방식에서는 사용하기가 복잡해 진다..
				다른 방법을 이용해야 한다!!!
*/
float4 
psDof_ShaderX3_Pass1( in float2 inTexcoord : TEXCOORD0 ) : COLOR
{
	// Depth
	float4 depthBuffer = tex2D( gDepthSampler, inTexcoord );
	float centerDepth = depthBuffer.a;

	// convert depth into blur radius in pixels
	float discRadius = abs( centerDepth * g_MaxCoC.y - g_MaxCoC.x );

	// compute disc radius on low-res image
	float2 inverseViewportDimensions = 1.0f / g_ScreenRes;
	float2 pixelSizeLow = 4.0f * inverseViewportDimensions;
	float discRadiusLow = discRadius * g_RadiusScale;

	float4 result = 0;

	for (int t=0; t<4; t++)
	{
		// fetch low-res tap
		float2 coordLow = inTexcoord + (pixelSizeLow * poisson[t] * discRadiusLow);
		float4 tapLow = tex2D( gLowSceneSampler, coordLow );

		// fetch high-res tap
		float2 coordHigh = inTexcoord + (inverseViewportDimensions * poisson[t] * discRadius);
		float4 tapHigh = tex2D( gSceneSampler, coordHigh );
		
/*
		// put tap blurriness into [0, 1] range
		float tapBlur = abs( tapHigh.a * 2.0f - 1.0f );

		// mix low- and hi-res taps based on tap blurriness
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		// apply leaking reduction: lower weight for taps that are
		// closer than the center tap and in focus
		tap.a = (tap.a >= centerDepth) ? 1.0f : abs(tap.a*2.0f - 1.0);

		// accumulate
		result.rgb += tap.rgb * tap.a;
		result.a += tap.a;
*/

		/// DepthBuffer를 이용하려면, blur 처리만 하고, leaking reduction은 할 수가 없을 듯!!!
		float tapBlur = abs( centerDepth * 2.0f - 1.0f );
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		result.rgb += (tap.rgb * centerDepth);
		result.a += centerDepth;
	}

	// normalize and return result
	return result/result.a;
}

//-------------------------------------------------------------
/** @brief		Poisson Filter 
	@remarks	이 방법은 장면 텍스쳐의 Alpha 채널에 Depth를 저장하여, 블러된 장면과 기본 장면을
				블랜딩하여, 그 차이의 깊이값 만큼 보간하는 방법인데,
				Depth를 따로 기록하는 방식에서는 사용하기가 복잡해 진다..
				다른 방법을 이용해야 한다!!!
*/
float4 
psDof_ShaderX3_Pass2( in float2 inTexcoord : TEXCOORD0 ) : COLOR
{
	// Depth
	float4 depthBuffer = tex2D( gDepthSampler, inTexcoord );
	float centerDepth = depthBuffer.a;

	// convert depth into blur radius in pixels
	float discRadius = abs( centerDepth * g_MaxCoC.y - g_MaxCoC.x );

	// compute disc radius on low-res image
	float2 inverseViewportDimensions = 1.0f / g_ScreenRes;
	float2 pixelSizeLow = 4.0f * inverseViewportDimensions;
	float discRadiusLow = discRadius * g_RadiusScale;

	float4 result = 0;

	for (int t=4; t<8; t++)
	{
		// fetch low-res tap
		float2 coordLow = inTexcoord + (pixelSizeLow * poisson[t] * discRadiusLow);
		float4 tapLow = tex2D( gLowSceneSampler, coordLow );

		// fetch high-res tap
		float2 coordHigh = inTexcoord + (inverseViewportDimensions * poisson[t] * discRadius);
		float4 tapHigh = tex2D( gSceneSampler, coordHigh );
		
/*
		// put tap blurriness into [0, 1] range
		float tapBlur = abs( tapHigh.a * 2.0f - 1.0f );

		// mix low- and hi-res taps based on tap blurriness
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		// apply leaking reduction: lower weight for taps that are
		// closer than the center tap and in focus
		tap.a = (tap.a >= centerDepth) ? 1.0f : abs(tap.a*2.0f - 1.0);

		// accumulate
		result.rgb += tap.rgb * tap.a;
		result.a += tap.a;
*/

		/// DepthBuffer를 이용하려면, blur 처리만 하고, leaking reduction은 할 수가 없을 듯!!!
		float tapBlur = abs( centerDepth * 2.0f - 1.0f );
		float4 tap = lerp( tapHigh, tapLow, tapBlur );

		result.rgb += (tap.rgb * centerDepth);
		result.a += centerDepth;
	}

	result = result/result.a;
	result.a = 0.5f;
	
	// normalize and return result
	return result;
}

//*************************************************************
//	ShaderX5's Depth Of Field
//*************************************************************
// Depth buffer resolve parameters
// x = focus distance
// y = focus range
// z = near clip
// w = far clip / (far clip - near clip)
float4 gDofParams : DOFParameter = float4(30.0f, 50.0f, 0.0f, 0.0f);
float4 gViewParams : ViewInfo;

//-------------------------------------------------------------
/**
*/
float4 
psDof_ShaderX5( in float2 inTexcoord : TEXCOORD0 ) : COLOR
{
	float4 Output = 0;

	// Depth
	float4 depthBuffer = tex2D( gDepthSampler, inTexcoord );

	// Blurred Image (1/4로 축소되어 Blurring된 이미지)	
	float4 blurredLowSceneBuffer = tex2D( gLowSceneSampler, inTexcoord );

	// 원본 이미지
	float4 sceneBuffer = tex2D( gSceneSampler, inTexcoord );

	// 카메라 공간으로 Depth를 역변환
	//float depth = DecodeDepth(depthBuffer);
	//float nearClip = gViewParams.z;
	//float q = gViewParams.w / (gViewParams.w - gViewParams.z);
	//float sceneZ = (-nearClip * q) / (depth - q);

	// viewDepth!!!
	float sceneZ = DecodeDepth(depthBuffer.xy);

	// Blur Factor 계산
	float blurFactor = saturate( abs(sceneZ - gDofParams.x) / gDofParams.y );

	// 결과 Texel 계산
	Output.rgb = lerp(sceneBuffer.rgb, blurredLowSceneBuffer.rgb, blurFactor);
	Output.a = 1.0f;

	return Output;
}

//*************************************************************
//	Nebula3's Depth Of Field
//*************************************************************
//-------------------------------------------------------------
// depth of field samples
static const int MaxDofSamples = 23;
float2 DofSamples[MaxDofSamples] = {
    { 
        { 0.0, 0.0 },
        
        { -0.326212, -0.40581  },
        { -0.840144, -0.07358  },
        { -0.695914,  0.457137 },
        { -0.203345,  0.620716 },
        {  0.96234,  -0.194983 },
        {  0.473434, -0.480026 },
        {  0.519456,  0.767022 },
        {  0.185461, -0.893124 },
        {  0.507431,  0.064425 },
        {  0.89642,   0.412458 },
        { -0.32194,   0.93261f },

        {  0.326212,  0.40581  },
        {  0.840144,  0.07358  },
        {  0.695914, -0.457137 },
        {  0.203345, -0.620716 },
        { -0.96234,   0.194983 },
        { -0.473434,  0.480026 },
        { -0.519456, -0.767022 },
        { -0.185461,  0.893124 },
        { -0.507431, -0.064425 },
        { -0.89642,  -0.412458 },
        {  0.32194,  -0.93261f },
    }
};   

/// focusDistance, focusLength, filterRadius
shared float3 dofDistances : DoFDistances  = {20.0f, 10.0f, 2.0f};

//-------------------------------------------------------------
//	Get a depth-fo-field blurred sample
//-------------------------------------------------------------
float4 
psDof_Nebula3( sampler sourceTexture, float2 uv )
{
	// depth
	float d = DecodeDepth( tex2D(gDepthSampler, uv).zw );

	// compute focus-blur param (0 -> no blur, 1 -> max blur)
	float focusDist = dofDistances.x;
	float focusLength = dofDistances.y;
	float filterRadius = dofDistances.z;
	float focus = saturate( abs(d-focusDist) / focusLength );

	// perform a gaussian blur around uv
	float4 sample = 0.0f;
	float dofWeight = 1.0f / MaxDofSamples;
	float uvMul = focus * filterRadius * (float2(1.0, 1.0) / g_ScreenRes.xy);
	for (int i=0; i<MaxDofSamples; i++)
	{
		sample += tex2D( sourceTexture, uv + (DofSamples[i] * uvMul) );
	}
    sample *= dofWeight;

    return sample;
}

//-------------------------------------------------------------
/**
*/
float4 
psDOF(in float2 inTexcoord : TEXCOORD0) : COLOR0
{
	float4 c = psDof_Nebula3( gSceneSampler, inTexcoord );
	return c;
}

//-------------------------------------------------------------
/**
*/
float4 
psDofPass1(in float2 inTexcoord : TEXCOORD0) : COLOR0
{
	float4 output = psDof_ShaderX3_Pass1(inTexcoord);
	return output;	
}
float4 
psDofPass2(in float2 inTexcoord : TEXCOORD0) : COLOR0
{
	float4 output = psDof_ShaderX3_Pass2(inTexcoord);
	return output;	
}


//-------------------------------------------------------------
/**
*/
float4 
psDof(in float2 inTexcoord : TEXCOORD0) : COLOR0
{
	float4 output = psDof_ShaderX5(inTexcoord);
	return output;	
}

//==================================================================
//	Techniques...
//==================================================================
technique DoF_ShaderX3
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psDofPass1();
	}
	Pass p1
	{
		AlphaBlendEnable = True;
		BlendOp = Add;
		DestBlend = InvSrcAlpha;
		SrcBlend = SrcAlpha;
		
		VertexShader = null;
		PixelShader = compile ps_3_0 psDofPass2();
	}
}

technique DoF_ShaderX5
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_3_0 psDof();
	}
}

technique DOF_Nebula3
{
	Pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 psDOF();
	}
}