//******************************************************************************
/**	@author		cagetu
	@brief		UberShaderDeferred Shader 만들기
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Shared.fxh"

//#define LDR

//--------------------------------------------------------------//
// ShaderVariables
//--------------------------------------------------------------//
float4x4 gWorldViewProj : ModelViewProjection;
float4x4 gWorldView	: ModelView;

#ifdef _SKINNED_
float4x3 gJoints[27] : JointPalette;
#endif

//--------------------------------------------------------------//
// Texture & Sampler
//--------------------------------------------------------------//
#ifdef _DIFFUSE_MAP_ 
texture gDiffuseMap0 : DiffuseMap0;
sampler DiffuseMapSampler0 = sampler_state
{
   Texture = <gDiffuseMap0>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
#endif
#ifdef _NORMAL_MAP_
texture gNormalMap : NormalMap;
sampler NormalMapSampler = sampler_state
{
   Texture = <gNormalMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = Point;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
#endif
#ifdef _SPECULAR_MAP_
texture gSpecularMap : SpecularMap;
sampler SpecularMapSampler = sampler_state
{
   Texture = <gSpecularMap>;
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};
#endif

//--------------------------------------------------------------//
// Structures
//--------------------------------------------------------------//
/// 입력
struct VertexInput
{
	float4 position : POSITION0;
	float3 normal : NORMAL0;
	float2 uv0 : TEXCOORD0;
#ifdef _VERTEXCOLOR_
	float4 diff : COLOR0;
#endif
#ifdef _NORMAL_MAP_
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
#endif
#ifdef _SKINNED_
	float4 blendWeights : BLENDWEIGHT;
	float4 blendIndices : BLENDINDICES; 
#endif
};
/// 출력
struct VertexOutput
{
	float4 position : POSITION0; 
   	float4 posView	: TEXCOORD0;
	float2 uv0 : TEXCOORD1;
	float3 normal : TEXCOORD2;
#ifdef _NORMAL_MAP_	
	float3 tangent : TEXCOORD3;
	float3 binormal : TEXCOORD4;
#endif
};

/// 입력
struct PixelInput
{
	float4 posView	: TEXCOORD0;
	float2 uv0		: TEXCOORD1;
	float3 normal	: TEXCOORD2;
#ifdef _NORMAL_MAP_	
	float3 tangent : TEXCOORD3;
	float3 binormal : TEXCOORD4;
#endif	
};
/// 출력
struct PixelOutput
{
	float4 MRT0		: COLOR0;	// DIFFUSE
	float4 MRT1		: COLOR1;	// POSITION (view space)
	float4 MRT2		: COLOR2;	// NORMAL(view space)
};

//--------------------------------------------------------------//
//	Functions
//--------------------------------------------------------------//
/**	GBuffer VertexShader
*/
VertexOutput vsFillGBuffer( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	/// position, normal
	half4 localPos = 0;
	half3 localNor = 0;
	half3 inputNormal = Input.normal;	//normalize(UnpackNormal(Input.normal));	// Input.normal;

#ifdef _NORMAL_MAP_
	half3 inputTangent = Input.tangent.xyz;	//normalize(UnpackNormal(Input.tangent));
	half3 inputBinormal = Input.binormal;	//normalize(UnpackNormal(Input.binormal));
	half3 localTan = 0;
	half3 localBir = 0;
#endif

#ifdef _SKINNED_
	localPos = SkinnedPosition( Input.position, Input.blendWeights, Input.blendIndices, gJoints );
	localNor = SkinnedNormal( inputNormal, Input.blendWeights, Input.blendIndices, gJoints );
	#ifdef _NORMAL_MAP_
	localTan = SkinnedNormal( inputTangent, Input.blendWeights, Input.blendIndices, gJoints );
	localBir = SkinnedNormal( inputBinormal, Input.blendWeights, Input.blendIndices, gJoints );
	#endif
#else
	localPos = Input.position;
	localNor = inputNormal;
	#ifdef _NORMAL_MAP_
	localTan = inputTangent;
	localBir = inputBinormal;
	#endif
#endif
	half4 position = TransformPosition( localPos, gWorldViewProj );
	half4 posView = TransformPosition( localPos, gWorldView );

	// Result
	Output.position = position;
	Output.posView = posView;
	Output.uv0 = Input.uv0;	// UnpackUv(Input.uv0);
	
	Output.normal = localNor;
#ifdef _NORMAL_MAP_
	//localBir = Input.tangent.w * cross(localNor, localTan);
	//localBir = normalize(localBir);

	Output.tangent = localTan;
	Output.binormal = localBir;
#endif

	return Output;
}

/** GBuffer PixelShader
*/
PixelOutput psFillGBuffer( PixelInput Input )
{
	PixelOutput Output = (PixelOutput)0;

#ifdef _DIFFUSE_MAP_
	Output.MRT0 = tex2D( DiffuseMapSampler0, Input.uv0 );
#endif

	// position(depth)
#ifdef LDR
	// depth
	Output.MRT1 = Input.posView.z / Input.posView.w;
#else
	//Output.MRT1 = half4(Input.posView.xyz, 1.0f);
	Output.MRT1 = Input.posView;
#endif

	// view space normal
	float3 normal = Input.normal;
#ifdef _NORMAL_MAP_
	normal = psNormalFromBumpMap( NormalMapSampler, Input.uv0, Input.tangent, Input.binormal, Input.normal );
#endif

	// half3 = fast normalise. 
	// Could skip this normalise to pick up additional FPS if it doesn't impact on quality too much.  
	// It's costing me 20 fps right now
	normal = normalize( mul( normal, (float3x3)gWorldView ) );

#ifdef LDR
	// pack
	normal = normal * 0.5f + 0.5f;
	//Output.MRT2.a = normal.y;
	//Output.MRT2.g = normal.x;
	//Test
	Output.MRT2 = half4(normal, 0.0f);
#else
	Output.MRT2 = half4(normal, 0.0f);
#endif

	return Output;
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique t0
{
	pass p1
	{
		VertexShader = compile vs_2_0 vsFillGBuffer();
		PixelShader = compile ps_2_0 psFillGBuffer();
		
        // Disable writing to depth buffer
        //ZWriteEnable		= true;
        //ZEnable				= true;
        //ZFunc				= LessEqual;

		//AlphaTestEnable	= false;
		//AlphaBlendEnable	= false;
	}
}
