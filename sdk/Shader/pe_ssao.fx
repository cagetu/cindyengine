#line 1 "pe_ssao.fx"
//******************************************************************************
/**	@author		cagetu
	@brief		Screen-Space Ambient Occlusion
*/
//******************************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"

//------------------------------------------------------------------------------
// ShaderVariables
//------------------------------------------------------------------------------
// Overlay
float4 gScreenSize : DisplayResolution;
float4 gScreenProj[2] : ScreenProj;
float4 gViewInfo : ViewInfo;	// cameraAspect, tanFOV, cameraNear, cameraFar
float3 gFrustumCorners[4] : FrustumCorners;
float3 gCameraPosition : WorldEyePosition;

float4x4 gProjectionTM : Projection;
float4x4 gPrevViewProjTM : PrevViewProj;

float4 gSourceSize : ColorBufferSize;

// x = default occlusion value
// y = kernel sphere radius 
// z = multiple of kernel radius to use for range truncation
// w = delta scale multiplier
float4 gSSAOParams = float4( 0.5f, 0.5f, 3.0f, 200.0f );	

/// SSAO Blur....
#define _NUM_BLURSAMPLES	9

float2 gSSAOBlurOffsets[_NUM_BLURSAMPLES] : SSAOBlurOffsets;
float gSSAOBlurWeights[_NUM_BLURSAMPLES] : SSAOBlurWeights;

//------------------------------------------------------------------------------
// Structures
//------------------------------------------------------------------------------
/// 입력
struct VertexInput
{
	float3 position : POSITION0;
	float2 texcoord : TEXCOORD0;
};
/// 출력
struct VertexOutput
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float3 viewray : TEXCOORD1;
};

//------------------------------------------------------------------------------
// Samplers
//------------------------------------------------------------------------------
texture GBufferTexture : GBuffer;
sampler GBufferSampler = 
sampler_state
{
    Texture = <GBufferTexture>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
};

texture gRotationTexture : RandomMap;
sampler RotationSampler = 
sampler_state
{
    Texture = <gRotationTexture>;
	AddressU = Wrap;
	AddressV = Wrap;
	AddressW = Wrap;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = Point;
};

// texture
texture SceneBuffer : DiffuseMap0;
sampler SceneBufferSampler = 
sampler_state
{
    Texture = <SceneBuffer>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

// texture
texture AOBuffer : OcclusionMap;
sampler AOBufferSampler = 
sampler_state
{
    Texture = <AOBuffer>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

texture HistoryAOMap : HistoryAOBuffer;
sampler HistoryAOMapSampler = 
sampler_state
{
    Texture = <HistoryAOMap>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

// texture
texture DownsampleBuffer : DiffuseMap2;
sampler DownscaleGBufferSampler = 
sampler_state
{
    Texture = <DownsampleBuffer>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    //MipMapLodBias = <MipLodBias>;
};

texture DownscaleSceneBuffer : DiffuseMap3;
sampler DownscaleSceneSampler = 
sampler_state
{
    Texture = <DownscaleSceneBuffer>;
    AddressU = Clamp;
    AddressV = Clamp;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = Point;
    //MipMapLodBias = <MipLodBias>;
};

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------
// Compress
#define _PACKEDNORMAL	1
#define _PACKEDDEPTH	1

float GetLinearDepth(in sampler depthmap, in float2 texcoord)
{
	float4 depthbuffer = tex2D(depthmap, texcoord);
 #if _PACKEDDEPTH
	float normalizedDepth = DecodeDepth(depthbuffer.zw);
 #else
	float normalizedDepth = depthbuffer.z;
 #endif
	return normalizedDepth;
}

float GetSceneDepth(in sampler depthmap, in float2 texcoord, float cameradist)
{
	const float linearDepth = GetLinearDepth(depthmap, texcoord);
	return DenormalizeDepth(linearDepth, cameradist);
}

void GetNormalDepth(float4 NormDepthBuffer, float cameradist, out float3 normal, out float2 depth)
{
	// get normal/depth
#if _PACKEDNORMAL
 #if _PACKEDDEPTH
	const float normalizedDepth = DecodeDepth(NormDepthBuffer.zw);
 #else
	const float normalizedDepth = NormDepthBuffer.z;
 #endif
	normal = normalize(DecodeNormal(NormDepthBuffer.xy));
#else
	const float normalizedDepth = NormDepthBuffer.w;
	normal = (NormDepthBuffer.xyz * 2.0) - 1.0;
#endif

	depth.x = normalizedDepth;
	depth.y = DenormalizeDepth(normalizedDepth, cameradist);
}

// Reconstructing Position From Depth
float3 ViewPosFromDepth(float3 frustumRay, float linearDepth)
{
	return frustumRay * linearDepth;
}
float3 WorldPosFromDepth(float3 cameraPos, float3 frumstumRay, float linearDepth)
{
	return cameraPos + frumstumRay * linearDepth;
}

/// ScreenPos
// [-1, 1] -> [0, 1]
float2 GetScreenPosition(in float4 HPos)
{
	float2 screenpos = HPos.xy/HPos.w * 0.5 + 0.5;
	screenpos.y = 1.0f - screenpos.y;
	return screenpos;
}


// Calculates the gaussian blur weight for a given distance and sigmas
float CalcGaussianWeight(int sampleDist, float sigma)
{
	float g = 1.0f / sqrt(2.0f * 3.14159 * sigma * sigma);  
	return (g * exp(-(sampleDist * sampleDist) / (2 * sigma * sigma)));
}

//------------------------------------------------------------------------------
// shade main
//------------------------------------------------------------------------------
VertexOutput vs_main( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	Output.position = float4(Input.position.xy, 0, 1) * gScreenProj[0] + gScreenProj[1];
	Output.texcoord = Input.texcoord;

	Output.viewray = gFrustumCorners[Input.position.z].xyz;
	return Output;
}

static float2 NoiseTextureSize = float2(64.0, 64.0);
static float3 OcclusionSamplesOffsets[8] = 
{
	float3(0.068099, -0.029749, 0.345655),
	float3(-0.333219, -0.031481, -0.371448),
	float3(0.484993, -0.106742, -0.358312),
	float3(0.140918, 0.672336, -0.167649),
	float3(0.005538, -0.785597, -0.088357),
	float3(-0.633421, 0.527250, 0.266055),
	float3(-0.744960, -0.458875, 0.330861),
	float3(0.870996, 0.392627, 0.295312),
};

float OcclusionFunction(float depthdelta)
{
	/*	@ddongry : 여기를 구현하시면 될 듯... (
	*/
	float2 SSAOThreshold = float2(0.000001, 0.7);
	
	float output = step( depthdelta, SSAOThreshold.x ) * ( 1 - smoothstep( SSAOThreshold.x, SSAOThreshold.y, depthdelta ) ); 
//	output = (smoothstep( SSAOThreshold.x, SSAOThreshold.y, depthdelta ) ); 
	return output;

}

float4 ps_generate_ao( VertexOutput Input ) : COLOR0
{
	float4 Output = float4(1,1,1,1);
	//return tex2D(SceneBufferSampler, Input.texcoord);
	

	/// get normal, depth	
	float3 SceneNormal;	
	float2 SceneDepth;
	const float4 NormDepthBuffer = tex2D(GBufferSampler, Input.texcoord);
	GetNormalDepth(NormDepthBuffer, (gViewInfo.w-gViewInfo.z), SceneNormal, SceneDepth);
	
	if (SceneDepth.x == 1.0)
	{
		Output = float4(1.0f, SceneDepth.x, NormDepthBuffer.xy);
		return Output;
	}

	// View 공간 포지션... (Z가 0~1사이의 값이다...)
	const float3 ViewSpacePosition = ViewPosFromDepth(Input.viewray.xyz, SceneDepth.x);
	//return ViewSpacePosition.z;

#if 0
	// Viewspace Position 테스트 코드 : viewpos를 화면 좌표를 변환해서 텍스쳐를 출력해봄..
	float4 testHPos = mul(float4(ViewSpacePosition, 1.0f), gProjectionTM);
	float2 testCoord = GetScreenPosition(testHPos);
	return tex2D(GBufferSampler, testCoord);
#endif

	// random vector
	float2 RandomUVScale = (gScreenSize.xy/NoiseTextureSize);
	float3 randomVector = normalize(tex2D(RotationSampler, (Input.texcoord.xy*RandomUVScale)).xyz * 2.0 - 1.0);

#define NUM_SAMPLES 8

	float KernelRadius = 0.3f;

	float TotalOcclusion = 0;
	float weight = 0;
	
#if 0
	float3 Ray = reflect(OcclusionSamplesOffsets[0], randomVector) * KernelRadius;
	Ray *= sign(dot(Ray, SceneNormal));

	float3 NeighborViewPos = ViewSpacePosition + Ray;
	float4 NeighborHPos = mul(float4(NeighborViewPos, 1), gProjectionTM);
	float2 NeighborScreenPos = GetScreenPosition(NeighborHPos);
	//float SampleDepth = GetSceneDepth(GBufferSampler, NeighborScreenPos.xy, (gViewInfo.w-gViewInfo.z));
	float samplelineardepth = GetLinearDepth(GBufferSampler, NeighborScreenPos.xy);
	float scenedepth = (gViewInfo.w-gViewInfo.z) * samplelineardepth;
	return NeighborViewPos.z / (gViewInfo.w-gViewInfo.z); discard;
#endif

#define _FLIPNORMAL	1

	for(int i=0; i<NUM_SAMPLES; i++)
	{
		float3 Ray = reflect(OcclusionSamplesOffsets[i], randomVector) * KernelRadius;
#if _FLIPNORMAL
		Ray *= sign(dot(Ray, SceneNormal));
#endif

		float3 NeighborViewPos = ViewSpacePosition + Ray;
		float4 NeighborHPos = mul(float4(NeighborViewPos, 1), gProjectionTM);
		float2 NeighborScreenPos = GetScreenPosition(NeighborHPos);
		//float SampleDepth = GetSceneDepth(GBufferSampler, NeighborScreenPos.xy, (gViewInfo.w-gViewInfo.z));
		float samplelineardepth = GetLinearDepth(GBufferSampler, NeighborScreenPos.xy);
		float SampleDepth = (gViewInfo.w-gViewInfo.z) * samplelineardepth;

		float DepthDiff = NeighborViewPos.z - samplelineardepth;

		/* @cagetu : Occlusion Function을 구해주세요...
		*/
		float OcclusionValue = OcclusionFunction(DepthDiff);

		TotalOcclusion += OcclusionValue;
	}

	TotalOcclusion /= NUM_SAMPLES;
	//TotalOcclusion = 1 - TotalOcclusion/NUM_SAMPLES;

	/*	@cagetu : TotalOcclusion의 처리를 어떻게???
		occlusion이 되는 부분이 아마도 값이 있는 부분일 듯...
		occlusion 부분을 검정색 부분으로 하느냐 흰색 부분으로 하느냐? 는 추후에 매우 중요한 결정...
	*/

	/// | TotalOcclusion | linearDepth | normalx | normaly |
	Output = float4(TotalOcclusion, SceneDepth.x, NormDepthBuffer.xy);
	return Output;
}

#define _ENABLE_NORALWEIGHT	0

float4 ps_blur_ao( VertexOutput Input, uniform bool vertical ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	float2 gaussianDir = (vertical==true) ? float2(0,1) : float2(1, 0);

#if 1
	/*	Bilateral Filtering
		- normal, depth를 얻어오기 위해서, loop 안에서 gbuffer를 샘플링하는 것이 너무 비싸다.
		- normalweight를 처리하기 위해서, (pow(dot(normal, sampleNorm), 32)) 비용이 너무 비싸다.
		- 대략 결론 
			: ssao와 같이 정교하지 않아도 된다면, depthweight 만 사용한다.
			: gbuffer의 샘플링을 줄이기 위해서, generateAO Buffer의 한 채널에 depth값을 기록해서 사용하도록 하자!!!		
	*/

	// AOBuffer는 [ AO | linearDepth | normal x | normal y ] 로 구성
	const float4 ssaobuffer = tex2D(AOBufferSampler, Input.texcoord);
	const float depth = ssaobuffer.y;
	const float3 normal = DecodeNormal(ssaobuffer.zw);

	float TotalOcclusion = 0.0f;
	float TotalWeight = 0.0f;

	for(int i=0; i<_NUM_BLURSAMPLES; i++)
	{
		float2 gaussianOffsets = (i-4) / gScreenSize * gaussianDir;

		float2 samplecoord = Input.texcoord + gaussianOffsets;

		/// get normal, depth
		const float4 sampleAOBuffer = tex2D(AOBufferSampler, samplecoord);
		const float sampleDepth = ssaobuffer.y;
		const float3 sampleNorm = DecodeNormal(ssaobuffer.zw);
		const float sampleOcclusion = sampleAOBuffer.x;

		/*	Bilateral Filtering
			for
				sampleWeight = filterWeight * depthWeight * normalWeight;
				totalColor += sampleColor * sampleWeight;
				totalWeight += sampleWeight;
		*/
		float deltadepth = sampleDepth - depth;
		float depthWeight = 1.0f / (abs(deltadepth) + 1.0f);
#if _ENABLE_NORALWEIGHT
		float normalWeight = (pow(dot(normal, sampleNorm), 32));
#else
		float normalWeight = 1.0f;
#endif
		float gaussianweight = CalcGaussianWeight((i-4), 1.0f);

		float weight = gaussianweight * depthWeight * normalWeight;

		TotalOcclusion += sampleOcclusion * weight;
		TotalWeight += weight;
	}
	float FinalOcclusion = TotalOcclusion/TotalWeight;
	Output = float4(saturate(FinalOcclusion), ssaobuffer.y, ssaobuffer.zw);  
#endif
#if 0
	// Gaussian Blur 테스트
	float4 totalColor = 0;
	for(int i=0; i<_NUM_BLURSAMPLES; i++)
	{
		float2 gaussianOffsets = (i-4) / gScreenSize * gaussianDir;
		float gaussianweight = CalcGaussianWeight((i-4), 4.0f);

		float4 sampleColor = tex2D(SceneBufferSampler, Input.texcoord+gaussianOffsets);
		totalColor += sampleColor * gaussianweight;
	}
	return totalColor;
#endif
#if 0
	// Bilateral Filtering 테스트

	/// get normal, depth
	float3 normal;
	float2 depth;
	const float4 NormDepthBuffer = tex2D(GBufferSampler, Input.texcoord);
	GetNormalDepth(NormDepthBuffer, (gViewInfo.w-gViewInfo.z), normal, depth);

	float4 TotalColor = 0.0f;
	float TotalWeight = 0.0f;

	/*	Bilateral Filtering 테스트
		- normal, depth를 얻어오기 위해서, loop 안에서 gbuffer를 샘플링하는 것이 너무 비싸다.
		- normalweight를 처리하기 위해서, (pow(dot(normal, sampleNorm), 32)) 비용이 너무 비싸다.
		- 대략 결론 
			: ssao와 같이 정교하지 않아도 된다면, depthweight 만 사용한다.
			: gbuffer의 샘플링을 줄이기 위해서, generateAO Buffer의 한 채널에 depth값을 기록해서 사용하도록 하자!!!		
	*/
	for(int i=0; i<_NUM_BLURSAMPLES; i++)
	{
		float2 gaussianOffsets = (i-4) / gScreenSize * gaussianDir;

		float2 samplecoord = Input.texcoord + gaussianOffsets;

		/// get normal, depth
		float3 sampleNorm;
		float2 sampleDepth;
		const float4 SampleNormalDepthBuffer = tex2D(GBufferSampler, samplecoord);
		GetNormalDepth(SampleNormalDepthBuffer, (gViewInfo.w-gViewInfo.z), sampleNorm, sampleDepth);

		/*	Bilateral Filtering
			for
				sampleWeight = filterWeight * depthWeight * normalWeight;
				totalColor += sampleColor * sampleWeight;
				totalWeight += sampleWeight;
		*/
		float deltadepth = sampleDepth.x - depth.x;
		float depthWeight = 1.0f / (abs(deltadepth) + 1.0f);
		float normalWeight = 1.0f; //(pow(dot(normal, sampleNorm), 32));
		float gaussianweight = CalcGaussianWeight((i-4), 1.0f);

		float weight = gaussianweight * depthWeight * normalWeight;

		//float occlusion = tex2D(AOBufferSampler, samplecoord).x;
		//TotalOcclusion += occlusion * weight;
		float4 sampleColor = tex2D(SceneBufferSampler, samplecoord);
		TotalColor += sampleColor * weight;
		TotalWeight += weight;
	}
	float4 FinalColor = TotalColor / TotalWeight;
	return FinalColor;
#endif
	return Output;
}

float4 ps_downscale_scene( VertexOutput Input ) : COLOR0
{
	const float2 texelSize = gSourceSize.zw;

	float4 c0 = tex2D(SceneBufferSampler, Input.texcoord + texelSize.xy * float2(-0.5, -0.5));
    float4 c1 = tex2D(SceneBufferSampler, Input.texcoord + texelSize.xy * float2(+0.5, -0.5));
    float4 c2 = tex2D(SceneBufferSampler, Input.texcoord + texelSize.xy * float2(-0.5, +0.5));
    float4 c3 = tex2D(SceneBufferSampler, Input.texcoord + texelSize.xy * float2(+0.5, +0.5));

	return (c0 + c1 + c2 + c3) * 0.25;
}

//#define _DOWNSCALE_DEPTHONLY

float4 ps_downscale_gbuffer( VertexOutput Input ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	const float2 texelSize = gSourceSize.zw;

	const float2 offsets[4] = {float2(-0.5, -0.5), float2(+0.5, -0.5), float2(-0.5, +0.5), float2(+0.5, +0.5)};

#ifdef _DOWNSCALE_DEPTHONLY
	float depth[4];
	for(int i=0; i<4; i++)
		depth[i] = DecodeDepth(tex2D(GBufferSampler, Input.texcoord + texelSize.xy * offsets[i]).zw);

	float maxZ = max(max(depth[0].x, depth[1].x), max(depth[2].x, depth[3].x));
	float minZ = min(min(depth[0].x, depth[1].x), min(depth[2].x, depth[3].x)); 

	return float4(minZ, maxZ, 0.0f, 0.0f);
#elif defined(_DOWNSCALE_DEPTH_HINORMAL)
	float depth[4];
	for(int i=0; i<4; i++)
		depth[i] = DecodeDepth(tex2D(GBufferSampler, Input.texcoord + texelSize.xy * offsets[i]).zw);

	float maxZ = max(max(depth[0].x, depth[1].x), max(depth[2].x, depth[3].x));
	float minZ = min(min(depth[0].x, depth[1].x), min(depth[2].x, depth[3].x)); 

	return float4(minZ, maxZ, 
#else
	int i = 0;

	float2 depth[4];
	float3 norm[4];
	float4 gbuffer;
	for(i=0; i<4; i++)
	{
		//gbuffer = tex2D(GBufferSampler, Input.texcoord + texelSize.xy * offsets[i]);
		gbuffer = tex2D(GBufferSampler, Input.texcoord + texelSize.xy * offsets[i]);
		GetNormalDepth(gbuffer, (gViewInfo.w-gViewInfo.z), norm[i], depth[i]);
	}

	float maxZ = max(max(depth[0].x, depth[1].x), max(depth[2].x, depth[3].x));
	float minZ = min(min(depth[0].x, depth[1].x), min(depth[2].x, depth[3].x)); 
	
	/*	
		- 주변 4개의 Pixel을 얻어온다.
		- 주변 4개의 픽셀을 z 순서로 정렬한다.
		- 정렬된 4개의 픽셀 중 가운데 2개의 픽셀을 찾는다.
		- 가장 큰 Pixel과 가장 작은 픽셀의 깊이 차이가 Epsilon보다 크다면, 중간 2픽셀의 평균으로 positon, normal로 사용.
		  가장 큰 Pixel과 가장 작은 픽셀의 깊이 차이가 Epsilon보다 작다면, 가장 작은, 큰 픽셀의 값을 postion, normal로 사용.
	*/

	//@< 정렬... 최대 pixel, 최소 pixel, 중간 두 픽셀값을 분류한다.
	int minp = 0, maxp = 0;
	int med0 = 0, med1 = 0;

	for(i=0; i<4; i++)
	{
		if (depth[i].x == minZ)	minp = i;
		if (depth[i].x == maxZ)	maxp = i;
		if (depth[i].x != minZ && depth[i].x != maxZ) med0 = i;
	}
	int idx = 0;
	for(i=0; i<4; i++)
	{
		if (i!=minp && i!=maxp && i!=med0)
		{
			med1 = i;
			break;
		}
	}
	//@>

	float downscaledepth;
	float3 downscalenormal;

	const float _EPSILON = 1.0f;
	float d = depth[minp].x - depth[maxp].x;
	//float d = distance(pos[minPos].xyz, pos[maxPos].xyz); 

	if (d < _EPSILON)
	{
		downscaledepth = (depth[med0].x + depth[med1].x) / 2.0;
		downscalenormal = (norm[med0] + norm[med1]) / 2.0;
	}
	else
	{
		downscaledepth = depth[med0].x;
		downscalenormal = norm[med0];
	}

	return float4(EncodeNormal(downscalenormal), EncodeDepth(downscaledepth));
#endif
}

#define _UPSAMPLING			1
#define _UPSAMPLINGSCENE	1

float4 ps_composite( VertexOutput Input ) : COLOR0
{
	float4 Output = float4(1,1,1,1);

	Output = tex2D(SceneBufferSampler, Input.texcoord);
	Output = tex2D(AOBufferSampler, Input.texcoord).x;
	Output = tex2D(DownscaleGBufferSampler, Input.texcoord);
	Output = tex2D(DownscaleSceneSampler, Input.texcoord);

#if _UPSAMPLING
	float3 pixelnormal;
	float2 pixeldepth;
	float4 gbuffer = tex2D(GBufferSampler, Input.texcoord);
	GetNormalDepth(gbuffer, (gViewInfo.w-gViewInfo.z), pixelnormal, pixeldepth);

	/*	저해상도 uv 구하기
		- 고해상도 uv를 구해서, 1/2만큼 축소한다.
	*/
	float2 lowresuv[4];
	float2 hirestexelsize = float2(0.5f, 0.5f) / gScreenSize.xy;
	lowresuv[0] = Input.texcoord.xy;
	lowresuv[1] = (Input.texcoord.xy + float2(hirestexelsize.x, 0));
	lowresuv[2] = (Input.texcoord.xy + float2(0, hirestexelsize.y));
	lowresuv[3] = (Input.texcoord.xy + hirestexelsize.xy);

	float3 normal[4];
	float depth[4];
	float ambientocclusion[4];
	
	int i=0;

#if _UPSAMPLINGSCENE
	for(i=0; i<4; i++)
	{
		float4 downscale_gbuffer = tex2D(DownscaleGBufferSampler, lowresuv[i]);
		depth[i] = DecodeDepth(downscale_gbuffer.zw);
		normal[i] = DecodeNormal(downscale_gbuffer.xy);
	}
#else
	for(i=0; i<4; i++)
	{
		const float4 aobuffer = tex2D(AOBufferSampler, lowresuv[i]);

		ambientocclusion[i] = aobuffer.x;
		depth[i] = aobuffer.y;
		norm[i] = DecodeNormal(aobuffer.zw);
	}
#endif

	float depthWeights[4];
	float normalWeights[4];
	float bilinearWeights[4];

	const float _EPSILON = 0.1f;

	// depth weights
	[unroll]
	for(i=0; i<4; i++)
		depthWeights[i] = 1.0f / (abs(depth[i] - pixeldepth.x) + _EPSILON);

	// normal weights
	[unroll]
	for(i=0; i<4; i++)
		normalWeights[i] = pow(dot(normal[i], pixelnormal), 32);

	/*	Bilinear Weight 구하기
		- texelsize를 구해서, 2로 나눈 나머지를 이용해서, 2x2에서의 텍셀 index를 구한다.
	*/
	float2 texelsize = gScreenSize.xy * Input.texcoord.xy;
	float2 pattern = fmod(floor(texelsize), 2);
	int texelidx = pattern.x + pattern.y;
	
	static const float4 vBilinearWeights[4] = 
	{              
			//	  0    1     2     3
		float4( 9/16.0f, 3/16.0f, 3/16.0f, 1/16.0f ), // 0
		float4( 3/16.0f, 9/16.0f, 1/16.0f, 3/16.0f ), // 1
		float4( 3/16.0f, 1/16.0f, 9/16.0f, 3/16.0f ), // 2
		float4( 1/16.0f, 3/16.0f, 3/16.0f, 9/16.0f )  // 3
	};

	float4 totalcolor = 0;
	float totalweight = 0;
	float weight;

	for(i=0; i<4; i++)
	{
		weight = depthWeights[i] * normalWeights[i] * vBilinearWeights[texelidx][i];
#if _UPSAMPLINGSCENE
		totalcolor += tex2D(DownscaleSceneSampler, lowresuv[i]) * weight;
#else
		totalcolor += ambientocclusion[i] * weight;
#endif
		totalweight += weight;
	}
	Output = totalcolor/totalweight;
#endif
	return Output;
}

/// Temporal Update 하는 셰이더..
float4 ps_temporal(VertexOutput Input) : COLOR0
{
	float4 Output = (float4)1;
	
	const float4 CurrentAOBuffer = tex2D(AOBufferSampler, Input.texcoord);
	const float CurrentOcclusion = CurrentAOBuffer.x;
	const float CurrentSceneDepth = CurrentAOBuffer.y;

	float3 worldPos = WorldPosFromDepth(gCameraPosition, Input.viewray.xyz, CurrentSceneDepth);
	float4 prevHPos = mul(float4(worldPos, 1), gPrevViewProjTM);
	float2 prevScreenCoord = GetScreenPosition(prevHPos);
	
	// 캐쉬버퍼에서 cache 정보 얻어오기...
	const float4 PrevAOBuffer = tex2D(HistoryAOMapSampler, prevScreenCoord);
	const float PrevOcclusion = min(PrevAOBuffer.x, 1.0f);
	const float PrevLinearDepth = PrevAOBuffer.y;
	const float PrevConvergence = PrevAOBuffer.z;

	Output = CurrentAOBuffer;
	return Output;
}

float4 ps_copyhistory(VertexOutput Input) : COLOR0
{
	return tex2D(AOBufferSampler, Input.texcoord);
}

//------------------------------------------------------------------------------
//	Techniques
//------------------------------------------------------------------------------
technique GenerateAO
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_generate_ao();
	}
}

technique DownscaleGBuffer
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_downscale_gbuffer();
	}
}


technique DownscaleScene
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_downscale_scene();
	}
}

technique BlurHoriAO
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_blur_ao(false);
	}
}

technique BlurVertAO
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_blur_ao(true);
	}
}

technique TemporalFilter
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_temporal();
	}
}

technique HistoryAO
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_copyhistory();
	}
}

technique CompositeAO
{
	pass p0
	{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader = compile ps_3_0 ps_composite();
	}
}