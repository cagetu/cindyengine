#line 1 "DeferredLighting.fx"
//*************************************************************
//	DeferredLighting.fx : Deferred Lighting
//	Copyright ?2009 cagetu, Inc. All Rights Reserved.
//*************************************************************
#include "..\\..\\sdk\\Shader\\Util.fxh"
#include "..\\..\\sdk\\Shader\\Util_Lighting.fxh"

//#define LDR;

//--------------------------------------------------------------//
// Constants
//--------------------------------------------------------------//
const float2 gScreenSize : DisplayResolution;
const float2 gUVAdjust : ScreenUVAdjust;
const float4 gViewInfo : ViewInfo;

float3 gLightDirection : LightDirection;
float3 gLightPosition : LightPosition;
float3 gLightAttenuation : LightAttenuation;
float4 gLightDiffuse : LightDiffuse;
float  gLightRange : LightRange;

float4x4 gWorldViewProj : ModelViewProjection;

//--------------------------------------------------------------//
// Texture & Sampler
//--------------------------------------------------------------//
texture gPosMap : DiffuseMap0;
sampler PosMapSampler = sampler_state
{
   Texture = <gPosMap>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MINFILTER = POINT;
   MAGFILTER = POINT;
   MIPFILTER = NONE;
};

texture gNormalMap : DiffuseMap1;
sampler NormalMapSampler = sampler_state
{
   Texture = <gNormalMap>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MINFILTER = POINT;
   MAGFILTER = POINT;
   MIPFILTER = NONE;
};

texture gDiffuseMap : DiffuseMap2;
sampler DiffuseMapSampler = sampler_state
{
   Texture = <gDiffuseMap>;
   ADDRESSU = CLAMP;
   ADDRESSV = CLAMP;
   MINFILTER = POINT;
   MAGFILTER = POINT;
   MIPFILTER = NONE;
};

//--------------------------------------------------------------//
// Structures
//--------------------------------------------------------------//
/// 입력
struct VertexInput
{
    float4 position : POSITION0;
    float2 texCoord	: TEXCOORD0;
};
/// 출력
struct VertexOutput
{
    float4 position			: POSITION0;
    float2 texCoord			: TEXCOORD0;
#ifdef LDR
    float3 eyeToScreenRay	: TEXCOORD1;
#endif // LDR
};

// Diffuse Lighting Pass (Point Light [omni])
struct PixelInput
{
	float2 vPos : VPOS;
#ifdef LDR
    float3 eyeToScreenRay	: TEXCOORD1;
#endif // LDR	
};

//--------------------------------------------------------------//
// Functions
//--------------------------------------------------------------//
/// VertexShader
/**
*/
VertexOutput vsMain( VertexInput Input )
{
	VertexOutput Output = (VertexOutput)0;

	float4 screenPos = mul(Input.position, gWorldViewProj);
	
#ifdef LDR 
	float viewAspect = gViewInfo.x;
	float tanFOV = gViewInfo.y;
	float3 eyeToScreenRay = float3(screenPos.x*viewAspect, screenPos.y, tanFOV);
	Output.eyeToScreenRay = eyeToScreenRay;
#endif // LDR

	Output.position = screenPos;
	Output.texCoord = Input.texCoord;
	return Output;
}

/**	Stencil Convex Light Pass
*/
VertexOutput vsShape( in float4 Position : POSITION0 )
{
	VertexOutput Output = (VertexOutput)0;

	float4 screenPos = mul(Position, gWorldViewProj); 

#ifdef LDR
	float viewAspect = gViewInfo.x;
	float tanFOV = gViewInfo.y;
	float3 eyeToScreenRay = float3(screenPos.x*viewAspect, screenPos.y, tanFOV);
	Output.eyeToScreenRay = eyeToScreenRay;
#endif // LDR

	Output.position = screenPos;
	return Output;
}

/// PixelShader 함수
/** AmbientLight를 계산
*/
float4 psAmbientLight( const VertexOutput Input ) :COLOR0
{
	float4 Output = (float4)0;
	
	half4 diffuse = tex2D(DiffuseMapSampler, Input.texCoord);
	Output = diffuse * float4(0.2f, 0.2f, 0.2f, 1.0f);
	
	return Output;
};

/** DirectionalLight를 계산
*/
float4 psDirectionalLight( const VertexOutput Input ) : COLOR0
{
	float4 Output = (float4)0;

	half4 diffuse = tex2D(DiffuseMapSampler, Input.texCoord);

#ifdef LDR
	half depth = tex2D(PosMapSampler, Input.texCoord).x;
	float3 viewSpacePos = depth * normalize(Input.eyeToScreenRay);
#else
	float3 viewSpacePos = tex2D(PosMapSampler, Input.texCoord).xyz;
#endif

#ifdef LDR
	half3 viewSpaceNormal = tex2D(NormalMapSampler, Input.texCoord).xyz * 2.0 - 1.0;
#else
	half3 viewSpaceNormal = tex2D(NormalMapSampler, Input.texCoord).xyz;
#endif

	half NdotL = max(0.0f, dot(viewSpaceNormal, gLightDirection) );
	if (NdotL > 0.0f)
	{
		/// Diffuse
		half4 diffuseColor = NdotL * diffuse;

		/// Specular
		half3 pixelToViewer = normalize( -viewSpacePos );
		half3 reflection = normalize( 2.0f * NdotL * viewSpaceNormal - gLightDirection );
		half RdotV= max( 0.0f, dot(reflection, pixelToViewer) );
		
		half specularPower = 2.0f;
		half4 specularColor = 0.0f; //diffuse.w * pow( RdotV, specularPower );

		/// 결과
		Output = saturate( gLightDiffuse * (diffuseColor+specularColor) );
	}

	return Output;
};

/** PointLight 계산
*/
float4 psPointLight( const PixelInput Input ) : COLOR
{
	half4 Output = half4(0.0f, 0.0f, 0.0f, 1.0f);

	// Co-ords for texture lookups = pixel rasterisation pos / screen dimensions (e.g. 512/1024 = 0.5f)
	// But due to D3D's weird sampling rules, we have to correct the texture co-ordinate by offsetting it by a predefined amount
	float2 coords = Input.vPos.xy / gScreenSize.xy;		
	coords += gUVAdjust;

	half4 diffuse = tex2D(DiffuseMapSampler, coords);
#ifdef LDR
	half depth = tex2D(PosMapSampler, coords).x;
	float3 viewSpacePos = depth * normalize(Input.eyeToScreenRay);
#else
	float3 viewSpacePos = tex2D(PosMapSampler, coords).xyz;
#endif

#ifdef LDR
	half3 viewSpaceNormal = tex2D(NormalMapSampler, coords).xyz * 2.0 - 1.0;
#else
	half3 viewSpaceNormal = tex2D(NormalMapSampler, coords).xyz;
#endif

	float3 pixelToLight = gLightPosition - viewSpacePos;
	float relDist = length(pixelToLight) * gLightRange;
	if (relDist < 1.0f)
	{
		float attenuation = (1.0 - relDist);
	
		half3 pixelToLightDir = normalize(pixelToLight);

		//half NdotL = HalfLambert(viewSpaceNormal, pixelToLightDir, 2);
		half NdotL = dot(viewSpaceNormal, pixelToLightDir);
		half diffuseIntensity = NdotL * attenuation;
		if (diffuseIntensity > 0.0f)
		{
			half4 lightDiffuse = gLightDiffuse * saturate(diffuseIntensity);

			Output = diffuse * lightDiffuse;
		}
	}

	return Output;
}

//--------------------------------------------------------------//
//	Techniques
//--------------------------------------------------------------//
technique AmbientLighting
{
	pass p0
	{
		VertexShader = compile vs_2_0 vsMain();
		PixelShader = compile ps_2_0 psAmbientLight();
		
		CullMode = CCW;

		ZEnable	= false;
		ZWriteEnable = false;
	}
}

technique DirectionalLighting
{
	pass p0
	{
   		VertexShader = compile vs_2_0 vsMain();
		PixelShader = compile ps_2_0 psDirectionalLight();
			
		CullMode = CCW;

		ZEnable	= false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = One;
        DestBlend = One;
	}
}

// PointStencil은 stencil volume을 처리하고 불필요한 shading을 피하는 pass 1/2이다.
// 
// shadow volume 알고리즘에서 발견한 것처럼 two-side stencil 구현을 기초했다.
// 하나의 pass에서 stencil buffer 안에 light volume을 랜더링한다.
// * Initial stencil buffer value = 1
// * Front facing polys drawn first.  If the depth test fails, increment the stencil value
// * Back facing polys drawn second.  If the depth test fails, decrement the stencil value
//
// So we have several cases:

technique PointLighting
{
	// Stencil
	pass p0
	{
		VertexShader = compile vs_2_0 vsShape();
		PixelShader = null;
        
        CullMode			= none;
        ColorWriteEnable	= 0x0;        
        
        // Disable writing to the frame buffer
        AlphaBlendEnable	= true;
        SrcBlend			= Zero;
        DestBlend			= One;
        
        // Disable writing to depth buffer
        ZWriteEnable		= false;
        ZEnable				= true;
        ZFunc				= Less;

        // Setup stencil states
        StencilEnable		= true;
        TwoSidedStencilMode = true;
        
        StencilRef			= 1;
        StencilMask			= 0xFFFFFFFF;
        StencilWriteMask	= 0xFFFFFFFF;
        
        // stencil settings for front facing triangles
        StencilFunc			= Always;
        StencilZFail		= Incr;
        StencilPass			= Keep;

        // stencil settings for back facing triangles
        Ccw_StencilFunc		= Always;
        Ccw_StencilZFail	= Decr;
        Ccw_StencilPass		= Keep;
	}
	// Calculate PointLight
	pass p1
	{
		VertexShader = compile vs_3_0 vsShape();
		PixelShader	= compile ps_3_0 psPointLight();

		ZEnable			= false;
		ZWriteEnable	= false;
							
		AlphaBlendEnable = true;
		SrcBlend		= One;
        DestBlend		= One;

        // draw backfaces so that we're always guaranteed to get the right behaviour when inside the light volume
        CullMode		= CW;	        
        
        ColorWriteEnable = 0xFFFFFFFF;

  		StencilEnable	= true;
  		TwoSidedStencilMode = false;
        StencilFunc		= Equal;

		StencilFail		= Keep;
		StencilZFail	= Keep;
		StencilPass		= Keep;

		StencilRef		= 0;
		StencilMask		= 0xFFFFFFFF;
        StencilWriteMask = 0xFFFFFFFF;
	}
}