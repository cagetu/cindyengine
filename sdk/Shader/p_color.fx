//------------------------------------------------------------------------------
//  p_color.fx : Pass-shader for the color pass.
//	[����] Nebula3 - Radon Labs GmbH
//	Copyright ?2008 cagetu, Inc. All Rights Reserved.
//------------------------------------------------------------------------------
technique t0
{
    pass p0
    {
        ColorWriteEnable  = RED|GREEN|BLUE|ALPHA;
        ZEnable           = True;
        ZWriteEnable      = False;
        ZFunc             = Equal;
        StencilEnable     = False;
        FogEnable         = False;
        AlphaBlendEnable  = False;
        AlphaTestEnable   = False;
        AlphaFunc         = Equal;
        ScissorTestEnable = False;
        CullMode          = CCW;
    }
}
