#ifndef __RD_MAXEXPORTER_HEADER_H__
#define __RD_MAXEXPORTER_HEADER_H__

#pragma once

#include "Max.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include "resource.h"

#include "shaders.h"
#include "macrorec.h"
#include "gport.h"
#include "utilapi.h"
#include "simpobj.h"
#include "modstack.h"
#include "spline3d.h"
#include "splshape.h"
#include "bmmlib.h"
#include "ikctrl.h"
#include "strclass.h"
#include "interpik.h"
#include "notetrck.h"

// now, Character Studio entirely included in 3dsmax7
// for other version of 3dsmax, specify include path of character studio
// in 'project > directory' page of 'tool > option' menu.
#if (MAX_RELEASE >= 6900)   // 3dsmax R7 or R7 alpha
    #include <cs/bipexp.h>
    #include <cs/phyexp.h>
#else
    #include <bipexp.h>
    #include <phyexp.h>    // for other version
	//#include "phyexp.h"
	//#include "bipexp.h"
#endif

#include "decomp.h"
#include "stdmat.h"
#include "shape.h"
#include "interpik.h"

#include "iEditNormals.h"

#include "simpmod.h"

#include "MNMATH.H"
#include <MeshNormalSpec.h>

#define EDIT_NORMALS_CLASS_ID Class_ID(0x4aa52ae3, 0x35ca1cde)

#define MAX_LEN			256
typedef unsigned short ushort;

//! 작업 체크용 macro
//----------------------------------------------------------
// FIXMEs / TODOs / NOTE macros
//----------------------------------------------------------
#define _QUOTE(x)		# x 
#define QUOTE(x)		_QUOTE(x)
#define __FILE__LINE__	__FILE__ "(" QUOTE(__LINE__) ") : "

#define FILE_LINE	message( __FILE__LINE__ )

#define NOTE( x )	message( __FILE__LINE__" NOTE :   " #x "\n" ) 
#define TODO( x )	message( __FILE__LINE__" TODO :   " #x "\n" ) 
#define FIXME( x )  message( __FILE__LINE__" FIXME:   " #x "\n" ) 

#define NEW_EXPORT_TANGENT
//#define NEW_VERTEXNORMAL
//#define _ENABLE_SMGROUP_
#define EXPORT_VERT_FROM_FACE

#endif // __RD_MAXEXPORTER_HEADER_H__