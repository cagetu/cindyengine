#include "RdMaxMeshExpUtil.h"
#include "RdMaxMeshExp.h"

//------------------------------------------------------------------
//	RdMeshExpUtil
//------------------------------------------------------------------
void RdMeshExpUtil::CheckOverlappedVertex( Tab<RdVertex>& rVerts, 
										   Tab<RdFace>& rFaces,
										   Tab<RdVertex>& rSrcVerts )
{
	rVerts.SetCount( 0 );
		
	RdFace nwface, tmpface;
	RdVertex nwvert, tmpvert;

	ulong ulFaceCnt = rFaces.Count();
	for( int i = 0; i < ulFaceCnt; ++i )
	{
		nwface = rFaces[i];
		for( int j = 0; j < 3; ++j )
		{
			tmpface.Index[j] = nwface.Index[j];
		}
		tmpface.MatID = nwface.MatID;

		for( int k = 0; k < 3; ++k )
		{
			nwvert = rSrcVerts[nwface.Index[k]];

			ulong ulVertCnt = rVerts.Count();
			ulong a = 0;
			for( a = 0; a < ulVertCnt; ++a )
			{
				tmpvert = rVerts[a];

				if (tmpvert == nwvert)
					break;
				//if( Point3Equal( tmpvert.Pos, nwvert.Pos ) && 
				//	Point3Equal( tmpvert.Normal, nwvert.Normal ) &&
				//	FloatEqual( tmpvert.UV.U, nwvert.UV.U ) &&
				//	FloatEqual( tmpvert.UV.V, nwvert.UV.V ) && 
				//	FloatEqual( tmpvert.UV2.U, nwvert.UV2.U ) && 
				//	FloatEqual( tmpvert.UV2.V, nwvert.UV2.V ) &&
				//	Point3Equal( tmpvert.Tangent, nwvert.Tangent ) &&
				//	Point3Equal( tmpvert.Binormal, nwvert.Binormal )) 
				//	break;
			}

			if( a < ulVertCnt )
			{
				tmpface.Index[k] = a;
			}
			else
			{
				tmpface.Index[k] = ulVertCnt;
				rVerts.Insert( ulVertCnt, 1, &nwvert );
			}
		}

		rFaces[i] = tmpface;
	}
}

//------------------------------------------------------------------
void RdMeshExpUtil::CollectUV( Mesh* pMesh,
							   Tab<RdVertex>& rSrcVerts,
							   Tab<RdVertex>& rOutputVerts,
							   Tab<RdFace>& rOutputFaces,
							   bool bHasMultiUV )
{
	if( bHasMultiUV )
	{
		CollectMultiUV( pMesh, rSrcVerts, rOutputVerts, rOutputFaces );
	}
	else
	{
		CollectSingleUV( pMesh, rSrcVerts, rOutputVerts, rOutputFaces );
	}
}

//------------------------------------------------------------------
//	RdStaticMeshExpUtil
//------------------------------------------------------------------
void RdStaticMeshExpUtil::CollectSingleUV( Mesh* pMesh,
										   Tab<RdVertex>& rSrcVerts,
										   Tab<RdVertex>& rOutputVerts,
										   Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		ulong ulNumVerts = rSrcVerts.Count();
		ulong ulFaceCnt = pMesh->getNumFaces();

		assert(ulFaceCnt == rOutputFaces.Count());

		BitArray bWritten;
		bWritten.SetSize( ulNumVerts );
		bWritten.ClearAll();

		Tab<RdVertex> newVerts;
		newVerts.SetCount( ulNumVerts );

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			tmpface.MatID = face.getMatID();

			for( int j = 0; j < 3; ++j )
			{
				nVertIdx = face.v[j];
				nTVertIdx = tvface.t[j];

#if defined (_ENABLE_SMGROUP_) && !defined(NEW_VERTEXNORMAL)
				nVertIndex = RdMeshExp::GetNormalVertexIndex( nVertIdx, face.getSmGroup(), rSrcVerts );
#else
				nVertIndex = nVertIdx;
#endif

				// 버텍스 uv 정보 세팅
				tmpvert = rSrcVerts[nVertIndex];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				// face에 버텍스 index 세팅
				tmpface.Index[j] = nVertIndex;

				// 만약, 기록 되어 있는 녀석들 중에, 다른 uv를 가지는 녀석이 있으면...
				if (bWritten[nVertIndex])
				{
					if( ( false == FloatEqual( tmpvert.UV.U, newVerts[nVertIndex].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, newVerts[nVertIndex].UV.V ) ) )
					{
						tmpface.Index[j] = newVerts.Count();
						newVerts.Insert( newVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					newVerts[nVertIndex] = tmpvert;
					bWritten.Set( nVertIndex );
				}
			}
			rOutputFaces[i] = tmpface;
		}

		rOutputVerts = newVerts;

		// 중복되는 거 없애기
		RdMeshExpUtil::CheckOverlappedVertex( rOutputVerts, rOutputFaces, newVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdStaticMeshExpUtil::CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
										  Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		ulong ulNormalVerts = rSrcVerts.Count();
		ulong ulFaceCnt = pMesh->getNumFaces();

		BitArray bWritten;
		bWritten.SetSize( ulNormalVerts );
		bWritten.ClearAll();

		Tab<RdVertex> tmpVerts;
		tmpVerts.SetCount( ulNormalVerts );

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			tmpface.MatID = face.getMatID();

			for( int j = 0; j < 3; ++j )
			{
				nVertIdx = face.v[j];
				nTVertIdx = tvface.t[j];

#if defined (_ENABLE_SMGROUP_) && !defined(NEW_VERTEXNORMAL)
				nVertIndex = RdMeshExp::GetNormalVertexIndex( nVertIdx, face.getSmGroup(), rSrcVerts );
#else
				nVertIndex = nVertIdx;
#endif

				// 버텍스 uv 정보 세팅
				tmpvert = rSrcVerts[nVertIndex];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				// render to texture는, 기본적으로 3번 채널에 그 UV값이 들어간다.
				if( pMesh->mapSupport(3) )
				{
					nTVertIdx = pMesh->mapFaces(3)[i].t[j];

					//! 2번째 uv 버텍스 정보 세팅
					tmpvert.UV2.U = pMesh->mapVerts(3)[nTVertIdx].x;
					tmpvert.UV2.V = 1.0f - pMesh->mapVerts(3)[nTVertIdx].y;
				}

				// face에 버텍스 index 세팅
				tmpface.Index[j] = nVertIndex;

				if( bWritten[nVertIndex] )
				{
					if( ( false == FloatEqual( tmpvert.UV.U, tmpVerts[nVertIndex].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, tmpVerts[nVertIndex].UV.V ) ) ||
						( false == FloatEqual( tmpvert.UV2.U, tmpVerts[nVertIndex].UV2.U ) ) ||
						( false == FloatEqual( tmpvert.UV2.V, tmpVerts[nVertIndex].UV2.V ) ) )
					{
						tmpface.Index[j] = tmpVerts.Count();
						tmpVerts.Insert( tmpVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					tmpVerts[nVertIndex] = tmpvert;
					bWritten.Set( nVertIndex );
				}
			}
			rOutputFaces[i] = tmpface;
		}

		// 중복되는 거 없애기
		RdMeshExpUtil::CheckOverlappedVertex( rOutputVerts, rOutputFaces, tmpVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdStaticMeshExpUtil::CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices )
{
	Face face;
	ulong ulFaceCnt = pMesh->getNumFaces();
	DWORD dwSmGroup = 0;

	int nColorVertID = 0;
	int nVertIndex = 0;
	int nIndex = 0;

	if( NULL != pMesh->vertColArray )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			dwSmGroup = face.getSmGroup();

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFaceData[i].t[j];

#ifdef _ENABLE_SMGROUP_
				nVertIndex = RdMeshExp::GetNormalVertexIndex( nIndex, dwSmGroup, rVertices );
#else
				nVertIndex = nIndex;
#endif

				rVertices[nVertIndex].Color = pMesh->vertColArray[nColorVertID];
			}
		}
	}
	else if( NULL != pMesh->vertCol )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			dwSmGroup = face.getSmGroup();

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFace[i].t[j];

#ifdef _ENABLE_SMGROUP_
				nVertIndex = RdMeshExp::GetNormalVertexIndex( nIndex, dwSmGroup, rVertices );
#else
				nVertIndex = nIndex;
#endif

				rVertices[nVertIndex].Color = pMesh->vertCol[nColorVertID];
			}
		}
	}
}

//------------------------------------------------------------------
//	RdSkinMeshExpUtil
//------------------------------------------------------------------
void RdSkinMeshExpUtil::CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
										 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		BitArray bWritten;
		bWritten.SetSize( pMesh->getNumVerts() );
		bWritten.ClearAll();

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		Tab<RdVertex> tmpVerts;
		tmpVerts.SetCount( pMesh->getNumVerts() );

		ulong ulFaceCnt = pMesh->getNumFaces();

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			for( int j = 0; j < 3; ++j )
			{
				tmpface.Index[j] = face.v[j];
			}
			tmpface.MatID = face.getMatID();

			for( int k = 0; k < 3; ++k )
			{
				nVertIdx = face.v[k];
				nTVertIdx = tvface.t[k];

				tmpvert = rSrcVerts[nVertIdx];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				if( bWritten[nVertIdx] )
				{
					if( ( false == FloatEqual( tmpvert.UV.U, tmpVerts[nVertIdx].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, tmpVerts[nVertIdx].UV.V ) ) )
					{
						tmpface.Index[k] = tmpVerts.Count();
						tmpVerts.Insert( tmpVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					tmpVerts[nVertIdx] = tmpvert;
					bWritten.Set( nVertIdx );
				}
			}
			rOutputFaces[i] = tmpface;

		}

		// 중복되는 거 없애기
		RdMeshExpUtil::CheckOverlappedVertex( rOutputVerts, rOutputFaces, tmpVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdSkinMeshExpUtil::CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
										Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		BitArray bWritten;
		bWritten.SetSize( pMesh->getNumVerts() );
		bWritten.ClearAll();

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		Tab<RdVertex> tmpVerts;
		tmpVerts.SetCount( pMesh->getNumVerts() );

		ulong ulFaceCnt = pMesh->getNumFaces();

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			int j = 0;
			for( j = 0; j < 3; ++j )
			{
				tmpface.Index[j] = face.v[j];
			}
			tmpface.MatID = face.getMatID();

			for( int k = 0; k < 3; ++k )
			{
				nVertIdx = face.v[k];
				nTVertIdx = tvface.t[k];

				tmpvert = rSrcVerts[nVertIdx];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				// render to texture는, 기본적으로 3번 채널에 그 UV값이 들어간다.
				if( pMesh->mapSupport(3) )
				{
					nTVertIdx = pMesh->mapFaces(3)[i].t[j];

					//! 2번째 uv 버텍스 정보 세팅
					tmpvert.UV2.U = pMesh->mapVerts(3)[nTVertIdx].x;
					tmpvert.UV2.V = 1.0f - pMesh->mapVerts(3)[nTVertIdx].y;
				}

				if( bWritten[nVertIdx] )
				{
					if( ( false == FloatEqual( tmpvert.UV.U, tmpVerts[nVertIdx].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, tmpVerts[nVertIdx].UV.V ) ) ||
						( false == FloatEqual( tmpvert.UV2.U, tmpVerts[nVertIndex].UV2.U ) ) ||
						( false == FloatEqual( tmpvert.UV2.V, tmpVerts[nVertIndex].UV2.V ) ) )
					{
						tmpface.Index[k] = tmpVerts.Count();
						tmpVerts.Insert( tmpVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					tmpVerts[nVertIdx] = tmpvert;
					bWritten.Set( nVertIdx );
				}
			}
			rOutputFaces[i] = tmpface;

		}

		// 중복되는 거 없애기
		RdMeshExpUtil::CheckOverlappedVertex( rOutputVerts, rOutputFaces, tmpVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdSkinMeshExpUtil::CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices )
{
	Face face;
	ulong ulFaceCnt = pMesh->getNumFaces();

	int nColorVertID = 0;
	int nIndex = 0;

	if( NULL != pMesh->vertColArray )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFaceData[i].t[j];

				rVertices[nIndex].Color = pMesh->vertColArray[nColorVertID];
			}
		}
	}
	else if( NULL != pMesh->vertCol )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFace[i].t[j];

				rVertices[nIndex].Color = pMesh->vertCol[nColorVertID];
			}
		}
	}
}
