#ifndef __RD_MAX_OUTPUTEXP_H__
#define __RD_MAX_OUTPUTEXP_H__

#include "..\DataExp\RdMaxUtil.h"

//------------------------------------------------------------------------------
/**	Output Dialog
*/
//------------------------------------------------------------------------------
class RdOutputExp
{
public:
	RdOutputExp();
	~RdOutputExp();

	void	Open( HWND hParent, const TCHAR* strName );
	void	Close();

	bool	IsOpen() const		{	return m_bOpen;		}

	void	Show();
	void	Hide();

	///
	void	SendMsg( const TCHAR* pFormat, ... );
	void	ClearMsg();

	static INT_PTR CALLBACK	MsgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam );
	static RdOutputExp*	Instance();
private:
	HWND	m_hOutputExpDlg;

	bool	m_bOpen;

	FILE*	m_hFile;
};

#define OutputExpLog	RdOutputExp::Instance()->SendMsg

#endif	// __RD_MAX_OUTPUTEXP_H__