#include "RdMaxOutputExp.h"
#include "../RdMax2008Exporter.h"

RdOutputExp::RdOutputExp()
: m_bOpen(false)
{
}

RdOutputExp::~RdOutputExp()
{
	Close();
}

//------------------------------------------------------------------
/**
*/
void RdOutputExp::Open( HWND hParent, const TCHAR* strName )
{
	if (m_bOpen)
		return;

	m_hOutputExpDlg = CreateDialog( hInstance, MAKEINTRESOURCE(IDD_OUTPUT_DLG), hParent, RdOutputExp::MsgProc );
	ShowWindow( m_hOutputExpDlg, SW_SHOW );
	CenterWindow( m_hOutputExpDlg, GetWindow( m_hOutputExpDlg, GW_OWNER ) );

	TCHAR buffer[256];
	_stprintf( buffer, _T("%s.txt"), strName );

	m_hFile = fopen( buffer, "wt" );

	m_bOpen = true;
}

//------------------------------------------------------------------
/**
*/
void RdOutputExp::Close()
{
	if (!m_bOpen)
		return;

	fclose(m_hFile);

	ClearMsg();
	DestroyWindow(m_hOutputExpDlg);
	m_bOpen = false;
}

//------------------------------------------------------------------
/**
*/
void RdOutputExp::Show()
{
	if (!m_bOpen)
		return;

	ShowWindow( m_hOutputExpDlg, SW_SHOW );
}

//------------------------------------------------------------------
/**
*/
void RdOutputExp::Hide()
{
	if (!m_bOpen)
		return;

	ShowWindow( m_hOutputExpDlg, SW_HIDE );
}

//------------------------------------------------------------------
/**
*/
void RdOutputExp::SendMsg( const TCHAR* pFormat, ... )
{
	va_list args;

	TCHAR buffer[1024];
	memset( buffer, 0, sizeof(TCHAR) * 1024 );

	va_start( args, pFormat );

	vsprintf( buffer, pFormat, args );
	AddStringInListBox( m_hOutputExpDlg, IDC_OUTPUT_LIST, buffer );

	fprintf( m_hFile, _T("%s\n"), buffer );

	va_end( args );
}

//------------------------------------------------------------------
/**
*/
void RdOutputExp::ClearMsg()
{
	ClearStringInListBox( m_hOutputExpDlg, IDC_OUTPUT_LIST );
}

//------------------------------------------------------------------
/**
*/
INT_PTR CALLBACK RdOutputExp::MsgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
		}
		return TRUE;

	case WM_CLOSE:
		{
			RdOutputExp::Instance()->Close();
		}
		return TRUE;

	case WM_COMMAND:
		{
			//if (HIWORD( wParam ) == BN_CLICKED)
			{
				switch (LOWORD( wParam ))
				{
				case IDC_OUTPUT_OK:
					{
						RdOutputExp::Instance()->Close();
						break;
					}
				}
			}
		}
		return TRUE;
	}

	return FALSE;
}

//------------------------------------------------------------------
/**
*/
RdOutputExp* RdOutputExp::Instance()
{
	static RdOutputExp inst;
	return &inst;
}