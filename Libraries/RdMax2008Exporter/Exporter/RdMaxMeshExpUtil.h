#ifndef __RD_MAX_MESHEXP_UTIL_H__
#define __RD_MAX_MESHEXP_UTIL_H__

#include "..\DataExp\RdMaxMesh.h"
#include "..\DataExp\RdMaxUtil.h"

//==================================================================
/** RdMeshExpUtil...
	@remarks
		Mesh의 성격에 따른 Export를 하기 위함이다..
*/
//==================================================================
class RdMeshExpUtil
{
public:
	/** 버텍스 버퍼 구하기
	*/
	virtual void	CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices ) = 0;

	/** Vertex의 UV를 뽑는다.
	*/
	void			CollectUV(	Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
								Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces,
								bool bHasMultiUV );

	/** 중복된 버텍스를 정리한다..
	*/
	static void		CheckOverlappedVertex(Tab<RdVertex>& rVerts,
										  Tab<RdFace>& rFaces,
										  Tab<RdVertex>& rSrcVerts );
protected:
	virtual void	CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
									 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces ) = 0;

	virtual void	CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
									 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces ) = 0;

};

//==================================================================
/** RdStaticMeshExpUtil...
	@remarks
		Mesh의 성격에 따른 Export를 하기 위함이다..
*/
//==================================================================
class RdStaticMeshExpUtil : public RdMeshExpUtil
{
public:
	void	CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices );

protected:
	void	CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );

	void	CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );
};

//==================================================================
/** RdSkinMeshExpUtil...
	@remarks
		SkinnedMesh의 Export를 하기
*/
//==================================================================
class RdSkinMeshExpUtil : public RdMeshExpUtil
{
public:
	void	CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices );

protected:
	void	CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );

	void	CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );
};

#endif	// __RD_MAX_MESHEXP_UTIL_H__