#include "RdMaxMeshExp.h"
#include "RdMaxMeshExpUtil.h"

#include "RdMaxOutputExp.h"

#define ENABLE_BONECNT		27
#define ENABLE_MAXLINKCNT	4

//------------------------------------------------------------------
// Global
//------------------------------------------------------------------
Modifier* FindPhysiqueModifier( INode* pNode )
{
	Object* pObj = pNode->GetObjectRef();

	if( !pObj )
		return NULL;

	while( pObj->SuperClassID() == GEN_DERIVOB_CLASS_ID )
	{
		IDerivedObject* pDerivObj = static_cast<IDerivedObject*>(pObj);

		int nModStackIndex = 0;
		while( nModStackIndex < pDerivObj->NumModifiers() )
		{
			Modifier* pModifier = pDerivObj->GetModifier( nModStackIndex );

			if( pModifier->ClassID() == Class_ID( PHYSIQUE_CLASS_ID_A, PHYSIQUE_CLASS_ID_B ) )
				return pModifier;

			++nModStackIndex;
		}
		pObj = pDerivObj->GetObjRef();
	}

	return NULL;
}

//-------------------------------------------------------------------------
//	FindSkinModifier
//-------------------------------------------------------------------------
Modifier *FindSkinModifier( INode *pNode )
{
	Object *pObj = pNode->GetObjectRef();
	if( !pObj )
		return NULL;

	while( pObj->SuperClassID() == GEN_DERIVOB_CLASS_ID )
	{
		IDerivedObject *pDerivedObj = static_cast<IDerivedObject*>(pObj);

		int stackIdx = 0;
		while(stackIdx < pDerivedObj->NumModifiers())
		{
			Modifier* pMod = pDerivedObj->GetModifier(stackIdx++);
			if(pMod->ClassID() == SKIN_CLASSID)
				return pMod;
		}
		pObj = pDerivedObj->GetObjRef();
	}

	return NULL;
}

//-------------------------------------------------------------------------
/**
*/
Point3 GetRVertexNormal( Mesh* pMesh, int nVertexId, DWORD dwFaceId, DWORD dwSmGroupFace )
{
	RVertex* prVert = pMesh->getRVertPtr( nVertexId );

	if (prVert->rFlags & SPECIFIED_NORMAL)
		return prVert->rn.getNormal();

	int nNormals = prVert->rFlags & NORCT_MASK;

	ASSERT_MBOX( ( nNormals == 1 && prVert->ern == NULL ) || 
		( nNormals > 1 && prVert->ern != NULL ), _T( "BOGUS RVERTEX" ) );

	if (nNormals == 1)
	{
		return prVert->rn.getNormal();
	}
	else
	{
		DWORD smgroup = 0;
		int i = 0;
		for( i = 0; i < nNormals; i++ )
		{
			smgroup = prVert->ern[i].getSmGroup();
			if( smgroup & dwSmGroupFace )
				break;
		}

		if (i >= nNormals)
			i = 0;

		return prVert->ern[i].getNormal();
	}

	return pMesh->getFaceNormal(dwFaceId);
}

//-------------------------------------------------------------------------
/**
*/
RdUV GetVertexUv( Mesh* pMesh, int nFaceId, int nVertexId, int nMapNo )
{
	RdUV uvs;

	int numTV = pMesh->getNumMapVerts(nMapNo);
	if (numTV)
	{
		int tVertId = pMesh->mapFaces(nMapNo)[nFaceId].t[nVertexId];

		Point3 pt1 = pMesh->mapVerts(nMapNo)[tVertId];

		uvs.U = pt1.x;
		uvs.V = 1.0f - pt1.y;
	}
	else
	{
		switch (nVertexId)
		{
		case 0:
			uvs.U = 0.0f;
			uvs.V = 0.0f;
			break;
		case 1:
			uvs.U = 1.0f;
			uvs.V = 0.0f;
			break;
		case 2:
			uvs.U = 0.0f;
			uvs.V = 1.0f;
			break;
		}
	}

	return uvs;
}

//-------------------------------------------------------------------------
/**
*/
void GetPhysiqueWeights( IPhyContextExport* pContextExp, int nVertex, RdVertex* pVertex )
{
	IPhyVertexExport* pExport = (IPhyVertexExport*)pContextExp->GetVertexInterface( nVertex );
	if( pExport )
	{
		INode* pBone;
		int vType = pExport->GetVertexType();
		if( RIGID_TYPE == vType )
		{
			pBone = ((IPhyRigidVertex*)pExport)->GetNode();

			pVertex->Link = 1;
			pVertex->Blend[0].BoneID = GetIndexOfNode( pBone );
			pVertex->Blend[0].Weight = 1.0f;
		}
		else if( RIGID_BLENDED_TYPE == vType )
		{
			IPhyBlendedRigidVertex* pBlend = (IPhyBlendedRigidVertex*)pExport;
			pVertex->Link = pBlend->GetNumberNodes();

			// 최대 링크가 4가 넘더라도 4개까지만 저장되고 에러처리 될 것이다.
			float fWeight = 0.0f;

			for( int i = 0; i < pVertex->Link; ++i )
			{
				pBone = pBlend->GetNode( i );
				fWeight = pBlend->GetWeight( i );

				if( i > 3 )
				{
					for( int j = 0; j < 4; ++j )
					{
						if( pVertex->Blend[j].Weight < fWeight )
						{
							pVertex->Blend[j].BoneID = GetIndexOfNode( pBone );
							pVertex->Blend[j].Weight = fWeight;
							break;
						}
					}
				}
				else
				{
					pVertex->Blend[i].BoneID = GetIndexOfNode( pBone );
					pVertex->Blend[i].Weight = fWeight;
				}		
			}

			if( pVertex->Link > 4 )
			{
				float fTotal = 0.0f;
				for( int j = 0; j < 3; ++j )
				{
					fTotal += pVertex->Blend[j].Weight;
				}

				pVertex->Blend[3].Weight = 1.0f - fTotal;
			}
		}

		pContextExp->ReleaseVertexInterface( pExport );
	}
}

struct sVertex
{
	ushort index;
	float weight;
};

int CompareVert( const void* elem1, const void* elem2 )
{
	sVertex* pVert1 = (sVertex*)elem1;
	sVertex* pVert2 = (sVertex*)elem2;

	return (pVert1->weight < pVert2->weight);
}

void GetSkinWeights( ISkin* pSkinInterface, ISkinContextData* pContextExp, Modifier* pMod, int nVertex, RdVertex* pVertex )
{
	// 최대 링크가 4가 넘더라도 4개까지만 저장되고 에러처리 될 것이다.
	float fWeight = 0.0f;

	int nAssignedBone = 0;
	INode* pBone = NULL;

	Tab<sVertex> list;
	list.Init();

	int nSize = 0;

	pVertex->Link = pContextExp->GetNumAssignedBones( nVertex );
	for( int i = 0; i < pVertex->Link; ++i )
	{
		nAssignedBone = pContextExp->GetAssignedBone( nVertex, i );
		if( nAssignedBone < 0 )
			continue;

		pBone = pSkinInterface->GetBone( nAssignedBone );
		fWeight = pContextExp->GetBoneWeight( nVertex, i );

		ushort index = GetIndexOfNode( pBone );
		if (index == 1014)
		{
			TCHAR* name = pBone->GetName();
			int a = 1;
		}
		if( UNDESIRABLE_MARKER != index )
		{
			sVertex vert;
			vert.index = index;
			vert.weight = fWeight;

			list.Append( 1, &vert );

			nSize++;
		}
	}

	list.Sort( CompareVert );

	if( nSize > 4 )
	{
		float fTotal = 0.0f;
		for( int j = 0; j < 3; ++j )
		{
			pVertex->Blend[j].BoneID = list[j].index;
			pVertex->Blend[j].Weight = list[j].weight;

			fTotal += pVertex->Blend[j].Weight;
		}

		pVertex->Blend[3].BoneID = list[3].index;
		pVertex->Blend[3].Weight = 1.0f - fTotal;
	}
	else
	{
		for( int j = 0; j < nSize; ++j )
		{
			pVertex->Blend[j].BoneID = list[j].index;
			pVertex->Blend[j].Weight = list[j].weight;
		}
	}

	list.Delete( 0, nSize );
}

//------------------------------------------------------------------
// Class
//------------------------------------------------------------------
RdMeshExp::RdMeshExp( Interface* pInterface, HWND hWnd, bool bSelected )
: m_pInterface( pInterface )
, m_hMeshExpDlg( hWnd )
, m_bSelectedExp( bSelected )
, m_dwActiveFlags(0)
{
	m_bEnableExport	= true;

	m_NodeList.Init();
	m_NodeList.ZeroCount();

	m_MtlList.Init();
	m_MtlList.ZeroCount();

	m_IndoorNodeList.Init();
	m_IndoorNodeList.ZeroCount();

	m_ptTraceTop.Set( 0.0f, 0.0f, 0.0f );
	m_ptTraceBottom.Set( 0.0f, 0.0f, 0.0f );

	m_bTraceTop		= false;
	m_bTraceBottom	= false;

	m_pcExport		= NULL;
	m_phyExport		= NULL;
	m_phyMod		= NULL;

	m_pHeightNode	= NULL;

	m_bApplyScale	= false;
	m_bVertexColor	= false;
	m_bVertTangent	= false;
	m_bVertPacking	= false;
}

RdMeshExp::~RdMeshExp()
{
	Clear();
}

void RdMeshExp::Clear()
{
	int nCount = m_BBoxList.Count();
	RdBBox** ppBox;
	for( int i = 0; i < nCount; ++i )
	{
		ppBox = m_BBoxList.Addr( i );
		delete (*ppBox);
	}
	m_BBoxList.Delete( 0, nCount );

	nCount = m_NodeList.Count();
	RdMeshNode** ppNode;
	for( int i = 0; i < nCount; ++i )
	{
		ppNode = m_NodeList.Addr( i );

		delete (*ppNode);
	}
	m_NodeList.Delete( 0, nCount );

	nCount = m_MtlList.Count();
	RdMaterial** ppMtl;
	for( int i = 0; i < nCount; ++i )
	{
		ppMtl = m_MtlList.Addr( i );

		delete (*ppMtl);
	}
	m_MtlList.Delete( 0, nCount );

	nCount = m_IndoorNodeList.Count();
	RdNormFaceNode** ppFace;
	for( int i = 0; i < nCount; ++i )
	{
		ppFace = m_IndoorNodeList.Addr( i );

		delete (*ppFace );
	}
	m_IndoorNodeList.Delete( 0, nCount );
}

//------------------------------------------------------------------
// 정보 수집 함수들
//------------------------------------------------------------------
bool RdMeshExp::IsHeightNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("MH_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("mh_"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsIndoorNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("CL_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("cl_"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsMeshDummyNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("DM_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("dm_"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsTraceTopNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("TTD"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("ttd"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsTraceBottomNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("TBD"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("tbd"), 3 ) == 0 )
		return true;

	return false;
} 

//------------------------------------------------------------------
void RdMeshExp::Initialize()
{
	//RdOutputExp::Instance()->Open( m_hMeshExpDlg );

	int nChildNum = m_pInterface->GetRootNode()->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectNode( m_pInterface->GetRootNode()->GetChildNode( i ) );
	}

	for( int i = 0; i < nChildNum; ++i )
	{
		CollectBBoxData( m_pInterface->GetRootNode()->GetChildNode(i) );
	}

	if( m_NodeList.Count() == 0 )
		m_bEnableExport = false;

	if( ( !m_bTraceTop && m_bTraceBottom ) || ( m_bTraceTop && !m_bTraceBottom ) )
		m_bEnableExport = false;
}

//------------------------------------------------------------------
void RdMeshExp::OutputInformation()
{
	// Material 정보
	ushort usMtlCnt = m_MtlList.Count();
	RdMaterial* pMtl;
	for( ushort i = 0; i < usMtlCnt; ++i )
	{
		pMtl = m_MtlList[i];
		SendDlgItemMessage( m_hMeshExpDlg, IDC_MATERIAL_LIST, LB_ADDSTRING, NULL, (LPARAM)pMtl->GetMaterialName() );
	}

	if( m_bEnableExport )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("***** Enable to Export *****") );
	}
	else
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("----- Disable to Export -----") );
		HWND hWnd = GetDlgItem( m_hMeshExpDlg, IDC_MESH_SAVE );
		EnableWindow( hWnd, FALSE );
	}

	AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("===================================") );

	// Mesh 정보
	ulong ulVertCnt = 0;
	ulong ulFaceCnt = 0;
	ushort usBoneCnt = 0;
	ushort usMaxLink = 0;

	ushort usMeshNodeCnt = 0;
	ushort usDummyNodeCnt = 0;

	RdMeshNode* pMesh;
	TCHAR strMsg[256];

	ushort usNodeCnt = m_NodeList.Count();

	for( ushort i = 0; i < usNodeCnt; ++i )
	{
		pMesh = (RdMeshNode*)(m_NodeList[i]);

		memset( strMsg, 0, sizeof(TCHAR)*256 );

		if( pMesh->GetNodeType() == 0 )
		{
			_stprintf( strMsg, _T( "Mesh Node : [%d] %s" ), pMesh->GetNodeID(), pMesh->GetNodeName() );
			++usMeshNodeCnt;
		}
		else
		{
			_stprintf( strMsg, _T( "Dummy Node : [%d] %s" ), pMesh->GetNodeID(), pMesh->GetNodeName() );		
			++usDummyNodeCnt;
		}

		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );

		if( pMesh->GetNodeType() == 0 )
		{
			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tVertex Count : %d" ), pMesh->GetVertexCount() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			ulVertCnt += pMesh->GetVertexCount();

			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tFace Count : %d" ), pMesh->GetFaceCount() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			ulFaceCnt += pMesh->GetFaceCount();

			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tLinked Bone Count : %d" ), pMesh->GetLinkedBoneCount() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			if( usBoneCnt < pMesh->GetLinkedBoneCount() )
				usBoneCnt = pMesh->GetLinkedBoneCount();

			if( pMesh->GetLinkedBoneCount() > ENABLE_BONECNT )
				AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("[Error]한 옵젝에서 27개의 본만 사용할 수 있습니다." ) );

			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tLink Count : %d" ), pMesh->GetMaxLink() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			if( usMaxLink < pMesh->GetMaxLink() )
				usMaxLink = pMesh->GetMaxLink();

			if( pMesh->GetMaxLink() > ENABLE_MAXLINKCNT )	
			{
				AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("[Warning]버텍스 링크 개수가 커서 애니메이션이 찢어질 수 있습니다." ) );
			}
		}

		if( pMesh->GetEnableVertexColor() )
		{
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("::Has VertexColor") );
		}
		if (this->IsPackVertex())
		{
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("::Pack Vertex"));
		}

		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("===================================") );
	}


	int nNumBBox = m_BBoxList.Count();
	for( int i = 0; i < nNumBBox; ++i )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("Custom Bounding Box") );
		memset( strMsg, 0, sizeof(TCHAR)*256 );
		_stprintf( strMsg, _T( "\t-Linked Node: %s"), m_BBoxList[i]->GetParentName() ) ;
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
	}

	if( nNumBBox > 0 )
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("===================================") );

	if( m_pHeightNode )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("**! It exist already the height node. !**") );
	}

	if( m_IndoorNodeList.Count() != 0 )
	{
		memset( strMsg, 0, sizeof(TCHAR)*256 );
		_stprintf( strMsg,  _T("**! Indoor Node Count : %d !**"), m_IndoorNodeList.Count() );
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
	}

	if( m_bTraceTop )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("**! This Mesh include the Trace Top Information. !**") );
	}

	if( m_bTraceBottom )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("**! This Mesh include the Trace Bottom Information !**") );
	}

	SetDlgItemInt( m_hMeshExpDlg, IDC_VERTEX_CNT, ulVertCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_FACE_CNT, ulFaceCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_BONE_CNT, usBoneCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_LINK_CNT, usMaxLink, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_OBJECT_CNT, usMeshNodeCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_MDUMMY_CNT, usDummyNodeCnt, FALSE );
}

//------------------------------------------------------------------
void RdMeshExp::CollectNode( INode* pNode )
{
	if( UndesirableNode( pNode ) )
		return;

	if( m_bSelectedExp && pNode->Selected() == FALSE )
		return;

	if( !m_bSelectedExp || pNode->Selected() )
	{
		if( _tcscmp( pNode->GetName(), "Bip01 Footsteps" ) == 0 )
			return;

		ushort id = GetIndexOfNode( pNode );
		if( IsMeshNode( pNode ) )
		{
			if( IsBoneDummyNode( pNode->GetName() ) || IsBoundingBoxNode( pNode->GetName() ) ) 
			{

			}
			else if( IsHeightNode( pNode->GetName() ) )
			{
				m_pHeightNode = new RdNormFaceNode();
				if( !CollectFaceNormNode( pNode, m_pHeightNode ) )
				{
					SAFEDEL( m_pHeightNode );
				}
			}
			else if( IsIndoorNode( pNode->GetName() ) )
			{
				RdNormFaceNode* pNodeData = new RdNormFaceNode();
				if( !CollectFaceNormNode( pNode, pNodeData ) )
				{
					SAFEDEL( pNodeData );
				}
				else
				{
					m_IndoorNodeList.Append( 1, &pNodeData );
				}
			}
			else if( IsTraceTopNode( pNode->GetName() ) )
			{
				m_bTraceTop = true;

				Matrix3 world = pNode->GetNodeTM( 0 );
				AffineParts ap;
				decomp_affine( world, &ap );
				m_ptTraceTop.Set( ap.t.x, ap.t.z, ap.t.y );
			}
			else if( IsTraceBottomNode( pNode->GetName() ) )
			{
				m_bTraceBottom = true;

				Matrix3 world = pNode->GetNodeTM( 0 );
				AffineParts ap;
				decomp_affine( world, &ap );
				m_ptTraceBottom.Set( ap.t.x, ap.t.z, ap.t.y );
			}
			else
			{
				TCHAR buffer[256];

				int nNodes = m_NodeList.Count();
				for( int i = 0; i < nNodes; ++i )
				{
					if( 0 == _tcscmp( m_NodeList[i]->GetNodeName(), pNode->GetName() ) )
					{
						_stprintf( buffer, _T( "동일한 MeshNode 이름 [%s]발견.. 수정해 주세요." ), pNode->GetName() );
						ASSERT_MBOX( FALSE, buffer );
						break;
					}
				}

				RdMeshNode* pNodeData = new RdMeshNode( 0, id, pNode->GetName() );
				CollectMeshNode( pNode, pNodeData );

				//if( pNodeData->GetVertexCount() == 0 || pNodeData->GetFaceCount() == 0 )
				//{
				//	SAFEDEL( pNodeData );
				//}
				//else
				//{
				//	m_NodeList.Append( 1, &pNodeData );
				//}
			}
		}
		else if( IsMeshDummyNode( pNode->GetName() ) )
		{
			RdMeshNode* pNodeData = new RdMeshNode( 2, id, pNode->GetName() );
			m_NodeList.Append( 1, &pNodeData );
			CollectNodeData( pNode, pNodeData, false, false );
		}

		//else if( IsDummyNode( pNode ) )
		//{
		//	RdMeshNode* pNodeData = new RdMeshNode( 2, id, pNode->GetName() );
		//	m_NodeList.Append( 1, &pNodeData );
		//	CollectNodeData( pNode, pNodeData, false );
		//}
	}

	int nChildNum = pNode->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectNode( pNode->GetChildNode( i ) ); 
	}
}

//------------------------------------------------------------------
void RdMeshExp::CollectNodeData( INode* pNode, RdMeshNode* pNodeData, bool bNeg, bool bForce )
{
	INode* pParent = pNode->GetParentNode();

	if( pParent && !pParent->IsRootNode() )
	{
		pNodeData->SetParentNode( GetIndexOfNode( pParent ), pParent->GetName() );
	}

	if (!bForce)
	{
		Matrix3 world = pNode->GetNodeTM( 0 );
		Matrix3 local = world;

		if (!bNeg)
		{
			//! 부모노드가 있고, 부모 노드가 Root노드가 아니라면, 로컬 Transform을 계산한다.
			if (pParent && !pParent->IsRootNode())
				local = world * Inverse( pParent->GetNodeTM(0) );
		}
		else
		{
			Matrix3 tmp = world;
			Uniform_Matrix( tmp, world );

			if (pParent && !pParent->IsRootNode())
			{
				Matrix3 worldp;
				tmp = pParent->GetNodeTM(0);
				Uniform_Matrix( tmp, worldp );

				local = world * Inverse( worldp );
			}
		}

		AffineParts ap;
		decomp_affine( local, &ap );
		pNodeData->SetLocalPosition( Point3( ap.t.x, ap.t.z, ap.t.y ) );	
		pNodeData->SetLocalRotation( Quat( ap.q.x, ap.q.z, ap.q.y, ap.q.w ) );
		pNodeData->SetLocalScale( Point3( ap.k.x, ap.k.z, ap.k.y ) );

		ConvertMatrixToD3D( &world );
		pNodeData->SetWorldTransform( world );

		pNodeData->SetMaxNode( pNode );
	}
}

//------------------------------------------------------------------
/**
*/
void RdMeshExp::CheckSkinnedType( INode* pNode )
{
	// Physic 정보 있는지 확인
	m_pcExport = NULL;
	m_phyExport = NULL;
	m_phyMod = NULL;

	// Skin 정보 있는지 확인
	m_skinMod = NULL;
	m_skinInterface = NULL;
	m_skinContext = NULL;

	m_phyMod = FindPhysiqueModifier( pNode );
	if( m_phyMod )
	{
		m_phyExport = (IPhysiqueExport*)m_phyMod->GetInterface( I_PHYINTERFACE );

		if( m_phyExport )
		{
			m_pcExport = (IPhyContextExport*)m_phyExport->GetContextInterface( pNode );
			if( m_pcExport )
			{
				m_pcExport->ConvertToRigid( TRUE );
				m_pcExport->AllowBlending( TRUE );
			}
		}
	}
	else
	{
		m_skinMod = FindSkinModifier( pNode );
		if( m_skinMod )
		{
			m_skinInterface = ( ISkin* )m_skinMod->GetInterface( I_SKIN );
			if( m_skinInterface )
			{
				m_skinContext = m_skinInterface->GetContextInterface( pNode );
			}
		}
	}
}

//------------------------------------------------------------------
/**
*/
void RdMeshExp::CollectMeshNode( INode* pNode, RdMeshNode* pNodeData )
{
	// 노드가 메쉬 데이터를 가지고 있는지 확인
	ObjectState os = pNode->EvalWorldState( 0 );
	Object* pObj = os.obj;
	TriObject* pTriObj;
	BOOL bConvertedToTriObject = ( pObj->CanConvertToType( triObjectClassID ) ) && 
		( ( pTriObj = (TriObject*)pObj->ConvertToType( 0, triObjectClassID ) ) != NULL );

	if( !bConvertedToTriObject )
		return;

	// Material 정보 있는지 확인
	bool bMultiMtl = false;

	Mtl* pNodeMtl = pNode->GetMtl();
	if( NULL == pNodeMtl )
		return;
	else if( pNodeMtl->ClassID() == Class_ID( MULTI_CLASS_ID, 0 ) && pNodeMtl->IsMultiMtl() )
		bMultiMtl = true;

	Mesh* pMesh = &pTriObj->GetMesh();
	if( NULL == pMesh || pMesh->getNumVerts() == 0 || pMesh->getNumFaces() == 0 )
		return;

	pMesh->buildNormals();
	pMesh->checkNormals(TRUE);
	// 미러된 건지 확인
	Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
	bool bNeg = IsTMNegParity( world );

//	world = pNode->GetNodeTM( 0 );
//	world.NoScale();

	this->CheckSkinnedType( pNode );

	//------------------------------------------------------------------
	// Node Data
	bool bForce = (m_pcExport || m_skinContext) ? true : false;
	CollectNodeData( pNode, pNodeData, bNeg, bForce );

	// Mesh Data
	CollectMeshData( pMesh, pNodeData, world, m_pcExport, m_skinContext, bNeg );
	CollectMaterialData( pNodeMtl, pNodeData, bMultiMtl );

	if( m_bEnableExport )
	{
		if( pNodeData->GetLinkedBoneCount() > ENABLE_BONECNT )
		{
			// 분리 작업 해야함...
			SeperateMeshNode( pNodeData );
			SAFEDEL( pNodeData );
		}
		else
		{
			pNodeData->NotifyComplete();
			m_NodeList.Append( 1, &pNodeData );
		}
//		m_bEnableExport = false;
	}

	NodeCleanUp();
}

//------------------------------------------------------------------
/**
*/
void RdMeshExp::CollectMeshData( Mesh* pMesh,
								 RdMeshNode* pNodeData,
								 const Matrix3& matWorld, 
								 IPhyContextExport* pExport,
								 ISkinContextData* pSkinExp,
								 bool bNeg )
{
	//! smooting Group에 맞는 노말 벡터를 뽑기 위해서, 버텍스의 개수를 늘려야 하는데, 스킨 메쉬의 경우에는
	//! 애니메이션의 부하를 줄이기 위해서 static mesh에만 적용한다.
	if( pExport )
	{
		CollectPhyMeshData( pMesh, pNodeData, matWorld, pExport, bNeg );
	}
	else if( pSkinExp )
	{
		CollectSkinMeshData( pMesh, pNodeData, matWorld, pSkinExp, bNeg );
	}
	else
	{
		CollectStaticMeshData( pMesh, pNodeData, matWorld, bNeg );
	}	
}

//@add by cagetu -09/09/28 : tangent가 정상적으로 추출되지 않아서, smoothingGroup은 일단 막자!!

//------------------------------------------------------------------
/**
*/
void RdMeshExp::CollectStaticMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, bool bNeg )
{
	Tab<RdVertex> verts;
	Tab<RdFace> faces;
	Tab<RdVertex> orgverts;

	Matrix3 matNorm = matWorld;
	Matrix3 mat = matWorld;

	//! 스케일을 적용해서 뽑을 것인지를 외부에서 결정한다.
	if (!m_bApplyScale)
	{
		mat.NoScale();
	}

	matNorm.NoScale();
	matNorm.NoTrans();

	ulong ulFaceCnt = pMesh->getNumFaces();
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;;

	RdVertex tmpvert;
	RdFace tmpface;
	ulong nIndex = 0, nVertIndex = 0;
	Point3 normal, pos;
	Face face;
	DWORD dwSmGroup;

	Matrix3 transpose;

#if defined (_ENABLE_SMGROUP_) && !defined(NEW_VERTEXNORMAL)
	//! smGroup의 개수에 맞춰서 버텍스 개수를 만들어준다.
	orgverts.SetCount( 0 );
	GetNormalVertexCount( pMesh, orgverts );
	ulong ulNormalVerts = orgverts.Count();

	bWritten.SetSize( ulNormalVerts );
	bWritten.ClearAll();
#else
	ulong ulOrgVtxCnt = pMesh->getNumVerts();
	orgverts.SetCount( ulOrgVtxCnt );

	bWritten.SetSize( ulOrgVtxCnt );
	bWritten.ClearAll();
#endif
	Point3 v0, v1, v2, norm;

	// 버텍스 정보 집어넣기
#ifndef NEW_VERTEXNORMAL

	Tab<RdVertex> vertexBuffer;
	vertexBuffer.SetCount(0);

	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];

		dwSmGroup = face.getSmGroup();
		tmpface.smGroup = dwSmGroup;
		tmpface.MatID = face.getMatID();

		for (int j = 0; j < 3; ++j)
		{
			/// vertex index
			nIndex = face.v[j];

	#ifdef _ENABLE_SMGROUP_
			nVertIndex = GetNormalVertexIndex( nIndex, dwSmGroup, orgverts );
	#else
			nVertIndex = nIndex;
	#endif

#ifdef EXPORT_VERT_FROM_FACE
			//if (!bWritten[nVertIndex])
			{
				//! 포지션 벡터 구하기;
				orgverts[nVertIndex].Pos = mat * pMesh->verts[nIndex];

				// Normal 벡터 구하기
				normal = GetRVertexNormal( pMesh, nIndex, i, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				/// [2009-04-21] 역전치를 하면, normal이 뭉개지는 문제가 발생... 
				//Transpose_Matrix( mat, transpose );
				//normal = normal * Inverse(transpose);
				//normal.Normalize();

				orgverts[nVertIndex].Normal = normal;

				// uv 구하기
				RdUV uvs = GetVertexUv( pMesh, i, j, 1 );
				orgverts[nVertIndex].UV = uvs;

				int layer = 0;
				for (int m=1; m<MAX_MESHMAPS-1; m++)
				{
					if (pMesh->mapSupport(m))
					{
						RdUV uv = GetVertexUv(pMesh, i, j, m);
						orgverts[nVertIndex].SetUv(layer++, uv);
					}
				}

				tmpvert.Pos = mat * pMesh->verts[nIndex];
				tmpvert.Normal = normal;
				tmpvert.UV = uvs;
				vertexBuffer.Append( 1, &tmpvert );

				//! uv 구하기
				//bWritten.Set( nVertIndex );
			}
			tmpface.Index[j] = vertexBuffer.Count() - 1;

#else
			if (!bWritten[nVertIndex])
			{
				// Normal 벡터 구하기
				normal = GetRVertexNormal( pMesh, nIndex, i, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				/// [2009-04-21] 역전치를 하면, normal이 뭉개지는 문제가 발생... 
				//Transpose_Matrix( mat, transpose );
				//normal = normal * Inverse(transpose);
				//normal.Normalize();

				orgverts[nVertIndex].Normal = normal;

				//! 포지션 벡터 구하기
				orgverts[nVertIndex].Pos = mat * pMesh->verts[nIndex];

				//! uv 구하기
				bWritten.Set( nVertIndex );
			}
			tmpface.Index[j] = nVertIndex;
#endif
		}

		faces[i] = tmpface;
	}
#else
	/// vertex normal....
	Tab<VNormal> vnormals;
	Tab<Point3> fnormals;
	ComputeVertexNormals(pMesh, bNeg, mat, vnormals, fnormals);

	// 버텍스 정보 집어넣기
	for (ushort t = 0; t < ulFaceCnt; ++t)
	{
		face = pMesh->faces[t];

		//tmpface.Normal = fnormals[t];
		tmpface.MatID = face.getMatID();
		tmpface.smGroup = face.smGroup;

		for( int i = 0; i < 3; ++i )
		{
			nIndex = face.v[i];

			if (vnormals[nIndex].next)
			{
				//Point3 normal = vnormals[nIndex].next->normal;
				int a = 1;
			}

			orgverts[nIndex].Normal = vnormals[nIndex].normal;
			//! 포지션 벡터 구하기
			orgverts[nIndex].Pos = mat * pMesh->verts[nIndex];
			
			tmpface.Index[i] = nIndex;
		}

		faces[t] = tmpface;
	}
#endif

	RdStaticMeshExpUtil* pMeshExpUtil = new RdStaticMeshExpUtil;
	{
		//! 버텍스 칼라 존재 여부
		bool bHasVertColor = ( pMesh->numCVerts > 0 ) ? true : false;
		pNodeData->SetEnableVertexColor( bHasVertColor );

#ifndef EXPORT_VERT_FROM_FACE
		if (bHasVertColor)
		{
			pMeshExpUtil->CollectVertexColor( pMesh, orgverts );
		}

		// 위치 버텍스와 텍스쳐 버텍스 맞추기
		bool bHasMultiUV = ( TRUE == pMesh->mapSupport(3) ) ? true : false;
		pNodeData->SetMultiUV( bHasMultiUV );

		pMeshExpUtil->CollectUV( pMesh, orgverts, verts, faces, bHasMultiUV );
#else
		if (bHasVertColor)
		{
			pMeshExpUtil->CollectVertexColor( pMesh, vertexBuffer );
		}

		RdMeshExpUtil::CheckOverlappedVertex( verts, faces, vertexBuffer );
#endif
	}
	delete pMeshExpUtil;

	// tangent 구하기..
	//ComputeVertexTangents( pNodeData, verts, faces, bNeg );

	pNodeData->SetFaceList( faces, bNeg );
#ifdef EXPORT_VERT_FROM_FACE
	pNodeData->SetVertexList( verts /*orgverts*/ );
#else
	pNodeData->SetVertexList( verts );
#endif
}

//------------------------------------------------------------------
//	Desc : Max에서 Physique을 적용하여 Skinning을 작업한 MeshObject의 데이터를 추출한다.
void RdMeshExp::CollectPhyMeshData(Mesh* pMesh,
								   RdMeshNode* pNodeData,
								   const Matrix3& matWorld,
								   IPhyContextExport* pExport,
								   bool bNeg )
{
	Tab<RdVertex> verts;
	Tab<RdFace> faces;
	Tab<RdVertex> orgverts;

	Matrix3 matNorm = matWorld;
	Matrix3 mat = matWorld;

	//! 스케일을 적용해서 뽑을 것인지를 외부에서 결정한다.
	if( !m_bApplyScale )
	{
		mat.NoScale();
	}

	matNorm.NoScale();
	matNorm.NoTrans();
	
	ulong ulFaceCnt = pMesh->getNumFaces();
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;;

	RdVertex tmpvert;
	RdFace tmpface;
	int nIndex;
	Point3 normal, pos;
	Face face;
	DWORD dwSmGroup;

	RVertex* prVert;
	Matrix3 transpose;

	// Skinned Mesh 와 구별해서 뽑자~
	ulong ulOrgVtxCnt = pMesh->getNumVerts();
	orgverts.SetCount( ulOrgVtxCnt );

	bWritten.SetSize( ulOrgVtxCnt );
	bWritten.ClearAll();

	Tab<RdVertex> vertexBuffer;
	vertexBuffer.SetCount(0);

	// 버텍스 정보 집어넣기
	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];
		dwSmGroup = face.getSmGroup();
		tmpface.smGroup = dwSmGroup;

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face.v[j];

#ifdef EXPORT_VERT_FROM_FACE
			//if( !bWritten[nIndex] )
			{
				normal = GetRVertexNormal( pMesh, nIndex, i, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				/// [2009-04-21] 역전치를 하면, normal이 뭉개지는 문제가 발생... 
				//Transpose_Matrix( mat, transpose );
				//normal = normal * Inverse(transpose);
				//normal.Normalize();

				tmpvert.Normal = normal;
				/// pos
				tmpvert.Pos = mat * pMesh->verts[nIndex];
				/// uv
				tmpvert.UV = GetVertexUv( pMesh, i, j, 1 );
				int layer = 0;
				for (int m=1; m<MAX_MESHMAPS-1; m++)
				{
					if (pMesh->mapSupport(m))
					{
						RdUV uv = GetVertexUv(pMesh, i, j, m);
						tmpvert.SetUv(layer++, uv);
					}
				}

				GetPhysiqueWeights( pExport, nIndex, &tmpvert );

				//bWritten.Set( nIndex );
				orgverts[nIndex] = tmpvert;

				vertexBuffer.Append( 1, &tmpvert );
			}
			tmpface.Index[j] = vertexBuffer.Count() - 1;
#else
			if( !bWritten[nIndex] )
			{
				normal = GetRVertexNormal( pMesh, nIndex, i, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				/// [2009-04-21] 역전치를 하면, normal이 뭉개지는 문제가 발생... 
				//Transpose_Matrix( mat, transpose );
				//normal = normal * Inverse(transpose);
				//normal.Normalize();

				tmpvert.Normal = normal;
				/// pos
				tmpvert.Pos = mat * pMesh->verts[nIndex];

				GetPhysiqueWeights( pExport, nIndex, &tmpvert );

				bWritten.Set( nIndex );
				orgverts[nIndex] = tmpvert;
			}
			tmpface.Index[j] = nIndex;
#endif // EXPORT_VERT_FROM_FACE
		}
		faces[i] = tmpface;
		faces[i].MatID = face.getMatID();
	}

	RdSkinMeshExpUtil* pMeshExpUtil = new RdSkinMeshExpUtil;
	{
		//! 버텍스 칼라 존재 여부
		bool bHasVertColor = ( pMesh->numCVerts > 0 ) ? true : false;
		pNodeData->SetEnableVertexColor( bHasVertColor );

#ifndef EXPORT_VERT_FROM_FACE
		if (bHasVertColor)
		{
			pMeshExpUtil->CollectVertexColor( pMesh, orgverts );
		}

		// 위치 버텍스와 텍스쳐 버텍스 맞추기
		bool bHasMultiUV = ( TRUE == pMesh->mapSupport(3) ) ? true : false;
		pNodeData->SetMultiUV( bHasMultiUV );

		pMeshExpUtil->CollectUV( pMesh, orgverts, verts, faces, bHasMultiUV );
#else
		if (bHasVertColor)
		{
			pMeshExpUtil->CollectVertexColor( pMesh, vertexBuffer );
		}

		RdMeshExpUtil::CheckOverlappedVertex( verts, faces, vertexBuffer );
#endif
	}
	delete pMeshExpUtil;

	// tangent 구하기..
	//ComputeVertexTangents( pNodeData, orgverts, faces, bNeg );

	pNodeData->SetFaceList( faces, bNeg );
#ifdef EXPORT_VERT_FROM_FACE
	pNodeData->SetVertexList( verts /*orgverts*/ );
#else
	pNodeData->SetVertexList( verts );
#endif
}

//------------------------------------------------------------------
//	Desc : Max에서 Skin을 적용하여 Skinning을 작업한 MeshObject의 데이터를 추출한다.
//------------------------------------------------------------------
void RdMeshExp::CollectSkinMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, ISkinContextData* pExport, bool bNeg )
{
	Tab<RdVertex> verts;
	Tab<RdFace> faces;
	Tab<RdVertex> orgverts;

	Matrix3 matNorm = matWorld;
	Matrix3 mat = matWorld;

	//! 스케일을 적용해서 뽑을 것인지를 외부에서 결정한다.
	if( !m_bApplyScale )
	{
		mat.NoScale();
	}
 
	matNorm.NoScale();
	matNorm.NoTrans();

	ulong ulFaceCnt = pMesh->getNumFaces();
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;;

	RdVertex tmpvert;
	RdFace tmpface;
	int nIndex;
	Point3 normal, pos;
	Face face;
	DWORD dwSmGroup;

	RVertex* prVert;
	Matrix3 transpose;

	// Skinned Mesh 와 구별해서 뽑자~
	ulong ulOrgVtxCnt = pMesh->getNumVerts();
	orgverts.SetCount( ulOrgVtxCnt );

	bWritten.SetSize( ulOrgVtxCnt );
	bWritten.ClearAll();

	Tab<RdVertex> vertexBuffer;
	vertexBuffer.SetCount(0);

	// 버텍스 정보 집어넣기
	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];
		dwSmGroup = face.getSmGroup();
		tmpface.smGroup = dwSmGroup;

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face.v[j];

#ifdef EXPORT_VERT_FROM_FACE
			//if( !bWritten[nIndex] )
			{
				normal = GetRVertexNormal( pMesh, nIndex, i, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				/// [2009-04-21] 역전치를 하면, normal이 뭉개지는 문제가 발생... 
				//Transpose_Matrix( mat, transpose );
				//normal = normal * Inverse(transpose);
				//normal.Normalize();

				tmpvert.Normal = normal;

				tmpvert.Pos = mat * pMesh->verts[nIndex];
				/// uv
				tmpvert.UV = GetVertexUv( pMesh, i, j, 1 );
				int layer = 0;
				for (int m=1; m<MAX_MESHMAPS-1; m++)
				{
					if (pMesh->mapSupport(m))
					{
						RdUV uv = GetVertexUv(pMesh, i, j, m);
						tmpvert.SetUv(layer++, uv);
					}
				}

				GetSkinWeights( m_skinInterface, pExport, m_skinMod, nIndex, &tmpvert );

				//bWritten.Set( nIndex );
				orgverts[nIndex] = tmpvert;

				vertexBuffer.Append( 1, &tmpvert );
			}
			tmpface.Index[j] = vertexBuffer.Count() - 1;
#else
			if( !bWritten[nIndex] )
			{
				normal = GetRVertexNormal( pMesh, nIndex, i, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				/// [2009-04-21] 역전치를 하면, normal이 뭉개지는 문제가 발생... 
				//Transpose_Matrix( mat, transpose );
				//normal = normal * Inverse(transpose);
				//normal.Normalize();

				tmpvert.Normal = normal;

				tmpvert.Pos = mat * pMesh->verts[nIndex];

				GetSkinWeights( m_skinInterface, pExport, m_skinMod, nIndex, &tmpvert );

				bWritten.Set( nIndex );
				orgverts[nIndex] = tmpvert;
			}
			tmpface.Index[j] = nIndex;
#endif // EXPORT_VERT_FROM_FACE
		}
		faces[i] = tmpface;
		faces[i].MatID = face.getMatID();
	}

	RdSkinMeshExpUtil* pMeshExpUtil = new RdSkinMeshExpUtil;
	{
		//! 버텍스 칼라 존재 여부
		bool bHasVertColor = ( pMesh->numCVerts > 0 ) ? true : false;
		pNodeData->SetEnableVertexColor( bHasVertColor );

#ifndef EXPORT_VERT_FROM_FACE
		if (bHasVertColor)
		{
			pMeshExpUtil->CollectVertexColor( pMesh, orgverts );
		}

		// 위치 버텍스와 텍스쳐 버텍스 맞추기
		bool bHasMultiUV = ( TRUE == pMesh->mapSupport(3) ) ? true : false;
		pNodeData->SetMultiUV( bHasMultiUV );

		pMeshExpUtil->CollectUV( pMesh, orgverts, verts, faces, bHasMultiUV );
#else
		if (bHasVertColor)
		{
			pMeshExpUtil->CollectVertexColor( pMesh, vertexBuffer );
		}

		RdMeshExpUtil::CheckOverlappedVertex( verts, faces, vertexBuffer );
#endif
	}
	delete pMeshExpUtil;

	// tangent 구하기..
	//ComputeVertexTangents( pNodeData, verts, faces, bNeg );

	pNodeData->SetFaceList( faces, bNeg );
#ifdef EXPORT_VERT_FROM_FACE
	pNodeData->SetVertexList( verts /*orgverts*/ );
#else
	pNodeData->SetVertexList( verts );
#endif
}

//------------------------------------------------------------------
void RdMeshExp::CollectMaterialData( Mtl* pNodeMtl, RdMeshNode* pNodeData, bool bMultiMtl )
{
	ushort usFaceCnt = pNodeData->GetFaceCount();

	if( bMultiMtl )
	{
		Mtl* pMtl;
		ushort usMtlID;

		Tab<ushort>	FaceMtlIDs;
		FaceMtlIDs.ZeroCount();
		FaceMtlIDs.SetCount( usFaceCnt );

		int nSubMtl = pNodeMtl->NumSubMtls();
		for( int i = 0; i < nSubMtl; ++i )
		{
			pMtl = pNodeMtl->GetSubMtl( i );

			if( NULL == pMtl || ( pMtl->ClassID() != Class_ID( DMTL_CLASS_ID, 0 ) ) )
				continue;

			usMtlID = AddMaterial( pNodeMtl, pMtl );
			if (usMtlID != 0xffff)
			{
				ushort mtlID;
				for( ushort j = 0; j < usFaceCnt; ++j )
				{
					// face의 서브 매터리얼 번호
					mtlID = pNodeData->GetFaceMtlIndex( j );

					if (HasSameMaterial( mtlID, nSubMtl, i ))
					{
						//pNodeData->SetFaceMtlIndex( j, usMtlID );
						FaceMtlIDs[j] = usMtlID;
					}
				}	// for
			}

			//@ 2009.6.11 - cagetu : 인덱스 변경(i->usMtlID)
			if (m_MtlList[usMtlID]->HaveNormalMap())
				pNodeData->UseNormalMap( true );
		}

		for ( ushort j=0; j<usFaceCnt; ++j)
		{
			pNodeData->SetFaceMtlIndex( j, FaceMtlIDs[j] );
		}
	}
	else
	{
		ushort usMtlID = AddMaterial( NULL, pNodeMtl );
		if( usMtlID != 0xffff )
		{
			for( ushort j = 0; j < usFaceCnt; ++j )
			{
				pNodeData->SetFaceMtlIndex( j, usMtlID );
			}
		}

		if (m_MtlList[usMtlID]->HaveNormalMap())
			pNodeData->UseNormalMap( true );
	}
}

bool RdMeshExp::HasSameMaterial( ushort usMtlID, int nNumMtls, int nCurrentMtl )
{
	MtlID id = usMtlID % nNumMtls;
	return ( (nCurrentMtl == -1 && nNumMtls == 1) || id == nCurrentMtl );
}

ushort RdMeshExp::AddMaterial( Mtl* pParentMtl, Mtl* pNodeMtl )
{
	static TCHAR MaterialName[512];

	if (pParentMtl)
	{
		_stprintf( MaterialName, _T( "%s_%s" ), pParentMtl->GetName(), pNodeMtl->GetName() );
	}
	else
	{
		_stprintf( MaterialName, _T( "%s" ), pNodeMtl->GetName() );
	}

	ushort usCount = m_MtlList.Count();
	ushort usIndex = 0;
	for( ushort i = 0; i < usCount; ++i )
	{
		if( _tcscmp( m_MtlList[i]->GetMaterialName(), MaterialName ) == 0 )
			return usIndex;

		++usIndex;
	}

	//if( !pNodeMtl->GetSubTexmap(ID_DI) )
	//	return 0xffff;

	RdMaterial* pMtl = new RdMaterial( MaterialName );
	pMtl->SetMaterialID( usCount );

	TSTR classname;
	pNodeMtl->GetClassName(classname);

	//! TwoSide 정보..
	StdMat2* pstdMtl = (StdMat2*)pNodeMtl;
	pMtl->SetTwoSide( pstdMtl->GetTwoSided() ? 1 : 0 );

	//! Map(Texture 정보 얻어오기)
	{
		TSTR strFullName, strPath, strFile;

		Texmap* pMap;
		
		/// Self-illum
		if (pstdMtl->MapEnabled(ID_SI))
		{
			//! Diffuse Map
			pMap = pNodeMtl->GetSubTexmap( ID_SI );
			if (pMap)
			{
				float selfIlumAmt = pstdMtl->GetTexmapAmt(ID_SI, 0);

				if (selfIlumAmt > 0.8f)
				{	// blank...
				}
				else
				{
				}

				TSTR classname;
				pNodeMtl->GetClassName(classname);

				Class_ID classID = pMap->ClassID();
				if (classID == Class_ID( BMTEX_CLASS_ID, 0 ))
				{
					strFullName = ((BitmapTex*)pMap)->GetMapName();
					SplitPathFile( strFullName, &strPath, &strFile );		

					pMtl->SetEmissiveMap( strFile );
				}
				else if (classID == Class_ID( VCOL_CLASS_ID, 0) )
				{	// blank...
				}
			}
		}

		if (IsMultiTextured(pstdMtl))
		{
			DumpMultiTexture( pstdMtl, pMtl );
		}
		else if (pstdMtl->MapEnabled(ID_DI))	// Diffuse
		{
			//! Diffuse Map
			pMap = pNodeMtl->GetSubTexmap( ID_DI );
			if( pMap )
			{
				TSTR classname;
				pNodeMtl->GetClassName(classname);

				Class_ID classID = pMap->ClassID();
				if (classID == Class_ID( BMTEX_CLASS_ID, 0 ))
				{
					strFullName = ((BitmapTex*)pMap)->GetMapName();
					SplitPathFile( strFullName, &strPath, &strFile );		

					if( IsShellMaterial( classname ) )
	//				if( IsMultiTextured( pstdMtl ) )
					{
						//! 라이트 맵! Render To Texture 정보를 구한다.
						pMtl->SetLightMap( strFile );
					}
					else
					{
						pMtl->SetDiffuseMap( strFile );
					}
				}
			}
		}
		else if (pstdMtl->MapEnabled(ID_RL))	// Reflection
		{
		}

		//! Opacity Map
		if (pstdMtl->MapEnabled(ID_OP))
		{
			pMap = pNodeMtl->GetSubTexmap( ID_OP );
			if( pMap )
			{
				if( pMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) )
				{
					pMtl->SetOpacity( 1 );
				}
			}
		}

		//! Specular Map
		pMap = pNodeMtl->GetSubTexmap( ID_SP );
		if( pMap )
		{
			if( pMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
			{
				strFullName = ((BitmapTex*)pMap)->GetMapName();
				SplitPathFile( strFullName, &strPath, &strFile );		
				pMtl->SetSpecularMap( strFile );
			}
		}

		//! Normal Map
		pMap = pNodeMtl->GetSubTexmap( ID_BU );
		if (pMap)
		{
			Class_ID classID = pMap->ClassID();

			if( classID == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
			{
				strFullName = ((BitmapTex*)pMap)->GetMapName();
				SplitPathFile( strFullName, &strPath, &strFile );		
				pMtl->SetNormalMap( strFile );
			}
			else
			{
				CStr className;
				pMap->GetClassName(className);
				if (className == CStr(_T("Normal Bump")))
				{
					static const int NB_NORMAL = 0;
					static const int NB_ADDITIONAL_BUMP = 1;

					//! Normal Map
					Texmap* subTexMap = 0;

					subTexMap = pMap->GetSubTexmap(NB_NORMAL);
					if (subTexMap)
					{
						if ( subTexMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
						{
							strFullName = ((BitmapTex*)subTexMap)->GetMapName();
							SplitPathFile( strFullName, &strPath, &strFile );		
							pMtl->SetNormalMap( strFile );
						}
					}
					subTexMap = pMap->GetSubTexmap(NB_ADDITIONAL_BUMP);
					if (subTexMap)
					{
						if ( subTexMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
						{
							strFullName = ((BitmapTex*)subTexMap)->GetMapName();
							SplitPathFile( strFullName, &strPath, &strFile );		
							pMtl->SetNormalMap( strFile );
						}
					}
				}
			}
		}

		//for (int i=0; i< pMap->NumSubTexmaps(); ++i)
		//{
		//	//! Diffuse Map
		//	Texmap* subTexMap = pMap->GetSubTexmap(i);
		//	if (subTexMap)
		//	{
		//		TSTR classname;
		//		if ( subTexMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
		//		{
		//			strFullName = ((BitmapTex*)subTexMap)->GetMapName();
		//			SplitPathFile( strFullName, &strPath, &strFile );		
		//		}
		//	}
		//}
	}

	m_MtlList.Append( 1, &pMtl );

	return usCount;
}

void RdMeshExp::DumpMultiTexture( StdMat2* pCurMtl, RdMaterial* pCurMaterial )
{
	assert( pCurMtl->MapEnabled(ID_DI) );

	Texmap* texMap = (BitmapTex*)pCurMtl->GetSubTexmap(ID_DI);
	assert(texMap);

	// self-illum slot -> dark map
	if (texMap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0))
	{
        Texmap *pSITm;

        assert(pCurMtl->MapEnabled(ID_SI));

        pSITm = (BitmapTex*) pCurMtl->GetSubTexmap(ID_SI);
        assert(pSITm);

        assert(pSITm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));

		// uv channel도 추가해주어야 한다.
	}

	Texmap* pSubTm = 0;
	bool bRequiresAlpha;

    // mix shader in diffuse slot --> decal
    if (texMap->ClassID() == Class_ID(MIX_CLASS_ID, 0))
	{
        bool bAlphaSet;
        int iAlphaFrom = -1;

        // we allow 3 in this assertion, but it's not actually handled
        // by the converter.
        assert(texMap->NumSubTexmaps() == 2 || texMap->NumSubTexmaps() == 3);

        bAlphaSet = false;
		bRequiresAlpha = false;

		for (int i=0; i<2; ++i)
		{
            pSubTm = texMap->GetSubTexmap(i);
            assert(pSubTm);
            assert(pSubTm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));

		}
	}
    // composite shader in diffuse slot --> glow
    else if (texMap->ClassID() == Class_ID(COMPOSITE_CLASS_ID, 0))
    {
		bool bAlphaSet;
        int iAlphaFrom = -1;

        assert(texMap->NumSubTexmaps() == 2);

        bAlphaSet = false;
        bRequiresAlpha = false;
		
		for (int i=0; i<2; ++i)
		{
            pSubTm = texMap->GetSubTexmap(i);
            assert(pSubTm);
            assert(pSubTm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));

		}
	}
    // RGBmultiply shader in diffuse slot --> dark map
    else if (texMap->ClassID() == Class_ID(RGBMULT_CLASS_ID, 0))
    {
		int iAlphaFrom = -1;
        //if (texMap->alphaFrom == 0 || texMap->alphaFrom == 1)
        //    iAlphaFrom = pMult->alphaFrom;

        assert(texMap->NumSubTexmaps() == 2);

        bRequiresAlpha = false;
        for (int i = 0; i < 2; i++)
        {
            pSubTm = texMap->GetSubTexmap(i);
            assert(pSubTm);
            assert(pSubTm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));
		}
	}
}


void RdMeshExp::NodeCleanUp()
{
	if( m_phyMod && m_phyExport )
	{
		if( m_pcExport )
		{
			m_phyExport->ReleaseContextInterface( m_pcExport );
			m_pcExport = NULL;
		}
		m_phyMod->ReleaseInterface( I_PHYINTERFACE, m_phyExport );
		m_phyExport = NULL;
		m_phyMod = NULL;
	}

	if( m_skinMod && m_skinContext )
	{
		m_skinMod->ReleaseInterface( I_SKIN, m_skinInterface );
		m_skinContext = NULL;
		m_skinInterface = NULL;
		m_skinMod = NULL;
	}
}

//------------------------------------------------------------------
bool RdMeshExp::CollectFaceNormNode( INode* pNode, RdNormFaceNode* pNodeData )
{
	// 노드가 메쉬 데이터를 가지고 있는지 확인
	ObjectState os = pNode->EvalWorldState( 0 );
	Object* pObj = os.obj;
	TriObject* pTriObj;
	BOOL bConvertedToTriObject = ( pObj->CanConvertToType( triObjectClassID ) ) && 
		( ( pTriObj = (TriObject*)pObj->ConvertToType( 0, triObjectClassID ) ) != NULL );

	if( !bConvertedToTriObject )
		return false;

	Mesh* pMesh = &pTriObj->GetMesh();
	if( NULL == pMesh || pMesh->getNumVerts() == 0 || pMesh->getNumFaces() == 0 )
		return false;

	// 미러된 건지 확인
	Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
	bool bNeg = world.Parity() ? true : false;
	
//	world.NoScale();

	Tab<RdNormFace> faces;
	Tab<Point3> orgverts;

	ulong ulFaceCnt = pMesh->getNumFaces();
	ulong ulOrgVtxCnt = pMesh->getNumVerts();
	
	orgverts.SetCount( ulOrgVtxCnt );
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;
	bWritten.SetSize( ulOrgVtxCnt );
	bWritten.ClearAll();
	
	RdNormFace tmpface;
	int nIndex;
	Point3 pos;
	Face face;

	// 버텍스 정보 집어넣기
	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face.v[j];
			tmpface.Index[j] = nIndex;

			if( !bWritten[nIndex] )
			{
				pos = world * pMesh->verts[nIndex];
				bWritten.Set( nIndex );
				orgverts[nIndex] = pos;
			}
		}
		faces[i] = tmpface;
	}

	// Vertex부터 세팅되어야 노멀을 구할 수 있당.
	pNodeData->SetVertexList( orgverts );
	pNodeData->SetFaceList( faces, bNeg );

	return true;
}

//------------------------------------------------------------------
void RdMeshExp::CollectBBoxData( INode* pNode )
{
	if( IsBoundingBoxNode( pNode->GetName() ) )
	{
		INode* pParent = pNode->GetParentNode();
		if( pParent && !pParent->IsRootNode() )
		{
			TCHAR* parentnodename = pParent->GetName();
			RdMeshNode* pMeshNode = GetNode( parentnodename );
			if (pMeshNode)
			{
				RdBBox* pBoxNode = new RdBBox();

				ushort idx = GetIndexOfNode( pParent );

				pBoxNode->SetParentNode( idx, parentnodename );

		//		RdMeshNode* pNode = GetNode( idx );
				
				pBoxNode->SetParentNodeXform( pMeshNode->GetWorldTransform() );

				ObjectState os = pNode->EvalWorldState( 0 );

				Object* pObj = os.obj;
				TriObject* pTriObj;
				BOOL bConvertedToTriObject = ( pObj->CanConvertToType( triObjectClassID ) ) && 
					( ( pTriObj = (TriObject*)pObj->ConvertToType( 0, triObjectClassID ) ) != NULL );

				if( !bConvertedToTriObject )
				{
					delete (pBoxNode);
					return;
				}

				Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
				Mesh* pMesh = &pTriObj->GetMesh();
				if( NULL == pMesh || pMesh->getNumVerts() == 0 || pMesh->getNumFaces() == 0 )
				{
					delete (pBoxNode);
					return;
				}

				Point3 pos;
				int nCount = pMesh->getNumVerts();
				for( int i = 0; i < nCount; ++i )
				{
					pos = world * pMesh->verts[i];
					pBoxNode->AddVertex( Point3( pos.x, pos.z, pos.y ) );
				}

				pBoxNode->CalculateOBB();

				m_BBoxList.Append( 1, &pBoxNode );
			} // if (pMeshNode)
		} // if (pParent...)
	} // if (IsBoundingBox...)

	int nChildNum = pNode->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectBBoxData( pNode->GetChildNode(i) );
	}
}

//------------------------------------------------------------------
RdMeshNode* RdMeshExp::GetNode( ushort usIndex )
{
	int nCount = m_NodeList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		if( m_NodeList[i]->GetNodeID() == usIndex )
			return m_NodeList[i];
	}

	return NULL;
}

//------------------------------------------------------------------
RdMeshNode* RdMeshExp::GetNode( const TCHAR* strName )
{
	int nCount = m_NodeList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		if( _tcscmp( m_NodeList[i]->GetNodeName(), strName ) == 0  )
			return m_NodeList[i];
	}

	return NULL;
}

//------------------------------------------------------------------
void RdMeshExp::UpdateMaterial()
{
	int num = SendDlgItemMessage( m_hMeshExpDlg, IDC_ALPHA_LIST, LB_GETCOUNT, 0, 0 );

	if( num != 0 )
	{
		ushort usCount = m_MtlList.Count();
		TCHAR str[128];

		for( ushort i = 0; i < num; ++i )
		{	
			SendDlgItemMessage( m_hMeshExpDlg, IDC_ALPHA_LIST, LB_GETTEXT, i, (LPARAM)str );
			for( ushort j = 0; j < usCount; ++j )
			{
				if( _tcscmp( m_MtlList[j]->GetMaterialName(), str ) == 0 )
				{
					m_MtlList[j]->SetAlphablending( 1 );
				}
			}
		}
	}
}

//------------------------------------------------------------------
bool EnableIncludeList( RdVertex& vert, Tab<ushort>& boneList )
{
	int nCnt = boneList.Count();
	BYTE nLink = vert.Link;
	if( nLink > 4 )
		nLink = 4;

	if( nCnt > 22 )
	{
		for( BYTE a = 0; a < nLink; ++a )
		{
			int i = 0;
			for( i = 0; i < nCnt; ++i )
			{
				if( boneList[i] == vert.Blend[a].BoneID )
					break;
			}

			if( i == nCnt )
				return false;
		}
	}
	else
	{
		int addcnt = nCnt;

		for( BYTE a = 0; a < nLink; ++a )
		{
			int i = 0;
			for( i = 0; i < nCnt; ++i )
			{
				if( boneList[i] == vert.Blend[a].BoneID )
					break;
			}

			if( i == nCnt )
				++addcnt;
		}

		if( addcnt > 22 )
			return false;
	}

	return true;
}

void CopyMeshNode( RdMeshNode* pScr, RdMeshNode* pDest )
{
	pDest->SetParentNode( pScr->GetParentNodeID(), pScr->GetParentNodeName() );
	pDest->SetWorldTransform( pScr->GetWorldTransform() );
	pDest->SetLocalPosition( pScr->GetLocalPosition() );
	pDest->SetLocalRotation( pScr->GetLocalRotation() );
	pDest->SetLocalScale( pScr->GetLocalScale() );
}

void CopyVertex( RdVertex* pVert, RdVertex& vert )
{
	vert.Pos = pVert->Pos;
	vert.Normal = pVert->Normal;
	vert.Tangent = pVert->Tangent;
	vert.Binormal = pVert->Binormal;
	vert.UV.U = pVert->UV.U;
	vert.UV.V = pVert->UV.V;
	vert.UV2.U = pVert->UV2.U;
	vert.UV2.V = pVert->UV2.V;
	vert.Link = pVert->Link;

	BYTE n = vert.Link;
	if( n > 4 )
		n = 4;

	for( BYTE i = 0; i < n; ++i )
	{
		vert.Blend[i].BoneID = pVert->Blend[i].BoneID;
		vert.Blend[i].Weight = pVert->Blend[i].Weight;
	}
}

void AddBoneID( RdVertex* pVert, Tab<ushort>& bonelist )
{
	BYTE nLink = pVert->Link;
	if( nLink > 4 )
		nLink = 4;

	for( BYTE a = 0; a < nLink; ++a )
	{
		int nCount = bonelist.Count();

		int b = 0;
		for( b = 0; b < nCount; ++b )
		{
			if( bonelist[b] == pVert->Blend[a].BoneID )
				break;
		}

		if( b == nCount )
		{
			bonelist.Append( 1, &(pVert->Blend[a].BoneID) );
		}
	}
}

//------------------------------------------------------------------
void RdMeshExp::SeperateMeshNode( RdMeshNode* pOrigin )
{
	Tab<RdFace*> oflist = pOrigin->GetFaceList();
	Tab<RdVertex*> ovlist = pOrigin->GetVertexList();

	// 원본 페이스 값을 복사한다.
	Tab<RdFace> srcflist;
	srcflist.ZeroCount();
	RdFace tmpFace;

	int nFaceCnt = pOrigin->GetFaceCount();
	for( int i = 0; i < nFaceCnt; ++i )
	{
		tmpFace.Index[0] = oflist[i]->Index[0];
		tmpFace.Index[1] = oflist[i]->Index[1];
		tmpFace.Index[2] = oflist[i]->Index[2];

		tmpFace.MatID = oflist[i]->MatID;

		srcflist.Append( 1, &tmpFace );
	}

	// 분리 시작
	Tab<RdVertex> tmpverts;
	Tab<ushort> bonelist;
	Tab<RdFace> tmpflist;
	
	bool bCreate = true;
	RdMeshNode* pNewNode = NULL;
	ushort usID = 2000;
	TCHAR strName[MAX_LEN];

	int idx = 0;
	int nRemainCount = srcflist.Count();

	RdVertex vert, tmpvert;

	do
	{
		if( bCreate )
		{
			++usID;
			memset( strName, 0, sizeof(TCHAR)*MAX_LEN );			
			_stprintf( strName, "%s_%d", pOrigin->GetNodeName(), usID );
			pNewNode = new RdMeshNode( 0, usID, strName );
			CopyMeshNode( pOrigin, pNewNode );

			tmpverts.ZeroCount();
			bonelist.ZeroCount();
			tmpflist.ZeroCount();
			bCreate = false;

			idx = 0;
		}

		// 한 페이스씩 돈다.
		tmpFace.MatID = srcflist[idx].MatID;

		tmpFace.Index[0] = srcflist[idx].Index[0];
		tmpFace.Index[1] = srcflist[idx].Index[1];
		tmpFace.Index[2] = srcflist[idx].Index[2];

		int a = 0;
		for( a = 0; a < 3; ++a )
		{
			// 페이스를 구성하는 버텍스 3개를 다 검사하여 기존 부분에 넣을 수 있는지 판단한다.
			CopyVertex( ovlist[tmpFace.Index[a]], vert );
			if( !EnableIncludeList( vert, bonelist ) )
				break;
		}

		if( a == 3 )
		{
			// 새로 넣어...
			for( int k = 0; k < 3; ++k )
			{
				CopyVertex( ovlist[tmpFace.Index[k]], vert );
				ulong ulVertCnt = tmpverts.Count();
				
				ulong b = 0;
				for( b = 0; b < ulVertCnt; ++b )
				{
					tmpvert = tmpverts[b];

					if (tmpvert == vert)
						break;
					//if( Point3Equal( tmpvert.Pos, vert.Pos ) && 
					//	FloatEqual( tmpvert.UV.U, vert.UV.U ) &&
					//	FloatEqual( tmpvert.UV.V, vert.UV.V ) &&
					//	ComparePhysic( tmpvert, vert ) ) 
					//	break;			
				}

				if( b < ulVertCnt )
				{
					tmpFace.Index[k] = b;
				}
				else
				{
					tmpFace.Index[k] = ulVertCnt;
					tmpverts.Append( 1, &vert );
					AddBoneID( &vert, bonelist );
				}
			}
			tmpflist.Append( 1, &tmpFace );
			nRemainCount = srcflist.Delete( idx, 1 );	
		}
		else 
		{
			// 못 넣는 넘....
			++idx;
		}

		if( idx == srcflist.Count() )
		{
			pNewNode->SetVertexList( tmpverts, false );
			pNewNode->SetFaceList( tmpflist, true );

			//@<add : 2009.06.11
			pNewNode->SetEnableVertexColor( pOrigin->GetEnableVertexColor() );
			pNewNode->UseNormalMap( pOrigin->HasNormalMap() );

			pNewNode->NotifyComplete();
			m_NodeList.Append( 1, &pNewNode );
			bCreate = true;
		}

	}while( 0 != nRemainCount );
}

//------------------------------------------------------------------
/**
*/
void RdMeshExp::ComputeVertexNormals( Mesh* pMesh,
									  bool bNeg,
									  const Matrix3& matWorld,
									  Tab<VNormal>& rVNorms,
									  Tab<Point3>& rFNorms )
{
	Face* face = NULL;
	Point3* vertices = NULL;
	Point3 v0, v1, v2;

	ulong ulVertCount = pMesh->getNumVerts();
	ulong ulFaceCount = pMesh->getNumFaces();

	face = pMesh->faces;
	vertices = pMesh->verts;
	rVNorms.SetCount( ulVertCount );
	rFNorms.SetCount( ulFaceCount );

	//! 1. Initialize
	ulong i = 0;
	for( i = 0; i < ulVertCount; ++i )
		rVNorms[i] = VNormal();

	//! 2. 버텍스의 노말값 추가.
	for( i = 0; i < ulFaceCount; ++i, face++ )
	{
		//@cagetu -09/09/29 : 여기를 잘 확인해보자!!!!
		//v0 = vertices[face->v[0]];
		//v1 = vertices[face->v[1]];
		//v2 = vertices[face->v[2]];
		if( bNeg )
		{
			v0 = vertices[face->v[0]];
			v1 = vertices[face->v[1]];
			v2 = vertices[face->v[2]];
		}
		else
		{
			v0 = vertices[face->v[0]];
			v1 = vertices[face->v[2]];
			v2 = vertices[face->v[1]];
		}

		rFNorms[i] = (v1-v0) ^ (v2-v1);

		for( int j = 0; j < 3; ++j )
		{
			rVNorms[face->v[j]].AddNormal( rFNorms[i], face->smGroup );
		}

		rFNorms[i] = Normalize(rFNorms[i]);
		Point3 faceNormal = pMesh->getFaceNormal(i);
	}

	//! 3. 버텍스 노말 계산
	for( i = 0; i < ulVertCount; ++i )
		rVNorms[i].Normalize();
}

#ifdef NEW_EXPORT_TANGENT
//------------------------------------------------------------------------------
/**
	1. 먼저, 접선공간에서 오브젝트 공간으로 변환하는 행렬을 만든다.
		1. 삼각형 꼭지점 P0, P1, P2로 부터 T와 B 법선벡터들(정규화되지 않았음)을 구하는 공식을 얻는다.
		2. 특정한 하나의 정점에서의 접선벡터를 얻으려면 그 정점을 공유하는 모든 삼각형들의 접선들의 평균을 구해야 한다.
		   (이는 정점의 법선벡터를 얻는 방식과 동일하다.)
		   만일 인접한 삼각형이 다른 텍스쳐 매핑을 사용한다면, 어차피 매핑 좌표들이 다르므로 경계선에 있는 정점들은 이미
		   중복되어 있을 것이다. 그런 삼각형들을 접선 평균 계산에 포함시키면 각 삼각형에 대한 범프 맵에 방향이 제대로 표현되지
		   않으므로 제외시켜야 한다.
	    3. 정점에 대한 법선벡터 N과 접선벡터 T와 B를 얻었다면, 접선 공간에서 오브젝트 공간으로 변환하는 행렬을 만든다.
	2. 생성된 변환의 역행렬로, 오브젝트 공간에서 접선공간으로 변환 행렬을 만든다.
*/
void RdMeshExp::ComputeVertexTangents( RdMeshNode* pNodeData, Tab<RdVertex> &rVertices, Tab<RdFace> &rFaces, bool bNeg )
{
	// http://www.terathon.com/code/tangent.html
	// B' = (u1-u0)(x2-x0) - (u2-u0)(x1-x0)
	// T = B' * N
	// B = T * N
	Matrix3 inv = Inverse( pNodeData->GetWorldTransform() );

    Point3* tan1 = new Point3[rVertices.Count() * 2];
    Point3* tan2 = tan1 + rVertices.Count();
    ZeroMemory(tan1, rVertices.Count() * sizeof(Point3) * 2);

	/**	Triangle Tangent 구하기
	*/
	long numTriangles = rFaces.Count();
	for (long triIndex = 0; triIndex < numTriangles; ++triIndex)
	{
		RdFace* face = &rFaces[triIndex];

		long i1, i2, i3;
		i1 = face->Index[0];
		i2 = face->Index[1];
		i3 = face->Index[2];
		//if (bNeg)
		//{
		//	i1 = face->Index[0];
		//	i2 = face->Index[1];
		//	i3 = face->Index[2];
		//}
		//else
		//{
		//	i1 = face->Index[0];
		//	i2 = face->Index[2];
		//	i3 = face->Index[1];
		//}

		RdVertex* vertex1 = &rVertices[i1];
		RdVertex* vertex2 = &rVertices[i2];
		RdVertex* vertex3 = &rVertices[i3];

		/// position
		//Point3 worldPos1 = Point3( vertex1->Pos.x, vertex1->Pos.z, vertex1->Pos.y );
		//Point3 worldPos2 = Point3( vertex2->Pos.x, vertex2->Pos.z, vertex2->Pos.y );
		//Point3 worldPos3 = Point3( vertex3->Pos.x, vertex3->Pos.z, vertex3->Pos.y );
		const Point3& worldPos1 = vertex1->Pos;
		const Point3& worldPos2 = vertex2->Pos;
		const Point3& worldPos3 = vertex3->Pos;

		const Point3 localV1 = inv * worldPos1; // = worldPos1;	//
		const Point3 localV2 = inv * worldPos2; // = worldPos2;	//
		const Point3 localV3 = inv * worldPos3; // = worldPos3;	//

		// compute tangents
        float x1 = localV2.x - localV1.x;
        float x2 = localV3.x - localV1.x;
        float y1 = localV2.y - localV1.y;
        float y2 = localV3.y - localV1.y;
        float z1 = localV2.z - localV1.z;
        float z2 = localV3.z - localV1.z;

        float s1 = vertex2->UV.U - vertex1->UV.U;
        float s2 = vertex3->UV.U - vertex1->UV.U;
        float t1 = vertex2->UV.V - vertex1->UV.V;
        float t2 = vertex3->UV.V - vertex1->UV.V;

        float l = (s1 * t2 - s2 * t1);
        // catch singularity
        //if (l == 0.0f)
        //{
        //    l = 0.0001f;
        //}
		if ( fabs(l) <= 1E-6f)
			continue;

        float r = 1.0f / l;
        Point3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
        Point3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;

		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
	}

	/**	
		<접선 공간->오브젝트 공간 변환>
		| Tx, Bx, Nx |
		| Ty, By, Ny |
		| Tz, Bz, Nz |

		<오브젝트 공간->접선 공간 변환>
		|  Tx', Ty', Tz' |
		|  Bx', By', Bz' |
		|  Nx,  Ny,  Nz  |
	*/

	for (long v=0; v<rVertices.Count(); v++)
	{
        const Point3& n = rVertices[v].Normal;
		//const Point3& n = Point3(rVertices[v].Normal.x, rVertices[v].Normal.z, rVertices[v].Normal.y);
        const Point3& t = tan1[v];

		// Gram-Schmidt orthogonalize
		Point3 invTangent = t - n * DotProd(n,t);
		invTangent = ::Normalize(invTangent);//invTangent.Normalize();
		// Calculate handedness
		float h = (DotProd(CrossProd(n,t), tan2[v]) < 0.0f) ? -1.0f : 1.0f;
		Point3 invBinormal = CrossProd(n, invTangent) * h;
		invBinormal = ::Normalize(invBinormal);//invBinormal.Normalize();

		rVertices[v].Tangent = invTangent;
		rVertices[v].TangentW = h;
		rVertices[v].Binormal = invBinormal;
	}

	delete[] tan1;
}
#else
//------------------------------------------------------------------------------
/**
*/
void RdMeshExp::ComputeVertexTangents( RdMeshNode* pNodeData, Tab<RdVertex> &rVertices, Tab<RdFace> &rFaces )
{
	// http://www.terathon.com/code/tangent.html
	// B' = (u1-u0)(x2-x0) - (u2-u0)(x1-x0)
	// T = B' * N
	// B = T * N
	Matrix3 inv = Inverse( pNodeData->GetWorldTransform() );
	
	Point3 binormal, tangent;

	//	Triangle Tangent 구하기
	int numTriangles = rFaces.Count();
	for (int triIndex = 0; triIndex < numTriangles; ++triIndex)
	{
		RdFace* face = &rFaces[triIndex];

		RdVertex* vertex0 = &rVertices[ face->Index[0] ];
		RdVertex* vertex1 = &rVertices[ face->Index[1] ];
		RdVertex* vertex2 = &rVertices[ face->Index[2] ];

		// vertexPos
		const Point3 localV0 = inv * (vertex0->Pos);
		const Point3 localV1 = inv * (vertex1->Pos);
		const Point3 localV2 = inv * (vertex2->Pos);

		// compute face normal
		Point3 v0 = localV1 - localV0;
		Point3 v1 = localV2 - localV0;
		Point3 normal = CrossProd(v0, v1);
		normal.Normalize();

		//@mod by cagetu 09.5.8 : Legacy
		RdUV uv0 = vertex1->UV - vertex0->UV;
		RdUV uv1 = vertex2->UV - vertex0->UV;

		//binormal = (v0 * uv1.U) - (v1 * uv0.U);
		binormal = -(v0 * uv1.U) + (v1 * uv0.U);
		binormal.Normalize();

		//tangent = (v0 * uv1.V) - (v1 * uv0.V);
		tangent = CrossProd(binormal, normal);
		tangent.Normalize();

		binormal = CrossProd(normal, tangent);
		binormal.Normalize();

		Point3 tangentCross = CrossProd(tangent, binormal);
		if (DotProd(tangentCross, normal) < 0.0f)
		{
			tangent = -tangent;
			binormal = -binormal;
		}
		
		face->Normal = normal;
		face->Binormal = binormal;
		face->Tangent = tangent;

		//vertex0->Binormal += binormal;
		//vertex1->Binormal += binormal;
		//vertex2->Binormal += binormal;
		vertex0->Tangent += tangent;
		vertex1->Tangent += tangent;
		vertex2->Tangent += tangent;
	}

	for (int v=0; v < rVertices.Count(); v++)
	{
		rVertices[v].Tangent.Normalize();
		rVertices[v].Binormal = CrossProd(rVertices[v].Normal, rVertices[v].Tangent).Normalize();
	}
	//const bool split = false;
	//if (split)
	//{
	//	ComputeVertexTangentsWithSplits( rVertices, rFaces );
	//}
	//else
	//{
	//	ComputeVertexTangentsWithoutSplits( rVertices, rFaces );
	//}
}
#endif	// NEW_EXPORT_TANGENT

//------------------------------------------------------------------
struct qSortData
{
	Tab<RdVertex>* vertices;
	Tab<RdFace>* faces;

	int wantedVertComponent;
};
static qSortData* g_qSortData = 0;

int __cdecl VertexSorter(const void* elm0, const void* elm1)
{
    qSortData* sortData = g_qSortData;
    int i0 = *(int*)elm0;
    int i1 = *(int*)elm1;
	RdVertex& v0 = (*sortData->vertices)[i0];
	RdVertex& v1 = (*sortData->vertices)[i1];
    return v0.Compare(v1, sortData->wantedVertComponent);
}

//------------------------------------------------------------------
/**
*/
void RdMeshExp::CleanupMesh( Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces, Tab<Collapse>& CollapseMap, int targetVertComponent )
{
#pragma TODO("[09.5.8]	Split하는 부분을 다시 확인해봐야 한다...")

	int numVert = rVertices.Count();

    // generate a index remapping table and sorted vertex array
	int* indexMap = new int[numVert];
	int* sortMap = new int[numVert];
	int* shiftMap = new int[numVert];

	for (int i=0; i<numVert; ++i)
	{
		indexMap[i] = i;
		sortMap[i] = i;
	}

    // generate a sorted index map (sort by 원하는 설정에 따라 )
	qSortData tmpSortData;
		tmpSortData.vertices = &rVertices;
		tmpSortData.faces = &rFaces;
		tmpSortData.wantedVertComponent = targetVertComponent;
	g_qSortData = &tmpSortData;

	qsort(sortMap, numVert, sizeof(int), VertexSorter);

	g_qSortData = 0;

	// 여분의 버텍스들에 대한 정렬된 array를 검색한다.
	for (int baseIndex=0; baseIndex< (numVert-1);)
	{
		int nextIndex = baseIndex+1;
		while ( (nextIndex < numVert) && 
				(1 == rVertices[sortMap[baseIndex]].Compare(rVertices[sortMap[nextIndex]], targetVertComponent)) )
		{
			// mark the vertex as invalid
			rVertices[sortMap[nextIndex]].Redundant = true;

			// index remapping table에 새로운 유효한 인덱스를 넣는다.
			indexMap[sortMap[nextIndex]] = sortMap[baseIndex];
			nextIndex++;
		}
		baseIndex = nextIndex;
	}

	// shiftMap을 채운다, this contains for each vertex index the number
    // of invalid vertices in front of it
	int numInvalid = 0;
	for (int vertIndex = 0; vertIndex < numVert; vertIndex++)
	{
		if (true == rVertices[vertIndex].Redundant)
		{
			numInvalid++;
		}
		shiftMap[vertIndex] = numInvalid;
	}

    // fix the triangle's vertex indices, first, remap the old index to a
    // valid index from the indexMap, then decrement by the shiftMap entry
    // at that index (which contains the number of invalid vertices in front
    // of that index)
    // fix vertex indices in triangles
	int numFaces = rFaces.Count();
	for (int face=0; face<numFaces; ++face)
	{
		RdFace* t = &rFaces[face];
		for (int i=0; i<3; ++i)
		{
			int newIndex = indexMap[t->Index[i]];
			t->Index[i] = newIndex - shiftMap[newIndex];
		}
	}

    // initialize the collapse map so that for each new (collapsed)
    // index it contains a list of old vertex indices which have been
    // collapsed into the new vertex
	if (CollapseMap.Count() > 0)
	{
		for (int i=0; i<numVert; i++)
		{
			int newIndex = indexMap[i];
			int collapsedIndex = newIndex - shiftMap[newIndex];
			//CollapseMap[collapsedIndex].indexMap.push_back( i );
			CollapseMap[collapsedIndex].indexMap.Append( 1, &i );
		}
	}

	// finally, remove the redundant vertices
	numVert = rVertices.Count();
	Tab<RdVertex> newArray;
	//newArray.SetCount(numVert);
	newArray.SetCount(0);

	int cur = 0;
	for (int vertIndex=0; vertIndex < numVert; ++vertIndex)
	{
		if (false == rVertices[vertIndex].Redundant)
		{
			newArray.Append(1, &rVertices[vertIndex]);
		}
	}
	rVertices = newArray;

	delete[] indexMap;
	delete[] sortMap;
	delete[] shiftMap;
}

//------------------------------------------------------------------------------
/**	vertex split를 한다면, mirrored된 uv edge들에 대해서 더 나은 결과를 얻을 수 있을 것이다.
	하지만, vertex 갯수의 변화가 있을 수 있으므로, blendshape들에 대해서는 문제가 있을 수 있다.
*/
void RdMeshExp::ComputeVertexTangentsWithSplits( Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces )
{
	int numVerts = rVertices.Count();

	Tab< Collapse > collapseMap;
	collapseMap.SetCount( 0 );

	for (int i=0; i<numVerts; ++i)
	{
		Collapse collapse;
		collapseMap.Append( 1, &collapse );
	}

	Tab<RdVertex> newVertics = rVertices;
	Tab<RdFace> newFaces = rFaces;
	CleanupMesh( newVertics, newFaces, collapseMap, RdVertex::POSITION | RdVertex::NORMAL );

    // create a connectivity map, which contains for each vertex
    // the triangle indices which use this vertex
	TriVertGroup* vertexTriangleArray = new TriVertGroup[newVertics.Count()];
	{
		// 
		int numTriangles = newFaces.Count();
		for (int triIndex=0; triIndex<numTriangles; ++triIndex)
		{
			RdFace* face = &newFaces[triIndex];

			for (int v=0; v<3; ++v)
			{
				TriVertGroup* triVertGroup = &vertexTriangleArray[face->Index[v]];

				if (0 == triVertGroup->triIndices)
					triVertGroup->triIndices = new int[numTriangles];

				triVertGroup->triIndices[triVertGroup->size] = triIndex;
				triVertGroup->size++;
			}
		}
	}

	// compute averaged vertex tangents
	Tab<Point3> averagedTangents;
	Tab<Point3> averagedBinormals;
	averagedTangents.SetCount( numVerts );
	averagedBinormals.SetCount( numVerts );

	Point3 avgTangent;
	Point3 avgBinormal;
	for (int vertIndex=0; vertIndex<newVertics.Count(); ++vertIndex)
	{
		avgTangent.Set(0.0f, 0.0f, 0.0f);
		avgBinormal.Set(0.0f, 0.0f, 0.0f);

		int numVertTris = vertexTriangleArray[vertIndex].size;
		for (int vertTriIndex=0; vertTriIndex<numVertTris; ++vertTriIndex)
		{
			RdFace* face = &newFaces[ vertexTriangleArray[vertIndex].triIndices[vertTriIndex] ];
			avgTangent += face->Tangent;
			avgBinormal += face->Binormal;
		}
		avgTangent.Normalize();
		avgBinormal.Normalize();

		//averagedTangents[vertIndex] = avgTangent;
		//averagedBinormals[vertIndex] = avgBinormal;

		int size = (int)collapseMap[vertIndex].indexMap.Count();
		for (int i=0; i<size; ++i)
		{
			int index = collapseMap[vertIndex].indexMap[i];
			averagedTangents[index] = avgTangent;
			averagedBinormals[index] = avgBinormal;
		}
	}

	// fill tangents by deciding for each vertex whether to
	// use the triangle tangent or the averaged tangent this is
	// done by comparing the averaged and the triangle vertex,
	// if they are close enough to each other, the averaged tangent
	// is used
	int numFaces = rFaces.Count();
	for (int faceIndex = 0; faceIndex < numFaces; ++faceIndex)
	{
		const Point3& triTangent = rFaces[faceIndex].Tangent;
		const Point3& triBinormal = rFaces[faceIndex].Binormal;

		for (int i=0; i<3; ++i)
		{
			//int vertIndex = faceIndex * 3 + i;
			int vertIndex = rFaces[faceIndex].Index[i];

			const Point3& avgTangent = averagedTangents[vertIndex];
			const Point3& avgBinormal = averagedBinormals[vertIndex];

			if ((0 == Point3Compare(triTangent, avgTangent, 1.0f)) &&
				(0 == Point3Compare(triBinormal, avgBinormal, 1.0f)))
			{
				// use averaged tangent for this vertex
				rVertices[vertIndex].Tangent = avgTangent;
				rVertices[vertIndex].Binormal = avgBinormal;
			}
			else
			{
				// use triangle tangent for this vertex
				rVertices[vertIndex].Tangent = triTangent;
				rVertices[vertIndex].Binormal = triBinormal;
			}
		}
	}

	delete[] vertexTriangleArray;
}

//------------------------------------------------------------------------------
/**	vertex tangent 구하기
	-공유하는 tangent의 평균을 구한다.
	 특정한 하나의 정점에서의 접선벡터를 얻으려면 그 정점을 공유하는 모든 삼각형들의 접선들의 평균을 구해야 한다.
	 만일 인접한 삼각영이 다른 텍스쳐 매핑을 사용한다면, 어차피 매핑 좌표들이 다르므로, 경계선에 있는 정점들은 이미
	 중복되어 있을 것이다. 그런 삼각형들을 접선 평균 계산에 포함시키면 각 삼각형에 대한 범프 맵의 방향이 제대로 표현
	 되지 않으므로, 제외시켜야 한다.
*/
void RdMeshExp::ComputeVertexTangentsWithoutSplits( Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces )
{
	int numVerts = rVertices.Count();
	TriVertGroup* vertexTriangleArray = new TriVertGroup[ numVerts ];

	int numTriangles = rFaces.Count();
	for (int triIndex=0; triIndex<numTriangles; ++triIndex)
	{
		RdFace* face = &rFaces[triIndex];

		for (int v=0; v<3; ++v)
		{
			TriVertGroup* triVertGroup = &vertexTriangleArray[ face->Index[v] ];

			if (0 == triVertGroup->triIndices)
				triVertGroup->triIndices = new int[numTriangles];

			triVertGroup->triIndices[ triVertGroup->size ] = triIndex;
			triVertGroup->size++;
		}
	}

	for (int vertIndex=0; vertIndex<numVerts; ++vertIndex)
	{
		Point3 avgTangent(0.0f, 0.0f, 0.0f);
		Point3 avgBinormal(0.0f, 0.0f, 0.0f);

		int numVertTris = vertexTriangleArray[vertIndex].size;
		for (int vertTriIndex=0; vertTriIndex<numVertTris; ++vertTriIndex)
		{
			RdFace* face = &rFaces[ vertexTriangleArray[vertIndex].triIndices[vertTriIndex] ];
			avgTangent += face->Tangent;
			avgBinormal += face->Binormal;
		}
		avgTangent.Normalize();
		avgBinormal.Normalize();

		rVertices[vertIndex].Tangent = avgTangent;

		//@mod by cagetu 09.5.7 : binormal 변경...
		//rVertices[vertIndex].Binormal = avgBinormal;
		rVertices[vertIndex].Binormal = rVertices[vertIndex].Normal ^ rVertices[vertIndex].Tangent;
		rVertices[vertIndex].Binormal.Normalize();
	}

	delete []vertexTriangleArray;
}

//------------------------------------------------------------------
//	Smoothing group에 맞게 버텍스를 추가하기 위한 구조체
//------------------------------------------------------------------
#define MAX_SMOOTING_GROUP	128

struct SmVert
{
	int			nIndex;
	ulong		smGroup[MAX_SMOOTING_GROUP];
	int			nCount;

	SmVert()
	{
		ClearSmGroup();
	}

	bool	FindSmGroup( ulong ulSmooth )
	{
		for( int i = 0; i < nCount; ++i )
		{
			if( smGroup[i] == ulSmooth )
				return true;
		}
		return false;
	}

	void	InsertSmGroup( ulong ulSmooth )
	{
		smGroup[ nCount ] = ulSmooth;
		nCount++;
	}

	void	ClearSmGroup()
	{
		for( int i=0; i < MAX_SMOOTING_GROUP; ++i )
		{
			smGroup[i] = 0xffffffff;
		}
		nCount = 0;
	}
};

//------------------------------------------------------------------
void RdMeshExp::GetNormalVertexCount( Mesh* pMesh, Tab<RdVertex>& rVertices )
{
	Face* face;

	RdVertex tmpVert;

	int nNormals = 0;
	int nVertexCount = 0;
	int nIndex = 0;

	Tab<SmVert>	smVerts;
	smVerts.SetCount(0);

	BitArray bWritten;
	bWritten.SetSize( pMesh->getNumVerts() );
	bWritten.ClearAll();

	ulong ulSmGroup = 0;
	ulong ulFaceCnt = pMesh->getNumFaces();
	for( ulong i = 0; i < ulFaceCnt; ++i )
	{
		face = &pMesh->faces[i];
		ulSmGroup = face->getSmGroup();

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face->v[j];

			bool bInsert = true;

			// 1. 인덱스가 같은 것이 없다면, 버텍스 추가
			// 2. 인덱스가 같아도, smgroup이 다른 것이 있다면, 버텍스 추가
			if (false == bWritten[nIndex])
			{
				bWritten.Set(nIndex);

				SmVert smvert;
				smvert.nIndex = nIndex;
				smvert.InsertSmGroup( ulSmGroup );
				smVerts.Insert( smVerts.Count(), 1, &smvert );
			}
			else
			{
				/// 현재 vertex의 smootingGroup 리스트에 
				ulong ulVerts = smVerts.Count();
				for( ulong v = 0; v < ulVerts; ++v )
				{
					if( nIndex == smVerts[v].nIndex )
					{
						if( smVerts[v].FindSmGroup( ulSmGroup ) )
						{
							bInsert = false;
							break;
						}
						else
						{
							smVerts[v].InsertSmGroup( ulSmGroup );
							break;
						}
					}
				}
			}

			if (bInsert)
			{
				tmpVert.SmGroup = ulSmGroup;
				tmpVert.OrgId = nIndex;

				rVertices.Insert( rVertices.Count(), 1, &tmpVert );
			}
		}
	}
}

//------------------------------------------------------------------
/**	
	현재 버텍스 리스트에서 주어진 버텍스 인덱스로 실제 버텍스 인덱스 얻기
*/
ulong RdMeshExp::GetNormalVertexIndex( int nIndex, ulong ulSmGroup, const Tab<RdVertex>& rVertices )
{
	int nCount = rVertices.Count();
	int nId = 0;
	ulong smgroup = 0;
	for( int i = 0; i < nCount; ++i )
	{
		nId = rVertices[i].OrgId;
		smgroup = rVertices[i].SmGroup;

		if( nIndex == nId && (ulSmGroup == smgroup) )
		{
			return i;
		}
	}

	ASSERT_MBOX( FALSE, _T( "[RdMeshExp::GetNormalVertexIndex] Invalid VertexIndex" ) );

	return 0xffffffff;
}
