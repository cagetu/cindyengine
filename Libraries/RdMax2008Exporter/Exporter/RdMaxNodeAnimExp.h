#ifndef __RD_MAX_NODEANIMEXP_H__
#define __RD_MAX_NODEANIMEXP_H__

#pragma once

#include "..\RdMaxExporterHeader.h"

struct VecKey
{
	ushort		Frame;
	Point3		Vector;

	VecKey() : Frame( 0 )
	{
		Vector.Set( 0.0f, 0.0f, 0.0f );
	}
};

struct QuatKey
{
	ushort		Frame;
	Quat		Quaternion;

	QuatKey() : Frame( 0 )
	{
		Quaternion.Set( 0.0f, 0.0f, 0.0f, 1.0f );
	}
};

struct AnimNode
{
	ushort			ID;
	TCHAR			Name[MAX_LEN];

	Tab<VecKey*>	PosTrack;
	Tab<QuatKey*>	RotTrack;
	Tab<VecKey*>	SclTrack;

	AnimNode()
	{
		ID			= 0xffff;
		memset( Name, 0, sizeof(TCHAR)*MAX_LEN );
		PosTrack.Init();
		PosTrack.ZeroCount();

		RotTrack.Init();
		RotTrack.ZeroCount();

		SclTrack.Init();
		SclTrack.ZeroCount();
	}

	~AnimNode()
	{
		int nCount = PosTrack.Count();
		VecKey** ppKey;
		for( int i = 0; i < nCount; ++i )
		{
			ppKey = PosTrack.Addr( i );
			delete (*ppKey);
		}
		PosTrack.Delete( 0, nCount );

		nCount = RotTrack.Count();
		QuatKey** ppQKey;
		for( int i = 0; i < nCount; ++i )
		{
			ppQKey = RotTrack.Addr( i );
			delete (*ppQKey);
		}
		RotTrack.Delete( 0, nCount );

		nCount = SclTrack.Count();
		for( int i = 0; i < nCount; ++i )
		{
			ppKey = SclTrack.Addr( i );
			delete (*ppKey);
		}
		SclTrack.Delete( 0, nCount );
	}
};


//------------------------------------------------------------------------------
/**	Animation Export Dialog
*/
//------------------------------------------------------------------------------
class RdNodeAnimExp
{
public:
	RdNodeAnimExp( Interface* pInterface );
	~RdNodeAnimExp();

	void			Initialize();
	void			OutputInformation();

	void			SetUseKeys( bool bUse )				{	m_bUseKeys = bUse;	}

	void			ExportAnim( const TCHAR* strName );

protected:
	Interface*			m_pInterface;
//	HWND				m_hAnimExpDlg;

	ulong				m_ulStartTime;
	ulong				m_ulEndTime;

	ulong				m_ulTFP;
	ushort				m_usTotalFrame;
	ulong				m_ulFrameRate;

	bool				m_bUseKeys;

	Tab< AnimNode* >	m_NodeList;

	void	Clear();

	void	CollectNodeAnim( INode* pNode );
	bool	CollectKeyData( INode* pNode, Tab<VecKey*>& pos, Tab<QuatKey*>& rot, Tab<VecKey*>& scl );
	bool	GetKeyDataByFrame( INode* pNode, Tab<VecKey*>& pos, Tab<QuatKey*>& rot, Tab<VecKey*>& scl );
	void	GetKeyDataByControl( INode* pNode, Control* pControl, Tab<VecKey*>& pos, Tab<QuatKey*>& rot, Tab<VecKey*>& scl );

	bool	CheckForAnimation( INode* pNode, bool& bEnablePos, bool& bEnableRot, bool& bEnableScl );

	void	DumpNodeList( FILE* pFile );
	void	DumpPosKey( FILE* pFile, Tab<VecKey*>& pos );
	void	DumpRotKey( FILE* pFile, Tab<QuatKey*>& rot );
	void	DumpSclKey( FILE* pFile, Tab<VecKey*>& scl );
};

#endif // __RD_MAX_NODEANIMEXP_H__