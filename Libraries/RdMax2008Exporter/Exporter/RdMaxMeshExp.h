#ifndef __RD_MAX_MESHEXP_H__
#define __RD_MAX_MESHEXP_H__


#include "..\DataExp\RdMaxMesh.h"
#include "..\DataExp\RdMaxMaterial.h"
#include "..\DataExp\RdMaxUtil.h"

#include "../DataExp/RdMaxBBox.h"
#include "../Dependency/ISkin.h"

#include <vector>

//------------------------------------------------------------------------------
/**	Mesh Export Dialog
*/
//------------------------------------------------------------------------------
class RdMeshExp
{
	friend class RdStaticMeshExpUtil;
	friend class RdSkinMeshExpUtil;
public:
	enum ActiveFlags
	{
		None			= (1<<0),
		VertColor		= (1<<1),	/// 버텍스 칼라 뽑기
		VertTangent		= (1<<2),	/// 버텍스 탄젠트 뽑기
		PackUV			= (1<<3),	/// UV 압축하기
		PackNormal		= (1<<4),	/// Normal 압축
		PackTangent		= (1<<5),	/// Tangent 압축
		PackSkinWeight	= (1<<6),	/// SkinWeight 압축
	};

	RdMeshExp( Interface* pInterface, HWND hWnd, bool bSelected = false );
	~RdMeshExp();

	void			Initialize();
	void			OutputInformation();

	void			SetApplyScale( bool bApply )			{	m_bApplyScale = bApply;		}

	void			ClearFeatures()							{	m_dwActiveFlags = 0;		}
	void			ActiveFeature( unsigned int Flag )		{	m_dwActiveFlags |= Flag;	}
	bool			HasFeature( unsigned int Flag ) const	{	return (m_dwActiveFlags & Flag);	}

	void			UseVertexColor( bool bUse )				{	m_bVertexColor = bUse;		}
	bool			IsUseVertexColor() const				{	return m_bVertexColor;		}

	void			UseVertTangent( bool bUse )				{	m_bVertTangent = bUse;		}
	bool			IsUseVertTangent() const				{	return m_bVertTangent;		}

	void			PackVertex( bool bEnable )				{	m_bVertPacking = bEnable;	}
	bool			IsPackVertex() const					{	return m_bVertPacking;		}

	void			ExportMesh( const TCHAR* strName );
	void			ExportMaterial( const TCHAR* strName );
	void			ExportMtl( const TCHAR* strName );

protected:
	struct Collapse
	{
		Tab<int> indexMap;

		Collapse()
		{
			indexMap.SetCount(0);
		}
	};

	// VertexTangent를 구하기 위한 vertexGroup
	struct TriVertGroup
	{
		int size;
		int* triIndices;

		TriVertGroup() : triIndices(0), size(0) {}
		~TriVertGroup() { delete [] triIndices; triIndices = NULL; size = 0; }
	};

	//------------------------------------------------------------------------------
	//	Variables
	//------------------------------------------------------------------------------
	Interface*				m_pInterface;
	bool					m_bSelectedExp;
	HWND					m_hMeshExpDlg;

	IPhyContextExport*		m_pcExport;
	IPhysiqueExport*		m_phyExport;
	Modifier*				m_phyMod;

	Modifier*				m_skinMod;
	ISkin*					m_skinInterface;
	ISkinContextData*		m_skinContext;

	Tab<RdMeshNode*>		m_NodeList;
	Tab<RdMaterial*>		m_MtlList;	
	Tab<RdBBox*>			m_BBoxList;

	Tab<RdNormFaceNode*>	m_IndoorNodeList;
	RdNormFaceNode*			m_pHeightNode;
	
	Point3					m_ptTraceTop;
	Point3					m_ptTraceBottom;

	bool					m_bTraceTop;
	bool					m_bTraceBottom;

	bool					m_bEnableExport;
	bool					m_bDummyExport;

	bool					m_bApplyScale;

	bool					m_bVertexColor;
	bool					m_bVertTangent;
	bool					m_bVertPacking;

	DWORD					m_dwActiveFlags;	// 뽑을 때 기능 활성화

	//------------------------------------------------------------------------------
	//	Methods
	//------------------------------------------------------------------------------
	void			Clear();
	void			UpdateMaterial();

	void			CollectNode( INode* pNode );

	void			CollectNodeData( INode* pNode, RdMeshNode* pNodeData, bool bNeg, bool bForce = false );
	void			CollectMeshNode( INode* pNode, RdMeshNode* pNodeData );

	void			CheckSkinnedType( INode* pNode );

	void			CollectMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, IPhyContextExport* pExport, ISkinContextData* pSkinExp, bool bNeg );
	void			CollectPhyMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, IPhyContextExport* pExport, bool bNeg );
	void			CollectSkinMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, ISkinContextData* pExport, bool bNeg );
	void			CollectStaticMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, bool bNeg );

	void			CollectMaterialData( Mtl* pNodeMtl, RdMeshNode* pNodeData, bool bMultiMtl );

	bool			CollectFaceNormNode( INode* pNode, RdNormFaceNode* pNodeData );

	// Bounding Box
	void			CollectBBoxData( INode* pNode );

	// Material
	ushort			AddMaterial( Mtl* pParentMtl, Mtl* pNodeMtl );
	bool			HasSameMaterial( ushort usMtlID, int nNumMtls, int nCurrentMtl );

	// Texture
	void			DumpMultiTexture( StdMat2* pCurMtl, RdMaterial* pCurMaterial );

	// 이함수 수정했다. 페이스 뒤집어 지던 넘들 땜에... 근데 왜 여태까지 큰 문제가 안됐었는지 몰겠다..-_-;; 
	void			SeperateMeshNode( RdMeshNode* pOrigin );

	void			NodeCleanUp();

	/** Vertex Normal을 계산합니다.
		@remarks
			discreet의 tech 문서에 나와있는 대로 구현.
	*/
	void			ComputeVertexNormals( Mesh* pMesh, bool bNeg, const Matrix3& matWorld, Tab<VNormal>& rVNorms, Tab<Point3>& rFNorms );

	/** Smooting Group을 이용한 노말 구하기
	*/
	static void		GetNormalVertexCount( Mesh* pMesh, Tab<RdVertex>& rVertices );
	static ulong	GetNormalVertexIndex( int nIndex, ulong ulSmGroup, const Tab<RdVertex>& rVertices );

	/**	VertexTangent 및 Binormal을 계산한다.
	*/
	void			ComputeVertexTangents( RdMeshNode* pNodeData, Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces, bool bNeg );
	void			ComputeVertexTangentsWithSplits( Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces );
	void			ComputeVertexTangentsWithoutSplits( Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces );

	/**
	*/
	void			CleanupMesh( Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces, Tab<Collapse>& CollapseMap, int targetVertComponent );

	/** Flags
	*/
	bool			IsHeightNode( const TCHAR* strName );
	bool			IsIndoorNode( const TCHAR* strName );
	bool			IsMeshDummyNode( const TCHAR* strName );
	bool			IsTraceTopNode( const TCHAR* strName );
	bool			IsTraceBottomNode( const TCHAR* strName );

	/**	Write
	*/
	void			DumpNodeList( FILE* pFile );

	void			DumpMeshNode( FILE* pFile, RdMeshNode* pNode );
	void			DumpSkinData( FILE* pFile, RdMeshNode* pNode );
	void			DumpVertexData( FILE* pFile, RdMeshNode* pNode, bool bPhysic );
	void			DumpFaceData( FILE* pFile, RdMeshNode* pNode );
	void			DumpMaterialData( FILE* pFile, RdMaterial* pMtl );

	void			DumpNormFaceNode( FILE* pFile, RdNormFaceNode* pNode );

//	void			DumpMeshData( FILE* pFile );
	void			DumpBBox( FILE* pFile );

	/**	Node 얻기
	*/
	RdMeshNode*		GetNode( ushort usIndex );
	RdMeshNode*		GetNode( const TCHAR* strName );

	//
	void			SaveLog();
};

#endif // __RD_MAX_MESHEXP_H__