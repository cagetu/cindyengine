
#pragma once

#define TO_BONE					_T("Bip01")		// Bip01로 시작하는 메쉬를 그냥 본으로 처리한다.

#define MESH_TO_BONE_U			_T("DB_")		// BD_ 도 사용가능. 메쉬 노드 중에, 본으로 사용해야 하는 녀석들..
#define MESH_TO_BONE_L			_T("db_")

#define DUMMY_TO_BONE_U			_T("BD_")		// DB_ 도 사용가능. 더미 노드 중에, 본으로 사용해야 하는 녀석들..
#define DUMMY_TO_BONE_L			_T("bd_")

#define CUSTOM_BOUNDINGBOX_U	_T("BO_")		// BO_ 도 사용가능. 바운딩 박스로 사용할 메쉬 노드
#define CUSTOM_BOUNDINGBOX_L	_T("bo_")



