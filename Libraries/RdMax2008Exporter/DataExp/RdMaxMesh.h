#ifndef __RD_MAX_MESH_H__
#define __RD_MAX_MESH_H__

#pragma once

#include "RdMaxNode.h"

struct RdUV
{
	float U;
	float V;

	RdUV() : U( 0.0f ), V( 0.0f )	{}
	RdUV( float u, float v )	{ U = u;	V = v; }

	inline RdUV operator -(const RdUV&) const;
};
inline 
RdUV RdUV::operator-(const RdUV& b) const {
	return(RdUV(V-b.U,V-b.V));
	}

struct RdVertexBlend
{
	ushort	BoneID;
	float	Weight;

	RdVertexBlend() : BoneID( 0xffff ), Weight( 0.0f )	{}
};

struct RdVertex
{
	Point3			Pos;
	Point3			Normal;
	Point3			Color;
	Point3			Tangent;
	float			TangentW;
	Point3			Binormal;

	RdUV			UV;				//!< 기본 uv
	RdUV			UV2;			//!< Render To Texture 채널 정보

	RdUV			uvs[4];

	BYTE			Link;
	RdVertexBlend	Blend[4];

	int				OrgId;
	ulong			SmGroup;

	bool			Redundant;

	enum Component
	{
		POSITION = (1<<0),
		NORMAL   = (1<<1),
		COLOR	 = (1<<2),
		TEX0	 = (1<<3),
		TEX1	 = (1<<4),
		TANGENT  = (1<<5),
		BINORMAL = (1<<6),
		BLEND	 = (1<<7),

		NUM_VERTEX_COMPONENTS = 8,
	};

    enum
    {
        MAX_TEXTURE_LAYERS = 4,
    };

	RdVertex()
	{
		this->Pos.Set( 0.0f, 0.0f, 0.0f );
		this->Normal.Set( 0.0f, 0.0f, 0.0f );	
		this->Color.Set( 1.0f, 1.0f, 1.0f );
		this->Tangent.Set( 0.0f, 0.0f, 0.0f );
		this->Binormal.Set( 0.0f, 0.0f, 0.0f );
		this->Link = 0;

		this->OrgId = -1;
		this->SmGroup = 0xffffffff;

		this->Redundant = false;
	}

	void SetUv( int index, const RdUV& uv );
	int Compare( const RdVertex& rhs, int WantedComponent = (POSITION | NORMAL | COLOR | TEX0 | TEX1 | TANGENT | BINORMAL | BLEND) ) const;

	bool operator ==(const RdVertex& rhs) const;
};

struct RdFace
{	
	ushort		MatID;
	ulong		smGroup;
	ushort		Index[3];

	Point3		Normal;			// Face Normal
	Point3		Tangent;		// Face Tangent
	Point3		Binormal;

	RdFace() : MatID( 0 ), smGroup(0)
	{
		Index[0] = Index[1] = Index[2] = 0;
	}
};

class RdMeshNode : public RdBaseNode
{
protected:
	//------------------------------------------------------------------
	BYTE			m_btMaxLink;
	
	bool			m_bHasVertexColor;
	bool			m_bHasMultiUV;
	bool			m_bHasNormalMap;

	Tab<RdVertex*>	m_VertexList;
	Tab<RdFace*>	m_FaceList;
	Tab<ushort>		m_LinkedBoneList;

	void			Clear();

	void			AddLinkedBoneID( ushort usID );
	ushort			FindLinkedBoneIndex( ushort usBoneID );

	void			CalculateOBB();
	void			CalculateNormal();
	void			CalculateTangent();
public:
	RdMeshNode( BYTE btType, ushort usID, const TCHAR* strName );
	virtual ~RdMeshNode();
	
	void			SetVertexList( Tab<RdVertex>& list, bool bReverse = true );
	void			SetFaceList( Tab<RdFace>& list, bool bNeg );

	void			SetFaceMtlIndex( ushort usIndex, ushort usMtlID );
	ushort			GetFaceMtlIndex( ushort usIndex );

	void			NotifyComplete();
	
	void			SetEnableVertexColor( bool bEnable )	{		m_bHasVertexColor = bEnable;				}
	bool			GetEnableVertexColor() const			{		return m_bHasVertexColor;					}

	void			SetMultiUV( bool bValue )				{		m_bHasMultiUV = bValue;						}
	bool			HasMultiUV() const						{		return m_bHasMultiUV;						}

	BYTE			GetMaxLink() const						{		return m_btMaxLink;							}
	ushort			GetLinkedBoneCount() const				{		return (ushort)m_LinkedBoneList.Count();	}
	ushort			GetVertexCount() const					{		return (ushort)m_VertexList.Count();		}
	ushort			GetFaceCount() const					{		return (ushort)m_FaceList.Count();			}

	void			UseNormalMap( bool bUse )				{		m_bHasNormalMap = bUse;						}
	bool			HasNormalMap() const					{		return m_bHasNormalMap;						}

	const Tab<ushort>&		GetLinkedBoneList() const		{		return m_LinkedBoneList;					}
	const Tab<RdVertex*>&	GetVertexList() const			{		return m_VertexList;						}
	const Tab<RdFace*>&		GetFaceList() const				{		return m_FaceList;							}
};

struct RdNormFace
{
	ushort		Index[3];
	Point3		Normal;

	RdNormFace()
	{
		Index[0] = Index[1] = Index[2] = 0;
		Normal.Set( 0.0f, 0.0f, 0.0f );
	}
};

class RdNormFaceNode
{
protected:
	Tab<Point3>			m_VertexList;
	Tab<RdNormFace*>	m_FaceList;

	void			Clear();

	Point3			CalcFaceNormal( ulong ulA, ulong ulB, ulong ulC );

public:
	RdNormFaceNode();
	~RdNormFaceNode();

	void			SetVertexList( Tab<Point3>& list );
	void			SetFaceList( Tab<RdNormFace>& list, bool bNeg );

	ushort			GetVertexCount() const					{		return (ushort)m_VertexList.Count();		}
	ushort			GetFaceCount() const					{		return (ushort)m_FaceList.Count();			}

	const Tab<Point3>&	GetVertexList() const				{		return m_VertexList;						}
	const Tab<RdNormFace*>	GetFaceList() const				{		return m_FaceList;							}
};

#endif // __RD_MAX_MESH_H__