#ifndef __RD_MAX_UTIL_H__
#define __RD_MAX_UTIL_H__

#pragma once

#include "../RdMaxExporterHeader.h"

struct RdVertex;

#define SAFEDEL( x )		{ if( x ) {	delete (x);		(x) = NULL;		} }
#define SAFEDELS( x )		{ if( x ) {	delete[] (x);	(x) = NULL;		} }

//==================================================================
/** Fixed point / floating point / integer conversion macros
	4.12 fixed point란?
		4비트로 부호와 정수를 표현하고, 12비트로 소수점이하의 자리를 표현하는 16비트 실수
*/
//==================================================================
#define inttof32(n)          ((n) << 12)
#define f32toint(n)          ((n) >> 12)
#define floattof32(n)        ((f32)((n) * (1 << 12)))
#define f32tofloat(n)        (((float)(n)) / (float)(1<<12))

typedef short int t16;       // text coordinate 12.4 fixed point
#define f32tot16(n)          ((t16)(n >> 8))
#define inttot16(n)          ((n) << 4)
#define t16toint(n)          ((n) >> 4)
#define floattot16(n)        ((t16)((n) * (1 << 4)))
#define TEXTURE_PACK(u,v)    (((u) << 16) | (v & 0xFFFF))

typedef short int v16;       // vertex 4.12 fixed format
#define inttov16(n)          ((n) << 12)
#define f32tov16(n)          (n)
#define v16toint(n)          ((n) >> 12)
#define floattov16(n)        ((v16)((n) * (1 << 12)))
#define VERTEX_PACK(x,y)	 (((y) << 16) | ((x) & 0xFFFF))

typedef short int v10;       // vertex .10 fixed point
#define inttov10(n)          ((n) << 9)
#define f32tov10(n)          ((v10)(n >> 3))
#define v10toint(n)          ((n) >> 9)
#define floattov10(n)        ((v10)((n) * (1 << 9)))
#define NORMAL_PACK(x,y,z)   (((x) & 0x3FF) | (((y) & 0x3FF) << 10) | ((z) << 20))

//---------------------------------------------------------------
int Round( float value );

//==================================================================
// Debug
//==================================================================
static int AssertFailedFunc( TCHAR* sz )
{
	MessageBox( GetActiveWindow(), sz, _T( "Assert failure" ), MB_OK );
	int Set_Your_Breakpoint_Here = 1;
	return 1;
}

#define ASSERT_MBOX(f, sz)	( (f) ? 1 : AssertFailedFunc(sz) )

#define ASSERT_AND_ABORT(f, sz)							\
	if (!(f))											\
	{													\
		ASSERT_MBOX(FALSE, sz);							\
		cleanup( );										\
		return TREE_ABORT;								\
	}

//==================================================================
// 
//==================================================================
struct NodeName
{
	TCHAR		Name[MAX_LEN];
	ushort		ID;
};

extern NodeName g_arNodeNameList[1024];
extern ushort g_nNodeCount;

//==================================================================
// 
//==================================================================
#define UNDESIRABLE_MARKER		0xffff

bool	UndesirableNode( INode* pNode );
void	SetIndexOfNode( INode* pNode, ushort nNode );
ushort	GetIndexOfNode( INode* pNode );

bool	IsDummyNode( INode* pNode );
bool	IsMeshNode( INode* pNode );
bool	IsBoneNode( INode* pNode );
bool	IsBoneDummyNode( const TCHAR* strName );
bool	IsDummyBoneNode( const TCHAR* strName );
bool	IsBoundingBoxNode( const TCHAR* strName );
bool	IsMultiTextured( StdMat2* pMtl );
bool	IsShellMaterial( const TCHAR* strName );

Modifier*	FindModifier( Object* pObject, Class_ID classModID );

bool	IsTMNegParity( Matrix3 tm );

//==================================================================
// Chunk 
//==================================================================
void	WriteChunkBegin( FILE* pFile, ulong ulTag, ulong& ulPtr, ulong& ulBegin );
void	WriteChunkEnd( FILE* pFile, ulong ulPtr, ulong ulBegin );

void	WriteString( FILE* pFile, const TCHAR* strString );

//==================================================================
void ConvertMatrixToD3D( Matrix3* pMat );

bool Point3Equal( const Point3& rA, const Point3& rB );
int Point3Compare( const Point3& rA, const Point3& rB, float tol );
bool FloatEqual( float fA, float fB );
bool QuatEqual( const Quat& rA, const Quat& rB );
bool ComparePhysic( const RdVertex& V1, const RdVertex& V2 );

void Uniform_Matrix( Matrix3& In, Matrix3& Out );
void Transpose_Matrix( Matrix3& In, Matrix3& Out );

//==================================================================
void AddStringInListBox( HWND hDlg, int nDlgItem, const TCHAR* strString );
void ClearStringInListBox( HWND hDlg, int nDlgItem );

void OutputMsg( const TCHAR* pFormat, ... );

//==================================================================
class VNormal
{
public:
	Point3		normal;
	ulong		smooth;
	VNormal*	next;

	bool		init;

	VNormal();
	VNormal( Point3& n, ulong s );
	~VNormal();

	void		AddNormal( Point3& n, ulong s );
	Point3&		GetNormal( ulong s );

	void		Normalize();
};

#endif // __RD_MAX_UTIL_H__