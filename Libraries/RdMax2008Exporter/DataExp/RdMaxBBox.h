#ifndef __RD_MAX_BBOX_H__
#define __RD_MAX_BBOX_H__

#pragma once

#include "../RdMaxExporterHeader.h"

class RdBBox
{
protected:
	ushort			m_usParentID;
	TCHAR			m_strParentName[MAX_LEN];

	Point3			m_ptCenter;
	Point3			m_arAxis[3];
	Point3			m_arExtent;

	Tab<Point3>		m_VertexList;

	Matrix3			m_matParentInv;

public:
	RdBBox();
	~RdBBox();

	void			AddVertex( const Point3& vPos );

	void			CalculateOBB();

	void			SetParentNode( ushort usID, const TCHAR* strName );
	void			SetParentNodeXform( const Matrix3& mat );

	ushort			GetParentID() const						{		return m_usParentID;		}
	const TCHAR*	GetParentName() const					{		return m_strParentName;		}

	const Point3&	GetOBBCenter() const					{		return m_ptCenter;			}
	const Point3&	GetOBBAxisX() const						{		return m_arAxis[0];			}
	const Point3&	GetOBBAxisY() const						{		return m_arAxis[1];			}
	const Point3&	GetOBBAxisZ() const						{		return m_arAxis[2];			}
	const Point3&	GetOBBExtent() const					{		return m_arExtent;			}
};

#endif // __RD_MAX_BBOX_H__