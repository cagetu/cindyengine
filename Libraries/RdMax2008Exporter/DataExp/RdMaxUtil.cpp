#include "RdMaxUtil.h"
#include "RdMaxDataDefines.h"
#include "RdMaxMesh.h"

//------------------------------------------------------------------
NodeName g_arNodeNameList[1024];
ushort g_nNodeCount = 0;

//------------------------------------------------------------------
bool UndesirableNode( INode* pNode )
{
	Object* pObj = pNode->GetObjectRef();

	if( pObj->SuperClassID() == CAMERA_CLASS_ID )
		return true;

	if( pObj->SuperClassID() == LIGHT_CLASS_ID )
		return true;

	if( pObj->SuperClassID() == TARGET_CLASS_ID )
		return true;

	return false;
}

//------------------------------------------------------------------
void SetIndexOfNode( INode* pNode, ushort nNode )
{
	NodeName* pName;

	ushort i = 0;
	for( i = 0; i < g_nNodeCount; ++i )
	{
		if( _tcscmp( g_arNodeNameList[i].Name, pNode->GetName() ) == 0 )
			break;
	}

	if( i < g_nNodeCount )
		pName = &g_arNodeNameList[i];
	else
	{
		ASSERT_MBOX( g_nNodeCount < 1024, _T( "NodeNameList is full" ) );
		pName = &g_arNodeNameList[g_nNodeCount++];
		_tcscpy( pName->Name, pNode->GetName() );
	}

	pName->ID = nNode;
}

ushort GetIndexOfNode( INode* pNode )
{
	for( ushort i = 0; i < g_nNodeCount; ++i )
	{
		if( _tcscmp( g_arNodeNameList[i].Name, pNode->GetName() ) == 0 )
		{
			return g_arNodeNameList[i].ID;
		}
	}

	return UNDESIRABLE_MARKER;
}


bool IsDummyNode( INode* pNode )
{
	if( NULL == pNode || pNode->IsRootNode() )
		return false;
	
	Object* pObj = pNode->EvalWorldState(0).obj;
	if( ( pObj->SuperClassID() == HELPER_CLASS_ID ) && ( pObj->ClassID() == Class_ID( DUMMY_CLASS_ID, 0 ) ) )
	{
//		Control* pCont = pNode->GetTMController();
//
//		if(	( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID ) ||
//			( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) )// ||
////			( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )
//			return false;

		return true;
	}

	return false;
}

bool IsMeshNode( INode* pNode )
{
	if( NULL == pNode || pNode->IsRootNode() )
		return false;

	Object* pObj = pNode->EvalWorldState(0).obj;
	if( pObj->SuperClassID() != GEOMOBJECT_CLASS_ID )
		return false;

	if( pObj->ClassID() == BONE_OBJ_CLASSID || pObj->ClassID() == Class_ID( BONE_CLASS_ID, 0 ) )
		return false;

	Control* pCont = pNode->GetTMController();
	if( pCont )
	{
		if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID )	||		// Biped Other Parts.
			( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) 	||		// Biped Root "Bip01".
			( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )				// 
		{
			return false;
		}
	}

	return true;
}

bool IsBoneNode( INode* pNode )
{
	if( NULL == pNode || pNode->IsRootNode() )
		return false;

	//******************************************************************
	//	comment by cagetu [2006-11-2]
	//	Biped 데이터에 Nub이라는 이름을 가진 녀석들은 원래 속성이 HelperObject(Dummy) 속성을 가진 녀석이다.
	//	그래서, 계산량을 한번이라도 줄이기 위해서, 이 녀석들을 뽑지 않으려고 더미 노드를 먼저 거르고, 본인지 여부를
	//	체크 하였는데, skin을 적용하다보니, HelperObject에도 영향을 받는 버텍스들이 존재하기 때문에 그 문제를 해결하기
	//	위해서, Bone/Biped(HelperObject포함)를 뭔지 체크해서 결과를 보고하고, Helper를 체크하기로 한다.
	//******************************************************************
	Object* pObj = pNode->EvalWorldState(0).obj;
	if( pObj->ClassID() == BONE_OBJ_CLASSID || pObj->ClassID() == Class_ID( BONE_CLASS_ID, 0 )  )
		return true;

	Control* pCont = pNode->GetTMController();
	if( pCont )
	{
		if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID )	||		// Biped Other Parts.
			( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) 	||		// Biped Root "Bip01".
			( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )				// FootStep
		{
			return true;
		}
	}

//	Object* pObj = pNode->EvalWorldState(0).obj;
	if( pObj->ClassID() == Class_ID( DUMMY_CLASS_ID, 0 ) || ( pObj->SuperClassID() == HELPER_CLASS_ID ) )
	{
		if( IsDummyBoneNode( pNode->GetName() ) )
			return true;

		//Control* pCont = pNode->GetTMController();

		//if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID ) ||
		//	( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) )//  ||
		//	( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )
		//	return true;
	
		return false;
	}

	// 이동..
	//if( pObj->ClassID() == BONE_OBJ_CLASSID || pObj->ClassID() == Class_ID( BONE_CLASS_ID, 0 )  )
	//	return true;

	//Control* pCont = pNode->GetTMController();
	//if( pCont )
	//{
	//	if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID )	||		// Biped Other Parts.
	//		( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) 	||		// Biped Root "Bip01".
	//		( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )				// 
	//	{
	//		return true;
	//	}
	//}

	return false;
}

// GeomtryObject 중에 본으로 사용하도록 한다.
bool IsBoneDummyNode( const TCHAR* strName )
{
	if (_tcsnicmp( strName, TO_BONE, 5 ) == 0)
		return true;

	if( _tcsnicmp( strName, MESH_TO_BONE_U, 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, MESH_TO_BONE_L, 3 ) == 0 )
		return true;

	return false;
}

// 더미 노드 중에 본으로 인식하도록 한다.
bool IsDummyBoneNode( const TCHAR* strName )
{
	if (_tcsnicmp( strName, TO_BONE, 5 ) == 0)
		return true;

	if( _tcsnicmp( strName, DUMMY_TO_BONE_U, 3 ) == 0 ||
		_tcsnicmp( strName, DUMMY_TO_BONE_L, 3 ) == 0 )
		return true;

	return false;
}

// 바운딩 박스로 인식해욤..
bool IsBoundingBoxNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, CUSTOM_BOUNDINGBOX_U, 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, CUSTOM_BOUNDINGBOX_L, 3 ) == 0 )
		return true;

	return false;
}

// 멀티 텍스쳐 된 것인지..
bool IsMultiTextured( StdMat2* pMtl )
{
	if( !pMtl->MapEnabled(ID_DI) )
		return false;

	Texmap* pTex = (BitmapTex*)pMtl->GetSubTexmap( ID_DI );
	if( !pTex )
		return false;

	// self-illum slot 안에는 bitmap이 존재한다. dark map으로 되어야 한다.
	Texmap* subTexMap = NULL;
	if( pMtl->MapEnabled( ID_SI ) )
	{
		subTexMap = (BitmapTex*)pMtl->GetSubTexmap( ID_SI );
		if( subTexMap && subTexMap->ClassID() != Class_ID(BMTEX_CLASS_ID, 0) )
			subTexMap = NULL;
	}

	if( subTexMap && pTex && pTex->ClassID() == Class_ID(BMTEX_CLASS_ID,0) )
		return true;

	if (pTex->ClassID() == Class_ID( MIX_CLASS_ID, 0))
	{	// decal map
		
        // There is a "Mix" shader in the diffuse slot. It should be
        // used to set up a decal map

        if (pMtl->NumSubTexmaps() != 2 && pMtl->NumSubTexmaps() != 3)
            return(false);

		for (int i=0; i<pMtl->NumSubTexmaps(); i++)
		{
			subTexMap = pMtl->GetSubTexmap(i);
            if (subTexMap == NULL || 
                subTexMap->ClassID() != Class_ID(BMTEX_CLASS_ID, 0))
                return(false);
		}

		return true;
	}
	else if (pTex->ClassID() == Class_ID( COMPOSITE_CLASS_ID, 0 ))
	{	// glow map
		// There is a "Composite" shader in the diffuse slot. It 
        // should be used to set up a glow map.

        if (pMtl->NumSubTexmaps() != 2)
            return(false);

		for (int i=0; i<pMtl->NumSubTexmaps(); i++)
		{
			subTexMap = pMtl->GetSubTexmap(i);
            if (subTexMap == NULL || 
                subTexMap->ClassID() != Class_ID(BMTEX_CLASS_ID, 0))
                return(false);
		}

		return true;
	}
	else if (pTex->ClassID() == Class_ID( RGBMULT_CLASS_ID, 0 ))
	{	// dark map
        // There is an "RGB Mult" shader in the diffuse slot. It should
        // be used to set up a dark map.

        if (pMtl->NumSubTexmaps() != 2)
            return(false);

		for (int i=0; i<pMtl->NumSubTexmaps(); i++)
		{
			subTexMap = pMtl->GetSubTexmap(i);
            if (subTexMap == NULL || 
                subTexMap->ClassID() != Class_ID(BMTEX_CLASS_ID, 0))
                return(false);
		}
	}
	return false;
}

// Render To Texture 로 인식한다는...
bool IsShellMaterial( const TCHAR* strName )
{
	if( strName[0] == 'S' && strName[1] == 'h' &&
		strName[2] == 'e' && strName[3] == 'l' &&
		strName[4] == 'l' )
		return true;

	return false;
}

//------------------------------------------------------------------
// Determine is the node has negative scaling.
// This is used for mirrored objects for example. They have a negative scale factor
// so when calculating the normal we should take the vertices counter clockwise.
// If we don't compensate for this the objects will be 'inverted'.
//------------------------------------------------------------------
bool IsTMNegParity( Matrix3 tm )
{
	return (DotProd(CrossProd(tm.GetRow(0), tm.GetRow(1)), tm.GetRow(2))<0.0)?1:0;
}

//------------------------------------------------------------------
//
//------------------------------------------------------------------
Modifier* FindModifier( Object* pObject, Class_ID classModID )
{
	if (!pObject)
		return NULL;

	IDerivedObject* pDerived = NULL;
	if (pObject->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		pDerived = (IDerivedObject*)pObject;

		for (int i = 0; i < pDerived->NumModifiers(); ++i )
		{
			Modifier* pMod = pDerived->GetModifier(i);

			Class_ID id = pMod->ClassID();
			if (id == classModID )
				return pMod;
		}
	}

	if (pDerived)
		return FindModifier( pDerived->GetObjRef(), classModID );
	else 
		return NULL;
}

//------------------------------------------------------------------
void WriteChunkBegin( FILE* pFile, ulong ulTag, ulong& ulPtr, ulong& ulBegin )
{
	ulBegin = ftell( pFile );
	// Tag 저장
	fwrite( &ulTag, sizeof(ulong), 1, pFile );

	ulPtr = ftell( pFile );
	// Size 저장 장소 확보
	fwrite( &ulPtr, sizeof(ulong), 1, pFile );
}

void WriteChunkEnd( FILE* pFile, ulong ulPtr, ulong ulBegin )
{
	ulong ulPos, ulSize;
	ulPos = ftell( pFile );
	fseek( pFile, ulPtr, SEEK_SET );
	ulSize = ulPos - ulBegin;

	// 실제 크기 저장 - begin에서 잡아놓았던 그 자리에 덮혀쓴다.
	fwrite( &ulSize, sizeof(ulong), 1, pFile );

	fseek( pFile, ulPos, SEEK_SET );
}

void WriteString( FILE* pFile, const TCHAR* strString )
{
	ushort len = (ushort)_tcslen( strString );
	WCHAR wild[MAX_LEN];
	wmemset( wild, 0x00, MAX_LEN );
	MultiByteToWideChar( CP_ACP, 0, strString, MAX_LEN, wild, sizeof(wild)/sizeof(wild[0]) );

	fwrite( &len, sizeof(ushort), 1, pFile );
	fwrite( wild, sizeof(WCHAR), len, pFile );
}

//------------------------------------------------------------------
void ConvertMatrixToD3D( Matrix3* pMat )
{
	Point3 row = pMat->GetRow( 1 );
	pMat->SetRow( 1, pMat->GetRow( 2 ) );
	pMat->SetRow( 2, row );

	Point4 col = pMat->GetColumn( 1 );
	pMat->SetColumn( 1, pMat->GetColumn( 2 ) );
	pMat->SetColumn( 2, col ); 
}

//------------------------------------------------------------------
bool Point3Equal( const Point3& rA, const Point3& rB )
{
	if( fabs( rA.x - rB.x ) > 1e-6f )
		return false;
	if( fabs( rA.y - rB.y ) > 1e-6f )
		return false;
	if( fabs( rA.z - rB.z ) > 1e-6f )
		return false;

	return true;
}
int Point3Compare( const Point3& rA, const Point3& rB, float tol )
{
	if (fabs( rA.x - rB.x ) > tol)		return (rA.x > rB.x) ? +1 : -1;
	else if (fabs( rA.y - rB.y ) > tol)	return (rA.y > rB.y) ? +1 : -1;
	else if (fabs( rA.z - rB.z ) > tol)	return (rA.z > rB.z) ? +1 : -1;
	return 0;
}

//------------------------------------------------------------------
bool FloatEqual( float fA, float fB )
{
	if( fabs( fA - fB ) > 1e-6f )
		return false;

	return true;
}

bool QuatEqual( const Quat& rA, const Quat& rB )
{
	if( fabs( rA.x - rB.x ) > 1e-6f )
		return false;
	if( fabs( rA.y - rB.y ) > 1e-6f )
		return false;
	if( fabs( rA.z - rB.z ) > 1e-6f )
		return false;
	if( fabs( rA.w - rB.w ) > 1e-6f )
		return false;

	return true;
}

bool ComparePhysic( const RdVertex& V1, const RdVertex& V2 )
{
	if( V1.Link != V2.Link )
		return false;

	BYTE nLink = V1.Link;
	if( nLink > 4 )
		nLink = 4;

	for( BYTE i = 0; i < nLink; ++i )
	{
		if (V1.Blend[i].BoneID != V2.Blend[i].BoneID)
			return false;

		if (fabsf(V1.Blend[i].Weight - V2.Blend[i].Weight) > 0.000001f)
			return false;
	}

	return true;
}

//---------------------------------------------------------------
int Round( float value )
{
	 return ((value - floor(value) > 0.5) ? (int)ceil(value) : (int)floor(value));
}

void Uniform_Matrix( Matrix3& In, Matrix3& Out )
{
	AffineParts parts;
	decomp_affine( In, &parts );

	parts.q.MakeMatrix( Out );
	Out.SetRow( 3, parts.t );
}

void Transpose_Matrix( Matrix3& In, Matrix3& Out )
{
	float transpose[4][3];

	int row;
	for(row = 0; row < 3; row++)
	{
		int column;
		for(column = 0; column < 3; column++)
		{
			transpose[column][row] = In.GetAddr()[row][column];
		}
	}

	int column;
	for(column = 0; column < 3; column++)
	{
		transpose[3][column] = 0;
	}

	Out = Matrix3(transpose);
	Out.NoTrans();
}


//------------------------------------------------------------------
void AddStringInListBox( HWND hDlg, int nDlgItem, const TCHAR* strString )
{
	int nNum = SendDlgItemMessage( hDlg, nDlgItem, LB_GETCOUNT, 0, 0 );
	SendDlgItemMessage( hDlg, nDlgItem, LB_INSERTSTRING, nNum, (LPARAM)strString );
}

//------------------------------------------------------------------
void ClearStringInListBox( HWND hDlg, int nDlgItem )
{
	SendDlgItemMessage( hDlg, nDlgItem, LB_RESETCONTENT, 0, 0 );
}

//------------------------------------------------------------------
void OutputMsg( const TCHAR* pFormat, ... )
{
	va_list args;

	TCHAR buffer[256];
	memset( buffer, 0, sizeof(TCHAR) * 256 );

	va_start( args, pFormat );

	vsprintf( buffer, pFormat, args );
	OutputDebugStr( buffer );

	va_end( args );
}

//==================================================================
//	버텍스 노말을 계산하기 위한 객체
//==================================================================
VNormal::VNormal()
{
	smooth = 0;
	next = 0;
	init = false;
	normal = Point3(0.0f, 0.0f, 0.0f);
}

//------------------------------------------------------------------
VNormal::VNormal( Point3& n, ulong s )
{
	next = NULL;
	init = true;
	normal = n;
	smooth = s;
}

//------------------------------------------------------------------
VNormal::~VNormal()
{
	delete next;
}

//------------------------------------------------------------------
// Add a normal to the list if the smoothing group bits overlap, 
// otherwise create a new vertex normal in the list 
void VNormal::AddNormal( Point3& n, ulong s )
{
	if( !(s & smooth ) && init )
	{
		if(next)
			next->AddNormal( n, s );
		else
			next = new VNormal( n, s );
	}
	else
	{
		normal += n;
		smooth |= s;
		init = true;
	}
}

//------------------------------------------------------------------
// Retrieves a normal if the smoothing groups overlap or there is 
// only one in the list 
Point3& VNormal::GetNormal( ulong s )
{
	if( smooth & s || !next )
		return normal;
	else
		return next->GetNormal(s);
}

//------------------------------------------------------------------
// Normalize each normal in the list 
void VNormal::Normalize()
{
	VNormal* ptr = next;
	VNormal* prev = this;

	while( ptr )
	{
		if( ptr->smooth & smooth )
		{
			normal += ptr->normal;
			prev->next = ptr->next;
			delete ptr;

			ptr = prev->next;
		}
		else
		{
			prev = ptr;
			ptr = ptr->next;
		}
	}

	normal = ::Normalize(normal);

	if( next )
		next->Normalize();
}
