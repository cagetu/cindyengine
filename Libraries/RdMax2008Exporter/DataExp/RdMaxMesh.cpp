#include "RdMaxMesh.h"
#include "RdMaxUtil.h"

//------------------------------------------------------------------------------
/**
*/
void RdVertex::SetUv( int index, const RdUV& uv )
{
	assert((index >= 0) && (index <RdVertex::MAX_TEXTURE_LAYERS));

	this->uvs[index] = uv;
}

//------------------------------------------------------------------------------
/**
*/
int RdVertex::Compare( const RdVertex& rhs, int WantedComponent ) const
{
	bool res = false;
	if (WantedComponent & RdVertex::POSITION)
	{
		res = Point3Equal( this->Pos, rhs.Pos );
		if (res == false)
			return 0;
	}
	if (WantedComponent & RdVertex::NORMAL)
	{
		res = Point3Equal( this->Normal, rhs.Normal );
		if (res == false)
			return 0;
	}
	if (WantedComponent & RdVertex::COLOR)
	{
		res = Point3Equal( this->Color, rhs.Color );
		if (res == false)
			return 0;
	}
	if (WantedComponent & RdVertex::TEX0)
	{
		res = FloatEqual( this->UV.U, rhs.UV.U ) && FloatEqual( this->UV.V, rhs.UV.V ) ? true : false;
		if (res == false)
			return 0;
	}
	if (WantedComponent & RdVertex::TEX1)
	{
		res = FloatEqual( this->UV2.U, rhs.UV2.U ) && FloatEqual( this->UV2.V, rhs.UV2.V ) ? true : false;
		if (res == false)
			return 0;
	}
	if (WantedComponent & RdVertex::TANGENT)
	{
		res = Point3Equal( this->Tangent, rhs.Tangent );
		if (res == false)
			return 0;
	}
	if (WantedComponent & RdVertex::BINORMAL)
	{
		res = Point3Equal( this->Binormal, rhs.Binormal );
		if (res == false)
			return 0;
	}
	if (WantedComponent & RdVertex::BLEND)
	{
		res = ComparePhysic( *this, rhs );
		if (res == false)
			return 0;
	}

	return 1;
}

//------------------------------------------------------------------------------
/**
*/
bool RdVertex::operator ==(const RdVertex& rhs) const
{
	bool res = false;

	/// position 비교
	if (!Point3Equal( this->Pos, rhs.Pos ))
		return false;
	/// normal 비교
	if (!Point3Equal( this->Normal, rhs.Normal ))
		return false;
	/// vertex color 비교
	if (!Point3Equal( this->Color, rhs.Color ))
		return false;

	/// uv 비교...
	res = FloatEqual( this->UV.U, rhs.UV.U ) && FloatEqual( this->UV.V, rhs.UV.V ) ? true : false;
	if (res == false)
		return false;
	res = FloatEqual( this->UV2.U, rhs.UV2.U ) && FloatEqual( this->UV2.V, rhs.UV2.V ) ? true : false;
	if (res == false)
		return false;
	for (int i = 0; i < 4; i++)
	{
		res = FloatEqual( this->uvs[i].U, rhs.uvs[i].U ) && FloatEqual( this->uvs[i].V, rhs.uvs[i].V ) ? true : false;
		if (res == false)
			return false;
	}

	/// tagent 비교
	res = Point3Equal( this->Tangent, rhs.Tangent );
	if (res == false)
		return false;
	/// binormal(bitangent) 비교
	res = Point3Equal( this->Binormal, rhs.Binormal );
	if (res == false)
		return 0;
	/// skinning 정보 비교
	res = ComparePhysic( *this, rhs );
	if (res == false)
		return 0;

	return true;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RdMeshNode::RdMeshNode( BYTE btType, ushort usID, const TCHAR* strName ) 
: RdBaseNode( btType, usID, strName )
{
	m_btMaxLink = 0;
	
	m_bHasVertexColor = false;
	m_bHasMultiUV = false;
	m_bHasNormalMap = false;

	m_VertexList.Init();
	m_VertexList.ZeroCount();

	m_FaceList.Init();
	m_FaceList.ZeroCount();

	m_LinkedBoneList.Init();
	m_LinkedBoneList.ZeroCount();
}

RdMeshNode::~RdMeshNode()
{
	Clear();
}

void RdMeshNode::Clear()
{
	ulong ulCount = m_VertexList.Count();
	RdVertex** ppVert;
	for( ulong i = 0; i < ulCount; ++i )
	{
		ppVert = m_VertexList.Addr( i );
		delete (*ppVert);
	}
	m_VertexList.Delete( 0, ulCount );

	ulCount = m_FaceList.Count();
	RdFace** ppFace;
	for( ulong i = 0; i < ulCount; ++i )
	{
		ppFace = m_FaceList.Addr( i );
		delete (*ppFace);
	}
	m_FaceList.Delete( 0, ulCount );

	m_LinkedBoneList.Delete( 0, m_LinkedBoneList.Count() );
}


void RdMeshNode::AddLinkedBoneID( ushort usID )
{
	ushort usBoneCnt = m_LinkedBoneList.Count();

	ushort i = 0;
	for( i = 0; i < usBoneCnt; ++i )
	{
		if( m_LinkedBoneList[i] == usID )
			break;
	}

	if( i == usBoneCnt )
		m_LinkedBoneList.Insert( usBoneCnt, 1, &usID );
}

void RdMeshNode::SetFaceList( Tab<RdFace>& list, bool bNeg )
{
	ulong ulCount = list.Count();

	m_FaceList.Init();
	m_FaceList.ZeroCount();
	m_FaceList.SetCount( ulCount );

	for( ulong i = 0; i < ulCount; ++i )
	{
		RdFace* pFace = new RdFace();

		if (bNeg)
		{
			pFace->Index[0] = list[i].Index[0];
			pFace->Index[1] = list[i].Index[1];
			pFace->Index[2] = list[i].Index[2];
		}
		else
		{
			pFace->Index[0] = list[i].Index[0];
			pFace->Index[1] = list[i].Index[2];
			pFace->Index[2] = list[i].Index[1];
		}
		pFace->MatID = list[i].MatID;
		pFace->smGroup = list[i].smGroup;

		//m_FaceList.Append( 1, &pFace );
		m_FaceList[i] = pFace;
	}
}

//------------------------------------------------------------------
/**
*/
void RdMeshNode::SetVertexList( Tab<RdVertex>& list, bool bReverse )
{
	Matrix3 inv = Inverse( m_matWorldTM );

	ulong ulCount = list.Count();
	for( ulong i = 0; i < ulCount; ++i )
	{
		RdVertex* pVert = new RdVertex();

//		if( m_btMaxLink == 0 )
//		{
		if( bReverse )
		{
			pVert->Pos = inv * Point3( list[i].Pos.x, list[i].Pos.z, list[i].Pos.y );

//			pVert->Normal = inv * Point3( list[i].Normal.x, list[i].Normal.z, list[i].Normal.y );
			pVert->Normal = Point3( list[i].Normal.x, list[i].Normal.z, list[i].Normal.y );
			pVert->Binormal = Point3( list[i].Binormal.x, list[i].Binormal.z, list[i].Binormal.y );
			pVert->Tangent = Point3( list[i].Tangent.x, list[i].Tangent.z, list[i].Tangent.y );
			//pVert->Binormal = Point3( list[i].Binormal.x, list[i].Binormal.y, list[i].Binormal.z );
			//pVert->Tangent = Point3( list[i].Tangent.x, list[i].Tangent.y, list[i].Tangent.z );
			pVert->TangentW = list[i].TangentW;
		}
		else
		{
			pVert->Pos = inv * Point3( list[i].Pos.x, list[i].Pos.y, list[i].Pos.z );

//			pVert->Normal = inv * Point3( list[i].Normal.x, list[i].Normal.y, list[i].Normal.z );
			pVert->Normal = Point3( list[i].Normal.x, list[i].Normal.y, list[i].Normal.z );
			pVert->Binormal = Point3( list[i].Binormal.x, list[i].Binormal.y, list[i].Binormal.z );
			pVert->Tangent = Point3( list[i].Tangent.x, list[i].Tangent.y, list[i].Tangent.z );
			pVert->TangentW = list[i].TangentW;
		}
//		}
//		else
//		{
//			pVert->Pos = Point3( list[i].Pos.x, list[i].Pos.z, list[i].Pos.y );
//			pVert->Normal = Point3( list[i].Normal.x, list[i].Normal.z, list[i].Normal.y );
//		}

		pVert->UV.U = list[i].UV.U;
		pVert->UV.V = list[i].UV.V;

		pVert->Color = list[i].Color;

		pVert->Link = list[i].Link;

		if( m_btMaxLink < pVert->Link )
		{
			m_btMaxLink = pVert->Link;
		}

		int nLink = pVert->Link;
		if( nLink > 4 )
			nLink = 4;

		for( int j = 0; j < nLink; ++j )
		{
			pVert->Blend[j].BoneID = list[i].Blend[j].BoneID;
			pVert->Blend[j].Weight = list[i].Blend[j].Weight;

			AddLinkedBoneID( pVert->Blend[j].BoneID );
		}

		if( m_ptBBoxMin.x > pVert->Pos.x )			m_ptBBoxMin.x = pVert->Pos.x;
		if( m_ptBBoxMin.y > pVert->Pos.y )			m_ptBBoxMin.y = pVert->Pos.y;
		if( m_ptBBoxMin.z > pVert->Pos.z )			m_ptBBoxMin.z = pVert->Pos.z;

		if( m_ptBBoxMax.x < pVert->Pos.x )			m_ptBBoxMax.x = pVert->Pos.x;
		if( m_ptBBoxMax.y < pVert->Pos.y )			m_ptBBoxMax.y = pVert->Pos.y;
		if( m_ptBBoxMax.z < pVert->Pos.z )			m_ptBBoxMax.z = pVert->Pos.z;

		m_VertexList.Append( 1, &pVert );
	}

	//CalculateNormal();
	CalculateTangent();
	CalculateOBB();
}

void RdMeshNode::SetFaceMtlIndex( ushort usIndex, ushort usMtlID )
{
	ASSERT_MBOX( usIndex < m_FaceList.Count(), _T( "Wrong Index. [SetFaceMtlIndex]" ) );

	m_FaceList[usIndex]->MatID = usMtlID;
}

ushort RdMeshNode::GetFaceMtlIndex( ushort usIndex )
{
	ASSERT_MBOX( usIndex < m_FaceList.Count(), _T( "Wrong Index. [GetFaceMtlIndex]" ) );

	return m_FaceList[usIndex]->MatID;
}

//------------------------------------------------------------------
/**
*/
void RdMeshNode::CalculateNormal()
{	
	Tab<VNormal> vNorms;
	vNorms.SetCount( m_VertexList.Count() );

	Tab<Point3> fNorms;
	fNorms.SetCount( m_FaceList.Count() );

	//! 1. Initialize
	ulong v = 0;
	for (v = 0; v < m_VertexList.Count(); ++v)
		vNorms[v] = VNormal();

	RdVertex* v0;
	RdVertex* v1;
	RdVertex* v2;

	//! 2. 버텍스의 노말값 추가.
	for (ulong t = 0; t < m_FaceList.Count(); ++t)
	{
		//@cagetu -09/09/29 : 여기를 잘 확인해보자!!!!
		v0 = m_VertexList[m_FaceList[t]->Index[0]];
		v1 = m_VertexList[m_FaceList[t]->Index[1]];
		v2 = m_VertexList[m_FaceList[t]->Index[2]];

		fNorms[t] = (v1->Pos - v0->Pos) ^ (v2->Pos - v1->Pos);

		for (int j = 0; j < 3; ++j)
		{
			vNorms[m_FaceList[t]->Index[j]].AddNormal( fNorms[t], m_FaceList[t]->smGroup );
		}

		fNorms[t] = Normalize(fNorms[t]);
		m_FaceList[t]->Normal = fNorms[t];
		//v0->Normal = fNorms[t];
		//v1->Normal = fNorms[t];
		//v2->Normal = fNorms[t];
	}

	//! 3. 버텍스 노말 계산
	for (v = 0; v < m_VertexList.Count(); ++v)
	{
		vNorms[v].Normalize();

		if (vNorms[v].next)
		{
		}

		m_VertexList[v]->Normal = vNorms[v].normal;
	}
}

//------------------------------------------------------------------
/**	09/10/01
	원하는 결과가 안나온다면,
	1. uv값에 문제가 없고, 식에 문제가 없다면, normal값을 의심해 봐야 하지 않을까?!
*/
void RdMeshNode::CalculateTangent()
{
	// http://www.terathon.com/code/tangent.html
	// B' = (u1-u0)(x2-x0) - (u2-u0)(x1-x0)
	// T = B' * N
	// B = T * N
    Point3* tan1 = new Point3[m_VertexList.Count()];
    Point3* tan2 = new Point3[m_VertexList.Count()];
    ZeroMemory(tan1, m_VertexList.Count() * sizeof(Point3));
	ZeroMemory(tan2, m_VertexList.Count() * sizeof(Point3));

	/**	Triangle Tangent 구하기
	*/
	long numTriangles = m_FaceList.Count();
	for (long triIndex = 0; triIndex < numTriangles; ++triIndex)
	{
		RdFace* face = m_FaceList[triIndex];

		long i1, i2, i3;
		i1 = face->Index[0];
		i2 = face->Index[1];
		i3 = face->Index[2];

		RdVertex* vertex1 = m_VertexList[i1];
		RdVertex* vertex2 = m_VertexList[i2];
		RdVertex* vertex3 = m_VertexList[i3];

		/// position
		const Point3 localV1 = vertex1->Pos;
		const Point3 localV2 = vertex2->Pos;
		const Point3 localV3 = vertex3->Pos;

		// compute tangents
        float x1 = localV2.x - localV1.x;
        float x2 = localV3.x - localV1.x;
        float y1 = localV2.y - localV1.y;
        float y2 = localV3.y - localV1.y;
        float z1 = localV2.z - localV1.z;
        float z2 = localV3.z - localV1.z;

        float s1 = vertex2->UV.U - vertex1->UV.U;
        float s2 = vertex3->UV.U - vertex1->UV.U;
        float t1 = vertex2->UV.V - vertex1->UV.V;
        float t2 = vertex3->UV.V - vertex1->UV.V;

        float l = (s1 * t2 - s2 * t1);
        // catch singularity
        //if (l == 0.0f)
        //{
        //    l = 0.0001f;
        //}
		if (fabs(l) <= 1E-6f)
			continue;

        float r = 1.0f / l;
        Point3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
        Point3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;

		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
	}

	/**	
		<접선 공간->오브젝트 공간 변환>
		| Tx, Bx, Nx |
		| Ty, By, Ny |
		| Tz, Bz, Nz |

		<오브젝트 공간->접선 공간 변환>
		|  Tx', Ty', Tz' |
		|  Bx', By', Bz' | *
		|  Nx,  Ny,  Nz  |
	*/

	for (ulong v=0; v<m_VertexList.Count(); v++)
	{
        const Point3& n = m_VertexList[v]->Normal;
        const Point3& t = tan1[v];

		// Gram-Schmidt orthogonalize
		Point3 invTangent = t - n * DotProd(n,t);
		invTangent = ::Normalize(invTangent);//invTangent.Normalize();
		// Calculate handedness
		float h = (DotProd(CrossProd(n,t), tan2[v]) < 0.0f) ? -1.0f : 1.0f;
		Point3 invBinormal = CrossProd(n, invTangent) * h;
		invBinormal = ::Normalize(invBinormal);//invBinormal.Normalize();

		m_VertexList[v]->Tangent = invTangent;
		m_VertexList[v]->TangentW = h;
		m_VertexList[v]->Binormal = invBinormal;
	}

	delete[] tan2;
	delete[] tan1;
}

void RdMeshNode::CalculateOBB()
{
	Point3 m( 0.0f, 0.0f, 0.0f );
	int nCount = m_VertexList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		m += m_VertexList[i]->Pos;
	}

	m /= (float)nCount;

	float c11, c22, c33, c12, c13, c23;
	c11 = c22 = c33 = c12 = c13 = c23 = 0.0f;

	for( int i = 0; i < nCount; ++i )
	{
		c11 += ( m_VertexList[i]->Pos.x - m.x ) * ( m_VertexList[i]->Pos.x - m.x );
		c22 += ( m_VertexList[i]->Pos.y - m.y ) * ( m_VertexList[i]->Pos.y - m.y );
		c33 += ( m_VertexList[i]->Pos.z - m.z ) * ( m_VertexList[i]->Pos.z - m.z );
		c12 += ( m_VertexList[i]->Pos.x - m.x ) * ( m_VertexList[i]->Pos.y - m.y );
		c13 += ( m_VertexList[i]->Pos.x - m.x ) * ( m_VertexList[i]->Pos.z - m.z );
		c23 += ( m_VertexList[i]->Pos.y - m.y ) * ( m_VertexList[i]->Pos.z - m.z );
	}

	c11 /= (float)nCount;
	c22 /= (float)nCount;
	c33 /= (float)nCount;
	c12 /= (float)nCount;
	c13 /= (float)nCount;
	c23 /= (float)nCount;

	float mm[3][3] = 
	{ 
		{ c11, c12, c13 }, 
		{ c12, c22, c23 }, 
		{ c13, c23, c33 } 
	};

	float eigenvector[3][3];
	float eigenvalue[3];
	Jacobi( mm, eigenvector, eigenvalue );

	for( int i = 0; i < 3; ++i )
	{
		m_arAxis[i].x = eigenvector[0][i];
		m_arAxis[i].y = eigenvector[1][i];
		m_arAxis[i].z = eigenvector[2][i];
	}

	float min[3] = { FLT_MAX, FLT_MAX, FLT_MAX };
	float max[3] = { -FLT_MAX, -FLT_MAX, -FLT_MAX };

	float dot;

	for( int i = 0; i < 3; ++i )
	{
		for( int j = 0; j < nCount; ++j )
		{
			dot = DotProd( m_VertexList[j]->Pos, m_arAxis[i] );
			if( min[i] > dot )			min[i] = dot;
			if( max[i] < dot )			max[i] = dot;
		}
	}

	m_ptCenter = (((min[0]+max[0])*0.5f)*m_arAxis[0]) + (((min[1]+max[1])*0.5f)*m_arAxis[1]) + 
					(((min[2]+max[2])*0.5f)*m_arAxis[2]);

	m_arExtent.x = ( max[0] - min[0] ) * 0.5f;
	m_arExtent.y = ( max[1] - min[1] ) * 0.5f;
	m_arExtent.z = ( max[2] - min[2] ) * 0.5f;
}

int CompFace( const void* pEl1, const void* pEl2 )
{
	RdFace* pF1 = (RdFace*)&pEl1;
	RdFace* pF2 = (RdFace*)&pEl2;

	if( pF1->MatID < pF2->MatID )		return -1;
	if( pF1->MatID == pF2->MatID )		return 0;
	return 1;

//	return (pF1->MatID < pF2->MatID );
}

int CompBone( const void* pEl1, const void* pEl2 )
{
	ushort* a = (ushort*)pEl1;
	ushort* b = (ushort*)pEl2;

	if( (*a) < (*b) )		return -1;
	if( (*a) == (*b) )		return 0;
	return 1;

//	return ( (*a) > (*b) );
}

//------------------------------------------------------------------
/**
*/
void RdMeshNode::NotifyComplete()
{
	m_FaceList.Sort( CompFace );
	m_LinkedBoneList.Sort( CompBone );

	int nCount = m_VertexList.Count();
	ushort link = 0;
	ushort idx = 0;
	ushort newidx = 0;
	for( int i = 0; i < nCount; ++i )
	{
		link = m_VertexList[i]->Link;
		if( link > 4 )
			link = 4;

		for( idx = 0; idx < link; ++idx )
		{
			newidx = FindLinkedBoneIndex( m_VertexList[i]->Blend[idx].BoneID );
			m_VertexList[i]->Blend[idx].BoneID = newidx;
		}
	}
}

ushort RdMeshNode::FindLinkedBoneIndex( ushort usBoneID )
{
	ushort usBoneCnt = m_LinkedBoneList.Count();
	ushort usIndex = 0;

	for( ushort i = 0; i < usBoneCnt; ++i )
	{
		if( m_LinkedBoneList[i] == usBoneID )
			break;

		++usIndex;
	}

	return usIndex;	
}

//------------------------------------------------------------------
RdNormFaceNode::RdNormFaceNode()
{
	m_VertexList.Init();
	m_VertexList.ZeroCount();

	m_FaceList.Init();
	m_FaceList.ZeroCount();
}

RdNormFaceNode::~RdNormFaceNode()
{
	Clear();
}

void RdNormFaceNode::Clear()
{
	ulong ulCount = m_VertexList.Count();
	m_VertexList.Delete( 0, ulCount );

	ulCount = m_FaceList.Count();
	RdNormFace** ppFace;
	for( ulong i = 0; i < ulCount; ++i )
	{
		ppFace = m_FaceList.Addr( i );
		delete (*ppFace );
	}
	m_FaceList.Delete( 0, ulCount );
}

void RdNormFaceNode::SetVertexList( Tab<Point3>& list )
{
	ulong ulCount = list.Count();
	m_VertexList.SetCount( ulCount );
	for( ulong i = 0; i < ulCount; ++i )
	{
		m_VertexList[i] = Point3( list[i].x, list[i].z, list[i].y );
	}
}

void RdNormFaceNode::SetFaceList( Tab<RdNormFace>& list, bool bNeg )
{
	ulong ulCount = list.Count();
	for( ulong i = 0; i < ulCount; ++i )
	{
		RdNormFace* pFace = new RdNormFace();

		pFace->Index[0] = list[i].Index[0];

		if( bNeg )
		{
			pFace->Index[1] = list[i].Index[1];
			pFace->Index[2] = list[i].Index[2];
		}
		else
		{
			pFace->Index[1] = list[i].Index[2];
			pFace->Index[2] = list[i].Index[1];
		}

		pFace->Normal = CalcFaceNormal( pFace->Index[0], pFace->Index[1], pFace->Index[2] );

		m_FaceList.Append( 1, &pFace );
	}
}

Point3 RdNormFaceNode::CalcFaceNormal( ulong ulA, ulong ulB, ulong ulC )
{
	Point3 v10 = m_VertexList[ulB] - m_VertexList[ulA];
	Point3 v20 = m_VertexList[ulC] - m_VertexList[ulA];

	Point3 normal = v10 ^ v20;
	return normal.Normalize();
}