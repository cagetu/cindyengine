#include "RdMaxBone.h"

RdBone::RdBone( BYTE btType, ushort usID, const TCHAR* strName )
: RdBaseNode( btType, usID, strName )
{
	m_bMirrored		= false;

	m_VertexList.Init();
	m_VertexList.ZeroCount();
}

RdBone::~RdBone()
{

}

void RdBone::AddVertex( const Point3& Pos )
{
	Point3 tmp = Pos;
	m_OrigVertexList.Append( 1, &tmp );
	Matrix3 inv = Inverse( m_matWorldTM );
	tmp = inv * Pos;
	
	m_VertexList.Append( 1, &tmp );

	if( m_ptBBoxMin.x > tmp.x )				m_ptBBoxMin.x = tmp.x;
	if( m_ptBBoxMin.y > tmp.y )				m_ptBBoxMin.y = tmp.y;
	if( m_ptBBoxMin.z > tmp.z )				m_ptBBoxMin.z = tmp.z;

	if( m_ptBBoxMax.x < tmp.x )				m_ptBBoxMax.x = tmp.x;
	if( m_ptBBoxMax.y < tmp.y )				m_ptBBoxMax.y = tmp.y;
	if( m_ptBBoxMax.z < tmp.z )				m_ptBBoxMax.z = tmp.z;
}

int RdBone::InsertToPool( Tab<Point3>& list )
{
	int nCount = m_OrigVertexList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		list.Append( 1, &m_OrigVertexList[i] );
	}

	return nCount;
}

void RdBone::CalculateOBB()
{
	Point3 m( 0.0f, 0.0f, 0.0f );
	int nCount = m_VertexList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		m += m_VertexList[i];
	}

	m /= (float)nCount;

	float c11, c22, c33, c12, c13, c23;
	c11 = c22 = c33 = c12 = c13 = c23 = 0.0f;

	for( int i = 0; i < nCount; ++i )
	{
		c11 += ( m_VertexList[i].x - m.x ) * ( m_VertexList[i].x - m.x );
		c22 += ( m_VertexList[i].y - m.y ) * ( m_VertexList[i].y - m.y );
		c33 += ( m_VertexList[i].z - m.z ) * ( m_VertexList[i].z - m.z );
		c12 += ( m_VertexList[i].x - m.x ) * ( m_VertexList[i].y - m.y );
		c13 += ( m_VertexList[i].x - m.x ) * ( m_VertexList[i].z - m.z );
		c23 += ( m_VertexList[i].y - m.y ) * ( m_VertexList[i].z - m.z );
	}

	c11 /= (float)nCount;
	c22 /= (float)nCount;
	c33 /= (float)nCount;
	c12 /= (float)nCount;
	c13 /= (float)nCount;
	c23 /= (float)nCount;

	float mm[3][3] = 
	{ 
		{ c11, c12, c13 }, 
		{ c12, c22, c23 }, 
		{ c13, c23, c33 } 
	};

	float eigenvector[3][3];
	float eigenvalue[3];
	Jacobi( mm, eigenvector, eigenvalue );

	for( int i = 0; i < 3; ++i )
	{
		m_arAxis[i].x = eigenvector[0][i];
		m_arAxis[i].y = eigenvector[1][i];
		m_arAxis[i].z = eigenvector[2][i];
	}

	float min[3] = { FLT_MAX, FLT_MAX, FLT_MAX };
	float max[3] = { -FLT_MAX, -FLT_MAX, -FLT_MAX };

	float dot;

	for( int i = 0; i < 3; ++i )
	{
		for( int j = 0; j < nCount; ++j )
		{
			dot = DotProd( m_VertexList[j], m_arAxis[i] );
			if( min[i] > dot )			min[i] = dot;
			if( max[i] < dot )			max[i] = dot;
		}
	}

	m_ptCenter = (((min[0]+max[0])*0.5f)*m_arAxis[0]) + (((min[1]+max[1])*0.5f)*m_arAxis[1]) + 
					(((min[2]+max[2])*0.5f)*m_arAxis[2]);

	m_arExtent.x = ( max[0] - min[0] ) * 0.5f;
	m_arExtent.y = ( max[1] - min[1] ) * 0.5f;
	m_arExtent.z = ( max[2] - min[2] ) * 0.5f;
}
