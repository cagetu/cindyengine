#include "RdMax2008Exporter.h"
#include "DataExp/RdMaxUtil.h"

//------------------------------------------------------------------
#define RDMAX2008EXP_CLASS_ID Class_ID(0x40817660, 0x6d080731)

//==================================================================
// ClassDesc Class
//==================================================================
class RdMax2008ExpClassDesc : public ClassDesc2
{
public:
	int 			IsPublic()						{	return TRUE;						}
	void*			Create(BOOL loading = FALSE)	{	return new RdMax2008Exp();			}
	const TCHAR*	ClassName()						{	return GetString(IDS_CLASS_NAME);	}
	SClass_ID		SuperClassID()					{	return SCENE_EXPORT_CLASS_ID;		}
	Class_ID		ClassID()						{	return RDMAX2008EXP_CLASS_ID;		}
	const TCHAR* 	Category()						{	return GetString(IDS_CATEGORY);		}

	const TCHAR*	InternalName()					{	return _T("RdMax2008Exp");			}	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance()						{	return hInstance;					}	// returns owning module handle
};

static RdMax2008ExpClassDesc RdMax2008ExpDesc;
ClassDesc2* GetRdMaxExporterDesc()	{	return &RdMax2008ExpDesc;		}

int g_nCurExp = -1;

//------------------------------------------------------------------
/**
*/
INT_PTR CALLBACK ExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax2008Exp* pExp = NULL;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			pExp = (RdMax2008Exp*)lParam;
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );		

			g_nCurExp = 0;
		}
		return TRUE;

	case WM_COMMAND:
		if( HIWORD( wParam ) == BN_CLICKED )
		{
			switch( LOWORD( wParam ) )
			{
			case IDC_MESH_EXPORT:
				{
					g_nCurExp = IDC_MESH_EXPORT;

					EndDialog( hDlg, 1 );
//					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_MESH_DLG), GetActiveWindow(), MeshExportDlgProc, (LPARAM)(pExp));					
					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_PROPERTY_DLG), GetActiveWindow(), PropertyExportDlgProc, (LPARAM)(pExp));
				}
				break;

			case IDC_SKEL_EXPORT:
				{
					g_nCurExp = IDC_SKEL_EXPORT;

					EndDialog( hDlg, 1 );
					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_SKEL_DLG), GetActiveWindow(), SkelExportDlgProc, (LPARAM)(pExp));					
				}
				break;

			case IDC_ANI_EXPORT:
				{
					g_nCurExp = IDC_ANI_EXPORT;

					EndDialog( hDlg, 1 );
					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_PROPERTY_DLG), GetActiveWindow(), PropertyExportDlgProc, (LPARAM)(pExp));
				}
				break;
			}
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog( hDlg, 0 );
		}
		return TRUE;
	}

	return FALSE;
}

//------------------------------------------------------------------
/**
*/
INT_PTR CALLBACK MeshExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax2008Exp* pExp = NULL;
	static HWND hCheckVertColor, hCheckTangent, hCheckPackVertex;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );

			SetCursor( LoadCursor( NULL, IDC_WAIT ) );
			pExp = (RdMax2008Exp*)lParam;
			pExp->m_pMeshExp = new RdMeshExp( pExp->m_pIFace, hDlg, pExp->m_bSelectedExp );
			pExp->m_pMeshExp->SetApplyScale( pExp->GetApplyScale() );

			pExp->m_pMeshExp->Initialize();
			pExp->m_pMeshExp->OutputInformation();
			SetCursor( LoadCursor( NULL, IDC_ARROW ) );	

			hCheckVertColor = GetDlgItem( hDlg, IDC_CHECK_VC );
			hCheckTangent = GetDlgItem( hDlg, IDC_CHECK_TANGENT );
			hCheckPackVertex = GetDlgItem( hDlg, IDC_PACK_VERTEX );
		}
		return TRUE;

	case WM_CLOSE:
		{
			RdOutputExp::Instance()->Close();
			EndDialog( hDlg, 0 );
		}
		return TRUE;

	case WM_COMMAND:
		{
//			if( HIWORD( wParam ) == BN_CLICKED )
//			{
				switch( LOWORD( wParam ) )
				{
				case IDC_MESH_CANCEL:
					{
						EndDialog( hDlg, 0 );
					}
					break;

				case IDC_MESH_SAVE:
					{
						SetCursor( LoadCursor( NULL, IDC_WAIT ) );

						TCHAR buffer[256];
						_stprintf( buffer, _T("%s.mtl"), pExp->m_strFileName );
						//pExp->m_pMeshExp->ExportMaterial( buffer );
						pExp->m_pMeshExp->ExportMtl( buffer );

						_stprintf( buffer, _T( "%s.mmf" ), pExp->m_strFileName );
						pExp->m_pMeshExp->ExportMesh( buffer );
						SetCursor( LoadCursor( NULL, IDC_ARROW ) );

						if( IDOK == MessageBox( GetActiveWindow(), _T("Complete to Export Mesh."), _T("Mesh Export"), MB_OK ) )
						{
						//	EndDialog( hDlg, 1 );						
						}
					}
					break;

				case IDC_ALPHA_ADD:
					{
						int num = SendDlgItemMessage( hDlg, IDC_MATERIAL_LIST, LB_GETSELCOUNT, 0, 0 );
						if( num != 0 )
						{
							int buffer[100];
							TCHAR str[128];
							SendDlgItemMessage( hDlg, IDC_MATERIAL_LIST, LB_GETSELITEMS, 100, (LPARAM)buffer );
							for( int i = 0; i < num; ++i )
							{
								SendDlgItemMessage( hDlg, IDC_MATERIAL_LIST, LB_GETTEXT, buffer[i], (LPARAM)str );
								AddStringInListBox( hDlg, IDC_ALPHA_LIST, str );
							}
						}
					}
					break;

				case IDC_ALPHA_DEL:
					{
						int num = SendDlgItemMessage( hDlg, IDC_ALPHA_LIST, LB_GETSELCOUNT, 0, 0 );
						if( num != 0 )
						{
							int buffer[100];
							SendDlgItemMessage( hDlg, IDC_ALPHA_LIST, LB_GETSELITEMS, 100, (LPARAM)buffer );
							for( int i = num-1; i > -1; --i )
							{
								SendDlgItemMessage( hDlg, IDC_ALPHA_LIST, LB_DELETESTRING, buffer[i], 0 );
							}
						}
					}
					break;

				// VertexColor �̱�
				case IDC_CHECK_VC:
					{
						if( SendMessage( hCheckVertColor, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheckVertColor, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pMeshExp->UseVertexColor( true );
						}
						else
						{
							SendMessage( hCheckVertColor, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pMeshExp->UseVertexColor( false );
						}
						InvalidateRect( hCheckVertColor, NULL, TRUE );
					}
					break;

				/// Tangent Vector �̱�
				case IDC_CHECK_TANGENT:
					{
						if( SendMessage( hCheckTangent, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheckTangent, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pMeshExp->UseVertTangent( true );
						}
						else
						{
							SendMessage( hCheckTangent, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pMeshExp->UseVertTangent( false );
						}
						InvalidateRect( hCheckTangent, NULL, TRUE );
					}
					break;

				/// vertex packing
				case IDC_PACK_VERTEX:
					{
						if( SendMessage( hCheckPackVertex, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheckPackVertex, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pMeshExp->PackVertex( true );
						}
						else
						{
							SendMessage( hCheckPackVertex, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pMeshExp->PackVertex( false );
						}
						InvalidateRect( hCheckPackVertex, NULL, TRUE );
					}
					break;
				} // switch
			//}
			return TRUE;
		}
	}

	return FALSE;
}

//------------------------------------------------------------------
/**
*/
INT_PTR CALLBACK SkelExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax2008Exp* pExp = NULL;
	static HWND hCheck[15];
	static HWND hList;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );

			SetCursor( LoadCursor( NULL, IDC_WAIT ) );
			{
				pExp = (RdMax2008Exp*)lParam;
				pExp->m_pSkelExp = new RdSkelExp( pExp->m_pIFace, hDlg );
				pExp->m_pSkelExp->Initialize();
				pExp->m_pSkelExp->OutputInformation();
			}
			SetCursor( LoadCursor( NULL, IDC_ARROW ) );
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog( hDlg, 0 );
		}
		return TRUE;

	case WM_COMMAND:
		{
//			if( HIWORD( wParam ) == BN_CLICKED )
			{
				switch( LOWORD( wParam ) )
				{
				case IDC_SKEL_CANCEL:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );

						EndDialog( hDlg, 0 );
					}
					break;

				case IDC_SKEL_SAVE:
					{
						// Export
						SetCursor( LoadCursor( NULL, IDC_WAIT ) );
						_stprintf( pExp->m_strFileName, _T( "%s.mbf" ), pExp->m_strFileName );
						pExp->m_pSkelExp->ExportSkel( pExp->m_strFileName );
						SetCursor( LoadCursor( NULL, IDC_ARROW ) );	

						if( IDOK == MessageBox( GetActiveWindow(), _T("Complete to Export Skeleton."), _T("Skeleton Export"), MB_OK ) )
						{
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );

							EndDialog( hDlg, 1 );						
						}
					}
					break;
				}
			}
			return TRUE;
		}
	}

	return FALSE;
}

//------------------------------------------------------------------
/**
*/
INT_PTR CALLBACK PropertyExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax2008Exp* pExp = NULL;
	static HWND hCheck;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );

			SetCursor( LoadCursor( NULL, IDC_WAIT ) );
			pExp = (RdMax2008Exp*)lParam;
			SetCursor( LoadCursor( NULL, IDC_ARROW ) );	
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog( hDlg, 0 );
		}
		return TRUE;

	case WM_COMMAND:
		{
			if( HIWORD( wParam ) == BN_CLICKED )
			{
				switch( LOWORD( wParam ) )
				{
				case IDC_SCALE:
					{
						if( SendMessage( hCheck, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->SetApplyScale( true );
						}
						else
						{
							SendMessage( hCheck, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->SetApplyScale( false );
						}
						InvalidateRect( hCheck, NULL, TRUE );
					}
					break;

				case IDC_SAMPLE:
					{
						if( SendMessage( hCheck, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->SetUseKeys( true );
						}
						else
						{
							SendMessage( hCheck, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->SetUseKeys( false );
						}
						InvalidateRect( hCheck, NULL, TRUE );
					}
					break;

				case IDOK:
					{
						if (g_nCurExp == IDC_MESH_EXPORT)
						{
							EndDialog( hDlg, 0 );
							DialogBoxParam( hInstance, MAKEINTRESOURCE( IDD_MESH_DLG ), GetActiveWindow(),
													MeshExportDlgProc, (LPARAM)pExp );	
						}

						if (g_nCurExp == IDC_ANI_EXPORT)
						{
							SetCursor( LoadCursor( NULL, IDC_WAIT ) );
							pExp->m_pNodeAnimExp = new RdNodeAnimExp( pExp->m_pIFace );
							pExp->m_pNodeAnimExp->SetUseKeys( pExp->GetUseKeys() );
							pExp->m_pNodeAnimExp->Initialize();

							_stprintf( pExp->m_strFileName, _T( "%s.maf" ), pExp->m_strFileName );
							pExp->m_pNodeAnimExp->ExportAnim( pExp->m_strFileName );
							SetCursor( LoadCursor( NULL, IDC_ARROW ) );	

							if( IDOK == MessageBox( GetActiveWindow(), _T("Complete to Export Animation."), _T("Node Animation Export"), MB_OK ) )
								EndDialog( hDlg, 1 );
						}
					}
					break;

				case IDCANCEL:
					{
						EndDialog( hDlg, 0 );
					}
					break;
				}
			}
		}
		return TRUE;
	}

	return FALSE;
}

//------------------------------------------------------------------
// Exporter Class
//------------------------------------------------------------------
RdMax2008Exp::RdMax2008Exp()
{
	m_pExpIFace		= NULL;
	m_pIFace		= NULL;

	m_bSelectedExp	= false;
	m_bApplyScale	= false;
	m_bUseKeys		= false;

	memset( m_strFileName, 0, sizeof(TCHAR)*MAX_LEN );

	m_pMeshExp		= NULL;
	m_pSkelExp		= NULL;
	m_pNodeAnimExp	= NULL;
}

RdMax2008Exp::~RdMax2008Exp()
{
	SAFEDEL( m_pMeshExp );
	SAFEDEL( m_pSkelExp );
	SAFEDEL( m_pNodeAnimExp );
}

int RdMax2008Exp::ExtCount()
{
	return 3;
}

const TCHAR* RdMax2008Exp::Ext( int n )
{
	if( n == 0 )
        return _T("mmf");
	else if( n == 1 )
		return _T("mbf");
	else if( n == 2 )
		return _T("maf");

	return _T("");
}

const TCHAR* RdMax2008Exp::LongDesc()
{
	return _T("Max Exporter Data File For 'CrazyXIII(Cindy)'" ); 
}

const TCHAR* RdMax2008Exp::ShortDesc()
{
	return _T("Cindy's Data File" );
}

const TCHAR* RdMax2008Exp::AuthorName()
{
	return _T( "cagetu(http://cagetu.egloos.com)" );
}

const TCHAR* RdMax2008Exp::CopyrightMessage()
{
	return _T( "Copyright (c)2005, cagetu" );
}

const TCHAR* RdMax2008Exp::OtherMessage1()
{
	return _T("");
}

const TCHAR* RdMax2008Exp::OtherMessage2()
{
	return _T("");
}

unsigned int RdMax2008Exp::Version()
{
	return 100;
}

void RdMax2008Exp::ShowAbout( HWND hWnd )
{

}

BOOL RdMax2008Exp::SupportsOptions( int ext, DWORD options )
{
	assert( 3 > ext );
	return	( options == SCENE_EXPORT_SELECTED ) ? TRUE : FALSE;
}

int RdMax2008Exp::DoExport( const TCHAR* name, ExpInterface* ei, Interface *i, BOOL suppressPrompts, DWORD options )
{
	m_pExpIFace = ei;
	m_pIFace = i;

	size_t len = _tcslen( name );
	len = len - 4;
	_tcsncpy( m_strFileName, name, len );

	m_bSelectedExp = ( options & SCENE_EXPORT_SELECTED ) ? true : false;

	CountNode();

	if( m_bSelectedExp )
	{
		DialogBoxParam( hInstance, MAKEINTRESOURCE( IDD_PROPERTY_DLG ), GetActiveWindow(),
						PropertyExportDlgProc, (LPARAM)this );	
	}
	else
	{
		DialogBoxParam( hInstance, MAKEINTRESOURCE( IDD_EXPORT_TYPE ), GetActiveWindow(), ExportDlgProc, 
						(LPARAM)this );	
	}

	return 1;
}

//------------------------------------------------------------------
void RdMax2008Exp::CountNode()
{
	CountNodeTEP procCountNode;
	procCountNode.m_nNodeCount = 1000;
	procCountNode.m_nBoneCount = 0;

	(void)m_pExpIFace->theScene->EnumTree( &procCountNode );
}

//------------------------------------------------------------------
int CountNodeTEP::callback( INode* pNode )
{
	if( !UndesirableNode( pNode ) )
	{
		if( IsBoneNode( pNode ) || ( IsMeshNode( pNode ) && IsBoneDummyNode( pNode->GetName() ) ) )
		{
			SetIndexOfNode( pNode, m_nBoneCount );
			++m_nBoneCount;
		}
		else if( IsMeshNode( pNode ) )
		{
			if( IsBoundingBoxNode( pNode->GetName() ) )
				return TREE_CONTINUE;
				
			SetIndexOfNode( pNode, m_nNodeCount );
			++m_nNodeCount;
		}
	}

	return TREE_CONTINUE;
}
