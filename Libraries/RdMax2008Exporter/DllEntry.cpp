#include "RdMax2008Exporter.h"

extern ClassDesc2* GetRdMaxExporterDesc();

HINSTANCE hInstance;
int controlIsInit = FALSE;

BOOL WINAPI DllMain( HINSTANCE hInstanceDll, DWORD dwReason, LPVOID )
{
	//hInstance = hInstanceDll;

	//if( !controlIsInit )
	//{
	//	controlIsInit = TRUE;
	//	InitCustomControls( hInstance );
	//	InitCommonControls();
	//}
   if( dwReason == DLL_PROCESS_ATTACH )
   {
      hInstance = hInstanceDll;
      DisableThreadLibraryCalls(hInstance);
   }

	return TRUE;
}

// This function returns a string that describes the DLL and where the user
// could purchase the DLL if they don't have it.
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

// This function returns the number of plug-in classes this DLL
//TODO: Must change this number when adding a new class
__declspec( dllexport ) int LibNumberClasses()
{
	return 1;
}

// This function returns the number of plug-in classes this DLL
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: return GetRdMaxExporterDesc();
		default: return 0;
	}
}

// This function returns a pre-defined constant indicating the version of 
// the system under which it was compiled.  It is used to allow the system
// to catch obsolete DLLs.
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}