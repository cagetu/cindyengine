//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RdMax2008Exporter.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDOK                            1
#define IDS_CATEGORY                    2
#define IDCANCEL                        2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDD_EXPORT_TYPE                 103
#define IDD_MESH_DLG                    104
#define IDD_OUTPUT_DLG                  105
#define IDD_PROPERTY_DLG                107
#define IDD_SKEL_DLG                    108
#define IDC_CHECK1                      1001
#define IDC_PACK_VERTEX                 1001
#define IDC_CHECK_OUTPUT                1001
#define IDC_MESH_EXPORT                 1002
#define IDC_OUTPUT_LIST                 1002
#define IDC_SKEL_EXPORT                 1003
#define IDC_MATERIAL_LIST               1003
#define IDC_OUTPUT_OK                   1003
#define IDC_ANI_EXPORT                  1004
#define IDC_ALPHA_LIST                  1004
#define IDC_ALPHA_DEL                   1006
#define IDC_VERTEX_CNT                  1007
#define IDC_FACE_CNT                    1008
#define IDC_BONE_CNT                    1009
#define IDC_LINK_CNT                    1010
#define IDC_MESH_LIST                   1011
#define IDC_MESH_SAVE                   1012
#define IDC_MESH_CANCEL                 1013
#define IDC_ALPHA_ADD                   1014
#define IDC_OBJECT_CNT                  1015
#define IDC_MDUMMY_CNT                  1016
#define IDC_SKEL_SAVE                   1016
#define IDC_SKEL_CANCEL                 1017
#define IDC_BBOX_CNT                    1024
#define IDC_BONE_LISTBOX                1038
#define IDC_SCALE                       1039
#define IDC_CHECK_VC                    1052
#define IDC_SAMPLE                      1053
#define IDC_CHECK_VC2                   1053
#define IDC_CHECK_TANGENT               1053

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
