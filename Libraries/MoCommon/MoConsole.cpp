// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoCommon.h"
#include "MoConsole.h"
#include "MoStringUtil.h"

namespace MoCommon
{
	//----------------------------------------------------------------
	MoConsole::MoConsole()
		: m_bIsCreated(false)
		, m_hHandle(0)
	{
	}
	MoConsole::~MoConsole()
	{
	}

	//----------------------------------------------------------------
	bool MoConsole::Create()
	{
		if (!m_bIsCreated)
		{
			::AllocConsole();

			m_hHandle = ::GetStdHandle( STD_OUTPUT_HANDLE );
			m_bIsCreated = m_hHandle != NULL;
		}

		return m_bIsCreated;
	}

	//----------------------------------------------------------------
	void MoConsole::Destroy()
	{
		if (m_bIsCreated)
		{
			m_bIsCreated = false;
			::FreeConsole();
		}
	}

	//----------------------------------------------------------------
	void MoConsole::Clear()
	{
		if (m_bIsCreated)
		{
			CONSOLE_SCREEN_BUFFER_INFO scInfo;
			COORD pos = {0, 0};
			unsigned long written;

			if (::GetConsoleScreenBufferInfo( m_hHandle, &scInfo ))
			{
				::FillConsoleOutputCharacter( m_hHandle, ' ', scInfo.dwSize.X * scInfo.dwSize.Y, pos, &written );
			}
		}
	}

	//----------------------------------------------------------------
	void MoConsole::Locate( int nX, int nY )
	{
		if (m_bIsCreated)
		{
			COORD pos = { (short)nX, (short)nY };

			::SetConsoleCursorPosition( m_hHandle, pos );
		}
	}

	//----------------------------------------------------------------
	void MoConsole::Print( const MoString& strMsg )
	{
		if (m_bIsCreated)
		{
			static MoString string;
			static char buffer[2048];
			static unsigned long writesize;

			string = strMsg;
			MoStringUtil::WidecharToMultibyte( string.c_str(), buffer );
			::WriteFile( m_hHandle, buffer, (unsigned long)strlen(buffer), &writesize, 0 );
		}
	}
	void MoConsole::Print( int nX, int nY, const MoString& strMsg )
	{
		if (m_bIsCreated)
		{
			Locate( nX, nY );

			static MoString string;
			static char buffer[2048];
			static unsigned long writesize;

			string = strMsg;
			MoStringUtil::WidecharToMultibyte( string.c_str(), buffer );
			::WriteFile( m_hHandle, buffer, (unsigned long)strlen(buffer), &writesize, 0 );
		}
	}

	//----------------------------------------------------------------
	/**
		@brief 색상 변경
		@param Attributes
							폰트 색상					<BR>
								FOREGROUND_BLUE			<BR>
								FOREGROUND_GREEN		<BR>
								FOREGROUND_RED			<BR>
								FOREGROUND_INTENSITY	<BR>
							배경색						<BR>
								BACKGROUND_BLUE			<BR>
								BACKGROUND_GREEN		<BR>
								BACKGROUND_RED			<BR>
								BACKGROUND_INTENSITY 	<BR>
	*/
	//----------------------------------------------------------------
	void MoConsole::SetColor( ushort usAttributes )
	{
		if (m_bIsCreated)
		{
			::SetConsoleTextAttribute( m_hHandle, usAttributes );
		}
	}
	ushort MoConsole::GetColor()
	{
		if (m_bIsCreated)
			return 0xffff;

		CONSOLE_SCREEN_BUFFER_INFO bufferInfo;
		::GetConsoleScreenBufferInfo( m_hHandle, &bufferInfo );
		return bufferInfo.wAttributes;
	}

	//----------------------------------------------------------------
	void MoConsole::EnableCursor( bool bOn )
	{
		if (m_bIsCreated)
		{
			CONSOLE_CURSOR_INFO info;

			::GetConsoleCursorInfo( m_hHandle, &info );

			info.bVisible = bOn;

			::GetConsoleCursorInfo( m_hHandle, &info );
		}
	}
}