//================================================================
// File					: MoCrashDump.cpp
// Related Header File	: MoCrashDump.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 17.
//================================================================
#include "../MoCommon.h"
#include "MoCrashDump.h"
#include "MoCrashGetWinVer.h"
#include "MoCrashDumpDefines.h"

#include <cstring>
#include <string>

#pragma warning( disable : 4312 )				// 64비트 호환성 문제 때문에 나는 워닝이라 확.. -_-;

namespace MoCommon
{
	typedef std::wstring	String;

	// define static variable
	MoCrashDump::DUMP_LEVEL		MoCrashDump::ms_eDumpLevel = MoCrashDump::DUMP_LEVEL_0;
	wchar						MoCrashDump::ms_szApplication[_MAX_PATH] = {0,};
	wchar						MoCrashDump::ms_szInstallPath[_MAX_PATH] = {0,};
	wchar						MoCrashDump::ms_szOutputFile[_MAX_PATH] = {0,};

	//------------------------------------------------------
	// CrashDump Class
	//------------------------------------------------------
	//// Const/Dest
	MoCrashDump::MoCrashDump( const wchar* p_strInstallPath, const wchar* p_strOutputFile, DUMP_LEVEL p_eLevel )
	{
		ms_eDumpLevel = p_eLevel;

		// 어플리케이션 이름을 얻어온다.
		wchar r_modulefile[_MAX_PATH];
		::GetModuleFileName( NULL, r_modulefile, _MAX_PATH );
		wchar* r_lpszDot = wcsrchr( r_modulefile, '.');
		::lstrcpyn( ms_szApplication, r_modulefile, (int)(r_lpszDot - r_modulefile + 1));

		wmemcpy( ms_szOutputFile, p_strOutputFile, _MAX_PATH );
		wmemcpy( ms_szInstallPath, p_strInstallPath, _MAX_PATH );

		//! 예외처리 핸들러 설정
		::SetUnhandledExceptionFilter( ExceptionFilter );
	}

	//================================================================
	/** ExceptionFilter
		@remarks	예외에 대한 정보를 얻는다.
		@param		p_pException : 예외 정보 구조체
		@return		result : 처리 결과
	*/
	//================================================================
	LONG __stdcall 
	MoCrashDump::ExceptionFilter( _EXCEPTION_POINTERS* p_pException )
	{
		LONG r_result = EXCEPTION_CONTINUE_SEARCH;

		//******************************************************************
		// 먼저 실행 파일이 있는 디렉토리에서 DBGHELP.DLL을 로드해 본다.
		// Windows 2000 의 System32 디렉토리에 있는 DBGHELP.DLL 파일은 버전이 
		// 오래된 것일 수 있기 때문이다. (최소 5.1.2600.0 이상이어야 한다.)
		//******************************************************************

		String dbghelp = String( ms_szInstallPath ) + String( L"DBGHELP.DLL" );
		HMODULE hDll = LoadLibrary( dbghelp.c_str() );
		if( NULL == hDll )
		{	// 현재 디렉토리에 없다면, 아무 버전의 DBGHELP.DLL을 읽는다.
			hDll = LoadLibrary( L"DBGHELP.DLL" );
			if( NULL == hDll )
			{
				OutputDebugString( L"[MoCrashDump::ExceptionFilter]: 'DBGHELP.DLL'을 찾을 수가 없다" );
				return r_result;
			}
			OutputDebugString( L"[MoCrashDump::ExceptionFilter]: Windows의'DBGHELP.DLL'을 로드" );
		}

		wchar r_outputfile[_MAX_PATH];
		GetDumpFileName( r_outputfile );

		r_result = CreateDumpFile( hDll, r_outputfile, p_pException );
		if( EXCEPTION_EXECUTE_HANDLER == r_result )
		{
		}

		return r_result;
	}

	//================================================================
	/** CreateDumpFile
		@remarks	예외에 대한 정보를 얻는다.
		@param		p_strResult : 결과
		@return		none
	*/
	//================================================================
	void 
	MoCrashDump::GetDumpFileName( wchar* p_strResult )
	{
		SYSTEMTIME r_time;
		GetLocalTime( &r_time );

		wchar r_tail[_MAX_PATH];
		_snwprintf_s( r_tail, sizeof(r_tail) - 1, _MAX_PATH, L"-%s_Exception-[%04d%02d%02d %02d-%02d-%02d]",
			ms_szApplication, r_time.wYear, r_time.wMonth, r_time.wDay, r_time.wHour, r_time.wMinute, r_time.wSecond );

		lstrcat( p_strResult, ms_szOutputFile );
		lstrcat( p_strResult, r_tail );
		lstrcat( p_strResult, L".dmp" );
	}

	//================================================================
	/** CreateDumpFile
		@remarks	메모리 덤프 파일을 생성한다.
		@param		p_hDll : dbghelp 라이브러리 핸들
		@param		p_strFileName : 덤프 파일이름
		@param		p_pException : 예외 정보 구조체
		@return		result : 처리 결과
	*/
	//================================================================
	LONG 
	MoCrashDump::CreateDumpFile( HMODULE p_hDll, const wchar* p_strFileName, EXCEPTION_POINTERS* p_pException )
	{
		LONG r_result = EXCEPTION_CONTINUE_SEARCH;

		// DLL 내부에서 MiniDumpWriteDump API를 찾는다.
		MINIDUMPWRITEDUMP pfnMiniDumpWriteDump = (MINIDUMPWRITEDUMP)::GetProcAddress( p_hDll, "MiniDumpWriteDump" );
		if( NULL == pfnMiniDumpWriteDump )
		{
			OutputDebugString( L"[MoCrashDump::CreateDumpFile]: 'MiniDumpWriteDump API'를 찾을 수가 없다" );
			return r_result;
		}

		HANDLE hFile = ::CreateFile( p_strFileName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
		if( INVALID_HANDLE_VALUE != hFile )
		{
			MINIDUMP_EXCEPTION_INFORMATION ExceptionParam;

			ExceptionParam.ThreadId = ::GetCurrentThreadId();		//! 쓰레드 ID
			ExceptionParam.ExceptionPointers = p_pException;
			ExceptionParam.ClientPointers = FALSE;

			//! 옵션에 따른 덤프 파일 생성
			BOOL r_bSuccess = FALSE;
			switch( ms_eDumpLevel )
			{
			// MiniDumpNormal
			case DUMP_LEVEL_0:
				r_bSuccess = pfnMiniDumpWriteDump( ::GetCurrentProcess(), ::GetCurrentProcessId(), hFile,
												 MiniDumpNormal, &ExceptionParam, NULL, NULL );
				break;

			// MiniDumpWithDataSegs
			case DUMP_LEVEL_1:
				r_bSuccess = pfnMiniDumpWriteDump( ::GetCurrentProcess(), ::GetCurrentProcessId(), hFile,
												 MiniDumpWithDataSegs, &ExceptionParam, NULL, NULL );
				break;

			// MiniDumpWithFullMemory
			case DUMP_LEVEL_2:
				r_bSuccess = pfnMiniDumpWriteDump( ::GetCurrentProcess(), ::GetCurrentProcessId(), hFile,
												 MiniDumpWithFullMemory, &ExceptionParam, NULL, NULL );
				break;
			default:
				r_bSuccess = pfnMiniDumpWriteDump( ::GetCurrentProcess(), ::GetCurrentProcessId(), hFile,
												 MiniDumpNormal, &ExceptionParam, NULL, NULL );
				break;
			}

			//! 성공했당..
			if( r_bSuccess )
			{
				// 덤프 파일을 생성
				r_result = EXCEPTION_EXECUTE_HANDLER;
			}

			CloseHandle( hFile );
		}

		return r_result;
	}

	//================================================================
	/** CreateLogFile
		@remarks	예외에 대한 Log파일을 생성한다.
		@param		p_pException : 예외 정보 구조체
		@return		true / false : 생성 여부
	*/
	//================================================================
	bool 
	MoCrashDump::CreateLogFile( EXCEPTION_POINTERS *p_pException )
	{
		String r_filename = String( ms_szOutputFile );
		r_filename += String( L"_ExceptionLog.txt" );

		HANDLE hFile = CreateFile( r_filename.c_str(), GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH, 0 );
		if( hFile )
		{
			PEXCEPTION_RECORD Exception	= p_pException->ExceptionRecord;
			PCONTEXT Context = p_pException->ContextRecord;

			TCHAR r_szCrashModulePathName[MAX_PATH*2];
			ZeroMemory( r_szCrashModulePathName, sizeof(r_szCrashModulePathName) );

			TCHAR* r_pszCrashModuleFileName = L"Unknown";

			MEMORY_BASIC_INFORMATION MemInfo;

			// VirtualQuery can be used to get the allocation base associated with a
			// code address, which is the same as the ModuleHandle. This can be used
			// to get the filename of the module that the crash happened in.
			if( VirtualQuery( (void*)Context->Eip, &MemInfo, sizeof(MemInfo) ) &&
				(GetModuleFileName((HINSTANCE)MemInfo.AllocationBase, r_szCrashModulePathName, sizeof(r_szCrashModulePathName) - 2) > 0) )
			{
				r_pszCrashModuleFileName = MoCrashDumpUtil::GetFilePart( r_szCrashModulePathName );
			}

			TCHAR r_szDesc[MAX_PATH];
			GetExceptionDesc( p_pException, r_szDesc );

			// Print out the beginning of the error log in a Win95 error window
			// compatible format.
			MoCrashDumpUtil::hprintf(	hFile, _T("caused %s (0x%08x) \r\nin module %s at %04x:%08x.\r\n\r\n"),
										r_szDesc,
										Exception->ExceptionCode,
										r_pszCrashModuleFileName, 
										Context->SegCs,
										Exception->ExceptionAddress/** Context->Eip */);


		}
		CloseHandle( hFile );

		return true;
	}

	//================================================================
	/** CreateLogFile
		@remarks	예외에 대한 Log파일을 생성한다.
		@param		p_pException : 예외 정보 구조체
		@return		true / false : 생성 여부
	*/
	//================================================================
	void 
	MoCrashDump::GetExceptionDesc( _EXCEPTION_POINTERS* p_pExceptionInfo, wchar* p_pResult )
	{
		if ( ::IsBadReadPtr( p_pExceptionInfo, sizeof(EXCEPTION_POINTERS)) ) 
		{
			lstrcpy( p_pResult, L"BAD EXCEPTION POINTERS" );
			return;
		}

		// 간단한 에러 코드라면 그냥 변환할 수 있다.
		// ms-help://MS.VSCC.2003/MS.MSDNQTR.2003FEB.1042/debug/base/exception_record_str.htm
		switch ( p_pExceptionInfo->ExceptionRecord->ExceptionCode )
		{
		case EXCEPTION_ACCESS_VIOLATION:         lstrcpy( p_pResult, _T("EXCEPTION_ACCESS_VIOLATION") );		break;
		case EXCEPTION_DATATYPE_MISALIGNMENT:    lstrcpy( p_pResult, _T("EXCEPTION_DATATYPE_MISALIGNMENT") );	break;
		case EXCEPTION_BREAKPOINT:               lstrcpy( p_pResult, _T("EXCEPTION_BREAKPOINT") );				break;
		case EXCEPTION_SINGLE_STEP:              lstrcpy( p_pResult, _T("EXCEPTION_SINGLE_STEP") );				break;
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:    lstrcpy( p_pResult, _T("EXCEPTION_ARRAY_BOUNDS_EXCEEDED") );	break;
		case EXCEPTION_FLT_DENORMAL_OPERAND:     lstrcpy( p_pResult, _T("EXCEPTION_FLT_DENORMAL_OPERAND") );	break;
		case EXCEPTION_FLT_DIVIDE_BY_ZERO:       lstrcpy( p_pResult, _T("EXCEPTION_FLT_DIVIDE_BY_ZERO") );		break;
		case EXCEPTION_FLT_INEXACT_RESULT:       lstrcpy( p_pResult, _T("EXCEPTION_FLT_INEXACT_RESULT") );		break;
		case EXCEPTION_FLT_INVALID_OPERATION:    lstrcpy( p_pResult, _T("EXCEPTION_FLT_INVALID_OPERATION") );	break;
		case EXCEPTION_FLT_OVERFLOW:             lstrcpy( p_pResult, _T("EXCEPTION_FLT_OVERFLOW") );			break;
		case EXCEPTION_FLT_STACK_CHECK:          lstrcpy( p_pResult, _T("EXCEPTION_FLT_STACK_CHECK") );			break;
		case EXCEPTION_FLT_UNDERFLOW:            lstrcpy( p_pResult, _T("EXCEPTION_FLT_UNDERFLOW" ) );			break;
		case EXCEPTION_INT_DIVIDE_BY_ZERO:       lstrcpy( p_pResult, _T("EXCEPTION_INT_DIVIDE_BY_ZERO") );		break;
		case EXCEPTION_INT_OVERFLOW:             lstrcpy( p_pResult, _T("EXCEPTION_INT_OVERFLOW") );			break;
		case EXCEPTION_PRIV_INSTRUCTION:         lstrcpy( p_pResult, _T("EXCEPTION_PRIV_INSTRUCTION") );		break;
		case EXCEPTION_IN_PAGE_ERROR:            lstrcpy( p_pResult, _T("EXCEPTION_IN_PAGE_ERROR") );			break;
		case EXCEPTION_ILLEGAL_INSTRUCTION:      lstrcpy( p_pResult, _T("EXCEPTION_ILLEGAL_INSTRUCTION") );		break;
		case EXCEPTION_NONCONTINUABLE_EXCEPTION: lstrcpy( p_pResult, _T("EXCEPTION_NONCONTINUABLE_EXCEPTION") );break;
		case EXCEPTION_STACK_OVERFLOW:           lstrcpy( p_pResult, _T("EXCEPTION_STACK_OVERFLOW") );			break;
		case EXCEPTION_INVALID_DISPOSITION:      lstrcpy( p_pResult, _T("EXCEPTION_INVALID_DISPOSITION") );		break;
		case EXCEPTION_GUARD_PAGE:               lstrcpy( p_pResult, _T("EXCEPTION_GUARD_PAGE") );				break;
		case EXCEPTION_INVALID_HANDLE:           lstrcpy( p_pResult, _T("EXCEPTION_INVALID_HANDLE") );			break;
		case 0xE06D7363:                         lstrcpy( p_pResult, _T("Microsoft C++ Exception") );			break;
		default:
			{
				lstrcpy( p_pResult, L"Unknown" ); 
				::FormatMessage( FORMAT_MESSAGE_FROM_HMODULE | FORMAT_MESSAGE_IGNORE_INSERTS,
								::GetModuleHandle( L"NTDLL.DLL" ),
								p_pExceptionInfo->ExceptionRecord->ExceptionCode, 
								0, p_pResult, 0, NULL );
			}
			break;
		}
	}

	//------------------------------------------------------
	// CrashLog Class
	//------------------------------------------------------
	//// Const / Dest
	MoCrashLog::MoCrashLog( const wchar* p_strFileName, EXCEPTION_POINTERS* p_pException )
	{
		if( WriteBegin( p_strFileName, p_pException ) )
		{
			WriteSystemInformation();
			WriteExceptionAddress( p_pException->ExceptionRecord );
			WriteRegister( p_pException->ContextRecord );
			WriteStack( p_pException->ContextRecord );
			WriteCallStack( p_pException );
		}
		WriteEnd();
	}

	//================================================================
	/** Write Begin
		@remarks	파일을 쓰기 시작
		@param		p_strFileName : 파일 이름
		@param		p_pException : 예외 정보 구조체
		@return		true / false : 쓰기 가능 여부
	*/
	//================================================================
	bool 
	MoCrashLog::WriteBegin( const wchar* p_strFileName, EXCEPTION_POINTERS* p_pException )
	{
		m_hFile = CreateFile( p_strFileName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH, 0 );
		if( NULL == m_hFile )
			return false;

		TCHAR r_szCrashModulePathName[MAX_PATH*2];
		ZeroMemory( r_szCrashModulePathName, sizeof(r_szCrashModulePathName) );

		TCHAR* r_pszCrashModuleFileName = L"Unknown";

		MEMORY_BASIC_INFORMATION MemInfo;

		PEXCEPTION_RECORD Exception	= p_pException->ExceptionRecord;
		PCONTEXT Context = p_pException->ContextRecord;

		// VirtualQuery can be used to get the allocation base associated with a
		// code address, which is the same as the ModuleHandle. This can be used
		// to get the filename of the module that the crash happened in.
		if( VirtualQuery( (void*)Context->Eip, &MemInfo, sizeof(MemInfo) ) &&
			(GetModuleFileName((HINSTANCE)MemInfo.AllocationBase, r_szCrashModulePathName, sizeof(r_szCrashModulePathName) - 2) > 0) )
		{
			r_pszCrashModuleFileName = MoCrashDumpUtil::GetFilePart( r_szCrashModulePathName );
		}

		TCHAR r_szDesc[MAX_PATH];
		MoCrashDump::GetExceptionDesc( p_pException, r_szDesc );

		// Print out the beginning of the error log in a Win95 error window
		// compatible format.
		MoCrashDumpUtil::hprintf(	m_hFile, _T("caused %s (0x%08x) \r\nin module %s at %04x:%08x.\r\n\r\n"),
									r_szDesc,
									Exception->ExceptionCode,
									r_pszCrashModuleFileName, 
									Context->SegCs,
									Exception->ExceptionAddress/** Context->Eip */);

		return true;
	}

	//================================================================
	/** write End
		@remarks	파일을 쓰기 끝
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoCrashLog::WriteEnd()
	{
		CloseHandle( m_hFile );
	}

	//================================================================
	/** write System Information
		@remarks	에러가 발생한 시간, 운영체제에 대한 정보를 알아오자..
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoCrashLog::WriteSystemInformation()
	{
		FILETIME r_CurrentTime;
		GetSystemTimeAsFileTime( &r_CurrentTime );
		TCHAR r_szTimeBuffer[100];
		MoCrashDumpUtil::FormatTime( r_szTimeBuffer, r_CurrentTime );

		MoCrashDumpUtil::hprintf( m_hFile, _T("Error occurred at %s.\r\n"), r_szTimeBuffer );

		TCHAR r_szModuleName[MAX_PATH*2];
		ZeroMemory( r_szModuleName, sizeof(r_szModuleName) );
		if( GetModuleFileName( 0, r_szModuleName, _countof(r_szModuleName)-2) <= 0 )
			lstrcpy( r_szModuleName, _T("Unknown") );

		TCHAR r_szUserName[200];
		ZeroMemory( r_szUserName, sizeof(r_szUserName) );
		DWORD UserNameSize = _countof(r_szUserName)-2;
		if (!GetUserName(r_szUserName, &UserNameSize))
			lstrcpy( r_szUserName, _T("Unknown") );

		MoCrashDumpUtil::hprintf( m_hFile, _T("%s, run by %s.\r\n"), r_szModuleName, r_szUserName );

		// print out operating system
		TCHAR r_szWinVer[50], r_szMajorMinorBuild[50];
		int r_nWinVer;
		GetWinVer( r_szWinVer, &r_nWinVer, r_szMajorMinorBuild );
		MoCrashDumpUtil::hprintf( m_hFile, _T("OS:  %s (%s).\r\n" ), 
				 r_szWinVer, r_szMajorMinorBuild );

		SYSTEM_INFO	SystemInfo;
		GetSystemInfo(&SystemInfo);
		MoCrashDumpUtil::hprintf( m_hFile, _T("Process: %d processor(s), type %d.\r\n"), 
					SystemInfo.dwNumberOfProcessors, SystemInfo.dwProcessorType );
	}

	//================================================================
	/** Write Exception Address
		@remarks	에러가 발생한 메모리 주소를 쓴다.
		@param		p_pExceptRecord : Exception Record
		@return		none
	*/
	//================================================================
	void 
	MoCrashLog::WriteExceptionAddress( PEXCEPTION_RECORD p_pExceptRecord )
	{
		// If the exception was an access violation, print out some additional
		// information, to the error log and the debugger.
		if ( p_pExceptRecord->ExceptionCode == STATUS_ACCESS_VIOLATION &&
			 p_pExceptRecord->NumberParameters >= 2)
		{
			TCHAR szDebugMessage[1000];
			const TCHAR* readwrite = _T("Read from");
			if ( p_pExceptRecord->ExceptionInformation[0] )
				readwrite = _T("Write to");

			wsprintf( szDebugMessage, _T("%s location %08x caused an access violation.\r\n"),
					  readwrite, p_pExceptRecord->ExceptionInformation[1]);

			MoCrashDumpUtil::hprintf( m_hFile, _T("%s"), szDebugMessage );
		}
	}

	//================================================================
	/** Write Register
		@remarks	레지스터 값을 저장한다.
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoCrashLog::WriteRegister( PCONTEXT p_pContext )
	{
		// Print out the register values in an XP error window compatible format.
		MoCrashDumpUtil::hprintf( m_hFile, _T("\r\n") );
		MoCrashDumpUtil::hprintf( m_hFile, _T("Context:\r\n") );
		MoCrashDumpUtil::hprintf( m_hFile, _T("EDI:    0x%08x  ESI: 0x%08x  EAX:   0x%08x\r\n"),
								 p_pContext->Edi, p_pContext->Esi, p_pContext->Eax );
		MoCrashDumpUtil::hprintf( m_hFile, _T("EBX:    0x%08x  ECX: 0x%08x  EDX:   0x%08x\r\n"),
								 p_pContext->Ebx, p_pContext->Ecx, p_pContext->Edx );
		MoCrashDumpUtil::hprintf( m_hFile, _T("EIP:    0x%08x  EBP: 0x%08x  SegCs: 0x%08x\r\n"),
								 p_pContext->Eip, p_pContext->Ebp, p_pContext->SegCs );
		MoCrashDumpUtil::hprintf( m_hFile, _T("EFlags: 0x%08x  ESP: 0x%08x  SegSs: 0x%08x\r\n"),
								 p_pContext->EFlags, p_pContext->Esp, p_pContext->SegSs );

		// Print out the bytes of code at the instruction pointer. Since the
		// crash may have been caused by an instruction pointer that was bad,
		// this code needs to be wrapped in an exception handler, in case there
		// is no memory to read. If the dereferencing of code[] fails, the
		// exception handler will print '??'.
		MoCrashDumpUtil::hprintf( m_hFile, _T("\r\nBytes at CS:EIP:\r\n") );
		BYTE* r_code = (BYTE*)p_pContext->Eip;
		for (int codebyte = 0; codebyte < NumCodeBytes; ++codebyte)
		{
			__try
			{
				MoCrashDumpUtil::hprintf( m_hFile, _T("%02x "), r_code[codebyte] );

			}
			__except(EXCEPTION_EXECUTE_HANDLER)
			{
				MoCrashDumpUtil::hprintf( m_hFile, _T("?? ") );
			}
		} // for
	}

	//================================================================
	/** Write Stack
		@remarks	스택 값을 저장한다.
		@param		p_pContext : Context Record
		@return		none
	*/
	//================================================================
	void 
	MoCrashLog::WriteStack( PCONTEXT p_pContext )
	{
		// Esp contains the bottom of the stack, or at least the bottom of
		// the currently used area
		DWORD* r_pStack = (DWORD*)p_pContext->Esp;

		MoCrashDumpUtil::hprintf( m_hFile, _T("\r\n\r\nStack:\r\n") );

		__try
		{
			// Esp contains the bottom of the stack, or at least the bottom of
			// the currently used area.
			DWORD* r_pStackTop;

			__asm
			{
				// Load the top (highest address) of the stack from the
				// thread information block. It will be found there in
				// Win9x and Windows NT.
				mov	eax, fs:[4]
				mov r_pStackTop, eax
			}

			if( r_pStackTop > r_pStack + MaxStackDump )
				r_pStackTop = r_pStack + MaxStackDump;

			int r_Count = 0;

			DWORD* r_pStackStart = r_pStack;

			int r_nDwordsPrinted = 0;

			while( r_pStack + 1 <= r_pStackTop )
			{
				if( (r_Count % StackColumns) == 0 )
				{
					r_pStackStart = r_pStack;
					r_nDwordsPrinted = 0;
					MoCrashDumpUtil::hprintf( m_hFile, _T("0x%08x: "), r_pStack );
				}

				if( (++r_Count % StackColumns) == 0 || r_pStack + 2 > r_pStackTop )
				{
					MoCrashDumpUtil::hprintf( m_hFile, _T("%08x "), *r_pStack );
					r_nDwordsPrinted++;

					int n = r_nDwordsPrinted;
					while( n < 4 )
					{
						MoCrashDumpUtil::hprintf( m_hFile, _T("         ") );
						n++;
					}

					for( int i = 0; i < r_nDwordsPrinted; i++ )
					{
						DWORD r_dwStack = *r_pStackStart;
						for( int j = 0; j < 4; j++ )
						{
							char c = (char)(r_dwStack & 0xFF);
							if (c < 0x20 || c > 0x7E)
								c = '.';
	#ifdef _UNICODE
							WCHAR w = (WCHAR)c;
							MoCrashDumpUtil::hprintf( m_hFile, _T("%c"), w );
	#else
							MoCrashDumpUtil::hprintf(m_hFile, _T("%c"), c);
	#endif
							r_dwStack = r_dwStack >> 8;
						}
						r_pStackStart++;
					}

					MoCrashDumpUtil::hprintf( m_hFile, _T("\r\n") );
				}
				else
				{
					MoCrashDumpUtil::hprintf( m_hFile, _T("%08x "), *r_pStack );
					r_nDwordsPrinted++;
				}
				r_pStack++;
			}
			MoCrashDumpUtil::hprintf( m_hFile, _T("\r\n") );
		}
		__except(EXCEPTION_EXECUTE_HANDLER)
		{
			MoCrashDumpUtil::hprintf( m_hFile, _T("Exception encountered during stack dump.\r\n") );
		}
	}

	//================================================================
	/** Write CallStack
		@remarks	호출스택 값을 저장한다.
		@param		p_pException : Exception
		@return		none
	*/
	//================================================================
	void 
	MoCrashLog::WriteCallStack( EXCEPTION_POINTERS* p_pException )
	{
		//MoCrashDumpUtil::hprintf( hLogFile, _T("\n\n[[Call Stack]]\n") );

		//DWORD dwOptions = GSTSO_MODULE | GSTSO_SYMBOL | GSTSO_SRCLINE;
		//LPCTSTR pStr = GetFirstCallStack( dwOptions, pException );
		//if( pStr ) 
		//{
		//	MoCrashDumpUtil::hprintf( hLogFile, _T("%s\n"), pStr );

		//	while ( pStr = GetNextCallStack( dwOptions, pException ) )
		//	{
		//		MoCrashDumpUtil::hprintf( hLogFile, _T("%s\n"), pStr );
		//	}
		//}

		//CleanSymbolEngine();
	}
}