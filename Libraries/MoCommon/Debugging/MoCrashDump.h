//================================================================
// File					: BcCrashDump.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 18.
//================================================================
#pragma once

#include <windows.h>

namespace MoCommon
{
	//==================================================================
	/** CrashDump Class
		@author		changhee
		@since		2007.1.18
		@remarks	MiniDump 생성기

			<참고>
			- http://serious-code.net/moin.cgi/MiniDump
			- http://serious-code.net/moin.cgi/CallStackTracing
			- http://www.codeproject.com/debug/XCrashReportPt4.asp
			- Debugging Applications .NET Windows - 13장 충돌 핸들러
	*/
	//==================================================================
	class MOCOM_DLL MoCrashDump
	{
		friend class MoCrashLog;
	public:
		enum DUMP_LEVEL	// 덤프할 데이터의 수준
		{
			DUMP_LEVEL_0, ///< MiniDumpNormal을 사용
			DUMP_LEVEL_1, ///< MiniDumpWithDataSegs를 사용
			DUMP_LEVEL_2  ///< MiniDumpWithFullMemory를 사용
		};

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		static wchar			ms_szApplication[_MAX_PATH];
		static wchar			ms_szInstallPath[_MAX_PATH];
		static wchar			ms_szOutputFile[_MAX_PATH];

		static DUMP_LEVEL		ms_eDumpLevel;

		//------------------------------------------------------
		// Constructor
		//------------------------------------------------------
		MoCrashDump();
		MoCrashDump( const MoCrashDump& );
		MoCrashDump& operator = ( const MoCrashDump& );

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		static LONG __stdcall	ExceptionFilter( struct _EXCEPTION_POINTERS* p_pException );
		static void				GetExceptionDesc( _EXCEPTION_POINTERS* p_pExceptionInfo, wchar* p_pResult );

		static LONG				CreateDumpFile( HMODULE p_hDll, const wchar* p_strFileName, EXCEPTION_POINTERS* p_pException );
		static void				GetDumpFileName( wchar* p_strResult );

		static bool				CreateLogFile( EXCEPTION_POINTERS* p_pException );
	public:
		MoCrashDump( const wchar* p_strInstallPath, const wchar* p_strOutputFile = L"CrashDump", DUMP_LEVEL p_eLevel = DUMP_LEVEL_0 );
	};

	//==================================================================
	/** CrashLog Class
		@author		changhee
		@since		2007.1.18
		@remarks	오류 로그
	*/
	//==================================================================
	class MoCrashLog
	{
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		HANDLE		m_hFile;

		bool	WriteBegin( const wchar* p_strFileName, EXCEPTION_POINTERS* p_pException );
		void	WriteSystemInformation();
		void	WriteExceptionAddress( PEXCEPTION_RECORD p_pExceptRecord );
		void	WriteRegister( PCONTEXT p_pContext );
		void	WriteStack( PCONTEXT p_pContext );
		void	WriteCallStack( EXCEPTION_POINTERS* p_pException );
		void	WriteEnd();

	public:
		MoCrashLog( const wchar* p_strFileName, EXCEPTION_POINTERS* p_pException );
	};

} // end of namespace MoCommon
