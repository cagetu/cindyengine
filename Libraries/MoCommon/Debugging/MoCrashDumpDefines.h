//================================================================
// File					: MoCrashDumpDefines.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 18.
//================================================================
#pragma once

#include <windows.h>
#include <dbghelp.h>
//#include <imagehlp.h>

#include <tchar.h>

namespace MoCommon
{
	/*//////////////////////////////////////////////////////////////////////
									Typedefs
	//////////////////////////////////////////////////////////////////////*/
	// The typedefs for the PSAPI.DLL functions used by this module.
	typedef BOOL (WINAPI *ENUMPROCESSMODULES) ( HANDLE    hProcess   ,
												HMODULE * lphModule  ,
												DWORD     cb         ,
												LPDWORD   lpcbNeeded  ) ;

	typedef DWORD (WINAPI *GETMODULEBASENAMEW) ( HANDLE  hProcess   ,
												 HMODULE hModule    ,
												 LPWSTR  lpBaseName ,
												 DWORD   nSize       ) ;

	typedef DWORD (WINAPI *GETMODULEFILENAMEEXW) ( HANDLE  hProcess   ,
												   HMODULE hModule    ,
												   LPWSTR  lpFilename ,
												   DWORD   nSize       ) ;

	typedef BOOL (WINAPI *MINIDUMPWRITEDUMP)( HANDLE hProcess,
											  DWORD dwProcessID,
											  HANDLE hFile,
											  MINIDUMP_TYPE eDumpType,
											  CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
											  CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
											  CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam );

	#ifndef _countof
	#define _countof(array) (sizeof(array)/sizeof(array[0]))
	#endif

	const int NumCodeBytes = 16;	// Number of code bytes to record.
	const int MaxStackDump = 3072;	// Maximum number of DWORDS in stack dumps.
	const int StackColumns = 4;		// Number of columns in stack dump.

	#define	ONEK			1024
	#define	SIXTYFOURK		(64*ONEK)
	#define	ONEM			(ONEK*ONEK)
	#define	ONEG			(ONEK*ONEK*ONEK)

	#define HPRINTF_BUFFER_SIZE (8*1024)				// must be at least 2048
	static TCHAR hprintf_buffer[HPRINTF_BUFFER_SIZE];	// wvsprintf never prints more than one K.
	static int  hprintf_index = 0;

	// The static source file and line number structure
	static IMAGEHLP_LINE64 stLine;

	#define GSTSO_PARAMS    0x01
	#define GSTSO_MODULE    0x02
	#define GSTSO_SYMBOL    0x04
	#define GSTSO_SRCLINE   0x08

	#ifdef _WIN64
	#define k_PARAMFMTSTRING   _T ( " (0x%016X 0x%016X 0x%016X 0x%016X)" )
	#else
	#define k_PARAMFMTSTRING   _T ( " (0x%08X 0x%08X 0x%08X 0x%08X)" )
	#endif

	#define k_NAMEDISPFMT       _T ( " %S()+%04d byte(s)" )
	#define k_NAMEFMT           _T ( " %S " )
	#define k_FILELINEDISPFMT   _T ( " %S, line %04d+%04d byte(s)" )
	#define k_FILELINEFMT       _T ( " %S, line %04d" )

	// Define the machine type.
	#ifdef _X86_
	#define CH_MACHINE IMAGE_FILE_MACHINE_I386
	#elif _AMD64_
	#define CH_MACHINE IMAGE_FILE_MACHINE_AMD64
	#elif _IA64_
	#define CH_MACHINE IMAGE_FILE_MACHINE_IA64
	#else
	#pragma FORCE COMPILE ABORT!
	#endif

	// Helper define to let code compile on pre W2K systems.
	#if _WIN32 >= 0x500
	#define GET_THREAD_ACP() CP_THREAD_ACP
	#else
	#define GET_THREAD_ACP() GetACP ( )
	#endif

	//------------------------------------------------------
	// Global Functions
	//------------------------------------------------------
	namespace MoCrashDumpUtil
	{
		///////////////////////////////////////////////////////////////////////////////
		// hprintf
		static void hprintf(HANDLE LogFile, LPCTSTR Format, ...)
		{
			if (hprintf_index > (HPRINTF_BUFFER_SIZE-1024))
			{
				DWORD NumBytes;
				WriteFile(LogFile, hprintf_buffer, lstrlen(hprintf_buffer), &NumBytes, 0);
				hprintf_index = 0;
			}

			va_list arglist;
			va_start( arglist, Format);
			hprintf_index += wvsprintf(&hprintf_buffer[hprintf_index], Format, arglist);
			va_end( arglist);
		}

		///////////////////////////////////////////////////////////////////////////////
		// hflush
		static void hflush(HANDLE LogFile)
		{
			if (hprintf_index > 0)
			{
				DWORD NumBytes;
				WriteFile(LogFile, hprintf_buffer, lstrlen(hprintf_buffer), &NumBytes, 0);
				hprintf_index = 0;
			}
		}

		///////////////////////////////////////////////////////////////////////////////
		// lstrrchr (avoid the C Runtime )
		static TCHAR * lstrrchr(LPCTSTR string, int ch)
		{
			TCHAR *start = (TCHAR *)string;

			while (*string++)                       /* find end of string */
				;
													/* search towards front */
			while (--string != start && *string != (TCHAR) ch)
				;

			if (*string == (TCHAR) ch)                /* char found ? */
				return (TCHAR *)string;

			return NULL;
		}

		///////////////////////////////////////////////////////////////////////////////
		// GetFilePart
		static TCHAR * GetFilePart(LPCTSTR source)
		{
			TCHAR *result = lstrrchr(source, _T('\\'));
			if (result)
				result++;
			else
				result = (TCHAR *)source;
			return result;
		}

		///////////////////////////////////////////////////////////////////////////////
		// FormatTime
		//
		// Format the specified FILETIME to output in a human readable format,
		// without using the C run time.
		static void FormatTime(LPTSTR output, FILETIME TimeToPrint)
		{
			output[0] = _T('\0');
			WORD Date, Time;
			if (FileTimeToLocalFileTime(&TimeToPrint, &TimeToPrint) &&
						FileTimeToDosDateTime(&TimeToPrint, &Date, &Time))
			{
				wsprintf(output, _T("%d/%d/%d %02d:%02d:%02d"),
							(Date / 32) & 15, Date & 31, (Date / 512) + 1980,
							(Time >> 11), (Time >> 5) & 0x3F, (Time & 0x1F) * 2);
			}
		}
	} // namespace MoCrashDumpUtil
}
