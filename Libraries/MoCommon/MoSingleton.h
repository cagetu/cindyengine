// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOSINGLETON_H__
#define __MOSINGLETON_H__


namespace MoCommon
{
	//================================================================
	/** Singleton Template 
		   @author Jaehee
		   @remarks 
				하나의 인스턴스를 보장하기 위한 템플릿 클래스.  
	*/
	//================================================================
	template <typename T> class MoSingleton
	{
	public:
		MoSingleton()
		{ 
			assert( !ms_pSingleton );
			ms_pSingleton = static_cast<T*>(this);
		}

		~MoSingleton()
		{
			assert( ms_pSingleton );
			ms_pSingleton = 0;
		}

		static T& GetSingleton()
		{
			assert( ms_pSingleton );
			return (*ms_pSingleton);
		}

		static T* GetSingletonPtr()
		{
			assert( ms_pSingleton );
			return ms_pSingleton;
		}
	protected:
		static T*				ms_pSingleton;	

	};
	template <typename T> T*	MoSingleton<T>::ms_pSingleton				= NULL;
}

#endif // __MOSINGLETON_H__