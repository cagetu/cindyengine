//================================================================
// File					: BcSmallObjAllocator.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 8.
//================================================================
#pragma once

#include "MoMemoryDefines.h"
#include <windows.h>

#undef new
#undef delete

namespace MoCommon
{
	//================================================================
	/**	Fixed Mememory Allocator
		@author		Changhee
		@since		2007.1.8.
		@remarks	고정된 크기의 메모리를 할당한다.
					참고자료: "제네릭 프로그래밍과 디자인 패턴을 적용한 Modern C++ Design" - Chapter4,
							  Gamebryo,
	*/
	//================================================================
	class MOCOM_DLL MoFixedMemAllocator
	{
		//------------------------------------------------------
		// ThreadSafe struct
		//------------------------------------------------------
		struct ThreadSafe
		{
			CRITICAL_SECTION	m_Cs;

			ThreadSafe( CRITICAL_SECTION& cs );
			~ThreadSafe();
		};
	private:
		//------------------------------------------------------
		// Chunk struct
		//------------------------------------------------------
		struct Chunk
		{
			unsigned char*		data;					//!< 할당된 데이터
			unsigned char		firstAvailableBlock;	//!< 아직 할당되지 않은 첫번째 블록의 index
			unsigned char		blocksAvailable;		//!< 아직 할당되지 않은 블록의 전체 개수(255개 한정)

			void		Init( unsigned int p_nBlockSize, unsigned char p_ucBlocks );
			void		Release();

			void*		Alloc( unsigned int p_nBlockSize );
			void		Dealloc( void* p_pAddress, unsigned int p_nBlockSize );

			bool		HasAvailable( unsigned char p_ucNumBlocks ) const;
			bool		HasBlock( unsigned char* p_pAddr, unsigned int p_nChunkSize ) const;
			bool		IsFilled() const;
		};

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CRITICAL_SECTION	m_Cs;				//!< 'Critical_section' by Win32..

		unsigned int		m_nBlockSize;
		unsigned char		m_ucNumBlocks;

		unsigned int		m_nNumChunks;
		unsigned int		m_nMaxNumChunks;

		Chunk*				m_pChunks;
		Chunk*				m_pAllocChunk;
		Chunk*				m_pDeallocChunk;
		Chunk*				m_pEmptyChunk;

		//------------------------------------------------------
		// Methods(Internal)
		//------------------------------------------------------
		void	Push( Chunk& p_rChunk );
		void	Pop();

		void	Reserve( unsigned int p_nNewSize );

		Chunk*	VicinityFind( void* p_pAddr );
		void	DoDeallocate( void* p_pAddr );
	public:
		MoFixedMemAllocator();
		~MoFixedMemAllocator();

		void			Init( unsigned int	p_nBlockSize, unsigned int p_nChunkSize );

		void*			Alloc();
		void			Dealloc( void* p_pAddr );

		unsigned int	GetBlockSize() const;
		unsigned int	GetNumChunks() const;

		unsigned long	GetTotalMemSize() const;
		unsigned long	GetUsedMemSize() const;

		bool			operator < ( unsigned int rhs ) const;	
	};

	//================================================================
	/**	Small Object Allocator
		@author		Changhee
		@since		2007.1.9.
		@remarks	작은 객체 메모리 할당기
					참고자료: "제네릭 프로그래밍과 디자인 패턴을 적용한 Modern C++ Design" - Chapter4,
							  Gamebryo,
	*/
	//================================================================
	class MOCOM_DLL MoSmallObjAllocator
	{
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		MoFixedMemAllocator		m_Pool[MAX_SMALL_OBJECT_SIZE];
		unsigned int			m_nChunkSize;

		//------------------------------------------------------
		// Constructor
		//------------------------------------------------------
		MoSmallObjAllocator( const MoSmallObjAllocator& );
		MoSmallObjAllocator& operator = ( const MoSmallObjAllocator& );

	public:
		MoSmallObjAllocator( unsigned int p_nChunkSize = DEFAULT_CHUNK_SIZE );

		void*					Alloc( unsigned int p_nSize );
		void					Dealloc( void* p_pAddr, unsigned int p_nSize );

		MoFixedMemAllocator*	GetFixedAllocator( unsigned int p_nBlockSize );

		unsigned long			GetTotalMemSize() const;
		unsigned long			GetUsedMemSize() const;
	};

#include "MoSmallObjAllocator.inl"
} // end of namespace 
