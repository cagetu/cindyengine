// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#pragma once

#include <windows.h>
#include "../MoHandle.h"

//#define MO_MEMORY_MONITER

namespace MoCommon
{
	typedef WHandle	AllocType;
	
	//================================================================
	/**	Memory Tracer Class
		@author		Changhee
		@since		2007.1.5.
		@remarks	메모리 모니터 클래스
					참고 자료 : http://www.flipcode.com/articles/article_memoryleaks.shtml
	*/
	//================================================================
	class MOCOM_DLL MoMemTracer
	{
	public:
		static const WHANDLE M_NEW;
		static const WHANDLE M_NEW_ARRAY;
		static const WHANDLE M_DELETE;
		static const WHANDLE M_DELETE_ARRAY;
		static const WHANDLE M_MALLOC;
		static const WHANDLE M_FREE;

	private:
		//------------------------------------------------------/
		// ALLOC_INFO struct
		//------------------------------------------------------
		struct ALLOC_INFO
		{
			unsigned long	ulAddress;		//!< 메모리 주소
			unsigned long	ulMemSize;		//!< 메모리 사이즈
			MoString		strSource;		//!< 소스 파일 이름
			MoString		strFunc;		//!< 호출된 함수 이름
			unsigned long	ulSrcLine;		//!< 소스 파일 라인
			AllocType		MemCreateType;	//!< 메모리 생성 타입
			unsigned long	ulMemSpace;		//!< 0: heap, 1:pool
		};

		typedef std::map< unsigned long, ALLOC_INFO* >	MEMBLOCK_MAP;
		typedef MEMBLOCK_MAP::iterator					MEMBLOCK_ITER;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		static MEMBLOCK_MAP		ms_mapAllocMemBlock;				//!< 할당된 메모리 블럭


	public:
		//------------------------------------------------------
		// Constructor
		//------------------------------------------------------
		MoMemTracer( const MoMemTracer& );
		MoMemTracer& operator = ( const MoMemTracer& );


		static void		AddTrack( unsigned long ulAddr, unsigned long ulMemSize,
								  const char* pstrSource, unsigned long ulSrcLine,
								  const char* pstrFunc, AllocType rType,
								  unsigned long ulMemSpace );
		static void		RemoveTrack( unsigned long ulAddress, AllocType rType );
		static void		RemoveAllTrack();

		static void		OutputMemories( const wchar* pstrFileName );
	};

}	// end of namespace