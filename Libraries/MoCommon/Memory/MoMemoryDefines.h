//================================================================
// File					: MoMemoryDefines.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 7.
//================================================================
#pragma once

// select using memory tracer..
#define MO_MEMORY_MONITER

#if defined(_DEBUG) && defined(MO_MEMORY_MONITER)
#define MO_MEMORY_DEBUG
#endif

// additional defines..
#define MO_MEM_FILE				0
#define MO_MEM_LINE				0
#define MO_MEM_FUNCTION			0
#define MO_MEM_DEALLOC_HEADER	(unsigned int)-1

// select using allocator
#define MO_USE_MEMORY_ALLOCATOR

// Allocator Defines
#define MO_MEM_ALIGNMENT		4
#define DEFAULT_CHUNK_SIZE		25600
#define MAX_SMALL_OBJECT_SIZE	256

#define MO_MEM_HEAP				0
#define MO_MEM_POOL				1
