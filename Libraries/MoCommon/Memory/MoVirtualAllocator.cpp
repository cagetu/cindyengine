//================================================================
// File					: MoVirtualAllocator.cpp
// Related Header File	: MoVirtualAllocator.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 15.
//================================================================
#include "../MoCommon.h"
#include "MoVirtualAllocator.h"

namespace MoCommon
{
	//------------------------------------------------------
	// Virtual Allocator Class
	//------------------------------------------------------
	MoVirtualAllocator::MoVirtualAllocator()
	{
	}

	MoVirtualAllocator::~MoVirtualAllocator()
	{
	}

	//================================================================
	/** 메모리 잡기
	*/
	//================================================================
	void* 
	MoVirtualAllocator::Alloc( unsigned int p_nSize )
	{
		return 0;
	}

	//================================================================
	/** 메모리 해제
	*/
	//================================================================
	void 
	MoVirtualAllocator::Dealloc( void* p_pAddr )
	{
	}
}