//================================================================
// File					: BcMemoryMacros.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 7.
//================================================================
#pragma once

// clear defines..
#undef new
#undef delete

//#include "BcMemoryManager.h"
#include "MoMemoryAllocObject.h"
#include "MoMemoryTracer.h"

#ifdef MO_MEMORY_DEBUG

////================================================================
///** new/delete
//	@remarks	전역으로 사용하는 new/delete를 재정의 한다.
//				전역으로 사용되는 것은 그냥 모니터링만 하고, Heap 메모리를 그대로 사용한다.
//*/
////================================================================
//inline void* 
//operator new( unsigned int size, const char* file, int line, const char* function )
//{
//	void* ptr = (void*)malloc( size );
//	MoCommon::MoMemTracer::AddTrack( (intptr_t)ptr, size, file, line, function, MoCommon::MoMemTracer::M_NEW, BC_MEM_HEAP );
//	return ptr;
//}
//
//inline void 
//operator delete(void* address)
//{
//	MoCommon::MoMemTracer::RemoveTrack( (intptr_t)address, MoCommon::MoMemTracer::M_DELETE );
//	free( address );
//}
//
//inline void* 
//operator new[]( unsigned int size, const char* file, int line, const char* function )
//{
//	void* ptr = (void*)malloc( size );
//	MoCommon::MoMemTracer::AddTrack( (intptr_t)ptr, size, file, line, function, MoCommon::MoMemTracer::M_NEW_ARRAY, BC_MEM_HEAP );
//	return ptr;
//}
//
//inline void 
//operator delete[](void* address)
//{
//	MoCommon::MoMemTracer::RemoveTrack( (intptr_t)address, MoCommon::MoMemTracer::M_DELETE_ARRAY );
//	free( address );
//}
#endif

//
////================================================================
///** BcNew
//	@remarks	디버깅 용도를 위해 new를 다시 설정한다.
//*/
////================================================================
//#ifdef MO_MEMORY_DEBUG
//	#define BcNew		new(__FILE__, __LINE__, __FUNCTION__)
//#else
//	#define BcNew		new
//#endif
//
//#define new BcNew

//================================================================
/** BcAlloc/BcFree
	@remarks	사용 빈도가 형 타입을 이용할 경우, 메모리 풀을 사용하고자 할 때,
				이 매크로를 이용해서 사용한다.
*/
//================================================================
//#ifdef MO_MEMORY_DEBUG
//#define MoAlloc(T, count)		((T*)g_MemMgr.Alloc( (sizeof(T)*count), MO_MEM_ALIGNMENT, \
//													 MoCommon::MoMemTracer::M_MALLOC, false, \
//													 __FILE__, __LINE__, __FUNCTION__ ) )
//#else
//#define BcAlloc(T, count)		((T*)g_MemMgr.Alloc( sizeof(T)*count, MO_MEM_ALIGNMENT, \
//													 MoCommon::MoMemTracer::M_MALLOC, false, \
//													 BC_MEM_FILE, BC_MEM_LINE, BC_MEM_FUNCTION ) )
//#endif
//
//#define MoFree(p)				g_MemMgr.Dealloc(p, MoCommon::MoMemTracer::M_FREE)
