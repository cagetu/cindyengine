// Created By Jinsik

// Modified by Changhee
// http://www.hi-win.com

// External Source

#ifndef _MEMORY_POOL_H_
#define _MEMORY_POOL_H_

#include <stdlib.h>
#include <assert.h>

#include "MoMemoryDefines.h"

namespace MoCommon
{
	#define MAX_FREE_PAGES 4

	//-------------------------------------------------------------------------
	/** @class		MemoryPool
		@author		cagetu

		@brief		http://www.jenkinssoftware.com/의 오픈라이브러리에 있는
					메모리풀.
	*/
	//-------------------------------------------------------------------------
	template<class T>
	class MOCOM_EXPORT MoMemoryPool
	{
	public:
		struct PAGE;
		struct MEMBLOCK
		{
			T memory;
			PAGE* pParentPage;
		};

		struct PAGE
		{
			MEMBLOCK** ppAvailableBlock;
			MEMBLOCK* pBlock;
			PAGE* prev;
			PAGE* next;
			int nAvailableBlockCounts;
		};

	public:
		MoMemoryPool();
		~MoMemoryPool();

		T*		Allocate();
		void	Release(T* m);
		void	Clear();

		void	SetPageSize(int size);
		int		GetUsedPageCounts();
		
	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		PAGE*	m_pAvailablePage;
		PAGE*	m_pUnavailablePage;
		int		m_nAvailablePageCounts;
		int		m_nUnavailablePageCounts;
		int		m_nMemoryPoolPageSize;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		int		BlocksPerPage() const;
		void	InitPage(PAGE* page);
	};

	//#include "MoMemoryPool.inl"
	//------------------------------------------------------
	template<class T>
	MoMemoryPool<T>::MoMemoryPool()
	{
		m_pAvailablePage = NULL;
		m_pUnavailablePage = NULL;
		m_nAvailablePageCounts = 0;
		m_nUnavailablePageCounts = 0;
		m_nMemoryPoolPageSize = 16384;
	}

	template<class T>
	MoMemoryPool<T>::~MoMemoryPool()
	{
		Clear();
	}

	//------------------------------------------------------
	template<class T>
	T* MoMemoryPool<T>::Allocate()
	{
		if (m_nAvailablePageCounts > 0)
		{
			PAGE* pCurPage = m_pAvailablePage;
			T* pRetBlock = (T*)pCurPage->ppAvailableBlock[ --pCurPage->nAvailableBlockCounts ];

			if (pCurPage->nAvailableBlockCounts == 0)
			{	
				m_nAvailablePageCounts--;
				m_pAvailablePage = pCurPage->next;

				assert(m_nAvailablePageCounts == 0 || m_pAvailablePage->nAvailableBlockCounts > 0);

				pCurPage->prev->next = pCurPage->next;
				pCurPage->next->prev = pCurPage->prev;

				if (m_nUnavailablePageCounts == 0)
				{	
					m_pUnavailablePage = pCurPage;
					pCurPage->prev = pCurPage;
					pCurPage->next = pCurPage;
				}
				else
				{
					pCurPage->next = m_pUnavailablePage;
					pCurPage->prev = m_pUnavailablePage->prev;
					m_pUnavailablePage->prev->next = pCurPage;
					m_pUnavailablePage->prev = pCurPage;
				}
				m_nUnavailablePageCounts++;
			}

			assert(m_nAvailablePageCounts == 0 || pCurPage->nAvailableBlockCounts > 0);

			return pRetBlock;
		}

		if (!GetUsedPageCounts())
		{	SetPageSize(sizeof(MEMBLOCK)*4);
		}

		m_pAvailablePage = (PAGE*)malloc(sizeof(PAGE));
		InitPage(m_pAvailablePage);

		m_nAvailablePageCounts = 1;

		assert(m_nAvailablePageCounts == 0 || m_pAvailablePage->nAvailableBlockCounts > 0);

		return (T*)m_pAvailablePage->ppAvailableBlock[ --m_pAvailablePage->nAvailableBlockCounts ];
	}

	//------------------------------------------------------
	template<class T>
	void MoMemoryPool<T>::Release(T* m)
	{
		MEMBLOCK* pBlock = (MEMBLOCK*)m;
		PAGE* pCurPage = pBlock->pParentPage;

		if (pCurPage->nAvailableBlockCounts == 0)
		{
			pCurPage->ppAvailableBlock[ pCurPage->nAvailableBlockCounts++ ] = pBlock;
			m_nUnavailablePageCounts--;

			if (m_nUnavailablePageCounts > 0 || pCurPage == m_pUnavailablePage)
			{
				m_pUnavailablePage = m_pUnavailablePage->next;
			}
			
			if (m_nAvailablePageCounts == 0)
			{
				m_pAvailablePage = pCurPage;
				pCurPage->prev = pCurPage;
				pCurPage->next = pCurPage;
			}
			else
			{
				pCurPage->next = m_pAvailablePage;
				pCurPage->prev = m_pAvailablePage->prev;
				m_pAvailablePage->prev->next = pCurPage;
				m_pAvailablePage->prev = pCurPage;
			}
			m_nAvailablePageCounts++;
		}
		else
		{
			pCurPage->ppAvailableBlock[ pCurPage->nAvailableBlockCounts++ ] = pBlock;

			if (pCurPage->nAvailableBlockCounts == BlocksPerPage() &&
				m_nAvailablePageCounts >= MAX_FREE_PAGES)
			{
				if (pCurPage == m_pAvailablePage)
				{	
					m_pAvailablePage = m_pAvailablePage->next;
					assert(m_pAvailablePage->nAvailableBlockCounts > 0);
				}

				pCurPage->prev->next = pCurPage->next;
				pCurPage->next->prev = pCurPage->prev;

				m_nAvailablePageCounts--;

				free(pCurPage->ppAvailableBlock);
				free(pCurPage->pBlock);
				free(pCurPage);
			}
		}
	}

	//------------------------------------------------------
	template<class T>
	void MoMemoryPool<T>::Clear()
	{
		PAGE* pCurPage = NULL;
		PAGE* pFreedPage = NULL;

		if (m_nAvailablePageCounts > 0)
		{
			pCurPage = m_pAvailablePage;
			while (1)
			{	
				free(pCurPage->ppAvailableBlock);
				free(pCurPage->pBlock);

				pFreedPage = pCurPage;
				pCurPage = pCurPage->next;

				if (pCurPage == m_pAvailablePage)
				{	
					free(pFreedPage);
					break;
				}
				free(pFreedPage);
			}
		}

		if (m_nUnavailablePageCounts > 0)
		{	
			pCurPage = m_pUnavailablePage;
			while (1)
			{	
				free(pCurPage->ppAvailableBlock);
				free(pCurPage->pBlock);

				pFreedPage = pCurPage;
				pCurPage = pCurPage->next;

				if (pCurPage == m_pUnavailablePage)
				{	
					free(pFreedPage);
					break;
				}
				free(pFreedPage);
			}
		}
	}

	//------------------------------------------------------
	template<class T>
	void MoMemoryPool<T>::SetPageSize(int size)
	{
		m_nMemoryPoolPageSize = size;
	}

	//------------------------------------------------------
	template<class T>
	int MoMemoryPool<T>::GetUsedPageCounts()
	{	
		return m_nAvailablePageCounts + m_nUnavailablePageCounts;
	}

	//------------------------------------------------------
	template<class T>
	int MoMemoryPool<T>::BlocksPerPage() const
	{
		return  m_nMemoryPoolPageSize / sizeof(MEMBLOCK);
	}

	//------------------------------------------------------
	template<class T>
	void MoMemoryPool<T>::InitPage(PAGE* page)
	{
		const int bpp = BlocksPerPage();
		page->pBlock = (MEMBLOCK*)malloc(m_nMemoryPoolPageSize);
		page->ppAvailableBlock = (MEMBLOCK**)malloc(sizeof(MEMBLOCK*) * bpp);

		MEMBLOCK* pCurBlock = page->pBlock;
		MEMBLOCK** ppCurStack = page->ppAvailableBlock;
		int i = 0;
		while (i <bpp)
		{
			pCurBlock->pParentPage = page;
			ppCurStack[i] = pCurBlock++;
			i++;
		}

		page->nAvailableBlockCounts = bpp;
		page->prev = m_pAvailablePage;
		page->next = m_pAvailablePage;
	}

}

#endif