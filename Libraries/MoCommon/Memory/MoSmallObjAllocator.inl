//================================================================
// File					: BcSmallObjAllocator.inl
// Related Header File	: BcSmallObjAllocator.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 8.
//================================================================

//------------------------------------------------------
// FixedMemAllocator Class
//------------------------------------------------------

//================================================================
/** GetBlockSize
	@remarks	고정 메모리의 사이즈를 얻는다
	@return		메모리 사이즈
*/
//================================================================
inline unsigned int 
MoFixedMemAllocator::GetBlockSize() const
{
	return m_nBlockSize;
}

//================================================================
/** Get Number Of Chunks
	@remarks	크기 비교 연산자 재정의
	@param		rhs : 비교 대상
	@return		true / false : 크기 비교 여부
*/
//================================================================
inline unsigned int 
MoFixedMemAllocator::GetNumChunks() const
{
	return m_nNumChunks;
}

//================================================================
/** Get Number Of Chunks
	@remarks	크기 비교 연산자 재정의
	@param		rhs : 비교 대상
	@return		true / false : 크기 비교 여부
*/
//================================================================
inline unsigned long 
MoFixedMemAllocator::GetTotalMemSize() const
{
	return GetBlockSize() * GetNumChunks() * m_ucNumBlocks;
}

//================================================================
/** operator < 
	@remarks	크기 비교 연산자 재정의
	@param		rhs : 비교 대상
	@return		true / false : 크기 비교 여부
*/
//================================================================
inline bool 
MoFixedMemAllocator::operator < ( unsigned int rhs ) const
{
	return GetBlockSize() < rhs;
}
