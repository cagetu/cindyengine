//================================================================
// File					: MoMemoryAllocObject.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 10.
//================================================================
#pragma once

#include "MoMemoryDefines.h"

namespace MoCommon
{
//================================================================
/** MoNew
	@remarks	디버깅 용도를 위해 new를 다시 설정한다.
*/
//================================================================
#ifdef MO_MEMORY_DEBUG
	#define MoNew		new(__FILE__, __LINE__, __FUNCTION__)
#else
	#define MoNew		new
#endif

	//================================================================
	/**	Memory Allocate Object
		@author		Changhee
		@since		2007.1.10.
		@remarks	객체의 생성/삭제 여부를 메모리풀에 맡기기 위한 객체
	*/
	//================================================================
	class MOCOM_DLL MoMemAllocObject
	{
#ifdef MO_MEMORY_DEBUG
	private:
		// The memory debugger uses the file, line, function 
        // routines. Therefore new and new[] should be 
        // unavailable to the outer application.
		static void* operator new( unsigned int p_nSize );
		static void* operator new[]( unsigned int p_nSize );
	public:
		static void* operator new( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction );
		static void* operator new[]( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction );

		// Required for exception handling in the compiler. These 
        // should not be used in practice.
		static void operator delete( void* p_pAddr, const char* p_strFile, int p_nLine, const char* p_strFunction )		{}
		static void operator delete[]( void* p_pAddr, const char* p_strFile, int p_nLine, const char* p_strFunction )	{}
#else
	public:
		static void* operator new( unsigned int p_nSize );
		static void* operator new[]( unsigned int p_nSize );
#endif
	public:
		static void operator delete( void* p_pAddr, unsigned int p_nSize );
		static void operator delete[]( void* p_pAddr, unsigned int p_nSize );
	};
};