//================================================================
// File					: BcMemoryManager.inl
// Related Header File	: BcMemoryManager.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 10.
//================================================================

//================================================================
/** GetInstance
	@remarks	싱글턴 전역 단일 객체를 얻어온다.
	@param		none
	@return		none
*/
//================================================================
inline MoMemoryManager& 
MoMemoryManager::GetInstance()
{
	return (*ms_pMemManager);
}

//================================================================
/** IsInitialized
	@remarks	전역 객체가 인스턴스 되어 있는지..
	@param		none
	@return		true/false
*/
//================================================================
inline bool 
MoMemoryManager::IsInitialized()
{
	return ( 0 != ms_pMemManager );
}

#ifdef MO_USE_MEMORY_ALLOCATOR
//================================================================
/** Get Memory Allocator
	@remarks	작은 객체 메모리 할당기를 얻어온다.
	@param		none
	@return		MoSmallObjAllocator&
*/
//================================================================
inline MoSmallObjAllocator& 
MoMemoryManager::GetMemAllocator()
{
	return (*ms_pSmallMemAlloc);
}

#endif