// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoMemoryTracer.h"
#include "../MoStringUtil.h"

#undef new
#undef delete

namespace MoCommon
{
	//------------------------------------------------------
	// MoMemTracer Class
	//------------------------------------------------------
	// define static variable
	MoMemTracer::MEMBLOCK_MAP MoMemTracer::ms_mapAllocMemBlock;
	
	// type 1 : array
	const WHANDLE MoMemTracer::M_NEW			= AllocType( 0, 0 );
	const WHANDLE MoMemTracer::M_DELETE			= AllocType( 0, 1 );
	const WHANDLE MoMemTracer::M_NEW_ARRAY		= AllocType( 1, 0 );
	const WHANDLE MoMemTracer::M_DELETE_ARRAY	= AllocType( 1, 1 );
	const WHANDLE MoMemTracer::M_MALLOC			= AllocType( 2, 0 );
	const WHANDLE MoMemTracer::M_FREE			= AllocType( 2, 1 );

	// static methods
	//================================================================
	/** AddTrack
		@remarks	메모리 사용정보를 추가한다.
		@param		p_address : 메모리 주소
		@param		p_size : 메모리 사이즈
		@param		p_filename : 소스 파일 이름
		@param		p_line : 소스 파일 라인수
		@param		p_function: 호출된 함수 이름
		@param		r_Type : 메모리 할당 타입
		@param		p_pcSpace : 메모리가 할당된 공간
		@return		none
	*/
	//================================================================		
	void MoMemTracer::AddTrack( unsigned long ulAddr, unsigned long ulMemSize,
								const char* pstrSource, unsigned long ulSrcLine,
								const char* pstrFunc, AllocType rType,
								unsigned long ulMemSpace )
	{
		ALLOC_INFO* info = new ALLOC_INFO;

		wchar buffer[512];

		info->ulAddress = ulAddr;
		info->ulMemSize = ulMemSize;

		wmemset( buffer, 0x00, 512 );
		swprintf_s( buffer, 512, L"%S", pstrSource );
		info->strSource = MoString(buffer);

		wmemset( buffer, 0x00, 512 );
		swprintf_s( buffer, 512, L"%S", pstrFunc );
		info->strFunc = MoString(buffer);

		info->ulSrcLine	= ulSrcLine;
		info->MemCreateType = rType;
		info->ulMemSpace = ulMemSpace;

		ms_mapAllocMemBlock.insert( make_pair( ulAddr, info ) );
	}

	//================================================================
	/** RemoveTrack
		@remarks	메모리 사용정보를 제거한다.
		@param		p_address : 메모리 주소
		@param		r_Type : 메모리 해제 타입
		@return		none
	*/
	//================================================================		
	void MoMemTracer::RemoveTrack( unsigned long ulAddress, AllocType rType )
	{
		MEMBLOCK_ITER iter = ms_mapAllocMemBlock.find( ulAddress );
		if( iter != ms_mapAllocMemBlock.end() )
		{
			delete (iter->second);
			ms_mapAllocMemBlock.erase( ulAddress );
		}
	}

	//================================================================
	/** RemoveAllTrack
		@remarks	등록된 모든 메모리를 해제한다.
		@param		none
		@return		none
	*/
	//================================================================
	void MoMemTracer::RemoveAllTrack()
	{
		MEMBLOCK_ITER iter;
		for( iter = ms_mapAllocMemBlock.begin(); iter != ms_mapAllocMemBlock.end(); ++iter )
			delete (iter->second);

		ms_mapAllocMemBlock.clear();
	}

	//================================================================
	/** OutputMemory
		@remarks	등록된 모든 메모리 상태를 출력한다.
		@param		none
		@return		none
	*/
	//================================================================
	void MoMemTracer::OutputMemories( const wchar* p_strFileName )
	{
//		FILE* fp = _wfopen_s( p_strFileName, L"wt" );
		FILE* fp = NULL;
		_wfopen_s( &fp, p_strFileName, L"wt" );
		if (fp)
		{
			SYSTEMTIME systime;
			GetLocalTime( & systime );

			wchar buffer[1024];
			wsprintf( buffer, L"*******************************************************************************\n" );
			fwprintf( fp, buffer );
			OutputDebugString( buffer );

			wsprintf( buffer, L"[%d-%d-%d %d:%d:%d] leaked memory infomation \n", systime.wYear, systime.wMonth, systime.wDay,
																			systime.wHour, systime.wMinute, systime.wSecond );
			fwprintf( fp, buffer );
			OutputDebugString( buffer );

			wsprintf( buffer, L"*******************************************************************************\n" );
			fwprintf( fp, buffer );
			OutputDebugString( buffer );

			if( ms_mapAllocMemBlock.empty() )
			{
				OutputDebugString( L"[complete]Empty Leaked Memories\n" );
				fwprintf( fp, buffer );
			}
			else
			{
				MEMBLOCK_ITER iter;
				int i = 0;

				for( iter = ms_mapAllocMemBlock.begin(); iter != ms_mapAllocMemBlock.end(); ++iter, ++i )
				{
					ALLOC_INFO* MemBlock = iter->second;
					
					MoString strMemSpace;
					if( MemBlock->ulMemSpace == 0 )
					{
						strMemSpace = L"heap memory";
					}
					else
					{
						strMemSpace = L"pool memory";
					}

					swprintf_s( buffer, 1024, L"[%d]addr[%d], size[%d], file: {%s [line:%d]}, function: {%s}, assign: {%s}\n",
										i, MemBlock->ulAddress, MemBlock->ulMemSize, MemBlock->strSource.c_str(), MemBlock->ulSrcLine, 
										MemBlock->strFunc.c_str(), strMemSpace.c_str() );

					fwprintf( fp, buffer );
					OutputDebugString( buffer );
				}
			}

			
			wsprintf( buffer, L"*******************************************************************************\n" );
			fwprintf( fp, buffer );
			//OutputDebugString( buffer );

			fflush( fp );
			fclose( fp );
		}
	}

}	// end of namespace