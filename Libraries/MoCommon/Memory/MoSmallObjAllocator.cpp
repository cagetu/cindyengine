//================================================================
// File					: MoSmallObjAllocator.cpp
// Related Header File	: MoSmallObjAllocator.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 8.
//================================================================
#include "../MoCommon.h"
#include "MoSmallObjAllocator.h"

#include <algorithm>

namespace MoCommon
{
	#define FORCE_CHUNK_SIZE 8

	//------------------------------------------------------
	// ThreadSafe Struct
	//------------------------------------------------------
	MoFixedMemAllocator::ThreadSafe::ThreadSafe( CRITICAL_SECTION& cs )
		: m_Cs(cs)
	{
		::EnterCriticalSection( &m_Cs );
	}
	MoFixedMemAllocator::ThreadSafe::~ThreadSafe()
	{
		::LeaveCriticalSection( &m_Cs );
	}

	//------------------------------------------------------
	// Chunk struct
	//------------------------------------------------------

	//================================================================
	/** Init
		@remarks	메모리 Chunk를 초기화 한다.
		@param		p_nBlockSize : 메모리 블럭의 크기
		@param		p_ucBlocks : 메모리 블럭 개수
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Chunk::Init( unsigned int p_nBlockSize, unsigned char p_ucBlocks )
	{
		data = (unsigned char*)_aligned_malloc( sizeof(unsigned char) * p_nBlockSize * p_ucBlocks, MO_MEM_ALIGNMENT );

		firstAvailableBlock = 0;
		blocksAvailable = p_ucBlocks;

		unsigned char* r_p = data;
		for( unsigned char i = 0; i != p_ucBlocks; r_p += p_nBlockSize )
		{
			*r_p = ++i;
		}
	}

	//================================================================
	/** Release
		@remarks	메모리를 해제
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Chunk::Release()
	{
		_aligned_free( data );
	}

	//================================================================
	/** Allocate
		@remarks	할당해 놓은 메모리에서 사이즈 만큼을 반환
		@param		p_nBlockSize : 메모리 블럭의 크기
		@return		메모리 주소
	*/
	//================================================================
	void* 
	MoFixedMemAllocator::Chunk::Alloc( unsigned int p_nBlockSize )
	{
		if( 0 == blocksAvailable )
			return 0;

		unsigned char* r_pResult = data + ( firstAvailableBlock * p_nBlockSize );

		firstAvailableBlock = *r_pResult;
		--blocksAvailable;

		return r_pResult;
	}

	//================================================================
	/** Deallocate
		@remarks	메모리 해제
		@param		p_nBlockSize : 메모리 블럭의 크기
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Chunk::Dealloc( void* p_pAddress, unsigned int p_nBlockSize )
	{
		unsigned char* r_pToRelease = static_cast<unsigned char*>(p_pAddress);

		// 정렬 문제 체크
		assert( (r_pToRelease - data) % p_nBlockSize == 0 );

		*r_pToRelease = firstAvailableBlock;
		firstAvailableBlock = static_cast<unsigned char>( (r_pToRelease - data) / p_nBlockSize );

		// Truncation check
		assert( firstAvailableBlock == (r_pToRelease - data) / p_nBlockSize );

		++blocksAvailable;
	}

	//================================================================
	/** HasAvailable
		@remarks	메모리 블럭의 개수 체크
		@param		사용가능한 블럭의 개수
		@return		true / false : 결과
	*/
	//================================================================
	bool 
	MoFixedMemAllocator::Chunk::HasAvailable( unsigned char p_ucNumBlocks ) const
	{
		return blocksAvailable == p_ucNumBlocks;
	}

	//================================================================
	/** HasBlock
		@remarks	청크의 영역 체크
		@param		p_pAddr : 비교할 메모리 주소 번지
		@param		p_nChunkSize : 메모리 영역 크기
		@return		true / false : 결과
	*/
	//================================================================
	bool 
	MoFixedMemAllocator::Chunk::HasBlock( unsigned char* p_pAddr, unsigned int p_nChunkSize ) const
	{
		return ( data <= p_pAddr ) && ( p_pAddr < data + p_nChunkSize );
	}

	//================================================================
	/** IsFilled
		@remarks	사용가능한 block이 있는지 여부
		@return		true / false 
	*/
	//================================================================
	bool 
	MoFixedMemAllocator::Chunk::IsFilled() const
	{
		return ( 0 == blocksAvailable );
	}

	//------------------------------------------------------
	// FixedMemAllocator Class
	//------------------------------------------------------
	// cons / dest
	MoFixedMemAllocator::MoFixedMemAllocator()
		: m_nBlockSize(0)
		, m_ucNumBlocks(0)
		, m_nNumChunks(0)
		, m_nMaxNumChunks(0)
		, m_pChunks(0)
		, m_pAllocChunk(0)
		, m_pDeallocChunk(0)
		, m_pEmptyChunk(0)
	{
		::InitializeCriticalSection( &m_Cs );
	}

	MoFixedMemAllocator::~MoFixedMemAllocator()
	{
		for( unsigned int i = 0; i < m_nNumChunks; ++i )
		{
			m_pChunks[i].Release();
		}

		::DeleteCriticalSection( &m_Cs );
	}

	//================================================================
	/** Init
		@remarks	초기화
		@param		p_nBlockSize : 메모리 블럭의 크기
		@param		p_nChunkSize : 메모리 청크 크기
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Init( unsigned int p_nBlockSize, unsigned int p_nChunkSize )
	{
		m_nNumChunks	= 0;
		m_nMaxNumChunks = 0;
		m_pAllocChunk	= 0;
		m_pChunks		= NULL;

		m_nBlockSize	= p_nBlockSize;

		unsigned int r_nNumBlocks = p_nChunkSize / p_nBlockSize;

		if( r_nNumBlocks > 0xff )
		{
			r_nNumBlocks = 0xff;
		}
		else if( 0 == r_nNumBlocks )
		{
			r_nNumBlocks = FORCE_CHUNK_SIZE * p_nBlockSize;
		}

		m_ucNumBlocks = static_cast<unsigned char>(r_nNumBlocks);
		
		assert( m_ucNumBlocks == r_nNumBlocks );
	}

	//================================================================
	/** Push
		@remarks	청크 리스트에 추가
		@param		p_rChunk : 새로운 청크
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Push( Chunk& p_rChunk )
	{
		Reserve( m_nNumChunks + 1 );

		m_pChunks[m_nNumChunks] = p_rChunk;
		m_nNumChunks++;
	}

	//================================================================
	/** Pop
		@remarks	청크 제거 : 실제 메모리를 삭제하지는 않는다.
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Pop()
	{
		--m_nNumChunks;
	}

	//================================================================
	/** Reserve
		@remarks	청크 리스트 재할당
		@param		p_nNewSize : 재할당할 리스트 크기
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Reserve( unsigned int p_nNewSize )
	{
		if( p_nNewSize > m_nMaxNumChunks )
		{
			// original data is left...
			m_pChunks = (Chunk*)realloc( m_pChunks, p_nNewSize*sizeof(Chunk) );
			m_nMaxNumChunks = p_nNewSize;
		}
	}

	//================================================================
	/** Alloc
		@remarks	할당
		@param		none
		@return		할당된 메모리
	*/
	//================================================================
	void* 
	MoFixedMemAllocator::Alloc()
	{
		// 멀티 쓰레드 동기화 객체를 이용하면 좋겠음..
		//ThreadSafe lock( m_Cs );
		::EnterCriticalSection( &m_Cs );

		assert( ( NULL == m_pEmptyChunk ) || ( m_pEmptyChunk->HasAvailable( m_ucNumBlocks ) ) );

		if( m_pAllocChunk && m_pAllocChunk->IsFilled() )
			m_pAllocChunk = NULL;

		// 가능하면, EmptyChunk를 재사용
		if( NULL != m_pEmptyChunk )
		{
			m_pAllocChunk = m_pEmptyChunk;
			m_pEmptyChunk = NULL;
		}

		if( NULL == m_pAllocChunk )
		{
			for( unsigned int i = 0; i < m_nNumChunks; ++i )
			{
				if( !m_pChunks[i].IsFilled() )
				{
					m_pAllocChunk = &m_pChunks[i];
					break;
				}
			} // for

			// 등록되어 있는 공간이 없다면, 새로운 메모리청크 추가
			if( NULL == m_pAllocChunk )
			{
				Reserve( m_nNumChunks + 1 );

				Chunk r_newchunk;
				r_newchunk.Init( m_nBlockSize, m_ucNumBlocks );
				Push( r_newchunk );

				m_pAllocChunk = &m_pChunks[m_nNumChunks-1];
				m_pDeallocChunk = &m_pChunks[0];
			} // if
		} // if

		assert( m_pAllocChunk != 0 );
		assert( !m_pAllocChunk->IsFilled() );

		assert( ( NULL == m_pEmptyChunk ) || ( m_pEmptyChunk->HasAvailable( m_ucNumBlocks ) ) );

		::LeaveCriticalSection( &m_Cs );

		return m_pAllocChunk->Alloc( m_nBlockSize );
	}

	//================================================================
	/** Dealloc
		@remarks	해제
		@param		p_pAddr : 해제할 메모리 주소
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::Dealloc( void* p_pAddr )
	{
		// 동기화 객체 사용
		//ThreadSafe lock( m_Cs );
		::EnterCriticalSection( &m_Cs );
		
		assert( m_nNumChunks != 0 );
		assert( &m_pChunks[0] <= m_pDeallocChunk );
		assert( &m_pChunks[m_nNumChunks-1] >= m_pDeallocChunk );

		m_pDeallocChunk = VicinityFind( p_pAddr );
		assert( m_pDeallocChunk );

		DoDeallocate( p_pAddr );

		::LeaveCriticalSection( &m_Cs );
	}

	//================================================================
	/** VicinityFind
		@remarks	-메모리 해제할 대상 주소값에서 가장 가까운 Chunk를 찾아준다.
					-메모리 해제에 마지막으로 사용된 Chunk 객체를 가리키는 m_pDeallocChunk를
					메모리 해제 요청이 발생 할 때마다 먼저 검사하고, 그리고 그것이
					주어진 포인터가 해당되는 Chunk가 아닐 경우, 선형 탐색을 수행하고,
					요청을 만족시킨 다음 m_pDeallocChunk의 값을 새로운 포인터로 갱신.
					-선형 탐색은 속도를 높이기 위해 양방향 탐색을 한다.
		@param		p_pAddr : 대상 주소값
		@return		Chunk* : 찾은 청크
	*/
	//================================================================
	MoFixedMemAllocator::Chunk* 
	MoFixedMemAllocator::VicinityFind( void* p_pAddr )
	{
		assert( m_nNumChunks != 0 );
		assert( m_pDeallocChunk );

		const unsigned int r_chunksize = m_ucNumBlocks * m_nBlockSize;

		Chunk* r_lo = m_pDeallocChunk;
		Chunk* r_hi = m_pDeallocChunk + 1;
		Chunk* r_loBound = &m_pChunks[0];
		Chunk* r_hiBound = &m_pChunks[m_nNumChunks-1] + 1;

		if( r_hi == r_hiBound )
			r_hi = 0;

		while(1)
		{	// 양방향 탐색..
			if( r_lo )
			{
				if( r_lo->HasBlock( (unsigned char*)p_pAddr, r_chunksize ) )
					return r_lo;

				// 최하위까지 순환했으면, 더 이상 검사하지 않는다.
				if( r_lo == r_loBound )
				{
					r_lo = NULL;
					if( NULL == r_hi )
						break;
				}
				else
				{
					--r_lo;
				}
			} // if(lo)

			if( r_hi )
			{
				if( r_hi->HasBlock( (unsigned char*)p_pAddr, r_chunksize ) )
					return r_hi;

				// 최상위까지 순환했으면, 더 이상 검사하지 않는다.
				if( ++r_hi == r_hiBound )
				{
					r_hi = NULL;
					if( NULL == r_lo )
						break;
				}
			} // if(hi)
		} // while(1)

		assert( !"Could not find pointer p in [MoFixedMemAllocator::VicinityFind]" );

		return 0;
	}

	//================================================================
	/** DoDeallcate
		@remarks	해제
		@param		p_pAddr : 해제할 메모리 주소
		@return		none
	*/
	//================================================================
	void 
	MoFixedMemAllocator::DoDeallocate( void* p_pAddr )
	{
		assert( m_pDeallocChunk->data <= p_pAddr );
		assert( m_pDeallocChunk->HasBlock( static_cast<unsigned char*>(p_pAddr), (m_ucNumBlocks * m_nBlockSize) ) );

		assert( ( NULL == m_pEmptyChunk ) || ( m_pEmptyChunk->HasAvailable( m_ucNumBlocks ) ) );

		m_pDeallocChunk->Dealloc( p_pAddr, m_nBlockSize );

		if( m_pDeallocChunk->HasAvailable( m_ucNumBlocks ) )
		{
			assert( m_pEmptyChunk != m_pDeallocChunk );

			// m_pDeallocChunk is empty, but a Chunk is only released if there 
			// are 2 empty chunks.  Since m_pkEmptyChunk may only point to a 
			// previously cleared Chunk, if it points to something else 
			// besides m_pDeallocChunk, then FixedAllocator currently has 2 empty Chunks.
			if( NULL != m_pEmptyChunk )
			{
				// If last Chunk is empty, just change what m_pDeallocChunk
				// points to, and release the last.  Otherwise, swap an empty
				// Chunk with the last, and then release it.
				Chunk* r_lastchunk = &m_pChunks[m_nNumChunks - 1];

				if( r_lastchunk == m_pDeallocChunk )
					m_pDeallocChunk = m_pEmptyChunk;
				else if( r_lastchunk != m_pEmptyChunk )
					std::swap( *m_pEmptyChunk, *r_lastchunk );

				assert( r_lastchunk->HasAvailable( m_ucNumBlocks ) );
				r_lastchunk->Release();
				Pop();
				m_pAllocChunk = m_pDeallocChunk;
			} // if

			m_pEmptyChunk = m_pDeallocChunk;
		} // if

		// prove either m_pkEmptyChunk points nowhere, or points to a truly empty Chunk.
		assert( ( NULL == m_pEmptyChunk ) || ( m_pEmptyChunk->HasAvailable( m_ucNumBlocks ) ) );
	}

	//================================================================
	/** Get Used Size Of Memory
		@remarks	사용된 메모리 사이즈
		@param		none
		@return		사용된 메모리
	*/
	//================================================================
	unsigned long 
	MoFixedMemAllocator::GetUsedMemSize() const
	{
		unsigned long r_sum = 0;
		for( unsigned int i = 0; i < m_nNumChunks; ++i )
		{
			r_sum += (m_pChunks[i].blocksAvailable * m_nBlockSize);
		}

		return (GetTotalMemSize() - r_sum);
	}

	//------------------------------------------------------
	// SmallObjAllocator Class
	//------------------------------------------------------
	MoSmallObjAllocator::MoSmallObjAllocator( unsigned int p_nChunkSize )
		: m_nChunkSize( p_nChunkSize )
	{
		for( unsigned int i = 1; i <= MAX_SMALL_OBJECT_SIZE; ++i )
		{
			m_Pool[i-1].Init( i, p_nChunkSize );
		}
	}

	//================================================================
	/** Alloc
		@remarks	메모리 할당
		@param		p_nSize : 메모리 크기
		@return		할당된 메모리
	*/
	//================================================================
	void* 
	MoSmallObjAllocator::Alloc( unsigned int p_nSize )
	{
		assert( p_nSize != 0 );
		assert( p_nSize <= MAX_SMALL_OBJECT_SIZE );
		assert( m_Pool[p_nSize-1].GetBlockSize() == p_nSize );

		return m_Pool[p_nSize-1].Alloc();
	}

	//================================================================
	/** Dealloc
		@remarks	메모리 해제
		@param		p_pAddr : 메모리 주소
		@param		p_nSize : 메모리 크기
		@return		none
	*/
	//================================================================
	void 
	MoSmallObjAllocator::Dealloc( void* p_pAddr, unsigned int p_nSize )
	{
		assert( p_nSize != 0 );
		assert( p_nSize <= MAX_SMALL_OBJECT_SIZE );
		assert( m_Pool[p_nSize-1].GetBlockSize() == p_nSize );

		return m_Pool[p_nSize-1].Dealloc( p_pAddr );
	}

	//================================================================
	/** GetFixedAllocator
		@remarks	메모리 크기를 관리하는 할당기 찾기
		@param		p_nBlockSize : 메모리 크기
		@return		MoFixedMemAllocator*
	*/
	//================================================================
	MoFixedMemAllocator* 
	MoSmallObjAllocator::GetFixedAllocator( unsigned int p_nBlockSize )
	{
		assert( p_nBlockSize != 0 );
		assert( p_nBlockSize <= MAX_SMALL_OBJECT_SIZE );
		assert( m_Pool[p_nBlockSize-1].GetBlockSize() == p_nBlockSize );

		return &m_Pool[p_nBlockSize-1];
	}

	//================================================================
	/** Get Total Size Of Memory
		@remarks	할당기에 할당된 총메모리
		@param		none
		@return		총 메모리
	*/
	//================================================================
	unsigned long 
	MoSmallObjAllocator::GetTotalMemSize() const
	{
		unsigned long r_sum = 0;
		for( int i = 0; i < MAX_SMALL_OBJECT_SIZE; ++i )
		{
			r_sum += m_Pool[i].GetTotalMemSize();
		}

		return r_sum;
	}

	//================================================================
	/** Get Used  Size Of Memory
		@remarks	메모리 해제
		@param		p_pAddr : 메모리 주소
		@param		p_nSize : 메모리 크기
		@return		none
	*/
	//================================================================
	unsigned long 
	MoSmallObjAllocator::GetUsedMemSize() const
	{
		unsigned long r_sum = 0;
		for( int i = 0; i < MAX_SMALL_OBJECT_SIZE; ++i )
		{
			r_sum += m_Pool[i].GetUsedMemSize();
		}

		return r_sum;
	}
}