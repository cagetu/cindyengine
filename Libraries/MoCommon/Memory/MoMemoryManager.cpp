//================================================================
// File					: BcMemoryManager.cpp
// Related Header File	: BcMemoryManager.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 5.
//================================================================
#include "../MoCommon.h"
#include "MoMemoryManager.h"
#include "MoMemoryTracer.h"

#ifdef MO_USE_MEMORY_ALLOCATOR
#include "MoSmallObjAllocator.h"
#endif

#include "../MoLog.h"

namespace MoCommon
{
	//------------------------------------------------------
	// BcMemoryManager Class
	//------------------------------------------------------
	// define static variable
	MoMemoryManager* MoMemoryManager::ms_pMemManager = NULL;

#ifdef MO_USE_MEMORY_ALLOCATOR
	
	MoSmallObjAllocator* MoMemoryManager::ms_pSmallMemAlloc = NULL;
#endif

	//================================================================
	/** Initialize
		@remarks	메모리 관리자를 초기화 한다.
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoMemoryManager::Initialize()
	{
		ms_pMemManager = new MoMemoryManager();

#ifdef MO_USE_MEMORY_ALLOCATOR
		
		ms_pSmallMemAlloc = new MoSmallObjAllocator();
#endif
	}

	//================================================================
	/** Shutdown
		@remarks	메모리 관리자 해제
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoMemoryManager::Shutdown()
	{
#ifdef MO_USE_MEMORY_ALLOCATOR
		
		free( ms_pSmallMemAlloc );
		ms_pSmallMemAlloc = NULL;
#endif

		free( ms_pMemManager );
		ms_pMemManager = NULL;

		MoMemTracer::RemoveAllTrack();
	}

	//================================================================
	/** Set Size To Address
		@remarks	메모리 주소의 4바이트에 메모리 크기값을 매핑한다.
		@param		p_pAddr : 메모리 주소
		@param		p_nSize : 크기
		@return		none
	*/
	//================================================================
	void 
	MoMemoryManager::SetSizeToAddress( void* p_pAddr, unsigned int p_nSize )
	{
		char* r_pMem = (char*)p_pAddr;
		char* r_pSize = (char*)&p_nSize;

		assert(sizeof(unsigned int) == 4);

		r_pMem[0] = r_pSize[0];
		r_pMem[1] = r_pSize[1];
		r_pMem[2] = r_pSize[2];
		r_pMem[3] = r_pSize[3];
	}

	//================================================================
	/** Get Size To Address
		@remarks	메모리 주소의 4바이트를 메모리 크기값을 매핑한다.
		@param		p_pAddr : 메모리 주소
		@return		메모리 크기
	*/
	//================================================================
	unsigned int 
	MoMemoryManager::GetSizeFromAddress( void* p_pAddr )
	{
		unsigned int r_size;

		char* r_pMem = (char*)p_pAddr;
		char* r_pSize = (char*)&r_size;

		assert(sizeof(unsigned int) == 4);

		r_pSize[0] = r_pMem[0];
		r_pSize[1] = r_pMem[1];
		r_pSize[2] = r_pMem[2];
		r_pSize[3] = r_pMem[3];

		return r_size;
	}

	//================================================================
	/** Alloc
		@remarks	메모리 할당
		@param		p_nSize : 메모리 사이즈
		@param		p_nAlign : 정렬 바이트
		@param		p_nAllocType : 할당 타입
		@param		p_bProvideSize : 메모리 크기를 할당된 메모리에 추가할지 여부
		@param		p_strSourceFile : 메모리 할당된 위치 파일 이름
		@param		p_nSourceLine : 메모리 할당된 위치 라인 수
		@param		p_strFunction : 메모리 할당된 위치 함수 이름
		@return		void*
	*/
	//================================================================
	void* 
	MoMemoryManager::Alloc( unsigned int p_nSize, unsigned int p_nAlign,
	 					     unsigned short p_nAllocType, bool p_bProvideSize,
							 const char* p_strSourceFile, unsigned int p_nSourceLine, const char* p_strFunction )
	{
		void* r_memptr = NULL;
		unsigned char r_memspace = 0;

#ifdef MO_USE_MEMORY_ALLOCATOR
		bool r_bPrepandSize = false;

		// 할당된 메모리에 4byte를 더해서, 할당된 메모리의 크기를 붙여준다..
		if( p_nAlign == MO_MEM_ALIGNMENT &&
			false == p_bProvideSize )
		{
			p_nSize += MO_MEM_ALIGNMENT;
			r_bPrepandSize = true;
		}

		if( p_nSize <= MAX_SMALL_OBJECT_SIZE && 
			p_nAlign == MO_MEM_ALIGNMENT )
		{
			r_memptr = ms_pSmallMemAlloc->Alloc( p_nSize );
			r_memspace = MO_MEM_POOL;
		}
		else
		{
			r_memptr = _aligned_malloc( p_nSize, p_nAlign );
			r_memspace = MO_MEM_HEAP;
		}

		if( r_memptr && r_bPrepandSize )
		{
			assert( MO_MEM_ALIGNMENT >= sizeof(unsigned int) );

			SetSizeToAddress( r_memptr, p_nSize );
			r_memptr = ((char*)r_memptr) + MO_MEM_ALIGNMENT;
		}

#else
		r_memptr = _aligned_malloc( p_nSize, p_nAlign );
		r_memspace = MO_MEM_HEAP;
#endif

#ifdef MO_MEMORY_DEBUG
		MoMemTracer::AddTrack( (unsigned long)(intptr_t)r_memptr, p_nSize, p_strSourceFile, p_nSourceLine, 
									p_strFunction, p_nAllocType, r_memspace );
#endif

		return r_memptr;
	}

	//================================================================
	/** Dealloc
		@remarks	메모리 해제
		@param		p_pAddr : 메모리 주소
		@param		p_nDeallocType : 해제 타입
		@param		p_nSize : 메모리 크기
		@return		none
	*/
	//================================================================
	void 
	MoMemoryManager::Dealloc( void* p_pAddr, unsigned short p_nDeallocType, unsigned int p_nSize )
	{
		if( NULL == p_pAddr )
			return;

#ifdef MO_MEMORY_DEBUG
		MoMemTracer::RemoveTrack( (unsigned long)(intptr_t)p_pAddr, p_nDeallocType );
#endif

#ifdef MO_USE_MEMORY_ALLOCATOR

		// 해제할 메모리를 구한다.
		if( p_nSize == MO_MEM_DEALLOC_HEADER )
		{
			p_pAddr = ((char*)p_pAddr) - MO_MEM_ALIGNMENT;

			assert( MO_MEM_ALIGNMENT >= sizeof(unsigned int) );

			p_nSize = GetSizeFromAddress( p_pAddr );
		}

		if( p_nSize <= MAX_SMALL_OBJECT_SIZE )
		{
			ms_pSmallMemAlloc->Dealloc( p_pAddr, p_nSize );
			return;
		}
#endif

		_aligned_free( p_pAddr );
	}


	//================================================================
	/** OutputLeakedMemory
		@remarks	메모리 사용상태를 출력한다.
		@param		p_strFileName : 로그 파일 이름
		@return		none
	*/
	//================================================================
	void 
	MoMemoryManager::OutputReport( const wchar* p_strFileName )
	{
		MoMemTracer::OutputMemories( p_strFileName );

#ifdef MO_USE_MEMORY_ALLOCATOR
		unsigned long r_total = ms_pSmallMemAlloc->GetTotalMemSize();
		unsigned long r_used = ms_pSmallMemAlloc->GetUsedMemSize();

		wchar buffer[256];
		swprintf_s( buffer, 256, L"\n" );
		OutputDebugString( buffer );
		swprintf_s( buffer, 256, L"\t*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
		OutputDebugString( buffer );
		swprintf_s( buffer, 256, L"\t Memory Pool Report\n" );
		OutputDebugString( buffer );
		swprintf_s( buffer, 256, L"\t*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
		OutputDebugString( buffer );
		swprintf_s( buffer, 256, L"\t Total Memory : %d byte (%d mb)\n", r_total, (r_total/1024) );
		OutputDebugString( buffer );
		swprintf_s( buffer, 256, L"\t Used Memory : %d byte\n", r_used );
		OutputDebugString( buffer );
		swprintf_s( buffer, 256, L"\t*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
		OutputDebugString( buffer );
		swprintf_s( buffer, 256, L"\n" );
		OutputDebugString( buffer );
#endif
	}
}
