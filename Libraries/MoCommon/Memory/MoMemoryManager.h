// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com
#pragma once

#include "MoMemoryDefines.h"

namespace MoCommon
{
#ifdef MO_USE_MEMORY_ALLOCATOR
	class MoSmallObjAllocator;
#endif

	//================================================================
	/**	Memory Manager Class
		@author		Changhee
		@since		2007.1.8.
		@remarks	메모리를 관리한다.
	*/
	//================================================================
	class MOCOM_DLL MoMemoryManager
	{
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		static MoMemoryManager*		ms_pMemManager;		// 싱글턴 객체

#ifdef MO_USE_MEMORY_ALLOCATOR
		static MoSmallObjAllocator*	ms_pSmallMemAlloc;	// 작은 객체 메모리 할당기
#endif
		//------------------------------------------------------
		// Constructor
		//------------------------------------------------------
		MoMemoryManager()							{}	// 외부에서 생성하지 못한다.
		MoMemoryManager( const MoMemoryManager& ) {}	// 복사 생성도 못한다.

		static void				SetSizeToAddress( void* p_pAddr, unsigned int p_nSize );
		static unsigned int		GetSizeFromAddress( void* p_pAddr );
	public:
		static MoMemoryManager&		GetInstance();

		static void						Initialize();
		static void						Shutdown();

		static bool						IsInitialized();

#ifdef MO_USE_MEMORY_ALLOCATOR
		static MoSmallObjAllocator&	GetMemAllocator();
#endif

		void*			Alloc( unsigned int p_nSize, unsigned int p_nAlign,
							   unsigned short p_nAllocType, bool p_bProvideSize,
							   const char* p_strSourceFile, unsigned int p_nSourceLine, const char* p_strFunction );

		void			Dealloc( void* p_pAddr, unsigned short p_nDeallocType, 
								 unsigned int p_nSize = MO_MEM_DEALLOC_HEADER );

		static void		OutputReport( const wchar* p_strFileName );
	};

#define g_MemMgr		MoMemoryManager::GetInstance()

#include "MoMemoryManager.inl"
} // end of namespace MoCommon

