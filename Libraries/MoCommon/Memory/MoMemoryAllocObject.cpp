//================================================================
// File					: BcMemoryAllocObject.cpp
// Related Header File	: BcMemoryAllocObject.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 5.
//================================================================
#include "../MoCommon.h"
#include "MoMemoryAllocObject.h"
#include "MoMemoryManager.h"
#include "MoMemoryTracer.h"

#undef new
#undef delete

#include <exception> // for std::bad_alloc
#include <new>

namespace MoCommon
{
	//------------------------------------------------------
	//	BcMemAlloc Class
	//------------------------------------------------------

#ifdef MO_MEMORY_DEBUG
	// define operator new/delete
	void* 
	MoMemAllocObject::operator new( unsigned int p_nSize )
	{
		throw std::bad_alloc();
	}

	void* 
	MoMemAllocObject::operator new[]( unsigned int p_nSize )
	{
		throw std::bad_alloc();
	}

	void* 
	MoMemAllocObject::operator new( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction )
	{
		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW, true, 
									p_strFile, (unsigned int)p_nLine, p_strFunction );

		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}

	void* 
	MoMemAllocObject::operator new[]( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction )
	{
		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW_ARRAY, false,
									p_strFile, (unsigned int)p_nLine, p_strFunction );
		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}

#else
	void* 
	MoMemAllocObject::operator new( unsigned int p_nSize )
	{
		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW, true,
									"temp", 0, "temp" );
		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}

	void* 
	MoMemAllocObject::operator new[]( unsigned int p_nSize )
	{
		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW_ARRAY, false,
									"temp", 0, "temp" );
		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}
#endif

	void 
	MoMemAllocObject::operator delete( void* p_pAddr, unsigned int p_nSize )
	{
		if( p_pAddr )
		{
			g_MemMgr.Dealloc( p_pAddr, MoMemTracer::M_DELETE, p_nSize );
		}
	}

	void 
	MoMemAllocObject::operator delete[]( void* p_pAddr, unsigned int p_nSize )
	{
		if( p_pAddr )
		{
			g_MemMgr.Dealloc( p_pAddr, MoMemTracer::M_DELETE_ARRAY );
		}
	}

} // end of namespace 