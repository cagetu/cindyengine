//================================================================
// File					: MoVirtualAllocator.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 15.
//================================================================
#pragma once

#include "MoMemoryDefines.h"

namespace MoCommon
{
	//================================================================
	/**	Virtual Memory Allocator
		@author		Changhee
		@since		2007.1.15.
		@remarks	가상 메모리를 사용한 메모리 할당기
	*/
	//================================================================
	class MoVirtualAllocator
	{
	public:
		MoVirtualAllocator();
		~MoVirtualAllocator();

		void*		Alloc( unsigned int p_nSize );
		void		Dealloc( void* p_pAddr );
	};
}
