// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOTIMER_H__
#define __MOTIMER_H__


namespace MoCommon
{
	//================================================================
	/** Timer
	       @author Jaehee
		   @remarks 
				타이머~~~
	*/
	//================================================================
	class MOCOM_DLL MoTimer
	{
	protected:
		bool			m_bStopped;			
		ulong			m_ulBaseTime;
		ulong			m_ulLastElapsedTime;
		ulong			m_ulStopTime;

	public:
		MoTimer();
		~MoTimer();

		void			ResetTimer();
		void			StartTimer();
		void			StopTimer();
		void			Advance();

		float			GetAppTime();
		float			GetAbsoluteTime();
		float			GetElapsedTime();
		bool			IsStopped() const			{		return m_bStopped;			}
	};
}



#endif // __MOTIMER_H__