// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#pragma once

#include "MoCommon.h"

namespace MoCommon
{
	typedef unsigned long LHANDLE;
	typedef unsigned short WHANDLE;

	//=========================================================================
	/** LHandle Class
		@author		Changhee Lee
		@since		2007. 1. 8.
		@remarks	32비트 핸들 클래스
					타입과 인덱스를 가진다.
	*/
	//=========================================================================	
	class MOCOM_DLL MoHandleL
	{
	public:
		enum {		INVALID_LHANDLE = 0xffffffff		};

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		union
		{
			struct
			{
				unsigned short	type	: 4;		//!< type
				unsigned short	index	: 4;		//!< index
			};
			LHANDLE value;			//!< 핸들 값
		};

	public:
		//------------------------------------------------------
		// Constructor
		//------------------------------------------------------
		MoHandleL();
		MoHandleL( const MoHandleL& p_rLHandle );
		MoHandleL( LHANDLE p_value );
		MoHandleL( unsigned short p_nType, unsigned short p_nIndex );
		~MoHandleL();

		void Invalid();

		//------------------------------------------------------
		// 연산자 재정의
		//------------------------------------------------------
		operator LHANDLE() const;
		MoHandleL& operator = ( const MoHandleL& p_rLHandle );
		bool operator < ( const MoHandleL& p_rLHandle );
	};

	//=========================================================================
	/** WHandle Class
		@author		Changhee Lee
		@since		2007. 1. 8.
		@remarks	16비트 핸들 클래스
					타입과 인덱스를 가진다.
	*/
	//=========================================================================	
	class MOCOM_DLL MoHandleW
	{
	public:
		enum {		INVALID_WHANDLE = 0xffff		};

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		union
		{
			struct
			{
				unsigned char	type	: 2;		//!< type
				unsigned char	index	: 2;		//!< index
			};
			WHANDLE value;			//!< 핸들 값
		};

	public:
		//------------------------------------------------------
		// Constructor
		//------------------------------------------------------
		MoHandleW();
		MoHandleW( const MoHandleW& p_rWHandle );
		MoHandleW( WHANDLE p_value );
		MoHandleW( unsigned char p_nType, unsigned char p_nIndex );
		~MoHandleW();

		void Invalid();

		//------------------------------------------------------
		// 연산자 재정의
		//------------------------------------------------------
		operator WHANDLE() const;
		MoHandleW& operator = ( const MoHandleW& p_rWHandle );
		bool operator < ( const MoHandleW& p_rWHandle );
	};

	typedef MoHandleL	LHandle;
	typedef MoHandleW	WHandle;

#include "MoHandle.inl"
}	// end of namespace 
