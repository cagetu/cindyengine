// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoCommon.h"
#include "MoStringUtil.h"

namespace MoCommon
{
	// Path 분할용
	wchar _Drive[_MAX_DRIVE];
	wchar _Dir[_MAX_DIR];
	wchar _Name[_MAX_FNAME];
	wchar _Ext[_MAX_EXT];

	//------------------------------------------------------------------
	void MoStringUtil::TrimSpace( MoString& str )
	{
		static const MoString s_del( L" \t\n\r" );

		str.erase( str.find_last_not_of( s_del ) + 1 );			// trim right
		str.erase( 0, str.find_first_not_of( s_del ) );			// trim left
	}

	//------------------------------------------------------------------
	void MoStringUtil::Trim( MoString &str, const MoString &strDel )
	{
		str.erase( str.find_last_not_of( strDel ) + 1 );			// trim right
		str.erase( 0, str.find_first_not_of( strDel ) );			// trim left
	}

	//------------------------------------------------------------------
	void MoStringUtil::LowerCase( MoString& str )
	{
		std::transform( str.begin(), str.end(), str.begin(), tolower );
	}

	//------------------------------------------------------------------
	void MoStringUtil::UpperCase( MoString& str )
	{
		std::transform( str.begin(), str.end(), str.begin(), toupper );
	}

	//------------------------------------------------------------------
	int MoStringUtil::WidecharToMultibyte( const wchar* pSrc, char* pOut, unsigned int CodePage )
	{
		if (pSrc)
		{
			//!! Debug요망
			int len = (int)(wcslen(pSrc) + 1) * 2;
			return WideCharToMultiByte(	CodePage, 0, pSrc, -1, pOut, len, NULL, NULL );	
		}

		return -1;
	}

	//------------------------------------------------------------------
	void MoStringUtil::Split( MoStringVector& vecOut, const MoString& str, const MoString& strDels, ushort usMaxSplit )
	{
		ushort usNumSplit = 0;

		size_t ulStart = 0;
		size_t ulCur;

		do
		{
			ulCur = str.find_first_of( strDels, ulStart );
			if( ulCur == ulStart )
			{
				ulStart = ulCur + 1;
			}
			else if( ulCur == MoString::npos || ( usMaxSplit && usNumSplit == usMaxSplit ) )
			{
				vecOut.push_back( str.substr( ulStart ) );
				break;
			}
			else
			{
				vecOut.push_back( str.substr( ulStart, ulCur - ulStart ) );
				ulStart = ulCur + 1;
			}

			ulStart = str.find_first_not_of( strDels, ulStart );
			++usNumSplit;
		}while (ulCur != MoString::npos);
	}

	//------------------------------------------------------------------
	errno_t _SplitPath( const MoString& strPath )
	{
		wmemset( _Drive, 0x00, _MAX_DRIVE );
		wmemset( _Dir, 0x00, _MAX_DIR );
		wmemset( _Name, 0x00, _MAX_FNAME );
		wmemset( _Ext, 0x00, _MAX_EXT );

		errno_t res = _wsplitpath_s( strPath.c_str(), _Drive, _MAX_DRIVE, _Dir, _MAX_DIR, _Name, _MAX_FNAME, _Ext, _MAX_EXT );

		return res;
	}

	//------------------------------------------------------------------
	void MoStringUtil::SplitPath( const MoString& strPath, MoString& strDrive, MoString& strDir, 
									MoString& strFileName, MoString& strExt )
	{
		if ((MO_MAX_PATH_LENGTH < strPath.length()) || strPath.empty())
		{
			return;
		}

		errno_t res = _SplitPath( strPath.c_str() );

		if (0 == res)
		{
			strDrive = _Drive;
			strDir = _Dir;
			strFileName = _Name;
			strExt = _Ext;
		}
	}

	//------------------------------------------------------------------
	void MoStringUtil::SplitPathDrive( const MoString& strPath, MoString& strOut )
	{
		if ((MO_MAX_PATH_LENGTH < strPath.length()) || strPath.empty())
		{
			return;
		}

		errno_t res = _SplitPath( strPath.c_str() );

		if (0 == res)
		{
			strOut = _Drive;
		}
	}

	//------------------------------------------------------------------
	void MoStringUtil::SplitPathDir( const MoString& strPath, MoString& strOut )
	{
		if ((MO_MAX_PATH_LENGTH < strPath.length()) || strPath.empty())
		{
			return;
		}

		errno_t res = _SplitPath( strPath.c_str() );

		if (0 == res)
		{
			strOut = _Dir;
		}
	}

	//------------------------------------------------------------------
	void MoStringUtil::SplitPathFileName( const MoString& strPath, MoString& strOut )
	{
		if ((MO_MAX_PATH_LENGTH < strPath.length()) || strPath.empty())
		{
			return;
		}

		errno_t res = _SplitPath( strPath.c_str() );

		if (0 == res)
		{
			strOut = _Name;
		}
	}

	//------------------------------------------------------------------
	void MoStringUtil::SplitPathExt( const MoString& strPath, MoString& strOut )
	{
		if ((MO_MAX_PATH_LENGTH < strPath.length()) || strPath.empty())
		{
			return;
		}

		errno_t res = _SplitPath( strPath.c_str() );

		if (0 == res)
		{
			strOut = _Ext;
		}
	}

	//------------------------------------------------------------------
	void MoStringUtil::SplitPathFullFileName( const MoString& strPath, MoString& strOut )
	{
		if ((MO_MAX_PATH_LENGTH < strPath.length()) || strPath.empty())
		{
			return;
		}

		errno_t res = _SplitPath( strPath.c_str() );

		if (0 == res)
		{
			strOut = MoString(_Name) + MoString(_Ext);
		}
	}

	//------------------------------------------------------------------
	void MoStringUtil::ToString( wchar* pOut, const wchar* pFormat, ... )
	{
		// pOut은 외부에서 제대로 세팅되어서 들어온다고 가정한다.
		assert( pOut );

		va_list args;
		
		va_start( args, pFormat );
		int len = _vscwprintf( pFormat, args ) + 1;
		len = vswprintf_s( pOut, len, pFormat, args );
		va_end( args );

		pOut[len] = L'\0';
	}

	//------------------------------------------------------------------
	/// \brief 주어진 문자열 내에 존재하는 문자열을 다른 문자열로 치환한다.
	/// \param text 원본 문자열
	/// \param find_token 찾고자 하는 문자열
	/// \param replace_token 치환하고자 하는 문자열
	//------------------------------------------------------------------
	void MoStringUtil::Replace( MoString& Output, const MoString& find_token, const MoString& replace_token )
	{
		size_t i = 0;
		while ((i = Output.find(find_token)) != MoString::npos)
			Output.replace(i, find_token.size(), replace_token);
	}

	//------------------------------------------------------------------
	/// \brief 문장 내부에 연속적인 대상 문자를 하나로 만든다.
	///
	/// 즉 target을 ' '로 준 경우, 두 개 이상의 ' '를 하나의 ' '로 만든다.
	/// 
	/// \param text 원본 문자열
	/// \param target 대상이 되는 글자
	/// \return string 변환한 문자열
	//------------------------------------------------------------------
	MoString MoStringUtil::Squeeze( const MoString& text, char target )
	{
		MoString str(text);
		Replace( str, MoString(2, target), MoString(1, target));
		return str;
	}
}


