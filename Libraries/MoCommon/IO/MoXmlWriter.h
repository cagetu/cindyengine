#ifndef __MO_XML_WRITER_H__
#define __MO_XML_WRITER_H__

#include "../External/minixml.h"

namespace MoCommon
{
	//================================================================
	/** File Stream
			@author cagetu
			@brief 
				XML을 작성해주는 IO 클래스
			@see
				Nebula3::IO::XmlWriter
	*/
	//================================================================
	class MOCOM_DLL MoXmlWriter
	{
	public:
		MoXmlWriter();
		virtual ~MoXmlWriter();

		bool	Open();
		void	Close();

		bool	BeginNode( const MoString& nodeName );
		void	EndNode();

		void	SetString( const MoString& name, const MoString& value );
		void	SetBool( const MoString& name, bool value );
		void	SetInt( const MoString& name, int value );
		void	SetFloat( const MoString& name, float value );

	private:
		CMiniXML			_xml;
		CMiniXML::iterator	_curNode;
		CMiniXML::iterator	_curParentNode;

		bool		_isOpened;
	};
}

#endif // __MO_XML_WRITER_H__