#include "../MoCommon.h"
#include "MoXmlWriter.h"
#include "../MoStringUtil.h"

#include <strsafe.h>
#include <windows.h>
#include <cassert>

namespace MoCommon
{
	//------------------------------------------------------------------
	MoXmlWriter::MoXmlWriter()
		: _isOpened(false)
	{
	}

	//------------------------------------------------------------------
	MoXmlWriter::~MoXmlWriter()
	{
		Close();
	}

	//------------------------------------------------------------------
	bool MoXmlWriter::Open()
	{
		_isOpened = true;
		return true;
	}

	//------------------------------------------------------------------
	void MoXmlWriter::Close()
	{
		_isOpened = false;
	}

	//------------------------------------------------------------------
	/**	현재 노드 아래에 새로운 노드를 시작한다.
	*/
	bool MoXmlWriter::BeginNode( const MoString& nodeName )
	{
		assert(_isOpened);
		
		if (_curNode == 0)
		{
			_curParentNode = _xml.GetRoot();
		}
		else
		{
			_curParentNode = _curNode;			
		}

		_curNode = _curParentNode.InsertChild(nodeName.c_str());
		return true;
	}

	//------------------------------------------------------------------
	/**	현재 노드를 끝낸다. 부모로 현재 노드를 설정한다.
	*/
	void MoXmlWriter::EndNode()
	{
		assert(_isOpened);
		assert(_curNode != 0);

		assert(0);

		//if (parentnode == rootnode)
		//{
		//	_curNode.Erase();
		//}
		//else
		//{
		//	_curNode = _curParentNode;
		//}
	}

	//------------------------------------------------------------------
	void MoXmlWriter::SetString( const MoString& name, const MoString& value )
	{
		assert(_isOpened);
		assert(_curNode != 0);
		assert(!name.empty());
		assert(!value.empty());

		_curNode.SetAttribute( name.c_str(), value.c_str() );
	}

	//------------------------------------------------------------------
	void MoXmlWriter::SetBool( const MoString& name, bool value )
	{
		MoString str = value ? L"true" : L"false";
		this->SetString( name, str );
	}

	//------------------------------------------------------------------
	void MoXmlWriter::SetInt( const MoString& name, int value )
	{
		static wchar buff[256];
		swprintf_s(buff, sizeof(buff), L"%d", value);
		this->SetString( name, buff );
	}

	//------------------------------------------------------------------
	void MoXmlWriter::SetFloat( const MoString& name, float value )
	{
		static wchar buff[256];
		swprintf_s(buff, sizeof(buff), L"%.6f", value);
		this->SetString( name, buff );
	}
}