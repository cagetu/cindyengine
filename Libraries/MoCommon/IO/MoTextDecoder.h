// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOTEXTDECODER_H__
#define __MOTEXTDECODER_H__

namespace MoCommon
{
	//================================================================
	/** Text File Decoder Class
		   @author	Jaehee
		   @remarks 
				텍스트 파일 인코딩 된 거 제대로 읽어오기
	*/
	//================================================================
	class MOCOM_DLL MoTextDecoder
	{
	public:
		enum TEXT_TYPE
		{
			TT_ANSI		= 0,		// ANSI
			TT_UTF8,				// UTF8 No BOM
			TT_UTF8_BOM,			// UTF BOM
			TT_UNICODE_LE,			// UNICODE Little Endian(General)
			TT_UNICODE_BE,			// UNICODE Big Endian

			TT_INVALID,
		};
	
		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		MoTextDecoder();
		~MoTextDecoder();

		TEXT_TYPE		DecodeText( const uchar* pBuffer, ulong nLen );

		wchar*			GetBuffer() const				{		return m_pBuffer;		}
		ulong			GetSize() const					{		return m_ulSize;		}

		TEXT_TYPE		GetEncodeType() const			{		return m_eType;			}

	private:
		wchar*			m_pBuffer;
		ulong			m_ulSize;
		TEXT_TYPE		m_eType;

		TEXT_TYPE		CheckEncodeType( const uchar* pBuffer, ulong nLen );

		void			DecodeANSI( const uchar* pBuffer, ulong nLen );
		void			DecodeUTF8( const uchar* pBuffer, ulong nLen );
		void			DecodeUnicode_LE( const uchar* pBuffer, ulong nLen );

		ulong			CalcSize_UTF8_BOM( const uchar* pBuffer, ulong nLen );
		void			DecodeUTF8_BOM( const uchar* pBuffer, ulong nLen );
		
		// BE는 멀라~~~
	};
}




#endif // __MOTEXTDECODER_H__