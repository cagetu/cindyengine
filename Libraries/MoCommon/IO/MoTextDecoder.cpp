// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "../MoCommon.h"
#include "MoTextDecoder.h"
#include <windows.h>

namespace MoCommon
{
	//------------------------------------------------------------------
	MoTextDecoder::MoTextDecoder()
	: m_pBuffer(NULL)
	, m_ulSize(0)
	, m_eType(TT_INVALID)
	{
	
	}

	MoTextDecoder::~MoTextDecoder()
	{
		SAFEDELS( m_pBuffer );
	}

	//------------------------------------------------------------------
	MoTextDecoder::TEXT_TYPE MoTextDecoder::CheckEncodeType( const uchar* pBuffer, ulong nLen )
	{
		// http://www.devpia.com/MAEUL/Contents/Detail.aspx?BoardID=50&MAEULNo=20&no=568605&ref=561246

		// BOM
		static uchar uni_le[] = { 0xff, 0xfe };
		static uchar uni_be[] = { 0xfe, 0xff };
		static uchar utf8[] = { 0xef, 0xbb, 0xbf };

		// 비교
		if (memcmp( pBuffer, uni_le, sizeof(char)*2 ) == 0)
		{
			return TT_UNICODE_LE;
		}
		else if (memcmp( pBuffer, uni_be, sizeof(char)*2 ) == 0)
		{
			return TT_UNICODE_BE;
		}
		else if (memcmp( pBuffer, utf8, sizeof(char)*3 ) == 0)
		{
			return TT_UTF8_BOM;
		}

		// UTF-8의 경우는 파일에 BOM에 안박히는 경우가 대부분이다.
		// 메모장은 UTF-8파일로 인식하지만 울트라 에디트는 그냥 ANSI파일로 인식해버린단다.
		// 그래서 노가다로 구분할 수 있어야 한다.

		// ANSI와 UTF-8 파일 구분
		ulong looknum	= 0;
		ulong read		= 0;
		ulong ansi		= 0;
				
		while (read < nLen)
		{
			if (pBuffer[read] <= 0x7f && looknum == 0)
			{
				++ansi;
			}
			else if ((pBuffer[read] == 0xff || pBuffer[read] == 0xfe) && looknum == 0)
			{
				return TT_ANSI;	
			}
			else
			{
				if (looknum > 0)
				{
					--looknum;
					if (pBuffer[read] < 0x80 || pBuffer[read] > 0xbf)
					{
						return TT_ANSI;
					}
				}
				else
				{
					if (pBuffer[read] >= 0xc0 && pBuffer[read] <= 0xdf)
					{
						looknum = 1;
					}
					else if (pBuffer[read] >= 0xe0 && pBuffer[read] <= 0xef)
					{
						looknum = 2;
					}
					else if (pBuffer[read] >= 0xf0 && pBuffer[read] <= 0xf7)
					{
						looknum = 3;
					}
					else if (pBuffer[read] >= 0xf8 && pBuffer[read] <= 0xfb)
					{
						looknum = 4;
					}
					else if (pBuffer[read] == 0xfc || pBuffer[read] == 0xfd)
					{
						looknum = 5;
					}
					else
					{
						++ansi;
					}
				}
			}

			++read;
		}

		if (read == ansi)
			return TT_ANSI;

		return TT_UTF8;
	}

	//------------------------------------------------------------------
	void MoTextDecoder::DecodeANSI( const uchar* pBuffer, ulong nLen )
	{
		m_ulSize = MultiByteToWideChar( CP_ACP, 0, (LPCSTR)pBuffer, nLen, NULL, 0 );
		m_pBuffer = new wchar[m_ulSize+1];
		MultiByteToWideChar( CP_ACP, 0, (LPCSTR)pBuffer, nLen, m_pBuffer, m_ulSize );
		m_pBuffer[m_ulSize] = L'\0';
	}

	//------------------------------------------------------------------
	void MoTextDecoder::DecodeUTF8( const uchar* pBuffer, ulong nLen )
	{
		m_ulSize = MultiByteToWideChar( CP_UTF8, 0, (LPCSTR)pBuffer, nLen, NULL, 0 );
		m_pBuffer = new wchar[m_ulSize+1];
		MultiByteToWideChar( CP_UTF8, 0, (LPCSTR)pBuffer, nLen, m_pBuffer, m_ulSize );
		m_pBuffer[m_ulSize] = L'\0';
	}

	//------------------------------------------------------------------
	ulong MoTextDecoder::CalcSize_UTF8_BOM( const uchar* pBuffer, ulong nLen )
	{
		ulong len = 0;

		for (ulong i = 0; i < nLen; ++i)
		{
			if ((pBuffer[i] & 0x80))
			{
				if ((pBuffer[i] & 0xe0) == 0xc0)
				{
					len += 2;
				}
				else if ((pBuffer[i] & 0xf0) == 0xe0)
				{
					len += 3;
				}
				else if ((pBuffer[i] & 0xf8) == 0xf0)
				{
					len += 4;
				}
				else if ((pBuffer[i] & 0xfc) == 0xf8)
				{
					len += 5;
				}
				else if ((pBuffer[i] & 0xfe) == 0xfc)
				{
					len += 6;
				}	
			}
			else
			{
				++len;
			}	
		}

		return len;
	}

	//------------------------------------------------------------------
	void MoTextDecoder::DecodeUTF8_BOM( const uchar* pBuffer, ulong nLen )
	{
		ushort code;
		int startpos = 3;
		ulong i;
		ulong cnt = CalcSize_UTF8_BOM( pBuffer, nLen );

		m_pBuffer = new wchar[cnt+1];
		for (i = startpos, cnt = 0; i < nLen; ++cnt )
		{
			if ((pBuffer[i] & 0x80) == 0x00)
			{
				code = (pBuffer[i] & 0x7f);
				++i;
			}
			else if ((pBuffer[i] & 0xe0) == 0xc0)
			{
				code = ((pBuffer[i] & 0x1f) << 6) | (pBuffer[i+1] & 0x3f);
				i += 2;
			}
			//!! 디버깅 해야 함
			else if ((pBuffer[i] & 0xf0) == 0xe0)
			{
				code = ((pBuffer[i] & 0x1f) << 12) | ((pBuffer[i+1] & 0x3f) << 6) | 
						(pBuffer[i+2] & 0x3f);
				i += 3;
			}
			else if ((pBuffer[i] & 0xf8) == 0xf0)
			{
				code = ((pBuffer[i] & 0x1f) << 18) | ((pBuffer[i+1] & 0x3f) << 12) | 
						((pBuffer[i+2] & 0x3f) << 6 ) | (pBuffer[i+3] & 0x3f);
				i += 4;
			}
			else if ((pBuffer[i] & 0xfc) == 0xf8)
			{
				code = ((pBuffer[i] & 0x1f) << 24) | ((pBuffer[i+1] & 0x3f) << 18) | 
						((pBuffer[i+2] & 0x3f) << 12 ) | ((pBuffer[i+3] & 0x3f) << 6 ) |
						(pBuffer[i+4] & 0x3f);
				i += 5;
			}
			else if ((pBuffer[i] & 0xfe) == 0xfc)
			{
				code = ((pBuffer[i] & 0x1f) << 30) | ((pBuffer[i+1] & 0x3f) << 24) | 
						((pBuffer[i+2] & 0x3f) << 18 ) | ((pBuffer[i+3] & 0x3f) << 12 ) |
						((pBuffer[i+4] & 0x3f) << 6 ) | (pBuffer[i+5] & 0x3f);
				i += 6;
			}
			else
			{
				code = pBuffer[i];
				++i;
			}

			m_pBuffer[cnt] = code;
		}

		m_pBuffer[cnt] = L'\0';

		m_ulSize = cnt;
	}

	//------------------------------------------------------------------
	void MoTextDecoder::DecodeUnicode_LE( const uchar* pBuffer, ulong nLen )
	{
		m_ulSize = (nLen-2) / sizeof(wchar);
		m_pBuffer = new wchar[m_ulSize + 1];
		memcpy( m_pBuffer, &pBuffer[2], nLen-2 );
		m_pBuffer[m_ulSize] = L'\0';
	}

	//------------------------------------------------------------------
	MoTextDecoder::TEXT_TYPE MoTextDecoder::DecodeText( const uchar* pBuffer, ulong nLen )
	{
		m_eType = CheckEncodeType( pBuffer, nLen );

		switch (m_eType)
		{
		case TT_ANSI:
			DecodeANSI( pBuffer, nLen );
			break;

		case TT_UTF8:
			DecodeUTF8( pBuffer, nLen );
			break;

		case TT_UTF8_BOM:
			DecodeUTF8_BOM( pBuffer, nLen );
			break;

		case TT_UNICODE_LE:
			DecodeUnicode_LE( pBuffer, nLen );
			break;

		default:
			m_eType = TT_INVALID;
			break;
		}

		return m_eType;
	}


}