// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "../MoCommon.h"
#include "MoBaseStream.h"

namespace MoCommon
{
	//================================================================
	/** Read Chunk Header ( Short )
		@remarks	청크 헤더를 읽는다.
		@param		p_pHeader : 읽을 청크.
		@return		true/false : 읽기 성공 여부
	*/
	//================================================================
	bool 
	MoBaseStream::ReadChunkHeaderS( ChunkHeaderS* p_pHeader )
	{
		if( 0 == Read( &(p_pHeader->usTag), sizeof(unsigned short), 1 ) )
		{
			return false;
		}

		if( 0 == Read( &(p_pHeader->ulSize), sizeof(unsigned long), 1 ) )
		{
			return false;
		}

		return true;
	}

	//================================================================
	/** Read Next Chunk Header ( Short )
		@remarks	다음 청크 헤더를 미리 읽는다. 읽고 나면 위치는 원상 복귀한다.
		@param		p_pNextHdr : 읽을 다음 청크.
		@return		true/false : 읽기 성공 여부
	*/
	//================================================================
	bool 
	MoBaseStream::ReadNextChunkHeaderS( ChunkHeaderS* p_pNextHdr )
	{
		long r_lCur = Tell();
		if( !ReadChunkHeaderS( p_pNextHdr ) )
			return false;

		Seek( r_lCur, SEEK_CUR );

		return true;
	}

	//================================================================
	/** Begin Write Chunk Header ( Short )
		@remarks	새 청크가 시작됨을 알려준다. 
		@param		p_usTag : 청크 태그
		@param		p_ulBeginPos : [out] 시작위치값을 저장한다. 끝낼 때 필요하다.
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::BeginWriteChunkS( unsigned short p_usTag, unsigned long& p_ulBeginPos )
	{
		ChunkHeaderS r_Hdr;
		p_ulBeginPos	= (unsigned long)Tell();		// 값 첵 한번 안하는게 맘에 걸림.
		r_Hdr.usTag		= p_usTag;
		r_Hdr.ulSize	= 0;							// 이값은 청크 내용이 다 쓰이기 전에 모르므로 자리 확보용의 의미다.

		Write( &r_Hdr.usTag, sizeof(r_Hdr.usTag), 1 );
		Write( &r_Hdr.ulSize, sizeof(r_Hdr.ulSize ), 1 );
	}

	//================================================================
	/** End Write Chunk Header ( Short )
		@remarks	청크 내용이 종료됨을 알린다.
		@param		p_ulBeginPos : 청크가 시작됐던 지점.
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::EndWriteChunkS( unsigned long p_ulBeginPos )
	{
		long r_lCurPos = Tell();
		assert( (long)p_ulBeginPos < r_lCurPos );
		
		unsigned long r_ulSize = (unsigned long)r_lCurPos - p_ulBeginPos;

		long r_lOffset = sizeof(unsigned short) - r_ulSize;
		Seek( r_lOffset, SEEK_CUR );							// 현재 위치에서 사이즈가 저장된 곳으로 이동한다.
		Write( &r_ulSize, sizeof(unsigned long), 1 );		// 가짜 데이터를 진짜 데이터로 교체한다.
		Seek( r_ulSize - sizeof(unsigned long), SEEK_CUR );	// 다시 원자리로 돌아온다.
	}

	//================================================================
	/** Skip Chunk
		@remarks	청크 하나를 건너뛴다.
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::SkipChunkS()
	{
		ChunkHeaderS r_Hdr;
		ReadChunkHeaderS( &r_Hdr );

		// Fixme - 요거 잘 안나올수도 있다.
		r_Hdr.ulSize -= ( sizeof(unsigned short) + sizeof( unsigned long) );

		if( 0 != Seek( r_Hdr.ulSize, SEEK_CUR ) )
		{
			assert( 0 );
		}
	}

	//================================================================
	/** Skip Chunk
		@remarks	청크내에서 원하는 크기만큼 건너뛴다.
		@param		p_ulSkipSize : 건너뛸 크기.
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::SkipChunkS( unsigned long p_ulSkipSize )
	{
		// 요거 코드 살짝 이상..
		long r_lRealSize = p_ulSkipSize - ( sizeof(unsigned short) + sizeof(unsigned long) );

		if( 0 != Seek( r_lRealSize, SEEK_CUR ) )
		{
			assert( 0 );
		}
	}

	//================================================================
	/** Read Chunk Header ( Long )
		@remarks	청크 헤더를 읽는다.
		@param		p_pHeader : 읽을 청크.
		@return		true/false : 읽기 성공 여부
	*/
	//================================================================
	bool 
	MoBaseStream::ReadChunkHeaderL( ChunkHeaderL* p_pHeader )
	{
		if( 0 == Read( &(p_pHeader->ulTag), sizeof(unsigned long), 1 ) )
		{
			return false;
		}

		if( 0 == Read( &(p_pHeader->ulSize), sizeof(unsigned long), 1 ) )
		{
			return false;
		}

		return true;
	}

	//================================================================
	/** Read Next Chunk Header ( Long )
		@remarks	다음 청크 헤더를 미리 읽는다. 읽고 나면 위치는 원상 복귀한다.
		@param		p_pNextHdr : 읽을 다음 청크.
		@return		true/false : 읽기 성공 여부
	*/
	//================================================================
	bool 
	MoBaseStream::ReadNextChunkHeaderL( ChunkHeaderL* p_pNextHdr )
	{
		long r_lCur = Tell();
		if( !ReadChunkHeaderL( p_pNextHdr ) )
			return false;

		Seek( r_lCur, SEEK_CUR );

		return true;
	}

	//================================================================
	/** Begin Write Chunk Header ( Long )
		@remarks	새 청크가 시작됨을 알려준다. 
		@param		p_usTag : 청크 태그
		@param		p_ulBeginPos : [out] 시작위치값을 저장한다. 끝낼 때 필요하다.
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::BeginWriteChunkL( unsigned long p_ulTag, unsigned long& p_ulBeginPos )
	{
		ChunkHeaderL r_Hdr;
		p_ulBeginPos	= (unsigned long)Tell();
		r_Hdr.ulTag		= p_ulTag;
		r_Hdr.ulSize	= 0;			// 이값은 청크 내용이 다 쓰이기 전에 모르므로 자리 확보용의 의미다.

		Write( &r_Hdr.ulTag, sizeof(r_Hdr.ulTag), 1 );
		Write( &r_Hdr.ulSize, sizeof(r_Hdr.ulSize ), 1 );
	}

	//================================================================
	/** End Write Chunk Header ( Long )
		@remarks	청크 내용이 종료됨을 알린다.
		@param		p_ulBeginPos : 청크가 시작됐던 지점.
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::EndWriteChunkL( unsigned long p_ulBeginPos )
	{
		long r_lCurPos = Tell();
		assert( (long)p_ulBeginPos < r_lCurPos );
		
		unsigned long r_ulSize = (unsigned long)r_lCurPos - p_ulBeginPos;

		long r_lOffset = sizeof(unsigned long) - r_ulSize;
		Seek( r_lOffset, SEEK_CUR );							// 현재 위치에서 사이즈가 저장된 곳으로 이동한다.
		Write( &r_ulSize, sizeof(unsigned long), 1 );		// 가짜 데이터를 진짜 데이터로 교체한다.
		Seek( r_ulSize - sizeof(unsigned long), SEEK_CUR );	// 다시 원자리로 돌아온다.
	}

	//================================================================
	/** Skip Chunk
		@remarks	청크 하나를 건너뛴다.
		@param		none
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::SkipChunkL()
	{
		ChunkHeaderL r_Hdr;
		ReadChunkHeaderL( &r_Hdr );

		// Fixme - 요거 잘 안나올수도 있다.
		r_Hdr.ulSize -= ( sizeof(unsigned long) + sizeof(unsigned long) );

		if( 0 != Seek( r_Hdr.ulSize, SEEK_CUR ) )
		{
			assert( 0 );
		}
	}

	//================================================================
	/** Skip Chunk
		@remarks	청크내에서 원하는 크기만큼 건너뛴다.
		@param		p_ulSkipSize : 건너뛸 크기.
		@return		none
	*/
	//================================================================
	void 
	MoBaseStream::SkipChunkL( unsigned long p_ulSkipSize )
	{
		// 요거 코드 살짝 이상..
		long r_lRealSize = p_ulSkipSize - ( sizeof(unsigned long) + sizeof(unsigned long) );

		if( 0 != Seek( r_lRealSize, SEEK_CUR ) )
		{
			assert( 0 );
		}
	}
}