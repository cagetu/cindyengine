// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOMEMSTREAM_H__
#define __MOMEMSTREAM_H__

#include "MoBaseStream.h"

namespace MoCommon
{
	//================================================================
	/** Memory Stream
	       @author Jaehee
		   @remarks 
				Memory Data 읽고 쓰는 Stream 클래스
	*/
	//================================================================
	class MOCOM_DLL MoMemStream : public MoBaseStream
	{
	public:
		MoMemStream();
		virtual ~MoMemStream();

		bool		Open( const MoString& strFileName, const MoString& strMode ) override;
		bool		Open( const char* strFileName, const char* strMode ) override;
		bool		Open( const MoString& strFileName ) override;
		bool		Open( uchar* pBuffer, ulong ulSize );
		void		Close() override;

		ulong		Read( void* pBuffer, ulong ulSize, ulong ulCount ) override;
		ulong		Write( const void* pBuffer, ulong ulSize, ulong ulCount ) override;

		long		Seek( long lOffset, ushort usOrigin ) override;
		long		Tell() override;

		void		ReadLine( wchar* pOut, ulong ulMaxCount = 1024 );
		void		ReadString( wchar* pOut, ulong ulMaxCount = 256 ) override;
		void		WriteString( const MoString& str ) override;

		uchar*		GetBuffer() const			{	return m_pBuffer;	}
		ulong		GetSize() const				{	return m_ulSize;	}
		ulong		GetCurPos() const			{	return m_ulCurPos;	}

		bool		IsEnd()						{	if( m_ulCurPos >= m_ulSize ) return true; return false;		}

	private:
		uchar*			m_pBuffer;				// Memory Buffer
		ulong			m_ulSize;				// Total Size
		ulong			m_ulCurPos;				// Current Position
	};
}


#endif // __MOMEMSTREAM_H__