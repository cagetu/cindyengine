// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "../MoCommon.h"
#include "MoFileStream.h"
#include "../MoStringUtil.h"

namespace MoCommon
{
	
	//------------------------------------------------------------------
	MoFileStream::MoFileStream() : MoBaseStream()
	, m_pFile(NULL)
	{
		
	}

	//------------------------------------------------------------------
	MoFileStream::~MoFileStream()
	{
		Close();
	}

	//------------------------------------------------------------------
	bool MoFileStream::Open( const MoString& strFileName, const MoString& strMode )
	{
		// 열려 있는 경우 닫고 새로 열어야 할까? 아니면 그냥 리턴 시켜야 할까?
		if (m_pFile)
			fclose( m_pFile );

		_wfopen_s( &m_pFile, strFileName.c_str(), strMode.c_str() );

		if (NULL == m_pFile)
			return false;

		return true;
	}

	bool MoFileStream::Open( const char* strFileName, const char* strMode )
	{
		if (m_pFile)
			fclose( m_pFile );

		fopen_s( &m_pFile, strFileName, strMode );

		if (NULL == m_pFile)
			return false;

		return true;
	}

	bool MoFileStream::Open( const MoString& strFileName )
	{
		if (m_pFile)
			fclose( m_pFile );

		_wfopen_s( &m_pFile, strFileName.c_str(), L"rb" );

		if (NULL == m_pFile)
			return false;

		return true;
	}

	//------------------------------------------------------------------
	void MoFileStream::Close()
	{
		if (m_pFile)
		{
			fflush( m_pFile );
			fclose( m_pFile );

			m_pFile = NULL;
		}
	}

	//------------------------------------------------------------------
	ulong MoFileStream::Read( void* pBuffer, ulong ulSize, ulong ulCount )
	{
		if (m_pFile)
		{
			return (ulong)(fread( pBuffer, ulSize, ulCount, m_pFile ));
		}

		return 0;
	}

	//------------------------------------------------------------------
	ulong MoFileStream::Write( const void* pBuffer, ulong ulSize, ulong ulCount )
	{
		if (m_pFile)
		{
			return (ulong)(fwrite( pBuffer, ulSize, ulCount, m_pFile ));
		}

		return 0;
	}

	//------------------------------------------------------------------
	long MoFileStream::Seek( long lOffset, ushort usOrigin )
	{
		if (m_pFile)
		{
			return fseek( m_pFile, lOffset, usOrigin );
		}

		return -1;
	}

	//------------------------------------------------------------------
	long MoFileStream::Tell()
	{
		if (m_pFile)
		{
			return ftell( m_pFile );
		}

		return -1;
	}

	//------------------------------------------------------------------
	void MoFileStream::ReadString( wchar* pOut, ulong ulMaxCount )
	{
		// Assumption : pOut != NULL, pOut = L'/0', enough space

		if (m_pFile)
		{	
			static wchar dest[MO_MAX_STRING_LENGTH];
			memset( dest, 0, sizeof(dest) );

			wchar* str = dest;
			int c;
			int len = 0;

			while ((( c = fgetwc( m_pFile )) != WEOF ) && ( (wchar)c != L'\0' ))
			{
				*str++ = (wchar)c;
				++len;
			}

			*str = L'\0';	
			++len;

			wcsncpy_s( pOut, ulMaxCount, dest, len );
		}
	}

	//------------------------------------------------------------------
	void MoFileStream::WriteString( const MoString& str )
	{
		if (m_pFile)
		{
			fputws( str.c_str(), m_pFile );
			fputwc( L'\0', m_pFile );
		}
	}
}