// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOSTREAMBASE_H__
#define __MOSTREAMBASE_H__

namespace MoCommon
{
	// Chunck Header
	struct MOCOM_DLL ChunkHeaderS
	{
		ushort	usTag;
		ulong	ulSize;

		const static ulong TagSize = sizeof(ushort);
		const static ulong SizeSize = sizeof(ulong);
		const static ulong HeaderSize = TagSize + SizeSize;
	};

	struct MOCOM_DLL ChunkHeaderL
	{
		ulong	ulTag;
		ulong	ulSize;

		const static ulong TagSize = sizeof(ulong);
		const static ulong SizeSize = sizeof(ulong);
		const static ulong HeaderSize = TagSize + SizeSize;
	};

	//================================================================
	/** Stream Base
		   @author Jaehee
		   @remarks 
				스트림 최상위 클래스. 추상 인터페이스 클래스
	*/
	//================================================================
	class MOCOM_DLL MoBaseStream
	{
	public:
		MoBaseStream() {}

		virtual bool		Open( const MoString& strFileName, const MoString& strMode ) abstract;
		virtual bool		Open( const char* strFileName, const char* strMode ) abstract;
		virtual bool		Open( const MoString& strFileName ) abstract;
		virtual void		Close() abstract;

		virtual ulong		Read( void* pBuffer, ulong ulSize, ulong ulCount ) abstract;
		virtual ulong		Write( const void* pBuffer, ulong ulSize, ulong ulCount ) abstract;

		virtual long		Seek( long lOffset, ushort usOrigin ) abstract;
		virtual long		Tell() abstract;

		virtual void		ReadString( wchar* pOut, ulong ulMaxCount = 256 ) abstract;
		virtual void		WriteString( const MoString& str ) abstract;

		virtual bool		ReadChunkHeaderS( ChunkHeaderS* p_pHeader );	
		virtual bool		ReadNextChunkHeaderS( ChunkHeaderS* p_pNextHdr );
		virtual void		BeginWriteChunkS( unsigned short p_usTag, unsigned long& p_ulBeginPos );
		virtual void		EndWriteChunkS( unsigned long p_ulBeginPos );
		virtual void		SkipChunkS();
		virtual void		SkipChunkS( unsigned long p_ulSkipSize );

		virtual bool		ReadChunkHeaderL( ChunkHeaderL* p_pHeader );	
		virtual bool		ReadNextChunkHeaderL( ChunkHeaderL* p_pNextHdr );
		virtual void		BeginWriteChunkL( unsigned long p_ulTag, unsigned long& p_ulBeginPos );
		virtual void		EndWriteChunkL( unsigned long p_ulBeginPos );
		virtual void		SkipChunkL();	
		virtual void		SkipChunkL( unsigned long p_ulSkipSize );

	private:
		MoBaseStream( const MoBaseStream& )	{}				// prevent copy const.

	};
}


#endif // __MOSTREAMBASE_H__