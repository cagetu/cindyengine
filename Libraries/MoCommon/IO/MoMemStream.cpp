// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "../MoCommon.h"
#include "MoMemStream.h"
#include "../MoStringUtil.h"

#include <stdio.h>
#include <windows.h>
#include <cassert>

namespace MoCommon
{
	//------------------------------------------------------------------
	MoMemStream::MoMemStream() : MoBaseStream()
	, m_pBuffer(NULL)
	, m_ulSize(0)
	, m_ulCurPos(0)
	{
	}

	//------------------------------------------------------------------
	MoMemStream::~MoMemStream()
	{
		Close();
	}

	//------------------------------------------------------------------
	bool MoMemStream::Open( const MoString& strFileName, const MoString& strMode )
	{
		if (m_pBuffer)
			Close();

		FILE* file = NULL;

		_wfopen_s( &file, strFileName.c_str(), strMode.c_str() );

		if (file)
		{
			fseek( file, 0, SEEK_END );
			m_ulSize = ftell( file );
			fseek( file, 0, SEEK_SET );

			m_pBuffer = new uchar[m_ulSize];
			fread( m_pBuffer, m_ulSize, 1, file );
			fclose( file );

			return true;
		}

		return false;
	}

	bool MoMemStream::Open( const char* strFileName, const char* strMode )
	{
		if (m_pBuffer)
			Close();

		FILE* file = NULL;

		fopen_s( &file, strFileName, strMode );

		if (file)
		{
			fseek( file, 0, SEEK_END );
			m_ulSize = ftell( file );
			fseek( file, 0, SEEK_SET );

			m_pBuffer = new uchar[m_ulSize];
			fread( m_pBuffer, m_ulSize, 1, file );
			fclose( file );

			return true;
		}

		return false;
	}

	//------------------------------------------------------------------
	bool MoMemStream::Open( const MoString& strFileName )
	{
		if( m_pBuffer ) 
			Close();

		FILE* handle = NULL;
		_wfopen_s( &handle, strFileName.c_str(), L"rb" );
		if (!handle)
			return false;

		long filesize = 0;
		fseek( handle , 0 , SEEK_END );
		filesize = ftell( handle );
		fclose( handle );

		//m_pBuf = ( unsigned char* )VirtualAlloc( NULL , filesize , MEM_COMMIT , PAGE_READWRITE );
		m_pBuffer = new uchar[filesize];
		m_ulSize = filesize;

		HANDLE hFile = CreateFile(	strFileName.c_str(),
									GENERIC_READ,
									FILE_SHARE_READ,
									NULL,
									OPEN_EXISTING,
									FILE_ATTRIBUTE_NORMAL | FILE_FLAG_NO_BUFFERING,
									NULL );

		long totalbytesread = 0;
		while ( totalbytesread < filesize )
		{
			DWORD bytesread = 0;
			ReadFile( hFile ,
					&m_pBuffer[ totalbytesread ] , 
					( totalbytesread < filesize - 32768 ) ? 32768 : filesize - totalbytesread,	//32k씩 읽는다
					&bytesread ,
					NULL );

			totalbytesread += bytesread;
		}

		CloseHandle( hFile );

		return true;
	}

	//------------------------------------------------------------------
	bool MoMemStream::Open( uchar* pBuffer, ulong ulSize )
	{
		// Assumption : pBuffer != NULL, ulSize != 0
		
		if (m_pBuffer)
			Close();

		m_pBuffer = new uchar[ulSize];
		memset( m_pBuffer, 0, sizeof(uchar)*ulSize );

		if (pBuffer)
		{
			memcpy( m_pBuffer, pBuffer, sizeof(uchar)*ulSize );
			m_ulSize = ulSize;

			return true;
		}

		return false;
	}

	//------------------------------------------------------------------
	void MoMemStream::Close()
	{
		if (m_pBuffer)
		{
			SAFEDELS( m_pBuffer );
			m_ulSize	= 0;
			m_ulCurPos	= 0;
		}
	}

	//------------------------------------------------------------------
	ulong MoMemStream::Read( void* pBuffer, ulong ulSize, ulong ulCount )
	{
		if (m_ulCurPos < m_ulSize)
		{
			ulong readsize = ulSize * ulCount;
			if (m_ulCurPos + readsize >= m_ulSize )
			{
				readsize = m_ulSize - m_ulCurPos;
			}

			memcpy( pBuffer, &m_pBuffer[m_ulCurPos], readsize );
			m_ulCurPos += readsize;

			return readsize;
		}
		return 0;
	}

	//------------------------------------------------------------------
	ulong MoMemStream::Write( const void* pBuffer, ulong ulSize, ulong ulCount )
	{
		if (m_ulCurPos < m_ulSize)
		{
			ulong writesize = ulSize * ulCount;

			if (m_ulCurPos + writesize >= m_ulSize )
			{
				writesize = m_ulSize - m_ulCurPos;
			}
			memcpy( &m_pBuffer[m_ulCurPos], pBuffer, writesize );
			m_ulCurPos += writesize;

			return writesize;
		}

		return 0;
	}

	//------------------------------------------------------------------
	long MoMemStream::Seek( long lOffset, ushort usOrigin )
	{
		if (m_pBuffer)
		{
			ulong tmp = m_ulCurPos;

			switch (usOrigin)
			{
			case SEEK_SET:
				if (lOffset >= 0 )
				{
					m_ulCurPos = lOffset;
				}
				break;

			case SEEK_END:
				if (m_ulSize - lOffset)
				{
					m_ulCurPos = m_ulSize - lOffset;
				}
				break;

			default:			
				m_ulCurPos += lOffset;
			}

			if ((m_ulCurPos < 0) || (m_ulCurPos > m_ulSize))
			{
				m_ulCurPos = tmp;
				return -1;
			}

			return 0;
		}

		return -1;
	}

	//------------------------------------------------------------------
	long MoMemStream::Tell()
	{
		return (long)m_ulCurPos;
	}

	//------------------------------------------------------------------
	void MoMemStream::ReadString( wchar* pOut, ulong ulMaxCount )
	{
		// !!!!! You Must Debug!!

		// Assumption : pOut != NULL, pOut = L'/0', enough space
		if (m_pBuffer)
		{	
			static wchar dest[MO_MAX_STRING_LENGTH];
			memset( dest, 0, sizeof(dest) );
			ulong size = sizeof(wchar);

			wchar* str = dest;
			int c;
			int len = 0;
			bool end = false;

			while (!end)
			{
				memcpy( &c, &m_pBuffer[m_ulCurPos], size);

				if (( (wchar)c == WEOF ) || ( (wchar)c == L'\0' ) || ( (wchar)c == L'\n' ) || ( (wchar)c == L'\r' ))
				{
					end = true;
				}
				else
				{
					*str++ = (wchar)c;
					++len;
				}

				m_ulCurPos += size;
			}

			*str = L'\0';	
			++len;

			wcsncpy_s( pOut, ulMaxCount, dest, len );
		}
	}

	//------------------------------------------------------------------
	void MoMemStream::ReadLine( wchar* pOut, ulong ulMaxCount )
	{
		// !!!!! You Must Debug!!

		// Assumption : pOut != NULL, pOut = L'/0', enough space
		if (m_pBuffer)
		{	
			static wchar dest[MO_MAX_STRING_LENGTH];
			memset( dest, 0, sizeof(dest) );
			ulong size = sizeof(wchar);

			wchar* str = dest;
			int c;
			int len = 0;
			bool end = false;

			while (!end)
			{
				memcpy( &c, &m_pBuffer[m_ulCurPos], size);

				if (( (wchar)c == WEOF ) || ( (wchar)c == L'\n' ))
				{
					end = true;
				}
				else
				{
					*str++ = (wchar)c;
					++len;
				}

				m_ulCurPos += size;
			}

			*str = L'\0';	
			++len;

			wcsncpy_s( pOut, ulMaxCount, dest, len );
		}
	}

	//------------------------------------------------------------------
	void MoMemStream::WriteString( const MoString& str )
	{
		if (m_pBuffer)
		{
			ulong size = sizeof(wchar);

			Write( str.c_str(), size, (ulong)str.length() );
			Write( L'\0', size, 1 );
		}
	}
}