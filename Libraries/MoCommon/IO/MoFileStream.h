// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOFILESTREAM_H__
#define __MOFILESTREAM_H__

#include "MoBaseStream.h"

namespace MoCommon
{
	//================================================================
	/** File Stream
		   @author Jaehee
		   @remarks 
				File Stream을 처리하는 Wrapper 클래스
	*/
	//================================================================
	class MOCOM_DLL MoFileStream : public MoBaseStream
	{
	public:
		MoFileStream();
		virtual ~MoFileStream();

		bool		Open( const MoString& strFileName, const MoString& strMode ) override;
		bool		Open( const char* strFileName, const char* strMode ) override;
		bool		Open( const MoString& strFileName ) override;
		void		Close() override;

		ulong		Read( void* pBuffer, ulong ulSize, ulong ulCount ) override;
		ulong		Write( const void* pBuffer, ulong ulSize, ulong ulCount ) override;

		long		Seek( long lOffset, ushort usOrigin ) override;
		long		Tell() override;

		void		ReadString( wchar* pOut, ulong ulMaxCount ) override;
		void		WriteString( const MoString& str ) override;

	private:
		FILE*				m_pFile;	
	};
}


#endif // __MOFILESTREAM_H__