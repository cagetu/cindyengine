// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOPROXY_H__
#define __MOPROXY_H__

namespace MoCommon
{
	//================================================================
	/** Proxy Object
	       @author cagetu
		   @remarks 
				기본적인 Proxy Pattern을 구현
	*/
	//================================================================	
	template <class T>
	class MOCOM_DLL MoProxy
	{
	protected:
		T*		m_pObject;

	public:
		MoProxy();
		MoProxy( T* pObject );
		MoProxy( const MoProxy& Ptr );

		bool		IsNull() const;
		T*			GetPtr() const;

		MoProxy&	operator= ( const MoProxy& Ptr );
		T*			operator-> () const;
	};

	#include "MoProxy.inl"
}

#endif	// __MOPROXY_H__