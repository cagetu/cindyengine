#ifndef _MINI_eXtensible_Markup_Language
#define _MINI_eXtensible_Markup_Language


#include <tchar.h>

#ifdef _UNICODE
#include <string>
#define	_string	std::wstring
#else
#include <string>
#define	_string	std::string
#endif

namespace MoCommon
{
class	MOCOM_DLL CMiniXML
{
public :
	CMiniXML() ;
	CMiniXML(const CMiniXML & xml) ;
	~CMiniXML() ;

	bool	Open(const TCHAR * fname, bool defaultutf8=true);

	bool	Parse(const TCHAR * src);
	bool	Parse(const void* ptr8, int len, bool utf8default=true);

	bool	Compare(const CMiniXML & xml, bool checkorder=true) const;

	bool	Save(const TCHAR * fname);

	void	Clear();

	const TCHAR*	GetError() { return m_ErrorCode; }
	CMiniXML&		operator = (const CMiniXML & xml) ;

	class iterator;
	class const_iterator;

	iterator		GetRoot();
	const_iterator	GetRoot() const;

	//	deprecated
//	iterator	Insert(iterator & destnode, const TCHAR * name, bool front=false) { return destnode.Insert(name, front); }
//	iterator	Insert(iterator & destnode, const const_iterator & src, bool front=false) { return destnode.Insert(src, front); }
//	iterator	InsertChild(iterator & destnode, const TCHAR * name, bool top=false) { return destnode.InsertChild(name, top); }
//	iterator	InsertChild(iterator & destnode, const const_iterator & src, bool top=false) { return destnode.InsertChild(src, top); }
//	iterator	Erase(iterator & iterator) { return iterator.Erase(); }

	static iterator nul;

private :
	struct _NODE;

public :
	class MOCOM_DLL const_iterator
	{
	public :
		const_iterator();
		const_iterator(const const_iterator & it);
		const_iterator(const iterator & it);

		const TCHAR *	GetName() const;
		const TCHAR *	GetValue() const;
		const TCHAR *	GetAttribute(const TCHAR * att) const;

		bool			HasChild() const;
		bool			HasNode() const;

		const TCHAR*	operator [] (const TCHAR* att) const { return GetAttribute(att); }
		const TCHAR*	operator* () const { return GetValue(); }

		const_iterator	Find(const TCHAR * name) const;
		const_iterator	FindChild(const TCHAR * name) const;

		const_iterator	Next() const;
		const_iterator	GetChild() const;

		bool operator == (const void * ptr) const;	// for compare NULL (ptr must be NULL)
		bool operator != (const void * ptr) const;	// for compare NULL (ptr must be NULL)

		const_iterator	operator ++ ();				// Prefix increment operator
		const_iterator	operator ++ (int);			// Postfix increment operator

	protected :
		const_iterator(const _NODE * iterator, const _NODE * parent, bool accessed=false);
		const _NODE * FindNode(const TCHAR * name) const;

		friend	CMiniXML;
		friend	CMiniXML::iterator;

		const _NODE *	m_pNodeConst;
		const _NODE *	m_pParentConst;

		bool			m_Accessed;
	} ;

	class MOCOM_DLL iterator : public const_iterator
	{
	public :
		iterator();
		iterator(const iterator & n);
#ifdef _DEBUG
		~iterator();
#endif
		void		SetName(const TCHAR * name);
		void		SetValue(const TCHAR *value);

		void		SetAttribute(const TCHAR * name, const TCHAR * value);
		void		RemoveAttribute(const TCHAR * name);

		iterator	Find(const TCHAR * name) const;
		iterator	FindChild(const TCHAR * name) const;

		iterator	Next() const;
		iterator	GetChild() const;

		iterator	Insert(const TCHAR * name, bool front=false);
		iterator	Insert(const const_iterator & src, bool front=false);
		iterator	InsertChild(const TCHAR * name, bool top=false);
		iterator	InsertChild(const const_iterator & src, bool top=false);

		iterator	Erase();

		iterator	operator ++ ();			// Prefix increment operator
		iterator	operator ++ (int);		// Postfix increment operator

	private :
		iterator(_NODE * iterator, _NODE * parent, bool accessed=false);

		friend	CMiniXML;

		_NODE *	m_pNode;
		_NODE *	m_pParent;
	} ;

private :
	struct _NODE
	{
		struct _ITEM		//	attribute
		{
			_string	name;
			_string	value;
			_ITEM *	next;
		} ;

		_string name;
		_string value;
		_NODE *	next, * prev, * child;
		_ITEM *	item;
	} ;

	const TCHAR *	GetToken(const TCHAR * buf, int *tokenlen);
	const TCHAR *	GetToken(const TCHAR * buf, int len, int *tokenlen, int * skipcnt, const TCHAR * blkch=_T("/="));
	TCHAR *			ReadText(const TCHAR * fname, bool utf8default=false);
	unsigned char *	ReadFile(const TCHAR * fname, int * len);

	_string			GetString(const TCHAR *ptr, int l);

	_NODE *			CreateNode(const TCHAR *p, int len, bool *singleitem);
	const TCHAR *	ParseXML(const TCHAR *p, _NODE * parent);
	void			Store(FILE *fp, _NODE * iterator, _NODE * parent, int lv=0, bool notab=false);

	static _NODE *	AllocNode(const _string & name);
	static void		ReleaseNode(_NODE * iterator);

	static _NODE *	Duplicate(_NODE * parent, const _NODE * src, bool all, _NODE * dest);

	static bool		CompareNodeList(const_iterator na, const_iterator nb, bool checkorder);
	static bool		CompareNode(const_iterator na, const_iterator nb, bool checkorder);
	static bool		CompareItem(const _NODE * na, const _NODE * nb);

	_NODE 			m_Root;			//	dummy root iterator
	const TCHAR *	m_ErrorCode;
} ;
}
#endif