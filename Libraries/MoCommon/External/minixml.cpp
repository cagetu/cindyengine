//	MINIXML	2007/02/05
//	noerror@hitel.net
//	http://www.digibath.com/slimxml
//
#include <windows.h>
#include "../MoCommon.h"
#include "minixml.h"
#include <stdio.h>
#include <crtdbg.h>

namespace MoCommon
{

#ifndef _countof
#define _countof(A)	(sizeof(A)/sizeof(*A))
#endif
#ifdef _DEBUG
static int _id = 1;
#endif

CMiniXML::iterator CMiniXML::nul;

	CMiniXML::CMiniXML()
{
	memset(&m_Root, 0, sizeof(m_Root));
	m_Root.child	= NULL;
	m_ErrorCode		= NULL;
}


	CMiniXML::CMiniXML(const CMiniXML & xml)
{
	operator = (xml);
}


CMiniXML&	CMiniXML::operator = (const CMiniXML & xml)
{
	memset(&m_Root, 0, sizeof(m_Root));
	m_Root.child	= NULL;
	m_ErrorCode		= NULL;

	Duplicate(&m_Root, xml.m_Root.child, true, m_Root.child);
	return *this;
}


CMiniXML::_NODE * CMiniXML::Duplicate(_NODE * parent, const _NODE * src, bool allnode, _NODE * dest)
{
	const _NODE * first = src;
	_NODE * node;

	if (src == NULL)
		return NULL;

	do
	{
		node = AllocNode(src->name);
		node->value = src->value;

		if (dest == NULL)
		{
			_ASSERT(parent->child == NULL);
			parent->child = dest = node;
			node->next = node->prev = node;
		}	else
		{
			node->next = dest->next;
			node->prev = dest;

			dest->next->prev = node;
			dest->next = node;
			dest = node;
		}

		const _NODE::_ITEM * ii = src->item;
		_NODE::_ITEM * prev = NULL;

		for(; ii!=NULL; ii=ii->next)
		{
			_NODE::_ITEM * item = new _NODE::_ITEM;
			item->name	= ii->name;
			item->value	= ii->value;
			item->next	= NULL;

			if (prev == NULL)
				node->item	= item;
			else
				prev->next	= item;
			prev = item;
		}

		if (src->child != NULL)
		{
			_ASSERT(node->child == NULL);
			Duplicate(node, src->child, true, node->child);
		}

		src = src->next;
	}	while(allnode == true && src != first);

	return node;
}


	CMiniXML::~CMiniXML()
{
	Clear();
}


const TCHAR * CMiniXML::GetToken(const TCHAR * buf, int *tokenlen)
{	//	for tag
	int i, l;
	for(i=0; buf[i]; i++)
	{
		if ((unsigned)buf[i] == '<' && memcmp(&buf[i], _T("<![CDATA["), sizeof(TCHAR)*9))
		{
			for(l=1; buf[i+l] != '>'; l++)
			{
				if (buf[i+l] == '\0')
				{
					m_ErrorCode = _T("괄호가 닫히지 않았습니다.");
					return NULL;
				}
			}
			*tokenlen = l+1;
			return &buf[i];
		}
		else if ((unsigned)buf[i] > ' ')
		{
			for(l=0; buf[i+l] != '\0';)
			{
				if (!memcmp(&buf[i+l], _T("<![CDATA["), sizeof(TCHAR)*9))
				{
					for(; buf[i+l] != '\0'; l++)
					{
						if (!memcmp(&buf[i+l], _T("]]>"), sizeof(TCHAR)*3))
						{
							l += 3;
							break;
						}
					}
				}
				else if (buf[i+l] == '<')
					break;
				else
					l++;
			}
			*tokenlen = i+l;
			return &buf[0];
		}
	}
	return NULL;
}


const TCHAR * CMiniXML::GetToken(const TCHAR * buf, int len, int *tokenlen, int * skipcnt, const TCHAR * blkch)
{	//	for item
	int i, l;
	for(i=0; i<len; i++)
	{
		if ((unsigned)buf[i] > ' ')
		{
			if (buf[i] == '\"' || buf[i] == '\'')
			{
				for(l=0; buf[i+l+1] != buf[i]; l++)
				{
					if (i+l+1 > len)
						return NULL;
				}
				*tokenlen	= l;
				*skipcnt	= l + 1;
				return &buf[i+1];
			}	else
			if (_tcschr(blkch, buf[i]) != NULL)
			{
				*tokenlen	= 1;
				*skipcnt	= 1;
				return &buf[i];
			}	else
			{
				for(l=0; (unsigned)buf[i+l] > ' ' && _tcschr(blkch, buf[i+l]) == NULL && i+l < len; l++) ;
				*tokenlen	= l;
				*skipcnt	= l;
				return &buf[i];
			}
		}
	}
	return NULL;
}


const TCHAR * CMiniXML::ParseXML(const TCHAR *p, _NODE * parent)
{
	_NODE	* node;
	int		len;
	bool	singleitem;

	p = GetToken(p, &len);

	if (p == NULL)
		return NULL;

	if (*p != '<')
	{
		m_ErrorCode = _T("엘리먼트는 태그로 시작해야 합니다.");
		return NULL;
	}

	if (memcmp(p, _T("<?"), 2*sizeof(TCHAR)) == 0)
	{
		p += len;
		return p;
	}

	//	주석은 <!-- ... -> 형태로 표기된다
	if (memcmp(p, _T("<!--"), 4*sizeof(TCHAR)) == 0)
	{
		for(; p[3] != '\0'; p++)
			if (!memcmp(p-3, _T("-->"), 3*sizeof(TCHAR)))
				return p;
		return p;
	}

	node = CreateNode(p+1, len-2, &singleitem);
	if (node == NULL)
		return NULL;
	p += len;

	if (parent->child == NULL)
	{
		parent->child = node;
		node->next = node->prev = node;
	}	else
	{
		node->next = parent->child;
		node->prev = parent->child->prev;

		parent->child->prev->next = node;
		parent->child->prev = node;
	}

	if (singleitem == true)
		return p;

	p = GetToken(p, &len);

	if (p == NULL)
	{
		m_ErrorCode = _T("태그가 닫히지 않았습니다.");
		return NULL;
	}

	if (p[0] != '<' || !memcmp(p, _T("<![CDATA["), sizeof(TCHAR)*9))
	{
		node->value = GetString(p, len);
		p += len;

		p = GetToken(p, &len);
	}	else
	{
		node->value = _T("");
	}

	while(p != NULL)
	{
		if (p[0] == '<' && p[1] == '/')
		{
			const TCHAR * t;
			int l, skipcnt;
			_string endtag;
			t = GetToken(p+2, len-3, &l, &skipcnt);
			endtag = GetString(t, l);
			if (_tcscmp(node->name.c_str(), endtag.c_str()) != 0)
			{
				m_ErrorCode = _T("시작태그와 종료태그가 일치하지 않습니다.");// <test></teeest>
				return NULL;
			}
			p += len;
			return p;
		}	else
		{
			p = ParseXML(p, node);

			if (p == NULL)
				return NULL;

			p = GetToken(p, &len);
		}
	}
	return NULL;
}


CMiniXML::_NODE * CMiniXML::CreateNode(const TCHAR *p, int len, bool *singleitem)
{
	int l, skipcnt;
	const TCHAR * t;
	_NODE * node;
	bool meetend = false;

	//	get tag name

	t = GetToken(p, len, &l, &skipcnt);
	node = AllocNode(GetString(t, l));
	t += skipcnt;

	//	parse attribute

	_NODE::_ITEM * prev = NULL;

	do {
		t = GetToken(t, (int)(p+len-t), &l, &skipcnt);

		if (t != NULL)
		{	//	attribute

			if (l == 1 && *t == '/')	//	<tag/>
			{
				meetend = true;
				break;
			}

			_NODE::_ITEM * item = new _NODE::_ITEM;
			item->next = NULL;
			item->name = GetString(t, l);
			t += skipcnt;

			t = GetToken(t, (int)(p+len-t), &l, &skipcnt);
			if (t[0] != '=')
			{
				m_ErrorCode = _T("속성과 속성값 정의가 올바르지 않거나 값이 지정되어 있지 않습니다.");// <test id>
				return NULL;
			}
			t += skipcnt;

			t = GetToken(t, (int)(p+len-t), &l, &skipcnt);
			item->value = GetString(t, l);
			t += skipcnt;

			if (prev == NULL)
			{
				node->item = item;
			}
			else
			{
				prev->next = item;
			}
			item->next = NULL;
			prev = item;
		}
	}	while(t != NULL);

	*singleitem = meetend;

	return node;
}


CMiniXML::_NODE * CMiniXML::AllocNode(const _string & name)
{
	_NODE * node = new _NODE;
	node->name	= name;
	node->item	= NULL;
	node->next	= NULL;
	node->prev	= NULL;
	node->child	= NULL;
	return node;
}


void CMiniXML::ReleaseNode(_NODE * node)
{
	_NODE * next, * first = node;

	do
	{
		_NODE::_ITEM * item, * nextitem;
		for(item=node->item; item!=NULL; )
		{
			nextitem = item->next;
			delete item;
			item = nextitem;
		}

		if (node->child)
			ReleaseNode(node->child);
		next = node->next;
		delete node;
		node = next;
	}	while(node != NULL && node != first);
}


void CMiniXML::Clear()
{
	iterator n = GetRoot();

	while(n != NULL)
	{
		n.Erase();
		n = GetRoot();
	}
}


CMiniXML::const_iterator CMiniXML::GetRoot() const
{
	return const_iterator(m_Root.child, &m_Root, false);
}


CMiniXML::iterator CMiniXML::GetRoot()
{
	return iterator(m_Root.child, &m_Root, false);
}


//	&#NNNN => 10진수
//	&#xNNN => 16진수


static const TCHAR * _convtable[][3] =
{
	_T("&lt;"), _T("<"), _T("<"),
	_T("&gt;"), _T(">"), _T(">"),
	_T("&amp;"), _T("&"), NULL,
	_T("&quot;"), _T("\""), NULL,
	_T("&apos;"), _T("'"), NULL,
} ;


_string	CMiniXML::GetString(const TCHAR *p, int l)
{
	int i, off;

	_string r = _string(p, l);

	for(off=0; off<(int)r.size();)
	{
		const TCHAR * ptr = r.c_str();

		if (ptr[off] == '&')
		{
			for(i=0; i<sizeof(_convtable)/sizeof(*_convtable); i++)
			{
				if (!memcmp(&ptr[off], _convtable[i][0], _tcslen(_convtable[i][0]) * sizeof(TCHAR)))
				{
					r.replace(off, _tcslen(_convtable[i][0]), _convtable[i][1]);
					break;
				}
			}
			off++;
		}
		else if (!memcmp(&ptr[off], _T("<![CDATA["), sizeof(TCHAR)*9))
		{
			r.replace(off, 9, _T(""));

			ptr = r.c_str();

			for(; off<(int)r.size(); off++)
			{
				if (!memcmp(&ptr[off], _T("]]>"), sizeof(TCHAR)*3))
				{
					r.replace(off, 3, _T(""));
					break;
				}
			}
		}
		else
		{
			off++;
		}
	}

	return r;
}


bool CMiniXML::Open(const TCHAR * fname, bool defaultutf8)
{
	unsigned char * ptr8;
	bool ret;
	int len;

	ptr8 = ReadFile(fname, &len);
	if (ptr8 == NULL)
		return NULL;

	ret = Parse(ptr8, len, defaultutf8);
	delete [] ptr8;
	return ret;
}

#define _STORE_UTF8

bool CMiniXML::Save(const TCHAR * fname)
{
	FILE * fp = NULL;

#if _MSC_VER >= 1400
	_tfopen_s(&fp, fname, _T("wb"));
#else
	fp = _tfopen(fname, _T("wb"));
#endif

	if (fp == NULL)
		return false;

#ifdef _STORE_UTF8
	static const unsigned char _utf8header[3] = { 0xef, 0xbb, 0xbf };
	fwrite(_utf8header, 3, 1, fp);
#elif defined(_UNICODE)
	static const unsigned char _unicodeheader[2] = { 0xff, 0xfe };
	fwrite(_unicodeheader, 2, 1, fp);
#endif

	Store(fp, m_Root.child, &m_Root);

	fclose(fp);
	return true;
}

static void _ftputs(FILE *fp, const TCHAR * str, int len=-1)
{
#ifdef _STORE_UTF8
	if (len == -1)
		len = (int)_tcslen(str);

	if (len > 512)
	{
		for(int i=0; i<len; i+=512)
			_ftputs(fp, &str[i], i+512 < len ? 512 : len-i);
	}
	else
	{
		char dest[1024];
		int l = WideCharToMultiByte(CP_UTF8, 0, str, len, dest, _countof(dest), NULL, NULL);
		fwrite(dest, 1, l, fp);
	}
#else
	_ftprintf(fp, _T("%s"), str);
#endif
}

static void _ftputs2(FILE *fp, const TCHAR * str)
{
	_string text = _string(str);
	int i;

	for(i=0; i<sizeof(_convtable)/sizeof(*_convtable); i++)
	{
		if (_convtable[i][2] != NULL)
		{
			const TCHAR * key = _tcsstr(text.c_str(), _convtable[i][2]);
			while(key != NULL)
			{
				int off = (int)(key - text.c_str());
				text.replace(off, _tcslen(_convtable[i][2]), _convtable[i][0]);
				key = _tcsstr(text.c_str() + off, _convtable[i][2]);
			}
		}
	}

	_ftputs(fp, text.c_str());
}


void CMiniXML::Store(FILE *fp, _NODE * node, _NODE * parent, int lv, bool notab)
{
	_NODE::_ITEM * item;
	int i;
	bool cr;

	if (node == NULL)
		return ;

	do
	{
		bool nullvalue = false;

		if (notab == false)
		{
			for(i=0; i<lv; i++)
				_ftputs(fp, _T("\t"));
		}	else
		{
			notab = false;
		}
//		_ftprintf(fp, _T("<%s"), node->name.c_str());
		_ftputs(fp, _T("<"));
		_ftputs2(fp, node->name.c_str());

		for(item=node->item; item; item=item->next)
		{
//			_ftprintf(fp, _T(" %s=\"%s\""), item->name.c_str(), item->value.c_str());
			_ftputs(fp, _T(" "));
			_ftputs2(fp, item->name.c_str());
			_ftputs(fp, _T("=\""));
			_ftputs2(fp, item->value.c_str());
			_ftputs(fp, _T("\""));
		}

		if (_tcslen(node->value.c_str()) > 0)
		{
//			_ftprintf(fp, _T(">%s"), node->value.c_str());
			_ftputs(fp, _T(">"));
			_ftputs2(fp, node->value.c_str());
			cr = false;
		}	else
		{
			if (node->child != NULL)
			{
				_ftputs(fp, _T(">"));
				_ftputs(fp, _T("\r\n"));
				cr = true;
			}	else
			{
				_ftputs(fp, _T("/>"));
				nullvalue = true;
				cr = false;
			}
		}

		if (node->child != NULL)
		{
			_ASSERT(nullvalue == false);
			Store(fp, node->child, node, lv+1, cr == false ? true : false);
			cr = true;
		}

		if (cr == true)
		{
			_ASSERT(nullvalue == false);
			for(i=0; i<lv; i++)
				_ftputs(fp, _T("\t"));
		}
//		_ftprintf(fp, _T("</%s>\r\n"), node->name.c_str());
		if (nullvalue == false)
		{
			_ftputs(fp, _T("</"));
			_ftputs2(fp, node->name.c_str());
			_ftputs(fp, _T(">"));
		}
		_ftputs(fp, _T("\r\n"));
		node = node->next;
	}	while(node != parent->child) ;
}


bool CMiniXML::Parse(const TCHAR * p)
{
	Clear();

	m_ErrorCode = NULL;
	do
	{
		p = ParseXML(p, &m_Root);
	}	while(p != NULL && *p != '\0');
	return m_ErrorCode == NULL && m_Root.child != NULL ? true : false;
}


bool CMiniXML::Parse(const void * vptr, int len, bool utf8default)
{
	const unsigned char * ptr8 = (const unsigned char *) vptr;
	TCHAR * ptr = NULL;

	if (ptr8[0] == 0xff && ptr8[1] == 0xfe)
	{	//	unicode
#ifdef _UNICODE
		ptr = new TCHAR [(len-2)/2 + 1];
		memcpy(ptr, &ptr8[2], len-2);
		ptr[(len-2)/2] = '\0';
#else
		int cnt = WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)&ptr8[2], (len-2)/2, NULL, 0, NULL, FALSE);
		ptr = new TCHAR [cnt+1];
		WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)&ptr8[2], (len-2)/2, ptr, cnt, NULL, FALSE);
		ptr[cnt] = '\0';
#endif
	}	else
	if (utf8default == true ||
		ptr8[0] == 0xef && ptr8[1] == 0xbb && ptr8[2] == 0xbf)
	{	//	Universal Set Transformation Format 8bit
		unsigned short code;
		int cnt, i, startpos = ptr8[0] == 0xef && ptr8[1] == 0xbb && ptr8[2] == 0xbf ? 3 : 0;

		for(i=startpos, cnt=0; i<len; )
		{
			if (ptr8[i] & 0x80)
			{
				i += (ptr8[i]&0xe0) == 0xc0 ? 2 : 3;
#ifdef _UNICODE
				cnt++;
#else
				cnt += 2;
#endif
			}	else
			{
				i ++;
				cnt++ ;
			}
		}
		ptr = new TCHAR [cnt + 1];
		for(i=startpos, cnt=0; i<len;)
		{
			if (ptr8[i] & 0x80)
			{
				if ((ptr8[i]&0xe0) == 0xc0)
				{
					code = ((ptr8[i] & 0x1f) << 6) | (ptr8[i+1] & 0x3f);
					i += 2;
				}	else
				{
					code = ((ptr8[i] & 0x0f) << 12) | ((ptr8[i+1] & 0x3f) << 6) | (ptr8[i+2] & 0x3f);
					i += 3;
				}
#ifdef _UNICODE
				ptr[cnt] = code;
				cnt ++;
#else
				WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)&code, 1, &ptr[cnt], 2, NULL, FALSE);
				cnt += 2;
#endif
			}	else
			{
				ptr[cnt] = ptr8[i];
				i ++;
				cnt ++;
			}
		}
		ptr[cnt] = '\0';
	}	else
	{
#ifdef _UNICODE
		int cnt = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)ptr8, len, NULL, 0);
		ptr = new TCHAR [cnt+1];
//		MultiByteToWideChar(CP_OEMCP, 0, (LPCSTR)ptr8, len, ptr, cnt);
		MultiByteToWideChar(CP_ACP, 0, (LPCSTR)ptr8, len, ptr, cnt);
		ptr[cnt] = '\0';
#else
		ptr = new TCHAR [len+1];
		memcpy(ptr, ptr8, len);
		ptr8[len] = '\0';
#endif
	}
	bool ret = Parse(ptr);
	delete [] ptr;
	return ret;
}


unsigned char * CMiniXML::ReadFile(const TCHAR * fname, int * len)
{
	unsigned char * ptr;
	FILE * fp = NULL;

#if _MSC_VER >= 1400
	_tfopen_s(&fp, fname, _T("rb"));
#else
	fp = _tfopen(fname, _T("rb"));
#endif
	if (fp == NULL)
		return NULL;

	fseek(fp, 0, SEEK_END);
	*len = ftell(fp);
	ptr = new unsigned char [*len];
	fseek(fp, 0, SEEK_SET);
	fread(ptr, 1, *len, fp);
	fclose(fp);

	return ptr;
}


///


CMiniXML::const_iterator::const_iterator()
{
	m_pNodeConst	= NULL;
	m_pParentConst	= NULL;
	m_Accessed		= false;
}


CMiniXML::const_iterator::const_iterator(const const_iterator & it)
{
	m_pNodeConst	= it.m_pNodeConst;
	m_pParentConst	= it.m_pParentConst;
	m_Accessed		= it.m_Accessed;
}


CMiniXML::const_iterator::const_iterator(const iterator & it)
{
	m_pNodeConst	= it.m_pNodeConst;
	m_pParentConst	= it.m_pParentConst;
	m_Accessed		= it.m_Accessed;
}


CMiniXML::const_iterator::const_iterator(const _NODE * node, const _NODE * parent, bool accessed)
{
	m_pNodeConst	= node;
	m_pParentConst	= parent;
	m_Accessed		= accessed;
}


const TCHAR * CMiniXML::const_iterator::GetName() const
{
	return m_pNodeConst->name.c_str();
}


const TCHAR * CMiniXML::const_iterator::GetValue() const
{
	return m_pNodeConst->value.c_str();
}


const TCHAR * CMiniXML::const_iterator::GetAttribute(const TCHAR * att) const
{
	for(const _NODE::_ITEM * item=m_pNodeConst->item; item!=NULL; item=item->next)
	{
		if (_tcscmp(item->name.c_str(), att) == 0)
		{
			return item->value.c_str();
		}
	}
	return NULL;
}


bool CMiniXML::const_iterator::HasChild() const
{
	return m_pNodeConst->child != NULL ? true : false;
}


bool CMiniXML::const_iterator::HasNode() const
{
	return m_pNodeConst ? true : false;
}


CMiniXML::const_iterator CMiniXML::const_iterator::Find(const TCHAR * name) const
{
	const _NODE * node = m_pNodeConst;
	if (node != NULL)
	{
		do
		{
			if ((m_Accessed == false || node != m_pNodeConst) && //	find 해서 찾은 노드라면 자신 노드에서 검색 성공하게 되므로, 자기 자신은 skip
				_tcscmp(node->name.c_str(), name) == 0)
			{
				return CMiniXML::const_iterator(node, m_pParentConst, true);
			}
			node = node->next;
		}	while(node != m_pParentConst->child);
	}
	return CMiniXML::const_iterator(NULL, NULL);
}


CMiniXML::const_iterator CMiniXML::const_iterator::FindChild(const TCHAR * name) const
{
	const_iterator item = GetChild();
	if (item != NULL)
		return item.Find(name);
	return item;
}


CMiniXML::const_iterator CMiniXML::const_iterator::GetChild() const
{
	return CMiniXML::const_iterator(m_pNodeConst->child, m_pNodeConst);
}


CMiniXML::iterator	CMiniXML::iterator::Insert(const TCHAR * name, bool front)
{
	if (m_pNode == NULL)
	{
		if (m_pParent != NULL)
		{
			_NODE * newnode = AllocNode(name);
			m_pNodeConst = m_pNode = newnode;
			m_pParent->child = newnode;
			newnode->next = newnode->prev = newnode;
			return CMiniXML::iterator(newnode, m_pParent);
		}
		return CMiniXML::iterator(NULL, NULL);
	}
	else
	{
		_NODE * newnode = AllocNode(name);

		if (front == false)
		{
			newnode->next = m_pNode->next;
			newnode->prev = m_pNode;
			m_pNode->next->prev = newnode;
			m_pNode->next = newnode;
		}
		else
		{
			newnode->next = m_pNode;
			newnode->prev = m_pNode->prev;
			m_pNode->prev->next = newnode;
			m_pNode->prev = newnode;

			if (m_pParent->child == m_pNode)
				m_pParent->child = newnode;
		}
		return CMiniXML::iterator(newnode, m_pParent);
	}
}


CMiniXML::iterator	CMiniXML::iterator::Insert(const const_iterator & src, bool front)
{
 	_NODE * node;

	if (front == false)
	{
		node = Duplicate(m_pParent, src.m_pNodeConst, false, m_pNode);
	}
	else
	{
		node = Duplicate(m_pParent, src.m_pNodeConst, false, m_pNode->prev);

		if (m_pParent->child == m_pNode)
			m_pParent->child = node;
	}
	return CMiniXML::iterator(node, m_pParent);
}



CMiniXML::iterator	CMiniXML::iterator::InsertChild(const TCHAR * name, bool top)
{
	_NODE * parent = m_pNode;

	if (parent == NULL)
	{
		return CMiniXML::iterator(NULL, NULL);
	}	else
	{
		_NODE * newnode = AllocNode(name);

		if (parent->child == NULL)
		{
			parent->child = newnode;
			newnode->next = newnode->prev = newnode;
		}	else
		{
			newnode->next = parent->child;
			newnode->prev = parent->child->prev;

			parent->child->prev->next = newnode;
			parent->child->prev = newnode;

			if (top == true)
				parent->child = newnode;
		}
		return CMiniXML::iterator(newnode, parent);
	}
}


CMiniXML::iterator	CMiniXML::iterator::InsertChild(const const_iterator & src, bool top)
{
	_ASSERT(m_pNode != src.m_pNodeConst || m_pParent != src.m_pParentConst);
	_NODE * node = Duplicate(m_pNode, src.m_pNodeConst, false, m_pNode->child != NULL ? m_pNode->child->prev : NULL);
	if (top == true)
		m_pNode->child = node;
	return CMiniXML::iterator(node, m_pNode);
}


CMiniXML::iterator CMiniXML::iterator::Erase()
{
	if (m_pNode != NULL)
	{
		_NODE * next = m_pNode->next;

		if (m_pParent->child == m_pNode)
		{
			m_pParent->child = m_pNode->next;
		}

		if (next == m_pNode)
		{
			m_pParent->child = NULL;
			next = NULL;
		}	else
		{
			m_pNode->prev->next = next;
			next->prev = m_pNode->prev;
		}

		m_pNode->next = m_pNode->prev = NULL;
		ReleaseNode(m_pNode);

		return CMiniXML::iterator(next == m_pParent->child ? NULL : next, m_pParent);
	}
	return CMiniXML::iterator(NULL, NULL);
}


bool CMiniXML::const_iterator::operator == (const void * ptr) const
{
	_ASSERT(ptr == NULL);
	return m_pNodeConst == ptr ? true : false;
}


bool CMiniXML::const_iterator::operator != (const void * ptr) const
{
	_ASSERT(ptr == NULL);
	return m_pNodeConst != ptr ? true : false;
}


CMiniXML::const_iterator CMiniXML::const_iterator::operator ++ ()
{
	CMiniXML::const_iterator it(*this);
	m_pNodeConst = m_pNodeConst->next != m_pParentConst->child ? m_pNodeConst->next : NULL;
	m_Accessed = false;
	return it;
}


CMiniXML::const_iterator CMiniXML::const_iterator::operator ++ (int)
{
	m_pNodeConst = m_pNodeConst->next != m_pParentConst->child ? m_pNodeConst->next : NULL;
	m_Accessed = false;
	return * this;
}


CMiniXML::const_iterator CMiniXML::const_iterator::Next() const
{
	return CMiniXML::const_iterator(m_pNodeConst->next != m_pParentConst->child ? m_pNodeConst->next : NULL, m_pParentConst, false);
}


//


CMiniXML::iterator::iterator()
{
	m_pNode		= NULL;
	m_pParent	= NULL;
}


CMiniXML::iterator::iterator(const iterator & it) : const_iterator(it.m_pNode, it.m_pParent, it.m_Accessed)
{
	m_pNode		= it.m_pNode;
	m_pParent	= it.m_pParent;
}


CMiniXML::iterator::iterator(_NODE * node, _NODE * parent, bool accessed) : const_iterator(node, parent, accessed)
{
	m_pNode		= node;
	m_pParent	= parent;
}


#ifdef _DEBUG
CMiniXML::iterator::~iterator()
{
	_ASSERT(m_pNode == m_pNodeConst);
	_ASSERT(m_pParent == m_pParentConst);
}
#endif


void CMiniXML::iterator::SetName(const TCHAR * name)
{
	m_pNode->name = name;
}


void CMiniXML::iterator::SetValue(const TCHAR *value)
{
	if (m_pNode != NULL)
		m_pNode->value = value;
}


void CMiniXML::iterator::SetAttribute(const TCHAR * name, const TCHAR * value)
{
	if (m_pNode != NULL)
	{
		_NODE::_ITEM * item;
		for(item=m_pNode->item; item != NULL; item=item->next)
		{
			if (_tcscmp(item->name.c_str(), name) == 0)
			{
				item->value = value;
				return;
			}
		}
		item = new _NODE::_ITEM;
		item->name = name;
		item->value = value;
		item->next = m_pNode->item;
		m_pNode->item = item;
	}
}


void CMiniXML::iterator::RemoveAttribute(const TCHAR * name)
{
	if (m_pNode != NULL)
	{
		_NODE::_ITEM * item, * prev = NULL;
		for(item=m_pNode->item; item != NULL; item=item->next)
		{
			if (_tcscmp(item->name.c_str(), name) == 0)
			{
				if (prev == NULL)
					m_pNode->item = item->next;
				else
					prev->next = item->next;
				delete item;
				return;
			}
			prev = item;
		}
	}
}


CMiniXML::iterator CMiniXML::iterator::Next() const
{
	return CMiniXML::iterator(m_pNode->next != m_pParent->child ? m_pNode->next : NULL, m_pParent, false);
}


CMiniXML::iterator	CMiniXML::iterator::operator ++ ()
{	// Prefix increment operator
	m_pNodeConst = m_pNode = m_pNode->next != m_pParent->child ? m_pNode->next : NULL;
	m_Accessed	= false;
	return * this;
}


CMiniXML::iterator	CMiniXML::iterator::operator ++ (int)
{	// Postfix increment operator
	CMiniXML::iterator it(*this);
	m_pNodeConst = m_pNode = m_pNode->next != m_pParent->child ? m_pNode->next : NULL;
	m_Accessed = false;
	return it;
}


CMiniXML::iterator CMiniXML::iterator::Find(const TCHAR * name) const
{
	_NODE * node = m_pNode;
	if (node != NULL)
	{
		do
		{
			if ((m_Accessed == false || node != m_pNode) && //	find 해서 찾은 노드라면 자신 노드에서 검색 성공하게 되므로, 자기 자신은 skip
				_tcscmp(node->name.c_str(), name) == 0)
			{
				return CMiniXML::iterator(node, m_pParent, true);
			}
			node = node->next;
		}	while(node != m_pParent->child);
	}
	return CMiniXML::iterator(NULL, NULL);
}


CMiniXML::iterator CMiniXML::iterator::GetChild() const
{
	return CMiniXML::iterator(m_pNode->child, m_pNode);
}


CMiniXML::iterator CMiniXML::iterator::FindChild(const TCHAR * name) const
{
	iterator item = GetChild();
	if (item != NULL)
		return item.Find(name);
	return item;
}


//	XML 비교
//	(저장시 변경된 내요잉 있는 지 검사할 때 유용)

bool CMiniXML::CompareItem(const _NODE * na, const _NODE * nb)
{
	if (_tcscmp(na->name.c_str(), nb->name.c_str()))
		return false;

	if (_tcscmp(na->value.c_str(), nb->value.c_str()))
		return false;

	for(const _NODE::_ITEM * i1=na->item; i1!=NULL; i1=i1->next) //	item 의 순서는 고려 안함
	{
		const _NODE::_ITEM * i2;

		for(i2=nb->item; i2!=NULL; i2=i2->next)
		{
			if (!_tcscmp(i1->name.c_str(), i2->name.c_str()) &&
				!_tcscmp(i1->value.c_str(), i2->value.c_str()))
				break;
		}

		if (i2 == NULL)
			return false;
	}

	return true;
}


bool CMiniXML::CompareNode(const_iterator na, const_iterator nb, bool checkorder)
{
	if (CompareItem(na.m_pNodeConst, nb.m_pNodeConst) == false ||
		CompareItem(nb.m_pNodeConst, na.m_pNodeConst) == false)		// 아이템 개수나 내용 관련해서
		return false;

	if (na.HasChild() == true)
	{
		if (CompareNodeList(na.GetChild(), nb.GetChild(), checkorder) == false)
			return false;
	}
	else if (nb.HasChild() == true)
	{
		return false;
	}

	return true;
}


bool CMiniXML::CompareNodeList(const_iterator na, const_iterator nb, bool checkorder)
{
	const_iterator nc, i1, i2;

	for(i1=na, i2=nb; i1!=NULL && i2!=NULL; i1++, i2++) ;

	if (i1 != NULL || i2 != NULL)									//	갯수부터 틀림
		return false;

	for(; na!=NULL; na++)
	{
		if (nb == NULL)
			return false;

		if (checkorder == true)
		{
			if (CompareNode(na, nb, checkorder) == false)
				return false;
			nb++;
		}
		else
		{
			const_iterator it;
			for(it=nb; it!=NULL; it++)
			{
				if (CompareNode(na, it, checkorder) == true)
					break;
			}
			if (it == NULL)
				return false;
		}
	}
	return true;
}


bool CMiniXML::Compare(const CMiniXML & xml, bool checkorder) const
{
	if (CompareNodeList(GetRoot(), xml.GetRoot(), checkorder) == true)
	{
		if (checkorder == false)
			return CompareNodeList(xml.GetRoot(), GetRoot(), checkorder);
		return true;
	}
	return false;
}

#ifdef _TEST
#include <stdio.h>

void	Read(CMiniXML::const_iterator it, int lv)
{
	int i;

	for(; it!=NULL; it++)
	{
		for(i=0; i<lv; i++)
			_tprintf(_T("  "));
		_tprintf(_T("%s\n"), it.GetName());

		if (it.GetChild() != NULL)
			Read(it.GetChild(), lv+1);
	}
}

void	Read(const CMiniXML & xml)
{
	CMiniXML::const_iterator it = xml.GetRoot();
	_tprintf(_T("--read--\n"));
	Read(it, 0);
}


void	__main()
{
#if 0
	CMiniXML xml;
	bool r = xml.Parse(_T("<test><item1/><item2/><item3/><item4/></test>"));
	if (r == false)
	{
		_tprintf(_T("%s\n"), xml.GetError());
	}
#if 0
	CMiniXML::iterator it = xml.GetRoot();
	_tprintf(_T("%s\n"), it.GetName());

	CMiniXML::const_iterator it2 = it;//xml.GetRoot();
	_tprintf(_T("%s\n"), it2.GetName());
//	it2.SetName(_T("ttt"));
//	_tprintf(_T("%s\n"), it.GetName());

	Read(xml);

	CMiniXML xml2;
	xml2.GetRoot().Insert(xml.GetRoot());
#endif
	CMiniXML xml3;
	CMiniXML::iterator ii = xml3.GetRoot().Insert(_T("root"));
	ii.InsertChild(_T("3"));
	Read(xml3);

	xml3.GetRoot().InsertChild(xml.GetRoot(), false);
	Read(xml3);

	CMiniXML xml4 = xml3;
	Read(xml4);
#else
	CMiniXML xml, origin, v1, v2;
	origin.Parse(_T("<item1/><item2/><item3/>"));
	v1.Parse(_T("<item1/><item3/>"));
	v2.Parse(_T("<item1/><item2/>"));

	xml.Merge(origin, v1, v2);
	Read(xml);
#endif
}

class	STUB
{
public :
	STUB() { __main(); }
} ;
static STUB _stub;
#endif

}