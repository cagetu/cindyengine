// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOSTRINGUTIL_H__
#define __MOSTRINGUTIL_H__

#include <windows.h>

namespace MoCommon
{
	#define MO_MAX_PATH_LENGTH		512
	#define MO_MAX_STRING_LENGTH	1024

	//================================================================
	/** String Utility
	       @author Jaehee
		   @remarks 
				stl 문자열에서 유용하게 쓸 수 있는 기능을 묶어놓은 클래스
				static 함수들로만 구성되어 있슴
	*/
	//================================================================
	class MOCOM_DLL MoStringUtil
	{
	public:
		static void		TrimSpace( MoString& str );
		static void		Trim( MoString& str, const MoString& strDel );

		static void		LowerCase( MoString& str );
		static void		UpperCase( MoString& str );

		static int		WidecharToMultibyte( const wchar* pSrc, char* pOut, unsigned int CodePage = CP_ACP );
		//static void	MultibyteToWideChar( const char* pString, MoString& Output );

		static void		Split( MoStringVector& vecOut, const MoString& str, const MoString& strDels = L" \t\n", ushort usMaxSplit = 0 );
		static void		SplitPath( const MoString& strPath, MoString& strDrive, MoString& strDir, 
									MoString& strFileName, MoString& strExt );
		static void		SplitPathFileName( const MoString& strPath, MoString& strOut );
		static void		SplitPathExt( const MoString& strPath, MoString& strOut );
		static void		SplitPathDir( const MoString& strPath, MoString& strOut );
		static void		SplitPathDrive( const MoString& strPath, MoString& strOut );
		static void		SplitPathFullFileName( const MoString& strPath, MoString& strOut );

		static void		ToString( wchar* pOut, const wchar* pFormat, ... );

		static void		Replace( MoString& Output, const MoString& find_token, const MoString& replace_token );
		static MoString	Squeeze( const MoString& text, char target );
	};
}


#endif // __MOSTRINGUTIL_H__