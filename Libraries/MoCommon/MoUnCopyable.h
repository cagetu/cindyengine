// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOUNCOPYABLE_H__
#define __MOUNCOPYABLE_H__

namespace MoCommon
{
	//================================================================
	/** UnCopyable Object
	       @author cagetu
		   @remarks 
				<복사 방지용 객체>
				복사 방지를 하고 싶은 객체가 private 상속을 받는다.
				ex) class MoResourceManager : private MoUnCopyable

				참고 자료 "Effect C++ 3판"
	*/
	//================================================================	
	class MOCOM_DLL MoUnCopyable
	{
	private:
		MoUnCopyable( const MoUnCopyable& );
		MoUnCopyable& operator =( const MoUnCopyable& );

	public:
		MoUnCopyable()		{}
		~MoUnCopyable()		{}
	};
}

#endif	// __MOUNCOPYABLE_H__