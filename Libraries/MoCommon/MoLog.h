// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOLOG_H__
#define __MOLOG_H__

#include "MoConsole.h"
#include "Thread/MoCriticalSection.h"

namespace MoCommon
{
	//================================================================
	/** Log
	       @author cagetu
		   @remarks 
				Log에 관련된 클래스

			log[명사] : <record, diary> 기록, 일지
			1. Keep a log 일지를 적다
	*/
	//================================================================	
	class MOCOM_DLL MoLog
	{
	public:
		MoLog( const MoString& strName, const MoString& strOutputPath = L"", bool bOutputDebug = false, bool bOutputConsole = false, bool bOpen = true );
		virtual ~MoLog();

		bool	Open();
		void	Close();

		void	OutputDebug( bool bEnable );

		virtual void	WriteString( const MoString& strMsg );
		virtual void	WriteError( const wchar* strFileName, int nLine, const MoString& strMsg );

	private:
		MoCriticalSection	m_Cs;

	protected:
		FILE*		m_pFile;
		
		MoString	m_strFileName;
		MoString	m_strOutputPath;

		bool		m_bOutputDebug;
		bool		m_bOutputConsole;
		MoConsole	m_OutputConsole;

		int			m_nIndentation;

		void			Begin();
		void			End();

		void			PutTime();
		void			PutDate();
		void			PutLine( int nNumLines = 1 );
		void			PutTab( int nNumTabs = 1 );
		void			PutString( const MoString& strMsg );

		virtual void	PutHeader();
		virtual void	PutFooter();
	};
}

#endif	// __MOLOG_H__