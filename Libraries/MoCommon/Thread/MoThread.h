// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOTHREAD_H__
#define __MOTHREAD_H__

#include <windows.h>

namespace MoCommon
{
	//================================================================
	/** Thread
	       @author cagetu
		   @remarks 
				Thread를 생성/사용/해제 
	*/
	//================================================================	
	class MOCOM_DLL MoThread
	{
	public:
		enum STATUS
		{
			STATUS_NONE = 0,			//!< 아무 상태도 아님
			STATUS_PROCESS,				//!< 쓰레드 작업 중
			STATUS_STOP,				//!< 쓰레드 멈춤
			STATUS_EXIT					//!< 쓰레드 종료
		};

	private:
		MoString				m_strName;			//!< 쓰레드 이름

		HANDLE					m_hThread;			//!< 쓰레드 핸들
		HANDLE					m_hWakeEvent;		//!< 쓰레드 작업 이벤트

		ulong					m_ulThreadID;		//!< 쓰레드 ID

		bool					m_bTerminated;		//!< 쓰레드 종료 
		STATUS					m_eStatus;			//!< 쓰레드 상태

		LPTHREAD_START_ROUTINE	m_lpThreadRotine;
		LPVOID					m_lpParameter;

		static ulong __stdcall		DefaultThreadFunc( LPVOID lpContext );

	protected:
		virtual ulong		Execute();
		virtual ulong		Stop( ulong ulWaitTime = INFINITE );

	public:
		MoThread( const MoString& strName );
		virtual ~MoThread();

		bool				CreateThread( LPTHREAD_START_ROUTINE lpStartRoutine, LPVOID lpParam, bool bSuspend = false );
		bool				CreateThread( bool bSuspend = false );

		const MoString&		GetName() const;
		ulong				GetID() const;
		STATUS				GetStatus() const;

		void				SetPriority( ulong ulPriority );
		ulong				GetPriority() const;

		virtual void		Terminate();
		bool				IsTerminated() const;

		bool				Play();
		bool				Resume();
		bool				Suspend();

		ulong				WaitForExit( ulong ulWaitTime = INFINITE );
	};

	#include "MoThread.inl"
}

#endif	// __MOTHREAD_H__