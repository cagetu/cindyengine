//================================================================
// File:               : MoMutex.inl
// Related Header File : MoMutex.h
// Original Author     : changhee
// Creation Date       : 2007. 3. 5
//================================================================
//----------------------------------------------------------------
inline
MoMutex::MoMutex()
{
#ifndef _NO_THREADS_
	m_hMutex = CreateMutex( NULL, FALSE, NULL );
#endif
}

//----------------------------------------------------------------
inline 
MoMutex::~MoMutex()
{
#ifndef _NO_THREADS_
	CloseHandle( m_hMutex );
#endif
	m_hMutex = NULL;
}

//----------------------------------------------------------------
inline
void MoMutex::Lock() const
{
#ifndef _NO_THREADS_
	WaitForSingleObject( m_hMutex, INFINITE );
#endif
}

//----------------------------------------------------------------
inline
void MoMutex::Unlock() const
{
#ifndef _NO_THREADS_
	ReleaseMutex( m_hMutex );
#endif
}