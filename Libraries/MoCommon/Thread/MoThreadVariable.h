//================================================================
// File:           : MOTheadVariable.h
// Original Author : changhee
// Creation Date   : 2007. 3. 5
//================================================================
#pragma once

#include "MoMutex.h"

namespace MoCommon
{
	//================================================================
	/** ThreadVariable
	    @author    changhee
		@since     2007. 3. 5
		@remarks   Thread Safe 한 변수를 만들기 위함
				   (Nebula2 참고)
	*/
	//================================================================
	template<class TYPE>
	class MoThreadVariable
	{
	private:
		MoMutex		m_Mutex;
		TYPE		m_Variable;

	public:
		MoThreadVariable();
		MoThreadVariable( const MoThreadVariable& src );
		MoThreadVariable( TYPE val );

		void operator =( const MoThreadVariable& rhs );
		void operator =( TYPE rhs );

		void Set( TYPE val );
		TYPE Get() const;
	};

	template<class TYPE>
	MoThreadVariable<TYPE>::MoThreadVariable()
	{
	}

	template<class TYPE>
	MoThreadVariable<TYPE>::MoThreadVariable( const MoThreadVariable& src )
	{
		this->Set( src.Get() );
	}

	template<class TYPE>
	MoThreadVariable<TYPE>::MoThreadVariable( TYPE val )
	{
		this->Set( val );
	}

	template<class TYPE>
	void MoThreadVariable<TYPE>::operator =( const MoThreadVariable& rhs )
	{
		Set( rhs.Get() );
	}

	template<class TYPE>
	void MoThreadVariable<TYPE>::operator =( TYPE rhs )
	{
		this->Set( rhs );
	}

	template<class TYPE>
	void MoThreadVariable<TYPE>::Set( TYPE val )
	{
		MoMutexLock lock( &m_Mutex );
		m_Variable = val;
	}

	template<class TYPE>
	TYPE MoThreadVariable<TYPE>::Get() const
	{
//		MoMutexLock lock( &m_Mutex );
//		return m_Variable;
		m_Mutex.Lock();
		TYPE value = m_Variable;
		m_Mutex.Unlock();
		return value;
	}
}