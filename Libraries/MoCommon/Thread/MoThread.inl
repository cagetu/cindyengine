// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com


//--------------------------------------------------------------
inline
bool MoThread::IsTerminated() const
{
	return m_bTerminated;
}

//--------------------------------------------------------------
inline
const MoString& MoThread::GetName() const
{
	return m_strName;
}

//--------------------------------------------------------------
inline
ulong MoThread::GetID() const
{
	return m_ulThreadID;
}

//--------------------------------------------------------------
inline
MoThread::STATUS MoThread::GetStatus() const
{
	return m_eStatus;
}