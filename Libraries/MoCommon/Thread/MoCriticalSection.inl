// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

//================================================================
//	class Thread Sync
//================================================================
//----------------------------------------------------------------
//template <class TYPE>
//inline
//MoThreadSync<TYPE>::MoThreadSync()
//{
//	InitializeCriticalSection( &sync_ );
//}
//template <class TYPE>
//inline
//MoThreadSync<TYPE>::~MoThreadSync()
//{
//	DeleteCriticalSection( &sync_ );
//}
//
////----------------------------------------------------------------
//template <class TYPE>
//inline
//MoThreadSync<TYPE>::Lock::Lock( MoThreadSync& host )
//: host_( host )
//{
//	::EnterCriticalSection( &host_.sync_ );
//}
//template <class TYPE>
//inline
//MoThreadSync<TYPE>::Lock::~Lock()
//{
//	::LeaveCriticalSection( &host_.sync_ );
//}

//================================================================
//	class Critical Section
//================================================================
inline
MoCriticalSection::MoCriticalSection()
{
	InitializeCriticalSection( &m_Cs );
}
inline
MoCriticalSection::~MoCriticalSection()
{
	DeleteCriticalSection( &m_Cs );
}

inline
void MoCriticalSection::Enter()
{
	EnterCriticalSection( &m_Cs );
}
inline
void MoCriticalSection::Leave()
{
	LeaveCriticalSection( &m_Cs );
}

//================================================================
//	class Thread Safe
//================================================================
inline
MoCriticalSection::ThreadSafe::ThreadSafe( MoCriticalSection& rCS )
: cs( rCS )
{
	cs.Enter();
}
inline
MoCriticalSection::ThreadSafe::~ThreadSafe()
{
	cs.Leave();
}
