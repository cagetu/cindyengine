// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com
#pragma once

#include <windows.h>
#include "../MoCommon.h"

namespace MoCommon
{
	//================================================================
	/** Thread Syncronization
	       @author cagetu
		   @remarks 
				MultiThread's syncronization - critical_section( by Win32 )
				cf) "Modern C++ Design" book's Loki library - thread
	*/
	//================================================================
	//template <class TYPE>
	//class MOCOM_DLL MoThreadSync
	//{
	//	mutable CRITICAL_SECTION	sync_;
	//public:
	//	MoThreadSync();
	//	~MoThreadSync();

	//	class Lock;
	//	friend class Lock;

	//	class Lock
	//	{
	//		MoThreadSync & host_;

	//		Lock( const Lock& );
	//		Lock& operator=( const Lock& );
	//	public:
	//		explicit Lock( MoThreadSync& host );
	//		~Lock();
	//	};

 //       typedef volatile TYPE VolatileType;

 //       typedef LONG IntType; 

 //       static IntType AtomicIncrement(volatile IntType& lval)
 //       { return InterlockedIncrement(&const_cast<IntType&>(lval)); }
 //       
 //       static IntType AtomicDecrement(volatile IntType& lval)
 //       { return InterlockedDecrement(&const_cast<IntType&>(lval)); }
 //       
 //       static void AtomicAssign(volatile IntType& lval, IntType val)
 //       { InterlockedExchange(&const_cast<IntType&>(lval), val); }
 //       
 //       static void AtomicAssign(IntType& lval, volatile IntType& val)
 //       { InterlockedExchange(&lval, val); }
	//};

	//================================================================
	/** Thread Syncronization
	       @author cagetu
		   @remarks 
				MultiThread's syncronization - critical_section( by Win32 )
				
				간편하게, Critical Section을 받아서 사용
	*/
	//================================================================
	class MOCOM_DLL MoCriticalSection
	{
	private:
		CRITICAL_SECTION		m_Cs;

	public:
		class ThreadSafe
		{
			MoCriticalSection&		cs;
		public:
			explicit ThreadSafe( MoCriticalSection& rCS );
			~ThreadSafe();
		};

	public:
		MoCriticalSection();
		~MoCriticalSection();

		void	Enter();
		void	Leave();
	};

	#include "MoCriticalSection.inl"
}