// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "../MoCommon.h"
#include "MoThread.h"

namespace MoCommon
{
	//--------------------------------------------------------------
	MoThread::MoThread( const MoString& strName )
		: m_strName( strName )
		, m_hThread( 0 )
		, m_hWakeEvent( 0 )
		, m_ulThreadID( 0xffff )
		, m_bTerminated( false )
		, m_eStatus( STATUS_NONE )
	{
	}

	//--------------------------------------------------------------
	MoThread::~MoThread()
	{
		CloseHandle( m_hWakeEvent );
		CloseHandle( m_hThread );
	}

	//--------------------------------------------------------------
	bool MoThread::CreateThread( bool bSuspend )
	{
		return CreateThread( NULL, NULL, bSuspend );
	}

	//--------------------------------------------------------------
	bool MoThread::CreateThread( LPTHREAD_START_ROUTINE lpStartRoutine, LPVOID lpParam, bool bSuspend )
	{
		m_hWakeEvent = ::CreateEvent( NULL, FALSE, FALSE, NULL );
		m_lpThreadRotine = lpStartRoutine;
		m_lpParameter = lpParam;

		m_hThread = ::CreateThread( NULL, 0, DefaultThreadFunc, this, CREATE_SUSPENDED, &m_ulThreadID );
		if( NULL == m_hThread )
		{
			m_bTerminated = true;
			m_eStatus = STATUS_EXIT;
			return false;
		}

		m_eStatus = STATUS_STOP;
		m_bTerminated = false;

		if( !bSuspend )
			Resume();

		return true;
	}

	//--------------------------------------------------------------
	void MoThread::SetPriority( ulong ulPriority )
	{
		SetThreadPriority( m_hThread, ulPriority );
	}

	//--------------------------------------------------------------
	ulong MoThread::GetPriority() const
	{
		return GetThreadPriority( m_hThread );
	}

	//--------------------------------------------------------------
	void MoThread::Terminate()
	{
		m_bTerminated = true;
	}

	//--------------------------------------------------------------
	ulong MoThread::Execute()
	{
		return 0;
	}

	//--------------------------------------------------------------
	bool MoThread::Play()
	{
		return ( 0 != SetEvent( m_hWakeEvent ) );
	}

	//--------------------------------------------------------------
	ulong MoThread::Stop( ulong ulWaitTime )
	{
		m_eStatus = STATUS_STOP;
		ulong result = WaitForSingleObject( m_hWakeEvent ,ulWaitTime );
		m_eStatus = STATUS_PROCESS;

		return result;
	}

	//--------------------------------------------------------------
	bool MoThread::Resume()
	{
		return ResumeThread( m_hThread ) !=  (DWORD)-1;
	}

	//--------------------------------------------------------------
	bool MoThread::Suspend()
	{
		return SuspendThread( m_hThread ) !=  (DWORD)-1;
	}

	//--------------------------------------------------------------
	ulong MoThread::WaitForExit( ulong ulWaitTime )
	{
		return WaitForSingleObject( m_hThread , ulWaitTime );
	}

	//============================================================================
	ulong __stdcall MoThread::DefaultThreadFunc( LPVOID lpContext )
	{
		MoThread *pThread = (MoThread*)lpContext;
		if( NULL == pThread )
			return 0xffffffff;	

		if( pThread->m_lpThreadRotine )
			pThread->m_lpThreadRotine( pThread->m_lpParameter );
		else
			pThread->Execute();

		pThread->m_eStatus = STATUS_EXIT;
		pThread->m_bTerminated = false;

		return 0;
	}
}