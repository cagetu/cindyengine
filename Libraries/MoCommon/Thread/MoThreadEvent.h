// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#pragma once

#include <windows.h>

namespace MoCommon
{
	//==================================================================
	/** Thread Event
		@author			cagetu
		@remarks
			Win32 Event class
	*/
	//==================================================================
	class MOCOM_DLL MoThreadEvent
	{
	public:
		enum TYPE
		{
			EVENT_SIGNAL = 0,
			EVENT_NONSIGNAL,
		};
	protected:
		HANDLE	m_hEvent;	//! Event Handle
		TYPE	m_eType;

	public:
		MoThreadEvent();
		~MoThreadEvent();

		/** Non-Signal
			@remarks
				Set Non-Signal Status
		*/
		void				NonSignal();

		/** Signal
			@remarks
				Set Signal Status
		*/
		void				Signal();

		// GetHandle
		const HANDLE&		GetHandle() const		{	return m_hEvent;	}

		// GetType
		TYPE				GetType() const			{	return m_eType;		}
	};
}