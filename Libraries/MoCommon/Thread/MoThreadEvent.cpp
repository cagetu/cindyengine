// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com
#include "../MoCommon.h"
#include "MoThreadEvent.h"

namespace MoCommon
{
	//------------------------------------------------------------------
	MoThreadEvent::MoThreadEvent()
	{
		m_hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	}

	//------------------------------------------------------------------
	MoThreadEvent::~MoThreadEvent()
	{
		CloseHandle( m_hEvent );
	}

	//------------------------------------------------------------------
	void MoThreadEvent::NonSignal()
	{
#ifdef _DEBUG
		OutputDebugString( L"[MoThreadEvent::NonSignal] Send 'Stop' Signal" );
#endif

		ResetEvent( m_hEvent );

		m_eType = EVENT_NONSIGNAL;
	}

	//------------------------------------------------------------------
	void MoThreadEvent::Signal()
	{
		SetEvent( m_hEvent );

#ifdef _DEBUG
		OutputDebugString( L"[MoThreadEvent::NonSignal] Send 'Play' Signal" );
#endif

		m_eType = EVENT_SIGNAL;
	}
}
