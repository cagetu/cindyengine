// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com
#pragma once

#include "../MoCommon.h"
#include <windows.h>

namespace MoCommon
{
	//==================================================================
	/** Mutex Class
		@author
			cagetu
		@since
			2007년 3월 5일
		@remarks
			Mutex를 이용한 동기화 클래스

			"http://www.flipcode.com/articles/article_multithreading02.shtml"
			Nebula2::nMutex
	*/
	//==================================================================
	class MoMutex
	{
	private:
#ifndef _NO_THREADS_
		HANDLE			m_hMutex;			//!< Mutex Handle
#endif

	public:
		MoMutex();
		~MoMutex();

		void		Lock() const;		//!< Lock
		void		Unlock() const;		//!< Release
	};

	//==================================================================
	/** Mutex Lock Class
		@author
			cagetu
		@since
			2007년 3월 5일
		@remarks
			함수 Block에서 작동하는 mutex를 만드는 객체
	*/
	//==================================================================
	class MoMutexLock
	{
	private:
		MoMutex*		m_pMutex;
	public:
		MoMutexLock( MoMutex* mutex ) : m_pMutex( mutex )
		{
			m_pMutex->Lock();
		}
		~MoMutexLock()
		{
			m_pMutex->Unlock();
		}
	};

#include "MoMutex.inl"
}
