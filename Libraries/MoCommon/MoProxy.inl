// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

//----------------------------------------------------------------
template <class T>
inline
MoProxy<T>::MoProxy()
: m_pObject(0)
{
}
//----------------------------------------------------------------
template <class T>
inline
MoProxy<T>::MoProxy( T* pObject )
: m_pObject( pObject )
{
}

//----------------------------------------------------------------
template <class T>
inline
MoProxy<T>::MoProxy( const MoProxy& Ptr )
: m_pObject( Ptr.m_pObject )
{
}

//----------------------------------------------------------------
template <class T>
inline
bool MoProxy<T>::IsNull() const
{
	return (0 == m_pObject);
}

//----------------------------------------------------------------
template <class T>
inline
MoProxy<T>& MoProxy<T>::operator= ( const MoProxy& Ptr )
{
	if (m_pObject != Ptr.m_pObject)
		m_pObject = Ptr.m_pObject;

	return *this;
}

//----------------------------------------------------------------
template <class T>
inline
T* MoProxy<T>::operator-> () const
{
	assert( m_pObject );
	return m_pObject;
}
//----------------------------------------------------------------
template <class T>
inline
T* MoProxy<T>::GetPtr() const
{
	assert( m_pObject );
	return m_pObject;
}