// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoCommon.h"
#include "MoLog.h"

//#ifdef _WIN32
//	#include <wchar.h>
//#endif

namespace MoCommon
{
	//----------------------------------------------------------------
	MoLog::MoLog( const MoString& strName, const MoString& strOutputPath, bool bOutputDebug, bool bOutputConsole, bool bOpen )
		: m_pFile(0)
		, m_nIndentation(0)
		, m_strFileName(strName)
		, m_strOutputPath(strOutputPath)
		, m_bOutputConsole(bOutputConsole)
		, m_bOutputDebug(bOutputDebug)
	{
		if (bOpen)
		{
			Open();
		}
	}
	MoLog::~MoLog()
	{
		Close();
	}

	//----------------------------------------------------------------
	void MoLog::OutputDebug( bool bEnable )
	{
		m_bOutputDebug = bEnable;
	}

	//----------------------------------------------------------------
	void MoLog::PutString( const MoString& strMsg )
	{
		MoCriticalSection::ThreadSafe threadsafe( m_Cs );

		int	pos = ftell( m_pFile );
		if (pos < 0)
			return;

		fwrite( strMsg.c_str(), sizeof(wchar), strMsg.size(), m_pFile );
		fflush( m_pFile );

#if defined _WIN32 & defined _DEBUG
		if (m_bOutputDebug)
			OutputDebugString( strMsg.c_str() );
#endif

		if (m_bOutputConsole)
			m_OutputConsole.Print( strMsg );
	}

	//----------------------------------------------------------------
	void MoLog::PutLine( int nNumLines )
	{
		MoCriticalSection::ThreadSafe threadsafe( m_Cs );

		for (int i=0; i<nNumLines; ++i)
			PutString( L"\r\n" );
	}

	//----------------------------------------------------------------
	void MoLog::PutTab( int nNumTabs )
	{
		MoCriticalSection::ThreadSafe threadsafe( m_Cs );

		for (int i=0; i<nNumTabs; ++i)
			PutString( L"\t" );
	}

#pragma warning(disable : 4996)
	//----------------------------------------------------------------
	void MoLog::PutDate()
	{
		MoCriticalSection::ThreadSafe threadsafe( m_Cs );

		static wchar buffer[256];
		_wstrtime( buffer );

		PutTab();
		PutString( buffer );
	}

	//----------------------------------------------------------------
	void MoLog::PutTime()
	{
		MoCriticalSection::ThreadSafe threadsafe( m_Cs );

		static wchar buffer[256];
		_wstrdate( buffer );

		PutTab();
		PutString( buffer );
	}

	//----------------------------------------------------------------
	void MoLog::Begin()
	{
		m_nIndentation++;
	}
	void MoLog::End()
	{
		if (--m_nIndentation < 0)
			m_nIndentation = 0;
	}

	//----------------------------------------------------------------
	bool MoLog::Open()
	{
		if (m_pFile)
			return false;

		MoString fullname = m_strOutputPath + m_strFileName;
		m_pFile = _wfopen( fullname.c_str(), L"wb" );
		assert( m_pFile );

		if (!m_pFile)
			return false;

		unsigned short unicode = 0xFEFF;
		fwrite( &unicode, sizeof(unsigned short), 1, m_pFile );

		if (m_bOutputConsole)
			m_OutputConsole.Create();

		PutHeader();
		return true;
	}
	void MoLog::Close()
	{
		PutFooter();

		if (m_pFile)
			fclose( m_pFile );
		m_pFile = NULL;
	}

	//----------------------------------------------------------------
	void MoLog::PutHeader()
	{
		PutLine( 2 );
		PutString( L"::::Begin Log [" + m_strFileName + L"]::::" );
		PutLine();
		PutDate();
		PutTime();
		PutLine();
	}
	void MoLog::PutFooter()
	{
		m_nIndentation = 0;

		PutLine( 2 );
		PutString( L"::::End Log [" + m_strFileName + L"]::::" );
		PutLine();
		PutDate();
		PutTime();
		PutLine( 2 );
	}

	//----------------------------------------------------------------
	void MoLog::WriteString( const MoString& strMsg )
	{
		PutTab( m_nIndentation );
		PutString( strMsg );
		PutLine();
	}

	//----------------------------------------------------------------
	void MoLog::WriteError( const wchar* strFileName, int nLine, const MoString& strMsg )
	{
		PutLine();
		PutString( L"----------------------[Error]----------------------" );
		PutLine();
		wchar strTmp[2048];
		swprintf( strTmp, L"File: %s, Line: %d", strFileName, nLine );
		PutString( strTmp );
		PutLine();
		swprintf( strTmp, L"Desc: %s", strMsg.c_str() );
		PutString( strTmp );
		PutLine();
		PutString( L"---------------------------------------------------" );
		PutLine();
	}
}