// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoCommon.h"
#include "MoTimer.h"

namespace MoCommon
{
	MoTimer::MoTimer()
	{
		m_bStopped			= true;

		m_ulBaseTime		= 0;
		m_ulLastElapsedTime	= 0;
		m_ulStopTime		= 0;
	}

	MoTimer::~MoTimer()
	{
	
	}

	void MoTimer::ResetTimer()
	{
		m_ulBaseTime		= (ulong)clock();		
		m_ulLastElapsedTime	= m_ulBaseTime;
		m_ulStopTime		= 0;
		m_bStopped			= true;
	}

	void MoTimer::StartTimer()
	{
		ulong cur = (ulong)clock();
		
		if( m_bStopped )
		{
			m_ulBaseTime += ( cur - m_ulStopTime );
		}

		m_ulStopTime = 0;
		m_ulLastElapsedTime = cur;
		m_bStopped = false;
	}

	void MoTimer::StopTimer()
	{
		if( !m_bStopped )
		{
			ulong cur;
			if( m_ulStopTime == 0 )
				cur = (ulong)clock();				
			else
				cur = m_ulStopTime;

			m_ulStopTime = cur;
			m_ulLastElapsedTime = cur;
			m_bStopped = true;
		}
	}

	void MoTimer::Advance()
	{
		// fix me
		m_ulStopTime += ( CLOCKS_PER_SEC / 10 );
	}

	float MoTimer::GetAbsoluteTime()
	{
		ulong cur;

		if( 0 == m_ulStopTime )
		{
			cur = (ulong)clock();
		}
		else
		{
			cur = m_ulStopTime;
		}

		float time = (float)cur / (float)( CLOCKS_PER_SEC ); 

		return time;
	}

	float MoTimer::GetAppTime()
	{
		ulong cur;

		if( 0 == m_ulStopTime )
		{
			cur = (ulong)clock();			
		}
		else
		{
			cur = m_ulStopTime;
		}

		float time = (float)( cur - m_ulBaseTime ) / (float)( CLOCKS_PER_SEC );

		return time;
	}

	float MoTimer::GetElapsedTime()
	{
		ulong cur;

		if( 0 == m_ulStopTime )
		{
			cur = (ulong)clock();			
		}
		else
		{
			cur = m_ulStopTime;
		}

		float time = (float)( cur - m_ulLastElapsedTime ) / (float)( CLOCKS_PER_SEC );
		m_ulLastElapsedTime = cur;

		return time;
	}

}