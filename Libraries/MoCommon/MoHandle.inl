// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

//----------------------------------------------------------------
//	HandleL Class
//----------------------------------------------------------------
// cons / dest
inline
MoHandleL::MoHandleL()
: value( INVALID_LHANDLE )
{
}

inline
MoHandleL::MoHandleL( const MoHandleL& p_rLHandle )
: value( p_rLHandle.value )
{
}

inline
MoHandleL::MoHandleL( LHANDLE p_value )
: value( p_value )
{
}

inline
MoHandleL::MoHandleL( unsigned short p_nType, unsigned short p_nIndex )
: type( p_nType ), index( p_nIndex )
{
}

inline
MoHandleL::~MoHandleL()
{
}

//================================================================
/** Invalid
	@remarks	초기화한다.
	@param		none
	@return		none
*/
//================================================================
inline
void	MoHandleL::Invalid()
{
	value = INVALID_LHANDLE;
}

//================================================================
/** LHANDLE
	@remarks	현재 핸들값을 반환한다.
	@return		value
*/
//================================================================
inline
MoHandleL::operator LHANDLE() const
{
	return value;
}

//================================================================
/** operator =
	@remarks	복사 연산자 재정의
	@param		p_LHANDLE : 복사할 핸들객체
	@return		this
*/
//================================================================
inline
MoHandleL&	MoHandleL::operator = ( const MoHandleL& p_rLHandle )
{
	value = p_rLHandle.value;
	return *this;
};

//================================================================
/** operator <
	@remarks	핸들의 크기를 비교한다.
	@param		p_LHANDLE : 비교할 대상 핸들 객체
	@return		true/false : 크기 여부 
*/
//================================================================
inline
bool MoHandleL::operator < ( const MoHandleL& p_rLHandle )
{
	return value < p_rLHandle.value;
}

//----------------------------------------------------------------
//	HandleW Class
//----------------------------------------------------------------
// cons / dest
inline
MoHandleW::MoHandleW()
: value( INVALID_WHANDLE )
{
}

inline
MoHandleW::MoHandleW( const MoHandleW& p_rWHandle )
: value( p_rWHandle.value )
{
}

inline
MoHandleW::MoHandleW( WHANDLE p_value )
: value( p_value )
{
}

inline
MoHandleW::MoHandleW( unsigned char p_nType, unsigned char p_nIndex )
: type( p_nType ), index( p_nIndex )
{
}

inline
MoHandleW::~MoHandleW()
{
}

//================================================================
/** Invalid
	@remarks	초기화한다.
	@param		none
	@return		none
*/
//================================================================
inline
void	MoHandleW::Invalid()
{
	value = INVALID_WHANDLE;
}

//================================================================
/** LHANDLE
	@remarks	현재 핸들값을 반환한다.
	@return		value
*/
//================================================================
inline
MoHandleW::operator WHANDLE() const
{
	return value;
}

//================================================================
/** operator =
	@remarks	복사 연산자 재정의
	@param		p_LHANDLE : 복사할 핸들객체
	@return		this
*/
//================================================================
inline
MoHandleW&	MoHandleW::operator = ( const MoHandleW& p_rWHandle )
{
	value = p_rWHandle.value;
	return *this;
};

//================================================================
/** operator <
	@remarks	핸들의 크기를 비교한다.
	@param		p_LHANDLE : 비교할 대상 핸들 객체
	@return		true/false : 크기 여부 
*/
//================================================================
inline
bool MoHandleW::operator < ( const MoHandleW& p_rWHandle )
{
	return value < p_rWHandle.value;
}