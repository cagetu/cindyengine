// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOCOLSOLE_H__
#define __MOCOLSOLE_H__

#include <windows.h>
#include "MoCommon.h"

namespace MoCommon
{
	//================================================================
	/** Console Class
	       @author cagetu
		   @remarks 
				Console로 출력을 해준다
				참고자료 "http://luna.sumomo.ne.jp/" luna library
	*/
	//================================================================	
	class MOCOM_DLL MoConsole
	{
	public:
		MoConsole();
		~MoConsole();

		bool	Create();
		void	Destroy();
		void	Clear();

		void	Locate( int nX, int nY );

		void	Print( const MoString& strMsg );
		void	Print( int nX, int nY, const MoString& strMsg );

		void	SetColor( ushort usAttributes );
		ushort	GetColor();

		void	EnableCursor( bool bOn );

	protected:
		bool		m_bIsCreated;
		HANDLE		m_hHandle;
	};
}

#endif	// __MOCOLSOLE_H__