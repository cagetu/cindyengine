// Copyright (c) 2008~. cagetu
//
//******************************************************************
#pragma once

#include "../Cagetu.h"

namespace Foundation
{
	class Rtti;
	class Object;

	//==================================================================
	/** Factory
		@author		cagetu
		@remarks	��ü ������ ��ü
	*/
	//==================================================================
	class Factory
	{
	public:
		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		Object*	Create( const char* Class ) const;

		void	Register( const Rtti* RTTI, const char* Class );
		bool	Has( const char* Class ) const;

		static Factory*	Instance();
	protected:
		Factory();
		~Factory();

	private:
		typedef HashMap<std::string, const Rtti*>	Table;
		
		Table	m_Table;
	};

	// factory
	template <class T>	T* CreateObject();
}

#include "Factory.inl"