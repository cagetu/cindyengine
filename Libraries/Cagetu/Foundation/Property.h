#pragma once

#include "Property/PropertyTable.h"

namespace Foundation
{
}

//-----------------------------------------------------------------------------
//	Property 매크로 정의부
//-----------------------------------------------------------------------------
template<class T> T*	GetProperty( const char* Name, Foundation::Object* pObject );

template<class T>
T* GetProperty( Foundation::Object* pObject )
{
	Foundation::AbstractProperty* prop = pObject->GetProperty(Name);
	if (prop)
	{
		return (T*)(prop->GetValue(pObject));
	}
	return NULL;
}

