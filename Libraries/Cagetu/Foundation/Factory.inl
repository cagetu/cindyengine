namespace Foundation
{
	//-----------------------------------------------------------------------------
	template <class T>
	T* CreateObject()
	{
		//T* object = static_cast<T*>( Foundation::Factory::Instance()->Create( T::RTTI.GetName().c_str() ) );
		T* object = static_cast<T*>( T::RTTI.Create() );
		assert( object );
		return object;
	}
}

//-----------------------------------------------------------------------------
//	Factory 매크로 정의부
//-----------------------------------------------------------------------------
#define DECLARE_FACTORY(classname) \
public: \
	static Foundation::Object*	FactoryCreator();	\
	static classname*			Create(); \
	static bool					RegisterFactoryFunction();	\
private:

//-----------------------------------------------------------------------------
//	Factory 매크로 구현부
//-----------------------------------------------------------------------------
#define IMPLEMENT_FACTORY(classname) \
	classname* classname::Create() \
	{ \
		return (classname*)FactoryCreator(); \
	} \
	Foundation::Object* classname::FactoryCreator() \
	{ \
		classname* result = new classname(); \
		return result; \
	} \
	bool classname::RegisterFactoryFunction() \
	{ \
		if (!Foundation::Factory::Instance()->Has(classname::RTTI.GetName().c_str()) )\
		{ \
			Foundation::Factory::Instance()->Register( &classname::RTTI, classname::RTTI.GetName().c_str() ); \
		} \
		return true; \
	} \
	static bool factoryRegistered_##classname = classname::RegisterFactoryFunction();

//-----------------------------------------------------------------------------
//	Factory 매크로 Factory 등록
//-----------------------------------------------------------------------------
//#define REGISTER_FACTORY(classname) \
//	static bool factoryRegistered_##classname = classname::RegisterFactoryFunction();
