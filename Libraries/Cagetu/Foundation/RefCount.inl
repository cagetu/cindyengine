// Copyright (c) 2008~. cagetu
//
//******************************************************************
namespace Foundation
{
	//--------------------------------------------------------------
	inline
	RefCounter::RefCounter()
		: m_RefCount(0)
	{
	}
	inline
	RefCounter::~RefCounter()
	{
		assert( 0 == m_RefCount );
	}

	//------------------------------------------------------------------
	// RefenceCount
	//------------------------------------------------------------------
	inline
	void RefCounter::AddRef()
	{
		++m_RefCount;
	}

	//--------------------------------------------------------------
	inline
	bool RefCounter::Release()
	{
		assert( m_RefCount > 0 );
		if( --m_RefCount == 0 )
		{
			delete this;
			return true;
		}
		return false;
	}

	//------------------------------------------------------------------
	inline
	int RefCounter::GetRefCount() const
	{
		return m_RefCount;
	}
}