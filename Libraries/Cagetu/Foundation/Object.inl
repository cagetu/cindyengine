// Copyright (c) 2008~. cagetu
//
//******************************************************************
namespace Foundation
{
	//-----------------------------------------------------------------------------
	inline
	bool Object::IsInstanceOf( const Rtti& Class ) const
	{
		return this->GetRtti()->IsInstanceOf( Class );
	}

	inline
	bool Object::IsInstanceOf( const std::string& Class ) const
	{
		return this->GetRtti()->IsInstanceOf( Class );
	}

	//-----------------------------------------------------------------------------
	inline
	bool Object::IsA( const Rtti& Class ) const
	{
		return this->GetRtti()->IsA( Class );
	}

	inline
	bool Object::IsA( const std::string& Class ) const
	{
		return this->GetRtti()->IsA( Class );
	}
}

//-----------------------------------------------------------------------------
template <class T>
T* StaticCast( Foundation::Object* pObject )
{
	return (T*)pObject;
}
template <class T>
const T* StaticCast( const Foundation::Object* pObject )
{
	return (const T*)pObject;
}
//-----------------------------------------------------------------------------
template <class T>
T* DynamicCast( Foundation::Object* pObject )
{
	return pObject && pObject->IsA(T::RTTI) ? (T*)pObject : 0;
}
template <class T>
const T* DynamicCast( const Foundation::Object* pObject )
{
	return pObject && pObject->IsA(T::RTTI) ? (const T*)pObject : 0;
}