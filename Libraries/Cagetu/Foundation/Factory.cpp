// Copyright (c) 2008~. cagetu
//
//******************************************************************
#include "Factory.h"
#include "Object.h"

namespace Foundation
{
	Factory* Factory::Instance()
	{
		static Factory instance;
		return &instance;
	}

	// Const/Dest
	Factory::Factory()
	{
	}
	Factory::~Factory()
	{
		m_Table.clear();
	}

	//-------------------------------------------------------------------------
	void Factory::Register( const Rtti* RTTI, const char* Class )
	{
		m_Table.insert( Table::value_type( Class, RTTI ) );
		//m_Table[Class] = newCreator;
	}

	//-------------------------------------------------------------------------
	bool Factory::Has( const char* Class ) const
	{
		Table::const_iterator it = m_Table.find( Class );
		if (it == m_Table.end())
			return false;

		return true;
	}

	//-------------------------------------------------------------------------
	Object* Factory::Create( const char* Class ) const
	{
		Table::const_iterator it = m_Table.find( Class );
		if (it == m_Table.end())
			return NULL;

		return it->second->Create();
	}
}