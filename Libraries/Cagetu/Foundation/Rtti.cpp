// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "Rtti.h"

namespace Foundation
{
	//--------------------------------------------------------------
	Rtti::Rtti( const char* lpszName, const Rtti* pBaseType, Creator CreatorFunc, PropertySetter SetterFunc )
		: m_strName( lpszName )
		, m_pBaseType( pBaseType )
		, m_Creator( CreatorFunc )
		//, m_PropertySetter( SetterFunc )
	{
		if (!Foundation::Factory::Instance()->Has(GetName().c_str()) )
		{
			Foundation::Factory::Instance()->Register(this, GetName().c_str());
		}

		//SetProperytSetter( SetterFunc );
		if (SetterFunc)
		{
			SetterFunc();
		}
	}

	//--------------------------------------------------------------
	bool Rtti::IsA( const Rtti& Type ) const
	{
		const Rtti* type = this;
		while (type)
		{
			if (type == &Type)
				return true;

			type = type->m_pBaseType;
		}
		return false;
	}
	bool Rtti::IsA( const std::string& TypeName ) const
	{
		const Rtti* type = this;
		while (type)
		{
			if (type->GetName() == TypeName)
				return true;

			type = type->m_pBaseType;
		}
		return false;
	}

	//--------------------------------------------------------------
	//void Rtti::SetProperytSetter( PropertySetter SetterFunc )
	//{
	//	if (SetterFunc)
	//	{
	//		SetterFunc();
	//	}
	//}
	//--------------------------------------------------------------
	void Rtti::SetProperty( const char* Name, AbstractProperty* NewProperty )
	{
		m_PropertyTable.Register( Name, NewProperty );
	}
	//--------------------------------------------------------------
	AbstractProperty* Rtti::GetProperty( const char* Name ) const
	{
		AbstractProperty* prop = m_PropertyTable.Lookup( Name );
		if (prop)
			return prop;

		if (m_pBaseType)
		{
			return m_pBaseType->GetProperty( Name );
		}

		return NULL;
	}

	//--------------------------------------------------------------
	Object* Rtti::Create() const
	{
		if (!m_Creator)
			return NULL;

		return m_Creator();
	}
}