namespace Foundation
{
	template <class T>
	inline TypedProperty<T>::TypedProperty( const char* Name )
		: AbstractProperty( Name )
	{
	}

	template <class T>
	PropertyTypeID::Enum TypedProperty<T>::GetType() const
	{
		return PropertyType<T>::GetTypeID();
	}
}