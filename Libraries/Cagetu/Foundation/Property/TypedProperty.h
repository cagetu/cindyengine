#pragma once

#include "AbstractProperty.h"
#include <Cindy/Math/CindyMath.h>

namespace Foundation
{
	class Object;

	//==================================================================
	/** TypedProperty
		@author		cagetu
		@remarks	This intermediate class defines a property that is typed
					,but not bound as a member of a particular class.
					GPG5 1.4
	*/
	//==================================================================
	template <class T>
	class TypedProperty : public AbstractProperty
	{
	public:
		TypedProperty( const char* Name );

		// Type
		virtual PropertyTypeID::Enum	GetType() const override;

		// ��
		virtual T		GetValue( Object* pOwner ) abstract;
		virtual void	SetValue( Object* pOwner, T Value ) abstract;
	};

	//template <class SetType, GetType>
	//class TypedProperty : public AbstractProperty
	//{
	//public:
	//	TypedProperty( const char* Name );

	//	// Type
	//	virtual PropertyTypeID::Enum	GetType() const abstract;

	//	// ��
	//	virtual GetType	GetValue( Object* pOwner ) abstract;
	//	virtual void	SetValue( Object* pOwner, SetType Value ) abstract;
	//};

	// Int Property
	class IntProperty : public TypedProperty<int>
	{
	public:
		IntProperty( const char* Name )
			: TypedProperty<int>(Name) {}
	};

	// Float Property
	class FloatProperty : public TypedProperty<float>
	{
	public:
		FloatProperty( const char* Name )
			: TypedProperty<float>(Name) {}
	};

	// Boolean Property
	class BoolProperty : public TypedProperty<bool>
	{
	public:
		BoolProperty( const char* Name )
			: TypedProperty<bool>(Name) {}
	};

	// Vector
	class Vector3Property : public TypedProperty<const Math::Vector3&>
	{
	public:
		Vector3Property( const char* Name )
			: TypedProperty<const Math::Vector3&>(Name) {}
	};
	class Vector4Property : public TypedProperty<const Math::Vector4&>
	{
	public:
		Vector4Property( const char* Name )
			: TypedProperty<const Math::Vector4&>(Name) {}
	};

	// Matrix
	class Matrix44Property : public TypedProperty<const Math::Matrix44&>
	{
	public:
		Matrix44Property( const char* Name )
			: TypedProperty<const Math::Matrix44&>(Name) {}
	};

	// String
	class StringProperty : public TypedProperty<const std::string&>
	{
	public:
		StringProperty( const char* Name )
			: TypedProperty<const std::string&>(Name) {}
	};
	class CharProperty : public TypedProperty<const char*>
	{
	public:
		CharProperty( const char* Name )
			: TypedProperty<const char*>(Name) {}
	};

	// WString
	class WStringProperty : public TypedProperty<const String&>
	{
	public:
		WStringProperty( const char* Name )
			: TypedProperty<const String&>(Name) {}
	};
	class WCharProperty : public TypedProperty<const wchar_t*>
	{
	public:
		WCharProperty( const char* Name )
			: TypedProperty<const wchar_t*>(Name) {}
	};

	// Object
	class ObjectProperty : public TypedProperty<Object*>
	{
	public:
		ObjectProperty( const char* Name )
			: TypedProperty<Object*>(Name) {}
	};
}

#include "TypedProperty.inl"