#pragma once

#include <Cindy/Math/CindyMath.h>

namespace Foundation
{
	struct PropertyTypeID
	{
		enum Enum
		{
			Void,
			Int,
			Float,
			Boolean,
			Vector3,
			Vector4,
			Matrix44,
			String,
			WString,
			Object,

			NumOfTypes
		};
	};

	//-----------------------------------------------------------------------------
	/**	@class		PropertyType
		@author		cagetu

		@brief		This templatized class will associate compile-time types with unique enum members.
					GPG5 1.4
	*/
	//-----------------------------------------------------------------------------
	template <class T>
	class PropertyType
	{
	public:
		static PropertyTypeID::Enum		GetTypeID();

	private:
		static PropertyTypeID::Enum		ms_TypeID;
	};

	template <class T>
	PropertyTypeID::Enum PropertyType<T>::GetTypeID()
	{
		return ms_TypeID;
	}

	template <> PropertyTypeID::Enum PropertyType<int>::ms_TypeID = PropertyTypeID::Int;
	template <> PropertyTypeID::Enum PropertyType<float>::ms_TypeID = PropertyTypeID::Float;
	template <> PropertyTypeID::Enum PropertyType<bool>::ms_TypeID = PropertyTypeID::Boolean;
	template <> PropertyTypeID::Enum PropertyType<Math::Vector3>::ms_TypeID = PropertyTypeID::Vector3;
	template <> PropertyTypeID::Enum PropertyType<Math::Vector4>::ms_TypeID = PropertyTypeID::Vector4;
	template <> PropertyTypeID::Enum PropertyType<Math::Matrix44>::ms_TypeID = PropertyTypeID::Matrix44;
	template <> PropertyTypeID::Enum PropertyType<const char*>::ms_TypeID = PropertyTypeID::String;
	template <> PropertyTypeID::Enum PropertyType<const std::string&>::ms_TypeID = PropertyTypeID::String;
	template <> PropertyTypeID::Enum PropertyType<const wchar_t*>::ms_TypeID = PropertyTypeID::WString;
	template <> PropertyTypeID::Enum PropertyType<const String&>::ms_TypeID = PropertyTypeID::WString;
	template <class T> PropertyTypeID::Enum PropertyType<T>::ms_TypeID = PropertyTypeID::Object;
}