#pragma once

#include "PropertyTypes.h"

namespace Foundation
{
	//==================================================================
	/** Abstract Property
		@author		cagetu
		@remarks	모든 Property 클래스의 기본 클래스
					GPG5 1.4
	*/
	//==================================================================
	class AbstractProperty
	{
	public:
		AbstractProperty( const char* Name );

		const char*						GetName() const;
		virtual PropertyTypeID::Enum	GetType() const abstract;

	protected:
		const char*		m_Name;	// Property Name
	};
}

#include "AbstractProperty.inl"