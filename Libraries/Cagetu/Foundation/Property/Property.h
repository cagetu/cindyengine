#pragma once

#include "TypedProperty.h"

namespace Foundation
{
	//==================================================================
	/** Property
		@author		cagetu
		@remarks	외부에서 접근 가능한 Property 객체
					(by C# or Java)
	*/
	//==================================================================
	template <class OwnerType, class T>
	class Property : public TypedProperty<T>
	{
	public:
		// Getter/Setter
		typedef T		(OwnerType::*pfnGetter)();
		typedef void	(OwnerType::*pfnSetter)( T Value );

		Property( const char* Name, pfnGetter Getter, pfnSetter Setter );

		// 값 
		virtual T		GetValue( Object* Owner ) override;
		virtual void	SetValue( Object* Owner, T Value ) override;

	protected:
		pfnGetter	m_Getter;
		pfnSetter	m_Setter;
	};
}

#include "Property.inl"