namespace Foundation
{
	template <class OwnerType, class T>
	Property<OwnerType, T>::Property( const char* Name, pfnGetter Getter, pfnSetter Setter )
		: TypedProperty<T>(Name)
		, m_Getter(Getter)
		, m_Setter(Setter)
	{
	}

	template <class OwnerType, class T>
	T Property<OwnerType, T>::GetValue( Object* Owner )
	{
		return (((OwnerType*)Owner)->*m_Getter)();
	}

	template <class OwnerType, class T>
	void Property<OwnerType, T>::SetValue( Object* Owner, T Value )
	{
		if (!m_Setter)
		{
			//assert( false );
			return;
		}
		(((OwnerType*)Owner)->*m_Setter)( Value );
	}
}