// Copyright (c) 2006~. cagetu
//
//******************************************************************
namespace Foundation
{
	//--------------------------------------------------------------
	template <class T>
	Pointer<T>::Pointer()
		: m_pObject(0)
	{
	}

	//--------------------------------------------------------------
	template <class T>
	Pointer<T>::Pointer( T* pObject )
	{
		m_pObject = pObject;

		if( m_pObject )
			m_pObject->AddRef();
	}

	//--------------------------------------------------------------
	template <class T>
	Pointer<T>::Pointer( const Pointer& spObject )
	{
		m_pObject = spObject.m_pObject;

		if( m_pObject )
			m_pObject->Release();
	}

	//--------------------------------------------------------------
	template <class T>
	Pointer<T>::~Pointer()
	{
		if( m_pObject )
			m_pObject->Release();
	}

	//--------------------------------------------------------------
	template <class T>
	void Pointer<T>::SetNull()
	{
		if( m_pObject )
		{
			m_pObject->Release();
			m_pObject = 0;
		}
	}

	//--------------------------------------------------------------
	template <class T>
	bool Pointer<T>::IsNull() const
	{
		return ( 0 == m_pObject );
	}

	//--------------------------------------------------------------
	template <class T>
	T* Pointer<T>::GetPtr() const
	{
		return m_pObject;
	}

	//--------------------------------------------------------------
	template <class T>
	Pointer<T>::operator T*() const
	{
		return m_pObject;
	}

	//--------------------------------------------------------------
	template <class T>
	Pointer<T>::operator const T*() const
	{
		return m_pObject;
	}

	//--------------------------------------------------------------
	template <class T>
	T& Pointer<T>::operator* () const
	{
		assert( m_pObject );

		return *m_pObject;
	}

	//--------------------------------------------------------------
	template <class T>
	T* Pointer<T>::operator-> () const
	{
		assert( m_pObject );

		return m_pObject;
	}

	//--------------------------------------------------------------
	template <class T>
	Pointer<T>& Pointer<T>::operator= ( T* pObject )
	{
		if( m_pObject != pObject )
		{
			if( m_pObject )
				m_pObject->Release();

			if( pObject )
				pObject->AddRef();

			m_pObject = pObject;
		}
		return *this;
	}

	//--------------------------------------------------------------
	template <class T>
	Pointer<T>& Pointer<T>::operator= ( const Pointer& spObject )
	{
		if( m_pObject != spObject.m_pObject )
		{
			if( m_pObject )
				m_pObject->Release();

			if( spObject.m_pObject )
				spObject.m_pObject->AddRef();

			m_pObject = spObject.m_pObject;
		}
		return *this;
	}

	//--------------------------------------------------------------
	template <class T>
	bool Pointer<T>::operator== ( T* pObject ) const
	{
		return (m_pObject == pObject);
	}

	//--------------------------------------------------------------
	template <class T>
	bool Pointer<T>::operator!= ( T* pObject ) const
	{
		return (m_pObject != pObject);
	}

	//--------------------------------------------------------------
	template <class T>
	bool Pointer<T>::operator== ( const Pointer& spObject ) const
	{
		return (m_pObject == spObject.m_pObject);
	}

	//--------------------------------------------------------------
	template <class T>
	bool Pointer<T>::operator!= ( const Pointer& spObject ) const
	{
		return (m_pObject != spObject.m_pObject);
	}
}
