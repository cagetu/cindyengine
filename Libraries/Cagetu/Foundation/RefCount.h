// Copyright (c) 2008~. cagetu
//
//******************************************************************
#pragma once

namespace Foundation
{
	//==================================================================
	/** RefCounter
		@author		cagetu
		@remarks	참조 카운터
	*/
	//==================================================================
	class RefCounter
	{
	public:
		RefCounter();

		// SmartPointer System
		void	AddRef();
		bool	Release();

		int		GetRefCount() const;

	protected:
		virtual ~RefCounter();

	private:
		int		m_RefCount;				//!< 참조 카운트
	};
}

#include "RefCount.inl"
