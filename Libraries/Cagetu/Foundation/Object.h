// Copyright (c) 2008~. cagetu
//
//******************************************************************
#pragma once

#include "Rtti.h"
#include "SmartPointer.h"
#include "RefCount.h"

namespace Foundation
{
	//==================================================================
	/** Object
		@author			cagetu
		@remarks
			객체의 최상위 클래스, 객체가 가져야 하는 기본 속성을 가진다.
			<delete this로 삭제한다는 것은 동적 메모리 할당해서 생성하겠다는 것을 뜻한다.>
		@see
			3D Game Engine Architecture( by Eberly ) - WildMagic3
	*/
	//==================================================================
	class Object : public RefCounter
	{
		DECLARE_RTTI;
	public:
		typedef std::map<unsigned long, Object*>	ObjectMap;
		typedef ObjectMap::iterator					ObjectIter;

		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		static ObjectMap		InUse;						//!< 생성된 객체 리스트 (원하는 시점에서 객체의 존재 여부를 알아온다.)

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		virtual ~Object();

		bool	IsInstanceOf( const Rtti& Class ) const;
		bool	IsInstanceOf( const std::string& Class ) const;
		bool	IsA( const Rtti& Class ) const;
		bool	IsA( const std::string& Class ) const;

	protected:
		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		Object();

		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		unsigned long			m_ulID;						//!< 객체 고유 ID
		static unsigned long	ms_ulNextID;				//!< 다음 객체에 할당할 ID
	};
} // end of namespace

// casting
template <class T>	T* StaticCast( Foundation::Object* pObject );
template <class T>	const T* StaticCast( const Foundation::Object* pObject );
template <class T>	T* DynamicCast( Foundation::Object* pObject );
template <class T>	const T* DynamicCast( const Foundation::Object* pObject );

#include "Object.inl"
