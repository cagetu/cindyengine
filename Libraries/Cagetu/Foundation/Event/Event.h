#pragma once

#include "../../Foundation/Object.h"

namespace Foundation
{
	class EventHandler;

	//-----------------------------------------------------------------------------
	/**	@class		Event Interface
		@author		cagetu

		@brief		메시지 기본 객체
	*/
	//-----------------------------------------------------------------------------
	class Event : public Foundation::Object
	{
		DECLARE_RTTI;
	public:
		virtual ~Event();

		virtual void	SendSync( EventHandler* );
		virtual void	BroadcastSync();

		virtual void	SendASync( EventHandler* );
		virtual void	BroadcastASync();
	};

	typedef Foundation::Pointer<Event>	EventPtr;
}
