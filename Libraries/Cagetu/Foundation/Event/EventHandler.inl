//================================================================
// File:               : EventHandler.inl
// Related Header File : EventHandler.h
// Original Author     : changhee
//================================================================
namespace Foundation
{
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	template<class T, class EventT>
	void MemberFunctionHandler<T, EventT>::Call( Event* pEvent )
	{
		(m_Instance->*m_Function)(static_cast<EventT*>(pEvent));
	}

	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	template <class T, class EventT>
	void EventHandler::RegisterProcFunction( T* pObject, void (T::*MemFn)(EventT*) )
	{
		m_Functions[TypeInfo(typeid(EventT))] = new MemberFunctionHandler<T, EventT>(pObject, MemFn);
	}

	//-------------------------------------------------------------------------
	/** @brief	비어 있는고? */
	//-------------------------------------------------------------------------
	inline
	bool EventHandler::IsEmpty() const
	{
		return m_Functions.empty();
	}
}