#pragma once

#include <typeinfo.h>
#include "Event.h"

namespace Foundation
{
	class AbstractFunctionHandler;

	//-----------------------------------------------------------------------------
	/**	@class		Event Handler
		@author		cagetu

		@brief		이벤트(메시지)를 처리하기 위한 처리기이다.
					이벤트를 처리할 함수를 등록하고, 이벤트를 처리한다.

					http://www.swallowstudio.com/wiki/doku.php?id=technicalarticles%3Aeffectiveeventhandlingincpp
	*/
	//-----------------------------------------------------------------------------
	class EventHandler : public Foundation::Object
	{
		DECLARE_RTTI;
	public:
		EventHandler();
		virtual ~EventHandler();

		template <class T, class EventT>
		void	RegisterProcFunction( T*, void (T::*MemFn)(EventT*) );

		// Message
		virtual void	HandleEvent( Event* );

		// Post Message 처리
		virtual void	Post( Event* );
		virtual void	HandlePendingEvents();

		// 유효성 판정
		virtual bool	IsValid( Event* pEvent );
		virtual bool	IsEmpty() const;

	private:
		// Message TypeInfo
		class TypeInfo
		{
		public:
			explicit TypeInfo(const type_info& info) : _typeInfo(info) {};
			bool operator < (const TypeInfo& rhs) const {
				return _typeInfo.before(rhs._typeInfo) != 0;
			}

		private:
			const type_info& _typeInfo;
		};

		// Typedefines..
		typedef std::map<TypeInfo, AbstractFunctionHandler*>	FunctionList;
		typedef std::vector<EventPtr>							EventList;

		FunctionList	m_Functions;
		EventList		m_PendingEvents;
	};

	//-----------------------------------------------------------------------------
	/**	@class		FunctionHandler
		@author		cagetu

		@brief		함수 포인터를 객체화 시킨다.
	*/
	//-----------------------------------------------------------------------------
	class AbstractFunctionHandler
	{
	public:
		virtual ~AbstractFunctionHandler() {};

		void	Execute( Event* pEvent )		{	Call(pEvent);	}
	private:
		virtual void	Call( Event* ) abstract;
	};

	template<class T, class EventT>
	class MemberFunctionHandler : public AbstractFunctionHandler
	{
	public:
		typedef void (T::*MemberFunc)(EventT*);

		MemberFunctionHandler( T* Instance, MemberFunc MemFn )
			: m_Instance(Instance), m_Function(MemFn) {}

		void	Call( Event* pEvent );
	private:
		T*			m_Instance;
		MemberFunc	m_Function;
	};
}

#include "EventHandler.inl"