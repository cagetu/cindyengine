//================================================================
// File:               : Event.cpp
// Related Header File : Event.h
// Original Author     : changhee
//================================================================
#include "Event.h"
#include "EventHandler.h"
//#include "Server.h"

namespace Foundation
{
	//-------------------------------------------------------------------------
	IMPLEMENT_RTTI( Foundation, Event, Object );
	//-------------------------------------------------------------------------
	Event::~Event()
	{
	}

	//-------------------------------------------------------------------------
	void Event::SendSync( EventHandler* pHandler )
	{
		//Server::Instance()->SendSync( pHandler, this );
		pHandler->HandleEvent( this );
	}

	//-------------------------------------------------------------------------
	void Event::BroadcastSync()
	{
		//Server::Instance()->BroadcastSync( this );
	}

	//-------------------------------------------------------------------------
	void Event::SendASync( EventHandler* pHandler )
	{
		//Server::Instance()->SendASync( pHandler, this );
		pHandler->Post( this );
	}
	void Event::BroadcastASync()
	{
		//Server::Instance()->BroadcastASync( this );
	}
}