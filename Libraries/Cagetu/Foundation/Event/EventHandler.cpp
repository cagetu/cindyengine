//================================================================
// File:               : EventHandler.cpp
// Related Header File : EventHandler.h
// Original Author     : changhee
//================================================================
#include "EventHandler.h"

namespace Foundation
{
	//-------------------------------------------------------------------------
	IMPLEMENT_RTTI( Foundation, EventHandler, Object );
	//-------------------------------------------------------------------------
	EventHandler::EventHandler()
	{
	}
	EventHandler::~EventHandler()
	{
		FunctionList::iterator i, iend;
		iend = m_Functions.end();
		for (i=m_Functions.begin(); i != iend; ++i)
			delete i->second;
		m_Functions.clear();
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 처리 */
	//-------------------------------------------------------------------------
	void EventHandler::HandleEvent( Event* pEvent )
	{
		FunctionList::iterator it = m_Functions.find( TypeInfo(typeid(*pEvent)) );
		if(it != m_Functions.end())
		{
			it->second->Execute( pEvent );
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 큐에 메시지 등록 */
	//-------------------------------------------------------------------------
	void EventHandler::Post( Event* pEvent )
	{
		if (this->IsValid(pEvent))
		{
			m_PendingEvents.push_back( pEvent );
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 큐에 등록된 메시지 처리 */
	//-------------------------------------------------------------------------
	void EventHandler::HandlePendingEvents()
	{
		EventList::iterator i, iend;
		iend = m_PendingEvents.end();

		for (i=m_PendingEvents.begin(); i!=iend; ++i)
		{
			this->HandleEvent( (*i) );
		}
		m_PendingEvents.clear();
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 처리 가능 */
	//-------------------------------------------------------------------------
	bool EventHandler::IsValid( Event* pEvent )
	{
		FunctionList::iterator it = m_Functions.find( TypeInfo(typeid(*pEvent)) );
		if (it != m_Functions.end())
			return true;

		return false;
	}
}