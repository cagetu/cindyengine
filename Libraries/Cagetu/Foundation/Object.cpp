// Copyright (c) 2008~. cagetu
//
//******************************************************************
#include "Object.h"

namespace Foundation
{
	//============================================================================
	IMPLEMENT_ROOT_RTTI( Object );
	//--------------------------------------------------------------
	unsigned long Object::ms_ulNextID = 0;
	Object::ObjectMap Object::InUse;

	//--------------------------------------------------------------
	Object::Object()
	{
		m_ulID = ms_ulNextID++;

		InUse.insert( ObjectMap::value_type( m_ulID, this ) );
	}

	//--------------------------------------------------------------
	Object::~Object()
	{
		ObjectIter iter = InUse.find( m_ulID );
		assert( iter != InUse.end() );

		InUse.erase( iter );
	}
}
