// Copyright (c) 2006~. cagetu
//
//******************************************************************
namespace Foundation
{
	//--------------------------------------------------------------
	inline
	const std::string& Rtti::GetName() const
	{
		return m_strName;
	}

	//--------------------------------------------------------------
	inline
	bool Rtti::IsInstanceOf( const Rtti& Type ) const
	{
		return &Type == this;
	}
	inline
	bool Rtti::IsInstanceOf( const std::string& TypeName ) const
	{
		return TypeName == GetName();
	}
}

//--------------------------------------------------------------
// Type declaration (header file)
//--------------------------------------------------------------
#define DECLARE_RTTI \
public: \
	static Foundation::Rtti RTTI; \
	virtual Foundation::Rtti* GetRtti() const { return &RTTI; }

//--------------------------------------------------------------
// Type implementation (source file)
//--------------------------------------------------------------
#define IMPLEMENT_RTTI(name_space, class_name, baseclass_name) \
    Foundation::Rtti class_name::RTTI(#name_space"@"#class_name, &baseclass_name::RTTI, 0);

//--------------------------------------------------------------
// Type implementation of topmost type in inheritance hierarchy (source file)
//--------------------------------------------------------------
#define IMPLEMENT_ROOT_RTTI(rootclass_name) \
	Foundation::Rtti rootclass_name::RTTI(#rootclass_name, 0, 0);

//**************************************************************
//	Class 에 필요한 매크로
//**************************************************************
//--------------------------------------------------------------
// Type declaration (header file)
//--------------------------------------------------------------
#define DECLARE_CLASS(class_name) \
public: \
	static Foundation::Rtti RTTI; \
	static Foundation::Object*	FactoryCreator();	\
	static class_name*			Create(); \
	static bool					RegisterFactoryFunction();	\
\
	template <class OwnerType, class Type> \
	static void	RegisterProperty( const char* Name, Type (OwnerType::*Getter)(), void (OwnerType::*Setter)(Type) ) \
	{ \
		Foundation::Property<OwnerType, Type>* newProperty = new Foundation::Property<OwnerType, Type>( Name, Getter, Setter ); \
		OwnerType::RTTI.SetProperty( Name, newProperty ); \
	} \
	virtual Foundation::AbstractProperty*	GetProperty( const char* Name ) const { return GetRtti()->GetProperty( Name ); } \
	virtual Foundation::Rtti* GetRtti() const { return &RTTI; } \
private:

#define DECLARE_ABSTRACT_CLASS(class_name) \
public: \
	static Foundation::Rtti RTTI; \
	virtual Foundation::Rtti* GetRtti() const { return &RTTI; } \
private:

//--------------------------------------------------------------
// Type implementation (source file)
//--------------------------------------------------------------
#define IMPLEMENT_CLASS(name_space, class_name, baseclass_name, propertysetter) \
	Foundation::Rtti class_name::RTTI(#name_space"@"#class_name, &baseclass_name::RTTI, class_name::FactoryCreator, propertysetter);	\
	Foundation::Object* class_name::FactoryCreator()	{	return class_name::Create();		} \
	class_name* class_name::Create() \
	{ \
		class_name* newObject = new class_name();	\
		return newObject;	\
	} \
	bool class_name::RegisterFactoryFunction() \
	{ \
		if (!Foundation::Factory::Instance()->Has(#name_space"@"#class_name) )\
		{ \
			Foundation::Factory::Instance()->Register(&class_name::RTTI, #name_space"@"#class_name); \
		} \
		return true; \
	} \

#define IMPLEMENT_ABSTRACT_CLASS(name_space, class_name, baseclass_name, propertysetter) \
	Foundation::Rtti class_name::RTTI(#name_space"@"#class_name, &baseclass_name::RTTI, 0, propertysetter);	\

//--------------------------------------------------------------
// Type implementation of topmost type in inheritance hierarchy (source file)
//--------------------------------------------------------------
#define IMPLEMENT_ROOT_CLASS(name_space, class_name, propertysetter) \
	Foundation::Rtti class_name::RTTI(#name_space"@"#class_name, 0, class_name::FactoryCreator, propertysetter);	\
	Foundation::Object* class_name::FactoryCreator()	{	return class_name::Create();		} \
	class_name* class_name::Create() \
	{ \
		class_name* newObject = new class_name();	\
		return newObject;	\
	} \
	bool class_name::RegisterFactoryFunction() \
	{ \
		if (!Foundation::Factory::Instance()->Has(#name_space"@"#class_name) )\
		{ \
			Foundation::Factory::Instance()->Register(&class_name::RTTI, #name_space"@"#class_name); \
		} \
		return true; \
	} \

//-----------------------------------------------------------------------------
//	Factory 매크로 Factory 등록
//-----------------------------------------------------------------------------
#define REGISTER_CLASS(name_space, class_name) \
	static const bool factoryRegistered_##name_space"@"#class_name = class_name::RegisterFactoryFunction();
