// Copyright (c) 2008~. cagetu
//
//******************************************************************
#pragma once

namespace Foundation
{
	//==================================================================
	/** Uncopyable
		@author			cagetu
		@remarks		객체 복사 방지 클래스
						private 상속을 이용한다.
	*/
	//==================================================================
	class Uncopyable
	{
	private:
		Uncopyable( const Uncopyable& );
		Uncopyable& operator =( const Uncopyable& );

	public:
		Uncopyable()		{}
		~Uncopyable()		{}
	};
}
