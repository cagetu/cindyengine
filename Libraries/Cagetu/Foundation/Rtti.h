// Copyright (c) 2008~. cagetu
//
//******************************************************************
#pragma once

#include "../Cagetu.h"
#include "Factory.h"
#include "Property.h"

namespace Foundation
{
	class Object;

	//==================================================================
	/** Rtti
		@author			cagetu
		@remarks
			Run-time type information system 객체
		@see
			3D Game Engine Design( by Eberly ) - WildMagic4
	*/
	//==================================================================
	class Rtti
	{
	public:
		typedef Object*	(*Creator)();
		typedef void	(*PropertySetter)();

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		Rtti( const char* lpszName, const Rtti* pBaseType, Creator CreatorFunc, PropertySetter SetterFunc = 0 );

		/** 타입 이름 / 기본 타입
		*/
		const Rtti*			GetBaseType() const;
		const std::string&	GetName() const;

		// Factory
		Object*				Create() const;

		// Hierarchy
		bool				IsInstanceOf( const Rtti& Type ) const;
		bool				IsInstanceOf( const std::string& TypeName ) const;
		
		bool				IsA( const Rtti& Type ) const;
		bool				IsA( const std::string& TypeName ) const;

		// Property
		AbstractProperty*	GetProperty( const char* Name ) const;
		void				SetProperty( const char* Name, AbstractProperty* );
	private:
		std::string			m_strName;			//!< 타입 이름
		const Rtti*			m_pBaseType;		//!< 부모 타입

		const Creator		m_Creator;

		PropertyTable		m_PropertyTable;
	};

} // namespace Foundation

#include "Rtti.inl"
