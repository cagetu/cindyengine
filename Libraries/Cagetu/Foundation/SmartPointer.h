// Copyright (c) 2008~. cagetu
//
//******************************************************************
#pragma once

namespace Foundation
{
	//==================================================================
	/** Pointer Class
		@author			cagetu
		@remarks		스마트 포인터 클래스. 
	*/
	//==================================================================
	template <class T>
	class Pointer
	{
	public:
		Pointer();
		Pointer( T* pObject );
		Pointer( const Pointer& spObject );
		virtual ~Pointer();

		// Null 판정
		void		SetNull();
		bool		IsNull() const;

		T*			GetPtr() const;

		// implicit conversions
		operator T*() const;
		operator const T*() const;
		T& operator* () const;
		T* operator-> () const;

		// assignment
		Pointer& operator= ( T* pObject );
		Pointer& operator= ( const Pointer& spObject );

		// comparisons
		bool operator== ( T* pObject ) const;
		bool operator!= ( T* pObject ) const;
		bool operator== ( const Pointer& spObject ) const;
		bool operator!= ( const Pointer& spObject ) const;

	protected:
		T*			m_pObject;			//!< 공유 객체
	};
}

#include "SmartPointer.inl"
