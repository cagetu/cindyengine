#ifndef __CN_COMPONENT_TEMPLATE_H__
#define __CN_COMPONENT_TEMPLATE_H__

#include "CnComponent.h"

namespace EntityComponentSystem
{
	//==================================================================
	/** Component Template
		@author		cagetu
		@since		2008년 4월 14일
		@remarks	같은 종류의 Component를 생산하기 위한 Template 클래스
	*/
	//==================================================================
	class ComponentTemplate
	{
	public:
		ComponentTemplate() {};
		virtual ~ComponentTemplate() = 0 {};

		virtual const CompID&	ComponentID() const abstract;
		virtual const FamilyID&	FamilyID() const abstract;

		virtual Component*		Create() abstract;
	};
}

#endif	// __CN_COMPONENT_TEMPLATE_H__