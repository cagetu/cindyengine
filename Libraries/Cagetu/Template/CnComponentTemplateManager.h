#ifndef __CN_COMPONENT_TEMPLATE_MANAGER_H__
#define __CN_COMPONENT_TEMPLATE_MANAGER_H__

#include <map>
#include "CnComponentTemplate.h"

namespace EntityComponentSystem
{
	//==================================================================
	/** Component Template Manager
		@author		cagetu
		@since		2008년 4월 14일
		@remarks	같은 종류의 Component를 생산하기 위한 Template들을 관리하는 녀석
	*/
	//==================================================================
	class ComponentTemplateManager
	{
		typedef std::map<const CompID, ComponentTemplate*>	TemplateList;
		typedef TemplateList::iterator						TemplateIter;

	private:
		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		TemplateList			m_Templates;

		static ComponentTemplateManager*	ms_pInstance;

	public:
		ComponentTemplateManager();
		~ComponentTemplateManager();

		void		Register( ComponentTemplate* pTemplate );
		void		Unregister( const CompID& ID );
		void		Clear();

		Component*	Create( const CompID& ID );

		static ComponentTemplateManager* Instance();
	};

}

#endif	// __CN_COMPONENT_TEMPLATE_MANAGER_H__