#ifndef __CN_ENTITY_TEMPLATE_H__
#define __CN_ENTITY_TEMPLATE_H__

#include "CnComponentTemplate.h"
#include <vector>

namespace EntityComponentSystem
{
	class EntityTemplate
	{
	public:
		typedef std::vector<ComponentTemplate*>	CompTemplateList;
		typedef CompTemplateList::iterator		CompTemplateIter;

	protected:
		CnString			m_strName;
		CompTemplateList	m_CompTemplates;

	public:
		EntityTemplate( const CnString& strName );
		~EntityTemplate();

		void				SetName( const CnString& strName )		{	m_strName = strName;		}
		const CnString&		GetName() const							{	return m_strName;			}

		CompTemplateList&	GetComponents()							{	return m_CompTemplates;		}

		void				Add( ComponentTemplate* pTemplate );
		void				Remove( const CompID& ID );
		void				Clear();

		ComponentTemplate*	Find( const CompID& ID ) const;
	};

}

#endif	// __CN_ENTITY_TEMPLATE_H__