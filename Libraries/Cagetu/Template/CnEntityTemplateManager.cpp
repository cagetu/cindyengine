#include "CnEntityTemplateManager.h"

namespace EntityComponentSystem
{
	EntityTemplateManager* EntityTemplateManager::ms_pInstance = NULL;

	// Const/Dest
	EntityTemplateManager::EntityTemplateManager()
	{
		ms_pInstance = this;
	}
	EntityTemplateManager::~EntityTemplateManager()
	{
		ms_pInstance = NULL;
	}

	//------------------------------------------------------
	void EntityTemplateManager::Register( EntityTemplate* pTemplate )
	{
		m_EntityTemplates.push_back( pTemplate );
	}

	//------------------------------------------------------
	void EntityTemplateManager::Unregister( const CnString& strName )
	{
		EntityTemplateIter iend = m_EntityTemplates.end();
		for (EntityTemplateIter i=m_EntityTemplates.begin(); i!=iend; ++i)
		{
			if (strName == (*i)->GetName())
			{
				delete (*i);
				m_EntityTemplates.erase( i );
				return;
			}
		}

	}

	//------------------------------------------------------
	void EntityTemplateManager::Clear()
	{
		EntityTemplateIter iend = m_EntityTemplates.end();
		for (EntityTemplateIter i=m_EntityTemplates.begin(); i!=iend; ++i)
			delete (*i);
		m_EntityTemplates.clear();
	}

	//------------------------------------------------------
	Entity* EntityTemplateManager::Create( const CnString& strTemplateName, const EntityTypeID& EntityID )
	{
		unsigned int index = 0;
		unsigned size = (unsigned int)m_EntityTemplates.size();
		while (index < size)
		{
			if (m_EntityTemplates[index]->GetName() == strTemplateName)
				break;

			++index;
		}
		if (index == size)
			return NULL;

		Entity* newEntity = new Entity( EntityID );

		EntityTemplate::CompTemplateList componentTemplates = m_EntityTemplates[index]->GetComponents();
		size = (unsigned int)componentTemplates.size();
		for (index=0; index<size; ++index)
		{
			newEntity->SetComponent( componentTemplates[index]->Create() );
		}
		return newEntity;
	}

	//------------------------------------------------------
	//------------------------------------------------------
	EntityTemplateManager* EntityTemplateManager::Instance()
	{
		return ms_pInstance;
	}
}