#ifndef __CN_ENTITY_TEMPLATE_MANAGER_H__
#define __CN_ENTITY_TEMPLATE_MANAGER_H__

#include "CnEntityTemplate.h"
#include "CnEntity.h"

namespace EntityComponentSystem
{
	class EntityTemplateManager
	{
		typedef std::vector<EntityTemplate*>	EntityTemplateList;
		typedef EntityTemplateList::iterator	EntityTemplateIter;
	protected:
		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		EntityTemplateList		m_EntityTemplates;

		static EntityTemplateManager*	ms_pInstance;
	public:
		EntityTemplateManager();
		~EntityTemplateManager();

		void	Register( EntityTemplate* pTemplate );
		void	Unregister( const CnString& strName );
		void	Clear();

		Entity*	Create( const CnString& strTemplateName, const EntityTypeID& EntityID );

		static EntityTemplateManager*	Instance();
	};
}

#endif	// __CN_ENTITY_TEMPLATE_MANAGER_H__