#include "CnComponentTemplateManager.h"

namespace EntityComponentSystem
{
	ComponentTemplateManager* ComponentTemplateManager::ms_pInstance = NULL;

	//----------------------------------------------------------------
	// Const/Dest
	ComponentTemplateManager::ComponentTemplateManager()
	{
		ms_pInstance = this;
	}

	ComponentTemplateManager::~ComponentTemplateManager()
	{
		ms_pInstance = NULL;
	}

	//----------------------------------------------------------------
	void ComponentTemplateManager::Register( ComponentTemplate* pTemplate )
	{
		m_Templates[ pTemplate->ComponentID() ] = pTemplate;
	}

	//----------------------------------------------------------------
	void ComponentTemplateManager::Unregister( const CompID& ID )
	{
		TemplateIter iter = m_Templates.find( ID );
		if (iter == m_Templates.end())
			return;

		delete iter->second;
		m_Templates.erase( ID );
	}

	//----------------------------------------------------------------
	void ComponentTemplateManager::Clear()
	{
		TemplateIter iend = m_Templates.end();
		for (TemplateIter i=m_Templates.begin(); i!=iend; ++i)
			delete i->second;
		m_Templates.clear();
	}

	//----------------------------------------------------------------
	Component* ComponentTemplateManager::Create( const CompID& ID )
	{
		return m_Templates[ ID ]->Create();
	}

	//----------------------------------------------------------------
	ComponentTemplateManager* ComponentTemplateManager::Instance()
	{
		return ms_pInstance;
	};
	
} // end of namespace "EntityComponentSystem"
