#include "CnEntityTemplate.h"

namespace EntityComponentSystem
{
	// Const/Dest
	EntityTemplate::EntityTemplate( const CnString& strName )
		: m_strName( strName )
	{
	}
	EntityTemplate::~EntityTemplate()
	{
		Clear();
	}

	//----------------------------------------------------------------
	void EntityTemplate::Add( ComponentTemplate* pTemplate )
	{
		m_CompTemplates.push_back( pTemplate );
	}

	//----------------------------------------------------------------
	void EntityTemplate::Remove(const CompID& ID )
	{
		CompTemplateIter iend = m_CompTemplates.end();
		for (CompTemplateIter i=m_CompTemplates.begin(); i != iend; ++i)
		{
			if ((*i)->ComponentID() == ID)
			{
				delete (*i);
				m_CompTemplates.erase(i);
				return;
			}
		} // for
	}

	//----------------------------------------------------------------
	void EntityTemplate::Clear()
	{
		CompTemplateIter iend = m_CompTemplates.end();
		for (CompTemplateIter i=m_CompTemplates.begin(); i!=iend; ++i)
			delete (*i);
		m_CompTemplates.clear();
	}

	//----------------------------------------------------------------
	ComponentTemplate* EntityTemplate::Find( const CompID& ID ) const
	{
		unsigned int index = 0;

		unsigned int numTemplates = (unsigned int)m_CompTemplates.size();
		while (index<numTemplates)
		{
			if (m_CompTemplates[index]->ComponentID() == ID)
				break;

			++index;
		}

		if (index == numTemplates)
			return NULL;

		return m_CompTemplates[index];
	}
}
