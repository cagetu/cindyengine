#ifndef __CAGETU_H__
#define __CAGETU_H__

#include <MoCommon/MoCommon.h>
#include <Cindy/CindyHeaders.h>

#ifdef CnString
typedef CnString	String;
#else
typedef	std::wstring	String;
#endif

using namespace Cindy;

#endif