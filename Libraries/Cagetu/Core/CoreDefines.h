#pragma once

namespace Core
{
	// Entity's Defines
	typedef String		EntityID;

	// Component's Defines
	typedef std::string ComID;
	typedef String		TypeID;
	typedef int			Priority;

}