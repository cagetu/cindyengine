#pragma once

namespace Core
{
	//-----------------------------------------------------------------------------
	/**	@class		Variable
		@author		cagetu

		@brief		가변 인자 클래스
	*/
	//-----------------------------------------------------------------------------
	class Variable
	{
	public:
		//-----------------------------------------------------------------------------
		//	Enum
		//-----------------------------------------------------------------------------
		enum ValueType
		{
			VT_UNDEFINED,
			VT_NULL,
			VT_BOOLEAN,
			VT_INT,
			VT_FLOAT,
			VT_STRING,

			VT_CONVERT_BIT		= 0x10,
			VT_CONVERT_BOOLEAN	= VT_CONVERT_BIT | VT_BOOLEAN,
			VT_CONVERT_INT		= VT_CONVERT_BIT | VT_INT,
			VT_CONVERT_FLOAT	= VT_CONVERT_BIT | VT_FLOAT,
			VT_CONVERT_STRING	= VT_CONVERT_BIT | VT_STRING,
		};

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		Variable();
		Variable( ValueType Type );
		Variable( int Value );
		Variable( float Value );
		Variable( const wchar_t* Value );
		Variable( const Variable& Value );

		const Variable& operator= (const Variable& Value );

		// Get Type
		ValueType		GetType() const;

		// Get Value
		bool			GetBoolean() const;
		int				GetInt() const;
		float			GetFloat() const;
		const wchar_t*	GetString() const;

		// Set Type And Value
		void			SetUndefined();
		void			SetNull();
		void			SetBoolean( bool Value );
		void			SetInt( int Value );
		void			SetFloat( float Value );
		void			SetString( const wchar_t* Value );

		void			SetConvertBoolean()		{	m_Type = VT_CONVERT_BOOLEAN;	}
		void		    SetConvertInt()			{	m_Type = VT_CONVERT_INT;		}
		void		    SetConvertFloat()		{	m_Type = VT_CONVERT_FLOAT;		}
		void			SetConvertString()		{	m_Type = VT_CONVERT_STRING;		}
	protected:
		union ValueUnion
		{
			float			m_Float;
			int				m_Int;
			bool			m_Boolean;
			const wchar_t*	m_String;
		};

		ValueType	m_Type;
		ValueUnion	m_Data;
	};
}

#include "Variable.inl"