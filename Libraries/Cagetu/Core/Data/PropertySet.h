#pragma once

#include "Property.h"

namespace Core
{
	//==================================================================
	/** Property Set
		@author		cagetu
		@remarks	속성들은 PropertySet 클래스를 통해 생성, 조작된다.
	*/
	//==================================================================
	class PropertySet
	{
	public:
		PropertySet();
		virtual ~PropertySet();

		Property*	Lookup( const String& Name );

		// 값 세팅
		bool		Set( const String& Name, int Value );
		bool		Set( const String& Name, float Value );
		bool		Set( const String& Name, bool Value );
		bool		Set( const String& Name, const String& Value );

	protected:
		typedef HashMap<String, Property*>	PropertyList;
		typedef PropertyList::iterator		PropertyIter;

		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		PropertyList		m_Properties;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		// 등록...
		void		Register( const String& Name, int* Value );
		void		Register( const String& Name, float* Value );
		void		Register( const String& Name, bool* Value );
		void		Register( const String& Name, String* Value );
	};
}