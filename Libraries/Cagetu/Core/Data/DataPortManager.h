#pragma once

#include "../Cagetu.h"

namespace Core
{
	typedef enum
	{
		DATAPORT_SUCCESS = 0,
		DATAPORT_ERROR_NAMECLASH,
		DATAPORT_ERROR_NOMOREDATAPORTSLOTS,
		DATAPORT_WARNING_BEINGREFERENCED,
		DATAPORT_ERROR_ENTRYNOTFOUND,
		DATAPORT_ERROR_NOTATTACHED,
		DATAPORT_ERROR_WRONGTYPE,
		DATAPORT_ERROR_REFCOUNTZERO
	} DataportReturn; 

	//-----------------------------------------------------------------------------
	/**	@class		DataPortManager
		@author		cagetu

		@brief		Dataport ������
	*/
	//-----------------------------------------------------------------------------
	class DataPortManager
	{
		static const unsigned int MAX_DATAPORTS = 196;
	public:
		DataPortManager();

		DataportReturn	Add( const String& Name, void* Ptr, unsigned int ID );
		DataportReturn	Remove( void* Ptr, bool Check = true );

		void*			Attach( const String& Name, unsigned int ID );
		DataportReturn	Detach( void* Ptr );

		DataportReturn	Poll( void* Ptr );

		static DataPortManager*	Instance();
	protected:
		void*			m_Dataports[ MAX_DATAPORTS ];
		unsigned int	m_DataportsMangled[ MAX_DATAPORTS ];
		unsigned int	m_DataportsRefCount[ MAX_DATAPORTS ];
		unsigned int	m_NumDataports;
	};
}