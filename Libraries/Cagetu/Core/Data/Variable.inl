namespace Core
{
	inline
	Variable::Variable()
	: m_Type(VT_UNDEFINED)
	{
	}
	inline
	Variable::Variable( ValueType Type )
	: m_Type(Type)
	{
		m_Data.m_String = 0;
	}
	inline
	Variable::Variable( int Value )
	: m_Type(VT_INT)
	{
		m_Data.m_Int = Value;
	}
	inline
	Variable::Variable( float Value )
	: m_Type(VT_FLOAT)
	{
		m_Data.m_Float = Value;
	}
	inline
	Variable::Variable( const wchar_t* Value )
	: m_Type(VT_STRING)
	{
		m_Data.m_String = Value;
	}
	inline
	Variable::Variable( const Variable& Value )
	: m_Type(Value.m_Type)
	{
		m_Data = Value.m_Data;
	}

	//-------------------------------------------------------------------------
	inline
	const Variable& Variable::operator =( const Variable& Value )
	{
		m_Type = Value.m_Type;
		m_Data = Value.m_Data;
		return *this;
	}

	//-------------------------------------------------------------------------
	inline
	Variable::ValueType Variable::GetType() const
	{
		return m_Type;
	}

	//-------------------------------------------------------------------------
	inline
	bool Variable::GetBoolean() const
	{
		assert( VT_BOOLEAN == m_Type );
		return m_Data.m_Boolean;
	}
	inline
	int Variable::GetInt() const
	{
		assert( VT_INT == m_Type );
		return m_Data.m_Int;
	}
	inline
	float Variable::GetFloat() const
	{
		assert( VT_FLOAT == m_Type );
		return m_Data.m_Float;
	}
	inline
	const wchar_t* Variable::GetString() const
	{
		assert( VT_STRING == m_Type );
		return m_Data.m_String;
	}

	//-------------------------------------------------------------------------
	inline
	Variable::SetUndefined()
	{
		m_Type = VT_UNDEFINED;
	}
	inline
	Variable::SetNull()
	{
		m_Type = VT_NULL;
	}
	inline
	Variable::SetBoolean( bool Value )
	{
		m_Type = VT_BOOLEAN;
	}
	inline
	Variable::SetInt( int Value )
	{
		m_Type = VT_INT;
	}
	inline
	Variable::SetFloat( float Value )
	{
		m_Type = VT_FLOAT;
	}
	inline
	Variable::SetString( const wchar_t* Value )
	{
		m_Type = VT_STRING;
	}

}