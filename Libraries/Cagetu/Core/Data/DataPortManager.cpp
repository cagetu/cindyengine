#include "DataPortManager.h"

namespace Core
{
	DataPortManager* DataPortManager::Instance()
	{
		static DataPortManager instance;
		return &instance;
	}

	//-----------------------------------------------------------------------------
	DataPortManager::DataPortManager()
	{
		memset(&m_Dataports, 0, sizeof(void*) * MAX_DATAPORTS);
		memset(&m_DataportsMangled, 0, sizeof(unsigned int) * MAX_DATAPORTS);
		memset(&m_DataportsRefCount, 0, sizeof(unsigned int) * MAX_DATAPORTS);

		m_NumDataports = 0;
	}

	//-----------------------------------------------------------------------------
	DataportReturn DataPortManager::Add( const String& Name, void* Ptr, unsigned int ID )
	{
		unsigned int hash = *((unsigned int*)(Name.c_str()));	// simple hash function;

		if (m_NumDataports < MAX_DATAPORTS)
		{
			if (!Name.empty() && Name != L"")
			{
				ID ^= hash;
			}

			for (unsigned int i=0; i<m_NumDataports; ++i)
			{
				if (m_DataportsMangled[i] == ID)
					return DATAPORT_ERROR_NAMECLASH;
			}

			m_Dataports[m_NumDataports] = Ptr;
			m_DataportsMangled[m_NumDataports] = ID;
			m_DataportsRefCount[m_NumDataports] = 0;

			m_NumDataports++;

			return DATAPORT_SUCCESS;
		}
		else
		{
			return DATAPORT_ERROR_NOMOREDATAPORTSLOTS;
		}
	}

	DataportReturn DataPortManager::Remove( void* Ptr, bool Check )
	{
		for (unsigned int i=0; i<m_NumDataports; ++i)
		{
			if (m_Dataports[i] == Ptr)
			{
				m_NumDataports--;
				m_Dataports[i] = m_Dataports[m_NumDataports];
				m_DataportsMangled[i] = m_DataportsMangled[m_NumDataports];
				m_DataportsRefCount[i] = m_DataportsRefCount[m_NumDataports];

				m_Dataports[m_NumDataports] = 0;
				m_DataportsMangled[m_NumDataports] = 0;

				if (m_DataportsRefCount[i] != 0)
				{
					return DATAPORT_WARNING_BEINGREFERENCED;
				}

				return DATAPORT_SUCCESS;
			}
		}
		return DATAPORT_ERROR_ENTRYNOTFOUND;
	}

	//-----------------------------------------------------------------------------
	void* DataPortManager::Attach( const String& Name, unsigned int ID )
	{
		if (!Name.empty() && Name != L"")
		{
			unsigned int hash = *((unsigned int*)(Name.c_str()));
			ID ^= hash;
		}

		for (unsigned int i=0; i<m_NumDataports; ++i)
		{
			if (m_DataportsMangled[i] == ID)
			{
				m_DataportsRefCount[i]++;
				return m_Dataports[i];
			}
		}

		return 0;
	}

	DataportReturn DataPortManager::Detach( void* Ptr )
	{
		for (unsigned int i=0; i<m_NumDataports; ++i)
		{
			if (m_Dataports[i] == Ptr)
			{
				if (m_DataportsRefCount[i] > 0)
				{
					m_DataportsRefCount[i]--;
					return DATAPORT_SUCCESS;
				}
				else
				{
					return DATAPORT_ERROR_NOTATTACHED;
				}
			}
		}
		return DATAPORT_ERROR_ENTRYNOTFOUND;
	}

	//-----------------------------------------------------------------------------
	DataportReturn DataPortManager::Poll( void* Ptr )
	{
		for (unsigned int i=0; i<m_NumDataports; ++i)
		{
			if (m_Dataports[i] == Ptr)
			{
				if (m_DataportsRefCount[i] > 0)
				{
					return DATAPORT_SUCCESS;
				}
				else
				{
					return DATAPORT_ERROR_REFCOUNTZERO;
				}
			}
		}

		return DATAPORT_ERROR_ENTRYNOTFOUND;
	}
}