#pragma once

#include "DataPortManager.h"

namespace Core
{
	#define FUNCPOINTER_TYPE unsigned int 

	template <class T>
	FUNCPOINTER_TYPE GetID()
	{
		typedef FUNCPOINTER_TYPE (*TempFunc)();

		TempFunc FuncPtr = GetID<T>;
		FUNCPOINTER_TYPE retID = (FUNCPOINTER_TYPE)FuncPtr;
		return retID;
	}

	//-----------------------------------------------------------------------------
	/**	@class		DataPort
		@author		cagetu

		@brief		http://www.gamasutra.com/features/20061116/linklater_01.shtml
	*/
	//-----------------------------------------------------------------------------
	template <class T>
	class DataPort
	{
	public:
		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		int				Register( const String& Name );
		int				Unregister( bool Check = true );

		DataPort<T>*	Attach( const String& Name );
		int				Detach();

		int				Poll();

		unsigned int	GetRefCount() const;
		
		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		T				m_Data;

	protected:
		unsigned int	m_RefCount;
	};
}

#include "DataPort.inl"