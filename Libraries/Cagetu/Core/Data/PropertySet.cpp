//================================================================
// File:               : PropertySet.cpp
// Related Header File : PropertySet.h
// Original Author     : changhee
//================================================================
#include "PropertySet.h"

namespace Core
{
	PropertySet::PropertySet()
	{
	}
	PropertySet::~PropertySet()
	{
		PropertyIter iend = m_Properties.end();
		for (PropertyIter i=m_Properties.begin(); i!=iend; ++i)
			delete (i->second);
		m_Properties.clear();
	}

	//-------------------------------------------------------------------------
	/** @brief	Property찾기 */
	//-------------------------------------------------------------------------
	Property* PropertySet::Lookup( const String& Name )
	{
		PropertyIter iter = m_Properties.find( Name );
		if (m_Properties.end() == iter)
			return NULL;

		return iter->second;
	}

	//-------------------------------------------------------------------------
	/** @brief	 등록 */
	//-------------------------------------------------------------------------
	void PropertySet::Register( const String& Name, int* Value )
	{
		Property* newProperty = new Property( Name, Value );
		m_Properties.insert( PropertyList::value_type( Name, newProperty ) );
	}

	void PropertySet::Register( const String& Name, float* Value )
	{
		Property* newProperty = new Property( Name, Value );
		m_Properties.insert( PropertyList::value_type( Name, newProperty ) );
	}

	void PropertySet::Register( const String& Name, bool* Value )
	{
		Property* newProperty = new Property( Name, Value );
		m_Properties.insert( PropertyList::value_type( Name, newProperty ) );
	}

	void PropertySet::Register( const String& Name, String* Value )
	{
		Property* newProperty = new Property( Name, Value );
		m_Properties.insert( PropertyList::value_type( Name, newProperty ) );
	}
	
	//-------------------------------------------------------------------------
	/** @brief	 값 설정 */
	//-------------------------------------------------------------------------
	bool PropertySet::Set( const String& Name, int Value )
	{
		Property* prop = Lookup( Name );
		if (!prop)
			return false;

		return prop->Set( Value );
	}

	bool PropertySet::Set( const String& Name, float Value )
	{
		Property* prop = Lookup( Name );
		if (!prop)
			return false;

		return prop->Set( Value );
	}

	bool PropertySet::Set( const String& Name, bool Value )
	{
		Property* prop = Lookup( Name );
		if (!prop)
			return false;

		return prop->Set( Value );
	}

	bool PropertySet::Set( const String& Name, const String& Value )
	{
		Property* prop = Lookup( Name );
		if (!prop)
			return false;

		return prop->Set( Value );
	}

}
