//================================================================
// File:               : Property.inl
// Related Header File : Property.h
// Original Author     : changhee
//================================================================
namespace Core
{
	inline
	Property::Property()
	{
	}
	inline
	Property::Property( const String& Name )
		: m_Name(Name)
	{
		m_Type = EMPTY;
	}
	inline
	Property::Property( const String& Name, int* value )
		: m_Name(Name)
	{
		m_Data.m_Int = value;
		m_Type = INT;
	}
	inline
	Property::Property( const String& Name, float* value )
		: m_Name(Name)
	{
		m_Data.m_Float = value;
		m_Type = FLOAT;
	}
	inline
	Property::Property( const String& Name, bool* value )
		: m_Name(Name)
	{
		m_Data.m_Boolean = value;
		m_Type = BOOLEAN;
	}
	inline
	Property::Property( const String& Name, String* value )
		: m_Name(Name)
	{
		m_Data.m_String = value;
		m_Type = STRING;
	}

	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------
	inline
	Property& Property::Copy( const Property& Source )
	{
		m_Data = Source.m_Data;
		m_Type = Source.m_Type;
		m_Name = Source.m_Name;

		return *this;
	}

	inline
	Property& Property::operator= ( const Property& Source )
	{
		return Copy( Source );
	}

	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------
	inline
	Property::Type Property::GetType() const
	{
		return m_Type;
	}

	inline
	bool Property::GetBool() const
	{
		assert( m_Type == BOOLEAN );
		return *m_Data.m_Boolean;
	}

	inline
	int Property::GetInt() const
	{
		assert( m_Type == INT );
		return *m_Data.m_Int;
	}

	inline
	float Property::GetFloat() const
	{
		assert( m_Type == FLOAT );
		return *m_Data.m_Float;
	}

	inline
	const String& Property::GetString() const
	{
		assert( m_Type == STRING );
		return *m_Data.m_String;
	}
}