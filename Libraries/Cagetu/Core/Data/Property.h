#pragma once

#include "../Cagetu.h"

namespace Core
{
	//-----------------------------------------------------------------------------
	/**	@class		Property
		@author		cagetu

		@brief		Game Programming Gems 2 권 - 범용 C++ 멤버 접근을 위한 클래스
					(comment)
						이론적으로는 프로젝트마다 매번 동일한 코드를 다시 작성하지
						않을 수 있도록, 재사용이 가능한 핵심 편집기 모듈을 개발할 수
						있어야 한다. 그러나 게임마다 서로 다른 객체들이 존재한다는 점을 생각하면,
						편집 목적으로 표시되어야 하는 객체가 어떤 것인지를 편집기 코드의 재작성
						없이도 편집기가 인식할 수 있도록 만드는 것은 어려운 문제라 할 수 있다.

						이를 해결하기 위해 필요한 것은 클래스의 내부에 대한 접근을 가능하게 하는
						범용 객체 인터페이스 이다. Boland C++ Builder가 제공하는 property 참고..

					임의의 자료형으로 확장이 가능하다. 속성들은 오직 원래 데이터에 대한 포인터들만 저장한다.
					또한 속성은 자신의 고유한 객체들을 선언하지 않으며, 자신을 위한 메모리도 할당하지 않으므로,
					속성을 통해서 객체를 다룬다는 것은 원래 데이터의 메모리에 담긴 내용을 다룬다는 의미이다.
					Set함수를 통해서 어떠한 값을 설정하면 자동적으로 속성의 형이 결정된다.
	*/
	//-----------------------------------------------------------------------------
	class Property
	{
	public:
		enum Type
		{
			EMPTY,
			INT,
			FLOAT,
			BOOLEAN,
			STRING,
		};

	public:
		Property();
		Property( const String& Name );
		Property( const String& Name, int* value );
		Property( const String& Name, float* value );
		Property( const String& Name, bool* value );
		Property( const String& Name, String* value );

		Type			GetType() const;

		void			SetName( const String& Name );
		const String&	GetName() const;

		bool			Set( int value );
		bool			Set( float value );
		bool			Set( bool value );
		bool			Set( const String& value );
		bool			SetUnknownValue( const String& value );

		bool			GetBool() const;
		int				GetInt() const;
		float			GetFloat() const;
		const String&	GetString() const;

	protected:
		union Data
		{
			int*		m_Int;
			float*		m_Float;
			bool*		m_Boolean;
			String*		m_String;
		};

		Data		m_Data;
		Type		m_Type;
		String		m_Name;

		Property& Copy( const Property& Source );
		Property& operator =( const Property& Source );
	};
}

#include "Property.inl"