namespace Core
{
	//-----------------------------------------------------------------------------
	template <class T>
	int DataPort<T>::Register( const String& Name )
	{
		FUNCPOINTER_TYPE id = GetID<T>();
		int retVal = DataPortManager::Instance()->Add( Name, (void*)this, id );
		m_RefCount = 0;
		return retVal;
	}

	template <class T>
	int DataPort<T>::Unregister( bool Check )
	{
		return DataPortManager::Instance()->Remove( (void*)this, Check );
	}

	//-----------------------------------------------------------------------------
	template <class T>
	DataPort<T>* DataPort<T>::Attach( const String& Name )
	{
		FUNCPOINTER_TYPE id = GetID<T>();
		DataPort<T>* ret = (DataPort<T>*)DataPortManager::Instance()->Attach( Name, ID );
		if (ret)
		{
			ret->m_RefCount++;
		}
		return ret;
	}
	template <class T>
	int DataPort<T>::Detach()
	{
		int ret = DataPortManager::Instance()->Detach( (void*)this );
		if (ret == DATAPORT_SUCCESS)
		{
			this->m_RefCount--;
		}
		return ret;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	int DataPort<T>::Poll()
	{
		return DataPortManager::Instance()->Poll( (void*)this );
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline
	unsigned int DataPort<T>::GetRefCount() const
	{
		return m_RefCount;
	}
}