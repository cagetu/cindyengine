
#include "Property.h"

namespace Core
{
	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------
	bool Property::Set( int value )
	{
		if (m_Type == INT)
		{
			*m_Data.m_Int = value;
			return true;
		}
		return false;
	}

	bool Property::Set( float value )
	{
		if (m_Type == FLOAT)
		{
			*m_Data.m_Float = value;
			return true;
		}
		return false;
	}

	bool Property::Set( bool value )
	{
		if (m_Type == BOOLEAN)
		{
			*m_Data.m_Boolean = value;
			return true;
		}
		return false;
	}

	bool Property::Set( const String& value )
	{
		if (m_Type == STRING)
		{
			*m_Data.m_String = value;
			return true;
		}
		return false;
	}

	bool Property::SetUnknownValue( const String& value )
	{
		bool return_value = true;
		switch (m_Type)
		{
		case INT:
			*m_Data.m_Int = _wtoi(value.c_str());
			break;
		case FLOAT:
			*m_Data.m_Float = (float)_wtof(value.c_str());
			break;
		case STRING:
			*m_Data.m_String = value;
			break;
		case BOOLEAN:
			*m_Data.m_Boolean = (value.compare(L"true") == 0) || (value.compare(L"TRUE") == 0);
			break;
		default:
			return_value = false;
			break;
		}

		return return_value;
	}

}
