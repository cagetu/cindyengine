#pragma once

#include "SubSystem.h"

namespace Core
{
	//==================================================================
	/** Component Manager
		@author		cagetu
		@remarks	Component 관리자

					Component들은 SubSystem에 의해 같은 타입별로 정리되어 있고,
					SubSystem은 Component 관리자에 의해 관리된다.
	*/
	//==================================================================
	class ComponentManager
	{
		friend class Entity;
	public:
		// Component
		ComPtr			GetComponent( const EntityID& OwnerID, const TypeID& CategoryID );

		// System
		void			AddSystem( ISubSystem* pSystem );
		ISubSystem*		GetSystem( const TypeID& Type );

		// Message
		void			SendSyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg );
		void			SendSyncMessage( const EntityID& OwnerID, const TypeID& Type, Message::IMsg* pMsg );

		void			SendASyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg );
		void			SendASyncMessage( const EntityID& OwnerID, const TypeID& Type, Message::IMsg* pMsg );

		void			BoardcastSyncMessage( Message::IMsg* pMsg );
		void			BoardcastASyncMessage( Message::IMsg* pMsg );

		// Singleton
		static ComponentManager*	Instance();
	private:
		typedef std::vector<ISubSystem*>	SystemList;
		typedef SystemList::iterator		SystemIter;

		SystemList		m_SubSystems;

		//-------------------------------------------------------------------------
		//	Methods
		//-------------------------------------------------------------------------
		ComponentManager();
		~ComponentManager();

		// Component
		bool			AddComponent( IComponent* pComponent );
		void			RemoveComponent( const EntityID& OwnerID, const TypeID& CategoryID );

		// System
		//void			RemoveSystem( ISubSystem* pSystem );
	};
}

#define ComponentDataBase	Core::ComponentManager::Instance()