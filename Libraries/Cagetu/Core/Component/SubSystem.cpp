//================================================================
// File:               : SubSystem.cpp
// Related Header File : SubSystem.h
// Original Author     : changhee
//================================================================
#include "SubSystem.h"
#include "ComponentManager.h"

namespace Core
{
	//*****************************************************************************
	// Class ISubSystem
	//-----------------------------------------------------------------------------
	// Const/Dest
	ISubSystem::ISubSystem()
		: m_Priority(-1)
	{
	}

	//-----------------------------------------------------------------------------
	/** @brief	SubSystem이 처리할 수 있는 컴포넌트 우선순위
	*/
	//-----------------------------------------------------------------------------
	void ISubSystem::SetPriority( Priority nPriority )
	{
		m_Priority = nPriority;
	}
	Priority ISubSystem::GetPriority() const
	{
		return m_Priority;
	}

	//*****************************************************************************
	// Class CommonSubSystem
	//-----------------------------------------------------------------------------
	// Const/Dest
	CommonSubSystem::CommonSubSystem()
	{
	}
	CommonSubSystem::~CommonSubSystem()
	{
		Clear();
	}
	//-----------------------------------------------------------------------------
	/** @brief	SubSystem이 처리할 수 있는 컴포넌트 카테고리
	*/
	//-----------------------------------------------------------------------------
	void CommonSubSystem::SetTypeID( const TypeID& Type )
	{
		m_TypeID = Type;
	}
	const TypeID& CommonSubSystem::GetTypeID() const
	{
		return m_TypeID;
	}

	//-----------------------------------------------------------------------------
	/** @brief	컴포넌트 등록
				SubSystem에 맞지 않는 타입의 Component가 들어오면 false를 반환
	*/
	//-----------------------------------------------------------------------------
	bool CommonSubSystem::Register( const EntityID& OwnerID, IComponent* pComponent )
	{
		if (m_TypeID != pComponent->GetTypeID())
			return false;

		ComponentList::iterator iter = m_Components.find( OwnerID );
		if (m_Components.end() != iter)
			return false;

		m_Components.insert( ComponentList::value_type( OwnerID, pComponent ) );
		return true;
	}

	//-----------------------------------------------------------------------------
	/** @brief	컴포넌트 해지
				원하는 컴포넌트를 리스트에서 해지 한다.
	*/
	//-----------------------------------------------------------------------------
	void CommonSubSystem::Unregister( const EntityID& OwnerID )
	{
		ComponentList::iterator iter = m_Components.find( OwnerID );
		if (m_Components.end() != iter)
		{
			m_Components.erase( iter );
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	모든 컴포넌트 해지 */
	//-----------------------------------------------------------------------------
	void CommonSubSystem::Clear()
	{
		m_Components.clear();
	}

	//-----------------------------------------------------------------------------
	/** @brief	컴포넌트 찾기 */
	//-----------------------------------------------------------------------------
	IComponent* CommonSubSystem::Find( const EntityID& OwnerID ) const
	{
		ComponentList::const_iterator iter = m_Components.find( OwnerID );
		if (m_Components.end() == iter)
			return NULL;

		return iter->second;
	}

	//-----------------------------------------------------------------------------
	/** @brief	원하는 컴포넌트에 동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	void CommonSubSystem::SendSyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		IComponent* component = Find( OwnerID );
		if (component && component->IsValid(pMsg))
		{
			component->HandleMessage( pMsg );
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	원하는 컴포넌트에 비동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	void CommonSubSystem::SendASyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		IComponent* component = Find( OwnerID );
		if (component && component->IsValid(pMsg))
		{
			component->Post( pMsg );
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	등록되어 있는 모든 컴포넌트에 동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	void CommonSubSystem::BoardcastSyncMessage( Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		ComponentList::iterator iter = m_Components.begin();
		if (iter->second->IsValid(pMsg))
		{
			ComponentList::iterator iend = m_Components.end();
			for (; iter != iend; ++iter)
			{
				iter->second->HandleMessage( pMsg );
			}
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	등록되어 있는 모든 컴포넌트에 비동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	void CommonSubSystem::BoardcastASyncMessage( Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		ComponentList::iterator iter = m_Components.begin();
		if (iter->second->IsValid(pMsg))
		{
			ComponentList::iterator iend = m_Components.end();
			for (; iter != iend; ++iter)
			{
				iter->second->Post( pMsg );
			}
		}
	}
}