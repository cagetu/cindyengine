#pragma once

#include "Component.h"

namespace Core
{
	//==================================================================
	/** Game SubSystem Interface
		@author		cagetu
		@since		2008년 4월 14일
		@remarks	Component들을 실행시키는 시스템
					-Event를 보내고 받을 수 있어야한다.
					Component는 이벤트를 보내지 않고, 생성하여,
					연결된 부모 SubSystem에 전달한다.
					SubSystem은 모든 자식 Component들로 부터 이벤트들을 전달받고,
					다른 SubSystem에게 Event를 보낸다.
					-SubSystem이 다른 SubSystem으로 부터 이벤트를 받았을 때,
					이벤트를 받아야 하는 모든 자식 Component들에게 이벤트를 전달한다.

					이 녀석은 하나의 쓰레드를 할당 받아서 작업을 처리할 수도 있다.
	*/
	//==================================================================
	class ISubSystem : public Foundation::RefCounter
	{
	public:
		ISubSystem();
		virtual ~ISubSystem() {}

		// Type
		virtual const TypeID&	GetTypeID() const abstract;

		// Priority
		virtual void			SetPriority( Priority nPriority );
		virtual Priority		GetPriority() const;

		// Component
		virtual bool			Register( const EntityID& OwnerID, IComponent* pComponent ) abstract;
		virtual void			Unregister( const EntityID& OwnerID ) abstract;
		virtual void			Clear() abstract;

		virtual IComponent*		Find( const EntityID& OwnerID ) const abstract;

		// Messgage
		virtual void			SendSyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg ) abstract;
		virtual void			SendASyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg ) abstract;

		virtual void			BoardcastSyncMessage( Message::IMsg* ) abstract;
		virtual void			BoardcastASyncMessage( Message::IMsg* ) abstract;
		
		// 처리
		virtual void			Process() abstract;
	private:
		Priority	m_Priority;
	};

	typedef Foundation::Pointer<Core::ISubSystem>		SysPtr;

	//==================================================================
	/** Common Game SubSystem
		@author		cagetu
		@remarks	특정 타입에 대해서, 작성되지 않은 SubSystem은
					일반형태의 SubSystem에 담아서 저장한다.
	*/
	//==================================================================
	class CommonSubSystem : public ISubSystem
	{
	public:
		CommonSubSystem();
		virtual ~CommonSubSystem();

		// Type
		void				SetTypeID( const TypeID& Type );
		const TypeID&		GetTypeID() const override;

		// Component
		virtual bool		Register( const EntityID& OwnerID, IComponent* pComponent ) override;
		virtual void		Unregister( const EntityID& OwnerID ) override;
		virtual void		Clear() override;

		virtual IComponent*	Find( const EntityID& OwnerID ) const override;

		// Message
		virtual void		SendSyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg ) override;
		virtual void		SendASyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg ) override;

		virtual void		BoardcastSyncMessage( Message::IMsg* pMsg ) override;
		virtual void		BoardcastASyncMessage( Message::IMsg* pMsg ) override;

		virtual void		Process() override {;}
	protected:
		typedef HashMap<const EntityID, IComponent*>	ComponentList;
		ComponentList		m_Components;

		TypeID		m_TypeID;
	};

	//==================================================================
	/** Game SubSystem
		@author		cagetu
		@remarks	원하는 Component에 대한 리스트를 하기 위해...

					어떻게 저장할까?!
					1. 그냥 리스트로 관리
					2. Entity당 하나의 타입의 component만을 가질 수 있다는 규칙에 의해
					   component들도 entity타입에 따라 관리한다.

				   일단 2번으로 만든다.
	*/
	//==================================================================
	template <class T>
	class SubSystem : public ISubSystem
	{
	public:
		SubSystem();
		virtual ~SubSystem();

		// Type
		const TypeID&		GetTypeID() const override;

		// Component
		virtual bool		Register( const EntityID& OwnerID, IComponent* pComponent ) override;
		virtual void		Unregister( const EntityID& OwnerID ) override;
		virtual void		Clear() override;

		virtual IComponent*	Find( const EntityID& OwnerID ) const override;

		// Message
		virtual void		SendSyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg ) override;
		virtual void		SendASyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg ) override;

		virtual void		BoardcastSyncMessage( Message::IMsg* pMsg ) override;
		virtual void		BoardcastASyncMessage( Message::IMsg* pMsg ) override;
	protected:
		typedef HashMap<const EntityID, T*>		ComponentList;
		ComponentList		m_Components;
	};
}

#include "SubSystem.inl"

//-----------------------------------------------------------------------------
//	Component SubSystem 매크로
//-----------------------------------------------------------------------------
// Header
#define DECLARE_COMPONENT_SYSTEM	\
public: \
	static bool		RegisterSystem(); \
private:

// Body
#define IMPLEMENT_COMPONENT_SYSTEM(classname) \
	bool classname::RegisterSystem() \
	{ \
		Core::ComponentManager::Instance()->AddSystem( new classname() ); \
		return true; \
	}

// Instance
#define REGISTER_COMPONENT_SYSTEM(classname) \
	static bool gs_System##classname = classname::RegisterSystem();