#pragma once

#include "../Message/Handler.h"
#include "../CoreDefines.h"

namespace Core
{
	class Entity;
	class IComponent;
	class ISubSystem;

	typedef Foundation::Pointer<IComponent>	ComPtr;

	//==================================================================
	/** Game Component
		@author		cagetu
		@since		2008년 4월 14일
		@remarks	이상적인 경우, 다른 컴포넌트에 대해 알 필요가 없지만,
					특수한 경우, 속도를 빠르게 하기 위해, 링크를 할 필요가 있다.
					Component 매니져를 통할 수도 있지만, 느리기 때문에, 포인터들을
					하나 이상 저장할 수 있어야 한다.
					즉, Initialize(start) 하는 부분에서 연관성이 있는 Component들을
					참조하고 있다가 사용을 하도록 한다.
	*/
	//==================================================================
	class IComponent : public Message::Port
	{
		//friend class ComponentManager;
		DECLARE_ABSTRACT_CLASS(IComponent);
	public:
		IComponent();
		virtual ~IComponent();

		// Owner Entity
		Entity*					GetOwner() const;

		// Component & Family(Category) ID
		virtual const TypeID&	GetTypeID() const abstract;
		virtual const ComID&	GetComponentID() const abstract;

		// 초기화
		virtual void			Start() abstract;
		virtual void			End() abstract;

		// 사용가능여부
		void					SetEnable( bool bEnable );
		bool					IsEnabled() const;

		// 우선순위
		void					SetPriority( Priority Value );
		Priority				GetPriority() const;

		// Message
		virtual bool			IsValid( Message::IMsg* ) override;
		virtual void			HandleMessage( Message::IMsg* ) override;

		// 업데이트
		//virtual bool			Update() abstract;

		// 의존성
		void					Link( IComponent* pComponent );
		void					Unlink( IComponent* pComponent );
		void					UnlinkAll();
		IComponent*				GetLink( const ComID& ID ) const;

		// 자식
		void					AddChild( IComponent* pComponent );
		void					RemoveChild( IComponent* pComponent );
		void					RemoveChildren();

	protected:
		bool		m_bInitialized;

	private:
		friend class Entity;

		typedef HashMap<ComID, ComPtr>		ComponentList;

		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		Entity*				m_pOwner;

		bool				m_bEnabled;

		Priority			m_Priority;

		ComponentList		m_Children;
		ComponentList		m_Dependencies;

		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		void		SetOwner( Entity* pOwner );
	};
}

#include "Component.inl"
