//================================================================
// File:               : Component.inl
// Related Header File : Component.h
// Original Author     : changhee
//================================================================
namespace Core
{
	//--------------------------------------------------------------
	/** @brief	Owner Entity 설정*/
	//--------------------------------------------------------------
	inline
	void IComponent::SetOwner( Entity* pEntity )
	{
		m_pOwner = pEntity;
	}

	//--------------------------------------------------------------
	/** @brief	Owner Entity 얻어오기 */
	//--------------------------------------------------------------
	inline
	Entity* IComponent::GetOwner() const
	{
		return m_pOwner;
	}

	//--------------------------------------------------------------
	/** @brief	우선 순위 설정 */
	//--------------------------------------------------------------
	inline
	void IComponent::SetPriority( Priority Value )
	{
		m_Priority = Value;
	}
	inline
	Priority IComponent::GetPriority() const
	{
		return m_Priority;
	}

	//--------------------------------------------------------------
	/** @brief	활성화 */
	//--------------------------------------------------------------
	inline
	void IComponent::SetEnable( bool bEnable )
	{
		m_bEnabled = bEnable;
	}
	inline
	bool IComponent::IsEnabled() const
	{
		return m_bEnabled;
	}
}

//-----------------------------------------------------------------------------
//	Component 매크로
//-----------------------------------------------------------------------------
// Component Category 
#define	DECLARE_COMPONENT_TYPE \
public: \
	static const Core::TypeID		ms_TypeID; \
	virtual const Core::TypeID&	GetTypeID() const override	{	return ms_TypeID;		}

#define IMPLEMENT_COMPONENT_TYPE(classname, type) \
	const Core::TypeID	classname::ms_TypeID = type; \

// Component ID
#define DECLARE_COMPONENT_ID \
public: \
	static const std::string	ms_ComponentID;	\
	virtual const Core::ComID&	GetComponentID() const override	{	return ms_ComponentID;	}

#define IMPLEMENT_COMPONENT_ID(classname) \
	const std::string	classname::ms_ComponentID = #classname;
