//================================================================
// File:               : SubSystem.inl
// Related Header File : SubSystem.h
// Original Author     : changhee
//================================================================
namespace Core
{
	//*****************************************************************************
	//	Class SubSystem
	//-----------------------------------------------------------------------------
	//	Implement
	//-----------------------------------------------------------------------------
	template <class T>
	SubSystem<T>::SubSystem()
	{
	}
	template <class T>
	SubSystem<T>::~SubSystem()
	{
		Clear();
	}

	//-----------------------------------------------------------------------------
	/** @brief	SubSystem이 처리할 수 있는 컴포넌트 카테고리
	*/
	//-----------------------------------------------------------------------------
	template <class T>
	const TypeID& SubSystem<T>::GetTypeID() const
	{
		return T::ms_TypeID;
	}

	//-----------------------------------------------------------------------------
	/** @brief	컴포넌트 등록
				SubSystem에 맞지 않는 타입의 Component가 들어오면 false를 반환
	*/
	//-----------------------------------------------------------------------------
	template <class T>
	bool SubSystem<T>::Register( const EntityID& OwnerID, IComponent* pComponent )
	{
		if (T::ms_TypeID != pComponent->GetTypeID())
			return false;

		ComponentList::iterator iter = m_Components.find( OwnerID );
		if (m_Components.end() != iter)
			return false;

		m_Components.insert( ComponentList::value_type( OwnerID, static_cast<T*>(pComponent) ) );
		return true;
	}

	//-----------------------------------------------------------------------------
	/** @brief	컴포넌트 해지
				원하는 컴포넌트를 리스트에서 해지 한다.
	*/
	//-----------------------------------------------------------------------------
	template <class T>
	void SubSystem<T>::Unregister( const EntityID& OwnerID )
	{
		ComponentList::iterator iter = m_Components.find( OwnerID );
		if (m_Components.end() != iter)
		{
			m_Components.erase( iter );
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	모든 컴포넌트 해지 */
	//-----------------------------------------------------------------------------
	template <class T>
	void SubSystem<T>::Clear()
	{
		m_Components.clear();
	}

	//-----------------------------------------------------------------------------
	/** @brief	컴포넌트 찾기 */
	//-----------------------------------------------------------------------------
	template <class T>
	IComponent* SubSystem<T>::Find( const EntityID& OwnerID ) const
	{
		ComponentList::const_iterator iter = m_Components.find( OwnerID );
		if (m_Components.end() == iter)
			return NULL;

		return iter->second;
	}

	//-----------------------------------------------------------------------------
	/** @brief	원하는 컴포넌트에 동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	template <class T>
	void SubSystem<T>::SendSyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		IComponent* component = Find( OwnerID );
		if (component && component->IsValid(pMsg))
		{
			component->HandleMessage( pMsg );
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	원하는 컴포넌트에 비동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	template <class T>
	void SubSystem<T>::SendASyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		IComponent* component = Find( OwnerID );
		if (component && component->IsValid(pMsg))
		{
			component->Post( pMsg );
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	등록되어 있는 모든 컴포넌트에 동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	template <class T>
	void SubSystem<T>::BoardcastSyncMessage( Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		ComponentList::iterator iter = m_Components.begin();
		if (iter->second->IsValid(pMsg))
		{
			ComponentList::iterator iend = m_Components.end();
			for (; iter != iend; ++iter)
			{
				iter->second->HandleMessage( pMsg );
			}
		}
	}

	//-----------------------------------------------------------------------------
	/** @brief	등록되어 있는 모든 컴포넌트에 비동기 메시지 보내기 */
	//-----------------------------------------------------------------------------
	template <class T>
	void SubSystem<T>::BoardcastASyncMessage( Message::IMsg* pMsg )
	{
		if (m_Components.empty())
			return;

		ComponentList::iterator iter = m_Components.begin();
		if (iter->second->IsValid(pMsg))
		{
			ComponentList::iterator iend = m_Components.end();
			for (; iter != iend; ++iter)
			{
				iter->second->Post( pMsg );
			}
		}
	}
}