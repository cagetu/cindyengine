//================================================================
// File:               : ComponentManager.cpp
// Related Header File : ComponentManager.h
// Original Author     : changhee
//================================================================
#include "ComponentManager.h"
#include "SubSystem.h"
#include "../Entity/Entity.h"

namespace Core
{
	//-------------------------------------------------------------------------
	ComponentManager* ComponentManager::Instance()
	{
		static ComponentManager instnace;
		return &instnace;
	}

	//-------------------------------------------------------------------------
	// Const/Dest
	ComponentManager::ComponentManager()
	{
	}
	ComponentManager::~ComponentManager()
	{
		SystemIter iend = m_SubSystems.end();
		for (SystemIter i=m_SubSystems.begin(); i!=iend; ++i)
			delete (*i);
		m_SubSystems.clear();
	}

	//-------------------------------------------------------------------------
	/** @brief	컴포넌트 추가 */
	//-------------------------------------------------------------------------
	bool ComponentManager::AddComponent( IComponent* pComponent )
	{
		ISubSystem* system = GetSystem( pComponent->GetTypeID() );
		if (system)
		{
			return system->Register( pComponent->GetOwner()->GetID(), pComponent );
		}
		else
		{
			CommonSubSystem* commonSystem = new CommonSubSystem();
			commonSystem->SetTypeID( pComponent->GetTypeID() );
			AddSystem( commonSystem );

			return commonSystem->Register( pComponent->GetOwner()->GetID(), pComponent );
		}

		return false;
	}

	//-------------------------------------------------------------------------
	/** @brief	컴포넌트 제거 */
	//-------------------------------------------------------------------------
	void ComponentManager::RemoveComponent( const EntityID& OwnerID, const TypeID& CategoryID )
	{
		ISubSystem* system = GetSystem( CategoryID );
		if (system)
		{
			system->Unregister( OwnerID );
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	컴포넌트를 얻는다. */
	//-------------------------------------------------------------------------
	ComPtr ComponentManager::GetComponent( const EntityID& OwnerID, const TypeID& CategoryID )
	{
		ISubSystem* system = GetSystem( CategoryID );
		if (system == NULL)
			return ComPtr();

		return system->Find( OwnerID );
	}

	//-------------------------------------------------------------------------
	/** @brief	System 등록 */
	//-------------------------------------------------------------------------
	void ComponentManager::AddSystem( ISubSystem* pSystem )
	{
		m_SubSystems.push_back( pSystem );
	}

	//-------------------------------------------------------------------------
	/** @brief	System 등록 해제 */
	//-------------------------------------------------------------------------
	//void ComponentManager::RemoveSystem( ISubSystem* pSystem )
	//{
	//	SystemIter iend = m_SubSystems.end();
	//	for (SystemIter i=m_SubSystems.begin(); i!=iend; ++i)
	//	{
	//		if ((*i)->GetTypeID() == pSystem->GetTypeID())
	//		{
	//			delete (*i);
	//			m_SubSystems.erase(i);
	//			return;
	//		}
	//	}
	//}

	//-------------------------------------------------------------------------
	/** @brief	System을 얻어온다 */
	//-------------------------------------------------------------------------
	ISubSystem* ComponentManager::GetSystem( const TypeID& Type )
	{
		SystemIter iend = m_SubSystems.end();
		for (SystemIter i=m_SubSystems.begin(); i!=iend; ++i)
		{
			if ((*i)->GetTypeID() == Type)
			{
				return (*i);
			}
		}

		return NULL;
	}

	//------------------------------------------------------------------
	/** @brief	Message 보내기 (Sync)*/
	//------------------------------------------------------------------
	void ComponentManager::SendSyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg )
	{
		ISubSystem* system = NULL;
		IComponent* component = NULL;

		SystemIter iend = m_SubSystems.end();
		for (SystemIter i=m_SubSystems.begin(); i!=iend; ++i)
		{
			system = (*i);

			system->SendSyncMessage( OwnerID, pMsg );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Message 보내기 (Async) */
	//------------------------------------------------------------------
	void ComponentManager::SendASyncMessage( const EntityID& OwnerID, Message::IMsg* pMsg )
	{
		ISubSystem* system = NULL;
		IComponent* component = NULL;

		SystemIter iend = m_SubSystems.end();
		for (SystemIter i=m_SubSystems.begin(); i!=iend; ++i)
		{
			system = (*i);

			system->SendASyncMessage( OwnerID, pMsg );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Message 보내기 (Sync)*/
	//------------------------------------------------------------------
	void ComponentManager::SendSyncMessage( const EntityID& OwnerID, const TypeID& Type, Message::IMsg* pMsg )
	{
		ISubSystem* system = GetSystem( Type );
		if (system)
		{
			system->SendSyncMessage( OwnerID, pMsg );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Message 보내기 (Async) */
	//------------------------------------------------------------------
	void ComponentManager::SendASyncMessage( const EntityID& OwnerID, const TypeID& Type, Message::IMsg* pMsg )
	{
		ISubSystem* system = GetSystem( Type );
		if (system)
		{
			system->SendASyncMessage( OwnerID, pMsg );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Message 전체 보내기 (Sync) */
	//------------------------------------------------------------------
	void ComponentManager::BoardcastSyncMessage( Message::IMsg* pMsg )
	{
		ISubSystem* system = NULL;
		IComponent* component = NULL;

		SystemIter iend = m_SubSystems.end();
		for (SystemIter i=m_SubSystems.begin(); i!=iend; ++i)
		{
			system = (*i);

			system->BoardcastSyncMessage( pMsg );
		}
	}

	//------------------------------------------------------------------
	/** @brief	Message 전체 보내기 (ASync) */
	//------------------------------------------------------------------
	void ComponentManager::BoardcastASyncMessage( Message::IMsg* pMsg )
	{
		ISubSystem* system = NULL;
		IComponent* component = NULL;

		SystemIter iend = m_SubSystems.end();
		for (SystemIter i=m_SubSystems.begin(); i!=iend; ++i)
		{
			system = (*i);

			system->BoardcastASyncMessage( pMsg );
		}
	}
}