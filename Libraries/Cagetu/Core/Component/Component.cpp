//================================================================
// File:               : Component.cpp
// Related Header File : Component.h
// Original Author     : changhee
//================================================================
#include "Component.h"
#include "ComponentManager.h"
#include "../Entity/Entity.h"

namespace Core
{
	//=========================================================================
	//IMPLEMENT_RTTI( Core, IComponent, Message::Handler );
	IMPLEMENT_ABSTRACT_CLASS( Core, IComponent, Message::Port, 0 );
	//-------------------------------------------------------------------------
	// Const/Dest
	IComponent::IComponent()
		: m_bEnabled(false)
		, m_bInitialized(false)
	{
	}
	IComponent::~IComponent()
	{
	}

	//-------------------------------------------------------------------------
	/** @brief	�޽���*/
	//-------------------------------------------------------------------------
	bool IComponent::IsValid( Message::IMsg* pMsg )
	{
		return false;
	}
	void IComponent::HandleMessage( Message::IMsg* pMsg )
	{
		return;
	}

	//-------------------------------------------------------------------------
	void IComponent::Link( IComponent* pComponent )
	{
		ComponentList::iterator iter = m_Dependencies.find( pComponent->GetComponentID() );
		if (iter==m_Dependencies.end())
		{
			m_Dependencies.insert( ComponentList::value_type( pComponent->GetComponentID(), pComponent ) );
			return;
		}
	}
	void IComponent::Unlink( IComponent* pComponent )
	{
		ComponentList::iterator iter = m_Dependencies.find( pComponent->GetComponentID() );
		if (iter==m_Dependencies.end())
			return;

		m_Dependencies.erase(iter);
	}
	void IComponent::UnlinkAll()
	{
		m_Dependencies.clear();
	}

	//-------------------------------------------------------------------------
	IComponent* IComponent::GetLink( const ComID& ID ) const
	{
		ComponentList::const_iterator iter = m_Dependencies.find( ID );
		if (iter==m_Dependencies.end())
			return NULL;

		return iter->second;
	}

	//-------------------------------------------------------------------------
	void IComponent::AddChild( IComponent* pComponent )
	{
		ComponentList::iterator iter = m_Children.find( pComponent->GetComponentID() );
		if (iter==m_Dependencies.end())
		{
			m_Children.insert( ComponentList::value_type( pComponent->GetComponentID(), pComponent ) );
			return;
		}
	}
	void IComponent::RemoveChild( IComponent* pComponent )
	{
		ComponentList::iterator iter = m_Children.find( pComponent->GetComponentID() );
		if (iter==m_Children.end())
			return;

		m_Children.erase(iter);
	}
	void IComponent::RemoveChildren()
	{
		m_Children.clear();
	}
}