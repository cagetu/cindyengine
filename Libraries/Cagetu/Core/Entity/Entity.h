#pragma once

#include "../Component/Component.h"
#include "../CoreDefines.h"

namespace Core
{
	//==================================================================
	/** Entity
		@author		cagetu
		@since		2008년 4월 14일
		@remarks	Game Object라고 불러도 된다.
					Game Entity는 월드에 존재하는 Object의 개념이고, 
					Game Object는 실제로 구체화된 것을 의미한다.

					Compotsite 객체들의 새로운 시스템과 게임 코드와의
					다리 역할을 할 수 있는 인터페이스 객체가 되어야...

					Component의 정렬 순서가 중요할 수 있다. Update가 순차적으로
					불리워 진다면, 그 순서에 맞게 기능이 적용되어야 하기에...
	*/
	//==================================================================
	class Entity
	{
		friend class EntityManager;
	public:
		typedef HashMap<const TypeID, IComponent*>	ComponentMap;

		typedef ComponentMap::iterator			ComIter;
		typedef ComponentMap::const_iterator	ComConstIter;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		Entity();
		Entity( const EntityID& ID );
		~Entity();

		// ID
		void					SetID( const EntityID& ID );
		const EntityID&			GetID() const;

		// Component
		IComponent*				AddComponent( IComponent* NewCom );
		void					RemoveComponent( const TypeID& ID );
		IComponent*				GetComponent( const TypeID& ID );
		const ComponentMap&		GetComponents() const;
		void					ClearComponents();

		// Handle Message
		void					HandleMessage( const TypeID& CategoryID, Message::IMsg* pMessage );

	private:
		typedef std::vector<IComponent*>	ComponentVector;

		EntityID	m_ID;
		
		ComponentMap	m_Components;
		ComponentVector	m_SortedComponents;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		// Initialize
		void					Start();
		void					End();
	};
}

#include "Entity.inl"
