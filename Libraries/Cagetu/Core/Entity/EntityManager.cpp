#include "EntityManager.h"

namespace Core
{
	EntityManager* EntityManager::ms_pInstance = NULL;
	//-------------------------------------------------------------------------
	EntityManager* EntityManager::Instance()
	{
		assert( ms_pInstance );
		return ms_pInstance;
	}
	// Const/Dest
	EntityManager::EntityManager()
	{
		assert( !ms_pInstance );
		ms_pInstance = this;
	}
	EntityManager::~EntityManager()
	{
		ClearEntities();

		ms_pInstance = 0;
	}

	//-----------------------------------------------------------------------------
	/** @brief	Entity 추가 */
	//-----------------------------------------------------------------------------
	bool EntityManager::AddEntity( Entity* pEntity )
	{
		Entity* entity = GetEntity( pEntity->GetID() );
		if (entity)
			return false;

		m_Entities.insert( EntityList::value_type( pEntity->GetID(), pEntity ) );
		return true;
	}
	
	//-----------------------------------------------------------------------------
	/** @brief	Entity 제거 */
	//-----------------------------------------------------------------------------
	void EntityManager::DeleteEntity( const EntityID& ID )
	{
		EntityList::iterator iter = m_Entities.find( ID );
		if (iter == m_Entities.end())
			return;

		delete iter->second;
		m_Entities.erase( iter );
	}
	
	//-----------------------------------------------------------------------------
	/** @brief	Entity 모두 제거 */
	//-----------------------------------------------------------------------------
	void EntityManager::ClearEntities()
	{
		EntityList::iterator i, iend;
		iend = m_Entities.end();
		for (i=m_Entities.begin(); i != iend; ++i)
			delete (i->second);
		m_Entities.clear();
	}

	//-----------------------------------------------------------------------------
	/** @brief	Entity 모두 제거 */
	//-----------------------------------------------------------------------------
	Entity* EntityManager::GetEntity( const EntityID& ID )
	{
		EntityList::iterator iter = m_Entities.find( ID );
		if (iter == m_Entities.end())
			return NULL;

		return iter->second;
	}

	//-------------------------------------------------------------------------
	/** @brief	Entity의 시작을 알린다. */
	//-------------------------------------------------------------------------
	void EntityManager::Start()
	{
		EntityList::iterator iend = m_Entities.end();
		for (EntityList::iterator i=m_Entities.begin(); i!=iend; ++i)
		{
			i->second->Start();
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	Entity의 끝을 알린다 */
	//-------------------------------------------------------------------------
	void EntityManager::End()
	{
		EntityList::iterator iend = m_Entities.end();
		for (EntityList::iterator i=m_Entities.begin(); i!=iend; ++i)
		{
			i->second->End();
		}
	}

}