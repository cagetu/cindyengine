#pragma once

#include "Entity.h"

namespace Core
{
	//==================================================================
	/** EntityManager
		@author		cagetu
		@remarks	1. Instance�� Entity ����
	*/
	//==================================================================
	class EntityManager
	{
	public:
		EntityManager();
		~EntityManager();

		// Entity
		bool			AddEntity( Entity* pEntity );
		void			DeleteEntity( const EntityID& ID );
		void			ClearEntities();

		Entity*			GetEntity( const EntityID& ID );

		// Initialize
		void			Start();
		void			End();

		// Instance
		static EntityManager*	Instance();
	private:
		//-------------------------------------------------------------------------
		//	Variables
		//-------------------------------------------------------------------------
		typedef HashMap<const EntityID, Entity*>		EntityList;

		EntityList		m_Entities;

		static EntityManager*	ms_pInstance;
	};
}

#define EntityDatabase	Core::EntityManager::Instance()
