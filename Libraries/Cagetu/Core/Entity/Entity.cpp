//================================================================
// File:               : Entity.cpp
// Related Header File : Entity.h
// Original Author     : changhee
//================================================================
#include "Entity.h"
#include "../Component/ComponentManager.h"

namespace Core
{
	// Construct/Destruct
	Entity::Entity()
	{
	}
	Entity::Entity( const EntityID& ID )
		: m_ID(ID)
	{
	}
	Entity::~Entity()
	{
		ClearComponents();
	}

	//-------------------------------------------------------------------------
	/** @brief	Entity는 같은 종류의 Component를 2개 가질 수 없다.*/
	//-------------------------------------------------------------------------
	IComponent* Entity::AddComponent( IComponent* NewCom )
	{
		const TypeID familyID = NewCom->GetTypeID();

		IComponent* oldComponent = NULL;
		ComIter iter = m_Components.find( familyID );
		if (iter != m_Components.end())
		{
			oldComponent = iter->second;
			oldComponent->SetOwner(0);
			ComponentManager::Instance()->RemoveComponent( GetID(), familyID );

			iter->second = NewCom;

			//
			ComponentVector::iterator si = std::find( m_SortedComponents.begin(), m_SortedComponents.end(), oldComponent );
			(*si) = oldComponent;
		}
		else
		{
			m_Components.insert( ComponentMap::value_type( familyID, NewCom ) );
			m_SortedComponents.push_back( NewCom );
		}

		NewCom->SetOwner( this );
		ComponentManager::Instance()->AddComponent( NewCom );
		return oldComponent;
	}

	//-------------------------------------------------------------------------
	/** @brief	Component 제거 */
	//-------------------------------------------------------------------------
	void Entity::RemoveComponent( const TypeID& ID )
	{
		ComponentMap::iterator iter = m_Components.find( ID );
		if (m_Components.end() != iter)
		{
			IComponent* component = iter->second;
			ComponentVector::iterator si = std::find( m_SortedComponents.begin(), m_SortedComponents.end(), component );

			ComponentManager::Instance()->RemoveComponent( GetID(), component->GetTypeID() );

			delete component;
			m_Components.erase( iter );
			m_SortedComponents.erase( si );
			return;
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	모든 Component 제거 */
	//-------------------------------------------------------------------------
	void Entity::ClearComponents()
	{
		End();

		ComponentManager* componentMgr = ComponentManager::Instance();
		IComponent* component = NULL;

		ComIter iend = m_Components.end();
		for (ComIter i=m_Components.begin(); i!=iend; ++i)
		{
			component = i->second;
			componentMgr->RemoveComponent( GetID(), component->GetTypeID() );
			delete component;
		}
		m_Components.clear();
		m_SortedComponents.clear();
	}

	//-------------------------------------------------------------------------
	/** @brief	Entity의 시작을 알린다. */
	//-------------------------------------------------------------------------
	void Entity::Start()
	{
		ComponentVector::iterator i, iend;
		iend = m_SortedComponents.end();
		for (i=m_SortedComponents.begin(); i != iend; ++i)
			(*i)->Start();

		//ComIter iend = m_Components.end();
		//for (ComIter i=m_Components.begin(); i!=iend; ++i)
		//{
		//	i->second->Start();
		//}
	}

	//-------------------------------------------------------------------------
	/** @brief	Entity의 끝을 알린다 */
	//-------------------------------------------------------------------------
	void Entity::End()
	{
		ComponentVector::iterator i, iend;
		iend = m_SortedComponents.end();
		for (i=m_SortedComponents.begin(); i != iend; ++i)
			(*i)->End();
		
		//ComIter iend = m_Components.end();
		//for (ComIter i=m_Components.begin(); i!=iend; ++i)
		//{
		//	i->second->End();
		//}
	}

	//-------------------------------------------------------------------------
	/** @brief	Message 처리 */
	//-------------------------------------------------------------------------
	void Entity::HandleMessage( const TypeID& CategoryID, Message::IMsg* pMessage )
	{
		IComponent* com = GetComponent( CategoryID );
		if (com)
		{
			return com->HandleMessage( pMessage );
		}
	}
}
