//================================================================
// File:               : Entity.inl
// Related Header File : Entity.h
// Original Author     : changhee
//================================================================
namespace Core
{
	//-------------------------------------------------------------------------
	/** @brief	ID 설정 */
	//-------------------------------------------------------------------------
	inline
	void Entity::SetID( const EntityID& ID )
	{
		m_ID = m_ID;
	}

	//-------------------------------------------------------------------------
	/** @brief	ID 얻어오기 */
	//-------------------------------------------------------------------------
	inline
	const EntityID& Entity::GetID() const
	{
		return m_ID;
	}

	//-------------------------------------------------------------------------
	/** @brief	카테고리 종류로 Component 얻어오기 */
	//-------------------------------------------------------------------------
	inline
	IComponent* Entity::GetComponent( const TypeID& ID )
	{
		ComIter iter = m_Components.find(ID);
		if (iter == m_Components.end())
			return NULL;

		return iter->second;
		//return m_Components[ID];
	}

	//-------------------------------------------------------------------------
	/** @brief	모든 컴포넌트 얻어오기 */
	//-------------------------------------------------------------------------
	inline
	const Entity::ComponentMap& Entity::GetComponents() const
	{
		return m_Components;
	}
}