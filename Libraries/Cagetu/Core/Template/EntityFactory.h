#pragma once

#include "EntityTemplate.h"
#include "../Entity/Entity.h"

namespace Core
{
	//==================================================================
	/** Entity Factory
		@author		cagetu
		@since		2008년 4월 14일
		@remarks	Entity를 생산 해준다.
	*/
	//==================================================================
	class EntityFactory
	{
	public:
		// Create
		Entity*			Create( const String& Name, const EntityID& ID = L"" );

		// Load
		bool			Load( const String& FilePath );

		// Template
		bool			AddTemplate( EntityTemplate* Template );
		void			DeleteTemplate( const String& Name );
		void			ClearTemplates();

		EntityTemplate*	GetTemplate( const String& Name );

		// Instance
		static EntityFactory*	Instance();
	protected:
		typedef HashMap<const String, EntityTemplate*>	TemplateList;

		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		TemplateList		m_Templates;

		//-------------------------------------------------------------------------
		//	Methods
		//-------------------------------------------------------------------------
		EntityFactory();
		~EntityFactory();

		void	addTemplate( EntityTemplate* Template );
	};
}

#define EntityFactoryInst	Core::EntityFactory::Instance()