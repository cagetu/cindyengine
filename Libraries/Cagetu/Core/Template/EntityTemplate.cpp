//================================================================
// File:               : EntityTemplate.cpp
// Related Header File : EntityTemplate.h
// Original Author     : changhee
//================================================================
#include "EntityTemplate.h"

namespace Core
{
	// Const/Dest
	EntityTemplate::EntityTemplate( const String& strName )
		: m_strName( strName )
	{
		m_ComponentTemplates.reserve( 16 );
	}
	EntityTemplate::~EntityTemplate()
	{
		Clear();
	}

	//----------------------------------------------------------------
	void EntityTemplate::Add( ComponentTemplate* pTemplate )
	{
		m_ComponentTemplates.push_back( pTemplate );
	}

	//----------------------------------------------------------------
	void EntityTemplate::Remove(const ComID& ID )
	{
		CompTemplateIter iend = m_ComponentTemplates.end();
		for (CompTemplateIter i=m_ComponentTemplates.begin(); i != iend; ++i)
		{
			if ((*i)->GetComponentID() == ID)
			{
				delete (*i);
				m_ComponentTemplates.erase(i);
				return;
			}
		} // for
	}

	//----------------------------------------------------------------
	void EntityTemplate::Clear()
	{
		CompTemplateIter iend = m_ComponentTemplates.end();
		for (CompTemplateIter i=m_ComponentTemplates.begin(); i!=iend; ++i)
			delete (*i);
		m_ComponentTemplates.clear();
	}

	//----------------------------------------------------------------
	ComponentTemplate* EntityTemplate::Find( const ComID& ID ) const
	{
		unsigned int index = 0;

		unsigned int numTemplates = (unsigned int)m_ComponentTemplates.size();
		while (index<numTemplates)
		{
			if (m_ComponentTemplates[index]->GetComponentID() == ID)
				break;

			++index;
		}

		if (index == numTemplates)
			return NULL;

		return m_ComponentTemplates[index];
	}
}
