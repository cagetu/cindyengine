//================================================================
// File:               : ComponentTemplate.cpp
// Related Header File : ComponentTemplate.h
// Original Author     : changhee
//================================================================
#include "ComponentTemplate.h"

namespace Core
{
	// Const
	ComponentTemplate::ComponentTemplate( const ComID& ID )
		: m_ComponentID( ID )
	{
	}

	//-------------------------------------------------------------------------
	/** @brief	Component ���� */
	//-------------------------------------------------------------------------
	IComponent* ComponentTemplate::CreateComponent()
	{
		ComID id("Component@");
		id += m_ComponentID;

		IComponent* component = (IComponent*)Foundation::Factory::Instance()->Create( id.c_str() );

		const PropertyContainer::VarList varList = m_PropertyContainer.GetVariables();
		PropertyContainer::VarConstIter i, iend;
		iend = varList.end();
		for (i=varList.begin(); i!=iend; ++i)
		{
			Foundation::AbstractProperty* prop = component->GetRtti()->GetProperty( i->first.c_str() );
			if (prop)
			{
				switch (prop->GetType())
				{
				case Foundation::PropertyTypeID::Int:
					{
						Foundation::IntProperty* typeProp = (Foundation::IntProperty*)prop;
						typeProp->SetValue( component, i->second.GetInt() );
					}
					break;
				case Foundation::PropertyTypeID::Float:
					{
						Foundation::FloatProperty* typeProp = (Foundation::FloatProperty*)prop;
						typeProp->SetValue( component, i->second.GetFloat() );
					}
					break;
				case Foundation::PropertyTypeID::Boolean:
					{
						Foundation::BoolProperty* typeProp = (Foundation::BoolProperty*)prop;
						typeProp->SetValue( component, i->second.GetBool() );
					}
					break;
				case Foundation::PropertyTypeID::Vector3:
					{
						Foundation::Vector3Property* typeProp = (Foundation::Vector3Property*)prop;
						typeProp->SetValue( component, i->second.GetVector3() );
					}
					break;
				case Foundation::PropertyTypeID::Vector4:
					{
						Foundation::Vector4Property* typeProp = (Foundation::Vector4Property*)prop;
						typeProp->SetValue( component, i->second.GetVector4() );
					}
					break;
				case Foundation::PropertyTypeID::Matrix44:
					{
						Foundation::Matrix44Property* typeProp = (Foundation::Matrix44Property*)prop;
						typeProp->SetValue( component, i->second.GetMatrix44() );
					}
					break;

				case Foundation::PropertyTypeID::String:
					{
						Foundation::StringProperty* typeProp = (Foundation::StringProperty*)prop;
						typeProp->SetValue( component, i->second.GetString() );
					}
					break;
				case Foundation::PropertyTypeID::WString:
					{
						Foundation::WStringProperty* typeProp = (Foundation::WStringProperty*)prop;
						typeProp->SetValue( component, i->second.GetWString() );
					}
					break;
				case Foundation::PropertyTypeID::Object:
					{
					}
					break;
				}
			}
		}
		return component;
	}
}