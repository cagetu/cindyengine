namespace Core
{
	inline
	const ComID& ComponentTemplate::GetComponentID() const
	{
		return m_ComponentID;
	}

	inline
	PropertyContainer* ComponentTemplate::GetPropertyContainer()
	{
		return &m_PropertyContainer;
	}
}