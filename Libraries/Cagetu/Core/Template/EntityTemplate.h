#pragma once

#include "ComponentTemplate.h"
#include <vector>

namespace Core
{
	//==================================================================
	/** Entity Template
		@author		cagetu
		@remarks	같은 종류의 Entity를 생산할 수 있는 틀..
	*/
	//==================================================================
	class EntityTemplate
	{
	public:
		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		typedef std::vector<ComponentTemplate*>		CompTemplateList;
		typedef CompTemplateList::iterator			CompTemplateIter;

		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		EntityTemplate( const String& strName = L"" );
		~EntityTemplate();

		// Name
		void					SetName( const String& strName );
		const String&			GetName() const;

		// ComponentTemplate 
		void					Add( ComponentTemplate* pTemplate );
		void					Remove( const ComID& ID );
		void					Clear();

		ComponentTemplate*		Find( const ComID& ID ) const;
		const CompTemplateList&	GetComponentTemplates()	const;

	private:
		//-------------------------------------------------------------------------
		//	Variables
		//-------------------------------------------------------------------------
		String				m_strName;
		CompTemplateList	m_ComponentTemplates;
	};
}

#include "EntityTemplate.inl"