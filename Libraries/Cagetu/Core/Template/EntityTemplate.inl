//================================================================
// File:               : EntityTemplate.inl
// Related Header File : EntityTemplate.h
// Original Author     : changhee
//================================================================
namespace Core
{
	//--------------------------------------------------------------
	/** @brief	Entity Template 이름 (==Entity Type 이름)*/
	//--------------------------------------------------------------
	inline
	void EntityTemplate::SetName( const String& strName )
	{
		m_strName = strName;
	}
	inline const String& EntityTemplate::GetName() const
	{
		return m_strName;
	}

	//--------------------------------------------------------------
	/** @brief	컴포넌트 템플릿들 얻어오기 */
	//--------------------------------------------------------------
	inline
	const EntityTemplate::CompTemplateList& EntityTemplate::GetComponentTemplates() const
	{
		return m_ComponentTemplates;
	}
}
