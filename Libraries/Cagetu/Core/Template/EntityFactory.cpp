//================================================================
// File:               : EntityFactory.cpp
// Related Header File : EntityFactory.h
// Original Author     : changhee
//================================================================
#include "EntityFactory.h"
#include <MoCommon/External/MoMiniXml.h>
#include <MoCommon/MoStringUtil.h>

namespace Core
{
	// Instance
	//-----------------------------------------------------------------------------
	EntityFactory* EntityFactory::Instance()
	{
		static EntityFactory instance;
		return &instance;
	}
	// Const/Dest
	EntityFactory::EntityFactory()
	{
	}
	EntityFactory::~EntityFactory()
	{
		ClearTemplates();
	}

	//-----------------------------------------------------------------------------
	/** @brief	EntityTemplate을 추가한다. */
	//-----------------------------------------------------------------------------
	bool EntityFactory::AddTemplate( EntityTemplate* Template )
	{
		EntityTemplate* templ = GetTemplate( Template->GetName() );
		if (templ)
			return false;

		addTemplate( Template );
		return true;
	}
	void EntityFactory::addTemplate( EntityTemplate* Template )
	{
		m_Templates.insert( TemplateList::value_type( Template->GetName(), Template ) );
	}

	//-----------------------------------------------------------------------------
	/** @brief	EntityTemplate을 삭제한다. */
	//-----------------------------------------------------------------------------
	void EntityFactory::DeleteTemplate( const String& Name )
	{
		TemplateList::iterator iter = m_Templates.find( Name );
		if (iter == m_Templates.end())
			return;

		delete iter->second;
		m_Templates.erase( iter );
	}

	//-----------------------------------------------------------------------------
	/** @brief	모든 EntityTemplate을 제거한다. */
	//-----------------------------------------------------------------------------
	void EntityFactory::ClearTemplates()
	{
		TemplateList::iterator i, iend;
		iend = m_Templates.end();
		for (i=m_Templates.begin(); i != iend; ++i)
			delete (i->second);
		m_Templates.clear();
	}

	//-----------------------------------------------------------------------------
	/** @brief	EntityTemplate을 얻어온다 */
	//-----------------------------------------------------------------------------
	EntityTemplate* EntityFactory::GetTemplate( const String& Name )
	{
		TemplateList::iterator iter = m_Templates.find( Name );
		if (iter == m_Templates.end())
			return NULL;

		return iter->second;
	}

	//-----------------------------------------------------------------------------
	/** @brief	Entity 생성 */
	//-----------------------------------------------------------------------------
	Entity* EntityFactory::Create( const String& Name, const EntityID& ID )
	{
		EntityTemplate* entityTemplate = GetTemplate( Name );
		if (NULL == entityTemplate)
			return NULL;

		Entity* newEntity = new Entity( ID );

		EntityTemplate::CompTemplateList componentTemplates = entityTemplate->GetComponentTemplates();
		unsigned int size = (unsigned int)componentTemplates.size();
		for (unsigned int index=0; index<size; ++index)
		{
			newEntity->AddComponent( componentTemplates[index]->CreateComponent() );
		}
		return newEntity;
	}

	//-----------------------------------------------------------------------------
	/** @brief	Entity 정보를 읽는다. */
	//-----------------------------------------------------------------------------
	bool EntityFactory::Load( const String& FilePath )
	{
		using namespace MoCommon;

		MoMiniXML doc;
		if (!doc.ReadFromFile( FilePath.c_str() ))
			return false;

		// Root
		MoMiniXML::CNode xmlRoot = doc.GetRoot();
		if (xmlRoot == NULL)
			return false;

		if (0 != wcscmp(xmlRoot.GetName(), L"Template"))
			return false;

		String name, id;
		EntityTemplate* entityTemplate;
		ComponentTemplate* componentTemplate;

		char buffer[128];
		memset( buffer, 0, 128 );

		// EntityNode
		MoMiniXML::CNode xmlEntity = xmlRoot.GetChildNode();
		for (xmlEntity=xmlEntity.Find(L"Entity"); xmlEntity != NULL; xmlEntity=xmlEntity.Find(L"Entity"))
		{
			// name
			name = xmlEntity.GetAttribute( L"name" );
			entityTemplate = this->GetTemplate( name );
			if (entityTemplate)
				continue;

			entityTemplate = new EntityTemplate( name );

			// ComponentNode
			MoMiniXML::CNode xmlComponent = xmlEntity.GetChildNode();
			for (xmlComponent=xmlComponent.Find(L"Component"); xmlComponent != NULL; xmlComponent=xmlComponent.Find(L"Component"))
			{
				id = xmlComponent.GetAttribute( L"id" );

				MoStringUtil::WidecharToMultibyte( id.c_str(), buffer );
				componentTemplate = new ComponentTemplate( buffer );

				PropertyContainer* propContainer = componentTemplate->GetPropertyContainer();

				MoMiniXML::CNode xmlProp = xmlComponent.GetChildNode();
				if (xmlProp != NULL)
				{
					for (xmlProp=xmlProp.Find(L"Property"); xmlProp != NULL; xmlProp=xmlProp.Find(L"Property"))
					{
						id = xmlProp.GetAttribute( L"id" );
						MoStringUtil::WidecharToMultibyte( id.c_str(), buffer );

						String type = xmlProp.GetAttribute( L"type" );

						if (type == L"int")
						{
							int value = _wtoi( xmlProp.GetValue() );

							propContainer->SetInt( buffer, value );
						}
						else if (type == L"float")
						{
							float value = (float)_wtof( xmlProp.GetValue() );

							propContainer->SetFloat( buffer, value );
						}
						else if (type == L"bool" || type == L"boolean")
						{
							int value = (int)_wtoi( xmlProp.GetValue() );
							bool flag = (value == 0) ? false : true;

							propContainer->SetBool( buffer, flag );
						}
						else if (type == L"vector3")
						{
							Math::Vector3 vector;

							MoMiniXML::CNode xmlValue = xmlProp.GetChildNode();
							for (; xmlValue != NULL; xmlValue=xmlValue.Next())
							{
								name = xmlValue.GetName();
								MoStringUtil::LowerCase( name );

								if (name == _T("x"))
								{
									vector.x = (float)_wtof(xmlValue.GetValue());
								}
								else if (name == _T("y"))
								{
									vector.y = (float)_wtof(xmlValue.GetValue());
								}
								else if (name == _T("z"))
								{
									vector.z = (float)_wtof(xmlValue.GetValue());
								}
							}

							propContainer->SetVector3( buffer, vector );
						}
						else if (type == L"color" || type == L"vector4")
						{
							Math::Vector4 vector;

							MoMiniXML::CNode xmlValue = xmlProp.GetChildNode();
							for (; xmlValue != NULL; xmlValue=xmlValue.Next())
							{
								name = xmlValue.GetName();
								MoStringUtil::LowerCase( name );

								if (name == _T("r") || name == _T("x"))
								{
									vector.x = (float)_wtof(xmlValue.GetValue());
								}
								else if (name == _T("g") || name == _T("y"))
								{
									vector.y = (float)_wtof(xmlValue.GetValue());
								}
								else if (name == _T("b") || name == _T("z"))
								{
									vector.z = (float)_wtof(xmlValue.GetValue());
								}
								else if (name == _T("a") || name == _T("w"))
								{
									vector.w = (float)_wtof(xmlValue.GetValue());
								}
							}

							propContainer->SetVector4( buffer, vector );
						}
						else if (type == L"matrix44" || type == L"matrix")
						{
						}
						else if (type == L"string")
						{
							name = xmlProp.GetValue();

							char str[128];
							MoStringUtil::WidecharToMultibyte( name.c_str(), str );

							propContainer->SetString( buffer, str );
						}
						else if (type == L"wstring")
						{
							name = xmlProp.GetValue();

							propContainer->SetWString( buffer, name.c_str() );
						}
						
					} // for(xmlProp...)
				} // if (xmlProp != NULL)

				entityTemplate->Add( componentTemplate );
			}

			this->addTemplate( entityTemplate );
		}

		return true;
	}
}