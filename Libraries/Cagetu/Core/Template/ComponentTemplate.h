#pragma once

#include "../Component/Component.h"
#include "../Property/PropertyContainer.h"

namespace Core
{
	//==================================================================
	/** Component Template
		@author		cagetu
		@since		2008년 4월 14일
		@remarks	같은 종류의 Component를 생산하기 위한 Template 클래스
	*/
	//==================================================================
	class ComponentTemplate
	{
	public:
		ComponentTemplate( const ComID& ID );

		// ComponentID
		const ComID&	GetComponentID() const;

		// Create Component
		IComponent*		CreateComponent();

		//
		PropertyContainer*	GetPropertyContainer();
	protected:
		ComID	m_ComponentID;

		PropertyContainer	m_PropertyContainer;
	};
}

#include "ComponentTemplate.inl"