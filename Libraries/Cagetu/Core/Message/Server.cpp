//================================================================
// File:               : Server.cpp
// Related Header File : Server.h
// Original Author     : changhee
//================================================================
#include "Server.h"
#include "Handler.h"

namespace Message
{
	Server* Server::ms_pInstance = NULL;
	// Const/Dest
	Server::Server()
	{
		assert( ms_pInstance == NULL );
		ms_pInstance = this;

		m_Handlers.reserve( 512 );
	}
	Server::~Server()
	{
		ms_pInstance = 0;
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 처리기 등록 */
	//-------------------------------------------------------------------------
	void Server::Register( Port* pHandler )
	{
		m_Handlers.push_back( pHandler );
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 처리기 해제 */
	//-------------------------------------------------------------------------
	void Server::Unregister( Port* pHandler )
	{
		HandlerList::iterator iter = std::find( m_Handlers.begin(), m_Handlers.end(), pHandler );
		if (iter == m_Handlers.end())
			return;

		m_Handlers.erase( iter );
	}

	//-------------------------------------------------------------------------
	/** @brief	동기 메시지 전송 */
	//-------------------------------------------------------------------------
	void Server::SendSync( Port* pHandler, IMsg* pMsg )
	{
		if (pHandler->IsValid(pMsg))
		{
			pHandler->HandleMessage( pMsg );
		}
	}
	void Server::BroadcastSync( IMsg* pMsg )
	{
		HandlerList::iterator i, iend;
		i = m_Handlers.begin();
		iend = m_Handlers.end();
		for (i=m_Handlers.begin(); i!=iend; ++i)
		{
			if ((*i)->IsValid(pMsg))
			{
				(*i)->HandleMessage( pMsg );
			}
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	비동기 메시지 전송 */
	//-------------------------------------------------------------------------
	void Server::SendASync( Port* pHandler, IMsg* pMsg )
	{
		if (pHandler->IsValid(pMsg))
		{
			pHandler->Post( pMsg );
		}
	}
	void Server::BroadcastASync( IMsg* pMsg )
	{
		HandlerList::iterator i, iend;
		i = m_Handlers.begin();
		iend = m_Handlers.end();
		for (i=m_Handlers.begin(); i!=iend; ++i)
		{
			if ((*i)->IsValid(pMsg))
			{
				(*i)->Post( pMsg );
			}
		}
	}
}