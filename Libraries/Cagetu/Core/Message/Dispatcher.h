#pragma once

#include "Handler.h"

namespace Message
{
	//-----------------------------------------------------------------------------
	/**	@class		Dispatcher
		@author		cagetu

		@brief		메시지 전달자

					Nebula Device2 Mangalore의 Dispatcher 참고

													+------+
												+-->| Port |
											   /    +------+
								+------------+/     +------+
					--- Msg --->| Dispatcher |----->| Port |
								+------------+\     +------+
											   \    +------+
												+-->| Port |
													+------+

					Dispatcher objects usually serve as front end message ports which hide
					a more complex message processing infrastructure underneath.
					Message sent to Dispatcher objects are immediately forwarded to
					Handlers, no messages are kept in the Dispatcher object. Thus,
					a Dispatcher always appears as an empty message Port.

					(C) 2005 RadonLabs GmbH
	*/
	//-----------------------------------------------------------------------------
	class Dispatcher : public Message::Port
	{
		//DECLARE_RTTI;
		DECLARE_CLASS(Dispatcher);
	public:
		Dispatcher();
		virtual ~Dispatcher();

		// 추가/제거
		void	AddPort( Port* );
		void	RemovePort( Port* );

		// Message
		void	HandleMessage( IMsg* ) override;
		void	Post( IMsg* ) override;

		// 유효성 판정
		bool	IsValid( IMsg* pMsg ) override;
		bool	IsEmpty() const override;
	private:
		typedef std::vector<Port*>		PortList;

		PortList			m_Ports;
		unsigned int		m_NumOfPorts;
	};
}