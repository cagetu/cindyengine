//================================================================
// File:               : Dispatcher.cpp
// Related Header File : Dispatcher.h
// Original Author     : changhee
//================================================================
#include "Dispatcher.h"

namespace Message
{
	//=========================================================================
	//IMPLEMENT_RTTI( Message, Dispatcher, Port );
	IMPLEMENT_CLASS( Message, Dispatcher, Port, 0 );
	//-----------------------------------------------------------------------------
	// Const/Dest
	Dispatcher::Dispatcher()
	{
		m_Ports.reserve( 64 );
	}
	Dispatcher::~Dispatcher()
	{
	}

	//-----------------------------------------------------------------------------
	/** @brief	메시지 처리기 추가 */
	//-----------------------------------------------------------------------------
	void Dispatcher::AddPort( Port* pPort )
	{
		m_Ports.push_back( pPort );
	}

	//-----------------------------------------------------------------------------
	/** @brief	메시지 처리기 제거 */
	//-----------------------------------------------------------------------------
	void Dispatcher::RemovePort( Port* pPort )
	{
		PortList::iterator iter = std::find( m_Ports.begin(), m_Ports.end(), pPort );
		if (iter == m_Ports.end())
			return;

		m_Ports.erase( iter );
	}

	//-----------------------------------------------------------------------------
	/** @brief	메시지를 처리한다.  */
	//-----------------------------------------------------------------------------
	void Dispatcher::HandleMessage( IMsg* pMsg )
	{
		PortList::iterator iend = m_Ports.end();
		PortList::iterator i = m_Ports.begin();
		for (; i != iend; ++i)
		{
			if ((*i)->IsValid(pMsg))
			{
				(*i)->HandleMessage( pMsg );
			} // if
		} // for
	}

	//-----------------------------------------------------------------------------
	/** @brief	Post Message */
	//-----------------------------------------------------------------------------
	void Dispatcher::Post( IMsg* pMsg )
	{
		PortList::iterator iend = m_Ports.end();
		PortList::iterator i = m_Ports.begin();
		for (; i != iend; ++i)
		{
			(*i)->Post( pMsg );
		} // for
	}

	//-----------------------------------------------------------------------------
	/** @brief	유효성 판정 */
	//-----------------------------------------------------------------------------
	bool Dispatcher::IsValid( IMsg* pMsg )
	{
		return true;
	}
	bool Dispatcher::IsEmpty() const
	{
		return m_Ports.empty();
	}
}
