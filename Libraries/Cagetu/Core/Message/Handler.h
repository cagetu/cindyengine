#pragma once

#include "Msg.h"

namespace Message
{
	//-----------------------------------------------------------------------------
	/**	@class		Message Port
		@author		cagetu

		@brief		메시지 처리기
					Nebula Device2 의 mangalore 메시지 시스템을 보고 만들다.
	*/
	//-----------------------------------------------------------------------------
	class Port : public Foundation::Object
	{
		DECLARE_RTTI;
	public:
		Port();
		virtual ~Port();

		// Message
		virtual void	HandleMessage( IMsg* ) abstract;

		// Post Message 처리
		virtual void	Post( IMsg* );
		virtual void	HandlePendingMessages();

		// 유효성 판정
		virtual bool	IsValid( IMsg* pMsg ) abstract;
		virtual bool	IsEmpty() const;

	private:
		typedef std::vector<MsgPtr>	MsgList;
		typedef MsgList::iterator	MsgIter;

		MsgList			m_PendingMessages;
	};
}

#include "Handler.inl"