#pragma once

#include "../../Foundation/Object.h"

namespace Message
{
	class Id
	{
	public:
		Id() {}
		bool operator ==( const Id& rhs ) const
		{
			return (this == &rhs);
		}
	};
}

//------------------------------------------------------------------------------
//<@
#define DECLARE_MSGID \
public:\
	static Message::Id Id; \
	virtual const Message::Id& GetId() const; \
private:

#define IMPLEMENT_MSGID(type) \
	Message::Id type::Id; \
	const Message::Id& type::GetId() const { return type::Id; }
//@>
//------------------------------------------------------------------------------

namespace Message
{
	class Port;

	//-----------------------------------------------------------------------------
	/** @class		Message
		@author		cagetu

		@brief		최상위 메시지 클래스
	*/
	//-----------------------------------------------------------------------------
	class IMsg : public Foundation::RefCounter
	{
		DECLARE_MSGID;
	public:
		IMsg();
		virtual ~IMsg();

		// Sync
		void	SendSync( Port* );
		void	BroadcastSync();

		// ASync
		void	SendASync( Port* );
		void	BroadcastASync();
	};
} // namespace Message

typedef Foundation::Pointer<Message::IMsg>	MsgPtr;
