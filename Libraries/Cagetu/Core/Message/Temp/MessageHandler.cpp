#include <BaseHeader.h>

#ifdef __PLANET_WAR__

#include "MessageHandler.h"
#include "MessageServer.h"

namespace PlanetWar
{
	namespace Message
	{
		//-----------------------------------------------------------------------------
		// Const/Dest
		Handler::Handler( const TCHAR* IDName )
			: m_IDName(IDName)
		{
			if (!m_IDName.empty())
				Register();
		}
		Handler::~Handler()
		{
			if (!m_IDName.empty())
				Unregister();
		}

		//-----------------------------------------------------------------------------
		/** @brief	이름 설정 */
		//-----------------------------------------------------------------------------
		void Handler::SetName( const TCHAR* IDName )
		{
			m_IDName = IDName;

			Register();
		}

		//-----------------------------------------------------------------------------
		/** @brief	메시지 처리기 등록/제거 */
		//-----------------------------------------------------------------------------
		void Handler::Register()
		{
			Server::Instance()->Register( m_IDName.c_str(), this );
		}
		void Handler::Unregister()
		{
			Server::Instance()->Unregister( m_IDName.c_str() );
		}

		//-----------------------------------------------------------------------------
		/** @brief	모아서 처리할 녀석을 모은다 */
		//-----------------------------------------------------------------------------
		void Handler::PendingMessage( IMessage* Msg )
		{
			m_PostMessages.push_back( Msg );
		}

		//-----------------------------------------------------------------------------
		/** @brief	모아둔 메시지를 한번에 처리한다

					<중요>처리 타이밍은 각자 핸들러가 상속받는 곳에서 적용...
		*/
		//-----------------------------------------------------------------------------
		void Handler::HandlePendingMessages()
		{
			MsgIter iend = m_PostMessages.end();
			for (MsgIter i=m_PostMessages.begin(); i!=iend; ++i)
			{
				HandleMessage( (*i) );
			}
			m_PostMessages.clear();
		}

	} // namespace Message
} //namespace PlanetWar

#endif //__PLANET_WAR__