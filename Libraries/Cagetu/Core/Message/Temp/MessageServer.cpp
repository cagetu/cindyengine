#include <BaseHeader.h>

#ifdef __PLANET_WAR__

#include "MessageServer.h"
#include "MessageHandler.h"

namespace PlanetWar
{
	namespace Message
	{
		Server* Server::Instance()
		{
			static Server inst;
			return &inst;
		}

		//-----------------------------------------------------------------------------
		// Const/Dest
		Server::Server()
		{
		}
		Server::~Server()
		{
			m_Handlers.clear();
		}

		//-----------------------------------------------------------------------------
		/** @brief	메시지 처리기 등록 */
		//-----------------------------------------------------------------------------
		void Server::Register( const TCHAR* IDName, Handler* pHandler )
		{
			HandlerIter iter = m_Handlers.find( IDName );
			if (m_Handlers.end() == iter)
			{
				m_Handlers.insert( HandlerList::value_type( IDName, pHandler ) );
				return;
			}
		}

		//-----------------------------------------------------------------------------
		/** @brief	메시지 처리기 해제 */
		//-----------------------------------------------------------------------------
		void Server::Unregister( const TCHAR* IDName )
		{
			HandlerIter iter = m_Handlers.find( IDName );
			if (m_Handlers.end() != iter)
			{
				m_Handlers.erase( iter );
				return;
			}
		}

		//-----------------------------------------------------------------------------
		/** @brief	동기 메시지 처리 */
		//-----------------------------------------------------------------------------
		void Server::SendSync( const TCHAR* IDName, IMessage* Msg )
		{
			HandlerIter iter = m_Handlers.find( IDName );
			if (m_Handlers.end() != iter)
			{
				iter->second->HandleMessage( Msg );
			}
		}
		void Server::BroadcastSync( IMessage* Msg )
		{
			HandlerIter i, iend;
			iend = m_Handlers.end();
			for (i=m_Handlers.begin(); i!=iend; ++i)
			{
				if ((i->second)->IsValid(Msg))
					continue;

				(i->second)->HandleMessage( Msg );
			}
		}

		//-----------------------------------------------------------------------------
		/** @brief	비동기 메시지 처리 */
		//-----------------------------------------------------------------------------
		void Server::SendASync( const TCHAR* IDName, IMessage* Msg )
		{
			HandlerIter iter = m_Handlers.find( IDName );
			if (m_Handlers.end() != iter)
			{
				iter->second->PendingMessage( Msg );
			}
		}
		void Server::BroadcastASync( IMessage* Msg )
		{
			HandlerIter i, iend;
			iend = m_Handlers.end();
			for (i=m_Handlers.begin(); i!=iend; ++i)
			{
				if ((i->second)->IsValid(Msg))
					continue;

				(i->second)->PendingMessage( Msg );
			}
		}

	} // namespace Message
} //namespace PlanetWar

#endif //__PLANET_WAR__