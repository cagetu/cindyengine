#pragma once

#include "MessageHandler.h"

namespace PlanetWar
{
	namespace Message
	{
		//-----------------------------------------------------------------------------
		/** @class		Message Server
			@author		cagetu

			@brief
				메시지를 관리하고 원하는 대상에게 전달해주는 클래스이다.
		*/
		//-----------------------------------------------------------------------------
		class Server
		{
			friend class IMessage;
		public:
			Server();
			~Server();

			// Singleton
			static Server* Instance();

			// Handler 등록/해제
			void	Register( const TCHAR*, Handler* );
			void	Unregister( const TCHAR* );

		private:
			typedef stdext::hash_map<tstring, Handler*>	HandlerList;
			typedef HandlerList::iterator				HandlerIter;

			HandlerList		m_Handlers;

			//-----------------------------------------------------------------------------
			//	Methods
			//-----------------------------------------------------------------------------
			void	SendSync( const TCHAR*, IMessage* );
			void	BroadcastSync( IMessage* );

			void	SendASync( const TCHAR*, IMessage* );
			void	BroadcastASync( IMessage* );
		};

	} // namespace Message
} // namespace PlanetWar
