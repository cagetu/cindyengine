#pragma once

#include "../../Foundation/RefCount.h"

namespace Message
{
	class Id
	{
	public:
		Id() {}
		bool operator ==( const Id& rhs ) const
		{
			return (this == &rhs);
		}
	};
}

//------------------------------------------------------------------------------
//<@
#define DECLARE_MSGID \
public:\
	static Message::Id Id; \
	virtual const Message::Id& GetId() const; \
private:

#define IMPLEMENT_MSGID(type) \
	Message::Id type::Id; \
	const Message::Id& type::GetId() const { return type::Id; }
//@>
//------------------------------------------------------------------------------

namespace Message
{
	//-----------------------------------------------------------------------------
	/** @class		Message
		@author		cagetu

		@brief		최상위 메시지 클래스
	*/
	//-----------------------------------------------------------------------------
	class IMessage : public Foundation::RefCounter
	{
		DECLARE_MSGID;
	public:
		IMessage();
		virtual ~IMessage();

		// Sync
		void	SendSync( const char* );
		void	BroadcastSync();

		// ASync
		void	SendASync( const char* );
		void	BroadcastASync();
	};
} // namespace Message

typedef Foundation::Pointer<Message::IMessage>	MsgPtr;
