//================================================================
// File:               : Message.cpp
// Related Header File : Message.h
// Original Author     : changhee
//================================================================
#include "Message.h"
#include "MessageServer.h"

namespace Message
{
	IMPLEMENT_MSGID( IMessage );
	//-----------------------------------------------------------------------------
	// Const/Dest
	IMessage::IMessage()
	{
	}
	IMessage::~IMessage()
	{
	}

	//-----------------------------------------------------------------------------
	/** @brief	동기 메시지 */
	//-----------------------------------------------------------------------------
	void IMessage::SendSync( const char* HandlerID )
	{
		Server::Instance()->SendSync( HandlerID, this );
	}
	void IMessage::BroadcastSync()
	{
		Server::Instance()->BroadcastSync( this );
	}

	//-----------------------------------------------------------------------------
	/** @brief	비동기 메시지 */
	//-----------------------------------------------------------------------------
	void IMessage::SendASync( const char* HandlerID )
	{
		Server::Instance()->SendASync( HandlerID, this );
	}
	void IMessage::BroadcastASync()
	{
		Server::Instance()->BroadcastASync( this );
	}

} // namespace Message
