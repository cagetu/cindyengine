#pragma once

#include "Message.h"

namespace PlanetWar
{
	namespace Message
	{
		//-----------------------------------------------------------------------------
		/** @class		Message Handler
			@author		cagetu

			@brief		메시지 처리
		*/
		//-----------------------------------------------------------------------------
		class Handler
		{
		public:
			explicit Handler( const TCHAR* IDName = _T("") );
			virtual ~Handler();

			// Name
			void			SetName( const TCHAR* IDName );

			// 즉시처리
			virtual void	HandleMessage( IMessage* ) abstract;

			// 모아서 처리
			virtual void	PendingMessage( IMessage* );
			virtual void	HandlePendingMessages();

			// Etc
			virtual bool	IsValid( IMessage* ) abstract;
			virtual bool	IsEmpty() const			{	return m_PostMessages.empty();	}
		protected:
			typedef std::vector<MsgPtr>	MsgList;
			typedef MsgList::iterator	MsgIter;

			MsgList		m_PostMessages;

			tstring		m_IDName;

			void	Register();
			void	Unregister();
		};
	} // namespace Message
} // namespace PlanetWar
