//================================================================
// File:               : Msg.cpp
// Related Header File : Msg.h
// Original Author     : changhee
//================================================================
#include "Msg.h"
#include "Server.h"

namespace Message
{
	IMPLEMENT_MSGID( IMsg );
	//-------------------------------------------------------------------------
	IMsg::IMsg()
	{
	}
	IMsg::~IMsg()
	{
	}

	//-------------------------------------------------------------------------
	void IMsg::SendSync( Port* pHandler )
	{
		Server::Instance()->SendSync( pHandler, this );
	}

	//-------------------------------------------------------------------------
	void IMsg::BroadcastSync()
	{
		Server::Instance()->BroadcastSync( this );
	}

	//-------------------------------------------------------------------------
	void IMsg::SendASync( Port* pHandler )
	{
		Server::Instance()->SendASync( pHandler, this );
	}
	void IMsg::BroadcastASync()
	{
		Server::Instance()->BroadcastASync( this );
	}
}