//================================================================
// File:               : Handler.cpp
// Related Header File : Handler.h
// Original Author     : changhee
//================================================================
#include "Handler.h"

namespace Message
{
	//-------------------------------------------------------------------------
	IMPLEMENT_RTTI( Message, Port, Object );
	//-------------------------------------------------------------------------
	Port::Port()
	{
	}
	Port::~Port()
	{
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 큐에 메시지 등록 */
	//-------------------------------------------------------------------------
	void Port::Post( IMsg* pMsg )
	{
		if (this->IsValid(pMsg))
		{
			m_PendingMessages.push_back( pMsg );
		}
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 큐에 등록된 메시지 처리 */
	//-------------------------------------------------------------------------
	void Port::HandlePendingMessages()
	{
		MsgList::iterator i, iend;
		iend = m_PendingMessages.end();

		for (i=m_PendingMessages.begin(); i!=iend; ++i)
		{
			this->HandleMessage( (*i) );
		}
		m_PendingMessages.clear();
	}
}