#pragma once

#include "../../Cagetu.h"
#include "../../Foundation/RefCount.h"

namespace Message
{
	class Port;
	class IMsg;

	//-----------------------------------------------------------------------------
	/**	@class		Server
		@author		cagetu

		@brief		엄밀히 말하면, Message 관리자..

					Nebula Device2 Mangalore의 Message::Server 참고
	*/
	//-----------------------------------------------------------------------------
	class Server
	{
		friend class IMsg;
	public:
		Server();
		~Server();

		void	Register( Port* pHandler );
		void	Unregister( Port* pHanlder );

		static Server*	Instance();
	private:
		//-------------------------------------------------------------------------
		//	Methods
		//-------------------------------------------------------------------------
		// send syncronous message
		void	SendSync( Port*, IMsg* );
		void	BroadcastSync( IMsg* );

		// send asyncronous message
		void	SendASync( Port*, IMsg* );
		void	BroadcastASync( IMsg* );

		//-------------------------------------------------------------------------
		//	Variables
		//-------------------------------------------------------------------------
		typedef std::vector<Port*>		HandlerList;

		HandlerList			m_Handlers;

		static Server*		ms_pInstance;
	};

	//-------------------------------------------------------------------------
	/** @brief	Singleton */
	//-------------------------------------------------------------------------
	inline
	Server* Server::Instance()
	{
		assert( 0 != ms_pInstance );
		return ms_pInstance;
	}

}