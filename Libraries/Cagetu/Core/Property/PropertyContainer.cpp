//================================================================
// File:               : PropertyContainer.cpp
// Related Header File : PropertyContainer.h
// Original Author     : changhee
//================================================================
#include "PropertyContainer.h"

//=========================================================================
//	Class PropertyContainer
//=========================================================================
// Const/Dest
PropertyContainer::PropertyContainer()
{
}
PropertyContainer::~PropertyContainer()
{
	Clear();
}

//-------------------------------------------------------------------------
/** @brief	추가 */
//-------------------------------------------------------------------------
void PropertyContainer::Add( const std::string& Name, const CnVariable& Var )
{
	VarIter iter = m_Variables.find( Name );
	if (m_Variables.end() == iter)
	{
		m_Variables.insert( VarList::value_type( Name, Var ) );
	}
	else
	{
		iter->second = Var;
	}
}

//-------------------------------------------------------------------------
/** @brief	제거 */
//-------------------------------------------------------------------------
void PropertyContainer::Remove( const std::string& Name )
{
	VarIter iter = m_Variables.find( Name );
	if (m_Variables.end() == iter)
		return;

	m_Variables.erase( iter );
}

//-------------------------------------------------------------------------
/** @brief	모두 제거 */
//-------------------------------------------------------------------------
void PropertyContainer::Clear()
{
	m_Variables.clear();
}

//-------------------------------------------------------------------------
/** @brief	찾기 */
//-------------------------------------------------------------------------
CnVariable* PropertyContainer::Get( const std::string& Name )
{
	VarIter iter = m_Variables.find( Name );
	if (m_Variables.end() == iter)
		return NULL;

	return &(iter->second);
}
const PropertyContainer::VarList& PropertyContainer::GetVariables() const
{
	return m_Variables;
}

//-------------------------------------------------------------------------
/** @brief	Int */
//-------------------------------------------------------------------------
void PropertyContainer::SetInt( const std::string& Name, int Value )
{
	Add( Name, CnVariable(Value) );
}
int PropertyContainer::GetInt( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetInt();
}

//-------------------------------------------------------------------------
/** @brief	Bool */
//-------------------------------------------------------------------------
void PropertyContainer::SetBool( const std::string& Name, bool Value )
{
	Add( Name, CnVariable(Value) );
}
bool PropertyContainer::GetBool( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetBool();
}

//-------------------------------------------------------------------------
/** @brief	Float */
//-------------------------------------------------------------------------
void PropertyContainer::SetFloat( const std::string& Name, float Value )
{
	Add( Name, CnVariable(Value) );
}
float PropertyContainer::GetFloat( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetFloat();
}

//-------------------------------------------------------------------------
/** @brief	String */
//-------------------------------------------------------------------------
void PropertyContainer::SetString( const std::string& Name, const char* Value )
{
	Add( Name, CnVariable(Value) );
}
const char* PropertyContainer::GetString( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetString();
}

//-------------------------------------------------------------------------
/** @brief	WString */
//-------------------------------------------------------------------------
void PropertyContainer::SetWString( const std::string& Name, const wchar_t* Value )
{
	Add( Name, Cindy::CnVariable(Value) );
}
const wchar_t* PropertyContainer::GetWString( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetWString();
}

//-------------------------------------------------------------------------
/** @brief	Vector3 */
//-------------------------------------------------------------------------
void PropertyContainer::SetVector3( const std::string& Name, const Math::Vector3& Value )
{
	Add( Name, CnVariable(Value) );
}
const Math::Vector3& PropertyContainer::GetVector3( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetVector3();
}

//-------------------------------------------------------------------------
/** @brief	Vector4 */
//-------------------------------------------------------------------------
void PropertyContainer::SetVector4( const std::string& Name, const Math::Vector4& Value )
{
	Add( Name, CnVariable(Value) );
}
const Math::Vector4& PropertyContainer::GetVector4( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetVector4();
}

//-------------------------------------------------------------------------
/** @brief	Matrix4x4 */
//-------------------------------------------------------------------------
void PropertyContainer::SetMatrix44( const std::string& Name, const Math::Matrix44& Value )
{
	Add( Name, CnVariable(Value) );
}
const Math::Matrix44& PropertyContainer::GetMatrix44( const std::string& Name )
{
	Cindy::CnVariable* var = Get( Name );
	assert( var );

	return var->GetMatrix44();
}
