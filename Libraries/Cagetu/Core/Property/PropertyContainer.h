#pragma once

#include "../../Cagetu.h"
#include <Cindy/Util/CnVariable.h>

//-----------------------------------------------------------------------------
/**	@class		Container
	@author		cagetu

	@brief		Attribute 컨테이너..
*/
//-----------------------------------------------------------------------------
class PropertyContainer
{
public:
	typedef HashMap<const std::string, CnVariable>	VarList;
	typedef VarList::iterator						VarIter;
	typedef VarList::const_iterator					VarConstIter;

	//-----------------------------------------------------------------------------
	//	Methods
	//-----------------------------------------------------------------------------
	PropertyContainer();
	virtual ~PropertyContainer();

	// 'Int'
	void				SetInt( const std::string& Name, int Value );
	int					GetInt( const std::string& Name );

	// 'Boolean'
	void				SetBool( const std::string& Name, bool Value );
	bool				GetBool( const std::string& Name );

	// 'Float'
	void				SetFloat( const std::string& Name, float Value );
	float				GetFloat( const std::string& Name );

	// 'std::string'
	void				SetString( const std::string& Name, const char* Value );
	const char*			GetString( const std::string& Name );

	// 'WString'
	void				SetWString( const std::string& Name, const wchar_t* Value );
	const wchar_t*		GetWString( const std::string& Name );

	// 'Vector3'
	void				SetVector3( const std::string& Name, const Math::Vector3& Value );
	const Math::Vector3&	GetVector3( const std::string& Name );

	// 'Vector4'
	void				SetVector4( const std::string& Name, const Math::Vector4& Value );
	const Math::Vector4&	GetVector4( const std::string& Name );

	// 'Matrix44'
	void				SetMatrix44( const std::string& Name, const Math::Matrix44& Value );
	const Math::Matrix44&	GetMatrix44( const std::string& Name );
	
	// 제거
	void				Remove( const std::string& Name );
	void				Clear();

	// 찾기
	CnVariable*			Get( const std::string& Name );
	const VarList&		GetVariables() const;

private:
	VarList		m_Variables;

	//-----------------------------------------------------------------------------
	//	Methods
	//-----------------------------------------------------------------------------
	// 추가
	void	Add( const std::string& Name, const CnVariable& Var );
};
