#ifndef __RDMAX70MESH_H__
#define __RDMAX70MESH_H__

#pragma once

#include "RdMax70Node.h"

struct RdUV
{
	float U;
	float V;

	RdUV() : U( 0.0f ), V( 0.0f )	{}
	RdUV( float u, float v )	{ U = u;	V = v; }

	inline RdUV operator -(const RdUV&) const;
};
inline RdUV RdUV::operator-(const RdUV& b) const {
	return(RdUV(V-b.U,V-b.V));
	}

struct RdVertexBlend
{
	ushort	BoneID;
	float	Weight;

	RdVertexBlend() : BoneID( 0xffff ), Weight( 0.0f )	{}
};

struct RdVertex
{
	Point3			Pos;
	Point3			Normal;
	Point3			Color;
	Point3			Tangent;
	Point3			Binormal;

	RdUV			UV;				//!< 기본 uv
	RdUV			UV2;			//!< Render To Texture 채널 정보

	BYTE			Link;
	RdVertexBlend	Blend[4];

	int				OrgId;
	ulong			SmGroup;

	RdVertex() : Link( 0 )
	{
		Pos.Set( 0.0f, 0.0f, 0.0f );
		Normal.Set( 0.0f, 0.0f, 0.0f );	
		Color.Set( 1.0f, 1.0f, 1.0f );
		Tangent.Set( 0.0f, 0.0f, 0.0f );
		Binormal.Set( 0.0f, 0.0f, 0.0f );
	}
};

struct RdFace
{	
	ushort		MatID;
	ushort		Index[3];

	Point3		Normal;			// Face Normal
	Point3		Tangent;		// Face Tangent

	RdFace() : MatID( 0 )
	{
		Index[0] = Index[1] = Index[2] = 0;
	}
};

class RdMeshNode : public RdBaseNode
{
protected:
	//------------------------------------------------------------------
	BYTE			m_btMaxLink;
	
	bool			m_bHasVertexColor;
	bool			m_bHasMultiUV;
	bool			m_bHasNormalMap;

	Tab<RdVertex*>	m_VertexList;
	Tab<RdFace*>	m_FaceList;
	Tab<ushort>		m_LinkedBoneList;

	void			Clear();

	void			AddLinkedBoneID( ushort usID );
	ushort			FindLinkedBoneIndex( ushort usBoneID );

	void			CalculateOBB();

public:
	RdMeshNode( BYTE btType, ushort usID, const TCHAR* strName );
	virtual ~RdMeshNode();
	
	void			SetVertexList( Tab<RdVertex>& list, bool bReverse = true );
	void			SetFaceList( Tab<RdFace>& list, bool bNeg );

	void			SetFaceMtlIndex( ushort usIndex, ushort usMtlID );
	ushort			GetFaceMtlIndex( ushort usIndex );

	void			NotifyComplete();
	
	void			SetEnableVertexColor( bool bEnable )	{		m_bHasVertexColor = bEnable;				}
	bool			GetEnableVertexColor() const			{		return m_bHasVertexColor;					}

	void			SetMultiUV( bool bValue )				{		m_bHasMultiUV = bValue;						}
	bool			HasMultiUV() const						{		return m_bHasMultiUV;						}

	BYTE			GetMaxLink() const						{		return m_btMaxLink;							}
	ushort			GetLinkedBoneCount() const				{		return (ushort)m_LinkedBoneList.Count();	}
	ushort			GetVertexCount() const					{		return (ushort)m_VertexList.Count();		}
	ushort			GetFaceCount() const					{		return (ushort)m_FaceList.Count();			}

	void			UseNormalMap( bool bUse )				{		m_bHasNormalMap = bUse;						}
	bool			HasNormalMap() const					{		return m_bHasNormalMap;						}

	const Tab<ushort>&		GetLinkedBoneList() const		{		return m_LinkedBoneList;					}
	const Tab<RdVertex*>&	GetVertexList() const			{		return m_VertexList;						}
	const Tab<RdFace*>&		GetFaceList() const				{		return m_FaceList;							}
};

struct RdNormFace
{
	ushort		Index[3];
	Point3		Normal;

	RdNormFace()
	{
		Index[0] = Index[1] = Index[2] = 0;
		Normal.Set( 0.0f, 0.0f, 0.0f );
	}
};

class RdNormFaceNode
{
protected:
	Tab<Point3>			m_VertexList;
	Tab<RdNormFace*>	m_FaceList;

	void			Clear();

	Point3			CalcFaceNormal( ulong ulA, ulong ulB, ulong ulC );

public:
	RdNormFaceNode();
	~RdNormFaceNode();

	void			SetVertexList( Tab<Point3>& list );
	void			SetFaceList( Tab<RdNormFace>& list, bool bNeg );

	ushort			GetVertexCount() const					{		return (ushort)m_VertexList.Count();		}
	ushort			GetFaceCount() const					{		return (ushort)m_FaceList.Count();			}

	const Tab<Point3>&	GetVertexList() const				{		return m_VertexList;						}
	const Tab<RdNormFace*>	GetFaceList() const				{		return m_FaceList;							}
};

#endif // __RDMAX70MESH_H__