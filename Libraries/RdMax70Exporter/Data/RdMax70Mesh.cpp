#include "RdMax70Mesh.h"
#include "..\RdMax70Util.h"

RdMeshNode::RdMeshNode( BYTE btType, ushort usID, const TCHAR* strName ) 
: RdBaseNode( btType, usID, strName )
{
	m_btMaxLink = 0;
	
	m_bHasVertexColor = false;
	m_bHasMultiUV = false;

	m_VertexList.Init();
	m_VertexList.ZeroCount();

	m_FaceList.Init();
	m_FaceList.ZeroCount();

	m_LinkedBoneList.Init();
	m_LinkedBoneList.ZeroCount();
}

RdMeshNode::~RdMeshNode()
{
	Clear();
}

void RdMeshNode::Clear()
{
	ulong ulCount = m_VertexList.Count();
	RdVertex** ppVert;
	for( ulong i = 0; i < ulCount; ++i )
	{
		ppVert = m_VertexList.Addr( i );
		delete (*ppVert);
	}
	m_VertexList.Delete( 0, ulCount );

	ulCount = m_FaceList.Count();
	RdFace** ppFace;
	for( ulong i = 0; i < ulCount; ++i )
	{
		ppFace = m_FaceList.Addr( i );
		delete (*ppFace);
	}
	m_FaceList.Delete( 0, ulCount );

	m_LinkedBoneList.Delete( 0, m_LinkedBoneList.Count() );
}


void RdMeshNode::AddLinkedBoneID( ushort usID )
{
	ushort usBoneCnt = m_LinkedBoneList.Count();

	ushort i = 0;
	for( i = 0; i < usBoneCnt; ++i )
	{
		if( m_LinkedBoneList[i] == usID )
			break;
	}

	if( i == usBoneCnt )
		m_LinkedBoneList.Insert( usBoneCnt, 1, &usID );
}

void RdMeshNode::SetFaceList( Tab<RdFace>& list, bool bNeg )
{
	ulong ulCount = list.Count();

	m_FaceList.Init();
	m_FaceList.ZeroCount();
	m_FaceList.SetCount( ulCount );

	for( ulong i = 0; i < ulCount; ++i )
	{
		RdFace* pFace = new RdFace();

		if( bNeg )
		{
			pFace->Index[0] = list[i].Index[0];
			pFace->Index[1] = list[i].Index[1];
			pFace->Index[2] = list[i].Index[2];
		}
		else
		{
			pFace->Index[0] = list[i].Index[0];
			pFace->Index[1] = list[i].Index[2];
			pFace->Index[2] = list[i].Index[1];
		}
		pFace->MatID = list[i].MatID;

		//m_FaceList.Append( 1, &pFace );
		m_FaceList[i] = pFace;
	}
}

void RdMeshNode::SetVertexList( Tab<RdVertex>& list, bool bReverse )
{
	Matrix3 inv = Inverse( m_matWorldTM );

	ulong ulCount = list.Count();
	for( ulong i = 0; i < ulCount; ++i )
	{
		RdVertex* pVert = new RdVertex();

//		if( m_btMaxLink == 0 )
//		{
		if( bReverse )
		{
			pVert->Pos = inv * Point3( list[i].Pos.x, list[i].Pos.z, list[i].Pos.y );

//			pVert->Normal = inv * Point3( list[i].Normal.x, list[i].Normal.z, list[i].Normal.y );
			pVert->Normal = Point3( list[i].Normal.x, list[i].Normal.z, list[i].Normal.y );
			pVert->Tangent = Point3( list[i].Tangent.x, list[i].Tangent.z, list[i].Tangent.y );
			pVert->Binormal = Point3( list[i].Binormal.x, list[i].Binormal.z, list[i].Binormal.y );
			//pVert->Tangent = Point3( list[i].Tangent.x, list[i].Tangent.y, list[i].Tangent.z );
			//pVert->Binormal = Point3( list[i].Binormal.x, list[i].Binormal.y, list[i].Binormal.z );
		}
		else
		{
			pVert->Pos = inv * Point3( list[i].Pos.x, list[i].Pos.y, list[i].Pos.z );

//			pVert->Normal = inv * Point3( list[i].Normal.x, list[i].Normal.y, list[i].Normal.z );
			pVert->Normal = Point3( list[i].Normal.x, list[i].Normal.y, list[i].Normal.z );
			pVert->Tangent = Point3( list[i].Tangent.x, list[i].Tangent.y, list[i].Tangent.z );
			pVert->Binormal = Point3( list[i].Binormal.x, list[i].Binormal.y, list[i].Binormal.z );
		}
//		}
//		else
//		{
//			pVert->Pos = Point3( list[i].Pos.x, list[i].Pos.z, list[i].Pos.y );
//			pVert->Normal = Point3( list[i].Normal.x, list[i].Normal.z, list[i].Normal.y );
//		}

		pVert->UV.U = list[i].UV.U;
		pVert->UV.V = list[i].UV.V;

		pVert->Color = list[i].Color;

		pVert->Link = list[i].Link;

		if( m_btMaxLink < pVert->Link )
		{
			m_btMaxLink = pVert->Link;
		}

		int nLink = pVert->Link;
		if( nLink > 4 )
			nLink = 4;

		for( int j = 0; j < nLink; ++j )
		{
			pVert->Blend[j].BoneID = list[i].Blend[j].BoneID;
			pVert->Blend[j].Weight = list[i].Blend[j].Weight;

			AddLinkedBoneID( pVert->Blend[j].BoneID );
		}

		if( m_ptBBoxMin.x > pVert->Pos.x )			m_ptBBoxMin.x = pVert->Pos.x;
		if( m_ptBBoxMin.y > pVert->Pos.y )			m_ptBBoxMin.y = pVert->Pos.y;
		if( m_ptBBoxMin.z > pVert->Pos.z )			m_ptBBoxMin.z = pVert->Pos.z;

		if( m_ptBBoxMax.x < pVert->Pos.x )			m_ptBBoxMax.x = pVert->Pos.x;
		if( m_ptBBoxMax.y < pVert->Pos.y )			m_ptBBoxMax.y = pVert->Pos.y;
		if( m_ptBBoxMax.z < pVert->Pos.z )			m_ptBBoxMax.z = pVert->Pos.z;

		m_VertexList.Append( 1, &pVert );
	}

	CalculateOBB();
}

void RdMeshNode::SetFaceMtlIndex( ushort usIndex, ushort usMtlID )
{
	ASSERT_MBOX( usIndex < m_FaceList.Count(), _T( "Wrong Index. [SetFaceMtlIndex]" ) );

	m_FaceList[usIndex]->MatID = usMtlID;
}

ushort RdMeshNode::GetFaceMtlIndex( ushort usIndex )
{
	ASSERT_MBOX( usIndex < m_FaceList.Count(), _T( "Wrong Index. [GetFaceMtlIndex]" ) );

	return m_FaceList[usIndex]->MatID;
}

void RdMeshNode::CalculateOBB()
{
	Point3 m( 0.0f, 0.0f, 0.0f );
	int nCount = m_VertexList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		m += m_VertexList[i]->Pos;
	}

	m /= (float)nCount;

	float c11, c22, c33, c12, c13, c23;
	c11 = c22 = c33 = c12 = c13 = c23 = 0.0f;

	for( int i = 0; i < nCount; ++i )
	{
		c11 += ( m_VertexList[i]->Pos.x - m.x ) * ( m_VertexList[i]->Pos.x - m.x );
		c22 += ( m_VertexList[i]->Pos.y - m.y ) * ( m_VertexList[i]->Pos.y - m.y );
		c33 += ( m_VertexList[i]->Pos.z - m.z ) * ( m_VertexList[i]->Pos.z - m.z );
		c12 += ( m_VertexList[i]->Pos.x - m.x ) * ( m_VertexList[i]->Pos.y - m.y );
		c13 += ( m_VertexList[i]->Pos.x - m.x ) * ( m_VertexList[i]->Pos.z - m.z );
		c23 += ( m_VertexList[i]->Pos.y - m.y ) * ( m_VertexList[i]->Pos.z - m.z );
	}

	c11 /= (float)nCount;
	c22 /= (float)nCount;
	c33 /= (float)nCount;
	c12 /= (float)nCount;
	c13 /= (float)nCount;
	c23 /= (float)nCount;

	float mm[3][3] = 
	{ 
		{ c11, c12, c13 }, 
		{ c12, c22, c23 }, 
		{ c13, c23, c33 } 
	};

	float eigenvector[3][3];
	float eigenvalue[3];
	Jacobi( mm, eigenvector, eigenvalue );

	for( int i = 0; i < 3; ++i )
	{
		m_arAxis[i].x = eigenvector[0][i];
		m_arAxis[i].y = eigenvector[1][i];
		m_arAxis[i].z = eigenvector[2][i];
	}

	float min[3] = { FLT_MAX, FLT_MAX, FLT_MAX };
	float max[3] = { -FLT_MAX, -FLT_MAX, -FLT_MAX };

	float dot;

	for( int i = 0; i < 3; ++i )
	{
		for( int j = 0; j < nCount; ++j )
		{
			dot = DotProd( m_VertexList[j]->Pos, m_arAxis[i] );
			if( min[i] > dot )			min[i] = dot;
			if( max[i] < dot )			max[i] = dot;
		}
	}

	m_ptCenter = (((min[0]+max[0])*0.5f)*m_arAxis[0]) + (((min[1]+max[1])*0.5f)*m_arAxis[1]) + 
					(((min[2]+max[2])*0.5f)*m_arAxis[2]);

	m_arExtent.x = ( max[0] - min[0] ) * 0.5f;
	m_arExtent.y = ( max[1] - min[1] ) * 0.5f;
	m_arExtent.z = ( max[2] - min[2] ) * 0.5f;
}

int CompFace( const void* pEl1, const void* pEl2 )
{
	RdFace* pF1 = (RdFace*)&pEl1;
	RdFace* pF2 = (RdFace*)&pEl2;

	if( pF1->MatID < pF2->MatID )		return -1;
	if( pF1->MatID == pF2->MatID )		return 0;
	return 1;

//	return (pF1->MatID < pF2->MatID );
}

int CompBone( const void* pEl1, const void* pEl2 )
{
	ushort* a = (ushort*)pEl1;
	ushort* b = (ushort*)pEl2;

	if( (*a) < (*b) )		return -1;
	if( (*a) == (*b) )		return 0;
	return 1;

//	return ( (*a) > (*b) );
}


void RdMeshNode::NotifyComplete()
{
	m_FaceList.Sort( CompFace );
	m_LinkedBoneList.Sort( CompBone );

	int nCount = m_VertexList.Count();
	ushort link = 0;
	ushort idx = 0;
	ushort newidx = 0;
	for( int i = 0; i < nCount; ++i )
	{
		link = m_VertexList[i]->Link;
		if( link > 4 )
			link = 4;

		for( idx = 0; idx < link; ++idx )
		{
			newidx = FindLinkedBoneIndex( m_VertexList[i]->Blend[idx].BoneID );
			m_VertexList[i]->Blend[idx].BoneID = newidx;
		}
	}
}

ushort RdMeshNode::FindLinkedBoneIndex( ushort usBoneID )
{
	ushort usBoneCnt = m_LinkedBoneList.Count();
	ushort usIndex = 0;

	for( ushort i = 0; i < usBoneCnt; ++i )
	{
		if( m_LinkedBoneList[i] == usBoneID )
			break;

		++usIndex;
	}

	return usIndex;	
}

//------------------------------------------------------------------
RdNormFaceNode::RdNormFaceNode()
{
	m_VertexList.Init();
	m_VertexList.ZeroCount();

	m_FaceList.Init();
	m_FaceList.ZeroCount();
}

RdNormFaceNode::~RdNormFaceNode()
{
	Clear();
}

void RdNormFaceNode::Clear()
{
	ulong ulCount = m_VertexList.Count();
	m_VertexList.Delete( 0, ulCount );

	ulCount = m_FaceList.Count();
	RdNormFace** ppFace;
	for( ulong i = 0; i < ulCount; ++i )
	{
		ppFace = m_FaceList.Addr( i );
		delete (*ppFace );
	}
	m_FaceList.Delete( 0, ulCount );
}

void RdNormFaceNode::SetVertexList( Tab<Point3>& list )
{
	ulong ulCount = list.Count();
	m_VertexList.SetCount( ulCount );
	for( ulong i = 0; i < ulCount; ++i )
	{
		m_VertexList[i] = Point3( list[i].x, list[i].z, list[i].y );
	}
}

void RdNormFaceNode::SetFaceList( Tab<RdNormFace>& list, bool bNeg )
{
	ulong ulCount = list.Count();
	for( ulong i = 0; i < ulCount; ++i )
	{
		RdNormFace* pFace = new RdNormFace();

		pFace->Index[0] = list[i].Index[0];

		if( bNeg )
		{
			pFace->Index[1] = list[i].Index[1];
			pFace->Index[2] = list[i].Index[2];
		}
		else
		{
			pFace->Index[1] = list[i].Index[2];
			pFace->Index[2] = list[i].Index[1];
		}

		pFace->Normal = CalcFaceNormal( pFace->Index[0], pFace->Index[1], pFace->Index[2] );

		m_FaceList.Append( 1, &pFace );
	}
}

Point3 RdNormFaceNode::CalcFaceNormal( ulong ulA, ulong ulB, ulong ulC )
{
	Point3 v10 = m_VertexList[ulB] - m_VertexList[ulA];
	Point3 v20 = m_VertexList[ulC] - m_VertexList[ulA];

	Point3 normal = v10 ^ v20;
	return normal.Normalize();
}