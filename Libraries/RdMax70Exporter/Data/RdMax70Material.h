#ifndef __RDMAX70MATERIAL_H__
#define __RDMAX70MATERIAL_H__

#pragma once

#include "..\RdMax70ExporterHeader.h"

class RdMaterial
{
protected:
	ushort			m_usID;		

	TCHAR			m_strMtlName[MAX_LEN];

	TCHAR			m_strDiffuse[MAX_LEN];
	TCHAR			m_strEmissive[MAX_LEN];
	TCHAR			m_strSpecular[MAX_LEN];
	TCHAR			m_strLightMap[MAX_LEN];
	TCHAR			m_strNormalMap[MAX_LEN];
	TCHAR			m_strBumpMap[MAX_LEN];

	bool			m_bDiffuseMap;
	bool			m_bEmissiveMap;
	bool			m_bSpecularMap;
	bool			m_bLightMap;
	bool			m_bNormalMap;
	bool			m_bBumpMap;

	BYTE			m_btTwoSide;
	BYTE			m_btAlphaBlending;
	BYTE			m_btOpacity;

public:
	RdMaterial( const TCHAR* strName );
	~RdMaterial();

	void			SetMaterialID( ushort usID )			{		m_usID = usID;					}
	const TCHAR*	GetMaterialName() const					{		return m_strMtlName;			}
	ushort			GetMaterialID() const					{		return m_usID;					}

	void			SetTwoSide( BYTE btTwoSide )			{		m_btTwoSide = btTwoSide;		}
	BYTE			GetTwoSide() const						{		return m_btTwoSide;				}

	void			SetAlphablending( BYTE btBlending )		{		m_btAlphaBlending = btBlending;	}
	BYTE			GetAlphaBlending() const				{		return m_btAlphaBlending;		}

	void			SetOpacity( BYTE btOpacity )			{		m_btOpacity = btOpacity;		}
	BYTE			GetOpacity() const						{		return m_btOpacity;				}

	// Textures
	void			SetDiffuseMap( const TCHAR* strName );
	bool			HaveDiffuseMap() const					{		return m_bDiffuseMap;			}
	const TCHAR*	GetDiffuseMap() const					{		return m_strDiffuse;			}

	void			SetEmissiveMap( const TCHAR* strName );
	const TCHAR*	GetEmissiveMap() const					{		return m_strEmissive;			}
	bool			HasEmissiveMap() const					{		return m_bEmissiveMap;			}

	void			SetSpecularMap( const TCHAR* strName );
	bool			HaveSpecularMap() const					{		return m_bSpecularMap;			}
	const TCHAR*	GetSpecularMap() const					{		return m_strSpecular;			}

	void			SetLightMap( const TCHAR* strName );
	bool			HaveLightMap() const					{		return m_bLightMap;				}
	const TCHAR*	GetLightMap() const						{		return m_strLightMap;			}

	void			SetNormalMap( const TCHAR* strName );
	bool			HaveNormalMap() const					{		return m_bNormalMap;			}
	const TCHAR*	GetNormalMap() const					{		return m_strNormalMap;			}

	void			SetBumpMap( const TCHAR* strName );
	bool			HaveBumpMap() const						{		return m_bBumpMap;				}
	const TCHAR*	GetBumpMap() const						{		return m_strBumpMap;			}
};


#endif // __RDMAX70MATERIAL_H__