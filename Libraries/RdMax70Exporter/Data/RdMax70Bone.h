#ifndef __RDMAX70BONE_H__
#define __RDMAX70BONE_H__

#pragma once

#include "RdMax70Node.h"

class RdBone : public RdBaseNode
{
protected:
	Tab< Point3 >	m_OrigVertexList;
	Tab< Point3 >	m_VertexList;

	bool			m_bMirrored;

public:
	RdBone( BYTE btType, ushort usID, const TCHAR* strName );
	virtual ~RdBone();

	void			AddVertex( const Point3& Pos );

	void			Mirrored()								{		m_bMirrored = true;			}
	bool			IsMirrored() const						{		return m_bMirrored;			}

	void			CalculateOBB();

	int				InsertToPool( Tab<Point3>& list );
};

#endif // __RDMAX70BONE_H__