//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RdMax70Exporter.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDD_EXPORT_TYPE                 102
#define IDD_MESH_DLG                    103
#define IDD_SKEL_DLG                    104
#define IDD_PROPERTY_DLG                107
#define IDC_MESH_EXPORT                 1002
#define IDC_SKEL_EXPORT                 1003
#define IDC_MATERIAL_LIST               1003
#define IDC_ANI_EXPORT                  1004
#define IDC_ALPHA_LIST                  1004
#define IDC_ALPHA_DEL                   1006
#define IDC_VERTEX_CNT                  1007
#define IDC_FACE_CNT                    1008
#define IDC_BONE_CNT                    1009
#define IDC_LINK_CNT                    1010
#define IDC_MESH_LIST                   1011
#define IDC_MESH_SAVE                   1012
#define IDC_MESH_CANCEL                 1013
#define IDC_ALPHA_ADD                   1014
#define IDC_BONE_TREE                   1015
#define IDC_OBJECT_CNT                  1015
#define IDC_SKEL_SAVE                   1016
#define IDC_FACE_CNT2                   1016
#define IDC_MDUMMY_CNT                  1016
#define IDC_SKEL_CANCEL                 1017
#define IDC_BONE_LIST                   1019
#define IDC_BBOX_COUNT                  1024
#define IDC_BBOX_CNT                    1024
#define IDC_DEFINE_HEAD                 1034
#define IDC_EDIT_DEFINE_HEAD            1035
#define IDC_DEFINE_RUARM                1036
#define IDC_EDIT_DEFINE_RUARM           1037
#define IDC_LIST3                       1038
#define IDC_BONE_LISTBOX                1038
#define IDC_SCALE                       1039
#define IDC_DEFINE_LUARM                1039
#define IDC_DEFINE_SPINE                1040
#define IDC_EDIT_DEFINE_SPINE           1041
#define IDC_DEFINE_RHAND                1042
#define IDC_EDIT_DEFINE_RHAND           1043
#define IDC_DEFINE_LHAND                1044
#define IDC_EDIT_DEFINE_LHAND           1045
#define IDC_DEFINE_RFOOT                1046
#define IDC_EDIT_DEFINE_RFOOT           1047
#define IDC_DEFINE_LFOOT                1048
#define IDC_EDIT_DEFINE_LFOOT           1049
#define IDC_BTN_HEAD                    1050
#define IDC_BTN_SPINE                   1051
#define IDC_BTN_RHAND                   1052
#define IDC_CHECK2                      1052
#define IDC_CHECK_VC                    1052
#define IDC_BTN_LHAND                   1053
#define IDC_CHECK1                      1053
#define IDC_SAMPLE                      1053
#define IDC_CHECK_VC2                   1053
#define IDC_CHECK_TANGENT               1053
#define IDC_BTN_RFOOT                   1054
#define IDC_BTN_LFOOT                   1055
#define IDC_BTN_RUARM                   1056
#define IDC_EDIT_DEFINE_LUARM           1057
#define IDC_BTN_LUARM                   1058
#define IDC_DEFINE_RLARM                1059
#define IDC_EDIT_DEFINE_RLARM           1060
#define IDC_BTN_RLARM                   1061
#define IDC_DEFINE_LLARM                1062
#define IDC_EDIT_DEFINE_LLARM           1063
#define IDC_BTN_LLARM                   1064
#define IDC_DEFINE_RTHIGH               1065
#define IDC_EDIT_DEFINE_RTHIGH          1066
#define IDC_BTN_RTHIGH                  1067
#define IDC_DEFINE_LTHIGH               1068
#define IDC_EDIT_DEFINE_LTHIGH          1069
#define IDC_BTN_LTHIGH                  1070
#define IDC_DEFINE_RCALF                1071
#define IDC_EDIT_DEFINE_RCALF           1072
#define IDC_BTN_RCALF                   1073
#define IDC_DEFINE_LCALF                1074
#define IDC_EDIT_DEFINE_LCALF           1075
#define IDC_BTN_HEAD9                   1076
#define IDC_BTN_LCALF                   1076

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1054
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
