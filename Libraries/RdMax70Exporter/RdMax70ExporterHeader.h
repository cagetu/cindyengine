#ifndef __RDMAX70EXPORTERHEADER_H__
#define __RDMAX70EXPORTERHEADER_H__

#pragma once

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

#include "shaders.h"
#include "macrorec.h"
#include "gport.h"
#include "utilapi.h"
#include "simpobj.h"
#include "modstack.h"
#include "spline3d.h"
#include "splshape.h"
#include "bmmlib.h"
#include "ikctrl.h"
#include "strclass.h"
#include "interpik.h"
#include "notetrck.h"


// now, Character Studio entirely included in 3dsmax7
// for other version of 3dsmax, specify include path of character studio
// in 'project > directory' page of 'tool > option' menu.
#if (MAX_RELEASE >= 6900)   // 3dsmax R7 or R7 alpha
    #include <cs/bipexp.h>
    #include <cs/phyexp.h>
#else
    #include <bipexp.h>
    #include <phyexp.h>    // for other version
	//#include "phyexp.h"
	//#include "bipexp.h"
#endif

#include "decomp.h"
#include "stdmat.h"
#include "shape.h"
#include "interpik.h"

#include "iEditNormals.h"

#include "simpmod.h"

#include "MNMATH.H"
#include <MeshNormalSpec.h>


#define EDIT_NORMALS_CLASS_ID Class_ID(0x4aa52ae3, 0x35ca1cde)

#define MAX_LEN			256
typedef unsigned short ushort;

#endif // __RDMAX70EXPORTERHEADER_H__