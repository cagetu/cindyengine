#include "RdMax70Exporter.h"
#include "RdMax70Util.h"

//------------------------------------------------------------------
#define RDMAX70EXPORTER_CLASS_ID Class_ID(0x40817660, 0x6d080731)

//==================================================================
// ClassDesc Class
//==================================================================
class RdMax70ExporterClassDesc : public ClassDesc2
{
public:
	int 			IsPublic()						{	return TRUE;						}
	void*			Create(BOOL loading = FALSE)	{	return new RdMax70Exporter();		}
	const TCHAR*	ClassName()						{	return GetString(IDS_CLASS_NAME);	}
	SClass_ID		SuperClassID()					{	return SCENE_EXPORT_CLASS_ID;		}
	Class_ID		ClassID()						{	return RDMAX70EXPORTER_CLASS_ID;	}
	const TCHAR* 	Category()						{	return GetString(IDS_CATEGORY);		}

	const TCHAR*	InternalName()					{	return _T("RdMax70Exporter");		}	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance()						{	return hInstance;					}	// returns owning module handle
};

static RdMax70ExporterClassDesc RdMax70ExporterDesc;
ClassDesc2* GetRdMaxExporterDesc()	{	return &RdMax70ExporterDesc;		}


bool SetBoneDefine( HWND hDlg, RdMax70Exporter* pExp );

int g_nCurExp = -1;

//------------------------------------------------------------------
INT_PTR CALLBACK ExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax70Exporter* pExp = NULL;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			pExp = (RdMax70Exporter*)lParam;
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );		

			g_nCurExp = 0;
		}
		return TRUE;

	case WM_COMMAND:
		if( HIWORD( wParam ) == BN_CLICKED )
		{
			switch( LOWORD( wParam ) )
			{
			case IDC_MESH_EXPORT:
				{
					g_nCurExp = IDC_MESH_EXPORT;

					EndDialog( hDlg, 1 );
//					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_MESH_DLG), GetActiveWindow(), MeshExportDlgProc, (LPARAM)(pExp));					
					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_PROPERTY_DLG), GetActiveWindow(), PropertyExportDlgProc, (LPARAM)(pExp));
				}
				break;

			case IDC_SKEL_EXPORT:
				{
					g_nCurExp = IDC_SKEL_EXPORT;

					EndDialog( hDlg, 1 );
					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_SKEL_DLG), GetActiveWindow(), SkelExportDlgProc, (LPARAM)(pExp));					
				}
				break;

			case IDC_ANI_EXPORT:
				{
					g_nCurExp = IDC_ANI_EXPORT;

					EndDialog( hDlg, 1 );
					DialogBoxParam( hInstance, MAKEINTRESOURCE(IDD_PROPERTY_DLG), GetActiveWindow(), PropertyExportDlgProc, (LPARAM)(pExp));
				}
				break;
			}
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog( hDlg, 0 );
		}
		return TRUE;
	}

	return FALSE;
}

INT_PTR CALLBACK MeshExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax70Exporter* pExp = NULL;
	static HWND hCheckVertColor, hCheckTangent;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );

			SetCursor( LoadCursor( NULL, IDC_WAIT ) );
			pExp = (RdMax70Exporter*)lParam;
			pExp->m_pMeshExp = new RdMeshExp( pExp->m_pIFace, hDlg, pExp->m_bSelectedExp );
			pExp->m_pMeshExp->SetApplyScale( pExp->GetApplyScale() );

			pExp->m_pMeshExp->Initialize();
			pExp->m_pMeshExp->OutputInformation();
			SetCursor( LoadCursor( NULL, IDC_ARROW ) );	

			hCheckVertColor = GetDlgItem( hDlg, IDC_CHECK_VC );
			hCheckTangent = GetDlgItem( hDlg, IDC_CHECK_TANGENT );
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog( hDlg, 0 );
		}
		return TRUE;

	case WM_COMMAND:
		{
//			if( HIWORD( wParam ) == BN_CLICKED )
//			{
				switch( LOWORD( wParam ) )
				{
				case IDC_MESH_CANCEL:
					{
						EndDialog( hDlg, 0 );
					}
					break;

				case IDC_MESH_SAVE:
					{
						SetCursor( LoadCursor( NULL, IDC_WAIT ) );

						TCHAR buffer[256];
						_stprintf( buffer, _T("%s.mtl"), pExp->m_strFileName );
						pExp->m_pMeshExp->ExportMaterial( buffer );

						_stprintf( pExp->m_strFileName, _T( "%s.mmf" ), pExp->m_strFileName );
						pExp->m_pMeshExp->ExportMesh( pExp->m_strFileName );
						SetCursor( LoadCursor( NULL, IDC_ARROW ) );

						if( IDOK == MessageBox( GetActiveWindow(), _T("Complete to Export Mesh."), _T("Mesh Export"), MB_OK ) )
						{
							EndDialog( hDlg, 1 );						
						}
					}
					break;

				case IDC_ALPHA_ADD:
					{
						int num = SendDlgItemMessage( hDlg, IDC_MATERIAL_LIST, LB_GETSELCOUNT, 0, 0 );
						if( num != 0 )
						{
							int buffer[100];
							TCHAR str[128];
							SendDlgItemMessage( hDlg, IDC_MATERIAL_LIST, LB_GETSELITEMS, 100, (LPARAM)buffer );
							for( int i = 0; i < num; ++i )
							{
								SendDlgItemMessage( hDlg, IDC_MATERIAL_LIST, LB_GETTEXT, buffer[i], (LPARAM)str );
								AddStringInListBox( hDlg, IDC_ALPHA_LIST, str );
							}
						}
					}
					break;

				case IDC_ALPHA_DEL:
					{
						int num = SendDlgItemMessage( hDlg, IDC_ALPHA_LIST, LB_GETSELCOUNT, 0, 0 );
						if( num != 0 )
						{
							int buffer[100];
							SendDlgItemMessage( hDlg, IDC_ALPHA_LIST, LB_GETSELITEMS, 100, (LPARAM)buffer );
							for( int i = num-1; i > -1; --i )
							{
								SendDlgItemMessage( hDlg, IDC_ALPHA_LIST, LB_DELETESTRING, buffer[i], 0 );
							}
						}
					}
					break;

				case IDC_CHECK_VC:
					{
						if( SendMessage( hCheckVertColor, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheckVertColor, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pMeshExp->UseVertexColor( true );
						}
						else
						{
							SendMessage( hCheckVertColor, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pMeshExp->UseVertexColor( false );
						}
						InvalidateRect( hCheckVertColor, NULL, TRUE );
					}
					break;
					
				/// Tangent Vector 뽑기
				case IDC_CHECK_TANGENT:
					{
						if( SendMessage( hCheckTangent, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheckTangent, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pMeshExp->UseVertTangent( true );
						}
						else
						{
							SendMessage( hCheckTangent, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pMeshExp->UseVertTangent( false );
						}
						InvalidateRect( hCheckTangent, NULL, TRUE );
					}
					break;
				} // switch
			//}
			return TRUE;
		}
	}

	return FALSE;
}

INT_PTR CALLBACK SkelExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax70Exporter* pExp = NULL;
	static HWND hCheck[15];
	static HWND hList;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );

			SetCursor( LoadCursor( NULL, IDC_WAIT ) );
			{
				pExp = (RdMax70Exporter*)lParam;
				pExp->m_pSkelExp = new RdSkelExp( pExp->m_pIFace, hDlg );
				pExp->m_pSkelExp->Initialize();
				pExp->m_pSkelExp->OutputInformation();

				//! 버튼 기본 설정..
				hCheck[0] = GetDlgItem( hDlg, IDC_DEFINE_HEAD );
				hCheck[1] = GetDlgItem( hDlg, IDC_DEFINE_SPINE );
				hCheck[2] = GetDlgItem( hDlg, IDC_DEFINE_RHAND );
				hCheck[3] = GetDlgItem( hDlg, IDC_DEFINE_LHAND );
				hCheck[4] = GetDlgItem( hDlg, IDC_DEFINE_RFOOT );
				hCheck[5] = GetDlgItem( hDlg, IDC_DEFINE_LFOOT );
				hCheck[6] = GetDlgItem( hDlg, IDC_DEFINE_RUARM );
				hCheck[7] = GetDlgItem( hDlg, IDC_DEFINE_LUARM );
				hCheck[8] = GetDlgItem( hDlg, IDC_DEFINE_RLARM );
				hCheck[9] = GetDlgItem( hDlg, IDC_DEFINE_LLARM );
				hCheck[10] = GetDlgItem( hDlg, IDC_DEFINE_RTHIGH );
				hCheck[11] = GetDlgItem( hDlg, IDC_DEFINE_LTHIGH );
				hCheck[12] = GetDlgItem( hDlg, IDC_DEFINE_RCALF );
				hCheck[13] = GetDlgItem( hDlg, IDC_DEFINE_LCALF );
			}
			SetCursor( LoadCursor( NULL, IDC_ARROW ) );
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog( hDlg, 0 );
		}
		return TRUE;

	case WM_COMMAND:
		{
//			if( HIWORD( wParam ) == BN_CLICKED )
			{
				switch( LOWORD( wParam ) )
				{
				case IDC_SKEL_CANCEL:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );

						EndDialog( hDlg, 0 );
					}
					break;

				case IDC_SKEL_SAVE:
					{
						if( !SetBoneDefine( hDlg, pExp ) )
							break;

						// Export
						SetCursor( LoadCursor( NULL, IDC_WAIT ) );
						_stprintf( pExp->m_strFileName, _T( "%s.mbf" ), pExp->m_strFileName );
						pExp->m_pSkelExp->ExportSkel( pExp->m_strFileName );
						SetCursor( LoadCursor( NULL, IDC_ARROW ) );	

						if( IDOK == MessageBox( GetActiveWindow(), _T("Complete to Export Skeleton."), _T("Skeleton Export"), MB_OK ) )
						{
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );

							EndDialog( hDlg, 1 );						
						}
					}
					break;

				//------------------------------------------------------------------
				//! 선택을 위한 버튼 클릭
				case IDC_DEFINE_HEAD:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( 0 == i )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Head타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[0], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[0], BM_SETCHECK, BST_CHECKED, 0 );
							
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_HEAD );
						}
						else
						{
							SendMessage( hCheck[0], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[0], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_SPINE:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( 1 == i )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 spine타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[1], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[1], BM_SETCHECK, BST_CHECKED, 0 );

							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_SPINE );
						}
						else
						{
							SendMessage( hCheck[1], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[1], NULL, TRUE );
					}
					break;
					
				case IDC_DEFINE_RHAND:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( 2 == i )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 RHAND타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[2], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[2], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_RHAND );
						}
						else
						{
							SendMessage( hCheck[2], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[2], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_LHAND:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 3 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 LHand타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[3], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[3], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_LHAND );
						}
						else
						{
							SendMessage( hCheck[3], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[3], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_RFOOT:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 4 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 RFoot타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[4], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[4], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_RFOOT );
						}
						else
						{
							SendMessage( hCheck[4], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[4], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_LFOOT:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 5 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 LFoot타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[5], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[5], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_LFOOT );
						}
						else
						{
							SendMessage( hCheck[5], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[5], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_RUARM:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 6 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Right Upper Arm타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[6], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[6], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_RUARM );
						}
						else
						{
							SendMessage( hCheck[6], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[6], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_LUARM:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 7 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Left Upper Arm타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[7], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[7], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_LUARM );
						}
						else
						{
							SendMessage( hCheck[7], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[7], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_RLARM:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 8 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Right Lower Arm타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[8], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[8], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_RLARM );
						}
						else
						{
							SendMessage( hCheck[8], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[8], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_LLARM:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 9 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Left Lower Arm타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[9], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[9], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_LLARM );
						}
						else
						{
							SendMessage( hCheck[9], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[9], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_RTHIGH:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 10 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Right Thigh타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[10], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[10], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_RTHIGH );
						}
						else
						{
							SendMessage( hCheck[10], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[10], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_LTHIGH:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 11 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Left Thigh타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[11], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[11], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_LTHIGH );
						}
						else
						{
							SendMessage( hCheck[11], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[11], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_RCALF:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 12 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Right Calf타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[12], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[12], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_RCALF );
						}
						else
						{
							SendMessage( hCheck[12], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[12], NULL, TRUE );
					}
					break;

				case IDC_DEFINE_LCALF:
					{
						//! Check 버튼을 클릭하면, 다른 체크 버튼들을 초기화한다.
						for( int i = 0; i < RdBaseNode::DEFINE_MAX; ++i )
						{
							if( i == 13 )
								continue;

							SendMessage( hCheck[i], BM_SETCHECK, BST_UNCHECKED, 0 );
						}

						//! 이펙트를 붙일 Left Calf타입 본을 결정할 권한을 준다.
						if( SendMessage( hCheck[13], BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck[13], BM_SETCHECK, BST_CHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_LCALF );
						}
						else
						{
							SendMessage( hCheck[13], BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						}
						InvalidateRect( hCheck[13], NULL, TRUE );
					}
					break;

				//------------------------------------------------------------------
				//! 이름 Clear
				case IDC_BTN_HEAD:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_HEAD, "" );
					}
					break;

				case IDC_BTN_SPINE:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_SPINE, "" );
					}
					break;

				case IDC_BTN_RHAND:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RHAND, "" );
					}
					break;

				case IDC_BTN_LHAND:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LHAND, "" );
					}
					break;

				case IDC_BTN_RFOOT:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RFOOT, "" );
					}
					break;

				case IDC_BTN_LFOOT:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LFOOT, "" );
					}
					break;

				case IDC_BTN_RUARM:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RUARM, "" );
					}
					break;

				case IDC_BTN_LUARM:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LUARM, "" );
					}
					break;

				case IDC_BTN_RLARM:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RLARM, "" );
					}
					break;

				case IDC_BTN_LLARM:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LLARM, "" );
					}
					break;

				case IDC_BTN_RTHIGH:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RTHIGH, "" );
					}
					break;

				case IDC_BTN_LTHIGH:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LTHIGH, "" );
					}
					break;

				case IDC_BTN_RCALF:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RCALF, "" );
					}
					break;

				case IDC_BTN_LCALF:
					{
						pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
						SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LCALF, "" );
					}
					break;

				//------------------------------------------------------------------
				//! 본 리스트에서 선택
				case IDC_BONE_LISTBOX:
					{
						RdBaseNode::DEFINE_TYPE eType = pExp->m_pSkelExp->GetActiveDefine();
						if( eType == RdBaseNode::DEFINE_NONE )
							break;

						switch( HIWORD(wParam) )
						{
						case LBN_SELCHANGE:
							{
								char str[128];
								hList = GetDlgItem( hDlg, IDC_BONE_LISTBOX );
								int i = SendMessage( hList, LB_GETCURSEL, 0, 0 );
								SendMessage( hList, LB_GETTEXT, i, (LPARAM)str );

								switch( eType )
								{
								case RdBaseNode::DEFINE_HEAD:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_HEAD, str );
									break;

								case RdBaseNode::DEFINE_SPINE:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_SPINE, str );
									break;

								case RdBaseNode::DEFINE_RHAND:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RHAND, str );
									break;

								case RdBaseNode::DEFINE_LHAND:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LHAND, str );
									break;

								case RdBaseNode::DEFINE_RFOOT:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RFOOT, str );
									break;

								case RdBaseNode::DEFINE_LFOOT:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LFOOT, str );
									break;

								case RdBaseNode::DEFINE_RUARM:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RUARM, str );
									break;

								case RdBaseNode::DEFINE_LUARM:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LUARM, str );
									break;

								case RdBaseNode::DEFINE_RLARM:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RLARM, str );
									break;

								case RdBaseNode::DEFINE_LLARM:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LLARM, str );
									break;

								case RdBaseNode::DEFINE_RTHIGH:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RTHIGH, str );
									break;

								case RdBaseNode::DEFINE_LTHIGH:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LTHIGH, str );
									break;

								case RdBaseNode::DEFINE_RCALF:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_RCALF, str );
									break;

								case RdBaseNode::DEFINE_LCALF:
									SetDlgItemText( hDlg, IDC_EDIT_DEFINE_LCALF, str );
									break;

								default:
									break;
								}
							}
							break;
						}
					}
					break;


				}
			}
			return TRUE;
		}
	}

	return FALSE;
}

//------------------------------------------------------------------
INT_PTR CALLBACK PropertyExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static RdMax70Exporter* pExp = NULL;
	static HWND hCheck;

	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			CenterWindow( hDlg, GetWindow( hDlg, GW_OWNER ) );

			SetCursor( LoadCursor( NULL, IDC_WAIT ) );
			pExp = (RdMax70Exporter*)lParam;
			SetCursor( LoadCursor( NULL, IDC_ARROW ) );	
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog( hDlg, 0 );
		}
		return TRUE;

	case WM_COMMAND:
		{
			if( HIWORD( wParam ) == BN_CLICKED )
			{
				switch( LOWORD( wParam ) )
				{
				case IDC_SCALE:
					{
						if( SendMessage( hCheck, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->SetApplyScale( true );
						}
						else
						{
							SendMessage( hCheck, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->SetApplyScale( false );
						}
						InvalidateRect( hCheck, NULL, TRUE );
					}
					break;

				case IDC_SAMPLE:
					{
						if( SendMessage( hCheck, BM_GETCHECK, 0, 0 ) == BST_UNCHECKED )
						{
							SendMessage( hCheck, BM_SETCHECK, BST_CHECKED, 0 );
							pExp->SetUseKeys( true );
						}
						else
						{
							SendMessage( hCheck, BM_SETCHECK, BST_UNCHECKED, 0 );
							pExp->SetUseKeys( false );
						}
						InvalidateRect( hCheck, NULL, TRUE );
					}
					break;

				case IDOK:
					{
						if (g_nCurExp == IDC_MESH_EXPORT)
						{
							EndDialog( hDlg, 0 );
							DialogBoxParam( hInstance, MAKEINTRESOURCE( IDD_MESH_DLG ), GetActiveWindow(),
													MeshExportDlgProc, (LPARAM)pExp );	
						}

						if (g_nCurExp == IDC_ANI_EXPORT)
						{
							SetCursor( LoadCursor( NULL, IDC_WAIT ) );
							pExp->m_pNodeAnimExp = new RdNodeAnimExp( pExp->m_pIFace );
							pExp->m_pNodeAnimExp->SetUseKeys( pExp->GetUseKeys() );
							pExp->m_pNodeAnimExp->Initialize();

							_stprintf( pExp->m_strFileName, _T( "%s.maf" ), pExp->m_strFileName );
							pExp->m_pNodeAnimExp->ExportAnim( pExp->m_strFileName );
							SetCursor( LoadCursor( NULL, IDC_ARROW ) );	

							if( IDOK == MessageBox( GetActiveWindow(), _T("Complete to Export Animation."), _T("Node Animation Export"), MB_OK ) )
								EndDialog( hDlg, 1 );
						}
					}
					break;

				case IDCANCEL:
					{
						EndDialog( hDlg, 0 );
					}
					break;
				}
			}
		}
		return TRUE;
	}

	return FALSE;
}

//------------------------------------------------------------------
// Exporter Class
//------------------------------------------------------------------
RdMax70Exporter::RdMax70Exporter()
{
	m_pExpIFace		= NULL;
	m_pIFace		= NULL;

	m_bSelectedExp	= false;
	m_bApplyScale	= false;
	m_bUseKeys		= false;

	memset( m_strFileName, 0, sizeof(TCHAR)*MAX_LEN );

	m_pMeshExp		= NULL;
	m_pSkelExp		= NULL;
	m_pNodeAnimExp	= NULL;
}

RdMax70Exporter::~RdMax70Exporter()
{
	SAFEDEL( m_pMeshExp );
	SAFEDEL( m_pSkelExp );
	SAFEDEL( m_pNodeAnimExp );
}

int RdMax70Exporter::ExtCount()
{
	return 3;
}

const TCHAR* RdMax70Exporter::Ext( int n )
{
	if( n == 0 )
        return _T("mmf");
	else if( n == 1 )
		return _T("mbf");
	else if( n == 2 )
		return _T("maf");

	return _T("");
}

const TCHAR* RdMax70Exporter::LongDesc()
{
	return _T("Hi-WIN Max Exporter Data File For Mythology" ); 
}

const TCHAR* RdMax70Exporter::ShortDesc()
{
	return _T("Mythology File" );
}

const TCHAR* RdMax70Exporter::AuthorName()
{
	return _T( "Hi-WIN R&D Team" );
}

const TCHAR* RdMax70Exporter::CopyrightMessage()
{
	return _T( "Copyright (c)2005, Hi-WIN" );
}

const TCHAR* RdMax70Exporter::OtherMessage1()
{
	return _T("");
}

const TCHAR* RdMax70Exporter::OtherMessage2()
{
	return _T("");
}

unsigned int RdMax70Exporter::Version()
{
	return 100;
}

void RdMax70Exporter::ShowAbout( HWND hWnd )
{

}

BOOL RdMax70Exporter::SupportsOptions( int ext, DWORD options )
{
	assert( 3 > ext );
	return	( options == SCENE_EXPORT_SELECTED ) ? TRUE : FALSE;
}

int RdMax70Exporter::DoExport( const TCHAR* name, ExpInterface* ei, Interface *i, BOOL suppressPrompts, DWORD options )
{
	m_pExpIFace = ei;
	m_pIFace = i;

	size_t len = _tcslen( name );
	len = len - 4;
	_tcsncpy( m_strFileName, name, len );

	m_bSelectedExp = ( options & SCENE_EXPORT_SELECTED ) ? true : false;

	CountNode();

	if( m_bSelectedExp )
	{
		DialogBoxParam( hInstance, MAKEINTRESOURCE( IDD_PROPERTY_DLG ), GetActiveWindow(),
						PropertyExportDlgProc, (LPARAM)this );	
	}
	else
	{
		DialogBoxParam( hInstance, MAKEINTRESOURCE( IDD_EXPORT_TYPE ), GetActiveWindow(), ExportDlgProc, 
						(LPARAM)this );	
	}

	return 1;
}

//------------------------------------------------------------------
void RdMax70Exporter::CountNode()
{
	CountNodeTEP procCountNode;
	procCountNode.m_nNodeCount = 1000;
	procCountNode.m_nBoneCount = 0;

	(void)m_pExpIFace->theScene->EnumTree( &procCountNode );
}

//------------------------------------------------------------------
int CountNodeTEP::callback( INode* pNode )
{
	if( !UndesirableNode( pNode ) )
	{
		if( IsBoneNode( pNode ) || ( IsMeshNode( pNode ) && IsBoneDummyNode( pNode->GetName() ) ) )
		{
			SetIndexOfNode( pNode, m_nBoneCount );
			++m_nBoneCount;
		}
		else if( IsMeshNode( pNode ) )
		{
			if( IsBoundingBoxNode( pNode->GetName() ) )
				return TREE_CONTINUE;
				
			SetIndexOfNode( pNode, m_nNodeCount );
			++m_nNodeCount;
		}
	}

	return TREE_CONTINUE;
}

//------------------------------------------------------------------
bool SetBoneDefine( HWND hDlg, RdMax70Exporter* pExp )
{
	char str[256];

	//! 머리
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_HEAD, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_HEAD, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 허리(?)
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_SPINE, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_SPINE, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 오른손
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_RHAND, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_RHAND, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 왼손
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_LHAND, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_LHAND, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 오른발
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_RFOOT, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_RFOOT, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 왼발
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_LFOOT, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_LFOOT, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 오른 어깨(?)
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_RUARM, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_RUARM, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 왼 어깨
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_LUARM, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_LUARM, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 오른 팔꿈치
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_RLARM, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_RLARM, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 왼 팔꿈치
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_LLARM, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_LLARM, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 오른 허벅지
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_RTHIGH, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_RTHIGH, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 왼 허벅지
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_LTHIGH, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_LTHIGH, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 오른 종아리
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_RCALF, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_RCALF, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	//! 왼 종아리
	GetDlgItemText( hDlg, IDC_EDIT_DEFINE_LCALF, str, 256 );
	if( !pExp->m_pSkelExp->AddBoneDefine( RdBaseNode::DEFINE_LCALF, str ) )
	{
		if( IDOK == MessageBox( GetActiveWindow(), _T("You select wrong bone."), _T("Warning"), MB_OK ) )
		{
			pExp->m_pSkelExp->SetActiveDefine( RdBaseNode::DEFINE_NONE );
			return false;
		}
	}

	return true;
}