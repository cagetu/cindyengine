#ifndef __RDMAX70EXPORTER_H__
#define __RDMAX70EXPORTER_H__

#pragma once

#include "RdMax70ExporterHeader.h"
#include "Exporter\RdMax70MeshExp.h"
#include "Exporter\RdMax70SkelExp.h"
#include "Exporter\RdMax70NodeAnimExp.h"

extern TCHAR* GetString( int id );
extern HINSTANCE hInstance;

//==================================================================
// Exporter Class
//==================================================================
class RdMax70Exporter : public SceneExport
{
protected:
	ExpInterface*	m_pExpIFace;
	Interface*		m_pIFace;
	TCHAR			m_strFileName[MAX_LEN];

	bool			m_bSelectedExp;
	bool			m_bApplyScale;
	bool			m_bUseKeys;

	RdMeshExp*		m_pMeshExp;
	RdSkelExp*		m_pSkelExp;
	RdNodeAnimExp*	m_pNodeAnimExp;

	void			CountNode();

public:
	static HWND hParams;
		
	int				ExtCount();					// Number of extensions supported
	const TCHAR*	Ext(int n);					// Extension #n (i.e. "3DS")
	const TCHAR*	LongDesc();					// Long ASCII description (i.e. "Autodesk 3D Studio File")
	const TCHAR*	ShortDesc();				// Short ASCII description (i.e. "3D Studio")
	const TCHAR*	AuthorName();				// ASCII Author name
	const TCHAR*	CopyrightMessage();			// ASCII Copyright message
	const TCHAR*	OtherMessage1();			// Other message #1
	const TCHAR*	OtherMessage2();			// Other message #2
	unsigned int	Version();					// Version number * 100 (i.e. v3.01 = 301)
	void			ShowAbout(HWND hWnd);		// Show DLL's "About..." box

	BOOL			SupportsOptions(int ext, DWORD options);
	int				DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);

	RdMax70Exporter();
	~RdMax70Exporter();

	void			SetApplyScale( bool bEnable )				{		m_bApplyScale = bEnable;		}
	bool			GetApplyScale() const						{		return m_bApplyScale;			}

	void			SetUseKeys( bool bUse )						{		m_bUseKeys = bUse;				}
	bool			GetUseKeys() const							{		return m_bUseKeys;				}

	//------------------------------------------------------------------
	friend INT_PTR CALLBACK	ExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam );
	friend INT_PTR CALLBACK	MeshExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam );
	friend INT_PTR CALLBACK	SkelExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam );
	friend INT_PTR CALLBACK PropertyExportDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam );

	friend bool SetBoneDefine( HWND hDlg, RdMax70Exporter* pExp );
};

//------------------------------------------------------------------
class CountNodeTEP : public ITreeEnumProc
{
public:
	virtual int callback( INode* node );

	ushort	m_nBoneCount;
	ushort	m_nNodeCount;
};

#endif // __RDMAX70EXPORTER_H__