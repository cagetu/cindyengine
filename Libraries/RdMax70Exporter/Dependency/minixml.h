#ifndef _SLIM_XML
#define _SLIM_XML



#ifdef _UNICODE
#include <string>
#define	_string	std::wstring
#else
#include <iostream>
#include <string>
#define	_string	std::string
#endif

#include <tchar.h>

class	CMiniXML
{
public :
	CMiniXML() ;
	CMiniXML(const CMiniXML & xml) ;
	~CMiniXML() ;

	bool			Open(const TCHAR * fname, bool defaultutf8=true);
	bool			Save(const TCHAR * fname);

	bool			Parse(const TCHAR * src);

	const TCHAR*	GetError() { return m_ErrorCode; }

	CMiniXML&	operator = (const CMiniXML & xml) ;

	struct _ITEM		//	attribute
	{
		_string	name;
		_string	value;
		_ITEM *	next;
	} ;

	struct _NODE
	{
		_string name;
		_string value;
		_NODE *	next, * prev, * child;
		_ITEM *	item;
	} ;

	class		node
	{
	public :
		node();
		node(const node & n);

		const TCHAR *	GetName() const;
		const TCHAR *	GetValue() const;
		const TCHAR *	GetAttribute(const TCHAR * att) const;

		void	SetName(const TCHAR * name);
		void	SetValue(const TCHAR *value);
		void	SetAttribute(const TCHAR * name, const TCHAR * value);
		void	DeleteAttribute(const TCHAR * name);

		bool	HasChild() const;
		node	Find(const TCHAR * name) const;
		node	Next() const;
		node	GetChildNode() const;

		void operator ++ ();
		void operator ++ (int);

		bool operator == (const void * ptr) const;	// for compare NULL (ptr must be NULL)
		bool operator != (const void * ptr) const;	// for compare NULL (ptr must be NULL)

	private :
		friend	CMiniXML;

		node(_NODE * node, _NODE * parent, bool accessed=false);
	
		_NODE *	m_pNode;
		_NODE * m_pParent;
		bool	m_Accessed;
	} ;

	node			GetRoot() { return node(m_Root.child, &m_Root); }
	node			Insert(node & destnode, const TCHAR * name);
	node			Insert(node & destnode, const node & src);
	node			InsertChild(node & destnode, const TCHAR * name);
	node			InsertChild(node & destnode, const node & src);
	node			Erase(node & node);

	void			Clear();

	node			Find( node & destnode, const TCHAR * findnode );
	TCHAR*			ReadReal( unsigned char * dest, int len, bool opened, bool forceutf9=false );

private :
	const TCHAR *	GetToken(const TCHAR * buf, int *tokenlen);
	const TCHAR *	GetToken(const TCHAR * buf, int len, int *tokenlen, int * skipcnt, const TCHAR * blkch=_T("/="));
	TCHAR *			ReadText(const TCHAR * fname, bool forceutf9=false);
	unsigned char *	ReadFile(const TCHAR * fname, int * len);

	_string			GetString(const TCHAR *ptr, int l);

	_NODE *			CreateNode(const TCHAR *p, int len, bool *singleitem);
	const TCHAR *	ParseXML(const TCHAR *p, _NODE * parent);
	void			Store(FILE *fp, _NODE * node, _NODE * parent, int lv=0, bool notab=false);

	_NODE *			AllocNode(const _string & name);
	void			ReleaseNode(_NODE * node);

	_NODE *			Duplicate(_NODE * dest, const _NODE * src, bool all=true);

	_NODE 			m_Root;	//	dummy root node

	const TCHAR *	m_ErrorCode;
} ;

#endif