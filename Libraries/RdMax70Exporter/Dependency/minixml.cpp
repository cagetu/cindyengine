//	slimxml	2007/02/05
//	noerror@hitel.net
//	http://www.digibath.com/slimxml
//
#include <windows.h>
#include <stdio.h>
#include "minixml.h"

#ifndef _countof
#define _countof(A)	(sizeof(A)/sizeof(*A))
#endif

	CMiniXML::CMiniXML()
{
	memset(&m_Root, 0, sizeof(m_Root));
	m_Root.child	= NULL;
	m_ErrorCode		= NULL;
}


	CMiniXML::CMiniXML(const CMiniXML & xml)
{
	operator = (xml);
}


CMiniXML&	CMiniXML::operator = (const CMiniXML & xml)
{
	memset(&m_Root, 0, sizeof(m_Root));
	m_Root.child	= NULL;
	m_ErrorCode		= NULL;

	Duplicate(&m_Root, xml.m_Root.child);
	return *this;
}


CMiniXML::_NODE * CMiniXML::Duplicate(_NODE * parent, const _NODE * src, bool allnode)
{
	const _NODE * first = src;
	_NODE * node;

	if (src == NULL)
		return NULL;

	do
	{
		node = AllocNode(src->name);
		node->value = src->value;

		if (parent->child == NULL)
		{
			parent->child = node;
			node->next = node->prev = node;
		}	else
		{
			node->next = parent->child;
			node->prev = parent->child->prev;

			parent->child->prev->next = node;
			parent->child->prev = node;
		}

		const _ITEM * ii = src->item;
		_ITEM * prev = NULL;

		for(; ii!=NULL; ii=ii->next)
		{
			_ITEM * item = new _ITEM;
			item->name	= ii->name;
			item->value	= ii->value;
			item->next	= NULL;

			if (prev == NULL)
				node->item	= item;
			else
				prev->next	= item;
			prev = item;
		}

		if (src->child != NULL)
		{
			Duplicate(node, src->child, true);
		}

		src = src->next;
	}	while(allnode == true && src != first);

	return node;
}


	CMiniXML::~CMiniXML()
{
	Clear();
}


const TCHAR * CMiniXML::GetToken(const TCHAR * buf, int *tokenlen)
{	//	for tag
	int i, l;
	for(i=0; buf[i]; i++)
	{
		if ((unsigned)buf[i] == '<')
		{
			for(l=1; buf[i+l] != '>'; l++)
				if (buf[i+l] == '\0')
					return NULL;
			*tokenlen = l+1;
			return &buf[i];
		}	else
		if ((unsigned)buf[i] > ' ')
		{
			for(l=1; buf[i+l] != '<'; l++)
			{
				if (buf[i+l] == '\0')
					return NULL;
			}
			*tokenlen = i+l;
			return &buf[0];
		}
	}
	return NULL;
}


const TCHAR * CMiniXML::GetToken(const TCHAR * buf, int len, int *tokenlen, int * skipcnt, const TCHAR * blkch)
{	//	for item
	int i, l;
	for(i=0; i<len; i++)
	{
		if ((unsigned)buf[i] > ' ')
		{
			if (buf[i] == '\"')
			{
				for(l=0; buf[i+l+1] != '\"'; l++)
				{
					if (i+l+1 > len)
						return NULL;
				}
				*tokenlen	= l;
				*skipcnt	= l + 1;
				return &buf[i+1];
			}	else
			if (_tcschr(blkch, buf[i]) != NULL)
			{
				*tokenlen	= 1;
				*skipcnt	= 1;
				return &buf[i];
			}	else
			{
				for(l=0; (unsigned)buf[i+l] > ' ' && _tcschr(blkch, buf[i+l]) == NULL && i+l < len; l++) ;
				*tokenlen	= l;
				*skipcnt	= l;
				return &buf[i];
			}
		}
	}
	return NULL;
}


const TCHAR * CMiniXML::ParseXML(const TCHAR *p, _NODE * parent)
{
	_NODE	* node;
	int		len;
	bool	singleitem;

	p = GetToken(p, &len);

	if (p == NULL)
		return NULL;

	if (*p != '<')
	{
		m_ErrorCode = _T("엘리먼트는 태그로 시작해야 합니다.");
		return NULL;
	}
	if (memcmp(p, _T("<?"), 2*sizeof(TCHAR)) == 0 ||
		memcmp(p, _T("<!-"), 3*sizeof(TCHAR)) == 0)
	{
		p += len;
		return p;
	}

	node = CreateNode(p+1, len-2, &singleitem);
	if (node == NULL)
		return NULL;
	p += len;

	if (parent->child == NULL)
	{
		parent->child = node;
		node->next = node->prev = node;
	}	else
	{
		node->next = parent->child;
		node->prev = parent->child->prev;

		parent->child->prev->next = node;
		parent->child->prev = node;
	}

	if (singleitem == true)
		return p;

	p = GetToken(p, &len);

	if (p[0] != '<')
	{
		node->value = GetString(p, len);
		p += len;

		p = GetToken(p, &len);
	}	else
	{
		node->value = _T("");
	}

	while(p != NULL)
	{
		if (p[0] == '<' && p[1] == '/')
		{
			const TCHAR * t;
			int l, skipcnt;
			_string endtag;
			t = GetToken(p+2, len-3, &l, &skipcnt);
			endtag = GetString(t, l);
			if (_tcscmp(node->name.c_str(), endtag.c_str()) != 0)
			{
				m_ErrorCode = _T("시작태그와 종료태그가 일치하지 않습니다.");// <test></teeest>
				return NULL;
			}
			p += len;
			return p;
		}	else
		{
			p = ParseXML(p, node);

			if (p == NULL)
				return NULL;

			p = GetToken(p, &len);
		}
	}
	return NULL;
}


CMiniXML::_NODE * CMiniXML::AllocNode(const _string & name)
{
	_NODE * node = new _NODE;
	node->name	= name;
	node->item	= NULL;
	node->next	= NULL;
	node->prev	= NULL;
	node->child	= NULL;
	return node;
}


CMiniXML::_NODE * CMiniXML::CreateNode(const TCHAR *p, int len, bool *singleitem)
{
	int l, skipcnt;
	const TCHAR * t;
	_NODE * node;
	bool meetend = false;

	//	get tag name

	t = GetToken(p, len, &l, &skipcnt);
	node = AllocNode(GetString(t, l));
	t += skipcnt;

	//	parse attribute

	_ITEM * prev = NULL;

	do {
		t = GetToken(t, (int)(p+len-t), &l, &skipcnt);

		if (t != NULL)
		{	//	attribute

			if (l == 1 && *t == '/')	//	<tag/>
			{
				meetend = true;
				break;
			}

			_ITEM * item = new _ITEM;
			item->next = NULL;
			item->name = GetString(t, l);
			t += skipcnt;

			t = GetToken(t, (int)(p+len-t), &l, &skipcnt);
			if (t[0] != '=')
			{
				m_ErrorCode = _T("속성과 속성값 정의가 올바르지 않거나 값이 지정되어 있지 않습니다.");// <test id>
				return NULL;
			}
			t += skipcnt;

			t = GetToken(t, (int)(p+len-t), &l, &skipcnt);
			item->value = GetString(t, l);
			t += skipcnt;

			if (prev == NULL)
			{
				node->item = item;
			}
			else
			{
				prev->next = item;
			}
			item->next = NULL;
			prev = item;
		}
	}	while(t != NULL);

	*singleitem = meetend;

	return node;
}


CMiniXML::node CMiniXML::Insert(node & node, const TCHAR * name)
{
	if (node.m_pNode == NULL)
	{
		if (node.m_pParent != NULL)
		{
			_NODE * newnode = AllocNode(name);
			node.m_pNode = newnode;
			node.m_pParent->child = newnode;
			newnode->next = newnode->prev = newnode;
			return CMiniXML::node(newnode, node.m_pParent);
		}
		return CMiniXML::node(NULL, NULL);
	}	else
	{
		_NODE * newnode = AllocNode(name);
		newnode->next = node.m_pNode->next;
		newnode->prev = node.m_pNode;
		node.m_pNode->next->prev = newnode;
		node.m_pNode->next = newnode;
		return CMiniXML::node(newnode, node.m_pParent);
	}
}


CMiniXML::node CMiniXML::Insert(node & destnode, const node & src)
{
	_NODE * node = Duplicate(destnode.m_pParent, src.m_pNode, false);
	return CMiniXML::node(node, destnode.m_pParent);
}


CMiniXML::node CMiniXML::InsertChild(node & node, const TCHAR * name)
{
	_NODE * parent = node.m_pNode;

	if (parent == NULL)
	{
		return CMiniXML::node(NULL, NULL);
	}	else
	{
		_NODE * newnode = AllocNode(name);

		if (parent->child == NULL)
		{
			parent->child = newnode;
			newnode->next = newnode->prev = newnode;
		}	else
		{
			newnode->next = parent->child;
			newnode->prev = parent->child->prev;

			parent->child->prev->next = newnode;
			parent->child->prev = newnode;
		}
		return CMiniXML::node(newnode, parent);
	}
}

CMiniXML::node CMiniXML::InsertChild(CMiniXML::node &destnode, const CMiniXML::node &src)
{
//	_ASSERT(destnode.m_pNode != src.m_pNode || destnode.m_pParent != src.m_pParent);
	_NODE * node = Duplicate(destnode.m_pNode, src.m_pNode, false);
	return CMiniXML::node(node, destnode.m_pNode);
}


CMiniXML::node CMiniXML::Erase(node & node)
{
	if (node.m_pNode != NULL)
	{
		_NODE * next = node.m_pNode->next;

		if (node.m_pParent->child == node.m_pNode)
		{
			node.m_pParent->child = node.m_pNode->next;
		}

		if (next == node.m_pNode)
		{
			node.m_pParent->child = NULL;
			next = NULL;
		}	else
		{
			node.m_pNode->prev->next = next;
			next->prev = node.m_pNode->prev;
		}

		node.m_pNode->next = node.m_pNode->prev = NULL;
		ReleaseNode(node.m_pNode);
		return CMiniXML::node(next, node.m_pParent);
	}
	return CMiniXML::node(NULL, NULL);
}

CMiniXML::node CMiniXML::Find(CMiniXML::node & destnode, const TCHAR * findnode)
{
	node ii = destnode;

	for(ii=ii.Find(findnode); ii != NULL; ii=ii.Find(findnode))
	{
		return ii;
	}
	return CMiniXML::node(NULL, NULL);
}

void CMiniXML::ReleaseNode(_NODE * node)
{
	_NODE * next, * first = node;

	do
	{
		_ITEM * item, * nextitem;
		for(item=node->item; item!=NULL; )
		{
			nextitem = item->next;
			delete item;
			item = nextitem;
		}

		if (node->child)
			ReleaseNode(node->child);
		next = node->next;
		delete node;
		node = next;
	}	while(node != NULL && node != first);
}


void CMiniXML::Clear()
{
	node n = GetRoot();

	while(n != NULL)
	{
		Erase(n);
		n = GetRoot();
	}
}


static const TCHAR * _convtable[][2] = {
	_T("&lt;"), _T("<"),
	_T("&gt;"), _T(">"),
//	_T("&amp;"), _T("&"),
//	_T("&apos;"), _T("'"),
//	_T("&quot;"), _T("\""),
} ;


_string	CMiniXML::GetString(const TCHAR *ptr, int l)
{
	const TCHAR * key;
	int i;

	_string r = _string(ptr, l);

	for(i=0; i<sizeof(_convtable)/sizeof(*_convtable); i++)
	{
		key = _tcsstr(r.c_str(), _convtable[i][0]);
		while(key != NULL)
		{
			int off = (int)(key - r.c_str());
			r.replace(off, _tcslen(_convtable[i][0]), _convtable[i][1]);
			key = _tcsstr(r.c_str() + off, _convtable[i][0]);
		}
	}

	return r;
}


bool CMiniXML::Open(const TCHAR * fname, bool defaultutf8)
{
	TCHAR * ptr;
	bool ret;
	ptr = ReadText(fname, defaultutf8);
	if (ptr == NULL)
		return false;
	ret = Parse(ptr);
	delete [] ptr;
	return ret;
}


bool CMiniXML::Save(const TCHAR * fname)
{
	FILE * fp = NULL;

#if defined(_MSC_VER) && (_MSC_VER >= 1400 )
	_tfopen_s(&fp, fname, _T("wb"));
#elif defined(_MSC_VER) && (_MSC_VER >= 1200 )
	fp = _tfopen(fname, _T("wb"));
#endif

	if (fp == NULL)
		return false;

#ifdef _UNICODE
	static const unsigned char _unicodeheader[2] = { 0xff, 0xfe };
	fwrite(_unicodeheader, 2, 1, fp);
#endif

	Store(fp, m_Root.child, &m_Root);

	fclose(fp);
	return true;
}

static void _ftputs2(FILE *fp, const TCHAR * str)
{
	_string text = _string(str);
	int i;

	for(i=0; i<sizeof(_convtable)/sizeof(*_convtable); i++)
	{
		const TCHAR * key = _tcsstr(text.c_str(), _convtable[i][1]);
		while(key != NULL)
		{
			int off = (int)(key - text.c_str());
			text.replace(off, _tcslen(_convtable[i][1]), _convtable[i][0]);
			key = _tcsstr(text.c_str() + off, _convtable[i][1]);
		}
	}

	_ftprintf(fp, _T("%s"), text.c_str());
}


void CMiniXML::Store(FILE *fp, _NODE * node, _NODE * parent, int lv, bool notab)
{
	_ITEM * item;
	int i;
	bool cr;

	if (node == NULL)
		return ;

	do
	{
		if (notab == false)
		{
			for(i=0; i<lv; i++)
				_ftprintf(fp, _T("\t"));
		}	else
		{
			notab = false;
		}
//		_ftprintf(fp, _T("<%s"), node->name.c_str());
		_ftprintf(fp, _T("<"));
		_ftputs2(fp, node->name.c_str());

		for(item=node->item; item; item=item->next)
		{
//			_ftprintf(fp, _T(" %s=\"%s\""), item->name.c_str(), item->value.c_str());
			_ftprintf(fp, _T(" "));
			_ftputs2(fp, item->name.c_str());
			_ftprintf(fp, _T("=\""));
			_ftputs2(fp, item->value.c_str());
			_ftprintf(fp, _T("\""));
		}

		if (_tcslen(node->value.c_str()) > 0)
		{
//			_ftprintf(fp, _T(">%s"), node->value.c_str());
			_ftprintf(fp, _T(">"));
			_ftputs2(fp, node->value.c_str());
			cr = false;
		}	else
		{
			_ftprintf(fp, _T(">"));

			if (node->child != NULL)
			{
				_ftprintf(fp, _T("\r\n"));
				cr = true;
			}	else
				cr = false;
		}

		if (node->child != NULL)
		{
			Store(fp, node->child, node, lv+1, cr == false ? true : false);
			cr = true;
		}

		if (cr == true)
		{
			for(i=0; i<lv; i++)
				_ftprintf(fp, _T("\t"));
		}
//		_ftprintf(fp, _T("</%s>\r\n"), node->name.c_str());
		_ftprintf(fp, _T("</"));
		_ftputs2(fp, node->name.c_str());
		_ftprintf(fp, _T(">\r\n"));
		node = node->next;
	}	while(node != parent->child) ;
}


bool CMiniXML::Parse(const TCHAR * p)
{
	do
	{
		p = ParseXML(p, &m_Root);
	}	while(p != NULL && *p != '\0');
	return m_Root.child != NULL ? true : false;
}


TCHAR * CMiniXML::ReadText(const TCHAR * fname, bool forceutf9)
{
	unsigned char * ptr8;
	int len;

	ptr8 = ReadFile(fname, &len);

	if (ptr8 == NULL)
		return NULL;

	return ReadReal( ptr8, len, true, forceutf9 );
}


TCHAR * CMiniXML::ReadReal( unsigned char * dest, int len, bool opened, bool forceutf9 )
{
	TCHAR * ptr;

	if (dest[0] == 0xff && dest[1] == 0xfe)
	{	//	unicode
#ifdef _UNICODE
		ptr = new TCHAR [(len-2)/2 + 1];
		memcpy(ptr, &dest[2], len-2);
		ptr[(len-2)/2] = '\0';
		if(opened)
			delete [] dest;
		return ptr;
#else
		int cnt = WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)&dest[2], (len-2)/2, NULL, 0, NULL, FALSE);
		ptr = new TCHAR [cnt+1];
		WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)&dest[2], (len-2)/2, ptr, cnt, NULL, FALSE);
		ptr[cnt] = '\0';

		if(opened)
			delete [] dest;
		return ptr;
#endif
	}	else
	if (forceutf9 == true ||
		dest[0] == 0xef && dest[1] == 0xbb && dest[2] == 0xbf)
	{	//	Universal Set Transformation Format 8bit
		unsigned short code;
		int cnt, i, startpos = dest[0] == 0xef && dest[1] == 0xbb && dest[2] == 0xbf ? 3 : 0;

		for(i=startpos, cnt=0; i<len; )
		{
			if (dest[i] & 0x80)
			{
				i += (dest[i]&0xe0) == 0xc0 ? 2 : 3;
#ifdef _UNICODE
				cnt++;
#else
				cnt += 2;
#endif
			}	else
			{
				i ++;
				cnt++ ;
			}
		}
		ptr = new TCHAR [cnt + 1];
		for(i=startpos, cnt=0; i<len;)
		{
			if (dest[i] & 0x80)
			{
				if ((dest[i]&0xe0) == 0xc0)
				{
					code = ((dest[i] & 0x1f) << 6) | (dest[i+1] & 0x3f);
					i += 2;
				}	else
				{
					code = ((dest[i] & 0x0f) << 12) | ((dest[i+1] & 0x3f) << 6) | (dest[i+2] & 0x3f);
					i += 3;
				}
#ifdef _UNICODE
				ptr[cnt] = code;
				cnt ++;
#else
				WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)&code, 1, &ptr[cnt], 2, NULL, FALSE);
				cnt += 2;
#endif
			}	else
			{
				ptr[cnt] = dest[i];
				i ++;
				cnt ++;
			}
		}
		ptr[cnt] = '\0';
		if(opened)
			delete [] dest;
		return ptr;
	}	else
	{
#ifdef _UNICODE
		int cnt = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)dest, len, NULL, 0);
		ptr = new TCHAR [cnt+1];
//		MultiByteToWideChar(CP_OEMCP, 0, (LPCSTR)dest, len, ptr, cnt);
		MultiByteToWideChar(CP_ACP, 0, (LPCSTR)dest, len, ptr, cnt);
		ptr[cnt] = '\0';
		if(opened)
			delete [] dest;
		return ptr;
#else
		ptr = new TCHAR [len+1];
		memcpy(ptr, dest, len);
		dest[len] = '\0';
		if(opened)
			delete [] dest;
		return ptr;
#endif
	}
	return NULL;
}

unsigned char * CMiniXML::ReadFile(const TCHAR * fname, int * len)
{
	unsigned char * ptr;
	FILE * fp = NULL;

#if defined(_MSC_VER) && (_MSC_VER >= 1400 )
	_tfopen_s(&fp, fname, _T("rb"));
#elif defined(_MSC_VER) && (_MSC_VER >= 1200 )
	fp = _tfopen(fname, _T("rb"));
#endif

	if (fp == NULL)
		return NULL;

	fseek(fp, 0, SEEK_END);
	*len = ftell(fp);
	ptr = new unsigned char [*len];
	fseek(fp, 0, SEEK_SET);
	fread(ptr, 1, *len, fp);
	fclose(fp);

	return ptr;
}


///


CMiniXML::node::node()
{
	m_pNode		= NULL;
	m_pParent	= NULL;
	m_Accessed	= false;
}


CMiniXML::node::node(_NODE * node, _NODE * parent, bool accessed)
{
	m_pNode		= node;
	m_pParent	= parent;
	m_Accessed	= accessed;
}


CMiniXML::node::node(const node & n)
{
	m_pNode		= n.m_pNode;
	m_pParent	= n.m_pParent;
	m_Accessed	= n.m_Accessed;
}


const TCHAR * CMiniXML::node::GetName() const
{
	return m_pNode->name.c_str();
}


void CMiniXML::node::SetName(const TCHAR * name)
{
	m_pNode->name = name;
}


const TCHAR * CMiniXML::node::GetValue() const
{
	return m_pNode->value.c_str();
}


void CMiniXML::node::SetValue(const TCHAR *value)
{
	if (m_pNode != NULL)
		m_pNode->value = value;
}


const TCHAR * CMiniXML::node::GetAttribute(const TCHAR * att) const
{
	for(_ITEM * item=m_pNode->item; item!=NULL; item=item->next)
		if (_tcscmp(item->name.c_str(), att) == 0)
			return item->value.c_str();
	return NULL;
}


void CMiniXML::node::SetAttribute(const TCHAR * name, const TCHAR * value)
{
	if (m_pNode != NULL)
	{
		_ITEM * item;
		for(item=m_pNode->item; item != NULL; item=item->next)
		{
			if (_tcscmp(item->name.c_str(), name) == 0)
			{
				item->value = value;
				return;
			}
		}
		item = new _ITEM;
		item->name = name;
		item->value = value;
		item->next = m_pNode->item;
		m_pNode->item = item;
	}
}


void CMiniXML::node::DeleteAttribute(const TCHAR * name)
{
	if (m_pNode != NULL)
	{
		_ITEM * item, * prev = NULL;
		for(item=m_pNode->item; item != NULL; item=item->next)
		{
			if (_tcscmp(item->name.c_str(), name) == 0)
			{
				if (prev == NULL)
					m_pNode->item = item->next;
				else
					prev->next = item->next;
				delete item;
				return;
			}
			prev = item;
		}
	}
}


bool CMiniXML::node::HasChild() const
{
	return m_pNode->child != NULL ? true : false;
}


CMiniXML::node CMiniXML::node::Next() const
{
	return CMiniXML::node(m_pNode->next != m_pParent->child ? m_pNode->next : NULL, m_pParent, false);
}


void CMiniXML::node::operator ++ ()
{	// Prefix increment operator
	m_pNode = m_pNode->next != m_pParent->child ? m_pNode->next : NULL;
	m_Accessed = false;
}


void CMiniXML::node::operator ++ (int)
{	// Postfix increment operator
	operator ++ ();
}

CMiniXML::node CMiniXML::node::Find(const TCHAR * name) const
{
	_NODE * node = m_pNode;
	if (node != NULL)
	{
		do
		{
			if ((m_Accessed == false || node != m_pNode) && //	find 해서 찾은 노드라면 자신 노드에서 검색 성공하게 되므로, 자기 자신은 skip
				_tcscmp(node->name.c_str(), name) == 0)
			{
				return CMiniXML::node(node, m_pParent, true);
			}
			node = node->next;
		}	while(node != m_pParent->child);
	}
	return CMiniXML::node(NULL, NULL);
}


CMiniXML::node CMiniXML::node::GetChildNode() const
{
	return CMiniXML::node(m_pNode->child, m_pNode);
}


bool CMiniXML::node::operator == (const void * ptr) const
{
	return m_pNode == ptr ? true : false;
}


bool CMiniXML::node::operator != (const void * ptr) const
{
	return m_pNode != ptr ? true : false;
}