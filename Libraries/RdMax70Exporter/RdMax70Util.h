#ifndef __RDMAX70UTIL_H__
#define __RDMAX70UTIL_H__

#pragma once

#include "RdMax70ExporterHeader.h"

#define SAFEDEL( x )		{ if( x ) {	delete (x);		(x) = NULL;		} }
#define SAFEDELS( x )		{ if( x ) {	delete[] (x);	(x) = NULL;		} }

//==================================================================
// Debug
//==================================================================
static int AssertFailedFunc( TCHAR* sz )
{
	MessageBox( GetActiveWindow(), sz, _T( "Assert failure" ), MB_OK );
	int Set_Your_Breakpoint_Here = 1;
	return 1;
}

#define ASSERT_MBOX(f, sz)	( (f) ? 1 : AssertFailedFunc(sz) )

#define ASSERT_AND_ABORT(f, sz)							\
	if (!(f))											\
	{													\
		ASSERT_MBOX(FALSE, sz);							\
		cleanup( );										\
		return TREE_ABORT;								\
	}

//==================================================================
// 
//==================================================================
struct NodeName
{
	TCHAR		Name[MAX_LEN];
	ushort		ID;
};

extern NodeName g_arNodeNameList[1024];
extern ushort g_nNodeCount;

//==================================================================
// 
//==================================================================
#define UNDESIRABLE_MARKER		0xffff

bool	UndesirableNode( INode* pNode );
void	SetIndexOfNode( INode* pNode, ushort nNode );
ushort	GetIndexOfNode( INode* pNode );

bool	IsDummyNode( INode* pNode );
bool	IsMeshNode( INode* pNode );
bool	IsBoneNode( INode* pNode );
bool	IsBoneDummyNode( const TCHAR* strName );
bool	IsDummyBoneNode( const TCHAR* strName );
bool	IsBoundingBoxNode( const TCHAR* strName );
bool	IsMultiTextured( StdMat2* pMtl );
bool	IsShellMaterial( const TCHAR* strName );

Modifier*	FindModifier( Object* pObject, Class_ID classModID );

bool	IsTMNegParity( Matrix3 tm );

//==================================================================
// Chunk 
//==================================================================
void	WriteChunkBegin( FILE* pFile, ulong ulTag, ulong& ulPtr, ulong& ulBegin );
void	WriteChunkEnd( FILE* pFile, ulong ulPtr, ulong ulBegin );

void	WriteString( FILE* pFile, const TCHAR* strString );

//==================================================================
void ConvertMatrixToD3D( Matrix3* pMat );

bool Point3Equal( const Point3& rA, const Point3& rB );
bool FloatEqual( float fA, float fB );
bool QuatEqual( const Quat& rA, const Quat& rB );

void Uniform_Matrix( Matrix3& In, Matrix3& Out );
void Transpose_Matrix( Matrix3& In, Matrix3& Out );

//==================================================================
void AddStringInListBox( HWND hDlg, int nDlgItem, const TCHAR* strString );


void		OutputMsg( const TCHAR* pFormat, ... );

//==================================================================
class VNormal
{
public:
	Point3		normal;
	ulong		smooth;
	VNormal*	next;

	bool		init;

	VNormal();
	VNormal( Point3& n, ulong s );
	~VNormal();

	void		AddNormal( Point3& n, ulong s );
	Point3&		GetNormal( ulong s );

	void		Normalize();
};

#endif // __RDMAX70UTIL_H__