#ifndef __RDMAX70MESHEXP_H__
#define __RDMAX70MESHEXP_H__

#pragma once

#include "..\Data\RdMax70Mesh.h"
#include "..\Data\RdMax70Material.h"
#include "..\RdMax70Util.h"

#include "../Data/RdMax70BBox.h"
#include "../Dependency/ISkin.h"

class RdMeshExp
{
	friend class RdStaticMeshExpUtil;
	friend class RdSkinMeshExpUtil;
protected:
	Interface*				m_pInterface;
	bool					m_bSelectedExp;
	HWND					m_hMeshExpDlg;

	IPhyContextExport*		m_pcExport;
	IPhysiqueExport*		m_phyExport;
	Modifier*				m_phyMod;

	Modifier*				m_skinMod;
	ISkin*					m_skinInterface;
	ISkinContextData*		m_skinContext;

	Tab<RdMeshNode*>		m_NodeList;
	Tab<RdMaterial*>		m_MtlList;	
	Tab<RdBBox*>			m_BBoxList;

	Tab<RdNormFaceNode*>	m_IndoorNodeList;
	RdNormFaceNode*			m_pHeightNode;
	
	Point3					m_ptTraceTop;
	Point3					m_ptTraceBottom;

	bool					m_bTraceTop;
	bool					m_bTraceBottom;

	bool					m_bEnableExport;
	bool					m_bDummyExport;

	bool					m_bApplyScale;
	bool					m_bVertexColor;
	bool					m_bVertTangent;

	void			Clear();
	void			UpdateMaterial();

	void			CollectNode( INode* pNode );

	void			CollectNodeData( INode* pNode, RdMeshNode* pNodeData, bool bNeg, bool bForce = false );
	void			CollectMeshNode( INode* pNode, RdMeshNode* pNodeData );

	void			CollectMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, IPhyContextExport* pExport, ISkinContextData* pSkinExp, bool bNeg );
	void			CollectPhyMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, IPhyContextExport* pExport, bool bNeg );
	void			CollectSkinMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, ISkinContextData* pExport, bool bNeg );
	void			CollectStaticMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, bool bNeg );

	void			CollectMaterialData( Mtl* pNodeMtl, RdMeshNode* pNodeData, bool bMultiMtl );

	bool			CollectFaceNormNode( INode* pNode, RdNormFaceNode* pNodeData );

	// Bounding Box
	void			CollectBBoxData( INode* pNode );

	ushort			AddMaterial( Mtl* pParentMtl, Mtl* pNodeMtl );
	bool			HasSameMaterial( ushort usMtlID, int nNumMtls, int nCurrentMtl );

	// Texture
	void			DumpMultiTexture( StdMat2* pCurMtl, RdMaterial* pCurMaterial );

	// 이함수 수정했다. 페이스 뒤집어 지던 넘들 땜에... 근데 왜 여태까지 큰 문제가 안됐었는지 몰겠다..-_-;; 
	void			SeperateMeshNode( RdMeshNode* pOrigin );

	void			NodeCleanUp();

	/** Vertex Normal을 계산합니다.
		@remarks
			discreet의 tech 문서에 나와있는 대로 구현.
	*/
	void			ComputeVertexNormals( Mesh* pMesh, bool bNeg, Tab<VNormal>& rVNorms );
	void			ComputeVertexTangents( RdMeshNode* pNodeData, Tab<RdVertex>& rVertices, Tab<RdFace>& rFaces );

	/** Smooting Group을 이용한 노말 구하기
	*/
	static void		GetNormalVertexCount( Mesh* pMesh, Tab<RdVertex>& rVertices );
	static ulong	GetNormalVertexIndex( int nIndex, ulong ulSmGroup, const Tab<RdVertex>& rVertices );

	bool			IsHeightNode( const TCHAR* strName );
	bool			IsIndoorNode( const TCHAR* strName );
	bool			IsMeshDummyNode( const TCHAR* strName );
	bool			IsTraceTopNode( const TCHAR* strName );
	bool			IsTraceBottomNode( const TCHAR* strName );

	void			DumpNodeList( FILE* pFile );

	void			DumpMeshNode( FILE* pFile, RdMeshNode* pNode );
	void			DumpSkinData( FILE* pFile, RdMeshNode* pNode );
	void			DumpVertexData( FILE* pFile, RdMeshNode* pNode, bool bPhysic );
	void			DumpFaceData( FILE* pFile, RdMeshNode* pNode );
	void			DumpMaterialData( FILE* pFile, RdMaterial* pMtl );

	void			DumpNormFaceNode( FILE* pFile, RdNormFaceNode* pNode );

//	void			DumpMeshData( FILE* pFile );
	void			DumpBBox( FILE* pFile );

	RdMeshNode*		GetNode( ushort usIndex );
	RdMeshNode*		GetNode( const TCHAR* strName );
public:
	RdMeshExp( Interface* pInterface, HWND hWnd, bool bSelected = false );
	~RdMeshExp();

	void			Initialize();
	void			OutputInformation();

	void			SetApplyScale( bool bApply )			{		m_bApplyScale = bApply;		}

	void			UseVertexColor( bool bUse )				{		m_bVertexColor = bUse;		}
	bool			IsUseVertexColor() const				{		return m_bVertexColor;		}

	void			UseVertTangent( bool bUse )				{		m_bVertTangent = bUse;		}
	bool			IsUseVertTangent() const				{		return m_bVertTangent;		}

	void			ExportMesh( const TCHAR* strName );
	void			ExportMaterial( const TCHAR* strName );
};


//==================================================================
/** RdMeshExpUtil...
	@remarks
		Mesh의 성격에 따른 Export를 하기 위함이다..
*/
//==================================================================
class RdMeshExpUtil
{
protected:
	virtual void	CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
									 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces ) = 0;

	virtual void	CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
									 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces ) = 0;

	/** 중복된 버텍스를 정리한다..
	*/
	virtual void	CheckOverlappedVertex( Tab<RdVertex>& rVerts, Tab<RdFace>& rFaces,
										  Tab<RdVertex>& rSrcVerts );

public:
	/** 버텍스 버퍼 구하기
	*/
	virtual void	CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices ) = 0;

	/** Vertex의 UV를 뽑는다.
	*/
	void			CollectUV(	Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
								Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces,
								bool bHasMultiUV );
};

class RdStaticMeshExpUtil : public RdMeshExpUtil
{
protected:
	void	CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );

	void	CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );

public:
	void	CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices );
};

class RdSkinMeshExpUtil : public RdMeshExpUtil
{
protected:
	void	CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );

	void	CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
							Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces );

public:
	void	CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices );
};

#endif // __RDMAX70MESHEXP_H__