#include "RdMax70MeshExp.h"

#define ENABLE_BONECNT		27
#define ENABLE_MAXLINKCNT	4

//------------------------------------------------------------------
// Global
//------------------------------------------------------------------
Modifier* FindPhysiqueModifier( INode* pNode )
{
	Object* pObj = pNode->GetObjectRef();

	if( !pObj )
		return NULL;

	while( pObj->SuperClassID() == GEN_DERIVOB_CLASS_ID )
	{
		IDerivedObject* pDerivObj = static_cast<IDerivedObject*>(pObj);

		int nModStackIndex = 0;
		while( nModStackIndex < pDerivObj->NumModifiers() )
		{
			Modifier* pModifier = pDerivObj->GetModifier( nModStackIndex );

			if( pModifier->ClassID() == Class_ID( PHYSIQUE_CLASS_ID_A, PHYSIQUE_CLASS_ID_B ) )
				return pModifier;

			++nModStackIndex;
		}
		pObj = pDerivObj->GetObjRef();
	}

	return NULL;
}

//-------------------------------------------------------------------------
//	FindSkinModifier
//-------------------------------------------------------------------------
Modifier *FindSkinModifier( INode *pNode )
{
	Object *pObj = pNode->GetObjectRef();
	if( !pObj )
		return NULL;

	while( pObj->SuperClassID() == GEN_DERIVOB_CLASS_ID )
	{
		IDerivedObject *pDerivedObj = static_cast<IDerivedObject*>(pObj);

		int stackIdx = 0;
		while(stackIdx < pDerivedObj->NumModifiers())
		{
			Modifier* pMod = pDerivedObj->GetModifier(stackIdx++);
			if(pMod->ClassID() == SKIN_CLASSID)
				return pMod;
		}
		pObj = pDerivedObj->GetObjRef();
	}

	return NULL;
}

Point3 GetRVertexNormal( RVertex* pVertex, DWORD dwSmGroupFace )
{
	if( pVertex->rFlags & SPECIFIED_NORMAL )
		return pVertex->rn.getNormal();

	int nNormals = pVertex->rFlags & NORCT_MASK;

	ASSERT_MBOX( ( nNormals == 1 && pVertex->ern == NULL ) || 
		( nNormals > 1 && pVertex->ern != NULL ), _T( "BOGUS RVERTEX" ) );

	if( nNormals == 1 )
		return pVertex->rn.getNormal();
	else
	{
		DWORD smgroup = 0;
		int i = 0;
		for( i = 0; i < nNormals; i++ )
		{
			smgroup = pVertex->ern[i].getSmGroup();
			if( smgroup & dwSmGroupFace )
				break;
		}

		if( i >= nNormals )
			i = 0;

		return pVertex->ern[i].getNormal();
	}

	return Point3( 0.0f, 0.0f, 0.0f );
}

void GetPhysiqueWeights( IPhyContextExport* pContextExp, int nVertex, RdVertex* pVertex )
{
	IPhyVertexExport* pExport = (IPhyVertexExport*)pContextExp->GetVertexInterface( nVertex );
	if( pExport )
	{
		INode* pBone;
		int vType = pExport->GetVertexType();
		if( RIGID_TYPE == vType )
		{
			pBone = ((IPhyRigidVertex*)pExport)->GetNode();

			pVertex->Link = 1;
			pVertex->Blend[0].BoneID = GetIndexOfNode( pBone );
			pVertex->Blend[0].Weight = 1.0f;
		}
		else if( RIGID_BLENDED_TYPE == vType )
		{
			IPhyBlendedRigidVertex* pBlend = (IPhyBlendedRigidVertex*)pExport;
			pVertex->Link = pBlend->GetNumberNodes();

			// 최대 링크가 4가 넘더라도 4개까지만 저장되고 에러처리 될 것이다.
			float fWeight = 0.0f;

			for( int i = 0; i < pVertex->Link; ++i )
			{
				pBone = pBlend->GetNode( i );
				fWeight = pBlend->GetWeight( i );

				if( i > 3 )
				{
					for( int j = 0; j < 4; ++j )
					{
						if( pVertex->Blend[j].Weight < fWeight )
						{
							pVertex->Blend[j].BoneID = GetIndexOfNode( pBone );
							pVertex->Blend[j].Weight = fWeight;
							break;
						}
					}
				}
				else
				{
					pVertex->Blend[i].BoneID = GetIndexOfNode( pBone );
					pVertex->Blend[i].Weight = fWeight;
				}		
			}

			if( pVertex->Link > 4 )
			{
				float fTotal = 0.0f;
				for( int j = 0; j < 3; ++j )
				{
					fTotal += pVertex->Blend[j].Weight;
				}

				pVertex->Blend[3].Weight = 1.0f - fTotal;
			}
		}

		pContextExp->ReleaseVertexInterface( pExport );
	}
}

struct sVertex
{
	ushort index;
	float weight;
};

int CompareVert( const void* elem1, const void* elem2 )
{
	sVertex* pVert1 = (sVertex*)elem1;
	sVertex* pVert2 = (sVertex*)elem2;

	return (pVert1->weight < pVert2->weight);
}

void GetSkinWeights( ISkin* pSkinInterface, ISkinContextData* pContextExp, Modifier* pMod, int nVertex, RdVertex* pVertex )
{
	// 최대 링크가 4가 넘더라도 4개까지만 저장되고 에러처리 될 것이다.
	float fWeight = 0.0f;

	int nAssignedBone = 0;
	INode* pBone = NULL;

	Tab<sVertex> list;
	list.Init();

	int nSize = 0;

	pVertex->Link = pContextExp->GetNumAssignedBones( nVertex );
	for( int i = 0; i < pVertex->Link; ++i )
	{
		nAssignedBone = pContextExp->GetAssignedBone( nVertex, i );
		if( nAssignedBone < 0 )
			continue;

		pBone = pSkinInterface->GetBone( nAssignedBone );
		fWeight = pContextExp->GetBoneWeight( nVertex, i );

		ushort index = GetIndexOfNode( pBone );
		if( UNDESIRABLE_MARKER != index )
		{
			sVertex vert;
			vert.index = index;
			vert.weight = fWeight;

			list.Append( 1, &vert );

			nSize++;
		}
	}

	list.Sort( CompareVert );

	if( nSize > 4 )
	{
		float fTotal = 0.0f;
		for( int j = 0; j < 3; ++j )
		{
			pVertex->Blend[j].BoneID = list[j].index;
			pVertex->Blend[j].Weight = list[j].weight;

			fTotal += pVertex->Blend[j].Weight;
		}

		pVertex->Blend[3].BoneID = list[3].index;
		pVertex->Blend[3].Weight = 1.0f - fTotal;
	}
	else
	{
		for( int j = 0; j < nSize; ++j )
		{
			pVertex->Blend[j].BoneID = list[j].index;
			pVertex->Blend[j].Weight = list[j].weight;
		}
	}

	list.Delete( 0, nSize );
}

//------------------------------------------------------------------
// Class
//------------------------------------------------------------------
RdMeshExp::RdMeshExp( Interface* pInterface, HWND hWnd, bool bSelected )
: m_pInterface( pInterface ), m_hMeshExpDlg( hWnd ), m_bSelectedExp( bSelected )
{
	m_bEnableExport	= true;

	m_NodeList.Init();
	m_NodeList.ZeroCount();

	m_MtlList.Init();
	m_MtlList.ZeroCount();

	m_IndoorNodeList.Init();
	m_IndoorNodeList.ZeroCount();

	m_ptTraceTop.Set( 0.0f, 0.0f, 0.0f );
	m_ptTraceBottom.Set( 0.0f, 0.0f, 0.0f );

	m_bTraceTop		= false;
	m_bTraceBottom	= false;

	m_pcExport		= NULL;
	m_phyExport		= NULL;
	m_phyMod		= NULL;

	m_pHeightNode	= NULL;

	m_bApplyScale	= false;
	m_bVertexColor	= false;
	m_bVertTangent	= false;
}

RdMeshExp::~RdMeshExp()
{
	Clear();
}

void RdMeshExp::Clear()
{
	int nCount = m_BBoxList.Count();
	RdBBox** ppBox;
	for( int i = 0; i < nCount; ++i )
	{
		ppBox = m_BBoxList.Addr( i );
		delete (*ppBox);
	}
	m_BBoxList.Delete( 0, nCount );

	nCount = m_NodeList.Count();
	RdMeshNode** ppNode;
	for( int i = 0; i < nCount; ++i )
	{
		ppNode = m_NodeList.Addr( i );

		delete (*ppNode);
	}
	m_NodeList.Delete( 0, nCount );

	nCount = m_MtlList.Count();
	RdMaterial** ppMtl;
	for( int i = 0; i < nCount; ++i )
	{
		ppMtl = m_MtlList.Addr( i );

		delete (*ppMtl);
	}
	m_MtlList.Delete( 0, nCount );

	nCount = m_IndoorNodeList.Count();
	RdNormFaceNode** ppFace;
	for( int i = 0; i < nCount; ++i )
	{
		ppFace = m_IndoorNodeList.Addr( i );

		delete (*ppFace );
	}
	m_IndoorNodeList.Delete( 0, nCount );
}

//------------------------------------------------------------------
// 정보 수집 함수들
//------------------------------------------------------------------
bool RdMeshExp::IsHeightNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("MH_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("mh_"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsIndoorNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("CL_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("cl_"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsMeshDummyNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("DM_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("dm_"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsTraceTopNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("TTD"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("ttd"), 3 ) == 0 )
		return true;

	return false;
}

bool RdMeshExp::IsTraceBottomNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("TBD"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("tbd"), 3 ) == 0 )
		return true;

	return false;
} 

//------------------------------------------------------------------
void RdMeshExp::Initialize()
{
	int nChildNum = m_pInterface->GetRootNode()->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectNode( m_pInterface->GetRootNode()->GetChildNode( i ) );
	}

	for( int i = 0; i < nChildNum; ++i )
	{
		CollectBBoxData( m_pInterface->GetRootNode()->GetChildNode(i) );
	}

	if( m_NodeList.Count() == 0 )
		m_bEnableExport = false;

	if( ( !m_bTraceTop && m_bTraceBottom ) || ( m_bTraceTop && !m_bTraceBottom ) )
		m_bEnableExport = false;
}

//------------------------------------------------------------------
void RdMeshExp::OutputInformation()
{
	// Material 정보
	ushort usMtlCnt = m_MtlList.Count();
	RdMaterial* pMtl;
	for( ushort i = 0; i < usMtlCnt; ++i )
	{
		pMtl = m_MtlList[i];
		SendDlgItemMessage( m_hMeshExpDlg, IDC_MATERIAL_LIST, LB_ADDSTRING, NULL, (LPARAM)pMtl->GetMaterialName() );
	}

	if( m_bEnableExport )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("***** Enable to Export *****") );
	}
	else
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("----- Disable to Export -----") );
		HWND hWnd = GetDlgItem( m_hMeshExpDlg, IDC_MESH_SAVE );
		EnableWindow( hWnd, FALSE );
	}

	AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("===================================") );

	// Mesh 정보
	ulong ulVertCnt = 0;
	ulong ulFaceCnt = 0;
	ushort usBoneCnt = 0;
	ushort usMaxLink = 0;

	ushort usMeshNodeCnt = 0;
	ushort usDummyNodeCnt = 0;

	RdMeshNode* pMesh;
	TCHAR strMsg[256];

	ushort usNodeCnt = m_NodeList.Count();

	for( ushort i = 0; i < usNodeCnt; ++i )
	{
		pMesh = (RdMeshNode*)(m_NodeList[i]);

		memset( strMsg, 0, sizeof(TCHAR)*256 );

		if( pMesh->GetNodeType() == 0 )
		{
			_stprintf( strMsg, _T( "Mesh Node : [%d] %s" ), pMesh->GetNodeID(), pMesh->GetNodeName() );
			++usMeshNodeCnt;
		}
		else
		{
			_stprintf( strMsg, _T( "Dummy Node : [%d] %s" ), pMesh->GetNodeID(), pMesh->GetNodeName() );		
			++usDummyNodeCnt;
		}

		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );

		if( pMesh->GetNodeType() == 0 )
		{
			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tVertex Count : %d" ), pMesh->GetVertexCount() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			ulVertCnt += pMesh->GetVertexCount();

			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tFace Count : %d" ), pMesh->GetFaceCount() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			ulFaceCnt += pMesh->GetFaceCount();

			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tLinked Bone Count : %d" ), pMesh->GetLinkedBoneCount() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			if( usBoneCnt < pMesh->GetLinkedBoneCount() )
				usBoneCnt = pMesh->GetLinkedBoneCount();

			if( pMesh->GetLinkedBoneCount() > ENABLE_BONECNT )
				AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("[Error]한 옵젝에서 27개의 본만 사용할 수 있습니다." ) );

			memset( strMsg, 0, sizeof(TCHAR)*256 );
			_stprintf( strMsg, _T( "\tLink Count : %d" ), pMesh->GetMaxLink() );
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
			if( usMaxLink < pMesh->GetMaxLink() )
				usMaxLink = pMesh->GetMaxLink();

			if( pMesh->GetMaxLink() > ENABLE_MAXLINKCNT )	
			{
				AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("[Warning]버텍스 링크 개수가 커서 애니메이션이 찢어질 수 있습니다." ) );
			}
		}

		if( pMesh->GetEnableVertexColor() )
		{
			AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("::Has VertexColor") );
		}

		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("===================================") );
	}


	int nNumBBox = m_BBoxList.Count();
	for( int i = 0; i < nNumBBox; ++i )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("Custom Bounding Box") );
		memset( strMsg, 0, sizeof(TCHAR)*256 );
		_stprintf( strMsg, _T( "\t-Linked Node: %s"), m_BBoxList[i]->GetParentName() ) ;
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
	}

	if( nNumBBox > 0 )
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("===================================") );

	if( m_pHeightNode )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("**! It exist already the height node. !**") );
	}

	if( m_IndoorNodeList.Count() != 0 )
	{
		memset( strMsg, 0, sizeof(TCHAR)*256 );
		_stprintf( strMsg,  _T("**! Indoor Node Count : %d !**"), m_IndoorNodeList.Count() );
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, strMsg );
	}

	if( m_bTraceTop )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("**! This Mesh include the Trace Top Information. !**") );
	}

	if( m_bTraceBottom )
	{
		AddStringInListBox( m_hMeshExpDlg, IDC_MESH_LIST, _T("**! This Mesh include the Trace Bottom Information !**") );
	}

	SetDlgItemInt( m_hMeshExpDlg, IDC_VERTEX_CNT, ulVertCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_FACE_CNT, ulFaceCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_BONE_CNT, usBoneCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_LINK_CNT, usMaxLink, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_OBJECT_CNT, usMeshNodeCnt, FALSE );
	SetDlgItemInt( m_hMeshExpDlg, IDC_MDUMMY_CNT, usDummyNodeCnt, FALSE );
}

//------------------------------------------------------------------
void RdMeshExp::CollectNode( INode* pNode )
{
	if( UndesirableNode( pNode ) )
		return;

	if( m_bSelectedExp && pNode->Selected() == FALSE )
		return;

	if( !m_bSelectedExp || pNode->Selected() )
	{
		if( _tcscmp( pNode->GetName(), "Bip01 Footsteps" ) == 0 )
			return;

		ushort id = GetIndexOfNode( pNode );
		if( IsMeshNode( pNode ) )
		{
			if( IsBoneDummyNode( pNode->GetName() ) || IsBoundingBoxNode( pNode->GetName() ) ) 
			{

			}
			else if( IsHeightNode( pNode->GetName() ) )
			{
				m_pHeightNode = new RdNormFaceNode();
				if( !CollectFaceNormNode( pNode, m_pHeightNode ) )
				{
					SAFEDEL( m_pHeightNode );
				}
			}
			else if( IsIndoorNode( pNode->GetName() ) )
			{
				RdNormFaceNode* pNodeData = new RdNormFaceNode();
				if( !CollectFaceNormNode( pNode, pNodeData ) )
				{
					SAFEDEL( pNodeData );
				}
				else
				{
					m_IndoorNodeList.Append( 1, &pNodeData );
				}
			}
			else if( IsTraceTopNode( pNode->GetName() ) )
			{
				m_bTraceTop = true;

				Matrix3 world = pNode->GetNodeTM( 0 );
				AffineParts ap;
				decomp_affine( world, &ap );
				m_ptTraceTop.Set( ap.t.x, ap.t.z, ap.t.y );
			}
			else if( IsTraceBottomNode( pNode->GetName() ) )
			{
				m_bTraceBottom = true;

				Matrix3 world = pNode->GetNodeTM( 0 );
				AffineParts ap;
				decomp_affine( world, &ap );
				m_ptTraceBottom.Set( ap.t.x, ap.t.z, ap.t.y );
			}
			else
			{
				TCHAR buffer[256];

				int nNodes = m_NodeList.Count();
				for( int i = 0; i < nNodes; ++i )
				{
					if( 0 == _tcscmp( m_NodeList[i]->GetNodeName(), pNode->GetName() ) )
					{
						_stprintf( buffer, _T( "동일한 MeshNode 이름 [%s]발견.. 수정해 주세요." ), pNode->GetName() );
						ASSERT_MBOX( FALSE, buffer );
						break;
					}
				}

				RdMeshNode* pNodeData = new RdMeshNode( 0, id, pNode->GetName() );
				CollectMeshNode( pNode, pNodeData );

				//if( pNodeData->GetVertexCount() == 0 || pNodeData->GetFaceCount() == 0 )
				//{
				//	SAFEDEL( pNodeData );
				//}
				//else
				//{
				//	m_NodeList.Append( 1, &pNodeData );
				//}
			}
		}
		else if( IsMeshDummyNode( pNode->GetName() ) )
		{
			RdMeshNode* pNodeData = new RdMeshNode( 2, id, pNode->GetName() );
			m_NodeList.Append( 1, &pNodeData );
			CollectNodeData( pNode, pNodeData, false, false );
		}

		//else if( IsDummyNode( pNode ) )
		//{
		//	RdMeshNode* pNodeData = new RdMeshNode( 2, id, pNode->GetName() );
		//	m_NodeList.Append( 1, &pNodeData );
		//	CollectNodeData( pNode, pNodeData, false );
		//}
	}

	int nChildNum = pNode->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectNode( pNode->GetChildNode( i ) ); 
	}
}

//------------------------------------------------------------------
void RdMeshExp::CollectNodeData( INode* pNode, RdMeshNode* pNodeData, bool bNeg, bool bForce )
{
	INode* pParent = pNode->GetParentNode();

	if( pParent && !pParent->IsRootNode() )
	{
		pNodeData->SetParentNode( GetIndexOfNode( pParent ), pParent->GetName() );
	}

	if( !bForce )
	{
		Matrix3 world = pNode->GetNodeTM( 0 );
		Matrix3 local = world;

		if (!bNeg)
		{
			if( pParent && !pParent->IsRootNode() )
				local = world * Inverse( pParent->GetNodeTM(0) );
		}
		else
		{
			Matrix3 tmp = world;
			Uniform_Matrix( tmp, world );

			if( pParent && !pParent->IsRootNode() )
			{
				Matrix3 worldp;
				tmp = pParent->GetNodeTM(0);
				Uniform_Matrix( tmp, worldp );

				local = world * Inverse( worldp );
			}
		}

		AffineParts ap;
		decomp_affine( local, &ap );
		pNodeData->SetLocalPosition( Point3( ap.t.x, ap.t.z, ap.t.y ) );	
		pNodeData->SetLocalRotation( Quat( ap.q.x, ap.q.z, ap.q.y, ap.q.w ) );
		pNodeData->SetLocalScale( Point3( ap.k.x, ap.k.z, ap.k.y ) );

		ConvertMatrixToD3D( &world );
		pNodeData->SetWorldTransform( world );

		pNodeData->SetMaxNode( pNode );
	}
}

void RdMeshExp::CollectMeshNode( INode* pNode, RdMeshNode* pNodeData )
{
	// 노드가 메쉬 데이터를 가지고 있는지 확인
	ObjectState os = pNode->EvalWorldState( 0 );
	Object* pObj = os.obj;
	TriObject* pTriObj;
	BOOL bConvertedToTriObject = ( pObj->CanConvertToType( triObjectClassID ) ) && 
		( ( pTriObj = (TriObject*)pObj->ConvertToType( 0, triObjectClassID ) ) != NULL );

	if( !bConvertedToTriObject )
		return;

	// Material 정보 있는지 확인
	bool bMultiMtl = false;

	Mtl* pNodeMtl = pNode->GetMtl();
	if( NULL == pNodeMtl )
		return;
	else if( pNodeMtl->ClassID() == Class_ID( MULTI_CLASS_ID, 0 ) && pNodeMtl->IsMultiMtl() )
		bMultiMtl = true;

	Mesh* pMesh = &pTriObj->GetMesh();
	if( NULL == pMesh || pMesh->getNumVerts() == 0 || pMesh->getNumFaces() == 0 )
		return;

	pMesh->buildNormals();
	pMesh->checkNormals(TRUE);

	// 미러된 건지 확인
	Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
	bool bNeg = IsTMNegParity( world );

//	world = pNode->GetNodeTM( 0 );
//	world.NoScale();

	// Physic 정보 있는지 확인
	m_pcExport = NULL;
	m_phyExport = NULL;
	m_phyMod = NULL;

	// Skin 정보 있는지 확인
	m_skinMod = NULL;
	m_skinInterface = NULL;
	m_skinContext = NULL;

	m_phyMod = FindPhysiqueModifier( pNode );
	if( m_phyMod )
	{
		m_phyExport = (IPhysiqueExport*)m_phyMod->GetInterface( I_PHYINTERFACE );

		if( m_phyExport )
		{
			m_pcExport = (IPhyContextExport*)m_phyExport->GetContextInterface( pNode );
			if( m_pcExport )
			{
				m_pcExport->ConvertToRigid( TRUE );
				m_pcExport->AllowBlending( TRUE );
			}
		}
	}
	else
	{
		m_skinMod = FindSkinModifier( pNode );
		if( m_skinMod )
		{
			m_skinInterface = ( ISkin* )m_skinMod->GetInterface( I_SKIN );
			if( m_skinInterface )
			{
				m_skinContext = m_skinInterface->GetContextInterface( pNode );
			}
		}
	}

	//! Normal Map
	bool bUseNormalMap = false;
	Texmap* pMap = pNodeMtl->GetSubTexmap( ID_BU );
	if (pMap && ( pMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ))
	{
		bUseNormalMap = true;
	}
	pNodeData->UseNormalMap( bUseNormalMap );

	//------------------------------------------------------------------
	// Node Data
	bool bForce = (m_pcExport || m_skinContext) ? true : false;
	CollectNodeData( pNode, pNodeData, bNeg, bForce );

	// Mesh Data
	CollectMeshData( pMesh, pNodeData, world, m_pcExport, m_skinContext, bNeg );
	CollectMaterialData( pNodeMtl, pNodeData, bMultiMtl );

	if( m_bEnableExport )
	{
		if( pNodeData->GetLinkedBoneCount() > ENABLE_BONECNT )
		{
			// 분리 작업 해야함...
			SeperateMeshNode( pNodeData );
			SAFEDEL( pNodeData );
		}
		else
		{
			pNodeData->NotifyComplete();
			m_NodeList.Append( 1, &pNodeData );
		}
//		m_bEnableExport = false;
	}

	NodeCleanUp();
}

//------------------------------------------------------------------
void RdMeshExp::CollectMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, 
								 IPhyContextExport* pExport, ISkinContextData* pSkinExp, bool bNeg )
{
	//! smooting Group에 맞는 노말 벡터를 뽑기 위해서, 버텍스의 개수를 늘려야 하는데, 스킨 메쉬의 경우에는
	//! 애니메이션의 부하를 줄이기 위해서 static mesh에만 적용한다.
	if( pExport )
	{
		CollectPhyMeshData( pMesh, pNodeData, matWorld, pExport, bNeg );
	}
	else if( pSkinExp )
	{
		CollectSkinMeshData( pMesh, pNodeData, matWorld, pSkinExp, bNeg );
	}
	else
	{
		CollectStaticMeshData( pMesh, pNodeData, matWorld, bNeg );
	}	
}

//------------------------------------------------------------------
void RdMeshExp::CollectStaticMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, bool bNeg )
{
	Tab<RdVertex> verts;
	Tab<RdFace> faces;
	Tab<RdVertex> orgverts;

	Matrix3 matNorm = matWorld;
	Matrix3 mat = matWorld;

	//! 스케일을 적용해서 뽑을 것인지를 외부에서 결정한다.
	if( !m_bApplyScale )
	{
		mat.NoScale();
	}

	matNorm.NoScale();
	matNorm.NoTrans();
	
	ulong ulFaceCnt = pMesh->getNumFaces();
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;;

	RdVertex tmpvert;
	RdFace tmpface;
	ulong nIndex = 0, nVertIndex = 0;
	Point3 normal, pos;
	Face face;
	DWORD dwSmGroup;

	RVertex* prVert;
	Matrix3 transpose;

	//! smGroup의 개수에 맞춰서 버텍스 개수를 만들어준다.
	orgverts.SetCount( 0 );
	GetNormalVertexCount( pMesh, orgverts );
	ulong ulNormalVerts = orgverts.Count();

	bWritten.SetSize( ulNormalVerts );
	bWritten.ClearAll();

	Point3 v0, v1, v2, norm;

	// 버텍스 정보 집어넣기
	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];
		dwSmGroup = face.getSmGroup();

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face.v[j];

			nVertIndex = GetNormalVertexIndex( nIndex, dwSmGroup, orgverts );
			tmpface.Index[j] = nVertIndex;

			if( !bWritten[nVertIndex] )
			{
				// Normal 벡터 구하기
				prVert = pMesh->getRVertPtr( nIndex );
				normal = GetRVertexNormal( prVert, dwSmGroup );
				normal.Normalize();

				orgverts[nVertIndex].Normal = normal;

				//! 포지션 벡터 구하기
				orgverts[nVertIndex].Pos = mat * pMesh->verts[nIndex];

				bWritten.Set( nVertIndex );
			}
		}

		faces[i] = tmpface;
		faces[i].MatID = face.getMatID();
	}

	//MNMesh* pMNMesh = NULL;

	//Object* pObj =  pNodeData->GetMaxNode()->GetObjectRef();
 //   if (pObj->ClassID() == Class_ID(POLYOBJ_CLASS_ID, 0 )) 
 //   {
 //       PolyObject* pPolyObject = (PolyObject*) pObj;
 //       assert(pPolyObject);
 //       pMNMesh = &pPolyObject->GetMesh();
 //   }

	//Modifier* pModifier = FindModifier( pNodeData->GetMaxNode()->GetObjectRef(), EDIT_NORMALS_CLASS_ID );
	//if (pModifier && pMNMesh )
	//{
	//	MNNormalSpec* pNormalSpec = pMNMesh->GetSpecifiedNormals();
	//}

	RdStaticMeshExpUtil* pMeshExpUtil = new RdStaticMeshExpUtil;
	{
		//! 버텍스 칼라 존재 여부
		bool bHasVertColor = ( pMesh->numCVerts > 0 ) ? true : false;
		pNodeData->SetEnableVertexColor( bHasVertColor );

		if( bHasVertColor )
		{
			pMeshExpUtil->CollectVertexColor( pMesh, orgverts );
		}

		// 위치 버텍스와 텍스쳐 버텍스 맞추기
		bool bHasMultiUV = ( TRUE == pMesh->mapSupport(3) ) ? true : false;
		pNodeData->SetMultiUV( bHasMultiUV );

		pMeshExpUtil->CollectUV( pMesh, orgverts, verts, faces, bHasMultiUV );
	}
	delete pMeshExpUtil;

	// tangent 구하기..
	ComputeVertexTangents( pNodeData, verts, faces );

	pNodeData->SetFaceList( faces, bNeg );
	pNodeData->SetVertexList( verts );
}

//------------------------------------------------------------------
//	Desc : Max에서 Physique을 적용하여 Skinning을 작업한 MeshObject의 데이터를 추출한다.
void RdMeshExp::CollectPhyMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld,
									IPhyContextExport* pExport, bool bNeg )
{
	Tab<RdVertex> verts;
	Tab<RdFace> faces;
	Tab<RdVertex> orgverts;

	Matrix3 matNorm = matWorld;
	Matrix3 mat = matWorld;

	//! 스케일을 적용해서 뽑을 것인지를 외부에서 결정한다.
	if( !m_bApplyScale )
	{
		mat.NoScale();
	}

	matNorm.NoScale();
	matNorm.NoTrans();
	
	ulong ulFaceCnt = pMesh->getNumFaces();
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;;

	RdVertex tmpvert;
	RdFace tmpface;
	int nIndex;
	Point3 normal, pos;
	Face face;
	DWORD dwSmGroup;

	RVertex* prVert;
	Matrix3 transpose;

	// Skinned Mesh 와 구별해서 뽑자~
	ulong ulOrgVtxCnt = pMesh->getNumVerts();
	orgverts.SetCount( ulOrgVtxCnt );

	bWritten.SetSize( ulOrgVtxCnt );
	bWritten.ClearAll();

	// 버텍스 정보 집어넣기
	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];
		dwSmGroup = face.getSmGroup();

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face.v[j];
			tmpface.Index[j] = nIndex;
			if( !bWritten[nIndex] )
			{
				prVert = pMesh->getRVertPtr( nIndex );
				normal = GetRVertexNormal( prVert, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				Transpose_Matrix( mat, transpose );
				tmpvert.Normal = normal * Inverse(transpose);
				tmpvert.Normal.Normalize();

				tmpvert.Pos = mat * pMesh->verts[nIndex];

				GetPhysiqueWeights( pExport, nIndex, &tmpvert );

				bWritten.Set( nIndex );
				orgverts[nIndex] = tmpvert;
			}
		}
		faces[i] = tmpface;
		faces[i].MatID = face.getMatID();
	}

	RdSkinMeshExpUtil* pMeshExpUtil = new RdSkinMeshExpUtil;
	{
		//! 버텍스 칼라 존재 여부
		bool bHasVertColor = ( pMesh->numCVerts > 0 ) ? true : false;
		pNodeData->SetEnableVertexColor( bHasVertColor );
		if( bHasVertColor )
		{
			pMeshExpUtil->CollectVertexColor( pMesh, orgverts );
		}

		// 위치 버텍스와 텍스쳐 버텍스 맞추기
		bool bHasMultiUV = ( TRUE == pMesh->mapSupport(3) ) ? true : false;
		pNodeData->SetMultiUV( bHasMultiUV );

		pMeshExpUtil->CollectUV( pMesh, orgverts, verts, faces, bHasMultiUV );
	}
	delete pMeshExpUtil;

	pNodeData->SetFaceList( faces, bNeg );
	pNodeData->SetVertexList( verts );
}

//------------------------------------------------------------------
//	Desc : Max에서 Skin을 적용하여 Skinning을 작업한 MeshObject의 데이터를 추출한다.
//------------------------------------------------------------------
void RdMeshExp::CollectSkinMeshData( Mesh* pMesh, RdMeshNode* pNodeData, const Matrix3& matWorld, ISkinContextData* pExport, bool bNeg )
{
	Tab<RdVertex> verts;
	Tab<RdFace> faces;
	Tab<RdVertex> orgverts;

	Matrix3 matNorm = matWorld;
	Matrix3 mat = matWorld;

	//! 스케일을 적용해서 뽑을 것인지를 외부에서 결정한다.
	if( !m_bApplyScale )
	{
		mat.NoScale();
	}
 
	matNorm.NoScale();
	matNorm.NoTrans();
	
	ulong ulFaceCnt = pMesh->getNumFaces();
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;;

	RdVertex tmpvert;
	RdFace tmpface;
	int nIndex;
	Point3 normal, pos;
	Face face;
	DWORD dwSmGroup;

	RVertex* prVert;
	Matrix3 transpose;

	// Skinned Mesh 와 구별해서 뽑자~
	ulong ulOrgVtxCnt = pMesh->getNumVerts();
	orgverts.SetCount( ulOrgVtxCnt );

	bWritten.SetSize( ulOrgVtxCnt );
	bWritten.ClearAll();

	// 버텍스 정보 집어넣기
	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];
		dwSmGroup = face.getSmGroup();

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face.v[j];
			tmpface.Index[j] = nIndex;
			if( !bWritten[nIndex] )
			{
				prVert = pMesh->getRVertPtr( nIndex );
				normal = GetRVertexNormal( prVert, dwSmGroup );

				//! 노말 벡터는 로컬 좌표계를 월드 좌표계로 변환해주는 역전치 행렬을 이용한다.
				//! 물체의 이동과 상관없이, 물체의 회전과 크기 변화에 대한 변환을 적용하기 위해...
				Transpose_Matrix( mat, transpose );
				tmpvert.Normal = normal * Inverse(transpose);
				tmpvert.Normal.Normalize();

				tmpvert.Pos = mat * pMesh->verts[nIndex];

				GetSkinWeights( m_skinInterface, pExport, m_skinMod, nIndex, &tmpvert );

				bWritten.Set( nIndex );
				orgverts[nIndex] = tmpvert;
			}
		}
		faces[i] = tmpface;
		faces[i].MatID = face.getMatID();
	}

	RdSkinMeshExpUtil* pMeshExpUtil = new RdSkinMeshExpUtil;
	{
		//! 버텍스 칼라 존재 여부
		bool bHasVertColor = ( pMesh->numCVerts > 0 ) ? true : false;
		pNodeData->SetEnableVertexColor( bHasVertColor );

		if( bHasVertColor )
		{
			pMeshExpUtil->CollectVertexColor( pMesh, orgverts );
		}

		// 위치 버텍스와 텍스쳐 버텍스 맞추기
		bool bHasMultiUV = ( TRUE == pMesh->mapSupport(3) ) ? true : false;
		pNodeData->SetMultiUV( bHasMultiUV );

		pMeshExpUtil->CollectUV( pMesh, orgverts, verts, faces, bHasMultiUV );
	}
	delete pMeshExpUtil;

	pNodeData->SetFaceList( faces, bNeg );
	pNodeData->SetVertexList( verts );
}

//------------------------------------------------------------------
void RdMeshExp::CollectMaterialData( Mtl* pNodeMtl, RdMeshNode* pNodeData, bool bMultiMtl )
{
	ushort usFaceCnt = pNodeData->GetFaceCount();

	if( bMultiMtl )
	{
		Mtl* pMtl;
		ushort usMtlID;

		Tab<ushort>	FaceMtlIDs;
		FaceMtlIDs.ZeroCount();
		FaceMtlIDs.SetCount( usFaceCnt );

		int nSubMtl = pNodeMtl->NumSubMtls();
		for( int i = 0; i < nSubMtl; ++i )
		{
			pMtl = pNodeMtl->GetSubMtl( i );

			if( NULL == pMtl || ( pMtl->ClassID() != Class_ID( DMTL_CLASS_ID, 0 ) ) )
				continue;

			usMtlID = AddMaterial( pNodeMtl, pMtl );
			if (usMtlID != 0xffff)
			{
				ushort mtlID;
				for( ushort j = 0; j < usFaceCnt; ++j )
				{
					// face의 서브 매터리얼 번호
					mtlID = pNodeData->GetFaceMtlIndex( j );

					if (HasSameMaterial( mtlID, nSubMtl, i ))
					{
						//pNodeData->SetFaceMtlIndex( j, usMtlID );
						FaceMtlIDs[j] = usMtlID;
					}
				}	// for
			}
		}

		for ( ushort j=0; j<usFaceCnt; ++j)
		{
			pNodeData->SetFaceMtlIndex( j, FaceMtlIDs[j] );
		}
	}
	else
	{
		ushort usMtlID = AddMaterial( NULL, pNodeMtl );
		if( usMtlID != 0xffff )
		{
			for( ushort j = 0; j < usFaceCnt; ++j )
			{
				pNodeData->SetFaceMtlIndex( j, usMtlID );
			}
		}
	}
}

bool RdMeshExp::HasSameMaterial( ushort usMtlID, int nNumMtls, int nCurrentMtl )
{
	MtlID id = usMtlID % nNumMtls;
	return ( (nCurrentMtl == -1 && nNumMtls == 1) || id == nCurrentMtl );
}

ushort RdMeshExp::AddMaterial( Mtl* pParentMtl, Mtl* pNodeMtl )
{
	static TCHAR MaterialName[512];

	if (pParentMtl)
	{
		_stprintf( MaterialName, _T( "%s_%s" ), pParentMtl->GetName(), pNodeMtl->GetName() );
	}
	else
	{
		_stprintf( MaterialName, _T( "%s" ), pNodeMtl->GetName() );
	}

	ushort usCount = m_MtlList.Count();
	ushort usIndex = 0;
	for( ushort i = 0; i < usCount; ++i )
	{
		if( _tcscmp( m_MtlList[i]->GetMaterialName(), MaterialName ) == 0 )
			return usIndex;

		++usIndex;
	}

	//if( !pNodeMtl->GetSubTexmap(ID_DI) )
	//	return 0xffff;

	RdMaterial* pMtl = new RdMaterial( MaterialName );
	pMtl->SetMaterialID( usCount );

	TSTR classname;
	pNodeMtl->GetClassName(classname);

	//! TwoSide 정보..
	StdMat2* pstdMtl = (StdMat2*)pNodeMtl;
	pMtl->SetTwoSide( pstdMtl->GetTwoSided() ? 1 : 0 );

	//! Map(Texture 정보 얻어오기)
	{
		TSTR strFullName, strPath, strFile;

		Texmap* pMap;
		
		/// Self-illum
		if (pstdMtl->MapEnabled(ID_SI))
		{
			//! Diffuse Map
			pMap = pNodeMtl->GetSubTexmap( ID_SI );
			if (pMap)
			{
				float selfIlumAmt = pstdMtl->GetTexmapAmt(ID_SI, 0);

				if (selfIlumAmt > 0.8f)
				{	// blank...
				}
				else
				{
				}

				TSTR classname;
				pNodeMtl->GetClassName(classname);

				Class_ID classID = pMap->ClassID();
				if (classID == Class_ID( BMTEX_CLASS_ID, 0 ))
				{
					strFullName = ((BitmapTex*)pMap)->GetMapName();
					SplitPathFile( strFullName, &strPath, &strFile );		

					pMtl->SetEmissiveMap( strFile );
				}
				else if (classID == Class_ID( VCOL_CLASS_ID, 0) )
				{	// blank...
				}
			}
		}

		if (IsMultiTextured(pstdMtl))
		{
			DumpMultiTexture( pstdMtl, pMtl );
		}
		else if (pstdMtl->MapEnabled(ID_DI))	// Diffuse
		{
			//! Diffuse Map
			pMap = pNodeMtl->GetSubTexmap( ID_DI );
			if( pMap )
			{
				TSTR classname;
				pNodeMtl->GetClassName(classname);

				Class_ID classID = pMap->ClassID();
				if (classID == Class_ID( BMTEX_CLASS_ID, 0 ))
				{
					strFullName = ((BitmapTex*)pMap)->GetMapName();
					SplitPathFile( strFullName, &strPath, &strFile );		

					if( IsShellMaterial( classname ) )
	//				if( IsMultiTextured( pstdMtl ) )
					{
						//! 라이트 맵! Render To Texture 정보를 구한다.
						pMtl->SetLightMap( strFile );
					}
					else
					{
						pMtl->SetDiffuseMap( strFile );
					}
				}
			}
		}
		else if (pstdMtl->MapEnabled(ID_RL))	// Reflection
		{
		}

		//! Opacity Map
		if (pstdMtl->MapEnabled(ID_OP))
		{
			pMap = pNodeMtl->GetSubTexmap( ID_OP );
			if( pMap )
			{
				if( pMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) )
				{
					pMtl->SetOpacity( 1 );
				}
			}
		}

		//! Specular Map
		pMap = pNodeMtl->GetSubTexmap( ID_SP );
		if( pMap )
		{
			if( pMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
			{
				strFullName = ((BitmapTex*)pMap)->GetMapName();
				SplitPathFile( strFullName, &strPath, &strFile );		
				pMtl->SetSpecularMap( strFile );
			}
		}

		//! Normal Map
		pMap = pNodeMtl->GetSubTexmap( ID_BU );
		if (pMap)
		{
			Class_ID classID = pMap->ClassID();

			if( classID == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
			{
				strFullName = ((BitmapTex*)pMap)->GetMapName();
				SplitPathFile( strFullName, &strPath, &strFile );		
				pMtl->SetNormalMap( strFile );
			}
			else
			{
				CStr className;
				pMap->GetClassName(className);
				if (className == CStr(_T("Normal Bump")))
				{
					static const int NB_NORMAL = 0;
					static const int NB_ADDITIONAL_BUMP = 1;

					//! Normal Map
					Texmap* subTexMap = 0;

					subTexMap = pMap->GetSubTexmap(NB_NORMAL);
					if (subTexMap)
					{
						if ( subTexMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
						{
							strFullName = ((BitmapTex*)subTexMap)->GetMapName();
							SplitPathFile( strFullName, &strPath, &strFile );		
							pMtl->SetNormalMap( strFile );
						}
					}
					subTexMap = pMap->GetSubTexmap(NB_ADDITIONAL_BUMP);
					if (subTexMap)
					{
						if ( subTexMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
						{
							strFullName = ((BitmapTex*)subTexMap)->GetMapName();
							SplitPathFile( strFullName, &strPath, &strFile );		
							pMtl->SetNormalMap( strFile );
						}
					}
				}
			}
		}

		//for (int i=0; i< pMap->NumSubTexmaps(); ++i)
		//{
		//	//! Diffuse Map
		//	Texmap* subTexMap = pMap->GetSubTexmap(i);
		//	if (subTexMap)
		//	{
		//		TSTR classname;
		//		if ( subTexMap->ClassID() == Class_ID( BMTEX_CLASS_ID, 0 ) ) 
		//		{
		//			strFullName = ((BitmapTex*)subTexMap)->GetMapName();
		//			SplitPathFile( strFullName, &strPath, &strFile );		
		//		}
		//	}
		//}
	}

	m_MtlList.Append( 1, &pMtl );

	return usCount;
}

void RdMeshExp::DumpMultiTexture( StdMat2* pCurMtl, RdMaterial* pCurMaterial )
{
	assert( pCurMtl->MapEnabled(ID_DI) );

	Texmap* texMap = (BitmapTex*)pCurMtl->GetSubTexmap(ID_DI);
	assert(texMap);

	// self-illum slot -> dark map
	if (texMap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0))
	{
        Texmap *pSITm;

        assert(pCurMtl->MapEnabled(ID_SI));

        pSITm = (BitmapTex*) pCurMtl->GetSubTexmap(ID_SI);
        assert(pSITm);

        assert(pSITm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));

		// uv channel도 추가해주어야 한다.
	}

	Texmap* pSubTm = 0;
	bool bRequiresAlpha;

    // mix shader in diffuse slot --> decal
    if (texMap->ClassID() == Class_ID(MIX_CLASS_ID, 0))
	{
        bool bAlphaSet;
        int iAlphaFrom = -1;

        // we allow 3 in this assertion, but it's not actually handled
        // by the converter.
        assert(texMap->NumSubTexmaps() == 2 || texMap->NumSubTexmaps() == 3);

        bAlphaSet = false;
		bRequiresAlpha = false;

		for (int i=0; i<2; ++i)
		{
            pSubTm = texMap->GetSubTexmap(i);
            assert(pSubTm);
            assert(pSubTm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));

		}
	}
    // composite shader in diffuse slot --> glow
    else if (texMap->ClassID() == Class_ID(COMPOSITE_CLASS_ID, 0))
    {
		bool bAlphaSet;
        int iAlphaFrom = -1;

        assert(texMap->NumSubTexmaps() == 2);

        bAlphaSet = false;
        bRequiresAlpha = false;
		
		for (int i=0; i<2; ++i)
		{
            pSubTm = texMap->GetSubTexmap(i);
            assert(pSubTm);
            assert(pSubTm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));

		}
	}
    // RGBmultiply shader in diffuse slot --> dark map
    else if (texMap->ClassID() == Class_ID(RGBMULT_CLASS_ID, 0))
    {
		int iAlphaFrom = -1;
        //if (texMap->alphaFrom == 0 || texMap->alphaFrom == 1)
        //    iAlphaFrom = pMult->alphaFrom;

        assert(texMap->NumSubTexmaps() == 2);

        bRequiresAlpha = false;
        for (int i = 0; i < 2; i++)
        {
            pSubTm = texMap->GetSubTexmap(i);
            assert(pSubTm);
            assert(pSubTm->ClassID() == Class_ID(BMTEX_CLASS_ID, 0));
		}
	}
}

void RdMeshExp::NodeCleanUp()
{
	if( m_phyMod && m_phyExport )
	{
		if( m_pcExport )
		{
			m_phyExport->ReleaseContextInterface( m_pcExport );
			m_pcExport = NULL;
		}
		m_phyMod->ReleaseInterface( I_PHYINTERFACE, m_phyExport );
		m_phyExport = NULL;
		m_phyMod = NULL;
	}

	if( m_skinMod && m_skinContext )
	{
		m_skinMod->ReleaseInterface( I_SKIN, m_skinInterface );
		m_skinContext = NULL;
		m_skinInterface = NULL;
		m_skinMod = NULL;
	}
}

//------------------------------------------------------------------
bool RdMeshExp::CollectFaceNormNode( INode* pNode, RdNormFaceNode* pNodeData )
{
	// 노드가 메쉬 데이터를 가지고 있는지 확인
	ObjectState os = pNode->EvalWorldState( 0 );
	Object* pObj = os.obj;
	TriObject* pTriObj;
	BOOL bConvertedToTriObject = ( pObj->CanConvertToType( triObjectClassID ) ) && 
		( ( pTriObj = (TriObject*)pObj->ConvertToType( 0, triObjectClassID ) ) != NULL );

	if( !bConvertedToTriObject )
		return false;

	Mesh* pMesh = &pTriObj->GetMesh();
	if( NULL == pMesh || pMesh->getNumVerts() == 0 || pMesh->getNumFaces() == 0 )
		return false;

	// 미러된 건지 확인
	Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
	bool bNeg = world.Parity() ? true : false;
	
//	world.NoScale();

	Tab<RdNormFace> faces;
	Tab<Point3> orgverts;

	ulong ulFaceCnt = pMesh->getNumFaces();
	ulong ulOrgVtxCnt = pMesh->getNumVerts();
	
	orgverts.SetCount( ulOrgVtxCnt );
	faces.SetCount( ulFaceCnt );

	BitArray bWritten;
	bWritten.SetSize( ulOrgVtxCnt );
	bWritten.ClearAll();
	
	RdNormFace tmpface;
	int nIndex;
	Point3 pos;
	Face face;

	// 버텍스 정보 집어넣기
	for( ushort i = 0; i < ulFaceCnt; ++i )
	{
		face = pMesh->faces[i];

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face.v[j];
			tmpface.Index[j] = nIndex;

			if( !bWritten[nIndex] )
			{
				pos = world * pMesh->verts[nIndex];
				bWritten.Set( nIndex );
				orgverts[nIndex] = pos;
			}
		}
		faces[i] = tmpface;
	}

	// Vertex부터 세팅되어야 노멀을 구할 수 있당.
	pNodeData->SetVertexList( orgverts );
	pNodeData->SetFaceList( faces, bNeg );

	return true;
}

//------------------------------------------------------------------
void RdMeshExp::CollectBBoxData( INode* pNode )
{
	if( IsBoundingBoxNode( pNode->GetName() ) )
	{
		INode* pParent = pNode->GetParentNode();
		if( pParent && !pParent->IsRootNode() )
		{
			TCHAR* parentnodename = pParent->GetName();
			RdMeshNode* pMeshNode = GetNode( parentnodename );
			if (pMeshNode)
			{
				RdBBox* pBoxNode = new RdBBox();

				ushort idx = GetIndexOfNode( pParent );

				pBoxNode->SetParentNode( idx, parentnodename );

		//		RdMeshNode* pNode = GetNode( idx );
				
				pBoxNode->SetParentNodeXform( pMeshNode->GetWorldTransform() );

				ObjectState os = pNode->EvalWorldState( 0 );

				Object* pObj = os.obj;
				TriObject* pTriObj;
				BOOL bConvertedToTriObject = ( pObj->CanConvertToType( triObjectClassID ) ) && 
					( ( pTriObj = (TriObject*)pObj->ConvertToType( 0, triObjectClassID ) ) != NULL );

				if( !bConvertedToTriObject )
				{
					delete (pBoxNode);
					return;
				}

				Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
				Mesh* pMesh = &pTriObj->GetMesh();
				if( NULL == pMesh || pMesh->getNumVerts() == 0 || pMesh->getNumFaces() == 0 )
				{
					delete (pBoxNode);
					return;
				}

				Point3 pos;
				int nCount = pMesh->getNumVerts();
				for( int i = 0; i < nCount; ++i )
				{
					pos = world * pMesh->verts[i];
					pBoxNode->AddVertex( Point3( pos.x, pos.z, pos.y ) );
				}

				pBoxNode->CalculateOBB();

				m_BBoxList.Append( 1, &pBoxNode );
			} // if (pMeshNode)
		} // if (pParent...)
	} // if (IsBoundingBox...)

	int nChildNum = pNode->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectBBoxData( pNode->GetChildNode(i) );
	}
}

//------------------------------------------------------------------
RdMeshNode* RdMeshExp::GetNode( ushort usIndex )
{
	int nCount = m_NodeList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		if( m_NodeList[i]->GetNodeID() == usIndex )
			return m_NodeList[i];
	}

	return NULL;
}

//------------------------------------------------------------------
RdMeshNode* RdMeshExp::GetNode( const TCHAR* strName )
{
	int nCount = m_NodeList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		if( _tcscmp( m_NodeList[i]->GetNodeName(), strName ) == 0  )
			return m_NodeList[i];
	}

	return NULL;
}

//------------------------------------------------------------------
void RdMeshExp::UpdateMaterial()
{
	int num = SendDlgItemMessage( m_hMeshExpDlg, IDC_ALPHA_LIST, LB_GETCOUNT, 0, 0 );

	if( num != 0 )
	{
		ushort usCount = m_MtlList.Count();
		TCHAR str[128];

		for( ushort i = 0; i < num; ++i )
		{	
			SendDlgItemMessage( m_hMeshExpDlg, IDC_ALPHA_LIST, LB_GETTEXT, i, (LPARAM)str );
			for( ushort j = 0; j < usCount; ++j )
			{
				if( _tcscmp( m_MtlList[j]->GetMaterialName(), str ) == 0 )
				{
					m_MtlList[j]->SetAlphablending( 1 );
				}
			}
		}
	}
}

//------------------------------------------------------------------
bool EnableIncludeList( RdVertex& vert, Tab<ushort>& boneList )
{
	int nCnt = boneList.Count();
	BYTE nLink = vert.Link;
	if( nLink > 4 )
		nLink = 4;

	if( nCnt > 22 )
	{
		for( BYTE a = 0; a < nLink; ++a )
		{
			int i = 0;
			for( i = 0; i < nCnt; ++i )
			{
				if( boneList[i] == vert.Blend[a].BoneID )
					break;
			}

			if( i == nCnt )
				return false;
		}
	}
	else
	{
		int addcnt = nCnt;

		for( BYTE a = 0; a < nLink; ++a )
		{
			int i = 0;
			for( i = 0; i < nCnt; ++i )
			{
				if( boneList[i] == vert.Blend[a].BoneID )
					break;
			}

			if( i == nCnt )
				++addcnt;
		}

		if( addcnt > 22 )
			return false;
	}

	return true;
}

void CopyMeshNode( RdMeshNode* pScr, RdMeshNode* pDest )
{
	pDest->SetParentNode( pScr->GetParentNodeID(), pScr->GetParentNodeName() );
	pDest->SetWorldTransform( pScr->GetWorldTransform() );
	pDest->SetLocalPosition( pScr->GetLocalPosition() );
	pDest->SetLocalRotation( pScr->GetLocalRotation() );
	pDest->SetLocalScale( pScr->GetLocalScale() );
}

void CopyVertex( RdVertex* pVert, RdVertex& vert )
{
	vert.Pos = pVert->Pos;
	vert.Normal = pVert->Normal;
	vert.Tangent = pVert->Tangent;
	vert.Binormal = pVert->Binormal;
	vert.UV.U = pVert->UV.U;
	vert.UV.V = pVert->UV.V;
	vert.UV2.U = pVert->UV2.U;
	vert.UV2.V = pVert->UV2.V;
	vert.Link = pVert->Link;

	BYTE n = vert.Link;
	if( n > 4 )
		n = 4;

	for( BYTE i = 0; i < n; ++i )
	{
		vert.Blend[i].BoneID = pVert->Blend[i].BoneID;
		vert.Blend[i].Weight = pVert->Blend[i].Weight;
	}
}

void AddBoneID( RdVertex* pVert, Tab<ushort>& bonelist )
{
	BYTE nLink = pVert->Link;
	if( nLink > 4 )
		nLink = 4;

	for( BYTE a = 0; a < nLink; ++a )
	{
		int nCount = bonelist.Count();

		int b = 0;
		for( b = 0; b < nCount; ++b )
		{
			if( bonelist[b] == pVert->Blend[a].BoneID )
				break;
		}

		if( b == nCount )
		{
			bonelist.Append( 1, &(pVert->Blend[a].BoneID) );
		}
	}
}

bool ComparePhysic( RdVertex* pV1, RdVertex* pV2 )
{
	if( pV1->Link != pV2->Link )
		return false;

	BYTE nLink = pV1->Link;
	if( nLink > 4 )
		nLink = 4;

	for( BYTE i = 0; i < nLink; ++i )
	{
		if( pV1->Blend[i].BoneID != pV2->Blend[i].BoneID )
			return false;

		if( fabsf(  pV1->Blend[i].Weight - pV2->Blend[i].Weight ) > 0.000001f )
			return false;
	}

	return true;
}

//------------------------------------------------------------------
void RdMeshExp::SeperateMeshNode( RdMeshNode* pOrigin )
{
	Tab<RdFace*> oflist = pOrigin->GetFaceList();
	Tab<RdVertex*> ovlist = pOrigin->GetVertexList();

	// 원본 페이스 값을 복사한다.
	Tab<RdFace> srcflist;
	srcflist.ZeroCount();
	RdFace tmpFace;

	int nFaceCnt = pOrigin->GetFaceCount();
	for( int i = 0; i < nFaceCnt; ++i )
	{
		tmpFace.Index[0] = oflist[i]->Index[0];
		tmpFace.Index[1] = oflist[i]->Index[1];
		tmpFace.Index[2] = oflist[i]->Index[2];

		tmpFace.MatID = oflist[i]->MatID;

		srcflist.Append( 1, &tmpFace );
	}

	// 분리 시작
	Tab<RdVertex> tmpverts;
	Tab<ushort> bonelist;
	Tab<RdFace> tmpflist;
	
	bool bCreate = true;
	RdMeshNode* pNewNode = NULL;
	ushort usID = 2000;
	TCHAR strName[MAX_LEN];

	int idx = 0;
	int nRemainCount = srcflist.Count();

	RdVertex vert, tmpvert;

	do
	{
		if( bCreate )
		{
			++usID;
			memset( strName, 0, sizeof(TCHAR)*MAX_LEN );			
			_stprintf( strName, "%s_%d", pOrigin->GetNodeName(), usID );
			pNewNode = new RdMeshNode( 0, usID, strName );
			CopyMeshNode( pOrigin, pNewNode );

			tmpverts.ZeroCount();
			bonelist.ZeroCount();
			tmpflist.ZeroCount();
			bCreate = false;

			idx = 0;
		}

		// 한 페이스씩 돈다.
		tmpFace.MatID = srcflist[idx].MatID;

		tmpFace.Index[0] = srcflist[idx].Index[0];
		tmpFace.Index[1] = srcflist[idx].Index[1];
		tmpFace.Index[2] = srcflist[idx].Index[2];

		int a = 0;
		for( a = 0; a < 3; ++a )
		{
			// 페이스를 구성하는 버텍스 3개를 다 검사하여 기존 부분에 넣을 수 있는지 판단한다.
			CopyVertex( ovlist[tmpFace.Index[a]], vert );
			if( !EnableIncludeList( vert, bonelist ) )
				break;
		}

		if( a == 3 )
		{
			// 새로 넣어...
			for( int k = 0; k < 3; ++k )
			{
				CopyVertex( ovlist[tmpFace.Index[k]], vert );
				ulong ulVertCnt = tmpverts.Count();
				
				ulong b = 0;
				for( b = 0; b < ulVertCnt; ++b )
				{
					tmpvert = tmpverts[b];

					if( Point3Equal( tmpvert.Pos, vert.Pos ) && 
						FloatEqual( tmpvert.UV.U, vert.UV.U ) &&
						FloatEqual( tmpvert.UV.V, vert.UV.V ) &&
						ComparePhysic( &tmpvert, &vert ) ) 
						break;			
				}

				if( b < ulVertCnt )
				{
					tmpFace.Index[k] = b;
				}
				else
				{
					tmpFace.Index[k] = ulVertCnt;
					tmpverts.Append( 1, &vert );
					AddBoneID( &vert, bonelist );
				}
			}
			tmpflist.Append( 1, &tmpFace );
			nRemainCount = srcflist.Delete( idx, 1 );	
		}
		else 
		{
			// 못 넣는 넘....
			++idx;
		}

		if( idx == srcflist.Count() )
		{
			pNewNode->SetVertexList( tmpverts, false );
			pNewNode->SetFaceList( tmpflist, true );
			pNewNode->NotifyComplete();
			m_NodeList.Append( 1, &pNewNode );
			bCreate = true;
		}

	}while( 0 != nRemainCount );
}

//------------------------------------------------------------------
void RdMeshExp::ComputeVertexNormals( Mesh* pMesh, bool bNeg, Tab<VNormal>& rVNorms )
{
	Face* face = NULL;
	Point3* vertices = NULL;
	Point3 v0, v1, v2;
	Tab<Point3> fnorms;

	ulong ulVertCount = pMesh->getNumVerts();
	ulong ulFaceCount = pMesh->getNumFaces();

	face = pMesh->faces;
	vertices = pMesh->verts;
	rVNorms.SetCount( ulVertCount );
	fnorms.SetCount( ulFaceCount );

	//! 1. Initialize
	ulong i = 0;
	for( i = 0; i < ulVertCount; ++i )
		rVNorms[i] = VNormal();

	//! 2. 버텍스의 노말값 추가.
	for( i = 0; i < ulFaceCount; ++i, face++ )
	{
		if( bNeg )
		{
			v0 = vertices[face->v[0]];
			v1 = vertices[face->v[2]];
			v2 = vertices[face->v[1]];
		}
		else
		{
			v0 = vertices[face->v[0]];
			v1 = vertices[face->v[1]];
			v2 = vertices[face->v[2]];
		}

		fnorms[i] = (v1-v0) ^ (v2-v1);

		for( int j = 0; j < 3; ++j )
		{
			rVNorms[face->v[j]].AddNormal( fnorms[i], face->smGroup );
		}

		fnorms[i].Normalize();
	}

	//! 3. 버텍스 노말 계산
	for( i = 0; i < ulVertCount; ++i )
		rVNorms[i].Normalize();
}

//------------------------------------------------------------------
void RdMeshExp::ComputeVertexTangents( RdMeshNode* pNodeData, Tab<RdVertex> &rVertices, Tab<RdFace> &rFaces )
{
	// B' = (u1-u0)(x2-x0) - (u2-u0)(x1-x0)
	// T = B' * N
	// B = T * N
	Matrix3 inv = Inverse( pNodeData->GetWorldTransform() );

	int numTriangles = rFaces.Count();
	for (int triIndex = 0; triIndex < numTriangles; ++triIndex)
	{
		RdFace* face = &rFaces[triIndex];

		RdVertex* vertex0 = &rVertices[ face->Index[0] ];
		RdVertex* vertex1 = &rVertices[ face->Index[1] ];
		RdVertex* vertex2 = &rVertices[ face->Index[2] ];

		//Point3 localV0 = inv * (vertex0->Pos);
		//Point3 localV1 = inv * (vertex1->Pos);
		//Point3 localV2 = inv * (vertex2->Pos);
		Point3 localV0 = (vertex0->Pos);
		Point3 localV1 = (vertex1->Pos);
		Point3 localV2 = (vertex2->Pos);

		Point3 v0 = localV1 - localV0;
		Point3 v1 = localV2 - localV0;
		Point3 normal = v0 ^ v1;
		normal.Normalize();
		face->Normal = normal;

		RdUV uv0 = vertex1->UV - vertex0->UV;
		RdUV uv1 = vertex2->UV - vertex0->UV;

		Point3 binormal = (v0 * uv1.U) - (v1 * uv0.U);
		//Point3 binormal = -(v0 * uv1.U) + (v1 * uv0.U);
		binormal.Normalize();

		//Point3 tangent = binormal ^ normal;
		Point3 tangent = (v0 * uv1.V) - (v1 * uv0.V);
		tangent.Normalize();
		face->Tangent = tangent;
	}

	/* vertex tangent 구하기
		-공유하는 tangent의 평균을 구한다.
		 특정한 하나의 정점에서의 접선벡터를 얻으려면 그 정점을 공유하는 모든 삼각형들의 접선들의 평균을 구해야 한다.
		 만일 인접한 삼각영이 다른 텍스쳐 매핑을 사용한다면, 어차피 매핑 좌표들이 다르므로, 경계선에 있는 정점들은 이미
		 중복되어 있을 것이다. 그런 삼각형들을 접선 평균 계산에 포함시키면 각 삼각형에 대한 범프 맵의 방향이 제대로 표현
		 되지 않으므로, 제외시켜야 한다.
	*/
	struct TriVertGroup
	{
		int size;
		int* triIndices;

		TriVertGroup() : triIndices(0), size(0) {}
		~TriVertGroup() { delete [] triIndices; triIndices = NULL; size = 0; }
	};

	int numVerts = rVertices.Count();
	TriVertGroup* vertexTriangleArray = new TriVertGroup[ numVerts ];

	for (int triIndex=0; triIndex<numTriangles; ++triIndex)
	{
		RdFace* face = &rFaces[triIndex];

		for (int v=0; v<3; ++v)
		{
			TriVertGroup* triVertGroup = &vertexTriangleArray[ face->Index[v] ];

			if (0 == triVertGroup->triIndices)
				triVertGroup->triIndices = new int[numTriangles];

			triVertGroup->triIndices[ triVertGroup->size ] = triIndex;
			triVertGroup->size++;
		}
	}

	for (int vertIndex=0; vertIndex<numVerts; ++vertIndex)
	{
		Point3 avgTangent(0.0f, 0.0f, 0.0f);

		int numVertTris = vertexTriangleArray[vertIndex].size;
		for (int vertTriIndex=0; vertTriIndex<numVertTris; ++vertTriIndex)
		{
			RdFace* face = &rFaces[ vertexTriangleArray[vertIndex].triIndices[vertTriIndex] ];
			avgTangent += face->Tangent;
		}

		avgTangent.Normalize();

		rVertices[vertIndex].Tangent = avgTangent;
		rVertices[vertIndex].Binormal = rVertices[vertIndex].Normal ^ rVertices[vertIndex].Tangent;
		rVertices[vertIndex].Binormal.Normalize();
	}

	delete []vertexTriangleArray;
}

//------------------------------------------------------------------
//	Smoothing group에 맞게 버텍스를 추가하기 위한 구조체
//------------------------------------------------------------------
#define MAX_SMOOTING_GROUP	128

struct SmVert
{
	int			nIndex;
	ulong		smGroup[MAX_SMOOTING_GROUP];
	int			nCount;

	SmVert()
	{
		ClearSmGroup();
	}

	bool	FindSmGroup( ulong ulSmooth )
	{
		for( int i = 0; i < nCount; ++i )
		{
			if( smGroup[i] == ulSmooth )
				return true;
		}
		return false;
	}

	void	InsertSmGroup( ulong ulSmooth )
	{
		smGroup[ nCount ] = ulSmooth;
		nCount++;
	}

	void	ClearSmGroup()
	{
		for( int i=0; i < MAX_SMOOTING_GROUP; ++i )
		{
			smGroup[i] = 0xffffffff;
		}
		nCount = 0;
	}
};

//------------------------------------------------------------------
void RdMeshExp::GetNormalVertexCount( Mesh* pMesh, Tab<RdVertex>& rVertices )
{
	Face* face;

	RdVertex tmpVert;

	int nNormals = 0;
	int nVertexCount = 0;
	int nIndex = 0;

	Tab<SmVert>	smVerts;
	smVerts.SetCount(0);

	BitArray bWritten;
	bWritten.SetSize( pMesh->getNumVerts() );
	bWritten.ClearAll();

	ulong ulSmGroup = 0;
	ulong ulFaceCnt = pMesh->getNumFaces();
	for( ulong i = 0; i < ulFaceCnt; ++i )
	{
		face = &pMesh->faces[i];
		ulSmGroup = face->getSmGroup();

		for( int j = 0; j < 3; ++j )
		{
			nIndex = face->v[j];

			bool bInsert = true;

			// 1. 인덱스가 같은 것이 없다면, 버텍스 추가
			// 2. 인덱스가 같아도, smgroup이 다른 것이 있다면, 버텍스 추가
			if( !bWritten[nIndex] )
			{
				bWritten.Set(nIndex);

				SmVert smvert;
				smvert.nIndex = nIndex;
				smvert.InsertSmGroup( ulSmGroup );

				smVerts.Insert( smVerts.Count(), 1, &smvert );
			}
			else
			{
				ulong ulVerts = smVerts.Count();
				for( ulong v = 0; v < ulVerts; ++v )
				{
					if( nIndex == smVerts[v].nIndex )
					{
						if( smVerts[v].FindSmGroup( ulSmGroup ) )
						{
							bInsert = false;
							break;
						}
						else
						{
							smVerts[v].InsertSmGroup( ulSmGroup );
							break;
						}
					}
				}
			}

			if( bInsert )
			{
				tmpVert.SmGroup = ulSmGroup;
				tmpVert.OrgId = nIndex;

				rVertices.Insert( rVertices.Count(), 1, &tmpVert );
			}
		}
	}
}

//------------------------------------------------------------------
ulong RdMeshExp::GetNormalVertexIndex( int nIndex, ulong ulSmGroup, const Tab<RdVertex>& rVertices )
{
	int nCount = rVertices.Count();
	int nId = 0;
	ulong smgroup = 0;
	for( int i = 0; i < nCount; ++i )
	{
		nId = rVertices[i].OrgId;
		smgroup = rVertices[i].SmGroup;

		if( nIndex == nId && (ulSmGroup == smgroup) )
		{
			return i;
		}
	}

	ASSERT_MBOX( FALSE, _T( "[RdMeshExp::GetNormalVertexIndex] Invalid VertexIndex" ) );

	return 0xffffffff;
}

//------------------------------------------------------------------
//	RdMeshExpUtil
//------------------------------------------------------------------
void RdMeshExpUtil::CheckOverlappedVertex( Tab<RdVertex>& rVerts, Tab<RdFace>& rFaces,
										   Tab<RdVertex>& rSrcVerts )
{
	rVerts.SetCount( 0 );
		
	RdFace nwface, tmpface;
	RdVertex nwvert, tmpvert;

	ulong ulFaceCnt = rFaces.Count();
	for( int i = 0; i < ulFaceCnt; ++i )
	{
		nwface = rFaces[i];
		for( int j = 0; j < 3; ++j )
		{
			tmpface.Index[j] = nwface.Index[j];
		}
		tmpface.MatID = nwface.MatID;

		for( int k = 0; k < 3; ++k )
		{
			nwvert = rSrcVerts[nwface.Index[k]];

			ulong ulVertCnt = rVerts.Count();
			ulong a = 0;
			for( a = 0; a < ulVertCnt; ++a )
			{
				tmpvert = rVerts[a];

				if( Point3Equal( tmpvert.Pos, nwvert.Pos ) && 
					Point3Equal( tmpvert.Normal, nwvert.Normal ) &&
					FloatEqual( tmpvert.UV.U, nwvert.UV.U ) &&
					FloatEqual( tmpvert.UV.V, nwvert.UV.V ) && 
					FloatEqual( tmpvert.UV2.U, nwvert.UV2.U ) && 
					FloatEqual( tmpvert.UV2.V, nwvert.UV2.V ) ) 
					break;
			}

			if( a < ulVertCnt )
			{
				tmpface.Index[k] = a;
			}
			else
			{
				tmpface.Index[k] = ulVertCnt;
				rVerts.Insert( ulVertCnt, 1, &nwvert );
			}
		}

		rFaces[i] = tmpface;
	}
}

//------------------------------------------------------------------
void RdMeshExpUtil::CollectUV(	Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
								Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces,
								bool bHasMultiUV )
{
	if( bHasMultiUV )
	{
		CollectMultiUV( pMesh, rSrcVerts, rOutputVerts, rOutputFaces );
	}
	else
	{
		CollectSingleUV( pMesh, rSrcVerts, rOutputVerts, rOutputFaces );
	}
}

//------------------------------------------------------------------
//	RdStaticMeshExpUtil
//------------------------------------------------------------------
void RdStaticMeshExpUtil::CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
										   Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		ulong ulNormalVerts = rSrcVerts.Count();
		ulong ulFaceCnt = pMesh->getNumFaces();

		BitArray bWritten;
		bWritten.SetSize( ulNormalVerts );
		bWritten.ClearAll();

		Tab<RdVertex> tmpVerts;
		tmpVerts.SetCount( ulNormalVerts );

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			tmpface.MatID = face.getMatID();

			for( int j = 0; j < 3; ++j )
			{
				nVertIdx = face.v[j];
				nTVertIdx = tvface.t[j];

				nVertIndex = RdMeshExp::GetNormalVertexIndex( nVertIdx, face.getSmGroup(), rSrcVerts );

				// 버텍스 uv 정보 세팅
				tmpvert = rSrcVerts[nVertIndex];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				// face에 버텍스 index 세팅
				tmpface.Index[j] = nVertIndex;

				if( bWritten[nVertIndex] )
				{
					if( ( false == FloatEqual( tmpvert.UV.U, tmpVerts[nVertIndex].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, tmpVerts[nVertIndex].UV.V ) ) )
					{
						tmpface.Index[j] = tmpVerts.Count();
						tmpVerts.Insert( tmpVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					tmpVerts[nVertIndex] = tmpvert;
					bWritten.Set( nVertIndex );
				}
			}
			rOutputFaces[i] = tmpface;
		}

		// 중복되는 거 없애기
		CheckOverlappedVertex( rOutputVerts, rOutputFaces, tmpVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdStaticMeshExpUtil::CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
										  Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		ulong ulNormalVerts = rSrcVerts.Count();
		ulong ulFaceCnt = pMesh->getNumFaces();

		BitArray bWritten;
		bWritten.SetSize( ulNormalVerts );
		bWritten.ClearAll();

		Tab<RdVertex> tmpVerts;
		tmpVerts.SetCount( ulNormalVerts );

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			tmpface.MatID = face.getMatID();

			for( int j = 0; j < 3; ++j )
			{
				nVertIdx = face.v[j];
				nTVertIdx = tvface.t[j];

				nVertIndex = RdMeshExp::GetNormalVertexIndex( nVertIdx, face.getSmGroup(), rSrcVerts );

				// 버텍스 uv 정보 세팅
				tmpvert = rSrcVerts[nVertIndex];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				// render to texture는, 기본적으로 3번 채널에 그 UV값이 들어간다.
				if( pMesh->mapSupport(3) )
				{
					nTVertIdx = pMesh->mapFaces(3)[i].t[j];

					//! 2번째 uv 버텍스 정보 세팅
					tmpvert.UV2.U = pMesh->mapVerts(3)[nTVertIdx].x;
					tmpvert.UV2.V = 1.0f - pMesh->mapVerts(3)[nTVertIdx].y;
				}

				// face에 버텍스 index 세팅
				tmpface.Index[j] = nVertIndex;

				if( bWritten[nVertIndex] )
				{
					if( ( false == FloatEqual( tmpvert.UV.U, tmpVerts[nVertIndex].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, tmpVerts[nVertIndex].UV.V ) ) ||
						( false == FloatEqual( tmpvert.UV2.U, tmpVerts[nVertIndex].UV2.U ) ) ||
						( false == FloatEqual( tmpvert.UV2.V, tmpVerts[nVertIndex].UV2.V ) ) )
					{
						tmpface.Index[j] = tmpVerts.Count();
						tmpVerts.Insert( tmpVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					tmpVerts[nVertIndex] = tmpvert;
					bWritten.Set( nVertIndex );
				}
			}
			rOutputFaces[i] = tmpface;
		}

		// 중복되는 거 없애기
		CheckOverlappedVertex( rOutputVerts, rOutputFaces, tmpVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdStaticMeshExpUtil::CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices )
{
	Face face;
	ulong ulFaceCnt = pMesh->getNumFaces();
	DWORD dwSmGroup = 0;

	int nColorVertID = 0;
	int nVertIndex = 0;
	int nIndex = 0;

	if( NULL != pMesh->vertColArray )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			dwSmGroup = face.getSmGroup();

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFaceData[i].t[j];

				nVertIndex = RdMeshExp::GetNormalVertexIndex( nIndex, dwSmGroup, rVertices );

				rVertices[nVertIndex].Color = pMesh->vertColArray[nColorVertID];
			}
		}
	}
	else if( NULL != pMesh->vertCol )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			dwSmGroup = face.getSmGroup();

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFace[i].t[j];

				nVertIndex = RdMeshExp::GetNormalVertexIndex( nIndex, dwSmGroup, rVertices );

				rVertices[nVertIndex].Color = pMesh->vertCol[nColorVertID];
			}
		}
	}
}

//------------------------------------------------------------------
//	RdSkinMeshExpUtil
//------------------------------------------------------------------
void RdSkinMeshExpUtil::CollectSingleUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
										 Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		BitArray bWritten;
		bWritten.SetSize( pMesh->getNumVerts() );
		bWritten.ClearAll();

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		Tab<RdVertex> tmpVerts;
		tmpVerts.SetCount( pMesh->getNumVerts() );

		ulong ulFaceCnt = pMesh->getNumFaces();

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			for( int j = 0; j < 3; ++j )
			{
				tmpface.Index[j] = face.v[j];
			}
			tmpface.MatID = face.getMatID();

			for( int k = 0; k < 3; ++k )
			{
				nVertIdx = face.v[k];
				nTVertIdx = tvface.t[k];

				tmpvert = rSrcVerts[nVertIdx];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				if( bWritten[nVertIdx] )
				{
					if( ( false == FloatEqual( tmpvert.UV.U, tmpVerts[nVertIdx].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, tmpVerts[nVertIdx].UV.V ) ) )
					{
						tmpface.Index[k] = tmpVerts.Count();
						tmpVerts.Insert( tmpVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					tmpVerts[nVertIdx] = tmpvert;
					bWritten.Set( nVertIdx );
				}
			}
			rOutputFaces[i] = tmpface;

		}

		// 중복되는 거 없애기
		CheckOverlappedVertex( rOutputVerts, rOutputFaces, tmpVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdSkinMeshExpUtil::CollectMultiUV( Mesh* pMesh, Tab<RdVertex>& rSrcVerts,
										Tab<RdVertex>& rOutputVerts, Tab<RdFace>& rOutputFaces )
{
	if( pMesh->numTVerts > 0 )
	{
		Face face;
		TVFace tvface;
		int nVertIdx, nTVertIdx; 

		BitArray bWritten;
		bWritten.SetSize( pMesh->getNumVerts() );
		bWritten.ClearAll();

		RdVertex tmpvert;
		RdFace tmpface;
		ulong nIndex = 0, nVertIndex = 0;

		Tab<RdVertex> tmpVerts;
		tmpVerts.SetCount( pMesh->getNumVerts() );

		ulong ulFaceCnt = pMesh->getNumFaces();

		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];
			tvface = pMesh->tvFace[i];

			int j = 0;
			for( j = 0; j < 3; ++j )
			{
				tmpface.Index[j] = face.v[j];
			}
			tmpface.MatID = face.getMatID();

			for( int k = 0; k < 3; ++k )
			{
				nVertIdx = face.v[k];
				nTVertIdx = tvface.t[k];

				tmpvert = rSrcVerts[nVertIdx];
				tmpvert.UV.U = pMesh->tVerts[nTVertIdx].x;
				tmpvert.UV.V = 1.0f - pMesh->tVerts[nTVertIdx].y;

				// render to texture는, 기본적으로 3번 채널에 그 UV값이 들어간다.
				if( pMesh->mapSupport(3) )
				{
					nTVertIdx = pMesh->mapFaces(3)[i].t[j];

					//! 2번째 uv 버텍스 정보 세팅
					tmpvert.UV2.U = pMesh->mapVerts(3)[nTVertIdx].x;
					tmpvert.UV2.V = 1.0f - pMesh->mapVerts(3)[nTVertIdx].y;
				}

				if( bWritten[nVertIdx] )
				{
					if( ( false == FloatEqual( tmpvert.UV.U, tmpVerts[nVertIdx].UV.U ) ) || 
						( false == FloatEqual( tmpvert.UV.V, tmpVerts[nVertIdx].UV.V ) ) ||
						( false == FloatEqual( tmpvert.UV2.U, tmpVerts[nVertIndex].UV2.U ) ) ||
						( false == FloatEqual( tmpvert.UV2.V, tmpVerts[nVertIndex].UV2.V ) ) )
					{
						tmpface.Index[k] = tmpVerts.Count();
						tmpVerts.Insert( tmpVerts.Count(), 1, &tmpvert );
					}
				}
				else
				{
					tmpVerts[nVertIdx] = tmpvert;
					bWritten.Set( nVertIdx );
				}
			}
			rOutputFaces[i] = tmpface;

		}

		// 중복되는 거 없애기
		CheckOverlappedVertex( rOutputVerts, rOutputFaces, tmpVerts );
	}
	else
	{
		rOutputVerts = rSrcVerts;
	}
}

//------------------------------------------------------------------
void RdSkinMeshExpUtil::CollectVertexColor( Mesh* pMesh, Tab<RdVertex>& rVertices )
{
	Face face;
	ulong ulFaceCnt = pMesh->getNumFaces();

	int nColorVertID = 0;
	int nIndex = 0;

	if( NULL != pMesh->vertColArray )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFaceData[i].t[j];

				rVertices[nIndex].Color = pMesh->vertColArray[nColorVertID];
			}
		}
	}
	else if( NULL != pMesh->vertCol )
	{
		for( ushort i = 0; i < ulFaceCnt; ++i )
		{
			face = pMesh->faces[i];

			for( int j = 0; j < 3; ++j )
			{
				nIndex = face.v[j];
				nColorVertID = pMesh->vcFace[i].t[j];

				rVertices[nIndex].Color = pMesh->vertCol[nColorVertID];
			}
		}
	}
}
