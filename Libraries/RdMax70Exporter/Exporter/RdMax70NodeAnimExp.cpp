#include "RdMax70NodeAnimExp.h"
#include "..\RdMax70Util.h"
#include "..\RdMax70ChunkDef.h"

RdNodeAnimExp::RdNodeAnimExp( Interface* pInterface ) : m_pInterface( pInterface )//, m_hAnimExpDlg( hWnd )
{
	m_ulStartTime	= 0;
	m_ulEndTime		= 0;

	m_ulTFP			= 0;
	m_usTotalFrame	= 0;

	m_bUseKeys		= false;

	m_NodeList.Init();
	m_NodeList.Count();
}

RdNodeAnimExp::~RdNodeAnimExp()
{
	Clear();
}

void RdNodeAnimExp::Clear()
{
	int nCount = m_NodeList.Count();
	AnimNode** ppNode;
	for( int i = 0; i < nCount; ++i )
	{
		ppNode = m_NodeList.Addr( i );
		delete (*ppNode);
	}
	m_NodeList.Delete( 0, nCount );
}

void RdNodeAnimExp::Initialize()
{
	m_ulStartTime = m_pInterface->GetAnimRange().Start();
	m_ulEndTime = m_pInterface->GetAnimRange().End();
	m_ulTFP = GetTicksPerFrame();
	m_usTotalFrame = (ushort)(( m_ulEndTime - m_ulStartTime ) / m_ulTFP);
	m_ulFrameRate = GetFrameRate();

	int nChildNum = m_pInterface->GetRootNode()->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectNodeAnim( m_pInterface->GetRootNode()->GetChildNode( i ) );
	}
}

void RdNodeAnimExp::OutputInformation()
{

}

void RdNodeAnimExp::CollectNodeAnim( INode* pNode )
{
	ushort ID = GetIndexOfNode( pNode );
	if( ID != UNDESIRABLE_MARKER )
	{
		if( IsBoneNode( pNode ) || IsMeshNode( pNode ) )
		{
			AnimNode* pAniNode = new AnimNode();
			pAniNode->ID = ID;
			_tcscpy( pAniNode->Name, pNode->GetName() );
			if (CollectKeyData( pNode, pAniNode->PosTrack, pAniNode->RotTrack, pAniNode->SclTrack ))
			{
				m_NodeList.Append( 1, &pAniNode );
			}
			else
			{
				delete pAniNode;
			}
		}
	}

	int nChildNum = pNode->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectNodeAnim( pNode->GetChildNode( i ) );
	}	
}

bool RdNodeAnimExp::CollectKeyData( INode* pNode, Tab<VecKey*>& pos, Tab<QuatKey*>& rot, Tab<VecKey*>& scl )
{
	// Node's transform controller
	Control* control = pNode->GetTMController();

	Control* posControl = control->GetPositionController();
	Control* rotControl = control->GetRotationController();
	Control* sclControl = control->GetScaleController();

	bool result = false;
	//
	//if (posControl || rotControl || sclControl)
	//{
	//	GetKeyDataByControl( pNode, control, pos, rot, scl );
	//}
	//else
	{
		result = GetKeyDataByFrame( pNode, pos, rot, scl );
	}

	return result;
}

bool RdNodeAnimExp::CheckForAnimation( INode* pNode, bool& bEnablePos, bool& bEnableRot, bool& bEnableScl )
{
	Matrix3 tm;
	AffineParts ap;
	Point3 firstPos;
	float rotAngle, firstRotAngle;
	Point3 rotAxis, firstRotAxis;
	Point3 firstScaleFactor;

	bEnablePos = bEnableRot = bEnableScl = false;

	for( ulong t = m_ulStartTime; t <= m_ulEndTime; t += m_ulTFP )
	{
		tm = pNode->GetNodeTM(t) * Inverse(pNode->GetParentTM(t));

		decomp_affine(tm, &ap);

		AngAxisFromQ(ap.q, &rotAngle, rotAxis);

		
		if (t != m_ulStartTime)
		{
			if (!bEnablePos) 
			{
				if (!Point3Equal(ap.t, firstPos))
				{
					bEnablePos = true;
				}
			}

			// MAX 2.x:
			// We examine the rotation angle to see if the rotation component
			// has changed.
			// Although not entierly true, it should work.
			// It is rare that the rotation axis is animated without
			// the rotation angle being somewhat affected.
			// MAX 3.x:
			// The above did not work, I have a repro scene that doesn't export a rotation track
			// because of this. I fixed it to also compare the axis.
			if (!bEnableRot)
			{
				if (fabs(rotAngle - firstRotAngle) > 1e-6f)
				{
					bEnableRot = true;
				}
				else if (!Point3Equal(rotAxis, firstRotAxis))
				{
					bEnableRot = true;
				}
			}

			if (!bEnableScl)
			{
				if (!Point3Equal(ap.k, firstScaleFactor))
				{
					bEnableScl = true;
				}
			}
		}
		else
		{
			firstPos = ap.t;
			firstRotAngle = rotAngle;
			firstRotAxis = rotAxis;
			firstScaleFactor = ap.k;
		}

		// No need to continue looping if all components are animated
		if (bEnablePos && bEnableRot && bEnableScl)
			break;
	}

	return bEnablePos || bEnableRot || bEnableScl;
}

bool RdNodeAnimExp::GetKeyDataByFrame( INode* pNode, Tab<VecKey*>& pos, Tab<QuatKey*>& rot, Tab<VecKey*>& scl )
{
	bool enablePos = false;
	bool enableRot = false;
	bool enableScl = false;

	if (!CheckForAnimation( pNode, enablePos, enableRot, enableScl ))
		return false;

	Point3 vCurPos;
	Point3 vCurScl;
	Quat qCurRot, qComp;
	qCurRot.Identity();

	VecKey* pPosKey = NULL;
	VecKey* pSclKey = NULL;
	QuatKey* pRotKey = NULL;

	Matrix3 tm, my, parent;
	AffineParts ap;
	ushort usFrame = 0;

	bool parentBone = false;
	bool myBone = IsBoneNode( pNode );

	if( !myBone )
	{
		if( IsMeshNode( pNode ) && IsBoneDummyNode( pNode->GetName() ) )
		{
			myBone = true;
		}
	}

	INode* pParent = pNode->GetParentNode();
	if( pParent && !pParent->IsRootNode() )
	{
		parentBone = IsBoneNode( pParent );		
	}

	int numKeys = 0;

	Matrix3 mat;
	for( ulong t = m_ulStartTime; t <= m_ulEndTime; t += m_ulTFP , ++numKeys )
	{
		my = pNode->GetNodeTM( t );
		parent = pNode->GetParentTM( t );

		if( myBone )
		{
			mat = my;
			my.IdentityMatrix();
			Uniform_Matrix( mat, my );
//			my.NoScale();
		}

		if( parentBone )
		{
			mat = parent;
			parent.IdentityMatrix();
			Uniform_Matrix( mat, parent );
//			parent.NoScale();
		}
		
		tm = my * Inverse( parent );

		decomp_affine( tm, &ap );
		usFrame = (ushort)(( t - m_ulStartTime ) / m_ulTFP);

		vCurPos = ap.t;
		pPosKey = new VecKey();
		pPosKey->Frame = usFrame;
		pPosKey->Vector = Point3( vCurPos.x, vCurPos.z, vCurPos.y );
		pos.Append( 1, &pPosKey );

		vCurScl = ap.k;
		pSclKey = new VecKey();
		pSclKey->Frame = usFrame;
		pSclKey->Vector = Point3( vCurScl.x, vCurScl.z, vCurScl.y );;
		scl.Append( 1, &pSclKey );

		qCurRot = ap.q;
		//if( ( t == m_ulStartTime ) || !qCurRot.IsIdentity() )
		{
			pRotKey = new QuatKey();
			pRotKey->Frame = usFrame;
			pRotKey->Quaternion = Quat( qCurRot.x, qCurRot.z, qCurRot.y, qCurRot.w );
			rot.Append( 1, &pRotKey );
		}
	}

	Tab<VecKey*>	tmpPosKeys;
	Tab<QuatKey*>	tmpRotKeys;
	Tab<VecKey*>	tmpSclKeys;

	for (int i=0; i<numKeys; ++i)
	{
		if ( (i==0) || ((numKeys-1) == i) )
		{
			tmpPosKeys.Append( 1, &pos[i] );
			tmpRotKeys.Append( 1, &rot[i] );
			tmpSclKeys.Append( 1, &scl[i] );
		}
		else
		{
			VecKey* prev = pos[i-1];
			VecKey* current = pos[i];
			VecKey* next = pos[i+1];

			// Point
			if ( !Point3Equal( prev->Vector, current->Vector ) ||
				 !Point3Equal( current->Vector, next->Vector ) )
			{
				tmpPosKeys.Append( 1, &current );
			}

			prev = scl[i-1];
			current = scl[i];
			next = scl[i+1];

			// Scale
			if (!Point3Equal( prev->Vector, current->Vector ) ||
				!Point3Equal( current->Vector, next->Vector ))
			{
				tmpSclKeys.Append( 1, &current );
			}

			// Rotate
			QuatKey* prevRot = rot[i-1];
			QuatKey* currentRot = rot[i];
			QuatKey* nextRot = rot[i+1];

			if (!prevRot->Quaternion.Equals( currentRot->Quaternion ) ||
				!currentRot->Quaternion.Equals( nextRot->Quaternion ))
			{
				tmpRotKeys.Append( 1, &currentRot );
			}
		}
	} // for (

	pos.Init();
	pos.ZeroCount();
	pos.SetCount( tmpPosKeys.Count() );
	pos = tmpPosKeys;

	rot.Init();
	rot.ZeroCount();
	rot.SetCount( tmpRotKeys.Count() );
	rot = tmpRotKeys;

	scl.Init();
	scl.ZeroCount();
	scl.SetCount( tmpSclKeys.Count() );
	scl = tmpSclKeys;

	return true;
}

void RdNodeAnimExp::GetKeyDataByControl( INode* pNode, Control* pControl, Tab<VecKey*>& pos, Tab<QuatKey*>& rot, Tab<VecKey*>& scl )
{
	Control* posControl = pControl->GetPositionController();
	Control* rotControl = pControl->GetRotationController();
	Control* sclControl = pControl->GetScaleController();

	if (posControl && posControl->NumKeys())
	{
		IKeyControl* keyControl = GetKeyControlInterface(posControl);

		if (keyControl)
		{
			int numKeys = keyControl->GetNumKeys();

			ulong partA = posControl->ClassID().PartA();
			if (LININTERP_POSITION_CLASS_ID == partA)
			{
				for (int i=0; i<numKeys; ++i)
				{
					ILinPoint3Key key;
					keyControl->GetKey( i, &key );

					VecKey* pPosKey = new VecKey();
					pPosKey->Frame = key.time * m_ulTFP;
					pPosKey->Vector = Point3( key.val.x, key.val.z, key.val.y );
					pos.Append( 1, &pPosKey );
				}
			} // if
		} // if (keyControl)
	}

	if (rotControl && rotControl->NumKeys())
	{
		IKeyControl* keyControl = GetKeyControlInterface(rotControl);

		if (keyControl)
		{
			int numKeys = keyControl->GetNumKeys();

			if (LININTERP_ROTATION_CLASS_ID == rotControl->ClassID().PartA())
			{
				for (int i=0; i<numKeys; ++i)
				{
					ILinRotKey key;
					keyControl->GetKey( i, &key );

					QuatKey* pRotKey = new QuatKey();
					pRotKey->Frame = key.time * m_ulTFP;
					pRotKey->Quaternion = Quat( key.val.x, key.val.z, key.val.y, key.val.w );
					rot.Append( 1, &pRotKey );
				}
			}
		} // if (keyControl)
	}

	if (sclControl && sclControl->NumKeys())
	{
		IKeyControl* keyControl = GetKeyControlInterface(sclControl);
		if (keyControl)
		{
			int numKeys = keyControl->GetNumKeys();

			if (LININTERP_SCALE_CLASS_ID == sclControl->ClassID().PartA())
			{
				for (int i=0; i<numKeys; ++i)
				{
					ILinScaleKey key;
					keyControl->GetKey( i, &key );

					VecKey* pSclKey = new VecKey();
					pSclKey->Frame = key.time * m_ulTFP;
					pSclKey->Vector = Point3( key.val.s.x, key.val.s.z, key.val.s.y );;
					scl.Append( 1, &pSclKey );
				}
				//MessageBox( NULL, "Scale Linear 방식만 지원", "Error", MB_OK );
			}
		} // if (keyControl)	
	}

}

void RdNodeAnimExp::ExportAnim( const TCHAR* strName )
{
	if( m_NodeList.Count() == 0 )
		return;

	FILE* pFile = fopen( strName, "wb" );

	if( pFile == NULL )
		return;

	ulong ulBegin, ulPtr;

	// Header
	ulong ulNum = RD_ANIMFILE_MAGICNUM;
	WriteChunkBegin( pFile, RD_ANIM_MAGIC, ulPtr, ulBegin );
	{
		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	ulNum = RD_ANIMFILE_VERSION;
	WriteChunkBegin( pFile, RD_ANIM_VERSION, ulPtr, ulBegin );
	{
		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_ANIM_TOTALFRAME, ulPtr, ulBegin );
	{
		fwrite( &m_usTotalFrame, sizeof(ushort), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_ANIM_FRAMERATE, ulPtr, ulBegin );
	{
		fwrite( &m_ulFrameRate, sizeof(ulong), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_ANIM_NODELIST, ulPtr, ulBegin );
	{
		DumpNodeList( pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	fclose( pFile );
}

void RdNodeAnimExp::DumpNodeList( FILE* pFile )
{
	ulong ulPtr, ulBegin, ulSubPtr, ulSubBegin;
	AnimNode* pNode;
	int nCount = m_NodeList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		pNode = m_NodeList[i];

//		if( ( pNode->PosTrack.Count() < 2 ) && 
//			( pNode->RotTrack.Count() < 2 ) && 
//			( pNode->SclTrack.Count() < 2 ) )
//			continue;
		
		WriteChunkBegin( pFile, RD_ANIM_NODE, ulPtr, ulBegin );
		{
			WriteChunkBegin( pFile, RD_ANIM_NODE_ID, ulSubPtr, ulSubBegin );
			{
				fwrite( &pNode->ID, sizeof(ushort), 1, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			WriteChunkBegin( pFile, RD_ANIM_NODE_NAME, ulSubPtr, ulSubBegin );
			{
				WriteString( pFile, pNode->Name );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

//			if( pNode->PosTrack.Count() > 1 )
				DumpPosKey( pFile, pNode->PosTrack );

//			if( pNode->RotTrack.Count() > 1 )
				DumpRotKey( pFile, pNode->RotTrack );

//			if( pNode->SclTrack.Count() > 1 )
				DumpSclKey( pFile, pNode->SclTrack );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

void RdNodeAnimExp::DumpPosKey( FILE* pFile, Tab<VecKey*>& pos )
{
	ulong ulPtr, ulBegin;
	ushort usCnt = (ushort)pos.Count();

	VecKey* pKey;
	WriteChunkBegin( pFile, RD_ANIM_NODE_POSTRACK, ulPtr, ulBegin );
	{
		fwrite( &usCnt, sizeof(ushort), 1, pFile );

		for( ushort i = 0; i < usCnt; ++i )
		{
			pKey = pos[i];
			fwrite( &pKey->Frame, sizeof(ushort), 1, pFile );
			fwrite( &pKey->Vector, sizeof(float), 3, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );
}

void RdNodeAnimExp::DumpRotKey( FILE* pFile, Tab<QuatKey*>& rot )
{
	ulong ulPtr, ulBegin;
	ushort usCnt = (ushort)rot.Count();

	QuatKey* pKey;
	WriteChunkBegin( pFile, RD_ANIM_NODE_ROTTRACK, ulPtr, ulBegin );
	{
		fwrite( &usCnt, sizeof(ushort), 1, pFile );

		for( ushort i = 0; i < usCnt; ++i )
		{
			pKey = rot[i];
			fwrite( &pKey->Frame, sizeof(ushort), 1, pFile );
			fwrite( &pKey->Quaternion, sizeof(float), 4, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

}

void RdNodeAnimExp::DumpSclKey( FILE* pFile, Tab<VecKey*>& scl )
{
	ulong ulPtr, ulBegin;
	ushort usCnt = (ushort)scl.Count();

	VecKey* pKey;
	WriteChunkBegin( pFile, RD_ANIM_NODE_SCLTRACK, ulPtr, ulBegin );
	{
		fwrite( &usCnt, sizeof(ushort), 1, pFile );

		for( ushort i = 0; i < usCnt; ++i )
		{
			pKey = scl[i];
			fwrite( &pKey->Frame, sizeof(ushort), 1, pFile );
			fwrite( &pKey->Vector, sizeof(float), 3, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

}