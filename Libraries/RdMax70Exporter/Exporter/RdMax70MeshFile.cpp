#include "RdMax70MeshExp.h"
#include "..\RdMax70Util.h"
#include "..\RdMax70ChunkDef.h"

#include "../Dependency/minixml.h"

//------------------------------------------------------------------
// 저장 함수들
//------------------------------------------------------------------
void RdMeshExp::ExportMesh( const TCHAR* strName )
{
	UpdateMaterial();

	FILE* pFile = fopen( strName, "wb" );

	if( pFile == NULL )
		return;

	ulong ulBegin, ulPtr;

	// Header
	ulong ulNum = RD_MESHFILE_MAGICNUM;
	WriteChunkBegin( pFile, RD_MESH_MAGIC, ulPtr, ulBegin );
	{
		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	ulNum = RD_MESHFILE_VERSION;
	WriteChunkBegin( pFile, RD_MESH_VERSION, ulPtr, ulBegin );
	{
		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_MESH_NODELIST, ulPtr, ulBegin );
	{
		DumpNodeList( pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	if( m_bTraceTop && m_bTraceBottom )
	{
		WriteChunkBegin( pFile, RD_MESH_TRACE, ulPtr, ulBegin );
		{
			fwrite( &m_ptTraceTop, sizeof(float), 3, pFile );
			fwrite( &m_ptTraceBottom, sizeof(float), 3, pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	if( m_BBoxList.Count() > 0 )
	{
		WriteChunkBegin( pFile, RD_MESH_BBOXLIST, ulPtr, ulBegin );
		{
			DumpBBox( pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	fclose( pFile );
}

void RdMeshExp::DumpNodeList( FILE* pFile )
{
	ulong ulCount = m_NodeList.Count();

	ulong ulPtr, ulBegin;
	BYTE type;
	for( ulong i = 0; i < ulCount; ++i )
	{
		type = m_NodeList[i]->GetNodeType();

		WriteChunkBegin( pFile, RD_MESH_NODE, ulPtr, ulBegin );
		{
			fwrite( &type, sizeof(BYTE), 1, pFile );
			DumpMeshNode( pFile, m_NodeList[i] );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	if( m_pHeightNode )
	{
		WriteChunkBegin( pFile, RD_MESH_NFNODE, ulPtr, ulBegin );
		{
			ulong ulType = 1;
			fwrite( &ulType, sizeof(BYTE), 1, pFile );
			DumpNormFaceNode( pFile, m_pHeightNode );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	ulCount = m_IndoorNodeList.Count();
	for( ulong i = 0; i < ulCount; ++i )
	{
		WriteChunkBegin( pFile, RD_MESH_NFNODE, ulPtr, ulBegin );
		{
			ulong ulType = 2;
			fwrite( &ulType, sizeof(BYTE), 1, pFile );
			DumpNormFaceNode( pFile, m_IndoorNodeList[i] );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

void RdMeshExp::DumpMeshNode( FILE* pFile, RdMeshNode* pNode )
{
	ulong ulPtr, ulBegin;

	ushort id;

	// Node ID
	WriteChunkBegin( pFile, RD_MESH_NODE_ID, ulPtr, ulBegin );
	{
		id = pNode->GetNodeID();
		fwrite( &id, sizeof(ushort), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	// Node Name
	WriteChunkBegin( pFile, RD_MESH_NODE_NAME, ulPtr, ulBegin );
	{
		WriteString( pFile, pNode->GetNodeName() );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	// Parent ID
	WriteChunkBegin( pFile, RD_MESH_NODE_PARENTID, ulPtr, ulBegin );
	{
		id = pNode->GetParentNodeID();
		fwrite( &id, sizeof(ushort), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	// Parent Name
	WriteChunkBegin( pFile, RD_MESH_NODE_PARENTNAME, ulPtr, ulBegin );
	{
		WriteString( pFile, pNode->GetParentNodeName() );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	// Local TM
	WriteChunkBegin( pFile, RD_MESH_NODE_LOCALTM, ulPtr, ulBegin );
	{
		fwrite( &(pNode->GetLocalPosition()), sizeof(float), 3, pFile );
		fwrite( &(pNode->GetLocalRotation()), sizeof(float), 4, pFile );
		fwrite( &(pNode->GetLocalScale()), sizeof(float), 3, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	// World TM
	WriteChunkBegin( pFile, RD_MESH_NODE_WORLDTM, ulPtr, ulBegin );
	{
		fwrite( &(pNode->GetWorldTransform()), sizeof(float), 12, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	if( pNode->GetNodeType() == 0 )
	{
		bool bPhysic = false;
		if( pNode->GetMaxLink() )
		{
			bPhysic = true;
			DumpSkinData( pFile, pNode );
		}

		if( IsUseVertexColor() )
		{
			WriteChunkBegin( pFile, RD_MESH_NODE_VERTEXCOLOR, ulPtr, ulBegin );
			{
				BYTE enable = (pNode->GetEnableVertexColor() == true ) ? 1 : 0;
				fwrite( &enable, sizeof(BYTE), 1, pFile );
			}
			WriteChunkEnd( pFile, ulPtr, ulBegin );
		}

		WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX, ulPtr, ulBegin );
		{	
			ushort usCount = pNode->GetVertexCount();
			fwrite( &usCount, sizeof(ushort), 1, pFile );
			DumpVertexData( pFile, pNode, bPhysic );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );

		WriteChunkBegin( pFile, RD_MESH_NODE_FACELIST, ulPtr, ulBegin );
		{
			DumpFaceData( pFile, pNode );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );

		WriteChunkBegin( pFile, RD_MESH_NODE_AABB, ulPtr, ulBegin );
		{
			fwrite( &(pNode->GetBBoxMin()), sizeof(float), 3, pFile );
			fwrite( &(pNode->GetBBoxMax()), sizeof(float), 3, pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );

		WriteChunkBegin( pFile, RD_MESH_NODE_OBB, ulPtr, ulBegin );
		{
			fwrite( &(pNode->GetOBBCenter()), sizeof(float), 3, pFile );
			fwrite( &(pNode->GetOBBAxisX()), sizeof(float), 3, pFile );
			fwrite( &(pNode->GetOBBAxisY()), sizeof(float), 3, pFile );
			fwrite( &(pNode->GetOBBAxisZ()), sizeof(float), 3, pFile );
			fwrite( &(pNode->GetOBBExtent()), sizeof(float), 3, pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

void RdMeshExp::DumpSkinData( FILE* pFile, RdMeshNode* pNode )
{
	ulong ulPtr, ulBegin;
	Tab<ushort> link = pNode->GetLinkedBoneList();
	ushort usCount = pNode->GetLinkedBoneCount();

	WriteChunkBegin( pFile, RD_MESH_NODE_LINKEDBONE, ulPtr, ulBegin );
	{
		fwrite( &usCount, sizeof(ushort), 1, pFile );
		for( ushort i = 0; i < usCount; ++i )
		{
			fwrite( &link[i], sizeof(ushort), 1, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_MESH_NODE_MAXLINK, ulPtr, ulBegin );
	{
		BYTE link = pNode->GetMaxLink();
		if( link > 4 )
			link = 4;
		fwrite( &link, sizeof(BYTE), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );
}

void RdMeshExp::DumpVertexData( FILE* pFile, RdMeshNode* pNode, bool bPhysic )
{
	ushort usCount = pNode->GetVertexCount();
	Tab<RdVertex*> VertexList = pNode->GetVertexList();

	ulong ulPtr, ulBegin;

	WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_POS, ulPtr, ulBegin );
	{
		for( ushort i = 0; i < usCount; ++i )
		{
			fwrite( &(VertexList[i]->Pos), sizeof(float), 3, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_NORM, ulPtr, ulBegin );
	{
		for( ushort i = 0; i < usCount; ++i )
		{
			fwrite( &(VertexList[i]->Normal), sizeof(float), 3, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_UV, ulPtr, ulBegin );
	{
		for( ushort i = 0; i < usCount; ++i )
		{
			fwrite( &(VertexList[i]->UV), sizeof(float), 2, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	if( bPhysic )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_BLEND, ulPtr, ulBegin );
		{
			BYTE nLinkCount;
			for( ushort i = 0; i < usCount; ++i )
			{
				nLinkCount = VertexList[i]->Link;
				if( nLinkCount > 4 )
					nLinkCount = 4;
				fwrite( &nLinkCount, sizeof(BYTE), 1, pFile );
				for( BYTE j = 0; j < nLinkCount; ++j )
				{
					fwrite( &(VertexList[i]->Blend[j].BoneID), sizeof(ushort), 1, pFile );
					fwrite( &(VertexList[i]->Blend[j].Weight), sizeof(float), 1, pFile );
				}
			}
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	//! 버텍스 칼라 뽑기
	if( pNode->GetEnableVertexColor() && IsUseVertexColor() )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_COLOR, ulPtr, ulBegin );
		{
			for( ushort i = 0; i < usCount; ++i )
			{
				fwrite( &(VertexList[i]->Color), sizeof(float), 3, pFile );
			}
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	//! 멀티 uv 좌표 뽑아내기
	if( pNode->HasMultiUV() )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_UV2, ulPtr, ulBegin );
		{
			for( ushort i = 0; i < usCount; ++i )
			{
				fwrite( &(VertexList[i]->UV2), sizeof(float), 2, pFile );
			}
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	// tangent 구하기
	if (pNode->HasNormalMap() || IsUseVertTangent())
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_TANGENT, ulPtr, ulBegin );
		{
			for (ushort i=0; i<usCount; ++i)
			{
				fwrite( &(VertexList[i]->Tangent), sizeof(float), 3, pFile );
			}
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );

		WriteChunkBegin( pFile, RD_MESH_NODE_VERTEX_BINORMAL, ulPtr, ulBegin );
		{
			for (ushort i=0; i<usCount; ++i)
			{
				fwrite( &(VertexList[i]->Binormal), sizeof(float), 3, pFile );
			}
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

void RdMeshExp::DumpFaceData( FILE* pFile, RdMeshNode* pNode )
{
	ushort usCount = pNode->GetFaceCount();
	Tab<RdFace*> FaceList = pNode->GetFaceList();

	ushort usCurMtlIdx = FaceList[0]->MatID;
	ushort usLastCount = 0;
	ulong ulPtr, ulBegin;
	ulong ulSubPtr, ulSubBegin;
	RdMaterial* pMtl;
	ushort usAdd = 0;
	while( usLastCount != usCount )
	{
		usAdd = 1;
		for( ulong i = usLastCount+1; i < usCount; ++i )
		{
			if( usCurMtlIdx != FaceList[i]->MatID )
			{
				usCurMtlIdx = FaceList[i]->MatID;
				break;
			}
			++usAdd;
		}

		pMtl = m_MtlList[FaceList[usLastCount]->MatID];

		// 저장
		WriteChunkBegin( pFile, RD_MESH_NODE_FACE, ulPtr, ulBegin );
		{
			fwrite( &usAdd, sizeof(ushort), 1, pFile );

			WriteChunkBegin( pFile, RD_MESH_NODE_FACE_INDICES, ulSubPtr, ulSubBegin );
			{
				for( ushort j = 0; j < usAdd; ++j )
				{	
					fwrite( FaceList[usLastCount+j]->Index, sizeof(ushort), 3, pFile );
				}
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			if( pMtl )
			{
				DumpMaterialData( pFile, pMtl );
			}
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );

		usLastCount += usAdd;
	}
}

void RdMeshExp::DumpMaterialData( FILE* pFile, RdMaterial* pMtl )
{
	ulong ulPtr, ulBegin;
	BYTE value;

	if( pMtl->HaveDiffuseMap() )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_FACE_DIMAP, ulPtr, ulBegin );
		{
			WriteString( pFile, pMtl->GetDiffuseMap() );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	if( pMtl->HaveSpecularMap() )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_FACE_SPMAP, ulPtr, ulBegin );
		{
			WriteString( pFile, pMtl->GetSpecularMap() );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
	
	value = pMtl->GetTwoSide();
	if( value )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_FACE_TWOSIDE, ulPtr, ulBegin );
		{
			fwrite( &value, sizeof(BYTE), 1, pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	value = pMtl->GetOpacity();
	if( value )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_FACE_OPACITY, ulPtr, ulBegin );
		{
			fwrite( &value, sizeof(BYTE), 1, pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	value = pMtl->GetAlphaBlending();
	if( value )
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_FACE_BLENDING, ulPtr, ulBegin );
		{
			fwrite( &value, sizeof(BYTE), 1, pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}

	if (pMtl->HaveNormalMap())
	{
		WriteChunkBegin( pFile, RD_MESH_NODE_FACE_NRMAP, ulPtr, ulBegin );
		{
			WriteString( pFile, pMtl->GetNormalMap() );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

void RdMeshExp::DumpNormFaceNode( FILE* pFile, RdNormFaceNode* pNode )
{
	ulong ulPtr, ulBegin;

	ushort usCount = pNode->GetVertexCount();
	Tab<Point3> VertexList = pNode->GetVertexList();
	WriteChunkBegin( pFile, RD_MESH_NFNODE_VERTEX, ulPtr, ulBegin );
	{
		fwrite( &usCount, sizeof(ushort), 1, pFile );
		for( ushort i = 0; i < usCount; ++i )
		{
			fwrite( &VertexList[i], sizeof(float), 3, pFile );
		}
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	ulong ulSubPtr, ulSubBegin;
	usCount = pNode->GetFaceCount();
	Tab<RdNormFace*> FaceList = pNode->GetFaceList();
	WriteChunkBegin( pFile, RD_MESH_NFNODE_FACE, ulPtr, ulBegin );
	{
		fwrite( &usCount, sizeof(ushort), 1, pFile );

		WriteChunkBegin( pFile, RD_MESH_NFNODE_FACE_INDEX, ulSubPtr, ulSubBegin );
		{
			for( ushort i = 0; i < usCount; ++i )
			{
				fwrite( FaceList[i]->Index, sizeof(ushort), 3, pFile );
			}
		}
		WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

		WriteChunkBegin( pFile, RD_MESH_NFNODE_FACE_NORMAL, ulSubPtr, ulSubBegin );
		{
			for( ushort i = 0; i < usCount; ++i )
			{
				fwrite( &(FaceList[i]->Normal), sizeof(float), 3, pFile );
			}
		}
		WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );
}

void RdMeshExp::DumpBBox( FILE* pFile )
{
	ulong ulPtr, ulBegin;
	ulong ulSubPtr, ulSubBegin;

	ushort id;

	int nNumBounds = m_BBoxList.Count();
	for ( int i = 0; i < nNumBounds; ++i )
	{
		RdBBox* pBox = m_BBoxList[i];

		WriteChunkBegin( pFile, RD_MESH_BBOX, ulPtr, ulBegin );
		{
			WriteChunkBegin( pFile, RD_MESH_BBOX_PARENTID, ulSubPtr, ulSubBegin );
			{
				id = pBox->GetParentID();
				fwrite( &id, sizeof(ushort), 1, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			WriteChunkBegin( pFile, RD_MESH_BBOX_PARENTNAME, ulSubPtr, ulSubBegin );
			{
				WriteString( pFile, pBox->GetParentName() );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			WriteChunkBegin( pFile, RD_MESH_BBOX_OBB, ulSubPtr, ulSubBegin );
			{
				fwrite( &(pBox->GetOBBCenter()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBAxisX()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBAxisY()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBAxisZ()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBExtent()), sizeof(float), 3, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

//------------------------------------------------------------------
//	Material 정보 저장
//------------------------------------------------------------------
void RdMeshExp::ExportMaterial( const TCHAR* strName )
{
	CMiniXML xml;

	CMiniXML::node xmlRootNode = xml.Insert( xml.GetRoot(), _T("Material") );
	if (xmlRootNode != NULL)
	{
		// 날짜 등록
		FILETIME CurrentTime;
		GetSystemTimeAsFileTime( &CurrentTime );

		WORD Date, Time;
		if( FileTimeToLocalFileTime(&CurrentTime, &CurrentTime) &&
					FileTimeToDosDateTime(&CurrentTime, &Date, &Time) )
		{
			TCHAR szTimeBuffer[100];
			szTimeBuffer[0] = _T('\0');

			sprintf(szTimeBuffer, "%d/%d/%d %02d:%02d:%02d",
						(Date / 32) & 15, Date & 31, (Date / 512) + 1980,
						(Time >> 11), (Time >> 5) & 0x3F, (Time & 0x1F) * 2);

			CMiniXML::node xmlDateNode = xml.InsertChild( xmlRootNode, _T("Date") );
			xmlDateNode.SetAttribute( _T("date"), szTimeBuffer );
		}

		// MeshNode 리스트
		int numMeshes = m_NodeList.Count();
		for (int i=0; i < numMeshes; ++i)
		{
			RdMeshNode* meshNode = m_NodeList[i];

			CMiniXML::node xmlMeshNode = xml.InsertChild( xmlRootNode, _T("MeshNode") );
			if (xmlMeshNode != NULL)
			{
				const TCHAR* nodeName = meshNode->GetNodeName();
				xmlMeshNode.SetAttribute( _T("name"), nodeName );

				ushort numFaces = meshNode->GetFaceCount();
				Tab<RdFace*> FaceList = meshNode->GetFaceList();

				ushort usCurMtlIdx = FaceList[0]->MatID;
				ushort usLastCount = 0;
				RdMaterial* pMtl;
				ushort usAdd = 0;

				int nSubMesh = 0;

				while (usLastCount != numFaces)
				{
					usAdd = 1;
					for( ulong i = usLastCount+1; i < numFaces; ++i )
					{
						if( usCurMtlIdx != FaceList[i]->MatID )
						{
							usCurMtlIdx = FaceList[i]->MatID;
							break;
						}
						++usAdd;
					}

					pMtl = m_MtlList[FaceList[usLastCount]->MatID];
					if (pMtl)
					{
						// submesh
						CMiniXML::node xmlSubMeshNode = xml.InsertChild( xmlMeshNode, _T("SubMesh") );
						if (xmlSubMeshNode != NULL)
						{
							TCHAR buffer[10];
							_itoa( nSubMesh, buffer, 10 );
							xmlSubMeshNode.SetAttribute( _T("id"), buffer );

							// Texture
							CMiniXML::node xmlTexNode = xml.InsertChild( xmlSubMeshNode, _T("Texture") );
							if (xmlTexNode != NULL)
							{
								if (pMtl->HaveDiffuseMap())
								{
									const TCHAR* diffuseMap = pMtl->GetDiffuseMap();
									CMiniXML::node xmlDiffuseNode = xml.InsertChild( xmlTexNode, _T("DiffuseMap") );
									xmlDiffuseNode.SetAttribute( _T("name"), diffuseMap );
								}

								if (pMtl->HaveSpecularMap())
								{
									const TCHAR* specularMap = pMtl->GetSpecularMap();
									CMiniXML::node xmlSpecNode = xml.InsertChild( xmlTexNode, _T("SpecularMap") );
									xmlSpecNode.SetAttribute( _T("name"), specularMap );
								}

								if (pMtl->HaveNormalMap())
								{
									const TCHAR* normalMap = pMtl->GetNormalMap();
									CMiniXML::node xmlNormalNode = xml.InsertChild( xmlTexNode, _T("NormalMap") );
									xmlNormalNode.SetAttribute( _T("name"), normalMap );
								}
							}

							// ColorState
							CMiniXML::node xmlCSNode = xml.InsertChild( xmlSubMeshNode, _T("ColorState") );
							if (xmlCSNode != NULL)
							{
								CMiniXML::node xmlAmbient = xml.InsertChild( xmlCSNode, _T("Ambient") );
								xmlAmbient.SetAttribute( _T("a"), _T("255") );
								xmlAmbient.SetAttribute( _T("b"), _T("255") );
								xmlAmbient.SetAttribute( _T("g"), _T("255") );
								xmlAmbient.SetAttribute( _T("r"), _T("255") );

								CMiniXML::node xmlDiffuse = xml.InsertChild( xmlCSNode, _T("Diffuse") );
								xmlDiffuse.SetAttribute( _T("a"), _T("255") );
								xmlDiffuse.SetAttribute( _T("b"), _T("255") );
								xmlDiffuse.SetAttribute( _T("g"), _T("255") );
								xmlDiffuse.SetAttribute( _T("r"), _T("255") );

								CMiniXML::node xmlSpecular = xml.InsertChild( xmlCSNode, _T("Specular") );
								xmlSpecular.SetAttribute( _T("a"), _T("255") );
								xmlSpecular.SetAttribute( _T("b"), _T("255") );
								xmlSpecular.SetAttribute( _T("g"), _T("255") );
								xmlSpecular.SetAttribute( _T("r"), _T("255") );

								CMiniXML::node xmlEmissive = xml.InsertChild( xmlCSNode, _T("Emissive") );
								xmlEmissive.SetAttribute( _T("a"), _T("255") );
								xmlEmissive.SetAttribute( _T("b"), _T("255") );
								xmlEmissive.SetAttribute( _T("g"), _T("255") );
								xmlEmissive.SetAttribute( _T("r"), _T("255") );

								CMiniXML::node xmlShininess = xml.InsertChild( xmlCSNode, _T("Shininess") );
								xmlShininess.SetAttribute( _T("value"), _T("6") );
							}

							// RenderState
							CMiniXML::node xmlRSNode = xml.InsertChild( xmlSubMeshNode, _T("RenderState") );
							if (xmlRSNode != NULL)
							{
								if (pMtl->GetTwoSide())
								{
									CMiniXML::node xmlCullNode = xml.InsertChild( xmlRSNode, _T("CullMode") );
									xmlCullNode.SetAttribute( _T("mode"), _T("none") );
								}
								else
								{
									CMiniXML::node xmlCullNode = xml.InsertChild( xmlRSNode, _T("CullMode") );
									xmlCullNode.SetAttribute( _T("mode"), _T("ccw") );
								}

								if (pMtl->GetOpacity())
								{
									CMiniXML::node xmlATNode = xml.InsertChild( xmlRSNode, _T("AlphaTest") );
									xmlATNode.SetAttribute( _T("mode"), _T("GreateEqual") );
									xmlATNode.SetAttribute( _T("ref"), _T("50") );
									xmlATNode.SetAttribute( _T("enable"), _T("true") );
								}

								if(pMtl->GetAlphaBlending())
								{
									CMiniXML::node xmlABNode = xml.InsertChild( xmlRSNode, _T("AlphaBlend") );
									xmlABNode.SetAttribute( _T("dest"), _T("InvSrcBlend") );
									xmlABNode.SetAttribute( _T("src"), _T("SrcBlend") );
									xmlABNode.SetAttribute( _T("enable"), _T("true") );
								}
							}
						}
					}
					usLastCount += usAdd;
					nSubMesh++;
				}

			}
		}
	}

	xml.Save( strName );
}
