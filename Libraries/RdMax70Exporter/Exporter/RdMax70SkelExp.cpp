#include "RdMax70SkelExp.h"
#include "..\RdMax70Util.h"
#include "..\RdMax70ChunkDef.h"

RdSkelExp::RdSkelExp( Interface* pInterface, HWND hWnd ) : m_pInterface( pInterface ), m_hSkelExpDlg( hWnd )
{
	m_BoneList.Init();
	m_BoneList.ZeroCount();

	m_BBoxList.Init();
	m_BBoxList.ZeroCount();

	m_BoneDefines.Init();
	m_BoneDefines.ZeroCount();

	m_bEnableExport	= true;

	m_eActiveDefine = RdBaseNode::DEFINE_NONE;
}

RdSkelExp::~RdSkelExp()
{
	Clear();	
}

void RdSkelExp::Clear()
{
	int nCount = m_BoneList.Count();
	RdBone** ppNode;
	for( int i = 0; i < nCount; ++i )
	{
		ppNode = m_BoneList.Addr( i );

		delete (*ppNode);
	}
	m_BoneList.Delete( 0, nCount );

	nCount = m_BBoxList.Count();
	RdBBox** ppBox;
	for( int i = 0; i < nCount; ++i )
	{
		ppBox = m_BBoxList.Addr( i );

		delete (*ppBox );
	}
	m_BBoxList.Delete( 0, nCount );

	nCount = m_BoneDefines.Count();
	BoneDefine** ppBoneDefine;
	for( int i = 0; i < nCount; ++i )
	{
		ppBoneDefine = m_BoneDefines.Addr( i );

		delete (*ppBoneDefine );
	}
	m_BoneDefines.Delete( 0, nCount );
}

void RdSkelExp::Initialize()
{
	int nChildNum = m_pInterface->GetRootNode()->NumberOfChildren();
	for( int i = 0; i < nChildNum; ++i )
	{
		CollectBone( m_pInterface->GetRootNode()->GetChildNode( i ) );
	}

	if( m_BoneList.Count() == 0 )
		m_bEnableExport = false;
}

void RdSkelExp::OutputInformation()
{
	if( !m_bEnableExport )	
	{
		HWND hWnd = GetDlgItem( m_hSkelExpDlg, IDC_SKEL_SAVE );
		EnableWindow( hWnd, FALSE );
	}

	MakeBoneList();

	ushort usCount = (ushort)m_BoneList.Count();
	SetDlgItemInt( m_hSkelExpDlg, IDC_BONE_CNT, usCount, FALSE );

	usCount = (ushort)m_BBoxList.Count();
	SetDlgItemInt( m_hSkelExpDlg, IDC_BBOX_CNT, usCount, FALSE );

	
}

void RdSkelExp::MakeBoneList()
{
	HWND hList = GetDlgItem( m_hSkelExpDlg, IDC_BONE_LISTBOX );

	TCHAR strName[256];
	int nCount = m_BoneList.Count();

	for( int i = 0; i < nCount; ++i )
	{
		memset( strName, 0, sizeof(TCHAR)*256 );
		_stprintf( strName, _T("[%d]%s"), m_BoneList[i]->GetNodeID(), m_BoneList[i]->GetNodeName() );

		SendMessage( hList, LB_ADDSTRING, 0, (LPARAM)strName );

		//! 초기값 설정 좀 지저분 하죠?! ^^a;;
		if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 Head" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_HEAD, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 Spine" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_SPINE, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 R Hand" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_RHAND, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 L Hand" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_LHAND, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 R Foot" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_RFOOT, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 L Foot" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_LFOOT, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 R UpperArm" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_RUARM, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 L UpperArm" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_LUARM, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 R Forearm" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_RLARM, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 L Forearm" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_LLARM, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 R Thigh" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_RTHIGH, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 L Thigh" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_LTHIGH, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 R Calf" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_RCALF, strName );
		else if( 0 == _tcscmp( m_BoneList[i]->GetNodeName(), "Bip01 L Calf" ) )
			SetDlgItemText( m_hSkelExpDlg, IDC_EDIT_DEFINE_LCALF, strName );
	}
}

void RdSkelExp::CollectBone( INode* pNode )
{
	if( UndesirableNode( pNode ) )
		return;
	
	ushort id = GetIndexOfNode( pNode );

	if( IsMeshNode( pNode ) )
	{
		if( IsBoneDummyNode( pNode->GetName() ) )
		{
			RdBone* pNodeData = new RdBone( 2, id, pNode->GetName() );
			m_BoneList.Append( 1, &pNodeData );
			CollectBoneData( pNode, pNodeData );
		}
		else if( IsBoundingBoxNode( pNode->GetName() ) )
		{
			CollectBBoxData( pNode );				
		}
	}
	else if( IsBoneNode( pNode ) )
	{
		RdBone* pNodeData = new RdBone( 1, id, pNode->GetName() );
		m_BoneList.Append( 1, &pNodeData );
		CollectBoneData( pNode, pNodeData );
	}

	int num = pNode->NumberOfChildren();
	for( int i = 0; i < num; ++i )
	{
		CollectBone( pNode->GetChildNode( i ) );
	}
}

void RdSkelExp::CollectBoneData( INode* pNode, RdBone* pNodeData )
{
	Matrix3 world = pNode->GetNodeTM( 0 );
//	Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
	
//	bool bNeg = world.Parity() ? true : false;
//	if( bNeg )
//	{
		Matrix3 tmp = world;
		Uniform_Matrix( tmp, world );
//	}
//	else
//	{
//		world.NoScale();
//	}
	Matrix3 local;// = world;

	INode* pParent = pNode->GetParentNode();

	if( pParent && !pParent->IsRootNode() )
	{
		pNodeData->SetParentNode( GetIndexOfNode( pParent ), pParent->GetName() );
		Matrix3 worldp = pParent->GetNodeTM( 0 );
//		Matrix3 worldp = pParent->GetObjTMAfterWSM( 0 );
//		worldp.NoScale();
		tmp = worldp;
		Uniform_Matrix( tmp, worldp );

		local = world * Inverse( worldp );
	}
	else
	{
		local = world;
	}

	AffineParts ap;
	decomp_affine( local, &ap );
	pNodeData->SetLocalPosition( Point3( ap.t.x, ap.t.z, ap.t.y ) );	
	pNodeData->SetLocalRotation( Quat( ap.q.x, ap.q.z, ap.q.y, ap.q.w ) );
	pNodeData->SetLocalScale( Point3( ap.k.x, ap.k.z, ap.k.y ) );

	ConvertMatrixToD3D( &world );
	pNodeData->SetWorldTransform( world );
}

void RdSkelExp::CollectBBoxData( INode* pNode )
{
	RdBBox* pBoxNode = new RdBBox();

	INode* pParent = pNode->GetParentNode();
	if( pParent && !pParent->IsRootNode() )
	{
		ushort idx = GetIndexOfNode( pParent );
		pBoxNode->SetParentNode( idx, pParent->GetName() );

		RdBone* pBone = GetBone( idx );
		pBoxNode->SetParentNodeXform( pBone->GetWorldTransform() );
	}
	
	ObjectState os = pNode->EvalWorldState( 0 );

	Object* pObj = os.obj;
	TriObject* pTriObj;
	BOOL bConvertedToTriObject = ( pObj->CanConvertToType( triObjectClassID ) ) && 
		( ( pTriObj = (TriObject*)pObj->ConvertToType( 0, triObjectClassID ) ) != NULL );

	if( !bConvertedToTriObject )
	{
		delete (pBoxNode);
		return;
	}

	Matrix3 world = pNode->GetObjTMAfterWSM( 0 );
	Mesh* pMesh = &pTriObj->GetMesh();
	if( NULL == pMesh || pMesh->getNumVerts() == 0 || pMesh->getNumFaces() == 0 )
	{
		delete (pBoxNode);
		return;
	}

	Point3 pos;
	int nCount = pMesh->getNumVerts();
	for( int i = 0; i < nCount; ++i )
	{
		pos = world * pMesh->verts[i];
		pBoxNode->AddVertex( Point3( pos.x, pos.z, pos.y ) );
	}

	pBoxNode->CalculateOBB();

	m_BBoxList.Append( 1, &pBoxNode );
}


void RdSkelExp::ExportSkel( const TCHAR* strName )
{
	FILE* pFile = fopen( strName, "wb" );
	if( NULL == pFile )
		return;

	ulong ulBegin, ulPtr;

	// Header
	ulong ulNum = RD_SKELFILE_MAGICNUM;
	WriteChunkBegin( pFile, RD_SKEL_MAGIC, ulPtr, ulBegin );
	{
		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	ulNum = RD_SKELFILE_VERSION;
	WriteChunkBegin( pFile, RD_SKEL_VERSION, ulPtr, ulBegin );
	{
		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_SKEL_BONELIST, ulPtr, ulBegin );
	{
		DumpBone( pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_SKEL_BBOXLIST, ulPtr, ulBegin );
	{
		DumpBBox( pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	WriteChunkBegin( pFile, RD_SKEL_BONE_DEFINE_LIST, ulPtr, ulBegin );
	{
		DumpBoneDefine( pFile );
	}
	WriteChunkEnd( pFile, ulPtr, ulBegin );

	fclose( pFile );
}

void RdSkelExp::DumpBone( FILE* pFile )
{
	ulong ulCount = m_BoneList.Count();

	ulong ulPtr, ulBegin, ulSubPtr, ulSubBegin;
	BYTE type;
	RdBone* pBone;
	ushort id;
	for( ulong i = 0; i < ulCount; ++i )
	{
		pBone = m_BoneList[i];
		type = pBone->GetNodeType();

		WriteChunkBegin( pFile, RD_SKEL_BONE, ulPtr, ulBegin );
		{
			fwrite( &type, sizeof(BYTE), 1, pFile );
			// Node ID
			WriteChunkBegin( pFile, RD_SKEL_BONE_ID, ulSubPtr, ulSubBegin );
			{
				id = pBone->GetNodeID();
				fwrite( &id, sizeof(ushort), 1, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			// Node Name
			WriteChunkBegin( pFile, RD_SKEL_BONE_NAME, ulSubPtr, ulSubBegin );
			{
				WriteString( pFile, pBone->GetNodeName() );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			// Parent ID
			WriteChunkBegin( pFile, RD_SKEL_BONE_PARENTID, ulSubPtr, ulSubBegin );
			{
				id = pBone->GetParentNodeID();
				fwrite( &id, sizeof(ushort), 1, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			// Parent Name
			WriteChunkBegin( pFile, RD_SKEL_BONE_PARENTNAME, ulSubPtr, ulSubBegin );
			{
				WriteString( pFile, pBone->GetParentNodeName() );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			// Local TM
			WriteChunkBegin( pFile, RD_SKEL_BONE_LOCALTM, ulSubPtr, ulSubBegin );
			{
				fwrite( &(pBone->GetLocalPosition()), sizeof(float), 3, pFile );
				fwrite( &(pBone->GetLocalRotation()), sizeof(float), 4, pFile );
				fwrite( &(pBone->GetLocalScale()), sizeof(float), 3, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			// World TM
			WriteChunkBegin( pFile, RD_SKEL_BONE_WORLDTM, ulSubPtr, ulSubBegin );
			{
				fwrite( &(pBone->GetWorldTransform()), sizeof(float), 12, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			if( pBone->GetNodeType() == 1 )
			{
				WriteChunkBegin( pFile, RD_SKEL_BONE_AABB, ulSubPtr, ulSubBegin );
				{
					fwrite( &(pBone->GetBBoxMin()), sizeof(float), 3, pFile );
					fwrite( &(pBone->GetBBoxMax()), sizeof(float), 3, pFile );
				}
				WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

				WriteChunkBegin( pFile, RD_SKEL_BONE_OBB, ulSubPtr, ulSubBegin );
				{
					fwrite( &(pBone->GetOBBCenter()), sizeof(float), 3, pFile );
					fwrite( &(pBone->GetOBBAxisX()), sizeof(float), 3, pFile );
					fwrite( &(pBone->GetOBBAxisY()), sizeof(float), 3, pFile );
					fwrite( &(pBone->GetOBBAxisZ()), sizeof(float), 3, pFile );
					fwrite( &(pBone->GetOBBExtent()), sizeof(float), 3, pFile );
				}
				WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );
			}
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

void RdSkelExp::DumpBBox( FILE* pFile )
{
	ulong ulCount = m_BBoxList.Count();

	ulong ulPtr, ulBegin, ulSubPtr, ulSubBegin;
	RdBBox* pBox;

	ushort id;

	for( ulong i = 0; i < ulCount; ++i )
	{
		pBox = m_BBoxList[i];

		WriteChunkBegin( pFile, RD_SKEL_BBOX, ulPtr, ulBegin );
		{
			WriteChunkBegin( pFile, RD_SKEL_BBOX_PARENTID, ulSubPtr, ulSubBegin );
			{
				id = pBox->GetParentID();
				fwrite( &id, sizeof(ushort), 1, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );

			// Parent Name
			WriteChunkBegin( pFile, RD_SKEL_BBOX_PARENTNAME, ulSubPtr, ulSubBegin );
			{
				WriteString( pFile, pBox->GetParentName() );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );
			
			WriteChunkBegin( pFile, RD_SKEL_BBOX_OBB, ulSubPtr, ulSubBegin );
			{
				fwrite( &(pBox->GetOBBCenter()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBAxisX()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBAxisY()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBAxisZ()), sizeof(float), 3, pFile );
				fwrite( &(pBox->GetOBBExtent()), sizeof(float), 3, pFile );
			}
			WriteChunkEnd( pFile, ulSubPtr, ulSubBegin );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

void RdSkelExp::DumpBoneDefine( FILE* pFile )
{
	ulong ulCount = m_BoneDefines.Count();

	ulong ulPtr, ulBegin;
	BoneDefine* pDefine;

	ushort usIndex = 0;

	for( ulong i = 0; i < ulCount; ++i )
	{
		pDefine = m_BoneDefines[i];

		WriteChunkBegin( pFile, RD_SKEL_DEFINE, ulPtr, ulBegin );
		{
			usIndex = (ushort)pDefine->nIndex;
			fwrite( &usIndex, sizeof(ushort), 1, pFile );

			usIndex = pDefine->usBoneIndex;
			fwrite( &usIndex, sizeof(ushort), 1, pFile );
		}
		WriteChunkEnd( pFile, ulPtr, ulBegin );
	}
}

RdBone* RdSkelExp::GetBone( ushort usBoneID )
{
	int nCount = m_BoneList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		if( m_BoneList[i]->GetNodeID() == usBoneID )
			return m_BoneList[i];
	}

	return NULL;
}

RdBone* RdSkelExp::GetBone( const TCHAR* strName )
{
	int nCount = m_BoneList.Count();
	for( int i = 0; i < nCount; ++i )
	{
		if( _tcscmp( m_BoneList[i]->GetNodeName(), strName ) == 0  )
			return m_BoneList[i];
	}

	return NULL;
}

bool RdSkelExp::AddBoneDefine( int nIndex, const TCHAR* strBone )
{
	if( strlen( strBone ) <= 0 )
		return true;

	// [0]Bip01 이런 식으로 이름이 정해지므로, [id]에서 id만을 얻어온다.
	int i = atoi( &strBone[1] );
	if( i <= m_BoneList.Count() )
	{
		BoneDefine* pDefine = new BoneDefine();
		pDefine->nIndex = nIndex;
		pDefine->usBoneIndex = (ushort)i;

		m_BoneDefines.Append( 1, &pDefine );

		return true;
	}

	return false;
}