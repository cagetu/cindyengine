#ifndef __RDMAX70SKELEXP_H__
#define __RDMAX70SKELEXP_H__

#pragma once

#include "..\Data\RdMax70Bone.h"
#include "..\Data\RdMax70BBox.h"

struct BoneDefine
{
	int		nIndex;
	ushort	usBoneIndex;

	BoneDefine() : nIndex(-1), usBoneIndex(0xffff) {}
};

class RdSkelExp
{
protected:
	Interface*			m_pInterface;
	HWND				m_hSkelExpDlg;
	
	Tab<RdBone*>		m_BoneList;
	Tab<RdBBox*>		m_BBoxList;

	Tab<BoneDefine*>	m_BoneDefines;

	RdBaseNode::DEFINE_TYPE		m_eActiveDefine;		//! 버튼 클릭시 활성화된 Define 버튼 설정

	bool			m_bEnableExport;

	void			Clear();

	void			CollectBone( INode* pNode );
	void			CollectBoneData( INode* pNode, RdBone* pNodeData );
	void			CollectBBoxData( INode* pNode );

	void			DumpBone( FILE* pFile );
	void			DumpBBox( FILE* pFile );
	void			DumpBoneDefine( FILE* pFile );

	void			MakeBoneList();

	RdBone*			GetBone( ushort usBoneID );
	RdBone*			GetBone( const TCHAR* strName );
	
public:
	RdSkelExp( Interface* pInterface, HWND hWnd );
	~RdSkelExp();

	void			Initialize();
	void			OutputInformation();

	void			ExportSkel( const TCHAR* strName );

	void						SetActiveDefine( RdBaseNode::DEFINE_TYPE eType )	{		m_eActiveDefine = eType;		}
	RdBaseNode::DEFINE_TYPE		GetActiveDefine() const								{		return m_eActiveDefine;			}

	bool			AddBoneDefine( int nIndex, const TCHAR* strBone );
};



#endif // __RDMAX70SKELEXP_H__