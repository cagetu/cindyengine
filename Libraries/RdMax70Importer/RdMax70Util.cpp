#include "RdMax70Util.h"

//------------------------------------------------------------------
NodeName g_arNodeNameList[512];
ushort g_nNodeCount = 0;

//------------------------------------------------------------------
bool UndesirableNode( INode* pNode )
{
	Object* pObj = pNode->GetObjectRef();

	if( pObj->SuperClassID() == CAMERA_CLASS_ID )
		return true;

	if( pObj->SuperClassID() == LIGHT_CLASS_ID )
		return true;

	if( pObj->SuperClassID() == TARGET_CLASS_ID )
		return true;

	return false;
}

//------------------------------------------------------------------
void SetIndexOfNode( INode* pNode, ushort nNode )
{
	NodeName* pName;

	ushort i = 0;
	for( i = 0; i < g_nNodeCount; ++i )
	{
		if( _tcscmp( g_arNodeNameList[i].Name, pNode->GetName() ) == 0 )
			break;
	}

	if( i < g_nNodeCount )
		pName = &g_arNodeNameList[i];
	else
	{
		ASSERT_MBOX( g_nNodeCount < 512, _T( "NodeNameList is full" ) );
		pName = &g_arNodeNameList[g_nNodeCount++];
		_tcscpy( pName->Name, pNode->GetName() );
	}

	pName->ID = nNode;
}

ushort GetIndexOfNode( INode* pNode )
{
	for( ushort i = 0; i < g_nNodeCount; ++i )
	{
		if( _tcscmp( g_arNodeNameList[i].Name, pNode->GetName() ) == 0 )
		{
			return g_arNodeNameList[i].ID;
		}
	}

	return UNDESIRABLE_MARKER;
}


bool IsDummyNode( INode* pNode )
{
	if( NULL == pNode || pNode->IsRootNode() )
		return false;
	
	Object* pObj = pNode->EvalWorldState(0).obj;
	if( ( pObj->SuperClassID() == HELPER_CLASS_ID ) && ( pObj->ClassID() == Class_ID( DUMMY_CLASS_ID, 0 ) ) )
	{
//		Control* pCont = pNode->GetTMController();
//
//		if(	( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID ) ||
//			( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) )// ||
////			( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )
//			return false;

		return true;
	}

	return false;
}

bool IsMeshNode( INode* pNode )
{
	if( NULL == pNode || pNode->IsRootNode() )
		return false;

	Object* pObj = pNode->EvalWorldState(0).obj;
	if( pObj->SuperClassID() != GEOMOBJECT_CLASS_ID )
		return false;

	if( pObj->ClassID() == BONE_OBJ_CLASSID || pObj->ClassID() == Class_ID( BONE_CLASS_ID, 0 ) )
		return false;

	Control* pCont = pNode->GetTMController();
	if( pCont )
	{
		if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID )	||		// Biped Other Parts.
			( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) 	||		// Biped Root "Bip01".
			( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )				// 
		{
			return false;
		}
	}

	return true;
}

bool IsBoneNode( INode* pNode )
{
	if( NULL == pNode || pNode->IsRootNode() )
		return false;

	//******************************************************************
	//	comment by cagetu [2006-11-2]
	//	Biped 데이터에 Nub이라는 이름을 가진 녀석들은 원래 속성이 HelperObject(Dummy) 속성을 가진 녀석이다.
	//	그래서, 계산량을 한번이라도 줄이기 위해서, 이 녀석들을 뽑지 않으려고 더미 노드를 먼저 거르고, 본인지 여부를
	//	체크 하였는데, skin을 적용하다보니, HelperObject에도 영향을 받는 버텍스들이 존재하기 때문에 그 문제를 해결하기
	//	위해서, Bone/Biped(HelperObject포함)를 뭔지 체크해서 결과를 보고하고, Helper를 체크하기로 한다.
	//******************************************************************
	Object* pObj = pNode->EvalWorldState(0).obj;
	if( pObj->ClassID() == BONE_OBJ_CLASSID || pObj->ClassID() == Class_ID( BONE_CLASS_ID, 0 )  )
		return true;

	Control* pCont = pNode->GetTMController();
	if( pCont )
	{
		if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID )	||		// Biped Other Parts.
			( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) 	||		// Biped Root "Bip01".
			( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )				// FootStep
		{
			return true;
		}
	}

//	Object* pObj = pNode->EvalWorldState(0).obj;
	if( pObj->ClassID() == Class_ID( DUMMY_CLASS_ID, 0 ) || ( pObj->SuperClassID() == HELPER_CLASS_ID ) )
	{
		if( IsDummyBoneNode( pNode->GetName() ) )
			return true;

		//Control* pCont = pNode->GetTMController();

		//if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID ) ||
		//	( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) )//  ||
		//	( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )
		//	return true;
	
		return false;
	}

	// 이동..
	//if( pObj->ClassID() == BONE_OBJ_CLASSID || pObj->ClassID() == Class_ID( BONE_CLASS_ID, 0 )  )
	//	return true;

	//Control* pCont = pNode->GetTMController();
	//if( pCont )
	//{
	//	if( ( pCont->ClassID() == BIPSLAVE_CONTROL_CLASS_ID )	||		// Biped Other Parts.
	//		( pCont->ClassID() == BIPBODY_CONTROL_CLASS_ID ) 	||		// Biped Root "Bip01".
	//		( pCont->ClassID() == FOOTPRINT_CLASS_ID ) )				// 
	//	{
	//		return true;
	//	}
	//}

	return false;
}

// GeomtryObject 중에 본으로 사용하도록 한다.
bool IsBoneDummyNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("DB_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("db_"), 3 ) == 0 )
		return true;

	return false;
}

// 더미 노드 중에 본으로 인식하도록 한다.
bool IsDummyBoneNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("BD_"), 3 ) == 0 ||
		_tcsnicmp( strName, _T("bd_"), 3 ) == 0 )
		return true;

	return false;
}

// 바운딩 박스로 인식해욤..
bool IsBoundingBoxNode( const TCHAR* strName )
{
	if( _tcsnicmp( strName, _T("BO_"), 3 ) == 0 )
		return true;

	if( _tcsnicmp( strName, _T("bo_"), 3 ) == 0 )
		return true;

	return false;
}

// 멀티 텍스쳐 된 것인지..
bool IsMultiTextured( StdMat2* pMtl )
{
	if( !pMtl->MapEnabled(ID_DI) )
		return false;

	Texmap* pTex = (BitmapTex*)pMtl->GetSubTexmap( ID_DI );
	if( !pTex )
		return false;

	// self-illum slot 안에는 bitmap이 존재한다. dark map으로 되어야 한다.
	Texmap* pSiTex = NULL;
	if( pMtl->MapEnabled( ID_SI ) )
	{
		pSiTex = (BitmapTex*)pMtl->GetSubTexmap( ID_SI );
		if( pSiTex && pSiTex->ClassID() != Class_ID(BMTEX_CLASS_ID, 0) )
			pSiTex = NULL;
	}

	if( pSiTex && pTex && pTex->ClassID() == Class_ID(BMTEX_CLASS_ID,0) )
		return true;

	return false;
}

// Render To Texture 로 인식한다는...
bool IsShellMaterial( const TCHAR* strName )
{
	if( strName[0] == 'S' && strName[1] == 'h' &&
		strName[2] == 'e' && strName[3] == 'l' &&
		strName[4] == 'l' )
		return true;

	return false;
}

//------------------------------------------------------------------
void ConvertMatrixToD3D( Matrix3* pMat )
{
	Point3 row = pMat->GetRow( 1 );
	pMat->SetRow( 1, pMat->GetRow( 2 ) );
	pMat->SetRow( 2, row );

	Point4 col = pMat->GetColumn( 1 );
	pMat->SetColumn( 1, pMat->GetColumn( 2 ) );
	pMat->SetColumn( 2, col ); 
}

bool Point3Equal( const Point3& rA, const Point3& rB )
{
	if( fabs( rA.x - rB.x ) > 1e-6f )
		return false;
	if( fabs( rA.y - rB.y ) > 1e-6f )
		return false;
	if( fabs( rA.z - rB.z ) > 1e-6f )
		return false;

	return true;
}

bool FloatEqual( float fA, float fB )
{
	if( fabs( fA - fB ) > 1e-6f )
		return false;

	return true;
}

bool QuatEqual( const Quat& rA, const Quat& rB )
{
	if( fabs( rA.x - rB.x ) > 1e-6f )
		return false;
	if( fabs( rA.y - rB.y ) > 1e-6f )
		return false;
	if( fabs( rA.z - rB.z ) > 1e-6f )
		return false;
	if( fabs( rA.w - rB.w ) > 1e-6f )
		return false;

	return true;
}


void Uniform_Matrix( Matrix3& In, Matrix3& Out )
{
	AffineParts parts;
	decomp_affine( In, &parts );

	parts.q.MakeMatrix( Out );
	Out.SetRow( 3, parts.t );
}

void Transpose_Matrix( Matrix3& In, Matrix3& Out )
{
	float transpose[4][3];

	int row;
	for(row = 0; row < 3; row++)
	{
		int column;
		for(column = 0; column < 3; column++)
		{
			transpose[column][row] = In.GetAddr()[row][column];
		}
	}

	int column;
	for(column = 0; column < 3; column++)
	{
		transpose[3][column] = 0;
	}

	Out = Matrix3(transpose);
	Out.NoTrans();
}


//------------------------------------------------------------------
void AddStringInListBox( HWND hDlg, int nDlgItem, const TCHAR* strString )
{
	int nNum = SendDlgItemMessage( hDlg, nDlgItem, LB_GETCOUNT, 0, 0 );
	SendDlgItemMessage( hDlg, nDlgItem, LB_INSERTSTRING, nNum, (LPARAM)strString );
}

//------------------------------------------------------------------
void WCharToChar( const wchar_t* pwstrSrc, TCHAR* pOut )
{
    int len = (int)(wcslen(pwstrSrc) + 1) * 2;

    WideCharToMultiByte( 949, 0, pwstrSrc, -1, pOut, len, NULL, NULL);
}

//------------------------------------------------------------------
void OutputMsg( const TCHAR* pFormat, ... )
{
	va_list args;

	TCHAR buffer[256];
	memset( buffer, 0, sizeof(TCHAR) * 256 );

	va_start( args, pFormat );

	vsprintf( buffer, pFormat, args );
	OutputDebugStr( buffer );

	va_end( args );
}

//==================================================================
//	버텍스 노말을 계산하기 위한 객체
//==================================================================
VNormal::VNormal()
{
	smooth = 0;
	next = 0;
	init = false;
	normal = Point3(0.0f, 0.0f, 0.0f);
}

//------------------------------------------------------------------
VNormal::VNormal( Point3& n, unsigned long s )
{
	next = NULL;
	init = true;
	normal = n;
	smooth = s;
}

//------------------------------------------------------------------
VNormal::~VNormal()
{
	delete next;
}

//------------------------------------------------------------------
// Add a normal to the list if the smoothing group bits overlap, 
// otherwise create a new vertex normal in the list 
void VNormal::AddNormal( Point3& n, unsigned long s )
{
	if( !(s & smooth ) && init )
	{
		if(next)
			next->AddNormal( n, s );
		else
			next = new VNormal( n, s );
	}
	else
	{
		normal += n;
		smooth |= s;
		init = true;
	}
}

//------------------------------------------------------------------
// Retrieves a normal if the smoothing groups overlap or there is 
// only one in the list 
Point3& VNormal::GetNormal( unsigned long s )
{
	if( smooth & s || !next )
		return normal;
	else
		return next->GetNormal(s);
}

//------------------------------------------------------------------
// Normalize each normal in the list 
void VNormal::Normalize()
{
	VNormal* ptr = next;
	VNormal* prev = this;

	while( ptr )
	{
		if( ptr->smooth & smooth )
		{
			normal += ptr->normal;
			prev->next = ptr->next;
			delete ptr;

			ptr = prev->next;
		}
		else
		{
			prev = ptr;
			ptr = ptr->next;
		}
	}

	normal.Normalize();

	if( next )
		next->Normalize();
}
