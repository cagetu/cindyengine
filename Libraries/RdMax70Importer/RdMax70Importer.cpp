#include "RdMax70Importer.h"
#include "Importer/RdMAx70MeshImp.h"

//------------------------------------------------------------------
//	Importer class
//------------------------------------------------------------------
RdMax70Importer::RdMax70Importer()
{
}

//------------------------------------------------------------------
RdMax70Importer::~RdMax70Importer()
{
}

int RdMax70Importer::ExtCount()
{
	return 3;
}

const TCHAR* RdMax70Importer::Ext( int n )
{
	if( n == 0 )
        return _T("mmf");
	else if( n == 1 )
		return _T("mbf");
	else if( n == 2 )
		return _T("maf");

	return _T("");
}

const TCHAR* RdMax70Importer::LongDesc()
{
	return _T("Hi-WIN Max Import Data File For Mythology" ); 
}

const TCHAR* RdMax70Importer::ShortDesc()
{
	return _T("Mythology File" );
}

const TCHAR* RdMax70Importer::AuthorName()
{
	return _T( "Hi-WIN R&D Team" );
}

const TCHAR* RdMax70Importer::CopyrightMessage()
{
	return _T( "Copyright (c)2005, Hi-WIN" );
}

const TCHAR* RdMax70Importer::OtherMessage1()
{
	return _T("");
}

const TCHAR* RdMax70Importer::OtherMessage2()
{
	return _T("");
}

unsigned int RdMax70Importer::Version()
{
	return 100;
}

void RdMax70Importer::ShowAbout( HWND hWnd )
{
}

//------------------------------------------------------------------
int RdMax70Importer::DoImport( const TCHAR* name, ImpInterface* ii, Interface* i, BOOL suppressPrompts )
{
	ip = i;
	impip = ii;

	RdMeshImp importer( this );
	importer.Import( name );

	return 1;
}
