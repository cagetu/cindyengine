#include "RdMax70Importer.h"

//------------------------------------------------------------------
#define RDMAX70IMPOTER_CLASS_ID Class_ID(0x6d080731, 0x40817660)

//==================================================================
// ClassDesc Class
//==================================================================
class RdMax70ImpoterClassDesc : public ClassDesc2
{
public:
	int 			IsPublic()						{	return 1;							}
	void *			Create(BOOL loading = FALSE)	{	return new RdMax70Importer;			}
	const TCHAR *	ClassName()						{	return GetString(IDS_CLASS_NAME);	}
	SClass_ID		SuperClassID()					{	return SCENE_IMPORT_CLASS_ID;		}
	Class_ID		ClassID()						{	return RDMAX70IMPOTER_CLASS_ID;		}
	const TCHAR* 	Category()						{	return GetString(IDS_CATEGORY);		}

	const TCHAR*	InternalName()					{	return _T("RdMax70Impoter");		}	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance()						{	return hInstance;					}	// returns owning module handle
};

// Statics
static RdMax70ImpoterClassDesc RdMax70ImpoterDesc;
ClassDesc2* GetRdMaxImpoterDesc()	{	return &RdMax70ImpoterDesc;		}


HINSTANCE hInstance;
int controlIsInit = FALSE;

//------------------------------------------------------------------
BOOL WINAPI DllMain( HINSTANCE hInstanceDll, DWORD dwReason, LPVOID )
{
	hInstance = hInstanceDll;

	if( !controlIsInit )
	{
		controlIsInit = TRUE;
		InitCustomControls( hInstance );
		InitCommonControls();
	}

	return TRUE;
}

//------------------------------------------------------
// This is the interface to Jaguar:
//------------------------------------------------------
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

__declspec( dllexport ) int LibNumberClasses()
{
	return 1;
}

__declspec( dllexport ) ClassDesc *LibClassDesc(int i)
{
	switch(i)
	{
		case 0: return GetRdMaxImpoterDesc(); break;
		default: return 0; break;
	}
}

// Return version so can detect obsolete DLLs
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

// Let the plug-in register itself for deferred loading
__declspec( dllexport ) ULONG CanAutoDefer()
{
	return 1;
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}
