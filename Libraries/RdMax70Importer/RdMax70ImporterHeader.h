#ifndef __RDMAX70IMPORTERHEADER_H__
#define __RDMAX70IMPORTERHEADER_H__

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

// now, Character Studio entirely included in 3dsmax7
// for other version of 3dsmax, specify include path of character studio
// in 'project > directory' page of 'tool > option' menu.
#if (MAX_RELEASE >= 6900)   // 3dsmax R7 or R7 alpha
    #include <cs/bipexp.h>
    #include <cs/phyexp.h>
#else
    #include <bipexp.h>
    #include <phyexp.h>    // for other version
	//#include "phyexp.h"
	//#include "bipexp.h"
#endif

#include "decomp.h"
#include "stdmat.h"
#include "shape.h"
#include "interpik.h"

#define MAX_LEN			256

#define _USE_IMPNODES

#ifndef ushort
typedef unsigned short	ushort;
#endif

#ifndef ulong
typedef unsigned long	ulong;
#endif

//==================================================================
// Macro Define
//==================================================================
#ifndef SAFEDEL
	#define	SAFEDEL(x)		{	if(x){	delete (x);		(x) = NULL;		}	}
#endif
#ifndef SAFEDELS
	#define SAFEDELS(x)		{	if(x){	delete[] (x);	(x) = NULL;		}	}
#endif
#ifndef SAFEREL
	#define SAFEREL(x)		{	if(x){	(x)->Release();	(x) = NULL;		}	}
#endif

#endif // __RDMAX70IMPORTERHEADER_H__