// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "RdChunk.h"

namespace RdMaxImp
{
	//-------------------------------------------------------------------------
	//	WriteBegin
	//-------------------------------------------------------------------------
	void RdChunk::WriteBegin( FILE* pFile, unsigned long ulTag, unsigned long& ulPtr, unsigned long& ulBegin )
	{
		ulBegin = ftell( pFile );
		// Tag 저장
		fwrite( &ulTag, sizeof(unsigned long), 1, pFile );

		ulPtr = ftell( pFile );

		// Size 저장 장소 확보
		fwrite( &ulPtr, sizeof(unsigned long), 1, pFile );
	}

	//-------------------------------------------------------------------------
	//	WriteEnd
	//-------------------------------------------------------------------------
	void RdChunk::WriteEnd( FILE* pFile, unsigned long ulPtr, unsigned long ulBegin )
	{
		unsigned long ulPos, ulSize;
		ulPos = ftell( pFile );
		fseek( pFile, ulPtr, SEEK_SET );
		ulSize = ulPos - ulBegin;

		// 실제 크기 저장 - begin에서 잡아놓았던 그 자리에 덮혀쓴다.
		fwrite( &ulSize, sizeof(unsigned long), 1, pFile );

		fseek( pFile, ulPos, SEEK_SET );
	}

	//-------------------------------------------------------------------------
	//	Read
	//-------------------------------------------------------------------------
	void RdChunk::Read(	sChunk* pChunk, RdMemStream& rStream )
	{
		pChunk->ulByteRead	= (unsigned long)rStream.Read( &pChunk->ulTag, sizeof(unsigned long), 1 );
		pChunk->ulByteRead += (unsigned long)rStream.Read( &pChunk->ulSize, sizeof(unsigned long), 1 );
	}

	//-------------------------------------------------------------------------
	//	Skip
	//-------------------------------------------------------------------------
	bool RdChunk::Skip( RdMemStream& rStream, unsigned long ulSkipPo )
	{
		return rStream.SkipChunk( ulSkipPo );
	}
}