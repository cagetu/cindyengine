#include "RdMax70MemStream.h"

#include <assert.h>

//------------------------------------------------------------------
RdMemStream::RdMemStream()
{
    m_pBuffer			= NULL;
	m_ulCurPos			= 0;
	m_ulSize			= 0;
	m_bOpen				= false;
}

//------------------------------------------------------------------
RdMemStream::~RdMemStream()
{
	delete[] m_pBuffer;
	m_pBuffer = NULL;
}

//------------------------------------------------------------------
bool RdMemStream::Open( const TCHAR* strFileName, const TCHAR* strMode )
{
    assert( !m_pBuffer );

	FILE* pF;

	pF = fopen( strFileName, strMode );
	if( !pF )
	{
		return false;
	}

	fseek( pF, 0, SEEK_END );
	m_ulSize = ftell( pF );
	fseek( pF, 0, SEEK_SET );

	m_pBuffer = new unsigned char[ m_ulSize ];
	fread( m_pBuffer, m_ulSize, 1, pF );
	fclose( pF );

	m_ulCurPos	= 0;
	m_bOpen		= true;

	return true;
}

//------------------------------------------------------------------
void RdMemStream::Close()
{
    if( !m_pBuffer )
		return;

	delete[] m_pBuffer;
	m_pBuffer = NULL;

	m_ulCurPos	= m_ulSize = 0;
	m_bOpen		= false;
}

//------------------------------------------------------------------
unsigned long RdMemStream::GetSize() const
{
	return m_ulSize;
}

//------------------------------------------------------------------
ulong RdMemStream::Read( void* pBuffer, ulong ulSize, ulong ulCount )
{
	assert( m_bOpen );

	if( m_ulCurPos >= m_ulSize )
	{
		//Except( MoException::ERR_FILE_IO_ERROR, L"메모리를 모두 다 읽었습니다.", L"MoMemStream::Read" );
		return 0;
	} // if( m_ulCurPos >= m_ulSize )

	ulong ulReadSize;
	if( m_ulCurPos + ulSize * ulCount >= m_ulSize )
	{
		ulReadSize = m_ulSize - m_ulCurPos;
	}
	else
	{
		ulReadSize = ulSize * ulCount;
	}

	memcpy( pBuffer, &m_pBuffer[ m_ulCurPos ], ulReadSize );
	m_ulCurPos += ulReadSize;

	return ulReadSize;
}

//------------------------------------------------------------------
bool RdMemStream::SkipChunk( ulong ulSkipSize )
{
	ulong ulRealSkipSize = ulSkipSize - 8L;
	bool res = ( !FSeek( ulRealSkipSize, SEEK_CUR ) ) ? true : false;

	return res;
	
}
//------------------------------------------------------------------
long RdMemStream::FSeek( ulong ulOffset, ushort usOrigin )
{
	assert( m_bOpen );

	switch( usOrigin )
	{
	case SEEK_SET:
		{
			m_ulCurPos = 0;
			m_ulCurPos += ulOffset;
		}
		break;
	case SEEK_END:
		{
			m_ulCurPos = m_ulSize;
			m_ulCurPos = m_ulSize - ulOffset;
		}
		break;
	default:
		{
			m_ulCurPos += ulOffset;
		}
		break;
	} // switch( nOrigin )

	if( ( m_ulCurPos < 0 ) || ( m_ulCurPos > m_ulSize ) )
	{
		assert( 0 );
		return -1;
	}

	return 0;
}
