// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __RDMAX70MEMSTREAM_H__
#define __RDMAX70MEMSTREAM_H__

#include "RdMax70ImporterHeader.h"

class RdMemStream
{
protected:
	unsigned char*		m_pBuffer;	//!< 데이터가 저장되는 버퍼
	unsigned long		m_ulSize;	//!< 데이터의 크기
	unsigned long		m_ulCurPos;	//!< 현재의 스트림포인터 위치

	bool				m_bOpen;
public:
	RdMemStream();
	virtual ~RdMemStream();

	bool		Open( const TCHAR* strFileName, const TCHAR* strMode );
	void		Close( void );

	/** 데이터 크기 받아오기
		@remarks
			데이터의 크기를 구한다.
		@return
			(unsigned long)데이터의 크기
	*/
	ulong		GetSize( void ) const;
	ulong		Read( void* pBuffer, ulong ulSize, ulong ulCount );

	bool		SkipChunk( ulong ulSkipSize );

	long		FSeek( ulong ulOffset, ushort usOrigin );
};

#endif	// __RDMAX70MEMSTREAM_H__