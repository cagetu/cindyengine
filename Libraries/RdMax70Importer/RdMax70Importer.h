#ifndef __RDMAX70IMPORTER_H__
#define __RDMAX70IMPORTER_H__

#include "RdMax70ImporterHeader.h"

extern TCHAR* GetString( int id );
extern HINSTANCE hInstance;

class RdMax70Importer : public SceneImport
{
public:
	static BOOL resetScene;
	
private:
	Interface*		ip;
	ImpInterface*	impip;
	
public:
	RdMax70Importer();
	~RdMax70Importer();

	int				ExtCount();					// Number of extensions supported
	const TCHAR*	Ext(int n);					// Extension #n (i.e. "3DS")
	const TCHAR*	LongDesc();					// Long ASCII description (i.e. "Autodesk 3D Studio File")
	const TCHAR*	ShortDesc();				// Short ASCII description (i.e. "3D Studio")
	const TCHAR*	AuthorName();				// ASCII Author name
	const TCHAR*	CopyrightMessage();			// ASCII Copyright message
	const TCHAR*	OtherMessage1();			// Other message #1
	const TCHAR*	OtherMessage2();			// Other message #2
	unsigned int	Version();					// Version number * 100 (i.e. v3.01 = 301)
	void			ShowAbout(HWND hWnd);		// Show DLL's "About..." box

	/** Get Interfaces
	*/
	Interface*		GetInterface() const		{	return ip;		}
	ImpInterface*	GetImpInterface() const		{	return impip;	}

	/** Import File Entry Point
	*/
	int				DoImport( const TCHAR* name, ImpInterface* ii, Interface *i, BOOL suppressPrompts );
};

#endif	// __RDMAX70IMPORTER_H__

