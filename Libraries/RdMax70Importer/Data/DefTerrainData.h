//////////////////////////////////////////////////////////////////////////////////////////
// DefTerrainData.h
//
//////////////////////////////////////////////////////////////////////////////////////////

#ifndef __DEFTERRAINDATA_H__
#define __DEFTERRAINDATA_H__

#pragma once

// 지형 파일 청크 아이디

#define		AVAT_MAGIC					0x00101010

// common
#define		AVAT_MAGIC_NUM				0x00001001
#define		AVAT_VERSION				0x00001002
#define		AVAT_NAME					0x00001003

// zone property
#define		AVAT_ZONE_NAME				0x00002003
#define		AVAT_ZONE_GRID_CNT_W		0x00002004
#define		AVAT_ZONE_GRID_CNT_H		0x00002005
#define		AVAT_ZONE_ONE_PATCH_GRID	0x00002006
#define		AVAT_ZONE_PATCH_CNT_W		0x00002007
#define		AVAT_ZONE_PATCH_CNT_H		0x00002008
#define		AVAT_ZONE_ONE_UNIT			0x00002009
#define		AVAT_ZONE_SKY_TEXTURE		0x00002010
#define		AVAT_ZONE_SKYHIGH_TEXTURE	0x0000201a
#define		AVAT_ZONE_SKYLOW_TEXTURE	0x0000201b
#define		AVAT_ZONE_SKY_MESH_NAME		0x00002011
#define		AVAT_ZONE_VARIANCE			0x00002012
#define		AVAT_ZONE_MIN_TRI_SIZE		0x00002013
#define		AVAT_ZONE_GLOBAL_AMBIENT	0x00002014
#define		AVAT_ZONE_LIGHT_AMBIENT		0x00002015
#define		AVAT_ZONE_LIGHT_DIFFUSE		0x00002016
#define		AVAT_ZONE_MTRL_AMBIENT		0x00002017
#define		AVAT_ZONE_MTRL_DIFFUSE		0x00002018
#define		AVAT_ZONE_FOG_COLOR			0x00002019
#define		AVAT_ZONE_FOG_RANGE_MIN		0x00002020
#define		AVAT_ZONE_FOG_RANGE_MAX		0x00002021
#define		AVAT_ZONE_FOG_SKY_RANGE_MIN	0x00002022
#define		AVAT_ZONE_FOG_SKY_RANGE_MAX	0x00002023
#define		AVAT_ZONE_CLIP_RANGE_HEIGHT	0x00002024
#define		AVAT_ZONE_CLIP_RANGE_OBJECT	0x00002025
#define		AVAT_ZONE_SKYDOM_SCALE		0x00002026
#define		AVAT_ZONE_WATER_HEIGHT		0x00002027
#define		AVAT_ZONE_HEIGHT_FOG_MIN	0x00002028
#define		AVAT_ZONE_HEIGHT_FOG_MAX	0x00002029
#define		AVAT_ZONE_WATER_LEFTTOP		0x00002030
#define		AVAT_ZONE_WATER_TOP			0x00002031
#define		AVAT_ZONE_WATER_RIGHTTOP	0x00002032
#define		AVAT_ZONE_WATER_LEFT		0x00002033
#define		AVAT_ZONE_WATER_RIGHT		0x00002034
#define		AVAT_ZONE_WATER_LEFTBOTTOM	0x00002035
#define		AVAT_ZONE_WATER_BOTTOM		0x00002036
#define		AVAT_ZONE_WATER_RIGHTBOTTOM	0x00002037
#define		AVAT_ZONE_DETAIL_TEXTURE	0x00002038
#define		AVAT_ZONE_PP_BLUR_FACTOR	0x00002040
#define		AVAT_ZONE_PP_LUM_MIN		0x00002041
#define		AVAT_ZONE_PP_LUM_MAX		0x00002042


// patch variable
#define		AVAT_PATCH_USE_COORD		0x00003001
#define		AVAT_PATCH_UV_CNT_WIDTH		0x00003002
#define		AVAT_PATCH_UV_CNT_HEIGHT	0x00003003
#define		AVAT_PATCH_TEXTURE_NAME0	0x00003004
#define		AVAT_PATCH_TEXTURE_NAME1	0x00003005
#define		AVAT_PATCH_TEXTURE_NAME2	0x00003006
#define		AVAT_PATCH_TEXTURE_NAME3	0x00003007
#define		AVAT_PATCH_TERRAIN_VERTEX	0x00003008
#define		AVAT_PATCH_VERTEX_ONE_START 0x00003009
#define		AVAT_PATCH_VERTEX_POSITON	0x00003010
#define		AVAT_PATCH_VERTEX_NORMAL	0x00003011
#define		AVAT_PATCH_VERTEX_DIFFUSE	0x00003012
#define		AVAT_PATCH_VERTEX_UV0		0x00003013
#define		AVAT_PATCH_VERTEX_UV1		0x00003014
#define		AVAT_PATCH_VERTEX_UV2		0x00003015
#define		AVAT_PATCH_VERTEX_UV3		0x00003016
#define		AVAT_PATCH_VERTEX_ONE_END	0x00003017
#define		AVAT_PATCH_OBJECT			0x00003018
#define		AVAT_PATCH_OBJECT_ONE_START 0x00003019
#define		AVAT_PATCH_OBJECT_NAME		0x00003020
#define		AVAT_PATCH_OBJECT_FILE		0x00003021
#define		AVAT_PATCH_OBJECT_BONE		0x00003022
#define		AVAT_PATCH_OBJECT_ANI		0x00003023
#define		AVAT_PATCH_OBJECT_TYPE		0x00003024
#define		AVAT_PATCH_OBJECT_ID		0x00003025
#define		AVAT_PATCH_OBJECT_POSITION	0x00003026
#define		AVAT_PATCH_OBJECT_ROT		0x00003027
#define		AVAT_PATCH_OBJECT_SCALE		0x00003028
#define		AVAT_PATCH_OBJECT_SOUND_RANGE		0x00003029
#define		AVAT_PATCH_OBJECT_PROPERTY	0x00003030
#define		AVAT_PATCH_OBJECT_SHADE_VALUE		0x00003040
#define		AVAT_PATCH_OBJECT_ONE_END	0x00003031
#define		AVAT_PATCH_GRID				0x00003032
#define		AVAT_PATCH_MH_ONEGRID_START	0x00003033
#define		AVAT_PATCH_MH_ONEMH_START		0x00003034
#define		AVAT_PATCH_MULTI_HEIGHT		0x00003035
#define		AVAT_PATCH_MH_HEIGHT		0x00003036
#define		AVAT_PATCH_MH_MASK			0x00003037
#define		AVAT_PATCH_MH_ONEMH_END		0x00003038
#define		AVAT_PATCH_MH_ONEGRID_END	0x00003039
#define		AVAT_PATCH_RIVER_START		0x00003040				// 강물
	#define		AVAT_PATCH_RIVER_MESH_MASK		0x00003048
	#define		AVAT_PATCH_RIVER_HEIGHT_MASK	0x00003049
#define		AVAT_PATCH_RIVER_END		0x00003041
#define		AVAT_PATCH_RIVER_VERTEX_SIZE	0x00003042
#define		AVAT_PATCH_RIVER_VERTEX_POS	0x00003043
#define		AVAT_PATCH_RIVER_VERTEX_UV1	0x00003044
#define		AVAT_PATCH_RIVER_VERTEX_UV2	0x00003045
#define		AVAT_PATCH_RIVER_INDEX_SIZE		0x00003046
#define		AVAT_PATCH_RIVER_INDEX		0x00003047
#define		AVAT_PATCH_LAVA_START		0x00003050				// 용암
#define		AVAT_PATCH_LAVA_END			0x00003051
#define		AVAT_PATCH_SPLATTING_START		0x00003052
#define		AVAT_PATCH_SPLATTING_ALPHA		0x00003053				// 스플레팅 알파
#define		AVAT_PATCH_SPLATTING_TEXTURE	0x00003054				// 스플레팅 텍스쳐
#define		AVAT_PATCH_SPLATTING_SIZE		0x00003055
#define		AVAT_PATCH_SPLATTING_END		0x00003056
#define		AVAT_PATCH_SPLATTING_SCALE		0x00003057
#define		AVAT_PATCH_SPLATTING_COMALPHA	0x00003058
#define		AVAT_PATCH_SPLATTING_SHADOW		0x00003059


// use tool
#define		AVATOOL_PATCH_ROT0			0x00004001
#define		AVATOOL_PATCH_ROT1			0x00004002
#define		AVATOOL_PATCH_ROT2			0x00004003
#define		AVATOOL_PATCH_ROT3			0x00004004
#define		AVATOOL_PATCH_END			0x00004011


namespace AvaLib
{
	
		

}	// end namespace

#endif
