#include "RdMax70Mesh.h"
#include "..\RdMax70Util.h"

RdMeshNode::RdMeshNode( BYTE btType, TriObject* pTriObject, ImpNode* pImpNode, INode* pNode )
: RdBaseNode( btType, pTriObject, pImpNode, pNode )
{
	m_btMaxLink = 0;
	
	m_bHasVertexColor = false;
	m_bHasMultiUV = false;

	m_FaceList.Init();
	m_FaceList.ZeroCount();

	m_LinkedBoneList.Init();
	m_LinkedBoneList.ZeroCount();
}

RdMeshNode::~RdMeshNode()
{
	Clear();
}

void RdMeshNode::Clear()
{
	unsigned long ulCount = m_FaceList.Count();
	RdFace** ppFace;
	for( unsigned long i = 0; i < ulCount; ++i )
	{
		ppFace = m_FaceList.Addr( i );
		delete (*ppFace);
	}
	m_FaceList.Delete( 0, ulCount );

	m_LinkedBoneList.Delete( 0, m_LinkedBoneList.Count() );
}


void RdMeshNode::AddLinkedBoneID( ushort usID )
{
	ushort usBoneCnt = m_LinkedBoneList.Count();

	ushort i = 0;
	for( ; i < usBoneCnt; ++i )
	{
		if( m_LinkedBoneList[i] == usID )
			break;
	}

	if( i == usBoneCnt )
		m_LinkedBoneList.Insert( usBoneCnt, 1, &usID );
}

void RdMeshNode::SetFaceList( Tab<RdFace>& list, bool bNeg )
{
	unsigned long ulCount = list.Count();
	for( unsigned long i = 0; i < ulCount; ++i )
	{
		RdFace* pFace = new RdFace();

		pFace->Index[0] = list[i].Index[0];

		if( bNeg )
		{
			pFace->Index[1] = list[i].Index[1];
			pFace->Index[2] = list[i].Index[2];
		}
		else
		{
			pFace->Index[1] = list[i].Index[2];
			pFace->Index[2] = list[i].Index[1];
		}
		pFace->MatID = list[i].MatID;

		m_FaceList.Append( 1, &pFace );
	}
}

void RdMeshNode::SetVertexList( Point3* pVertices )
{
	// static 메쉬의 경우.
	// 저장된 버텍스는 로컬 버텍스이다..
	for( unsigned short i = 0; i < m_usVertexCount; ++i )
	{
		m_pTriObj->mesh.verts[i].x = pVertices[i].x;
		m_pTriObj->mesh.verts[i].y = pVertices[i].z;
		m_pTriObj->mesh.verts[i].z = pVertices[i].y;
	}

	m_apVertices = pVertices;
}

void RdMeshNode::SetNormals( Point3* pNormals )
{
	m_apNormals = pNormals;
}

void RdMeshNode::SetTexCoords( Point2* pUv )
{
	m_apUVs = pUv;

	m_pTriObj->mesh.setNumTVerts( GetVertexCount() );

	for( ushort i = 0; i < GetVertexCount(); ++i )
	{
		Point3 vx;
		vx.x = m_apUVs[i].x;
		vx.y = 1.0f - m_apUVs[i].y;
		vx.z = 1.0f;

		m_pTriObj->mesh.setTVert( i, vx );
	}
}

void RdMeshNode::SetVertColor( Point3* pColor )
{
	m_apColors = pColor;

	m_pTriObj->mesh.setNumVertCol( GetVertexCount() );

	//! 버텍스 칼라 설정
	for( ushort i = 0; i < GetVertexCount(); ++i )
	{
		m_pTriObj->mesh.vertCol[i].x = m_apColors[i].x;
		m_pTriObj->mesh.vertCol[i].y = m_apColors[i].z;
		m_pTriObj->mesh.vertCol[i].z = m_apColors[i].y;
	}
}

void RdMeshNode::SetIndices( unsigned short* pIndices )
{
	m_apIndices = pIndices;
}

void RdMeshNode::EnableTwoSide( BYTE btEnable )
{
	m_btTwoSide = btEnable;
}

void RdMeshNode::EnableOpacity( BYTE btEnable )
{
	m_btOpacity = btEnable;
}

void RdMeshNode::EnableAlphaBlend( BYTE btEnable )
{
	m_btAlphaBlend = btEnable;
}

void RdMeshNode::SetFaceMtlIndex( ushort usIndex, ushort usMtlID )
{
	ASSERT_MBOX( usIndex < m_FaceList.Count(), _T( "Wrong Index. [SetFaceMtlIndex]" ) );

	m_FaceList[usIndex]->MatID = usMtlID;
}

ushort RdMeshNode::GetFaceMtlIndex( ushort usIndex )
{
	ASSERT_MBOX( usIndex < m_FaceList.Count(), _T( "Wrong Index. [GetFaceMtlIndex]" ) );

	return m_FaceList[usIndex]->MatID;
}

int CompFace( const void* pEl1, const void* pEl2 )
{
	RdFace* pF1 = (RdFace*)&pEl1;
	RdFace* pF2 = (RdFace*)&pEl2;

	if( pF1->MatID < pF2->MatID )		return -1;
	if( pF1->MatID == pF2->MatID )		return 0;
	return 1;

//	return (pF1->MatID < pF2->MatID );
}

int CompBone( const void* pEl1, const void* pEl2 )
{
	ushort* a = (ushort*)pEl1;
	ushort* b = (ushort*)pEl2;

	if( (*a) < (*b) )		return -1;
	if( (*a) == (*b) )		return 0;
	return 1;

//	return ( (*a) > (*b) );
}

//
//void RdMeshNode::NotifyComplete()
//{
//	m_FaceList.Sort( CompFace );
//	m_LinkedBoneList.Sort( CompBone );
//
//	int nCount = m_VertexList.Count();
//	ushort link = 0;
//	ushort idx = 0;
//	ushort newidx = 0;
//	for( int i = 0; i < nCount; ++i )
//	{
//		link = m_VertexList[i]->Link;
//		if( link > 4 )
//			link = 4;
//
//		for( idx = 0; idx < link; ++idx )
//		{
//			newidx = FindLinkedBoneIndex( m_VertexList[i]->Blend[idx].BoneID );
//			m_VertexList[i]->Blend[idx].BoneID = newidx;
//		}
//	}
//}

ushort RdMeshNode::FindLinkedBoneIndex( ushort usBoneID )
{
	ushort usBoneCnt = m_LinkedBoneList.Count();
	ushort usIndex = 0;

	for( ushort i = 0; i < usBoneCnt; ++i )
	{
		if( m_LinkedBoneList[i] == usBoneID )
			break;

		++usIndex;
	}

	return usIndex;	
}

void RdMeshNode::SetVertexCount( ushort usCount )
{
	m_usVertexCount = usCount;

	m_pTriObj->mesh.setNumVerts( usCount );
}

void RdMeshNode::SetDiffuseMap( TCHAR* strFile )
{
	StdMat* material = (StdMat*)m_pNode->GetMtl();
	if( NULL == material )
	{
		material = NewDefaultStdMat();
		material->SetName( GetNodeName() );
	}
	material->LockAmbDiffTex(FALSE);
	material->SetTwoSided( (BOOL)m_btTwoSide );

	BitmapTex* tex = (BitmapTex*)NewDefaultBitmapTex();
	tex->SetMapName( strFile );

	material->SetSubTexmap( ID_DI, tex );
	material->EnableMap( ID_DI, TRUE );

	m_pNode->SetMtl( material );

	material->DontKeepOldMtl();
}

void RdMeshNode::SetSpecularMap( TCHAR* strFile )
{
	StdMat* material = (StdMat*)m_pNode->GetMtl();
	if( NULL == material )
	{
		material = NewDefaultStdMat();
		material->SetName( GetNodeName() );
	}

	BitmapTex* tex = (BitmapTex*)NewDefaultBitmapTex();
	tex->SetMapName( strFile );

	material->SetSubTexmap( ID_SP, tex );
	material->EnableMap( ID_SP, TRUE );

	m_pNode->SetMtl( material );
}

void RdMeshNode::Build()
{
	// face 리스트 설정
	m_pTriObj->mesh.setNumFaces( m_usFaceCount );
	m_pTriObj->mesh.setNumTVFaces( m_usFaceCount );
	m_pTriObj->mesh.setNumVCFaces( m_usFaceCount );

	Face* face;
	for( ushort i = 0; i < m_usFaceCount; ++i )
	{
		face = &m_pTriObj->mesh.faces[i];

		face->v[0] = m_apIndices[ 3*i ];
		face->v[2] = m_apIndices[ 3*i + 1 ];
		face->v[1] = m_apIndices[ 3*i + 2 ];
	}

	// Normal..
	for( ushort i = 0; i < m_usFaceCount; ++i )
	{
		m_pTriObj->mesh.vcFace[i].t[0] = m_apNormals[ m_apIndices[3*i] ].x;
		m_pTriObj->mesh.vcFace[i].t[2] = m_apNormals[ m_apIndices[3*i + 1] ].y;
		m_pTriObj->mesh.vcFace[i].t[1] = m_apNormals[ m_apIndices[3*i + 2] ].z;
	}

	// texcoord face 리스트
	for( ushort i = 0; i < m_usFaceCount; ++i )
	{
		m_pTriObj->mesh.tvFace[i].t[0] = m_apIndices[ 3*i ];
		m_pTriObj->mesh.tvFace[i].t[2] = m_apIndices[ 3*i + 1 ];
		m_pTriObj->mesh.tvFace[i].t[1] = m_apIndices[ 3*i + 2 ];
	}

	//// 버텍스 칼라 Face
	//if( m_bHasVertexColor )
	//{
	//	for( ushort i = 0; i < m_usFaceCount; ++i )
	//	{
	//		m_pTriObj->mesh.vcFace[i].t[0] = m_apIndices[ 3*i ];
	//		m_pTriObj->mesh.vcFace[i].t[1] = m_apIndices[ 3*i + 1 ];
	//		m_pTriObj->mesh.vcFace[i].t[2] = m_apIndices[ 3*i + 2 ];
	//	}
	//}

	m_pTriObj->mesh.InvalidateTopologyCache();
	m_pTriObj->mesh.InvalidateGeomCache();
	m_pTriObj->mesh.buildNormals();
}

//------------------------------------------------------------------
//	RdNormFaceNode Class 
//------------------------------------------------------------------

RdNormFaceNode::RdNormFaceNode()
{
	m_VertexList.Init();
	m_VertexList.ZeroCount();

	m_FaceList.Init();
	m_FaceList.ZeroCount();
}

RdNormFaceNode::~RdNormFaceNode()
{
	Clear();
}

void RdNormFaceNode::Clear()
{
	unsigned long ulCount = m_VertexList.Count();
	m_VertexList.Delete( 0, ulCount );

	ulCount = m_FaceList.Count();
	RdNormFace** ppFace;
	for( unsigned long i = 0; i < ulCount; ++i )
	{
		ppFace = m_FaceList.Addr( i );
		delete (*ppFace );
	}
	m_FaceList.Delete( 0, ulCount );
}

void RdNormFaceNode::SetVertexList( Tab<Point3>& list )
{
	unsigned long ulCount = list.Count();
	m_VertexList.SetCount( ulCount );
	for( unsigned long i = 0; i < ulCount; ++i )
	{
		m_VertexList[i] = Point3( list[i].x, list[i].z, list[i].y );
	}
}

void RdNormFaceNode::SetFaceList( Tab<RdNormFace>& list, bool bNeg )
{
	unsigned long ulCount = list.Count();
	for( unsigned long i = 0; i < ulCount; ++i )
	{
		RdNormFace* pFace = new RdNormFace();

		pFace->Index[0] = list[i].Index[0];

		if( bNeg )
		{
			pFace->Index[1] = list[i].Index[1];
			pFace->Index[2] = list[i].Index[2];
		}
		else
		{
			pFace->Index[1] = list[i].Index[2];
			pFace->Index[2] = list[i].Index[1];
		}

		pFace->Normal = CalcFaceNormal( pFace->Index[0], pFace->Index[1], pFace->Index[2] );

		m_FaceList.Append( 1, &pFace );
	}
}

Point3 RdNormFaceNode::CalcFaceNormal( unsigned long ulA, unsigned long ulB, unsigned long ulC )
{
	Point3 v10 = m_VertexList[ulB] - m_VertexList[ulA];
	Point3 v20 = m_VertexList[ulC] - m_VertexList[ulA];

	Point3 normal = v10 ^ v20;
	return normal.Normalize();
}
