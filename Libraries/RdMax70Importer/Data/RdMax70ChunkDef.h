#ifndef __RDMAX70CHUNKDEF_H__
#define __RDMAX70CHUNKDEF_H__

#pragma once

//==================================================================
// Mesh Data Chunk
//==================================================================
#define RD_MESHFILE_MAGICNUM						0x19760601
#define RD_MESHFILE_VERSION							0x20051225


#define RD_MESH_MAGIC								0xabcd0123				// magic num 저장					
#define RD_MESH_VERSION								0xabcd4567				// file version 저장

#define RD_MESH_NODELIST							0xabcd8901				// 저장하는 거 음따
	#define RD_MESH_NODE							0x50531000				// node type 저장( normal, dummy )
		#define RD_MESH_NODE_ID						0x50531010				// node id 저장
		#define RD_MESH_NODE_NAME					0x50531011				// node name 길이 및 문자열 저장
		#define RD_MESH_NODE_PARENTID				0x50531012				// parent id 저장
		#define RD_MESH_NODE_PARENTNAME				0x50531013				// parent node name 길이 및 문자열 저장

		#define RD_MESH_NODE_LOCALTM				0x50532020				// local pos, rot, scl 저장
		#define RD_MESH_NODE_WORLDTM				0x50532021				// world matrix 3 by 4 저장

		#define RD_MESH_NODE_LINKEDBONE				0x50533111				// 이 노드에 걸려있는 총 본의 개수와 노드 인덱스들 저장
		#define RD_MESH_NODE_MAXLINK				0x50533112				// 버텍스 하나당 최대 링크 수
		#define RD_MESH_NODE_VERTEXCOLOR			0x50533113				// 버텍스 칼라 사용 여부

		#define RD_MESH_NODE_VERTEX					0x50534100				// vertex 총 개수 저장
			#define RD_MESH_NODE_VERTEX_POS			0x50534121				// 개수만큼 position vector 저장
			#define RD_MESH_NODE_VERTEX_NORM		0x50534122				// 개수만큼 normal vector 저장
			#define RD_MESH_NODE_VERTEX_UV			0x50534123				// 개수만큼 uv 저장
			#define RD_MESH_NODE_VERTEX_BLEND		0x50534124				// 개수만큼 link 개수, bone id, weight 저장, 여러번 호출 예정
			#define RD_MESH_NODE_VERTEX_COLOR		0x50534125				// 개수만큼 color vector 저장
			#define RD_MESH_NODE_VERTEX_UV2			0x50534126				// 개수만큼 uv2 저장

			// color나 기타 정보 저장해도 됨.

		#define RD_MESH_NODE_FACELIST				0x50535000				// 저장하는 그 음따
			#define RD_MESH_NODE_FACE				0x50535010				// 같은 머테리얼 쓰는 face 개수 저장
				#define RD_MESH_NODE_FACE_INDICES	0x50535011				// index list 저장, face cnt * 3만큼 저장
				#define RD_MESH_NODE_FACE_DIMAP		0x50535012				// diffuse map 이름 길이와 문자열 저장
				#define RD_MESH_NODE_FACE_SPMAP		0x50535013				// specular map 이름 길이와 문자열 저장
				#define RD_MESH_NODE_FACE_TWOSIDE	0x50535014				// two side 여부 저장
				#define RD_MESH_NODE_FACE_OPACITY	0x50535015				// opacity 여부 저장
				#define RD_MESH_NODE_FACE_BLENDING	0x50535016				// alpha blending 여부 저장

		#define RD_MESH_NODE_AABB					0x50536000				// AABB min, max vector 저장
		#define RD_MESH_NODE_OBB					0x50537000				// center, axis(3), extend(3)

	#define RD_MESH_NFNODE							0x50541000				// type 저장( 1 : height, 2 : indoor )
		#define RD_MESH_NFNODE_VERTEX				0x50541010				// vertex 총개수와 개수만큼의 pos 저장
		#define RD_MESH_NFNODE_FACE					0x50541020				// face 총개수 저장
			#define RD_MESH_NFNODE_FACE_INDEX		0x50541021				// face index 저장
			#define RD_MESH_NFNODE_FACE_NORMAL		0x50541022				// face normal 저장

#define RD_MESH_TRACE								0x09876543				// vector 2개

//==================================================================
// Skeleton Data Chunk
//==================================================================
#define RD_SKELFILE_MAGICNUM						0x19790901
#define RD_SKELFILE_VERSION							0x20051225

#define RD_SKEL_MAGIC								0x0987edcb			
#define RD_SKEL_VERSION								0x0987dcba

#define RD_SKEL_BONELIST							0x0987abcd				// 저장하는 그 음따
	#define RD_SKEL_BONE							0x58361100				// bone type 저장( bone, dummy )
		#define RD_SKEL_BONE_ID						0x58361101				// node id 저장
		#define RD_SKEL_BONE_NAME					0x58361102				// node name 길이 및 문자열 저장
		#define RD_SKEL_BONE_PARENTID				0x58361103				// parent id 저장
		#define RD_SKEL_BONE_PARENTNAME				0x58361104				// parent node name 길이 및 문자열 저장

		#define RD_SKEL_BONE_LOCALTM				0x58361201				// local pos, rot, scl 저장
		#define RD_SKEL_BONE_WORLDTM				0x58361202				// world matrix 3 by 4 저장

		#define RD_SKEL_BONE_AABB					0x58361301				// min, max
		#define RD_SKEL_BONE_OBB					0x58361302				// center, axis(3), extend(3)

#define RD_SKEL_BBOXLIST							0x0987fcad				// 저장하는 그 음따
	#define RD_SKEL_BBOX							0x87871500				// 저장하는 그 음따
		#define RD_SKEL_BBOX_PARENTID				0x87871501				// parent id
		#define RD_SKEL_BBOX_PARENTNAME				0x87871502				// parent name
		#define RD_SKEL_BBOX_OBB					0x87871503				// center, axis(3), extend(3)

#define RD_SKEL_BONE_DEFINE_LIST					0x0975acad				// 본을 지정하는 성질
	#define RD_SKEL_DEFINE							0x20060720				// 설정 정보

//==================================================================
// Animation Data Chunk
//==================================================================
#define RD_ANIMFILE_MAGICNUM						0x19741031
#define RD_ANIMFILE_VERSION							0x20051225

#define RD_ANIM_MAGIC								0x09090909
#define RD_ANIM_VERSION								0x38383838

#define RD_ANIM_TOTALFRAME							0x10381038				// 총 Frame
#define RD_ANIM_FRAMERATE							0x12345678				// Frame Rate

#define RD_ANIM_NODELIST							0x89498949				// 저장하는 그 음따
	#define RD_ANIM_NODE							0x70003000				// 저장하는 그 음따
		#define RD_ANIM_NODE_ID						0x70003010				// node id 저장
		#define RD_ANIM_NODE_NAME					0x70003020				// node name 길이 및 문자열 저장

		#define RD_ANIM_NODE_POSTRACK				0x70003030				// pos track 개수 저장, 개수만큼 poskey 저장
		#define RD_ANIM_NODE_ROTTRACK				0x70003040				// rot track 개수 저장, 개수만큼 rotkey 저장
		#define RD_ANIM_NODE_SCLTRACK				0x70003050				// scl track 개수 저장, 개수만큼 sclkey 저장

#endif // __RDMAX70CHUNKDEF_H__