#ifndef __RDMAX70MATERIAL_H__
#define __RDMAX70MATERIAL_H__

#pragma once

#include "..\RdMax70ImporterHeader.h"

class RdMaterial
{
protected:
	ushort			m_usID;		

	TCHAR			m_strMtlName[MAX_LEN];

	TCHAR			m_strDiffuse[MAX_LEN];
	TCHAR			m_strSpecular[MAX_LEN];
	TCHAR			m_strLightMap[MAX_LEN];

	bool			m_bDiffuseMap;
	bool			m_bSpecularMap;
	bool			m_bLightMap;

	BYTE			m_btTwoSide;
	BYTE			m_btAlphaBlending;
	BYTE			m_btOpacity;

public:
	RdMaterial( const TCHAR* strName );
	~RdMaterial();

	void			SetMaterialID( ushort usID )			{		m_usID = usID;					}
	void			SetTwoSide( BYTE btTwoSide )			{		m_btTwoSide = btTwoSide;		}
	void			SetAlphablending( BYTE btBlending )		{		m_btAlphaBlending = btBlending;	}
	void			SetOpacity( BYTE btOpacity )			{		m_btOpacity = btOpacity;		}

	void			SetDiffuseMap( const TCHAR* strName );
	void			SetSpecularMap( const TCHAR* strName );
	void			SetLightMap( const TCHAR* strName );

	const TCHAR*	GetMaterialName() const					{		return m_strMtlName;			}
	ushort			GetMaterialID() const					{		return m_usID;					}
	bool			HaveDiffuseMap() const					{		return m_bDiffuseMap;			}
	const TCHAR*	GetDiffuseMap() const					{		return m_strDiffuse;			}
	bool			HaveSpecularMap() const					{		return m_bSpecularMap;			}
	const TCHAR*	GetSpecularMap() const					{		return m_strSpecular;			}
	bool			HaveLightMap() const					{		return m_bLightMap;				}
	const TCHAR*	GetLightMap() const						{		return m_strLightMap;			}

	BYTE			GetTwoSide() const						{		return m_btTwoSide;				}
	BYTE			GetAlphaBlending() const				{		return m_btAlphaBlending;		}
	BYTE			GetOpacity() const						{		return m_btOpacity;				}
};


#endif // __RDMAX70MATERIAL_H__