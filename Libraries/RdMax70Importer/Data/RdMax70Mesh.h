#ifndef __RDMAX70MESH_H__
#define __RDMAX70MESH_H__

#pragma once

#include "RdMax70Node.h"

struct RdUV
{
	float U;
	float V;

	RdUV() : U( 0.0f ), V( 0.0f )	{}
};

struct RdVertexBlend
{
	ushort	BoneID;
	float	Weight;

	RdVertexBlend() : BoneID( 0xffff ), Weight( 0.0f )	{}
};

struct RdVertex
{
	Point3			Pos;
	Point3			Normal;
	Point3			Color;
	
	RdUV			UV;				//!< 기본 uv
	RdUV			UV2;			//!< Render To Texture 채널 정보

	BYTE			Link;
	RdVertexBlend	Blend[4];

	int				OrgId;
	unsigned long	SmGroup;

	RdVertex() : Link( 0 )
	{
		Pos.Set( 0.0f, 0.0f, 0.0f );
		Normal.Set( 0.0f, 0.0f, 0.0f );	
		Color.Set( 1.0f, 1.0f, 1.0f );
	}
};

struct RdFace
{	
	ushort		MatID;
	ushort		Index[3];

	RdFace() : MatID( 0 )
	{
		Index[0] = Index[1] = Index[2] = 0;
	}
};

class RdMeshNode : public RdBaseNode
{
protected:
	//------------------------------------------------------------------
	BYTE			m_btMaxLink;
	
	bool			m_bHasVertexColor;
	bool			m_bHasMultiUV;

	Tab<RdFace*>	m_FaceList;
	Tab<ushort>		m_LinkedBoneList;

	Point3*			m_apVertices;
	Point3*			m_apNormals;
	Point2*			m_apUVs;
	Point3*			m_apColors;

	unsigned short	m_usVertexCount;
	unsigned short	m_usFaceCount;
	unsigned short	m_usLinkedBoneCount;

	unsigned short*	m_apIndices;

	TCHAR			m_strDiffuseMap[256];
	TCHAR			m_strSpecularMap[256];

	BYTE			m_btTwoSide;
	BYTE			m_btOpacity;
	BYTE			m_btAlphaBlend;

	void			Clear();

	void			AddLinkedBoneID( ushort usID );
	ushort			FindLinkedBoneIndex( ushort usBoneID );

public:
	RdMeshNode( BYTE btType, TriObject* pTriObject, ImpNode* pImpNode, INode* pNode );
	virtual ~RdMeshNode();
	
	void			SetVertexList( Point3* pVertices );
	void			SetNormals( Point3* pNormals );
	void			SetTexCoords( Point2* pUv );
	void			SetVertColor( Point3* pColor );

	void			SetIndices( unsigned short* pIndices );

	void			SetDiffuseMap( TCHAR* strFile );
	void			SetSpecularMap( TCHAR* strFile );

	void			EnableTwoSide( BYTE btEnable );
	void			EnableOpacity( BYTE btEnable );
	void			EnableAlphaBlend( BYTE btEnable );

	void			SetFaceList( Tab<RdFace>& list, bool bNeg );

	void			SetFaceMtlIndex( ushort usIndex, ushort usMtlID );
	ushort			GetFaceMtlIndex( ushort usIndex );

//	void			NotifyComplete();
	
	void			SetEnableVertexColor( bool bEnable )	{		m_bHasVertexColor = bEnable;				}
	bool			GetEnableVertexColor() const			{		return m_bHasVertexColor;					}

	void			SetMultiUV( bool bValue )				{		m_bHasMultiUV = bValue;						}
	bool			HasMultiUV() const						{		return m_bHasMultiUV;						}

	void			SetMaxLink( BYTE btLink )				{		m_btMaxLink = btLink;						}
	BYTE			GetMaxLink() const						{		return m_btMaxLink;							}

	void			SetLinkedBoneCount( ushort usCount )	{		m_usLinkedBoneCount = usCount;				}
	ushort			GetLinkedBoneCount() const				{		return (ushort)m_LinkedBoneList.Count();	}

	void			SetVertexCount( ushort usCount );
	ushort			GetVertexCount() const					{		return m_usVertexCount;						}

	void			SetFaceCount( ushort usCount )			{		m_usFaceCount = usCount;					}
	ushort			GetFaceCount() const					{		return (ushort)m_FaceList.Count();			}

	const Tab<ushort>&		GetLinkedBoneList() const		{		return m_LinkedBoneList;					}
	const Tab<RdFace*>&		GetFaceList() const				{		return m_FaceList;							}

	void			Build();
};

struct RdNormFace
{
	ushort		Index[3];
	Point3		Normal;

	RdNormFace()
	{
		Index[0] = Index[1] = Index[2] = 0;
		Normal.Set( 0.0f, 0.0f, 0.0f );
	}
};

class RdNormFaceNode
{
protected:
	Tab<Point3>			m_VertexList;
	Tab<RdNormFace*>	m_FaceList;

	void			Clear();

	Point3			CalcFaceNormal( unsigned long ulA, unsigned long ulB, unsigned long ulC );

public:
	RdNormFaceNode();
	~RdNormFaceNode();

	void			SetVertexList( Tab<Point3>& list );
	void			SetFaceList( Tab<RdNormFace>& list, bool bNeg );

	ushort			GetVertexCount() const					{		return (ushort)m_VertexList.Count();		}
	ushort			GetFaceCount() const					{		return (ushort)m_FaceList.Count();			}

	const Tab<Point3>&	GetVertexList() const				{		return m_VertexList;						}
	const Tab<RdNormFace*>	GetFaceList() const				{		return m_FaceList;							}
};

#endif // __RDMAX70MESH_H__