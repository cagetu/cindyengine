#ifndef __RDMAX70NODE_H__
#define __RDMAX70NODE_H__

#pragma once

#include "..\RdMax70ImporterHeader.h"

class RdBaseNode
{
public:
	enum DEFINE_TYPE
	{
		DEFINE_NONE = 0,

		DEFINE_HEAD,			//!< �Ӹ�
		DEFINE_SPINE,			//!< �㸮
		DEFINE_RHAND,			//!< ������
		DEFINE_LHAND,			//!< �޼�
		DEFINE_RFOOT,			//!< ������
		DEFINE_LFOOT,			//!< �޹�
		DEFINE_RUARM,			//!< �������
		DEFINE_LUARM,			//!< �޾��
		DEFINE_RLARM,			//!< ���� �Ȳ�ġ
		DEFINE_LLARM,			//!< �� �Ȳ�ġ
		DEFINE_RTHIGH,			//!< ���� �����(�������ϵ�)
		DEFINE_LTHIGH,			//!< �� �����(�������ϵ�)
		DEFINE_RCALF,			//!< ���� ���Ƹ�(�����ϵ�)
		DEFINE_LCALF,			//!< �� ���Ƹ�(�����ϵ�)

		DEFINE_MAX
	};

protected:
	TriObject*		m_pTriObj;
	INode*			m_pNode;

#ifdef _USE_IMPNODES
	ImpNode*		m_pImpNode;
#endif

	BYTE			m_btType;					// 0 : mesh 1 : bone 2 : dummy
	unsigned short	m_usID;
	TCHAR			m_strName[MAX_LEN];

	unsigned short	m_usParentID;
	TCHAR			m_strParentName[MAX_LEN];

	Point3			m_ptLocalPos;
	Quat			m_qLocalRot;
	Point3			m_ptLocalScale;

	Matrix3			m_matWorldTM;

	Point3			m_ptBBoxMin;
	Point3			m_ptBBoxMax;

	Point3			m_ptCenter;
	Point3			m_arAxis[3];
	Point3			m_arExtent;
public:
	RdBaseNode( BYTE btType, TriObject* pTriObject, ImpNode* pImpNode, INode* pNode );
	virtual ~RdBaseNode();

	TriObject*		GetTriObject() const							{		return m_pTriObj;			}
	INode*			GetINode() const								{		return m_pNode;				}

	void			SetNodeID( unsigned short usID )				{		m_usID = usID;				}
	void			SetNodeName( const TCHAR* strName );
	void			SetParentNode( unsigned short usID, const TCHAR* strName, RdBaseNode* pParent );

	void			SetLocalPosition( const Point3& ptPos )			{		m_ptLocalPos = ptPos;		}
	void			SetLocalRotation( const Quat& qRot )			{		m_qLocalRot = qRot;			}
	void			SetLocalScale( const Point3& ptScl )			{		m_ptLocalScale = ptScl;		}			
	void			SetWorldTransform( const Matrix3& matTM );

	BYTE			GetNodeType() const								{		return m_btType;			}

	unsigned short	GetNodeID() const								{		return m_usID;				}
	const TCHAR*	GetNodeName() const								{		return m_strName;			}
	unsigned short	GetParentNodeID() const							{		return m_usParentID;		}
	const TCHAR*	GetParentNodeName() const						{		return m_strParentName;		}

	const Point3&	GetLocalPosition() const						{		return m_ptLocalPos;		}
	const Quat&		GetLocalRotation() const						{		return m_qLocalRot;			}
	const Point3&	GetLocalScale() const							{		return m_ptLocalScale;		}

	const Matrix3&	GetWorldTransform() const						{		return m_matWorldTM;		}

	const Point3&	GetBBoxMin() const								{		return m_ptBBoxMin;			}
	const Point3&	GetBBoxMax() const								{		return m_ptBBoxMax;			}

	const Point3&	GetOBBCenter() const							{		return m_ptCenter;			}
	const Point3&	GetOBBAxisX() const								{		return m_arAxis[0];			}
	const Point3&	GetOBBAxisY() const								{		return m_arAxis[1];			}
	const Point3&	GetOBBAxisZ() const								{		return m_arAxis[2];			}
	const Point3&	GetOBBExtent() const							{		return m_arExtent;			}

	virtual void	Build() {;}
};	

void Rotate( float a[3][3], float s, float tau, int i, int j, int k, int l );
bool Jacobi( float a[3][3], float v[3][3], float d[3] );


#endif // __RDMAX70NODE_H__