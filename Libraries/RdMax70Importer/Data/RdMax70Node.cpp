#include "RdMax70Node.h"

RdBaseNode::RdBaseNode( BYTE btType, TriObject* pTriObj, ImpNode* pImpNode, INode* pNode )
: m_btType( btType )
#ifdef _USE_IMPNODES
, m_pImpNode( pImpNode )
#endif
, m_pNode( pNode )
, m_pTriObj( pTriObj )
{
	m_usID = 0xffff;

	memset( m_strName, 0, sizeof(TCHAR)*MAX_LEN );

	m_usParentID	= 0xffff;
	memset( m_strParentName, 0, sizeof(TCHAR)*MAX_LEN );

	m_ptLocalPos.Set( 0.0f, 0.0f, 0.0f );
	m_qLocalRot.Set( 0.0f, 0.0f, 0.0f, 1.0f );
	m_ptLocalScale.Set( 1.0f, 1.0f, 1.0f );

	m_matWorldTM.IdentityMatrix();

	m_ptBBoxMin.Set( FLT_MAX, FLT_MAX, FLT_MAX );
	m_ptBBoxMax.Set( -FLT_MAX, -FLT_MAX, -FLT_MAX );

	m_ptCenter.Set( 0.0f, 0.0f, 0.0f );
	m_arAxis[0].Set( 0.0f, 0.0f, 0.0f );
	m_arAxis[1].Set( 0.0f, 0.0f, 0.0f );
	m_arAxis[2].Set( 0.0f, 0.0f, 0.0f );
	m_arExtent.Set( 0.0f, 0.0f, 0.0f );
}

RdBaseNode::~RdBaseNode()
{

}

void RdBaseNode::SetNodeName( const TCHAR* strName )
{
	_tcscpy( m_strName, strName );

	m_pNode->SetName( m_strName );
}


void RdBaseNode::SetParentNode( ushort usID, const TCHAR* strName, RdBaseNode* pParent )
{
	m_usParentID = usID;
	_tcscpy( m_strParentName, strName );

	pParent->GetINode()->AttachChild( m_pNode, 1 );
}

void RdBaseNode::SetWorldTransform( const Matrix3& matTM )
{
	m_matWorldTM = matTM;

#ifdef _USE_IMPNODES
	m_pImpNode->SetTransform( 0, matTM );
#else
	m_pNode->SetNodeTM( 0, matTM );
#endif
}

// OBB 구하기 위해 필요한 것들...
void Rotate( float a[3][3], float s, float tau, int i, int j, int k, int l )
{
	float h, g;
	g = a[i][j];
	h = a[k][l];
	a[i][j] = g - s * ( h + g *tau );
	a[k][l] = h + s * ( g - h *tau );
}

bool Jacobi( float a[3][3], float v[3][3], float d[3] )
{
	int n = 3;
	int i, j, iq, ip;
	float tresh, theta, tau, t, sm, s, h, g, c, b[3], z[3];

	for( ip = 0; ip < n; ip++ ){
		for( iq = 0; iq < n; iq++ ) v[ip][iq] = 0.0f;
		v[ip][ip] = 1.0f;
	}
	for( ip = 0; ip < n; ip++ ){
		b[ip] = d[ip] = a[ip][ip];
		z[ip] = 0.0f;
	}
	for( i = 0; i < 50; i++ ){
		sm = 0.0f;
		for( ip = 0; ip < n - 1; ip++ ){
			for( iq = ip + 1; iq < n; iq++ ) sm += (float)fabs(a[ip][iq]);
		}

		if( sm == 0.0f ) return true;
		if( i < 3 ) tresh = 0.2f * sm / ( n * n );
		else tresh = 0.0f;
		for( ip = 0; ip < n - 1; ip++ ){
			for( iq = ip + 1; iq < n; iq++ ){
				g = 100.0f * (float)fabs( a[ip][iq] );
				if( i > 3 && ( fabs( d[ip] ) + g ) == fabs( d[ip] )
					&& ( fabs( d[iq] ) + g ) == fabs( d[iq] ) ) a[ip][iq] = 0.0f;
				else if( fabs( a[ip][iq] ) > tresh ){
					h = d[iq] - d[ip];
					if( ( fabs( h ) + g ) == fabs( h ) ) t = a[ip][iq] / h;
					else{
						theta = 0.5f * h / a[ip][iq];
						t = 1.0f / ( (float)fabs( theta ) + (float)sqrt( 1.0f + theta * theta ) );
						if( theta < 0.0f ) t = -t;
					}
					c = 1.0f / (float)sqrt( 1 + t * t );
					s = t * c;
					tau = s / ( 1.0f + c );
					h = t * a[ip][iq];
					z[ip] -= h;
					z[iq] += h;
					d[ip] -= h;
					d[iq] += h;
					a[ip][iq] = 0.0f;

					for( j = 0; j < ip; j++ ) Rotate( a, s, tau, j, ip, j, iq );
					for( j = ip + 1; j < iq; j++ ) Rotate( a, s, tau, ip, j, j, iq );
					for( j = iq + 1; j < n; j++ ) Rotate( a, s, tau, ip, j, iq, j );
					for( j = 0; j < n; j++ ) Rotate( v, s, tau, j, ip, j, iq );
				}
			}
		}
		for( ip = 0; ip < n; ip++ ){
			b[ip] += z[ip];
			d[ip] = b[ip];
			z[ip] = 0.0f;
		}
	}

	return false;
}