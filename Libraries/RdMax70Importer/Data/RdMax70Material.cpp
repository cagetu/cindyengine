#include "RdMax70Material.h"

RdMaterial::RdMaterial( const TCHAR* strName )
{
	m_usID				= 0;

	memset( m_strMtlName, 0, sizeof(TCHAR )*MAX_LEN );
	_tcscpy( m_strMtlName, strName );

	memset( m_strDiffuse, 0, sizeof(TCHAR)*MAX_LEN );
	memset( m_strSpecular, 0, sizeof(TCHAR)*MAX_LEN );
	memset( m_strLightMap, 0, sizeof(TCHAR)*MAX_LEN );

	m_btTwoSide			= 0;
	m_btAlphaBlending	= 0;
	m_btOpacity			= 0;

	m_bDiffuseMap		= false;
	m_bSpecularMap		= false;
	m_bLightMap			= false;
}

RdMaterial::~RdMaterial()
{
	
}

void RdMaterial::SetDiffuseMap( const TCHAR* strName )
{
	_tcscpy( m_strDiffuse, strName );
	m_bDiffuseMap = true;
}

void RdMaterial::SetSpecularMap( const TCHAR* strName )
{
	_tcscpy( m_strSpecular, strName );
	m_bSpecularMap = true;
}

void RdMaterial::SetLightMap( const TCHAR* strName )
{
	_tcscpy( m_strLightMap, strName );
	m_bLightMap = true;
}
