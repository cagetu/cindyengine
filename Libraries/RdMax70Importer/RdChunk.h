// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __RDCHUNK_H__
#define __RDCHUNK_H__

#include "RdMax70MemStream.h"

namespace RdMaxImp
{
	struct sChunk
	{
		unsigned long	ulTag;			// Chunk Tag.
		unsigned long	ulSize;			// Chunk Size.
		unsigned long	ulByteRead;		//
	};

	//!< 청크 헤더 크기
	const unsigned long ChunkHeaderSize = sizeof(unsigned long) + sizeof(unsigned long);

	class RdChunk
	{
	public:
		/** 청크 쓰기 시작 */
		static void	WriteBegin( FILE* pFile, unsigned long ulTag, unsigned long& ulPtr, unsigned long& ulBegin );

		/** 청크 쓰기 끝 */
		static void	WriteEnd( FILE* pFile, unsigned long ulPtr, unsigned long ulBegin );

		/** 청크 읽기 */
		static void	Read( sChunk *pChunk, RdMemStream& rStream );

		/** 청크 건너띄기 */
		static bool	Skip( RdMemStream& rStream, unsigned long ulSkipPo );
	};
}

#endif	// __CNRESOURCECHUNK_H__