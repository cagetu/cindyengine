#include "RdMax70MeshImp.h"

#include "../RdChunk.h"
#include "../RdMax70Importer.h"
#include "../RdMax70Util.h"

#include "../Data/RdMax70ChunkDef.h"

using namespace RdMaxImp;

#define INVALID_INDEX	0xffffffff

//------------------------------------------------------------------
RdMeshImp::RdMeshImp( RdMax70Importer* pParent )
: RdBaseImp( pParent )
{
}

//------------------------------------------------------------------
RdMeshImp::~RdMeshImp()
{
	Clear();
}

//------------------------------------------------------------------
void RdMeshImp::Clear()
{
	unsigned long ulCount = m_NodeList.Count();
	RdMeshNode** ppNode;
	for( unsigned long i = 0; i < ulCount; ++i )
	{
		ppNode = m_NodeList.Addr( i );
		delete (*ppNode);
	}
	m_NodeList.Delete( 0, ulCount );
}

//------------------------------------------------------------------
void RdMeshImp::BuildNodes()
{
	unsigned long ulCount = m_NodeList.Count();
	for( unsigned long i = 0; i < ulCount; ++i )
	{
		m_NodeList[i]->Build();
	}
}

//------------------------------------------------------------------
//	메쉬 파일 불러오기
//------------------------------------------------------------------
bool RdMeshImp::Import( const TCHAR* strFileName )
{
	Clear();

	RdMemStream	stream;
	if( stream.Open( strFileName, "rb" ) )
	{
		if( !ReadMeshData( stream ) )
		{
			//!
			return false;
		}

		BuildNodes();
	}
	stream.Close();

	return true;
}

//------------------------------------------------------------------
bool RdMeshImp::ReadMeshData( RdMemStream& rStream )
{
	sChunk chunk;

	unsigned long leftsize = rStream.GetSize();
	while( leftsize )
	{
		RdChunk::Read( &chunk, rStream );

		switch( chunk.ulTag )
		{
		case RD_MESH_MAGIC:
			{
				if( !ReadMagic( rStream ) )
					return false;
			}
			break;

		case RD_MESH_VERSION:
			{
				if( !ReadVersion( rStream ) )
					return false;
			}
			break;

		case RD_MESH_NODELIST:
			ReadMeshList( rStream, chunk.ulSize );
			break;

		case RD_MESH_TRACE:
			ReadTrace( rStream );
			break;

		default:
			RdChunk::Skip( rStream, chunk.ulSize - RdMaxImp::ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}

	return true;
}

//------------------------------------------------------------------
bool RdMeshImp::ReadMagic( RdMemStream& rStream )
{
	unsigned long value = 0;
	rStream.Read( &value, sizeof(unsigned long), 1 );
	if( RD_MESHFILE_MAGICNUM != value )
		return false;

	return true;
}

//------------------------------------------------------------------
bool RdMeshImp::ReadVersion( RdMemStream& rStream )
{
	unsigned long value = 0;
	rStream.Read( &value, sizeof(unsigned long), 1 );
	if( RD_MESHFILE_VERSION != value )
		return false;

	return true;
}

//------------------------------------------------------------------
void RdMeshImp::ReadMeshList( RdMemStream& rStream, unsigned long ulSize )
{
	unsigned long leftsize = ulSize - RdMaxImp::ChunkHeaderSize;

	RdMaxImp::sChunk chunk;

	while( leftsize )
	{
		RdMaxImp::RdChunk::Read( &chunk, rStream );
		switch( chunk.ulTag )
		{
		case RD_MESH_NODE:
			ReadMeshNode( rStream, chunk.ulSize );
			break;

		case RD_MESH_NFNODE:
			ReadNFMeshNode( rStream, chunk.ulSize );
			break;

		default:
			RdMaxImp::RdChunk::Skip( rStream, chunk.ulSize - RdMaxImp::ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}
}

//------------------------------------------------------------------
void RdMeshImp::ReadTrace( RdMemStream& rStream )
{
	float top[3];
	float bottom[3];

	rStream.Read( top, sizeof(float), 3 );
	rStream.Read( bottom, sizeof(float), 3 );
}

//------------------------------------------------------------------
void RdMeshImp::ReadMeshNode( RdMemStream& rStream, unsigned long ulSize )
{
	BYTE type = 0;
	rStream.Read( &type, sizeof(BYTE), 1 );

	unsigned long leftsize = ulSize - sizeof(BYTE);
	leftsize -= RdMaxImp::ChunkHeaderSize;

	//@<
	TriObject* tri = CreateNewTriObject();

	ImpNode* node = m_pParent->GetImpInterface()->CreateNode();
	node->Reference(tri);

	m_pParent->GetImpInterface()->AddNodeToScene(node);
	INode* realNode = node->GetINode();

//	INode* realNode = m_pParent->GetInterface()->CreateObjectNode(tri);

	RdMeshNode* pMeshNode = new RdMeshNode( type, tri, node, realNode );

	m_NodeList.Append( 1, &pMeshNode, 50 );
	//@>

	unsigned short nodeid = 0;
	unsigned short parentnodeid = 0;

	RdMaxImp::sChunk chunk;
	while( leftsize )
	{
		RdMaxImp::RdChunk::Read( &chunk, rStream );

		switch( chunk.ulTag )
		{
		case RD_MESH_NODE_ID:
			{
				rStream.Read( &nodeid, sizeof(unsigned short), 1 );

				pMeshNode->SetNodeID( nodeid );
			}
			break;

		case RD_MESH_NODE_NAME:
			{
				if( nodeid != INVALID_INDEX )
				{
					TCHAR name[256];
					ReadName( rStream, name );

//					MoString name = ReadName( rStream );

//#ifdef _USE_IMPNODES
//					node->SetName( name.c_str() );
//#else
//					realNode->SetName( name.c_str() );
//#endif
					pMeshNode->SetNodeName( name );
				}
			}
			break;

		case RD_MESH_NODE_PARENTID:
			{
				rStream.Read( &parentnodeid, sizeof(unsigned short), 1 );
			}
			break;

		case RD_MESH_NODE_PARENTNAME:
			{
				if( parentnodeid != INVALID_INDEX )
				{
					TCHAR name[256];
					ReadName( rStream, name );

					//MoString name = ReadName( rStream );

					RdMeshNode* pParentNode = GetNodeByName( name );
					if( pParentNode )
					{
						pMeshNode->SetParentNode( parentnodeid, name, pParentNode );
					}	// if
				}	// if
			}
			break;

		case RD_MESH_NODE_LOCALTM:
			{
				Matrix3 mat;

				float pos[3];
				float scl[3];
				float rot[4];

				rStream.Read( pos, sizeof(float), 3 );
				rStream.Read( rot, sizeof(float), 4 );
				rStream.Read( scl, sizeof(float), 3 );

				// 좌표 체계가 다르다.
				pMeshNode->SetLocalPosition( Point3( pos[0], pos[2], pos[1] ) );
				pMeshNode->SetLocalRotation( Quat( rot[0], rot[2], rot[1], rot[3] ) );
				pMeshNode->SetLocalScale( Point3( scl[0], scl[2], scl[1] ) );
			}
			break;

		case RD_MESH_NODE_WORLDTM:
			{
				// 3 x 4
				float matrix[12];
				rStream.Read( matrix, sizeof(float), 12 );

				Matrix3 worldTM;

				worldTM.SetRow( 0, Point3( matrix[0], matrix[1], matrix[2] ) );
				worldTM.SetRow( 1, Point3( matrix[3], matrix[4], matrix[5] ) );
				worldTM.SetRow( 2, Point3( matrix[6], matrix[7], matrix[8] ) );
				worldTM.SetRow( 3, Point3( matrix[9], matrix[10], matrix[11] ) );

				// max 좌표계에 맞게 변환
				Point3 row = worldTM.GetRow( 1 );
				worldTM.SetRow( 1, worldTM.GetRow( 2 ) );
				worldTM.SetRow( 2, row );

				Point4 col = worldTM.GetColumn( 1 );
				worldTM.SetColumn( 1, worldTM.GetColumn( 2 ) );
				worldTM.SetColumn( 2, col ); 

				pMeshNode->SetWorldTransform( worldTM );
			}
			break;

		case RD_MESH_NODE_LINKEDBONE:
			{
				unsigned short count = 0;
				rStream.Read( &count, sizeof(unsigned short), 1 );

				pMeshNode->SetLinkedBoneCount( count );
			}
			break;

		case RD_MESH_NODE_MAXLINK:
			{
				BYTE count = 0;
				rStream.Read( &count, sizeof(BYTE), 1 );

				pMeshNode->SetMaxLink( count );
			}
			break;

		case RD_MESH_NODE_VERTEXCOLOR:
			{
				BYTE enable = 0;
				rStream.Read( &enable, sizeof(BYTE), 1 );

				bool bEnable = ( 1 == enable ) ? true : false;

				pMeshNode->SetEnableVertexColor( bEnable );
			}
			break;

		case RD_MESH_NODE_VERTEX:
			{
				unsigned short vertexcount = 0;
				rStream.Read( &vertexcount, sizeof(unsigned short), 1 );

				pMeshNode->SetVertexCount( vertexcount );

				unsigned long leftchunksize = chunk.ulSize - sizeof(unsigned short);
				ReadVertices( rStream, leftchunksize, pMeshNode );
			}
			break;

		case RD_MESH_NODE_FACELIST:
			{
				ReadFaces( rStream, chunk.ulSize, pMeshNode );
			}
			break;

		case RD_MESH_NODE_AABB:
			{
				float min[3];
				float max[3];

				rStream.Read( min, sizeof(float), 3 );
				rStream.Read( max, sizeof(float), 3 );
			}
			break;

		case RD_MESH_NODE_OBB:
			{
				float center[3];
				rStream.Read( center, sizeof(float), 3 );

				unsigned short i = 0;
				for( i = 0; i < 3; ++i )
				{
					float axis[3];
					rStream.Read( axis, sizeof(float), 3 );
				}

				for( i = 0; i < 3; ++i )
				{
					float extent;
					rStream.Read( &extent, sizeof(float), 1 );
				}
			}
			break;

		default:
			RdMaxImp::RdChunk::Skip( rStream, chunk.ulSize - RdMaxImp::ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}
}

//------------------------------------------------------------------
void RdMeshImp::ReadNFMeshNode( RdMemStream& rStream, unsigned long ulSize )
{
	using namespace RdMaxImp;

	sChunk chunk;

	unsigned long leftsize = ulSize - RdMaxImp::ChunkHeaderSize;
	while( leftsize )
	{
		RdChunk::Read( &chunk, rStream );

		switch( chunk.ulTag )
		{
		case RD_MESH_NFNODE_VERTEX:
			{
				unsigned short count = 0;
				rStream.Read( &count, sizeof(unsigned short), 1 );

				Point3* pVertice = new Point3[count];
				rStream.Read( pVertice, sizeof(Point3), count );
			}
			break;

		case RD_MESH_NFNODE_FACE:
			{
				unsigned short count = 0;
				rStream.Read( &count, sizeof(unsigned short), 1 );

				unsigned long left = chunk.ulSize - sizeof(unsigned short);
				ReadNFMeshFaces( rStream, left, count );
			}
			break;

		default:
			RdChunk::Skip( rStream, chunk.ulSize - RdMaxImp::ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}
}

//------------------------------------------------------------------
void RdMeshImp::ReadNFMeshFaces( RdMemStream& rStream, unsigned long ulSize, unsigned short usNumFace )
{
	using namespace RdMaxImp;

	sChunk chunk;

	unsigned long leftsize = ulSize - ChunkHeaderSize;
	while( leftsize )
	{
		RdChunk::Read( &chunk, rStream );
		switch( chunk.ulTag )
		{
		case RD_MESH_NFNODE_FACE_INDEX:
			{
				unsigned short count = usNumFace * 3;
				unsigned short* indices = new unsigned short[count];

				rStream.Read( indices, sizeof(unsigned short), count );

				SAFEDELS( indices );
			}
			break;

		case RD_MESH_NFNODE_FACE_NORMAL:
			{
				Point3* normals = new Point3[usNumFace];

				rStream.Read( normals, sizeof(Point3), usNumFace );

				SAFEDELS( normals );
			}
			break;

		default:
			RdChunk::Skip( rStream, chunk.ulSize - ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}
}

//------------------------------------------------------------------
void RdMeshImp::ReadVertices( RdMemStream& rStream, unsigned long ulSize, RdMeshNode* pMeshNode )
{
	RdMaxImp::sChunk chunk;

	unsigned long leftsize = ulSize - RdMaxImp::ChunkHeaderSize;
	while( leftsize )
	{
		RdMaxImp::RdChunk::Read( &chunk, rStream );

		switch( chunk.ulTag )
		{
		case RD_MESH_NODE_VERTEX_POS:
			{
				Point3* pos = new Point3[ pMeshNode->GetVertexCount() ];
				rStream.Read( pos, sizeof(float), 3 * pMeshNode->GetVertexCount() );

				pMeshNode->SetVertexList( pos );
			}
			break;

		case RD_MESH_NODE_VERTEX_NORM:
			{
				Point3* nor = new Point3[ pMeshNode->GetVertexCount() ];
				rStream.Read( nor, sizeof(float), 3 * pMeshNode->GetVertexCount()  );

				pMeshNode->SetNormals( nor );
			}
			break;

		case RD_MESH_NODE_VERTEX_UV:
			{
				Point2* uv = new Point2[ pMeshNode->GetVertexCount() ];
				rStream.Read( uv, sizeof(float), 2 * pMeshNode->GetVertexCount() );

				pMeshNode->SetTexCoords( uv );
//				pTriObject->mesh.setNumTVerts( pMeshNode->GetVertexCount() );
			}
			break;

		case RD_MESH_NODE_VERTEX_BLEND:
			{	// 일단 저장하지 않는다.
				for( unsigned short i = 0; i < pMeshNode->GetVertexCount(); ++i )
				{
					BYTE count;
					rStream.Read( &count, sizeof(BYTE), 1 );
					for( BYTE j = 0; j < count; ++j )
					{
						unsigned short boneid = 0;
						float weight = 0.0f;
						rStream.Read( &boneid, sizeof(unsigned short), 1 );
						rStream.Read( &weight, sizeof(float), 1 );
					}
				}
			}
			break;

		case RD_MESH_NODE_VERTEX_COLOR:
			{
				Point3* color = new Point3[ pMeshNode->GetVertexCount() ];
				rStream.Read( color, sizeof(float), 3 * pMeshNode->GetVertexCount() );

				pMeshNode->SetVertColor( color );
			}
			break;

		default:
			RdMaxImp::RdChunk::Skip( rStream, chunk.ulSize - RdMaxImp::ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}
}

//------------------------------------------------------------------
void RdMeshImp::ReadFaces( RdMemStream& rStream, unsigned long ulSize, RdMeshNode* pMeshNode )
{
	using namespace RdMaxImp;

	sChunk chunk;

	unsigned long leftsize = ulSize - ChunkHeaderSize;
	while( leftsize )
	{
		RdChunk::Read( &chunk, rStream );

		switch( chunk.ulTag )
		{
		case RD_MESH_NODE_FACE:
			{
				unsigned short facecount = 0;
				rStream.Read( &facecount, sizeof(unsigned short), 1 );

				pMeshNode->SetFaceCount( facecount );

				unsigned long size = chunk.ulSize - sizeof(unsigned short);
				ReadFaceGroup( rStream, size, facecount, pMeshNode );
			}
			break;

		default:
			RdChunk::Skip( rStream, chunk.ulSize - RdMaxImp::ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}
}

//------------------------------------------------------------------
void RdMeshImp::ReadFaceGroup( RdMemStream& rStream, unsigned long ulSize,
							   unsigned short usFaceCount, RdMeshNode* pMeshNode )
{
	using namespace RdMaxImp;

	sChunk chunk;
	unsigned long leftsize = ulSize - ChunkHeaderSize;
	while( leftsize )
	{
		RdChunk::Read( &chunk, rStream );

		switch( chunk.ulTag )
		{
		case RD_MESH_NODE_FACE_INDICES:
			{
				unsigned short count = usFaceCount * 3;
				unsigned short* indices = new unsigned short[ count ];
				rStream.Read( indices, sizeof(unsigned short), count );

				pMeshNode->SetIndices( indices );
			}
			break;

		case RD_MESH_NODE_FACE_DIMAP:
			{
				TCHAR string[256];
				ReadName( rStream, string );

				//*******************************************************************
				// Only use dds format
				//MoString strFile = MoStringUtil::SplitPathFileName( string );
				//strFile += MoString( L".dds" );
				//*******************************************************************

				pMeshNode->SetDiffuseMap( string );
			}
			break;

		case RD_MESH_NODE_FACE_SPMAP:
			{
				TCHAR string[256];
				ReadName( rStream, string );

				//*******************************************************************
				// Only use dds format
				//MoString strFile = MoStringUtil::SplitPathFileName( string );
				//strFile += MoString( L".dds" );
				//*******************************************************************

				pMeshNode->SetSpecularMap( string );
			}
			break;

		case RD_MESH_NODE_FACE_TWOSIDE:
			{
				BYTE enabled = 0;
				rStream.Read( &enabled, sizeof(BYTE), 1 );

				pMeshNode->EnableTwoSide( enabled );
			}
			break;

		case RD_MESH_NODE_FACE_OPACITY:
			{
				BYTE enabled = 0;
				rStream.Read( &enabled, sizeof(BYTE), 1 );

				pMeshNode->EnableOpacity( enabled );
			}
			break;

		case RD_MESH_NODE_FACE_BLENDING:
			{
				BYTE enabled = 0;
				rStream.Read( &enabled, sizeof(BYTE), 1 );

				pMeshNode->EnableAlphaBlend( enabled );
			}
			break;

		default:
			RdChunk::Skip( rStream, chunk.ulSize - RdMaxImp::ChunkHeaderSize );
			break;
		}

		leftsize -= chunk.ulSize;
	}
}

//------------------------------------------------------------------
void RdMeshImp::ReadName( RdMemStream& rStream, TCHAR* pResult )
{
	unsigned short length = 0;
	rStream.Read( &length, sizeof(unsigned short), 1 );
	if( length > 0 )
	{
		wchar_t buffer[128];
		memset( buffer, 0, sizeof(wchar_t) * 128 );
		rStream.Read( buffer, sizeof(wchar_t), length );

		WCharToChar( buffer, pResult );
	}
}

//------------------------------------------------------------------
RdMeshNode* RdMeshImp::GetNodeByName( const TCHAR* name )
{
	int count = m_NodeList.Count();
	for( int i = count - 1; i >= 0; --i )
	{
		if( !_tcscmp( name, m_NodeList[i]->GetNodeName() ) )
			return m_NodeList[i];
	}

	return 0;
}