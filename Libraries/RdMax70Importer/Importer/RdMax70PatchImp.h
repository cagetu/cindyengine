#ifndef __RDMAX70PATCHIMP_H__
#define __RDMAX70PATCHIMP_H__

#include "RdMax70BaseImp.h"

#include "../RdMax70MemStream.h"

class RdPatchImp : public RdBaseImp
{
private:
	bool		ReadPatch( RdMemStream& rStream );
public:
	RdPatchImp( RdMax70Importer* pParent );
	virtual ~RdPatchImp();

	bool		Import( const TCHAR* strFileName );
};

#endif	// __RDMAX70PATCHIMP_H__