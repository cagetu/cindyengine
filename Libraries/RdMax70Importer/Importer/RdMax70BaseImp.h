#ifndef __RDMAX70BASEIMP_H__
#define __RDMAX70BASEIMP_H__

#include "../RdMax70ImporterHeader.h"

class RdMax70Importer;

class RdBaseImp
{
protected:
	RdMax70Importer*		m_pParent;

public:
	RdBaseImp( RdMax70Importer* pParent );
	virtual ~RdBaseImp();

	virtual bool		Import( const TCHAR* strFileName ) = 0;
};

#endif	// __RDMAX70PATCHIMP_H__