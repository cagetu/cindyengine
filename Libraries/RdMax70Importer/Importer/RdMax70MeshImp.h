#ifndef __RDMAXMESHIMP_H__
#define __RDMAXMESHIMP_H__

#include "RdMax70BaseImp.h"

#include "..\Data\RdMax70Mesh.h"
#include "../RdMax70MemStream.h"

class RdMeshImp : public RdBaseImp
{
private:
//	INodeTab				m_NodeList;
	Tab<RdMeshNode*>		m_NodeList;

	bool		ReadMeshData( RdMemStream& rStream );

	bool		ReadMagic( RdMemStream& rStream );
	bool		ReadVersion( RdMemStream& rStream );

	void		ReadMeshList( RdMemStream& rStream, unsigned long ulSize );

	void		ReadMeshNode( RdMemStream& rStream, unsigned long ulSize );

	void		ReadVertices( RdMemStream& rStream, unsigned long ulSize, RdMeshNode* pMeshNode );

	void		ReadFaces( RdMemStream& rStream, unsigned long ulSize, RdMeshNode* pMeshNode );
	void		ReadFaceGroup( RdMemStream& rStream, unsigned long ulSize, unsigned short usFaceCount, RdMeshNode* pMeshNode );

	void		ReadNFMeshNode( RdMemStream& rStream, unsigned long ulSize );
	void		ReadNFMeshFaces( RdMemStream& rStream, unsigned long ulSize, unsigned short usNumFace );

	void		ReadTrace( RdMemStream& rStream );

	void		ReadName( RdMemStream& rStream, TCHAR* pResult );

	RdMeshNode*	GetNodeByName( const TCHAR* name );

	void		Clear();
	void		BuildNodes();
public:
	RdMeshImp( RdMax70Importer* pParent );
	virtual ~RdMeshImp();

	/** Mesh를 임포트 합니다.
	*/
	bool		Import( const TCHAR* strFileName );
};

#endif	// __RDMAXMESHIMP_H__