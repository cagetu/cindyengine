// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOVIRTUALDISK_H__
#define __MOVIRTUALDISK_H__

// dll define
#ifdef MOVIRTUALDISK_EXPORTS
	#define MOVD_DLL __declspec(dllexport)
#else
	#define MOVD_DLL __declspec(dllimport)
#endif

#include <MoCommon/MoCommon.h>
#include <MoCommon/IO/MoFileStream.h>
#include <MoCommon/IO/MoMemStream.h>

//======================================================================================
// VD Data Definition
//======================================================================================
#define MOVD_MAGIC_NUM			0xA85C0601
#define MOVD_CUR_VERSION		0x20050923

//======================================================================================
// VD Chunk Header Definition
//======================================================================================
#define MOVD_MAGIC						0x9C28DAC1
#define MOVD_VERSION					0x9C28ABB1
#define MOVD_PACKVER					0x9C28D001
#define MOVD_NODELIST					0x9C289671
	#define MOVD_DIRNODE				0x9C289101
		#define MOVD_DIRNAME			0x9C289111
	#define MOVD_FILENODE				0x9C289201
		#define MOVD_FILENAME			0x9C28925A
		#define MOVD_SAVE_TYPE			0x9C28923B
		#define MOVD_CHECKSUM			0x9C28927C
		#define MOVD_DATA				0x9C2892BD
//======================================================================================


namespace MoVirtualDisk
{
	using namespace MoCommon;

	/** MOVD_DATA_TYPE
		@remarks
			MoVDisk에 저장되는 데이터의 저장방식			
	*/
	enum MOVD_DATA_TYPE
	{
		MOVD_DATA_RAW			= 0,	//!< 변형없이 데이터 원형 저장
		MOVD_DATA_ENCRYPT,				//!< 비트 암호화 저장
		MOVD_DATA_COMPRESS,				//!< 압축 저장
		MOVD_DATA_COMPENCRYPT			//!< 압축 및 비트암호화 저장
	};

	/** MOVD_NODE_TYPE
		@remarks
			MoVDisk에 저장된 노드의 형식
	*/
	enum MOVD_NODE_TYPE
	{
		MOVD_NODE_FILE			= 0,	//!< 저장된 파일 노드		
		MOVD_NODE_DIR,					//!< 디렉토리 노드		
		MOVD_NODE_ERROR
	};

	enum MOVD_THREAD_STATE
	{
		MOVD_THREAD_IDLE		= 0,
		MOVD_THREAD_LOADING,
		MOVD_THREAD_CANCELING,
		MOVD_THREAD_PAUSED
	};

}

#endif // __MOVIRTUALDISK_H__
