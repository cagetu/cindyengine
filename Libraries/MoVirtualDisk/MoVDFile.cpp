// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoVirtualDisk.h"
#include "MoVDFile.h"
#include "MoVDDir.h"
#include "MoVDisk.h"
#include "zlib/GZipHelper.h"
#include <process.h>


namespace MoVirtualDisk
{
	MoVDFile::MoVDFile( MoVDisk* pVDisk, 
						MoVDDir* pParentDir, 
						const ushort& usSaveType, 
						const uchar& ucChecksum,
						const MoString& strFileName,
						const MoString& strHardPath )
	{
		m_pVDisk			= pVDisk;
		m_pParentDir		= pParentDir;
		m_strName			= strFileName;
		m_strHardPath		= strHardPath;
		m_usSaveType		= usSaveType;
		m_ucChecksum		= ucChecksum;
		m_ulPosition		= 0;
		m_ulSize			= 0;
		m_pData				= NULL;

		m_pVDisk->IncreaseFileNum();
	}


	MoVDFile::~MoVDFile()
	{
		m_pVDisk->DecreaseFileNum();
		SAFEDELS( m_pData );
	}

	//MoString			GetName( void );
	// MOD: Alex [1/3/2005]
	const MoString& MoVDFile::GetName()
	{
		return m_strName;
	}


	MoString MoVDFile::GetHardPath()
	{
		return m_strHardPath;
	}


	ushort MoVDFile::GetSaveType()
	{
		return m_usSaveType;
	}


	uchar MoVDFile::GetChecksum()
	{
		return m_ucChecksum;
	}   


	MoVDDir* MoVDFile::GetParentDir()
	{
		return m_pParentDir;
	}

	void MoVDFile::SetParentDir( MoVDDir* pParentDir )
	{
		m_pParentDir = pParentDir;
	}


	void MoVDFile::RenameFIle( const MoString& strNewName )
	{
		m_strName = strNewName;
	}


	bool MoVDFile::WriteFile( MoFileStream& fStream )
	{
		ulong ulBegin, ulPos;

		fStream.BeginWriteChunkL( MOVD_FILENODE, ulBegin );
		{			
			// 이름
			fStream.BeginWriteChunkL( MOVD_FILENAME, ulPos );
			fStream.WriteString( m_strName.c_str() );
			fStream.EndWriteChunkL( ulPos );
			
			// 저장 방식
			fStream.BeginWriteChunkL( MOVD_SAVE_TYPE, ulPos );
			fStream.Write( &m_usSaveType, sizeof(ushort), 1 );
			fStream.EndWriteChunkL( ulPos );

			// 비트암호값
			fStream.BeginWriteChunkL( MOVD_CHECKSUM, ulPos );
			fStream.Write( &m_ucChecksum, sizeof(uchar), 1 );
			fStream.EndWriteChunkL( ulPos );

			// 데이터
			fStream.BeginWriteChunkL( MOVD_DATA, ulPos );
			{
				if (!m_pData)
					GetData();

				bool bRes;
				switch (m_usSaveType)
				{
				case MOVD_DATA_RAW:
					{
						bRes = WriteRaw( fStream );
					}
					break;
				case MOVD_DATA_ENCRYPT:
					{
						bRes = WriteEncrypt( fStream );
					}				
					break;
				case MOVD_DATA_COMPRESS:
					{
						bRes = WriteCompress( fStream );
					}
					break;
				case MOVD_DATA_COMPENCRYPT:
					{
						bRes = WriteCompEncrypt( fStream );
					}
					break;
				}
				assert( bRes );            
			}
			fStream.EndWriteChunkL( ulPos );
		}
		fStream.EndWriteChunkL( ulBegin );

		m_pVDisk->NotifyBuild();
		return true;
	}


	bool MoVDFile::WriteRaw( MoFileStream& fStream )
	{
		assert( m_pData );
		ulong ulWriteSize = fStream.Write( m_pData, 1, m_ulSize );

		assert( ulWriteSize == m_ulSize );
		

		return true;
	}


	bool MoVDFile::WriteEncrypt( MoFileStream& fStream )
	{
		assert( m_pData );
		for( ulong lPos = 0; lPos < m_ulSize; lPos++ )
		{
			m_pData[ lPos ] ^= m_ucChecksum;
		}

		ulong ulWriteSize = fStream.Write( m_pData, 1, m_ulSize );
		assert( ulWriteSize == m_ulSize );

		return true;
	}


	bool MoVDFile::WriteCompress( MoFileStream& fStream )
	{
		assert( m_pData );
		CA2GZIP gzip( (char*)m_pData, m_ulSize );
		ulong ulWriteSize = fStream.Write( gzip.pgzip, 1, gzip.Length );
		assert( ulWriteSize == gzip.Length );

		return true;
	}


	bool MoVDFile::WriteCompEncrypt( MoFileStream& fStream )
	{
		assert( m_pData );
		for( ulong ulPos = 0; ulPos < m_ulSize; ulPos++ )
		{
			m_pData[ ulPos ] ^= m_ucChecksum;
		}
		CA2GZIP gzip( (char*)m_pData, m_ulSize );
		ulong ulWriteSize = fStream.Write( gzip.pgzip, 1, gzip.Length );
		assert( ulWriteSize == gzip.Length );

		return true;
	}

	uchar* MoVDFile::GetData()
	{
		// 데이터가 없으면 로딩한다.
		if( m_pData )
		{
			return m_pData;
		}
		else	// 로딩
		{			
			if( m_strHardPath.empty() )	// 빌드된 넘이면 가상 디스크에서 로드
			{
				switch( m_usSaveType )
				{
				case MOVD_DATA_RAW:
					LoadRaw();
					break;
				case MOVD_DATA_ENCRYPT:
					LoadEncrypt();
					break;
				case MOVD_DATA_COMPRESS:
					LoadCompress();
					break;
				case MOVD_DATA_COMPENCRYPT:
					LoadCompEncrypt();
					break;
				} // switch( m_usSaveType )                
			}
			else									// 빌드 안된 넘은 하드디스크에서 로드
			{
                LoadFromHard();
			}
		}

        return m_pData;
	}

	ulong MoVDFile::GetDataSize()
	{
		return m_ulSize;
	}


	void MoVDFile::Close()
	{
		SAFEDELS( m_pData );
	}


	bool MoVDFile::LoadFromHard()
	{
		SAFEDELS( m_pData );

		MoFileStream fStream;
		bool bOpened = fStream.Open( m_strHardPath, L"rb" );
		if (bOpened)
		{
			fStream.Seek( 0, SEEK_END );
			m_ulSize = fStream.Tell();
			fStream.Seek( 0, SEEK_SET );

			m_pData = new uchar[ m_ulSize ];		
			ulong ulReadSize = fStream.Read( m_pData, 1, m_ulSize );
			assert( ulReadSize == m_ulSize );
			fStream.Close();		
		}
		return true;
	}

	bool MoVDFile::LoadRaw()
	{		
		assert( m_ulPosition );

		MoString strDiskPath = m_pVDisk->GetVDiskPath();
		MoFileStream fStream;
		bool bOpened = fStream.Open( strDiskPath, L"rb" );
		if (bOpened)
		{
			m_pData = new uchar[ m_ulSize ];
			memset( m_pData, 0, m_ulSize );

			fStream.Seek( m_ulPosition, SEEK_SET );
			ulong ulReadSize = fStream.Read( m_pData, 1, m_ulSize );
			assert( ulReadSize == m_ulSize );

			fStream.Close();		
		}
		else
		{
			Close();
		}

		return true;
	}


	bool MoVDFile::LoadEncrypt()
	{     
		assert( m_ulPosition );

		MoString strDiskPath = m_pVDisk->GetVDiskPath();
		MoFileStream fStream;
		bool bOpened = fStream.Open( strDiskPath, L"rb" );
		if (bOpened)
		{
			m_pData = new uchar[ m_ulSize ];
			memset( m_pData, 0, m_ulSize );

			fStream.Seek( m_ulPosition, SEEK_SET );
			fStream.Read( m_pData, m_ulSize, 1 );
			fStream.Close();

			for( ulong ulPos = 0; ulPos < m_ulSize; ulPos++ )
			{
				m_pData[ ulPos ] ^= m_ucChecksum;
			}
		}

		return true;
	}


	bool MoVDFile::LoadCompress()
	{		
		assert( m_ulPosition );

		MoString strDiskPath = m_pVDisk->GetVDiskPath();
		MoFileStream fStream;
		bool bOpened = fStream.Open( strDiskPath, L"rb" );
		if (bOpened)
		{
			LPGZIP buffer = (LPGZIP)malloc( m_ulSize );

			fStream.Seek( m_ulPosition, SEEK_SET );
			fStream.Read( buffer, m_ulSize, 1 );
			fStream.Close();

			CGZIP2A a( buffer, m_ulSize );
			m_pData = new uchar[ a.Length ];
			memcpy( m_pData, a.psz, a.Length );
			m_ulSize = a.Length;

			free( buffer );
		}

		return true;
	}


	bool MoVDFile::LoadCompEncrypt()
	{
		assert( m_ulPosition );
		
		MoString strDiskPath = m_pVDisk->GetVDiskPath();
		MoFileStream fStream;
		bool bOpened = fStream.Open( strDiskPath, L"rb" );
		if (bOpened)
		{
			LPGZIP buffer = (LPGZIP)malloc( m_ulSize );

			fStream.Seek( m_ulPosition, SEEK_SET );
			ulong ulReadSize = fStream.Read( buffer, 1, m_ulSize );
			assert( ulReadSize == m_ulSize );

			fStream.Close();

			CGZIP2A a( buffer, m_ulSize );
			m_pData = new uchar[ a.Length ];
			memcpy( m_pData, a.psz, a.Length );
			m_ulSize = a.Length;
			free( buffer );

			for( ulong ulPos = 0; ulPos < m_ulSize; ulPos++ )
			{
				m_pData[ ulPos ] ^= m_ucChecksum;
			}
		}

		return true;
	}

	void MoVDFile::SetPosition( ulong ulPosition )
	{
		m_ulPosition = ulPosition;
	}


	void MoVDFile::SetSize( ulong ulSize )
	{
		m_ulSize = ulSize;
	}

	void MoVDFile::SetData( uchar* pData, ulong ulSize )
	{
		assert( pData );

		m_pData = pData;
		m_ulSize = ulSize;
	}

	
	bool MoVDFile::Extract( const MoString& strPath )
	{
		MoFileStream fStream;
		bool bOpened = fStream.Open( strPath, L"wb" );
		if (bOpened)
		{
			uchar* pData = GetData();
			ulong ulWriteSize = fStream.Write( pData, 1, m_ulSize );
			assert( ulWriteSize == m_ulSize );
			fStream.Close();
		}

		return true;
	}


	uchar* MoVDFile::GetDataBackground()
	{
		//int val = 0;
		//_beginthreadEx( NULL, 0, 		// 플랫폼에 독립적이지 않은 넘

		return NULL;
	}

	MoVDFile* MoVDFile::Clone()
	{
		MoVDFile* pNew = new MoVDFile( m_pVDisk, m_pParentDir, m_usSaveType, m_ucChecksum, m_strName );

		pNew->m_ulSize = GetDataSize();
		pNew->m_pData = new uchar[ pNew->m_ulSize ];
		memcpy( pNew->m_pData, GetData(), pNew->m_ulSize );

		return pNew;
	}
}