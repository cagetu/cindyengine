// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOVDFILE_H__
#define __MOVDFILE_H__

namespace MoVirtualDisk
{
	//================================================================
	/** Virtual Disk File
	       @author Jaehee
		   @remarks 
				가상 디스크에서 파일을 담당하는 클래스
	*/
	//================================================================
	class MOVD_DLL MoVDFile
	{
		friend class MoVDisk;
		friend class MoVDDir;

	public:		
		virtual ~MoVDFile();

		const MoString&		GetName( void );
		MoString		GetHardPath( void );

		ushort			GetSaveType( void );
		uchar			GetChecksum( void );

		MoVDDir*		GetParentDir( void );
		uchar*			GetData( void );
		ulong			GetDataSize( void );

		void			Close( void );
		bool			Extract( const MoString& strPath );
		uchar*			GetDataBackground( void );
		
	private:
		MoVDFile( MoVDisk* pVDisk, 
				  MoVDDir* pParentDir, 
				  const ushort& usSaveType, 
				  const uchar& ucChecksum,
				  const MoString& strFileName,
				  const MoString& strHardPath = MoString( L"" ) );

		void			SetParentDir( MoVDDir* pParentDir );
		void			RenameFIle( const MoString& strNewName );

		bool			WriteFile( MoFileStream& fStream );
		bool			WriteRaw( MoFileStream& fStream );
		bool			WriteEncrypt( MoFileStream& fStream );
		bool			WriteCompress( MoFileStream& fStream );
		bool			WriteCompEncrypt( MoFileStream& fStream );

		bool			LoadFromHard( void );
		bool			LoadRaw( void );
		bool			LoadEncrypt( void );
		bool			LoadCompress( void );
		bool			LoadCompEncrypt( void );

		void			SetPosition( ulong ulPosition );
		void			SetSize( ulong ulSize );
		void			SetData( uchar* pData, ulong ulSize );
		MoVDFile*		Clone();


		MoVDisk*		m_pVDisk;		//!< 소속된 VDisk
		MoVDDir*		m_pParentDir;	//!< 부모 디렉토리의 포인터
		MoString		m_strName;		//!< 가상디스크 내의 이름
		MoString		m_strHardPath;	//!< 하드디스크 내의 파일 경로
		ushort			m_usSaveType;	//!< 저장 방식
		uchar			m_ucChecksum;	//!< 비트암호값
		uchar*			m_pData;		//!< 데이터 버퍼		

		ulong			m_ulPosition;	//!< 가상디스크내에서의 데이터 시작 포인트
		ulong			m_ulSize;		//!< 데이터의 크기
		
	};
}


#endif // __MOVDFILE_H__