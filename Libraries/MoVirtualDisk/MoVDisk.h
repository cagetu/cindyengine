// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOVDISK_H__
#define __MOVDISK_H__

#include <windows.h>

namespace MoVirtualDisk
{
	class MoVDFile;
	class MoVDDir;

	typedef void ( CALLBACK* VDISK_BUILD_CALLBACK )( ushort usProgress );

	//================================================================
	/** Virtual Disk
		   @author Jaehee
		   @remarks 
				가상 디스크를 총 책임지는 클래스
	*/
	//================================================================
	class MOVD_DLL MoVDisk
	{
	public:
		MoVDisk();
		virtual ~MoVDisk();
		
		bool		OpenVDisk( const MoString& strVDiskPath );
		bool		OpenFileStream( MoMemStream*& rStream, const MoString& strVDiskPath );
		bool		OpenVDisk( void );
		void		Close( void );

		bool		BuildVDisk( ulong ulPackVersion = 0x00000000 );
		
		ushort		ConfirmNode( MoString strNodePath );

		MoString	GetVDiskPath( void );
		void		SetVDiskPath( const MoString& strDiskPath );


		MoVDDir*	GetDir( MoString strDirPath = L"vdisk root" );
		MoVDDir*	GetRootDir( void )		{	return m_pRootDir;	}

		MoVDDir*	CreateDir( MoVDDir* pParent, MoString strDirName );
		MoVDDir*	RenameDir( MoString strDirPath, MoString strNewName );
		bool		RemoveDir( MoString strDirPath );


		MoVDFile*	CreateVDFile( MoVDDir* pParentDir,
								  const ushort& usSaveType, 
								  const uchar& ucChecksum,
								  MoString strFileName, 
								  const MoString& strHardPath = MoString( L"" ) );
		
		MoVDFile*	CreateVDFile( MoVDDir* pParentDir,
								  ushort usSaveType,
								  uchar ucChecksum,
								  MoString strFileName,
								  uchar* pData, ulong ulSize );
		
		MoVDFile*	GetFile( MoString strFilePath );
		MoVDFile*	RenameFile( MoString strFilePath, MoString strNewName );	
		bool		RemoveFile( MoString strFilePath );
		uchar*		GetFileData( MoString strFilePath );
		void		GetFileDataBackground( MoString strFilePath );

		ulong		GetPackVersion( void )		{	return m_ulPackVersion;		}		
		
		void		SetBuildCallback( VDISK_BUILD_CALLBACK pBuildCallback );
		void		IncreaseFileNum( void )		{	++m_ulFileNum;				}
		void		DecreaseFileNum( void )		{	--m_ulFileNum;				}
		void		NotifyBuild( void );

		bool		MergeVDisk( MoVDisk* pVDisk );

		ulong		GetTotalNum()				{		return m_ulTotalNum;		}

	protected:		

		MoVDDir*	m_pRootDir;			//!< 최상위 디렉토리		
		MoString	m_strVDiskPath;		//!< 하드디스크 상의 VDisk 파일의 절대 경로
		ulong		m_ulPackVersion;	//!< 저장한 가상 디스크의 파일 버젼. 사용자가 임의 설정

		ulong		m_ulTotalNum;		// 디렉토리, 파일 총 개수
		
		ulong		m_ulFileNum;
		ulong		m_ulRemainFileNum;
		VDISK_BUILD_CALLBACK	m_BuildCallback;

		MoString	GetFirstDirName( const MoString& strFullPath );
		MoString	GetRemainPath( MoString strFullPath );	
		
	private:
		bool		ReadDirNode( MoFileStream& fStream, MoVDDir* pParentDir );
		bool		ReadFileNode( MoFileStream& fStream, MoVDDir* pParentDir );

		bool		MergeVDir( MoVDDir* pA, MoVDDir* pB );
	};
}


#endif // __MOVDISK_H__