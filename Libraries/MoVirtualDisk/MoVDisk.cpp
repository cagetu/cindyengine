// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoVirtualDisk.h"
#include "MoVDisk.h"
#include "MoVDFile.h"
#include "MoVDDir.h"
#include <MoCommon/MoStringUtil.h>

namespace MoVirtualDisk
{
		MoVDisk::MoVDisk()
	{	
		m_pRootDir			= NULL;				
		m_ulPackVersion		= 0x00000000;
		m_BuildCallback		= NULL;
		m_ulFileNum			= 0;
		m_ulRemainFileNum	= 0;

		m_ulTotalNum		= 0;
	}


	MoVDisk::~MoVDisk()
	{
        Close();
	}


	MoString MoVDisk::GetVDiskPath()
	{
		return m_strVDiskPath;
	}


	void MoVDisk::SetVDiskPath( const MoString& strDiskPath )
	{
        m_strVDiskPath = strDiskPath;
	}


	//MoVDDir* MoVDisk::GetDir( const MoString& strDirPath )
	MoVDDir* MoVDisk::GetDir( MoString strDirPath )
	{
		MoStringUtil::LowerCase( strDirPath );		 

		vector<MoString> dirList;
		dirList.reserve( 10 );

		size_t start, pos;
		start = 0;
		do
		{
			pos = strDirPath.find_first_of( L"\\", start );
			if( pos == start )
			{
				start = pos + 1;
			}
			else if( pos == MoString::npos )
			{
				dirList.push_back( strDirPath.substr( start ) );
				break;
			}
			else
			{
				dirList.push_back( strDirPath.substr( start, pos - start ) );
				start = pos + 1;
			}
			start = strDirPath.find_first_not_of( L"\\", start );

		}while( pos != MoString::npos );

		vector<MoString>::iterator i = dirList.begin();
		size_t nSize = dirList.size();

		MoVDDir* pDir = m_pRootDir;

		if( nSize > 1)
		{	
			++i;
			
			for( size_t n = 1; n < nSize; ++n, ++i )
			{
				pDir = pDir->GetChildDir( (*i) );
				if( NULL == pDir )
					break;
			}
		
			dirList.clear();
		}

		return pDir;
	}


	MoVDDir* MoVDisk::CreateDir( MoVDDir* pParent, MoString strDirName )
	{
		// NotBuilded 추가
		MoStringUtil::LowerCase( strDirName );        	

		if( !pParent )
		{
			pParent = m_pRootDir;
		}
		return pParent->CreateChildDir( strDirName );
	}


	MoVDDir* MoVDisk::RenameDir( MoString strDirPath, MoString strNewName )
	{
		MoStringUtil::LowerCase( strDirPath );
		MoStringUtil::LowerCase( strNewName );


		MoVDDir* pDir = GetDir( strDirPath );
		
		MoVDDir* pParent = pDir->GetParentDir();
		
		if( !pParent )	// 부모가 없으면 루트이므로 바꾸지 않고 리턴한다.
		{			
			ushort usRes = ConfirmNode( strDirPath );
			if( MOVD_NODE_DIR == usRes )
			{
				return m_pRootDir;
			}
			
		} // if( !pParent )	
		else			// 부모가 있으면 중복검사
		{
			// 중복되면 그대로 리턴			
			if( pParent->GetChildDir( strNewName ) )
			{
				return NULL;
			}
        }

		pParent->DetachChildDir( pDir );	// Parent의 맵에서 분리시키고
		pDir->RenameDir( strNewName );		// 이름을 바꾼후
		pParent->AttachChildDir( pDir );	// 다시 맵에 넣는다.        

		return pDir;
	}


	bool MoVDisk::RemoveDir( MoString strDirPath )
	{
		MoStringUtil::LowerCase( strDirPath );

		MoVDDir* pDir = GetDir( strDirPath );
		MoVDDir* pParent = pDir->GetParentDir();
		
		if( NULL == pParent )	// 루트이면 걍 리턴
		{
			return false;
		}

		return pParent->RemoveChildDir( pDir->GetName() );		
	}


	MoVDFile* MoVDisk::CreateVDFile( MoVDDir* pParentDir,
									 const ushort& usSaveType, 
									 const uchar& ucChecksum,
									 MoString strFileName, 
									 const MoString& strHardPath )
	{
		MoStringUtil::LowerCase( strFileName );

		// 중복되지 않으면 생성		
		return pParentDir->CreateChildFile( usSaveType, ucChecksum, strFileName, strHardPath );
	}

	MoVDFile* MoVDisk::CreateVDFile( MoVDDir* pParentDir,
									 ushort usSaveType,
									 uchar ucChecksum,
									 MoString strFileName,
									 uchar* pData, ulong ulSize )
	{
		MoStringUtil::LowerCase( strFileName );

		MoVDFile* pFile = pParentDir->CreateChildFile( usSaveType, ucChecksum, strFileName );
		if( pFile )
		{
			pFile->SetData( pData, ulSize );
		}

		return pFile;
	}

	MoString MoVDisk::GetFirstDirName( const MoString& strFullPath )
	{
		MoString strOut;

        size_t ulPos = strFullPath.find( L"\\", 0 );		
		size_t ulLen = strFullPath.length();

		if( ulPos > ulLen ) // Last Node
		{
			strOut = strFullPath;
		}
		else				// remain node exist
		{
			// !!! Debug 요망
			strOut.assign( strFullPath.c_str(), &strFullPath[ ulPos ] );
		}

		return strOut;
	}


	MoString MoVDisk::GetRemainPath( MoString strFullPath )
	{
		MoString strOut;
		strOut.clear();

		size_t ulPos = strFullPath.find( L"\\", 0 );
		size_t ulLen = strFullPath.length();
		size_t ulRes = ulLen - ulPos;
		//strOut.resize( ulLen - ulPos );

		wchar_t wc0[512];

		if( ulPos > ulLen )
		{
			strOut.clear();
		}
		else
		{	
			// !!! Debug 요망
			MoString strTmp;
			strTmp.assign( &strFullPath[ ulPos + 1 ], ulRes );			
			wcsncpy_s( wc0, 512, strTmp.c_str(), ulRes );
			wc0[ ulRes + 1 ] = L'\0';
			wc0[ ulRes + 2 ] = L'\0';
			strOut = wc0;			
		}
		return strOut;
	}


	MoVDFile* MoVDisk::GetFile( MoString strFilePath )
	{
		MoStringUtil::LowerCase( strFilePath );

		vector<MoString> dirList;
		dirList.reserve( 10 );

		size_t start, pos;
		start = 0;
		do
		{
			pos = strFilePath.find_first_of( L"\\", start );
			if( pos == start )
			{
				start = pos + 1;
			}
			else if( pos == MoString::npos )
			{
				dirList.push_back( strFilePath.substr( start ) );
				break;
			}
			else
			{
				dirList.push_back( strFilePath.substr( start, pos - start ) );
				start = pos + 1;
			}
			start = strFilePath.find_first_not_of( L"\\", start );

		}while( pos != MoString::npos );

//		vector<MoString>(dirList).swap(dirList);

		vector<MoString>::iterator i = dirList.begin();
		size_t nSize = dirList.size();

		MoVDDir* pDir = m_pRootDir;
		++i;
		
		for( size_t n = 1; n < nSize - 1; ++n, ++i )
		{
			pDir = pDir->GetChildDir( (*i) );
			if( NULL == pDir )
				return NULL; 
		}

		MoVDFile* pFile = pDir->GetChildFile( (*i) );
		
		dirList.clear();

		return pFile;
	}

	MoVDFile* MoVDisk::RenameFile( MoString strFilePath, MoString strNewName )
	{
		MoStringUtil::LowerCase( strFilePath );
		MoStringUtil::LowerCase( strNewName );

		MoVDFile* pFile = GetFile( strFilePath );
		MoVDDir* pParentDir = pFile->GetParentDir();

		if( !pParentDir )	// if there is no parent accur error
		{
			assert( 0 );
			return NULL;
		} // if( !pParentDir )
		else				// 부모가 있으면 중복 검사
		{
			if( pParentDir->GetChildFile( strNewName ) )
			{				
				return NULL;
			}
		}

		pParentDir->DetachChildFile( pFile );
		pFile->RenameFIle( strNewName );
		pParentDir->AttachChildFile( pFile );

		return pFile;
	}


	bool MoVDisk::RemoveFile( MoString strFilePath )
	{
		MoStringUtil::LowerCase( strFilePath );

		MoVDFile* pFile = GetFile( strFilePath );
		if( !pFile )
		{
			return false;
		}

		MoVDDir* pParentDir = pFile->GetParentDir();
		if( !pParentDir )
		{
			return false;
		}

		return pParentDir->RemoveChildFile( pFile->GetName() );
	}

	

	ushort MoVDisk::ConfirmNode( MoString strNodePath )
	{
		MoStringUtil::LowerCase( strNodePath );

		MoVDDir* pCurDir;		
		MoString strFirstDir, strRemain, strOld;

		// 1. 루트인지 임시루트인지 판별
		//ushort usRes = IsBuilded( strNodePath );		
		pCurDir = m_pRootDir;
		
		// 2. 루트면 결과 리턴
		//SeperatePath( strNodePath, strFirstDir, strOld );
		strFirstDir = GetFirstDirName( strNodePath );
		strOld = GetRemainPath( strNodePath );

		//if( MoStringUtil::BLANK == strOld )
		if( true == strOld.empty() )
		{
			return MOVD_NODE_DIR;
		}
		

		// 3. 파일일 때나 리프 디렉토리 일때까지 루프		
		while( 1 )
		{
			//SeperatePath( strOld, strFirstDir, strRemain );
			strFirstDir = GetFirstDirName( strOld );
			strRemain = GetRemainPath( strOld );
						
			//if( MoStringUtil::BLANK == strRemain )	// 최종 노드일 때
			if( true == strRemain.empty() )
			{
				if( NULL != pCurDir->GetChildDir( strFirstDir ) )	// 디렉토리 이면
				{
					//return ( ( MOVD_NODE_DIR == usRes ) ? MOVD_NODE_DIR : MOVD_NODE_TMP_DIR );
					return MOVD_NODE_DIR;
				}
				else if( NULL != pCurDir->GetChildFile( strFirstDir ) )	// 파일 이면
				{
					//return ( ( MOVD_NODE_DIR == usRes ) ? MOVD_NODE_FILE : MOVD_NODE_TMP_FILE );
					return MOVD_NODE_FILE;
				}
			} // if( strFirstDir == strRemain )
			else							// 노드가 남아 있으면
			{
				pCurDir = pCurDir->GetChildDir( strFirstDir );
                strOld = strRemain;
			}
		}
			
		return MOVD_NODE_ERROR;
	}


	bool MoVDisk::BuildVDisk( ulong ulPackVersion )
	{
		m_ulRemainFileNum = m_ulFileNum;

		m_ulPackVersion = ulPackVersion;

		if( m_strVDiskPath.empty() )
		{
			assert( 0 );
			return false;
		}

		// 임시 파일 경로 설정
		//MoString strDrive	= MoStringUtil::SplitPathDrive( m_strVDiskPath );		//.SplitPathDrive();
		//MoString strDir		= MoStringUtil::SplitPathDir( m_strVDiskPath );			//.SplitPathDir();
		//MoString strFile	= MoStringUtil::SplitPathFileName( m_strVDiskPath );	//.SplitPathFilename();
		//MoString strExt		= MoStringUtil::SplitPathExt( m_strVDiskPath );			//.SplitPathExt();

		MoString strDrive, strDir, strFile, strExt;
		MoStringUtil::SplitPath( m_strVDiskPath, strDrive, strDir, strFile, strExt );

		MoString strTmpPath = strDrive + strDir + strFile + MoString( L".tmp" );

		// 스트림열기
		MoFileStream fStream;
		bool bRes = fStream.Open( strTmpPath, L"wb" );
		assert( bRes );

		ulong ulBegin;

		// MagicNum
		ulong ulMagic = MOVD_MAGIC_NUM;
		fStream.BeginWriteChunkL( MOVD_MAGIC, ulBegin );
		fStream.Write( &ulMagic, sizeof( ulong ), 1 );
		fStream.EndWriteChunkL( ulBegin );

		// VDisk File Version
		ulong ulVersion = MOVD_CUR_VERSION;
		fStream.BeginWriteChunkL( MOVD_VERSION, ulBegin );
		fStream.Write( &ulVersion, sizeof( ulong ), 1 );
		fStream.EndWriteChunkL( ulBegin );

		// Packing Version
		fStream.BeginWriteChunkL( MOVD_PACKVER, ulBegin );
		fStream.Write( &m_ulPackVersion, sizeof( ulong ), 1 );
		fStream.EndWriteChunkL( ulBegin );

		// NodeList
		fStream.BeginWriteChunkL( MOVD_NODELIST, ulBegin );
        m_pRootDir->WriteDir( fStream );		
		fStream.EndWriteChunkL( ulBegin );

		fStream.Close();

		::_wremove( m_strVDiskPath.c_str() );
		::_wrename( strTmpPath.c_str(), m_strVDiskPath.c_str() );

		m_ulRemainFileNum = 0;

		bRes = OpenVDisk( m_strVDiskPath.c_str() );	// const MoString& 를 파라메터로 넘겨야 하기 때문에...
		assert( bRes );

		return true;
	}


	void MoVDisk::Close()
	{
		SAFEDEL( m_pRootDir );
		m_ulTotalNum	= 0;
		m_strVDiskPath.clear();
		//m_strVDiskPath	= MoStringUtil::BLANK;
	}


	bool MoVDisk::OpenVDisk()
	{
		Close();
		m_pRootDir		= new MoVDDir( this, NULL, L"vdisk root" );		
		return true;
	}


	bool MoVDisk::OpenVDisk( const MoString& strVDiskPath )
	{
		Close();
		m_pRootDir = new MoVDDir( this, NULL, L"vdisk root" );

		m_strVDiskPath = strVDiskPath;		

		// open Stream
		MoFileStream fStream;
		bool bRes = fStream.Open( strVDiskPath, L"rb" );
		if( !bRes )
		{
			//assert( bRes );
//			Except( MoException::ERR_FILE_NOT_FOUND, L"Can't find file : " + strVDiskPath, L"MoVDisk::OpenVDisk" );
			return false;
		}
		

		ChunkHeaderL hdr;
		ulong ulBuf;

        // Magic
		fStream.ReadChunkHeaderL( &hdr );
		if( MOVD_MAGIC != hdr.ulTag )
		{
			return false;
		}

		fStream.Read( &ulBuf, sizeof( ulong ), 1 );
		if( MOVD_MAGIC_NUM != ulBuf )
		{
			return false;
		}

		// Version : 버젼 정보 따로 저장하지 않는다. 추후 필요시 저장하여 필요한 곳에 사용한다.		
		fStream.ReadChunkHeaderL( &hdr );	
		if( MOVD_VERSION != hdr.ulTag )
		{
			return false;
		}
		fStream.Read( &ulBuf, sizeof( ulong ), 1 );
		

		// Pack Version
		fStream.ReadChunkHeaderL( &hdr );
		fStream.Read( &m_ulPackVersion, sizeof( ulong ), 1 );

		// Node : 노드 리스트와 루트디렉토리까지 디폴트로 읽는다.
		fStream.ReadChunkHeaderL( &hdr );
		if( MOVD_NODELIST == hdr.ulTag )
		{
			bRes = ReadDirNode( fStream, NULL );
			assert( bRes );
		} // if( MOVD_NODELIST != hdr.ulTag );

		
		// 스트림 닫기
		fStream.Close();

		return true;
	}


	
	bool MoVDisk::ReadDirNode( MoFileStream& fStream, MoVDDir* pParentDir )
	{
        ChunkHeaderL hdr, next;
        
		fStream.ReadChunkHeaderL( &hdr );
		hdr.ulSize -= 8L;

		switch( hdr.ulTag )
		{
		case MOVD_DIRNODE:
			{
				// 이름 읽고 디렉토리 노드 생성
				ChunkHeaderL name;
				fStream.ReadChunkHeaderL( &name );
				if( MOVD_DIRNAME != name.ulTag )
				{
					assert( 0 );
					return false;
				}

//				MoString strDirName = fStream.ReadString();
				wchar szDir[512];
				fStream.ReadString( szDir, 512 );
				MoString strDirName(szDir);

				hdr.ulSize -= name.ulSize;
				MoVDDir* pCurDir;

				// 루트이면 루트 생성
				if( !pParentDir && ( L"vdisk root" == strDirName ) )
				{
					if( !m_pRootDir )
						m_pRootDir	= new MoVDDir( this, NULL, L"vdisk root" );
					pCurDir		= m_pRootDir;
				}
				else
				{
					pCurDir		= CreateDir( pParentDir, strDirName );					
				}

				// 노드 끝까지 읽기
				
				while( hdr.ulSize )
				{
					fStream.ReadNextChunkHeaderL( &next );
					switch( next.ulTag )
					{
						case MOVD_DIRNODE:
							//++m_ulTotalNum;
							ReadDirNode( fStream, pCurDir );
							break;
						case MOVD_FILENODE:
							++m_ulTotalNum;
							ReadFileNode( fStream, pCurDir );
							break;
						default:
							fStream.SkipChunkL();
							break;
					} // switch( next.ulTag )		
					hdr.ulSize -= next.ulSize;
				}
			}
			break;

		default:
            fStream.SkipChunkL();
			break;
		}

		return true;
	}


	bool MoVDisk::ReadFileNode( MoFileStream& fStream, MoVDDir* pParentDir )
	{
		ChunkHeaderL hdr;
		ulong ulSize, ulBeginPos, ulPosition;

		// File Chunk
		ulBeginPos = fStream.Tell();
		fStream.ReadChunkHeaderL( &hdr );
		if( MOVD_FILENODE != hdr.ulTag )
		{
			assert( 0 );
			return false;
		} // if( MOVD_FILENODE != hdr.ulTag )
		ulSize = hdr.ulSize;		

		// Name
		fStream.ReadChunkHeaderL( &hdr );
		if( MOVD_FILENAME != hdr.ulTag )
		{
			assert( 0 );
			return false;
		} // if( MOVD_FILENAME != hdr.ulTag )
//		MoString strFileName = fStream.ReadString();
		wchar szFileName[64];
		fStream.ReadString( szFileName, 64 );
		MoString strFileName(szFileName);

		// SaveType
		fStream.ReadChunkHeaderL( &hdr );
		if( MOVD_SAVE_TYPE != hdr.ulTag )
		{
			assert( 0 );
			return false;
		} // if( MOVD_SAVE_TYPE != hdr.ulTag )
		ushort usSaveType;
		fStream.Read( &usSaveType, sizeof(ushort), 1 );

		// Chekcsum
		fStream.ReadChunkHeaderL( &hdr );
		if( MOVD_CHECKSUM != hdr.ulTag )
		{
			assert( 0 );
			return false;
		} // if( MOVD_CHECKSUM != hdr.ulTag )
		uchar ucChecksum;
		fStream.Read( &ucChecksum, sizeof( uchar ), 1 );

        // Data
		fStream.ReadChunkHeaderL( &hdr );
		if( MOVD_DATA != hdr.ulTag )
		{
			assert( 0 );
			return false;
		} // if( MOVD_DATA != hdr.ulTag )

		// size, position
		ulPosition = fStream.Tell();
		ulSize = ulSize - ( ulPosition - ulBeginPos );

		fStream.Seek( ulSize, SEEK_CUR );

		MoVDFile* pFile = CreateVDFile( pParentDir, usSaveType, ucChecksum, strFileName );
		pFile->SetPosition( ulPosition );
		pFile->SetSize( ulSize );		

		return true;
	}


	uchar* MoVDisk::GetFileData( MoString strFilePath )
	{
		MoStringUtil::LowerCase( strFilePath );

		MoVDFile* pFile = GetFile( strFilePath );
		return pFile->GetData();
	}


	
	void MoVDisk::GetFileDataBackground( MoString strFilePath )
	{
		MoStringUtil::LowerCase( strFilePath );

		MoVDFile* pFile = GetFile( strFilePath );
		pFile->GetDataBackground();
	}

	void MoVDisk::SetBuildCallback( VDISK_BUILD_CALLBACK pBuildCallback )
	{
		m_BuildCallback = pBuildCallback;
	}


	void MoVDisk::NotifyBuild()
	{
		m_ulRemainFileNum--;

		if( !m_BuildCallback )
			return;

		float fPercent = 100.0f - ( ( (float)m_ulRemainFileNum / (float)m_ulFileNum ) * 100.0f );
		

		m_BuildCallback( (ushort)fPercent );
    }



	bool MoVDisk::MergeVDisk( MoVDisk* pVDisk )
	{
		MoVDDir* pBRootDir = pVDisk->GetRootDir();
		MergeVDir( m_pRootDir, pBRootDir );

		BuildVDisk( ++m_ulPackVersion );

		return true;
	}


	bool MoVDisk::MergeVDir( MoVDDir* pA, MoVDDir* pB )
	{
		// File
		MoVDDir::FileMapIter i, iend;
		iend = pB->m_FileMap.end();
		
		MoVDFile* pFile = NULL;
		for( i = pB->m_FileMap.begin(); i != iend; ++i )
		{
			pFile = i->second;
			if( pA->GetChildFile( pFile->GetName() ) )
			{
				pA->RemoveChildFile( pFile->GetName() );
			}

			pA->AttachChildFile( pFile->Clone() );
		}

		// Dir 
		MoVDDir::DirMapIter di, diend;
		diend = pB->m_DirMap.end();

		MoVDDir* pBDir = NULL;
		MoVDDir* pADir = NULL;
		for( di = pB->m_DirMap.begin(); di != diend; ++di )
		{
			pBDir = di->second;
			pADir = pA->GetChildDir( pBDir->GetName() );

			if( NULL == pADir )
			{
				pADir = pA->CreateChildDir( pBDir->GetName() );
			}

			MergeVDir( pADir, pBDir );
		}

		return true;	
	}

	bool MoVDisk::OpenFileStream( MoMemStream*& rStream, const MoString& strVDiskPath )
	{
		OpenVDisk( strVDiskPath );

		MoVDFile* pFile = NULL;
		pFile	= GetFile( L"vdisk root\\cltexcel\\cbd.cbd" );

		if( pFile == NULL )
		{
			return false;
		}

		if( false == rStream->Open( pFile->GetData(), pFile->GetDataSize() ) )
		{
//			LOGH( L"faild file open" );
			return false;
		}

		return true;
	}
}

