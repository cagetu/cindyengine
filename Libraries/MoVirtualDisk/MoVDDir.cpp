// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#include "MoVirtualDisk.h"
#include "MoVDDir.h"
#include "MoVDFile.h"

namespace MoVirtualDisk
{
	MoVDDir::MoVDDir( MoVDisk* pVDisk, MoVDDir* pParent, const MoString& strName )
	{
		m_pParentDir	= pParent;
		m_strName		= strName;
		m_pVDisk		= pVDisk;
	}

	MoVDDir::~MoVDDir()
	{
		DirMapIter dirIte = m_DirMap.end();
		for( DirMapIter it = m_DirMap.begin(); it != dirIte; ++it )
		{
			SAFEDEL( it->second );
		}		

		FileMapIter fileIte = m_FileMap.end();
		for( FileMapIter it = m_FileMap.begin(); it != fileIte; ++it )
		{
			SAFEDEL( it->second );			
		}

		m_DirMap.clear();
		m_FileMap.clear();
	}


	MoString MoVDDir::GetName() const
	{
		return m_strName;
	}


	MoVDDir* MoVDDir::CreateChildDir( const MoString& strDirName )
	{
        MoVDDir* pDir = GetChildDir( strDirName );

		if( pDir )
		{
			return NULL;	// 존재하면 생성되지 않는다.
		}
		else
		{
			pDir = new MoVDDir( m_pVDisk, this, strDirName );	
		}

		m_DirMap.insert( DirMap::value_type( pDir->GetName(), pDir ) );		

		return pDir;
	}


	MoVDDir* MoVDDir::GetChildDir( const MoString& strDirName )
	{
		DirMapIter it = m_DirMap.find( strDirName );
		if( it != m_DirMap.end() )
		{
			return it->second;
		}

		return NULL;
	}

	MoVDDir* MoVDDir::GetChildDir( ulong ulIndex )
	{
		DirMap::iterator it = m_DirMap.begin();
		for( ushort i = 0; i < ulIndex; i++ )
		{
			it++;			
		}

		if( it != m_DirMap.end() )
			return it->second;
		
		return NULL;
	}


	ulong MoVDDir::GetChildDirNum()
	{
		return (ulong)m_DirMap.size();
	}


	MoVDDir* MoVDDir::GetParentDir()
	{
		return m_pParentDir;
	}

	
	void MoVDDir::SetParentDir( MoVDDir* pParent )
	{
		m_pParentDir = pParent;
		
	}


	void MoVDDir::RenameDir( const MoString& strNewName )
	{
		m_strName = strNewName;
	}

	
	MoVDDir* MoVDDir::AttachChildDir( MoVDDir* pChild )
	{
		MoVDDir* pDir = GetChildDir( pChild->GetName() );
		if( pDir )
		{
			//return pDir;	// 디렉토리는 존재하믄 중복시킬수 있으니 널말고 걍 나둬본다.
			return NULL;
		}

		m_DirMap.insert( DirMap::value_type( pChild->GetName(), pChild ) );
		pChild->SetParentDir( this );

		return pChild;
	}


	MoVDDir* MoVDDir::DetachChildDir( MoVDDir* pChild )
	{		
		DirMapIter it = m_DirMap.find( pChild->GetName() );
		if( it != m_DirMap.end() )
		{
			m_DirMap.erase( it );
		}
		else
		{
			return NULL;
		}

		pChild->SetParentDir( NULL );

        return pChild;
	}


	bool MoVDDir::RemoveChildDir( const MoString& strDirName )
	{
		DirMapIter it = m_DirMap.find( strDirName );
		if( it != m_DirMap.end() )
		{			
			SAFEDEL( it->second );
			m_DirMap.erase( it );
			return true;
		} // if( it != m_DirMap.end() )

		return false;		
	}
	

	bool MoVDDir::HasChild()
	{
		ulong ulSize = (ulong)m_DirMap.size();
		ulSize += (ulong)m_FileMap.size();

		if( ulSize > 0 )
		{
			return true;
		}
		return false;
	}



	MoVDFile* MoVDDir::CreateChildFile( const ushort& usSaveType, 
										const uchar& ucChecksum,
										const MoString& strFileName, 
										const MoString& strHardPath)
	{
		MoVDFile* pFile = GetChildFile( strFileName );

		if( pFile )
		{
			return NULL;
		}
		else
		{			
			pFile = new MoVDFile( m_pVDisk, this, usSaveType, ucChecksum, strFileName, strHardPath );
		}
		m_FileMap.insert( FileMap::value_type( pFile->GetName(), pFile ) );		
        
		return pFile;
	}


	MoVDFile* MoVDDir::GetChildFile( const MoString& strFileName )
	{
		FileMapIter it = m_FileMap.find( strFileName );
		if( it == m_FileMap.end() )
		{
			return NULL;			
		}		

		return it->second;		
	}


	MoVDFile* MoVDDir::GetChildFile( ulong ulIndex )
	{
		// TODO : Debuging 요망
		FileMapIter it = m_FileMap.begin();

		for( ushort i = 0; i < ulIndex; i++ )
		{
			it++;			
		} // for( ushort i = 0; i < ulIndex; i++, ++it )

		if( it != m_FileMap.end() )
		{
			return it->second;
		}

		return NULL;
	}


	ulong MoVDDir::GetChildFileNum()
	{
		return (ulong)m_FileMap.size();
	}


	MoVDFile* MoVDDir::AttachChildFile( MoVDFile* pFile )
	{
		MoVDFile* pTmpFile = GetChildFile( pFile->GetName() );
		if( pTmpFile )
		{
			return NULL;
		}

		m_FileMap.insert( FileMap::value_type( pFile->GetName(), pFile ) );
		pFile->SetParentDir( this );

		return pFile;
	}


	MoVDFile* MoVDDir::DetachChildFile( MoVDFile* pChildFile )
	{
		FileMapIter it = m_FileMap.find( pChildFile->GetName() );
		if( it != m_FileMap.end() )
		{
			m_FileMap.erase( it );
		}
		else 
		{
			return NULL;
		}
		pChildFile->SetParentDir( NULL );

		return pChildFile;
	}

	bool MoVDDir::RemoveChildFile( const MoString& strFileName )
	{
		FileMapIter it = m_FileMap.find( strFileName );
		if( it != m_FileMap.end() )
		{
			SAFEDEL( it->second );
			m_FileMap.erase( it );

			return true;
		}

		return false;
	}


	bool MoVDDir::WriteDir( MoFileStream& fStream )
	{
		ulong ulBegin, ulPos;

		// 자기 쓰고
		fStream.BeginWriteChunkL( MOVD_DIRNODE, ulBegin );
		{
			// 이름 쓰고
			fStream.BeginWriteChunkL( MOVD_DIRNAME, ulPos );
			fStream.WriteString( m_strName.c_str() );
			fStream.EndWriteChunkL( ulPos );

			// 자식 디렉토리 쓰고
			DirMap::iterator itDir, itDirEnd;
			itDirEnd = m_DirMap.end();
			for( itDir = m_DirMap.begin(); itDir != itDirEnd; ++itDir )
			{
				itDir->second->WriteDir( fStream );
			}

            // 자식 파일 쓰고
			FileMap::iterator itFile, itFileEnd;
			itFileEnd = m_FileMap.end();
			for( itFile = m_FileMap.begin(); itFile != itFileEnd; ++itFile )
			{
				itFile->second->WriteFile( fStream );
			}

		}
		fStream.EndWriteChunkL( ulBegin );		

		return true;
	}
}