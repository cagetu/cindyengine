// The Project 새마을운동
// Copyright (c) 2005~ Hi-WIN Corporation
// All Rights Reserved

// http://www.hi-win.com

#ifndef __MOVDDIR_H__
#define __MOVDDIR_H__

namespace MoVirtualDisk
{
	class MoVDFile;
	//================================================================
	/** Virtual Disk Directory
		   @author Jaehee
		   @remarks 
				가상 디스크에서 디렉토리 역할을 하는 클래스
	*/
	//================================================================
	class MOVD_DLL MoVDDir
	{
		friend class MoVDisk;

	public:
		MoVDDir( MoVDisk* pVDisk, MoVDDir* pParent, const MoString& strName );
		virtual ~MoVDDir();

		MoString	GetName( void ) const;
		MoVDDir*	CreateChildDir( const MoString& strDirName );
		MoVDDir*	GetChildDir( const MoString& strDirName );
		MoVDDir*	GetChildDir( ulong ulIndex );
		ulong		GetChildDirNum( void );
		MoVDDir*	GetParentDir( void );

		void		SetParentDir( MoVDDir* pParent );

		bool		RemoveChildDir( const MoString& strDirName );

		bool		HasChild( void );

		MoVDFile* CreateChildFile( const ushort& usSaveType, 
								   const uchar& ucChecksum,
								   const MoString& strFileName, 
								   const MoString& strHardPath = MoString( L"" ) );

		MoVDFile*	GetChildFile( const MoString& strFileName );
		MoVDFile*	GetChildFile( ulong ulIndex );
		ulong		GetChildFileNum( void );

		bool		RemoveChildFile( const MoString& strFileName );

	protected:		
		//typedef std::map< MoString, MoVDDir* >	DirMap;
		typedef HashMap< MoString, MoVDDir*, MoStringHash >		DirMap;
		typedef DirMap::iterator				DirMapIter;
		DirMap				m_DirMap;			//!< 자식 디렉토리 맵
		
		
		//typedef std::map< MoString, MoVDFile* > FileMap;
		typedef HashMap< MoString, MoVDFile*, MoStringHash >	FileMap;
		typedef FileMap::iterator				FileMapIter;
		FileMap				m_FileMap;			//!< 자식 파일 맵


		MoVDDir*	m_pParentDir;	//!< 부모 디렉토리의 포인터
		MoString	m_strName;		//!< 디렉토리 이름
		MoVDisk*	m_pVDisk;		//!< 소속된 가상디스크

	private:
		void		RenameDir( const MoString& strNewName );

		MoVDDir*	DetachChildDir( MoVDDir* pChild );
		MoVDDir*	AttachChildDir( MoVDDir* pChild );
		MoVDFile*	AttachChildFile( MoVDFile* pFile );

		MoVDFile*	DetachChildFile( MoVDFile* pChildFile );

		bool		WriteDir( MoFileStream& fStream );
	};
}


#endif // __MOVDDIR_H__