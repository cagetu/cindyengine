// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CN_TYPES_H__
#define __CN_TYPES_H__

namespace Cindy
{
	//------------------------------------------------------------------
	// Type Definition
	//------------------------------------------------------------------
#ifndef uint
	typedef unsigned int		uint;
#endif

//#ifndef uchar
//	typedef unsigned char		uchar;
//#endif
//	
//#ifndef ushort
//	typedef unsigned short		ushort;
//#endif
//
//#ifndef ulong 
//	typedef unsigned long		ulong;
//#endif
//
//#ifndef wchar
//	typedef	wchar_t				wchar;
//#endif

	typedef unsigned char		uint8;
	typedef signed char			int8;

	typedef unsigned short		uint16;
	typedef signed short		int16;

	typedef unsigned int		uint32;
	typedef signed int			int32;

	typedef unsigned __int64	uint64;
	typedef signed __int64		int64;

	typedef	short				float16;
	typedef float				float32;
	typedef double				float64;

	// RGBA
	typedef unsigned long	RGBA;
	#define CN_ARGB(a,r,g,b) ((unsigned long)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))
	#define CN_RGBA(r,g,b,a) CN_ARGB(a,r,g,b)
	#define CN_XRGB(r,g,b)   CN_ARGB(0xff,r,g,b)
	#define CN_COLORVALUE(r,g,b,a) CN_RGBA((uint32)((r)*255.f),(uint32)((g)*255.f),(uint32)((b)*255.f),(uint32)((a)*255.f))

	// UByte4
	typedef int ubyte4;
	// (x, y, z, w) assuming these are in the range 0..1
	#define CN_UBYTE4(x, y, z, w) (((int)(x * 255.0f) & 0xff) << 24) | (((int)(y * 255.0f) & 0xff) << 16) | (((int)(z * 255.0f) & 0xff) << 8) | (((int)(w * 255.0f) & 0xff))	// the endianness might be wrong, but you get the idea

	/////
	//struct short2
	//{
	//	union {
	//		struct {
	//			int16 y;
	//			int16 x;
	//		};
	//		//int16 value[2];
	//		int32 value;
	//	};
	//};

	/////
	//struct ubyte4
	//{
	//	union {
	//		struct {
	//			uint8 w;
	//			uint8 z;
	//			uint8 y;
	//			uint8 x;
	//		};
	//		//uint8 value[4];
	//		unsigned long value;
	//	};
	//};
}

#endif	// __CN_TYPES_H__