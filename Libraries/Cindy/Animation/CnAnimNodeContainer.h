//================================================================
// File:           : CnAnimNodeContainer.h
// Original Author : changhee
// Creation Date   : 2008. 12. 4
//================================================================
#ifndef __CN_ANIMATEDNODE_CONTAINER_H__
#define __CN_ANIMATEDNODE_CONTAINER_H__

#include "../Foundation/CnObject.h"

namespace Cindy
{
	class CnNode;
	class CnTransformable;
	class CnAnimController;
	class CnAnimChannel;

	//================================================================
	/** AnimatedNode Controller
	    @author    changhee
		@brief	   애니메이션 대상 노드 컨트롤러
	*/
	//================================================================
	class CN_DLL CnAnimContainer : public CnObject
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------
		// Defines
		//------------------------------------------------------
		typedef std::map<ushort, Ptr<CnTransformable>>	NodeMap;
		typedef NodeMap::iterator							NodeIter;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnAnimContainer();
		virtual ~CnAnimContainer();

		// Owner
		void	SetOwner( CnNode* Owner );

		// Node
		virtual bool	AddNode( CnTransformable* pNode, bool IsRoot );
		virtual bool	RemoveNode( ushort usIndex );
		virtual void	RemoveAllNodes( bool bDelete = false );

		CnTransformable*	GetNode( ushort usIndex );
		CnTransformable*	GetNode( const CnString& Name );
		const NodeMap&		GetNodes() const;

		uint			Size();
	protected:
		friend class CnAnimChannel;
		friend class CnAnimController;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnNode*		m_Owner;
		NodeMap		m_Nodes;

		//------------------------------------------------------
		// Internal Methods
		//------------------------------------------------------
		// 애니메이션 업데이트
		virtual void	_Animate( CnAnimChannel* pChannel );
		//virtual void	_Animate( CnAnimController* pController );
	};

	#include "CnAnimNodeContainer.inl"
}

#endif	// __CN_ANIMATEDNODE_CONTAINER_H__