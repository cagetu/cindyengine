//================================================================
// File:               : CnAnimationSequencer.inl
// Related Header File : CnAnimationSequencer.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
//----------------------------------------------------------------
inline
ControllerID CnAnimController::GetID() const
{
	return ms_nID;
}

//================================================================
//	AnimatedNodeCache
//================================================================
inline
void CnAnimController::SetContainer( CnAnimContainer* pNodeCtrl )
{
	m_NodeCtrl = pNodeCtrl;
}
//----------------------------------------------------------------
inline
const Ptr<CnAnimContainer>&
CnAnimController::GetContainer() const
{
	return m_NodeCtrl;
}