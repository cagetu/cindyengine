//================================================================
// File:               : CnFrameController.inl
// Related Header File : CnFrameController.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
//------------------------------------------------------
inline
void CnFrameController::_SetCurrentStatus( ControllerStatus nStatus )
{
	m_nStatus = nStatus;
}

//------------------------------------------------------
inline
ControllerStatus CnFrameController::GetCurrentStatus() const
{
	return m_nStatus;
}