//================================================================
// File:           : CnAnimationKeyBuffer.h
// Original Author : changhee
// Creation Date   : 2008. 12. 4
//================================================================
#ifndef __CN_ANIMATION_KEY_BUFFER_H__
#define __CN_ANIMATION_KEY_BUFFER_H__

#include "CnAnimationInstance.h"

namespace Cindy
{
	//================================================================
	/** AnimationKeyBuffer
	    @author    changhee
		@brief	   애니메이션 Key를 저장해 놓는다.
	*/
	//================================================================
	class CnAnimKeyBuffer : public CnObject
	{
		__DeclareClass(CnAnimKeyBuffer);
	public:
		//typedef std::vector<AnimKey>	KeyList;
		typedef std::map<ushort, AnimKey>	KeyList;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnAnimKeyBuffer();
		virtual ~CnAnimKeyBuffer();

		//void	Setup( ushort NumNodes );
	private:
		void	Add( ushort Index, AnimKey& Key );
		//const KeyList&	GetBuffer();
	};
}

#endif	//	__CN_ANIMATION_KEY_BUFFER_H__