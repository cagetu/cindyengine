//================================================================
// File:               : CnAnimationLayer.inl
// Related Header File : CnAnimationLayer.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 4
//================================================================

//================================================================
//	Current PlayState
//================================================================
inline
void CnAnimationLayer::SetPlayState( CnAnimationLayer::PlayState eState )
{
	m_CurrentPlayState = eState;
}
//----------------------------------------------------------------
inline
CnAnimationLayer::PlayState
CnAnimationLayer::GetPlayState() const
{
	return m_CurrentPlayState;
}

//================================================================
//	Mix
//================================================================
inline
bool CnAnimationLayer::IsEnableMix() const
{
	return m_bEnableMix;
}

//----------------------------------------------------------------
inline
void CnAnimationLayer::SetWeight( float fWeight )
{
	m_fMixWeight = fWeight;
}

//----------------------------------------------------------------
inline
float CnAnimationLayer::GetWeight() const
{
	return m_fMixWeight;
}
