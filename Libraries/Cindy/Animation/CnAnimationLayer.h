//================================================================
// File:           : CnAnimationLayer.h
// Original Author : changhee
// Creation Date   : 2008. 12. 4
//================================================================
#ifndef __CN_ANIMATION_LAYER_H__
#define __CN_ANIMATION_LAYER_H__

#include "CnAnimationInstance.h"

namespace Cindy
{
	class CnAnimationCtrl;
	class CnXFormObject;

	//================================================================
	/** Animation Layer
	    @author    changhee
		@since     2007. 2. 1
		@remarks   동일한 우선순위를 가지는 애니메이션을 처리
	*/
	//================================================================
	class CnAnimationLayer : public CnObject
	{
		CN_DECLARE_RTTI;
	public:
		enum PlayState
		{
			STOPPED = 0,
			PLAYING,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnAnimationLayer( CnAnimationCtrl* Parent );
		virtual ~CnAnimationLayer();

		// Motion
		void			SetMotion( AnimInstPtr& spMotion, bool bRepeat = true, float fBlendWeight = 0.0f, float fBlendRate = 1.0f );

		// 갱신
		void			Update( SECOND fElapsedTime, bool bMix = false );

		// 제어
		void			Play();
		void			Stop();
		void			Reset();

		// State
		void			Repeat( bool bSet = true );
		bool			IsRepeat() const;
		bool			IsEnd() const;

		// PlayState
		void			SetPlayState( PlayState eState );
		PlayState		GetPlayState() const;

		// MixWeight
		bool			IsEnableMix() const;
		void			SetWeight( float fWeight );
		float			GetWeight() const;
	private:
		friend class CnAnimationCtrl;
		friend class CnAnimatedNodeCache;
		friend class CnSkeletonInstance;

		// parent
		CnAnimationCtrl*	m_Parent;

		// motion
		AnimInstPtr	m_spCurrentMotion;

		// animation time
		SECOND		m_fCurrentTime;
		SECOND		m_fTotalTime;

		// motion blend
		bool		m_bEnableBlending;
		float		m_fMotionBlendWeight;
		float		m_fMotionBlendRate;

		// mix
		bool		m_bEnableMix;
		float		m_fMixWeight;

		// state
		PlayState	m_CurrentPlayState;
		bool		m_bRepeat;
		bool		m_bIsEnd;

		//------------------------------------------------------
		// Internal Methods
		//------------------------------------------------------
		// blend
		void	_StartBlending( float fBlendWeight, float fBlendRate );
		void	_EndBlending();

		// update
		void	_UpdateBlending( SECOND fElapsedTime );
		void	_UpdateCurrentTime( SECOND fElaspedTime );

		void	_UpdateNodes();

		//
		bool	_GetKey( CnXFormObject* TargetNode, AnimKey* Output );
	};

#include "CnAnimationLayer.inl"
}

#endif	// __CN_ANIMATION_LAYER_H__