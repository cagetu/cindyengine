//================================================================
// File:               : CnAnimationLayer.cpp
// Related Header File : CnAnimationLayer.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnAnimationLayer.h"
#include "CnAnimationController.h"
#include "CnAnimatedNodeCache.h"
#include "CnBoneNode.h"

namespace Cindy
{
	//================================================================
	//defines
	CN_IMPLEMENT_RTTI( Cindy, CnAnimationLayer, CnObject );
	/// Const/Dest
	CnAnimationLayer::CnAnimationLayer( CnAnimationCtrl* Parent )
		: m_Parent(Parent)
		, m_CurrentPlayState(STOPPED)
		, m_bEnableBlending(false)
		, m_fMotionBlendWeight(1.0f)
		, m_fMotionBlendRate(0.0f)
		, m_bEnableMix(false)
		, m_fMixWeight(1.0f)
	{
	}
	CnAnimationLayer::~CnAnimationLayer()
	{
	}

	//================================================================
	//	Motion 변경
	//================================================================
	//----------------------------------------------------------------
	void CnAnimationLayer::SetMotion( AnimInstPtr& spMotion, bool bRepeat, float fBlendWeight, float fBlendRate )
	{
		if (spMotion.IsNull())
			return;

		if (!m_spCurrentMotion.IsNull())
		{
			if (m_spCurrentMotion->GetName() != spMotion->GetName())
			{
				_StartBlending( fBlendWeight, fBlendRate );
			}
		}

		m_spCurrentMotion = spMotion;
		m_bRepeat = bRepeat;
		m_bIsEnd = false;

		m_fTotalTime = m_spCurrentMotion->GetTotalTime();
		m_fCurrentTime = 0.0f;
	}

	//================================================================
	//	Animation Blending
	//================================================================
	//----------------------------------------------------------------
	void CnAnimationLayer::_StartBlending( float fBlendWeight, float fBlendRate )
	{
		m_bEnableBlending = true;
		m_fMotionBlendWeight = fBlendWeight;
		m_fMotionBlendRate = fBlendRate;
	}
	//----------------------------------------------------------------
	void CnAnimationLayer::_EndBlending()
	{
		m_bEnableBlending = false;
		m_fMotionBlendWeight = 1.0f;
	}
	//----------------------------------------------------------------
	void CnAnimationLayer::_UpdateBlending( SECOND fElapsedTime )
	{
		if (false == m_bEnableBlending)
			return;

		m_fMotionBlendWeight += fElapsedTime * m_fMotionBlendRate;
		if (m_fMotionBlendWeight >= 1.0f)
		{
			_EndBlending();
		}
	}

	//================================================================
	//	Play/Stop
	//================================================================
	//----------------------------------------------------------------
	void CnAnimationLayer::Play()
	{
		m_fCurrentTime = 0.0f;
		m_bIsEnd = false;

		SetPlayState( PLAYING );
	}

	//----------------------------------------------------------------
	void CnAnimationLayer::Stop()
	{
		SetPlayState( STOPPED );
	}

	//----------------------------------------------------------------
	void CnAnimationLayer::Reset()
	{
		Stop();

		_EndBlending();
	}

	//================================================================
	//	Update
	//================================================================
	//----------------------------------------------------------------
	void CnAnimationLayer::Update( SECOND fElapsedTime, bool bMix )
	{
		if (PLAYING != GetPlayState())
			return;

		m_bEnableMix = bMix;

		// 시간 갱신
		_UpdateBlending( fElapsedTime );
		_UpdateCurrentTime( fElapsedTime );

		// 노드 갱신
		_UpdateNodes();
	}

	//----------------------------------------------------------------
	/** @brief	애니메이션을 처리하고, Node들을 업데이트 하자 */
	//----------------------------------------------------------------
	void CnAnimationLayer::_UpdateNodes()
	{
		m_Parent->m_NodeCache->_UpdateNodes( this );
		//CnXFormObject* node = NULL;
		//CnAnimatedNodeCache* nodeCache = NULL;

		//CnAnimatedNodeCache::NodeList nodelist;
		//CnAnimatedNodeCache::NodeList::const_iterator in, inend;

		//nodelist = m_Parent->m_NodeCache->GetNodes();
		//inend = nodelist.end();
		//for (in = nodelist.begin(); in != inend; ++in)
		//{
		//	node = (in->second);

		//	if (node->IsExactOf(&CnBoneNode::RTTI))
		//	{
		//		CnBoneNode* bone = StaticCast<CnBoneNode>(node);
		//		if (bone->IsLocked())
		//			continue;
		//	}

		//	// 현재 노드의 TM을 구한다.
		//	AnimKey newKey;
		//	if (true == _GetKey( node, &newKey ))
		//	{
		//		if (m_bEnableMix)
		//		{
		//			// 현재 상태
		//			CnVector3 prevPos = node->GetLocalPosition();
		//			CnQuaternion prevRot = node->GetLocalRotation();
		//			CnVector3 prevScl = node->GetLocalScale();

		//			// 보간
		//			newKey.pos.Lerp( prevPos, newKey.pos, this->m_fMixWeight );
		//			newKey.rot.Slerp( prevRot, newKey.rot, this->m_fMixWeight );
		//			newKey.scl.Lerp( prevScl, newKey.scl, this->m_fMixWeight );
		//		}

		//		node->SetPosition( newKey.pos );
		//		node->SetRotation( newKey.rot );
		//		node->SetScale( newKey.scl );
		//	}
		//}
	}

	//----------------------------------------------------------------
	/** @brief	현재 시간 갱신 */
	//----------------------------------------------------------------
	void CnAnimationLayer::_UpdateCurrentTime( SECOND fElapsedTime )
	{
		m_fCurrentTime += fElapsedTime;
		if (m_fCurrentTime > m_fTotalTime)
		{
			if (m_bRepeat)
			{
				m_fCurrentTime = 0.0f;
			}
			else
			{
				m_fCurrentTime = m_fTotalTime;
				m_bIsEnd = true;

				SetPlayState( STOPPED );
			} // if
		} // if
	}

	//----------------------------------------------------------------
	/** @brief	Get Animation Key */
	//----------------------------------------------------------------
	bool CnAnimationLayer::_GetKey( CnXFormObject* TargetNode, AnimKey* Output )
	{
		// 모션 블랜딩 끝
		if (m_fMotionBlendWeight == 1.0f)
		{
			// 현재 노드의 TM을 구한다.
			return m_spCurrentMotion->GetKey( Output, TargetNode->GetName(), this->m_fCurrentTime );
		}
		else
		{
			AnimKey key;
			if (m_spCurrentMotion->GetKey( &key, TargetNode->GetName(), this->m_fCurrentTime ))
			{
				// 현재 상태
				CnVector3 prevPos = TargetNode->GetLocalPosition();
				CnQuaternion prevRot = TargetNode->GetLocalRotation();
				CnVector3 prevScl = TargetNode->GetLocalScale();

				// 보간
				Output->pos.Lerp( prevPos, key.pos, this->m_fMotionBlendWeight );
				Output->rot.Slerp( prevRot, key.rot, this->m_fMotionBlendWeight );
				Output->scl.Lerp( prevScl, key.scl, this->m_fMotionBlendWeight );
				return true;
			}
		}

		return false;
	}
}