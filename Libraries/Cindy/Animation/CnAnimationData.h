//================================================================
// File:           : CnAnimationData.h
// Original Author : changhee
//================================================================
#ifndef __CN_ANIMATION_DATA_H__
#define __CN_ANIMATION_DATA_H__

#include "../Resource/CnResource.h"
#include "../Resource/CnExportedDataDefines.h"

#include <MoCommon/IO/MoMemStream.h>

namespace Cindy
{
	class CnAnimInstance;

	//================================================================
	/** Animation Resource Data
	    @author    changhee
		@since     2007. 8. 29
		@brief	   Animation 리소스 데이터
	*/
	//================================================================	
	class CN_DLL CnAnimationData : public CnResource
	{
		__DeclareRtti;
	public:
		virtual ~CnAnimationData();

		// Load/Unload
		bool		Load( const wchar* strFileName ) override;
		bool		Unload() override;

		// Save
		bool		Save( const CnString& strFileName );

		// Device Lost/Restore
		void		OnLost() override;
		void		OnRestore() override;

		// Frame Info
		ushort		GetTotalFrame() const;
		ulong		GetFrameRate() const;
		void		SetFrameRate( ulong ulFrameRate );	

		// Node
		ushort		GetNumOfNodes() const;
		Export::AnimNode*	GetNodeData( const CnString& NodeName );

		// Instance
		CnAnimInstance*	CreateInstance();
	private:
		friend class CnAnimationDataManager;

		typedef HashMap<CnString, Export::AnimNode*>	NodeDataMap;
		typedef NodeDataMap::iterator					NodeDataIter;

		//------------------------------------------------------
		// Internal Variables
		//------------------------------------------------------
		MoCommon::MoMemStream	m_DataMemStream;
		ulong					m_ulTotalDataSize;

		ushort					m_usStartFrame;		//!< 애니메이션 시작 프레임
		ushort					m_usEndFrame;		//!< 애니메이션 끝 프레임

		ushort					m_usTotalFrame;		//!< 전체 프레임
		ulong					m_ulFrameRate;		//!< FrameRate
		ulong					m_ulPriority;		//!< 애니메이션 우선순위

		NodeDataMap				m_NodeDatas;
		ushort					m_usNumNodeDatas;

		//------------------------------------------------------
		// Internal Methods
		//------------------------------------------------------
		CnAnimationData( CnResourceManager* pParent );

		// Data 읽기 시작
		bool		LoadData();

		// 노드 정보 추가
		bool		AddNodeData( Export::AnimNode* pNode );

		// 파일 정보 읽기
		bool		ReadMagicNumber();
		bool		ReadFileVersion();

		// 노드 애니메이션 정보 읽기
		void		ReadAnim( ulong ulDataSize );
		void		ReadAnimNode( ulong ulDataSize, Export::AnimNode* pNode );

		// 노드 Key정보 읽기
		void		ReadPosTrack( ulong ulDataSize, Export::AnimNode* pNode );
		void		ReadRotTrack( ulong ulDataSize, Export::AnimNode* pNode );
		void		ReadSclTrack( ulong ulDataSize, Export::AnimNode* pNode );

		// Name 읽기
		void		ReadNameData( CnString& rResult );
	};

	//==================================================================
	/** AnimDataPtr
		@author			cagetu
		@brief		Animation 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL AnimDataPtr : public Ptr<CnAnimationData>
	{
	public:
		AnimDataPtr();
		explicit AnimDataPtr( CnAnimationData* pObject );
		AnimDataPtr( const AnimDataPtr& spObject );
		AnimDataPtr( const RscPtr& spRsc );

		/** operator = 	*/
		AnimDataPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnAnimationData.inl"
}

#endif	// __CN_ANIMATION_DATA_H__