//================================================================
// File:           : CnSkeletonInstance.h
// Original Author : changhee
// Creation Date   : 2007. 2. 1
//================================================================
#ifndef __CN_SKELETON_INSTANCE_H__
#define __CN_SKELETON_INSTANCE_H__

#include "CnSkeletonData.h"
#include "CnAnimNodeContainer.h"
#include "CnBoneNode.h"
#include "Math/CnObjectOrientedBox.h"
#include "Math/CnMatrix44.h"

namespace Cindy
{
	class CnNode;

	//================================================================
	/** CnSkeletonInstance
	    @author    changhee
		@since     2007. 2. 1
		@brief	   뼈대 관리자
	*/
	//================================================================
	class CN_DLL CnSkeletonInstance : public CnAnimContainer
	{
		__DeclareClass(CnSkeletonInstance);
	public:
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnSkeletonInstance();
		virtual ~CnSkeletonInstance();

		// Setup
		void	Setup( const SkelDataPtr& SkeletonResource );
		void	Clear();

		// Update
		void	Update();

		// Debug
		void	DrawForDebug();
	private:
		struct BBox
		{
			CnBoneNode*		boneNode;
			Math::OBB		OBB;
			Math::Matrix44	lastXForm;

			BBox() : boneNode(0) {}
		};

		typedef std::vector<BBox*>				OBBArray;
		typedef OBBArray::iterator				OBBIter;

		typedef std::vector<Ptr<CnBoneNode>>	RootNodeArray;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		// 본 리소스
		SkelDataPtr		m_spResource;
		OBBArray		m_OBBs;

		bool			m_bNeedUpdate;

		RootNodeArray	m_RootNodes;

		//------------------------------------------------------
		// Internal Methods
		//------------------------------------------------------
		// Update Volume
		void	_UpdateBounds();
		void	_UpdateTransform();

		// Bone
		bool	AddNode( CnTransformable* pNode, bool IsRoot ) override;
		bool	RemoveNode( ushort usIndex ) override;
		void	RemoveAllNodes( bool bDelete = false ) override;

		void	_Animate( CnAnimChannel* pChannel ) override;
		//void	_Animate( CnAnimController* pController ) override;
	};

	#include "CnSkeletonInstance.inl"
}

#endif	// __CN_SKELETON_INSTANCE_H__