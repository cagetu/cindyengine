//================================================================
// File:               : CnAnimInstance.inl
// Related Header File : CnAnimInstance.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 2
//================================================================

//================================================================
//	Name
//================================================================
//----------------------------------------------------------------
inline
const CnString& CnAnimInstance::GetName() const
{
	return m_spAnimResource->GetName();
}

//================================================================
//	Priority
//================================================================
//----------------------------------------------------------------
inline
void CnAnimInstance::SetPriority( PRIORITY nPriority )
{
	m_Priority = nPriority;
}
//----------------------------------------------------------------
inline
PRIORITY CnAnimInstance::GetPriority() const
{
	return m_Priority;
}

//================================================================
//	Frame, Time Information
//================================================================
//----------------------------------------------------------------
inline
SECOND CnAnimInstance::GetTotalTime() const
{
	return m_fTotalTime;
}

//----------------------------------------------------------------
inline
FRAME CnAnimInstance::GetTotalFrame() const
{
	return ( m_fTotalTime / m_fSecondPerFrame );
}

//----------------------------------------------------------------
inline
SECOND CnAnimInstance::GetSecondPerFrame() const
{
	return m_fSecondPerFrame;
}

//----------------------------------------------------------------
//inline
//FRAME CnAnimInstance::GetFramePerSecond() const
//{
//	return m_fFramePerSecond;
//}

//================================================================
//	State
//================================================================
//----------------------------------------------------------------
//inline
//void CnAnimInstance::Repeat( bool bEnable )
//{
//	m_bRepeat = bEnable;
//}
//
////----------------------------------------------------------------
//inline
//bool CnAnimInstance::IsRepeat() const
//{
//	return m_bRepeat;
//}
//
////----------------------------------------------------------------
//inline
//bool CnAnimInstance::IsEnd() const
//{
//	return m_bIsEnd;
//}
