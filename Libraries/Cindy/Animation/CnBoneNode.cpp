//================================================================
// File:               : CnBoneNode.cpp
// Related Header File : CnBoneNode.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnBoneNode.h"
#include "CnSkeletonInstance.h"
#include "CnAnimationSequencer.h"

#include "Scene/CnCamera.h"
#include "Util/CnLog.h"

namespace Cindy
{
	bool CnBoneNode::ms_NeedUpdateTransform = false;
	//----------------------------------------------------------------
	/** @brief	Transform 업데이트 여부 */
	//----------------------------------------------------------------
	void CnBoneNode::NeedUpdateTransform( bool bNeed )
	{
		ms_NeedUpdateTransform = bNeed;
	}

	//================================================================
	// defines..
	__ImplementRtti( Cindy, CnBoneNode, CnAnimNode );
	/// Const/Dest
	CnBoneNode::CnBoneNode( const wchar* strName, ushort nIndex )
		: CnAnimNode(strName, nIndex)
		, m_Type(None)
	{
	}
	CnBoneNode::~CnBoneNode()
	{
	}

	//----------------------------------------------------------------
	/** @brief	자식 노드 구현 재정의 */
	//----------------------------------------------------------------
	CnNode* CnBoneNode::_CreateChildImpl( const CnString& strName, ushort nIndex )
	{
		return CnNew CnBoneNode( strName.c_str(), nIndex );
	}

	//----------------------------------------------------------------
	/** Set Skeleton Controller
	    @brief		뼈대 관리자 세팅
		@param      pSkeleton : 뼈대 관리자
		@return     none
	*/
	//----------------------------------------------------------------
	void CnBoneNode::SetSkeleton( CnSkeletonInstance* pSkeleton )
	{
		m_pSkeleton = pSkeleton;
	}

	//----------------------------------------------------------------
	/** GetLocalXform
	    @brief      뼈대 로컬 변환 행렬을 구한다.
		@param		rOut : 결과 행렬
		@return     none  
	*/
	//----------------------------------------------------------------
	void CnBoneNode::GetLocalXform( Math::Matrix44& rOut )
	{
		rOut.Identity();

		Math::Matrix44 world = GetWorldXForm();
		rOut.Multiply( m_mInverseTransform, world );
	}

	//----------------------------------------------------------------
	/** Update
	    @brief		Node 클래스의 Update 재정의.
		@remarks	뼈대가 업데이트 준비가 되기 전에는 업데이트 되어선 안된다.
		@see		CnNode::Update
	*/
	//----------------------------------------------------------------
	void CnBoneNode::UpdateTransform( bool bUpdateChildren, bool bChangedParent )
	{
		if (false == ms_NeedUpdateTransform)
			return;

		CnNode::UpdateTransform( bUpdateChildren, bChangedParent );
	}

	//----------------------------------------------------------------
	/** UpdateScene
	    @breif      Node클래스의 UpdateScene 재정의.
		@remarks	Bone은 랜더링 하지 않는다.
		@see		ISceneObject::UpdateScene
	*/
	//----------------------------------------------------------------
	void CnBoneNode::UpdateScene( CnCamera* pCamera, CnVisibleSet* pVisibleSet )
	{

		ChildIter iend = m_Children.end();
		for( ChildIter i = m_Children.begin(); i != iend; ++i )
			(i->second)->UpdateScene( pCamera, pVisibleSet );
	}
}