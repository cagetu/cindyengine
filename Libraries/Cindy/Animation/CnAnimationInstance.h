//================================================================
// File:           : CnAnimInstance.h
// Original Author : changhee
// Creation Date   : 2007. 2. 1
//================================================================
#ifndef __CN_ANIMATION_INSTANCE_H__
#define __CN_ANIMATION_INSTANCE_H__

#include "CnAnimationData.h"

namespace Cindy
{
	//------------------------------------------------------
	typedef float	SECOND;
	typedef float	FRAME;
	typedef ulong	TICK;
	typedef int		PRIORITY;

	struct AnimKey
	{
		Math::Vector3		pos;
		Math::Quaternion	rot;
		Math::Vector3		scl;
	};

	//================================================================
	/** Animation Instance
	    @author		changhee
		@brief		하나의 AnimationData에 애니메이션
	*/
	//================================================================	
	class CN_DLL CnAnimInstance : public CnObject
	{
		__DeclareClass(CnAnimInstance);
	public:
		// Lock 범위
		struct Locker
		{
			enum Result
			{
				Ignore = 0,
				Lock,
				Free,
			};

			CnString	begin;	//!< 애니메이션 lock 시작
			CnString	end;	//!< 애니메이션 lock 끝
		};

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnAnimInstance();
		virtual ~CnAnimInstance();

		// Setup
		void	Setup( AnimDataPtr& animResource );

		// Name
		const CnString&	GetName() const;

		// Priority
		void		SetPriority( PRIORITY nPriority );
		PRIORITY	GetPriority() const;

		// Time
		SECOND		GetTotalTime() const;
		FRAME		GetTotalFrame() const;

		// FrameRate
		void		SetFrameRate( float fFrameRate );
		//FRAME		GetFramePerSecond() const;
		SECOND		GetSecondPerFrame() const;

		// State
		//void		Repeat( bool bEnable = true );
		//bool		IsRepeat() const;
		//bool		IsEnd() const;

		// 애니메이션 적용 범위
		void		Lock( const CnString& BeginNode, const CnString& EndNode = L"" );
		Locker::Result	IsLocked( const CnString& NodeName );

		// Get KeyFrame
		bool		GetKey( AnimKey* Output, const CnString& NodeName, SECOND CurrentTime );
	private:
		typedef std::vector<Locker>	LockerArray;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		// priority
		PRIORITY	m_Priority;

		// animation time
		SECOND		m_fTotalTime;

		// frame rate
		//FRAME		m_fFramePerSecond;		//!< 초당 프레임
		SECOND		m_fSecondPerFrame;		//!< 프레임당 시간(초)

		// motion
		AnimDataPtr	m_spAnimResource;

		// state
		//bool		m_bRepeat;
		//bool		m_bIsEnd;

		// Apply
		LockerArray	m_Lockers;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		void	GetPosKey( Export::AnimNode* KeyData, SECOND Time, Math::Vector3& Output );
		void	GetRotKey( Export::AnimNode* KeyData, SECOND Time, Math::Quaternion& Output );
		void	GetSclKey( Export::AnimNode* KeyData, SECOND Time, Math::Vector3& Output );
	};

	typedef Ptr<CnAnimInstance>		AnimInstPtr;

#include "CnAnimationInstance.inl"
}

#endif	// __CN_ANIMATION_INSTANCE_H__
