//================================================================
// File:               : CnSkeletonDataManager.cpp
// Related Header File : CnSkeletonDataManager.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 12
//================================================================
#include "../Cindy.h"
#include "CnSkeletonDataManager.h"
#include "CnSkeletonData.h"

namespace Cindy
{
	//================================================================
	__ImplementClass(Cindy, CnSkeletonDataManager, CnResourceManager);
	__ImplementSingleton(CnSkeletonDataManager);
	///Const/Dest
	CnSkeletonDataManager::CnSkeletonDataManager()
	{
		__ConstructSingleton;
	}
	CnSkeletonDataManager::~CnSkeletonDataManager()
	{
		__DestructSingleton;
	}

	//--------------------------------------------------------------
	/** 리소스 읽기
		@remarks
			리소스를 읽는다.
	*/
	//--------------------------------------------------------------
	RscPtr CnSkeletonDataManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = Get( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnSkeletonData* mesh = CnNew CnSkeletonData( this );
		if( mesh->Load( strFileName ) )
		{
			Add( mesh );
		}
		else
		{
			SAFEDEL( mesh );
		}

		return mesh;
	}

}