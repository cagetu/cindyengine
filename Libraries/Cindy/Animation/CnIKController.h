//================================================================
// File:           : CnIKController.h
// Original Author : changhee
// Creation Date   : 2008. 12. 31
//================================================================
#ifndef __CN_IK_CONTROLLER_H__
#define __CN_IK_CONTROLLER_H__

#include "../Scene/CnController.h"

namespace Cindy
{
	class CnIKObject;

	//================================================================
	/** Inverse Kinematic Controller
	    @author		changhee
		@brief		IK 처리
		@desc		Inverse Kinematics을 처리한다.
	*/
	//================================================================
	class CN_DLL CnIKController : public CnController
	{
		__DeclareClass(CnIKController);
	public:
		static const ControllerID ID = 6;

		class IKSolver : public CnRefCount
		{
		public:
			uint	priority;

			uint	maxCCDTries;
			Math::Vector3	lastGoalPosition;

			Ptr<CnIKObject>	goal;
			Ptr<CnIKObject>	endEffector;
			Ptr<CnIKObject>	finish;

			IKSolver();
			virtual ~IKSolver();

			bool	ComputeCCD();
		};

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnIKController();
		virtual ~CnIKController();

		// ID
		ControllerID	GetID() const override	{	return CnIKController::ID;	}

		void	Update() override;
		void	Clear() override;

		Ptr<IKSolver>	AddIKSolver( uint Priority );
		Ptr<IKSolver>	GetIKSolver( uint Priority );
		void		RemoveIKSolver( uint Priority );

		void	NeedUpdate( bool bNeed );
	private:
		typedef std::map<uint, Ptr<IKSolver> >	IKSolverMap;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		IKSolverMap		m_IKSolvers;
		bool			m_bNeedUpdate;
	};

	typedef Ptr<CnIKController::IKSolver>	IKSolverPtr;
	#include "CnIKController.inl"
}

#endif	// __CN_IK_CONTROLLER_H__