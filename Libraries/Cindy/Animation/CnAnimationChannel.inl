//================================================================
// File:               : CnAnimationChannel.inl
// Related Header File : CnAnimationChannel.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 4
//================================================================

//================================================================
//	Name
//================================================================
inline
void CnAnimChannel::SetName( const CnString& Name )
{
	m_Name = Name;
}
//----------------------------------------------------------------
inline
const CnString& CnAnimChannel::GetName() const
{
	return m_Name;
}

//================================================================
//	Motion
//================================================================
//----------------------------------------------------------------
inline
const AnimInstPtr&
CnAnimChannel::GetMotion() const
{
	return m_spCurrentMotion;
}

//================================================================
//	Weight
//================================================================
inline
void CnAnimChannel::SetMixtureRatio( float Ratio )
{
	m_fMixtureRatio = Ratio;
	
	if (m_fMixtureRatio < 0.0f)
		m_fMixtureRatio = 0.0f;

	if (m_fMixtureRatio > 1.0f)
		m_fMixtureRatio = 1.0f;
}
//----------------------------------------------------------------
inline
float CnAnimChannel::GetMixtureRatio() const
{
	return m_fMixtureRatio;
}

//================================================================
//	Current PlayState
//================================================================
inline
void CnAnimChannel::SetPlayState( CnAnimChannel::PlayState eState )
{
	m_CurrentPlayState = eState;
}
//----------------------------------------------------------------
inline
CnAnimChannel::PlayState
CnAnimChannel::GetPlayState() const
{
	return m_CurrentPlayState;
}

//================================================================
//	Parent
//================================================================
inline
void CnAnimChannel::_SetParent( CnAnimController* pParent )
{
	m_pParent = pParent;
}