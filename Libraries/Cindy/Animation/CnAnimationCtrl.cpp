//================================================================
// File:               : CnAnimationCtrl.cpp
// Related Header File : CnAnimationCtrl.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnAnimationCtrl.h"

#include "../Scene/CnXFormObject.h"

namespace Cindy
{
	//================================================================
	//defines
	CN_IMPLEMENT_RTTI( Cindy, CnAnimCtrl, CnFrameCtrl );
	//----------------------------------------------------------------
	ControllerID CnAnimCtrl::ms_nID = 0;

	/// Const/Dest
	CnAnimCtrl::CnAnimCtrl()
	{
	}
	CnAnimCtrl::~CnAnimCtrl()
	{
	}

	//----------------------------------------------------------------
	void CnAnimCtrl::Play()
	{
	}

	//----------------------------------------------------------------
	void CnAnimCtrl::Stop()
	{
	}

	//----------------------------------------------------------------
	void CnAnimCtrl::Reset()
	{
		Stop();
	}

	//----------------------------------------------------------------
	void CnAnimCtrl::Update()
	{
	}

	//----------------------------------------------------------------
	void CnAnimCtrl::SetAnimatedNodeCtrl( CnAnimatedNodeCtrl* pController )
	{
		m_AnimatedNodeCtrl = pController;
	}
}