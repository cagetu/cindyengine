//================================================================
// File:           : CnAnimationChannel.h
// Original Author : changhee
// Creation Date   : 2008. 12. 4
//================================================================
#ifndef __CN_ANIMATION_CHANNEL_H__
#define __CN_ANIMATION_CHANNEL_H__

#include "CnAnimationInstance.h"

namespace Cindy
{
	class CnAnimController;
	class CnTransformable;

	//================================================================
	/** AnimationChannel
	    @author    changhee
		@since     2007. 2. 1
		@brief	   애니메이션을 처리..
		@remarks   애니메이션 블랜딩까지 처리..
	*/
	//================================================================
	class CN_DLL CnAnimChannel : public CnObject
	{
		__DeclareClass(CnAnimChannel);
	public:
		enum PlayState
		{
			STOPPED = 0,
			PLAYING,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnAnimChannel();
		virtual ~CnAnimChannel();

		// Name
		void	SetName( const CnString& Name );
		const CnString&	GetName() const;

		// Motion
		void	SetMotion( AnimInstPtr& spMotion, bool bRepeat = true, float fBlendWeight = 0.0f, float fBlendRate = 1.0f );
		const AnimInstPtr&	GetMotion() const;

		// 갱신
		void	Update( SECOND fElapsedTime );

		// 제어
		void	Play();
		void	Stop();
		void	Reset();

		// State
		void	Repeat( bool bSet = true );
		bool	IsRepeat() const;
		bool	IsEnd() const;

		// PlayState
		void	SetPlayState( PlayState eState );
		PlayState	GetPlayState() const;

		// Weight
		void	SetMixtureRatio( float Ratio );
		float	GetMixtureRatio() const;

		// Animated key.
		bool	GetKey( CnTransformable* TargetNode, AnimKey* Output );
	private:
		friend class CnAnimController;

		//------------------------------------------------------
		// Internal Variables
		//------------------------------------------------------
		CnString			m_Name;			//!< Name
		CnAnimController*	m_pParent;		//!< Parent

		// motion
		AnimInstPtr	m_spCurrentMotion;
		
		// animation time
		SECOND		m_fCurrentTime;
		SECOND		m_fTotalTime;

		// state
		PlayState	m_CurrentPlayState;
		bool		m_bRepeat;
		bool		m_bIsEnd;

		// motion blend
		bool		m_bEnableBlending;
		float		m_fMotionBlendWeight;
		float		m_fMotionBlendRate;

		// Weight
		float		m_fMixtureRatio;

		//------------------------------------------------------
		// Internal Methods
		//------------------------------------------------------
		// Parent
		void	_SetParent( CnAnimController* pParent );

		// blend
		void	_StartBlending( float fBlendWeight, float fBlendRate );
		void	_EndBlending();

		// update
		void	_ComputeBlendWeight( SECOND fElapsedTime );
		void	_ComputeCurrentTime( SECOND fElaspedTime );

		void	_Animate();
	};

	#include "CnAnimationChannel.inl"
}

#endif	// __CN_ANIMATION_CHANNEL_H__