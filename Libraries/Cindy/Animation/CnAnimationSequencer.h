//================================================================
// File:           : CnAnimationSequencer.h
// Original Author : changhee
// Creation Date   : 2007. 2. 1
//================================================================
#ifndef __CN_ANIMATIONCONTROLLER_H__
#define __CN_ANIMATIONCONTROLLER_H__

#include "CnFrameController.h"
#include "CnAnimationChannel.h"
#include "CnAnimNodeContainer.h"

namespace Cindy
{
	class CnTransformable;

	//================================================================
	/** Animation Controller
	    @author    changhee
		@since     2007. 2. 1
		@brief	   애니메이션 제어기 
	*/
	//================================================================
	class CN_DLL CnAnimController : public CnFrameController
	{
		__DeclareClass(CnAnimController);
	public:
		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnAnimController();
		virtual ~CnAnimController();

		// 컨트롤러 ID 구하기
		ControllerID	GetID() const override;

		// Node
		void	SetContainer( CnAnimContainer* pContainer );
		const Ptr<CnAnimContainer>&	GetContainer() const;

		// Motion
		//void	SetMotion( AnimInstPtr& spMotion, bool bRepeat = true, float fBlendWeight = 0.0f, float fBlendRate = 1.0f );

		// Channel
		void	AddChannel( PRIORITY Priority, CnAnimChannel* pChannel );
		void	RemoveChannel( const CnString& Name );
		void	RemoveChannel( PRIORITY Priority );
		void	RemoveAllChannels();

		CnAnimChannel*	GetChannel( const CnString& Name );
		CnAnimChannel*	GetChannel( PRIORITY Priority );

		// Key
		//bool	GetKey( CnTransformable* TargetNode, AnimKey* Output );

		// 갱신
		void	Update() override;
		void	Clear() override;

		// 제어
		void	Play() override;
		void	Stop() override;
		void	Reset() override;
	private:
		static ControllerID	ms_nID;

		//------------------------------------------------------
		// Defines
		//------------------------------------------------------
		typedef std::map<PRIORITY, Ptr<CnAnimChannel>>	ChannelMap;
		typedef ChannelMap::iterator						ChannelIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		Ptr<CnAnimContainer>	m_NodeCtrl;

		// channels
		ChannelMap	m_Channels;

		// animation time
		TICK		m_ulPrevTick;
	};

#include "CnAnimationSequencer.inl"
}

#endif	// __CN_ANIMATIONCONTROLLER_H__