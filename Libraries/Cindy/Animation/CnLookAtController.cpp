//================================================================
// File:               : CnLookAtController.cpp
// Related Header File : CnLookAtController.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 26
//================================================================
#include "../Cindy.h"
#include "../Scene/CnNode.h"
#include "CnLookAtController.h"

namespace Cindy
{
	//================================================================
	__ImplementClass( Cindy, CnLookAtController, CnController );
	//----------------------------------------------------------------
	CnLookAtController::CnLookAtController()
	{
	}
	CnLookAtController::~CnLookAtController()
	{
		Clear();
	}

	//----------------------------------------------------------------
	/** @brief	모든 요소들을 제거 */
	//----------------------------------------------------------------
	void CnLookAtController::Clear()
	{
		m_TargetNode.SetNull();
	}

	//----------------------------------------------------------------
	/** @brief	타겟을 향해 회전 업데이트
		@todo	Gamebryo와도 비교해본다.
	*/
	//----------------------------------------------------------------
	void CnLookAtController::Update()
	{
		if (m_TargetNode.IsNull())
			return;

	}

	//----------------------------------------------------------------
	/** @brief	타겟 노드 설정 */
	//----------------------------------------------------------------
	void CnLookAtController::SetTarget( CnTransformable* Target ) 
	{
		m_TargetNode = Target;
	}
}
