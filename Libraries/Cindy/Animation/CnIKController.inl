//================================================================
// File:               : CnIKController.inl
// Related Header File : CnIKController.h
// Original Author     : changhee
//================================================================

inline
void CnIKController::NeedUpdate( bool bNeed )
{
	m_bNeedUpdate = bNeed;
}