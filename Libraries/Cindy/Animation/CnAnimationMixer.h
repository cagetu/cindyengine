//================================================================
// File:           : CnAnimationMixer.h
// Original Author : changhee
// Creation Date   : 2008. 12. 4
//================================================================
#ifndef __CN_ANIMATION_MIXER_H__
#define __CN_ANIMATION_MIXER_H__

namespace Cindy
{
	struct AnimKey;
	class CnTransformable;

	//================================================================
	/** AnimatedNode Controller
	    @author    changhee
		@brief	   KeyBlender
	*/
	//================================================================
	class CnAnimationMixer
	{
	public:
		// blend
		static void		Blend( AnimKey* Prev, AnimKey* Current, AnimKey* Output, float Delta );
	};
}

#endif	// __CN_ANIMATION_MIXER_H__