//================================================================
// File:               : CnAnimNodeContainer.cpp
// Related Header File : CnAnimNodeContainer.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 4
//================================================================
#include "Cindy.h"
#include "Scene/CnNode.h"

#include "CnAnimNodeContainer.h"
#include "CnAnimationSequencer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnAnimContainer, CnObject);
	// Const/Dest
	CnAnimContainer::CnAnimContainer()
		: m_Owner(0)
	{
	}
	CnAnimContainer::~CnAnimContainer()
	{
		RemoveAllNodes();
	}

	//================================================================
	/** Add Node
	    @remarks      노드 추가
	*/
	//================================================================
	bool CnAnimContainer::AddNode( CnTransformable* pNode, bool IsRoot )
	{
		pair<NodeIter, bool> result = m_Nodes.insert( NodeMap::value_type( pNode->GetID(), pNode ) );
		return result.second;
	}

	//================================================================
	/** Remove Node
	    @remarks      노드 제거
	*/
	//================================================================
	bool CnAnimContainer::RemoveNode( ushort usIndex )
	{
		NodeIter iter = m_Nodes.find( usIndex );
		if (iter == m_Nodes.end())
			return false;

		CnTransformable* node = iter->second;

		m_Nodes.erase( iter );
		return true;
	}

	//================================================================
	/** Remove All Nodes
	    @remarks      모든 노드 제거
	*/
	//================================================================
	void CnAnimContainer::RemoveAllNodes( bool bDelete )
	{
		m_Nodes.clear();
	}

	//================================================================
	/** Get Node
	    @remarks      노드 얻어오기
	*/
	//================================================================
	CnTransformable* CnAnimContainer::GetNode( ushort usIndex )
	{
		NodeIter iter = m_Nodes.find( usIndex );
		if (iter == m_Nodes.end())
			return NULL;

		return iter->second;
	}
	//================================================================
	CnTransformable* CnAnimContainer::GetNode( const CnString& Name )
	{
		NodeIter inend = m_Nodes.end();
		for (NodeIter in = m_Nodes.begin(); in != inend; ++in)
		{
			if (in->second->GetName() == Name)
				return in->second;
		}
		return NULL;
	}

	//================================================================
	/** Update Nodes
	    @remarks	노드 업데이트
	*/
	//================================================================
	void CnAnimContainer::_Animate( CnAnimChannel* pChannel )
	{
		float mixtureRatio = pChannel->GetMixtureRatio();
		if (mixtureRatio == 0.0f)
			return;

		AnimKey newKey;
		AnimKey prevKey;
		CnTransformable* node = NULL;

		NodeIter inend = m_Nodes.end();
		for (NodeIter in = m_Nodes.begin(); in != inend; ++in)
		{
			node = (in->second);

			// 현재 노드의 TM을 구한다.
			if (true == pChannel->GetKey( node, &newKey ))
			{
				if (mixtureRatio < 1.0f)
				{
					prevKey.pos = node->GetLocalPosition();
					prevKey.rot = node->GetLocalRotation();
					prevKey.scl = node->GetLocalScale();

					// 보간
					newKey.pos.Lerp( prevKey.pos, newKey.pos, mixtureRatio );
					newKey.rot.Slerp( prevKey.rot, newKey.rot, mixtureRatio );
					newKey.scl.Lerp( prevKey.scl, newKey.scl, mixtureRatio );
				}

				node->SetPosition( newKey.pos );
				node->SetRotation( newKey.rot );
				node->SetScale( newKey.scl );
			}
		}
	}
	//void CnAnimContainer::_Animate( CnAnimController* pController )
	//{
	//	AnimKey newKey;
	//	CnTransformable* node = NULL;

	//	NodeIter inend = m_Nodes.end();
	//	for (NodeIter in = m_Nodes.begin(); in != inend; ++in)
	//	{
	//		node = (in->second);

	//		// 현재 노드의 TM을 구한다.
	//		if (true == pController->GetKey( node, &newKey ))
	//		{
	//			node->SetPosition( newKey.pos );
	//			node->SetRotation( newKey.rot );
	//			node->SetScale( newKey.scl );
	//		}
	//	}
	//}

	//================================================================
	/** UpdateTransform
	    @remarks      노드 변환 업데이트
	*/
	//================================================================
	//void CnAnimContainer::_UpdateTransform()
	//{
	//	RootNodeList::iterator i, iend;
	//	iend = m_RootNodes.end();
	//	for (i = m_RootNodes.begin(); i != iend; ++i)
	//		(*i)->UpdateTransform( false, false );
	//}
}