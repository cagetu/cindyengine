//================================================================
// File:               : CnIKController.cpp
// Related Header File : CnIKController.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 31
//================================================================
#include "../Cindy.h"
#include "../Scene/CnNode.h"
#include "CnIKController.h"
#include "CnBoneNode.h"

namespace Cindy
{
	//================================================================
	//	Class CnIKController::IKSolver
	//================================================================
	CnIKController::IKSolver::IKSolver()
		: maxCCDTries(5)
	{
	}
	//----------------------------------------------------------------
	CnIKController::IKSolver::~IKSolver()
	{
		goal.SetNull();
		endEffector.SetNull();
		finish.SetNull();
	}

	//----------------------------------------------------------------
	/** @desc	Compute an IK Solution to an end effector position in 3D */
	//----------------------------------------------------------------
	bool CnIKController::IKSolver::ComputeCCD()
	{
		//Math::Vector3 goalPosition = goal->GetWorldPosition();
		//if (lastGoalPosition == goalPosition)
		//	return true;

		//lastGoalPosition = goalPosition;

		//! lastLink는 EndEffect의 상위 노드를 설정한다.
		CnIKObject* lastLink = DynamicCast<CnIKObject>(endEffector->GetParent());
		if (lastLink)
		{
			uint countOfTries = 0;
			do
			{
				if (true == lastLink->_ComputeIK( this, countOfTries ))
					return true;
			} while(countOfTries < maxCCDTries);
		}

		return false;
	}

	//================================================================
	__ImplementClass( Cindy, CnIKController, CnController );
	//----------------------------------------------------------------
	CnIKController::CnIKController()
		: m_bNeedUpdate(true)
	{
	}
	CnIKController::~CnIKController()
	{
		Clear();
	}

	//----------------------------------------------------------------
	/** @brief	모든 요소들을 제거 */
	//----------------------------------------------------------------
	void CnIKController::Clear()
	{
		m_IKSolvers.clear();
	}

	//----------------------------------------------------------------
	/** @brief	타겟을 향해 회전 업데이트
		@todo	World or Local 좌표 중 어느 것을 중심으로 할 것인지를 결정한다.
		@todo	Gamebryo와도 비교해본다.
	*/
	//----------------------------------------------------------------
	void CnIKController::Update()
	{
		if (m_IKSolvers.empty())
			return;

		CnBoneNode::NeedUpdateTransform( true );
		{
			IKSolverMap::iterator i, iend;
			iend = m_IKSolvers.end();
			for (i = m_IKSolvers.begin(); i!=iend; ++i)
			{
				(i->second)->ComputeCCD();
			}
		}
		CnBoneNode::NeedUpdateTransform( false );
	}

	//----------------------------------------------------------------
	/** @brief	IKSolver 추가 */
	//----------------------------------------------------------------
	Ptr<CnIKController::IKSolver>
	CnIKController::AddIKSolver( uint Priority )
	{
		IKSolverMap::iterator iter = m_IKSolvers.find( Priority );
		if (iter == m_IKSolvers.end())
		{
			IKSolver* newSolver = new IKSolver();
			newSolver->priority = Priority;

			m_IKSolvers.insert( IKSolverMap::value_type( Priority, newSolver ) );
			return newSolver;
		}

		return iter->second;
	}

	//----------------------------------------------------------------
	/** @brief	IKSolver 얻어오기 */
	//----------------------------------------------------------------
	Ptr<CnIKController::IKSolver>
	CnIKController::GetIKSolver( uint Priority )
	{
		IKSolverMap::iterator iter = m_IKSolvers.find( Priority );
		if (iter == m_IKSolvers.end())
		{
			return NULL;
		}

		return iter->second;
	}

	//----------------------------------------------------------------
	/** @brief	IKSolver 제거 */
	//----------------------------------------------------------------
	void CnIKController::RemoveIKSolver( uint Priority )
	{
		IKSolverMap::iterator iter = m_IKSolvers.find( Priority );
		if (iter != m_IKSolvers.end())
			m_IKSolvers.erase( iter );
	}
}
