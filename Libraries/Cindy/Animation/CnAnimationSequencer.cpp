//================================================================
// File:               : CnAnimationSequencer.cpp
// Related Header File : CnAnimationSequencer.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnAnimationSequencer.h"
#include "CnAnimNodeContainer.h"
#include "CnSkeletonInstance.h"

#include "CnBoneNode.h"

namespace Cindy
{
	ControllerID CnAnimController::ms_nID = 0;
	//================================================================
	//defines
	__ImplementClass( Cindy, CnAnimController, CnFrameController );
	//----------------------------------------------------------------
	/// Const/Dest
	CnAnimController::CnAnimController()
		: m_ulPrevTick(0)
	{
	}
	CnAnimController::~CnAnimController()
	{
		Clear();
	}

	//================================================================
	// Animation Channel
	//================================================================
	void CnAnimController::AddChannel( PRIORITY Priority, CnAnimChannel* pChannel )
	{
		pair<ChannelIter, bool> result = m_Channels.insert( ChannelMap::value_type( Priority, pChannel ) );
		if (result.second == false)
			return;

		pChannel->_SetParent( this );
	}

	//----------------------------------------------------------------
	void CnAnimController::RemoveChannel( const CnString& Name )
	{
		ChannelIter iend = m_Channels.end();
		for (ChannelIter i=m_Channels.begin(); i!=iend; ++i)
		{
			if (i->second->GetName() == Name)
			{
				i->second->_SetParent( 0 );
				m_Channels.erase( i );
				return;
			}
		}
	}
	void CnAnimController::RemoveChannel( PRIORITY Priority )
	{
		ChannelIter iter = m_Channels.find( Priority );
		if (iter == m_Channels.end())
			return;

		m_Channels.erase( iter );
	}

	//----------------------------------------------------------------
	void CnAnimController::RemoveAllChannels()
	{
		m_Channels.clear();
	}

	//----------------------------------------------------------------
	CnAnimChannel* CnAnimController::GetChannel( const CnString& Name )
	{
		ChannelIter iend = m_Channels.end();
		for (ChannelIter i=m_Channels.begin(); i!=iend; ++i)
		{
			if (i->second->GetName() == Name)
				return i->second;
		}

		return NULL;
	}
	CnAnimChannel* CnAnimController::GetChannel( PRIORITY Priority )
	{
		ChannelIter iter = m_Channels.find( Priority );
		if (iter == m_Channels.end())
			return NULL;

		return iter->second;
	}

	//================================================================
	//	Play/Stop
	//================================================================
	//----------------------------------------------------------------
	void CnAnimController::Play()
	{
		m_ulPrevTick = timeGetTime();

		_SetCurrentStatus( PLAY );
	}

	//----------------------------------------------------------------
	void CnAnimController::Stop()
	{
		_SetCurrentStatus( STOP );
	}

	//----------------------------------------------------------------
	void CnAnimController::Reset()
	{
		Stop();
	}

	//================================================================
	//	Update
	//================================================================
	//----------------------------------------------------------------
	void CnAnimController::Update()
	{
		if (PLAY != GetCurrentStatus())
			return;

		// 애니메이션 시간을 업데이트 한다.
		TICK current = timeGetTime();
		TICK elapsed = current - m_ulPrevTick;
		m_ulPrevTick = current;

		// tick을 second로 변환
		SECOND elapsedTime = (SECOND)elapsed * 0.001f;

		//struct Sample
		//{
		//	CnTransformable*	node;
		//	AnimKey			key;
		//};

		//Sample* sampleBuffer1 = new Sample[GetContainer()->Size()];
		//Sample* sampleBuffer2 = new Sample[GetContainer()->Size()];

		CnAnimChannel* channel = NULL;
		ChannelIter iend = m_Channels.end();
		for (ChannelIter i=m_Channels.begin(); i!=iend; ++i)
		{
			channel = i->second;
			channel->Update( elapsedTime );
		} // for (ChannelIter i=m_Channels.begin(); i!=iend; ++i)

		//GetContainer()->_UpdateNodes( this );
	}

	//----------------------------------------------------------------
	void CnAnimController::Clear()
	{
		RemoveAllChannels();
	}
}