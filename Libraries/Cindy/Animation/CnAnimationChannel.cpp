//================================================================
// File:               : CnAnimationChannel.cpp
// Related Header File : CnAnimationChannel.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnAnimationChannel.h"
#include "CnAnimationSequencer.h"
#include "CnAnimationMixer.h"
#include "CnAnimNodeContainer.h"
#include "CnBoneNode.h"
#include "CnAnimationKeyBuffer.h"

namespace Cindy
{
	//================================================================
	//defines
	__ImplementClass(Cindy, CnAnimChannel, CnObject);
	/// Const/Dest
	CnAnimChannel::CnAnimChannel()
		: m_CurrentPlayState(PLAYING)
		, m_bEnableBlending(false)
		, m_fMotionBlendWeight(1.0f)
		, m_fMotionBlendRate(0.0f)
		, m_fMixtureRatio(1.0f)
		, m_pParent(0)
	{
	}
	CnAnimChannel::~CnAnimChannel()
	{
	}

	//================================================================
	//	Motion
	//================================================================
	//----------------------------------------------------------------
	void CnAnimChannel::SetMotion( AnimInstPtr& spMotion, bool bRepeat, float fBlendWeight, float fBlendRate )
	{
		if (spMotion.IsNull())
			return;

		if (!m_spCurrentMotion.IsNull())
		{
			if (m_spCurrentMotion->GetName() != spMotion->GetName())
			{
				_StartBlending( fBlendWeight, fBlendRate );
			}
		}

		m_spCurrentMotion = spMotion;
		m_bRepeat = bRepeat;
		m_bIsEnd = false;

		m_fTotalTime = m_spCurrentMotion->GetTotalTime();
		m_fCurrentTime = 0.0f;
	}

	//================================================================
	//	Animation Blending
	//================================================================
	//----------------------------------------------------------------
	void CnAnimChannel::_StartBlending( float fBlendWeight, float fBlendRate )
	{
		m_bEnableBlending = true;
		m_fMotionBlendWeight = fBlendWeight;
		m_fMotionBlendRate = fBlendRate;
	}
	//----------------------------------------------------------------
	void CnAnimChannel::_EndBlending()
	{
		m_bEnableBlending = false;
		m_fMotionBlendWeight = 1.0f;
	}
	//----------------------------------------------------------------
	void CnAnimChannel::_ComputeBlendWeight( SECOND fElapsedTime )
	{
		if (false == m_bEnableBlending)
			return;

		m_fMotionBlendWeight += fElapsedTime * m_fMotionBlendRate;
		if (m_fMotionBlendWeight >= 1.0f)
		{
			_EndBlending();
		}
	}

	//================================================================
	//	Play/Stop
	//================================================================
	//----------------------------------------------------------------
	void CnAnimChannel::Play()
	{
		m_fCurrentTime = 0.0f;
		m_bIsEnd = false;

		SetPlayState( PLAYING );
	}

	//----------------------------------------------------------------
	void CnAnimChannel::Stop()
	{
		SetPlayState( STOPPED );
	}

	//----------------------------------------------------------------
	void CnAnimChannel::Reset()
	{
		Stop();

		_EndBlending();
	}

	//================================================================
	//	Update
	//================================================================
	//----------------------------------------------------------------
	void CnAnimChannel::Update( SECOND fElapsedTime )
	{
		if (PLAYING != GetPlayState())
			return;

		// 시간 갱신
		_ComputeBlendWeight( fElapsedTime );
		_ComputeCurrentTime( fElapsedTime );

		// 노드 갱신
		_Animate();
	}

	//----------------------------------------------------------------
	/** @brief	애니메이션을 처리하고, Node들을 업데이트 하자 */
	//----------------------------------------------------------------
	void CnAnimChannel::_Animate()
	{
		if (m_fMixtureRatio <= 0.0f)
			return;

		m_pParent->GetContainer()->_Animate( this );
	}

	//----------------------------------------------------------------
	/** @brief	현재 시간 갱신 */
	//----------------------------------------------------------------
	void CnAnimChannel::_ComputeCurrentTime( SECOND fElapsedTime )
	{
		m_fCurrentTime += fElapsedTime;
		if (m_fCurrentTime > m_fTotalTime)
		{
			if (m_bRepeat)
			{
				m_fCurrentTime = 0.0f;
			}
			else
			{
				m_fCurrentTime = m_fTotalTime;
				m_bIsEnd = true;

				SetPlayState( STOPPED );
			} // if
		} // if
	}

	//----------------------------------------------------------------
	/** @brief	Get Animation Key */
	//----------------------------------------------------------------
	bool CnAnimChannel::GetKey( CnTransformable* TargetNode, AnimKey* Output )
	{
		// 모션 블랜딩 끝
		if (m_fMotionBlendWeight == 1.0f)
		{
			// 현재 노드의 TM을 구한다.
			return m_spCurrentMotion->GetKey( Output, TargetNode->GetName(), this->m_fCurrentTime );
		}
		else
		{
			/** [choose one]
				1. 현재 애니메이션과 이전 애니메이션의 key 데이터를 섞는다.
				2. 현재 애니메이션의 key 데이터와 이전 프레임 key 데이터를 섞는다.
			*/
			AnimKey curKey;
			if (m_spCurrentMotion->GetKey( &curKey, TargetNode->GetName(), this->m_fCurrentTime ))
			{
				// 현재 상태
				AnimKey prevKey;
				prevKey.pos = TargetNode->GetLocalPosition();
				prevKey.rot = TargetNode->GetLocalRotation();
				prevKey.scl = TargetNode->GetLocalScale();

				// 보간
				CnAnimationMixer::Blend( &prevKey, &curKey, Output, this->m_fMotionBlendWeight );
				return true;
			}

			//AnimKey curKey;
			//if (false == m_spCurrentMotion->GetKey( &curKey, TargetNode->GetName(), this->m_fCurrentTime ))
			//	return false;

			//AnimKey prevKey;
			//if (false == m_spPrevMotion->GetKey( &prevKey, TargetNode->GetName(), this->m_fCurrentTime ))
			//	return false;

			//// 보간
			//CnAnimationMixer::Blend( &prevKey, &curKey, Output, this->m_fMotionBlendWeight );
			//return true;

		}

		return false;
	}
}