//================================================================
// File:               : CnSkeletonData.cpp
// Related Header File : CnSkeletonData.h
// Original Author     : changhee
// Creation Date       : 2007. 8. 29
//================================================================
#include "../Cindy.h"
#include "CnSkeletonData.h"
#include "CnSkeletonDataManager.h"

#include "Util/CnLog.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnSkeletonData, CnResource );

	//================================================================
	/** 생성자
		@remarks			이 리소스를 관리하는 매니져만 생성 가능하다.
		@param pParent		부모 관리자
	*/
	//================================================================
	CnSkeletonData::CnSkeletonData( CnResourceManager* pParent )
		: CnResource( pParent )
	{
	}
	CnSkeletonData::~CnSkeletonData()
	{
		Unload();
	}

	//================================================================
	/** OnLost
		@remarks	디바이스를 잃었을 때..
	*/
	//================================================================
	void CnSkeletonData::OnLost()
	{
	}

	//================================================================
	/** OnLost
		@remarks	디바이스를 복구 했을 때
	*/
	//================================================================
	void CnSkeletonData::OnRestore()
	{
	}

	//================================================================
	/** Load
	    @remarks      메쉬 로딩
		@param        strFileName : 파일 이름
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnSkeletonData::Load( const wchar* strFileName )
	{
		using namespace MoCommon;

		if (m_pParent->FindData( strFileName, m_DataMemStream ))
		{
			m_ulTotalDataSize = m_DataMemStream.GetSize();

			m_strName = strFileName;
			if( !LoadData() )
			{
				m_DataMemStream.Close();
				m_strName = L"";
				return false;
			}

			SetState( Loaded );

			return true;
		}

		CnError( ToStr(L"file not found: %s", strFileName) );
		return true;
	}

	//================================================================
	/** Unload
	    @remarks      메쉬 해제
		@param        none
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnSkeletonData::Unload()
	{
		RemoveAllBones();
		RemoveAllBoxes();

		SetState( Unloaded );
		return true;
	}

	//================================================================
	/** LoadData
	    @remarks      데이터 읽기 시작
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnSkeletonData::LoadData()
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = m_ulTotalDataSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_SKEL_MAGIC:
				{
					if (!ReadMagicNumber())
						return false;
				}
				break;

			case RD_SKEL_VERSION:
				{
					if (!ReadFileVersion())
						return false;
				}
				break;

			case RD_SKEL_BONELIST:
				ReadSkeleton( chunk.ulSize );
				break;

			case RD_SKEL_BBOXLIST:
				ReadBoundingBoxList( chunk.ulSize );
				break;

			case RD_SKEL_BONE_DEFINE_LIST:
				ReadBoneDefineList( chunk.ulSize );
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}

		return true;
	}

	//================================================================
	/**	Read Magic Number
		@remarks
			파일 고유 번호를 읽어온다.
	*/
	//================================================================
	bool CnSkeletonData::ReadMagicNumber()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if( RD_SKELFILE_MAGICNUM != result )
		{
			Assert( RD_SKELFILE_MAGICNUM == result, L"[CnSkeletonData::ReadMagicNumber] different file unique number" );
			return false;
		}

		return true;
	}

	//================================================================
	/** Read File Version
		@remarks
			파일 버전을 읽어온다.
	*/
	//================================================================
	bool CnSkeletonData::ReadFileVersion()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if( RD_SKELFILE_VERSION != result )
		{
			Assert( RD_SKELFILE_VERSION == result, L"[CnSkeletonData::ReadFileVersion] different file version" );
			return false;
		}

		return true;
	}

	//================================================================
	/** Read Skeleton
		@remarks
			뼈다구 정보들 읽기
	*/
	//================================================================
	bool CnSkeletonData::ReadSkeleton( ulong ulDataSize )
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_SKEL_BONE:
				{
					Export::BoneNode* bone = CnNew Export::BoneNode();

					BYTE type = 0;
					m_DataMemStream.Read( &type, sizeof(BYTE), 1 );
					switch (type)
					{
					case 0:
						break;
					case 1:
						bone->type = Export::Node::BONE;
						break;
					case 2:
						bone->type = Export::Node::DUMMY;
						break;
					}

					ulong ulLeftChunkSize = chunk.ulSize - sizeof(BYTE);
					ReadBone( ulLeftChunkSize, bone );

					Export::BoneNode* test = GetBone( bone->id );
					if (test)
					{
						int a = 1;
					}

					AddBone( bone );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}
		return true;
	}

	//================================================================
	/** Read Bone
		@remarks
			뼈다구 정보들 읽기
	*/
	//================================================================
	bool CnSkeletonData::ReadBone( ulong ulDataSize, Export::BoneNode* pBoneData )
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch( chunk.ulTag )
			{
			case RD_SKEL_BONE_ID:
				{	//! 부모 노드의 ID
					m_DataMemStream.Read( &pBoneData->id, sizeof(ushort), 1 );
				}
				break;

			case RD_SKEL_BONE_NAME:
				{
					ReadNameData( pBoneData->name );
				}
				break;

			case RD_SKEL_BONE_PARENTID:
				{	//! 본 노드 ID
					m_DataMemStream.Read( &pBoneData->parent_id, sizeof(ushort), 1 );
				}
				break;

			case RD_SKEL_BONE_PARENTNAME:
				{
					ReadNameData( pBoneData->parent_name );
				}
				break;

			case RD_SKEL_BONE_LOCALTM:
				{	//! 노드의 변환 행렬의 위치, 회전, 크기 정보 받아오기
					m_DataMemStream.Read( &pBoneData->local_pos, sizeof(float), 3 );
					m_DataMemStream.Read( &pBoneData->local_rot, sizeof(float), 4 );
					m_DataMemStream.Read( &pBoneData->local_scl, sizeof(float), 3 );
				}
				break;

			case RD_SKEL_BONE_WORLDTM:
				{	//! 3x4
					float matrix[12];
					m_DataMemStream.Read( matrix, sizeof(float), 12 );

					D3DXMATRIXA16 mat( matrix[0], matrix[1], matrix[2], 0.0f,
									   matrix[3], matrix[4], matrix[5], 0.0f,
									   matrix[6], matrix[7], matrix[8], 0.0f,
									   matrix[9], matrix[10], matrix[11], 1.0f );

					pBoneData->worldTM = mat;
				}
				break;

			case RD_SKEL_BONE_AABB:
				{	// BoneNode에 필요하면 나중에 추가하기 바람
					Math::Vector3 vPoint;
					m_DataMemStream.Read( &vPoint, sizeof(float), 3 );
					m_DataMemStream.Read( &vPoint, sizeof(float), 3 );
				}
				break;

			case RD_SKEL_BONE_OBB:
				{
					Math::Vector3 center;
					Math::Vector3 axis[3];
					float extent[3];

					m_DataMemStream.Read( &center, sizeof(float), 3 );

					ushort i = 0;
					for( i = 0; i < 3; ++i )
						m_DataMemStream.Read( &axis[i], sizeof(float), 3 );

					for( i = 0; i < 3; ++i )
						m_DataMemStream.Read( &extent[i], sizeof(float), 1 );
				}
				break;

			//@< 삭제요망 [2006-07-06] cagetu
			case RD_SKEL_BONE_DEFINE:
				{
					ushort type = 0;
					m_DataMemStream.Read( &type, sizeof(ushort), 1 );
				}
				break;
			//@>

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}

		return true;
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadBoundingBoxList( ulong ulDataSize )
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_SKEL_BBOX:
				{
					Export::BoundingBox* pBox = new Export::BoundingBox();
					ReadBoundingBox( chunk.ulSize, pBox );

					AddBoundingBox( pBox );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadBoundingBox( ulong ulDataSize, Export::BoundingBox* pBBox )
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_SKEL_BBOX_PARENTID:
				{
					m_DataMemStream.Read( &pBBox->parentID, sizeof(ushort), 1 );
				}
				break;

			case RD_SKEL_BBOX_PARENTNAME:
				{
					ReadNameData( pBBox->parentName );
				}
				break;

			case RD_SKEL_BBOX_OBB:
				{
					Math::Vector3 center;
					Math::Vector3 axis[3];
					float extent[3];

					m_DataMemStream.Read( &center, sizeof(float), 3 );

					ushort i = 0;
					for( i = 0; i < 3; ++i )
						m_DataMemStream.Read( &axis[i], sizeof(float), 3 );

					for( i = 0; i < 3; ++i )
						m_DataMemStream.Read( &extent[i], sizeof(float), 1 );

					pBBox->obb.SetCenter( center );
					pBBox->obb.SetAxis( axis );
					pBBox->obb.SetExtent( extent );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadBoneDefineList( ulong ulDataSize )
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_SKEL_DEFINE:
				{
					ushort index = 0;
					m_DataMemStream.Read( &index, sizeof(ushort), 1 );

					ushort boneid = 0;
					m_DataMemStream.Read( &boneid, sizeof(ushort), 1 );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}
	}

	//================================================================
	/** AddBone
	    @remarks
			본 데이터 추가
	*/
	//================================================================
	void CnSkeletonData::AddBone( Export::BoneNode* pBoneData )
	{
		pair<BoneDataIter, bool> result = m_BoneDatas.insert( BoneDataMap::value_type( pBoneData->id, pBoneData ) );
		assert( result.second == true );
	}
	void CnSkeletonData::RemoveAllBones()
	{
		BoneDataIter iend = m_BoneDatas.end();
		for( BoneDataIter i = m_BoneDatas.begin(); i != iend; ++i )
			delete (i->second);
		m_BoneDatas.clear();
	}
	Export::BoneNode* CnSkeletonData::GetBone( ushort usIndex )
	{
		BoneDataIter iter = m_BoneDatas.find( usIndex );
		if (iter == m_BoneDatas.end())
			return NULL;

		return iter->second;
	}

	//================================================================
	/** BoundingBox
	*/
	//================================================================
	void CnSkeletonData::AddBoundingBox( Export::BoundingBox* pBox )
	{
		m_BoundingBoxes.push_back( pBox );
	}
	void CnSkeletonData::RemoveAllBoxes()
	{
		std::for_each( m_BoundingBoxes.begin(), m_BoundingBoxes.end(), DeleteObject() );
	}

	//================================================================
	/** Read Name Data
	    @remarks      이름 정보를 읽어온다.
	*/
	//================================================================
	void CnSkeletonData::ReadNameData( CnString& rResult )
	{
		ushort length = 0;
		m_DataMemStream.Read( &length, sizeof(ushort), 1 );
		if( length > 0 )
		{
			wchar buffer[128];
			memset( buffer, 0, sizeof(wchar) * 128 );
			m_DataMemStream.Read( buffer, sizeof(wchar), (ulong)length );

			rResult = buffer;
		}
	}

	//================================================================
	/** Get OBB
	    @remarks    OBB 구하기
	*/
	//================================================================
	const Math::OBB& CnSkeletonData::GetOBB( ushort usIndex )
	{
		assert( usIndex < (ushort)m_BoundingBoxes.size() );
		return m_BoundingBoxes[usIndex]->obb;
	}
}