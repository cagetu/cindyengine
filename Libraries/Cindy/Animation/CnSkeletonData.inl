//================================================================
// File:               : CnSkeletonData.inl
// Related Header File : CnSkeletonData.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 16
//================================================================
//----------------------------------------------------------------
//	SkeletonData Class
//----------------------------------------------------------------
inline
const CnSkeletonData::BoneDataMap& CnSkeletonData::GetBones() const
{
	return m_BoneDatas;
}

//----------------------------------------------------------------
//	SkelDataPtr Class
//----------------------------------------------------------------
inline
SkelDataPtr::SkelDataPtr()
: Ptr< CnSkeletonData >()
{
}

//----------------------------------------------------------------
inline
SkelDataPtr::SkelDataPtr( CnSkeletonData* pObject )
: Ptr< CnSkeletonData >( pObject )
{
}

//----------------------------------------------------------------
inline
SkelDataPtr::SkelDataPtr( const SkelDataPtr& spObject )
: Ptr< CnSkeletonData >( spObject )
{
}

//----------------------------------------------------------------
inline
SkelDataPtr::SkelDataPtr( const RscPtr& spRsc )
: Ptr< CnSkeletonData >()
{
	m_pObject = (CnSkeletonData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->AddRef();
	}
}

//----------------------------------------------------------------
inline
SkelDataPtr& SkelDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnSkeletonData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->Release();

		if( !spRsc.IsNull() )
			spRsc->AddRef();

		m_pObject = (CnSkeletonData*)spRsc.GetPtr();
	}

	return *this;
}