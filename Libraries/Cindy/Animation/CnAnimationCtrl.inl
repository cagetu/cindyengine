//================================================================
// File:               : CnAnimationCtl.inl
// Related Header File : CnAnimationCtl.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 2
//================================================================
//----------------------------------------------------------------
inline
ControllerID CnAnimCtrl::GetID() const
{
	return ms_nID;
}
