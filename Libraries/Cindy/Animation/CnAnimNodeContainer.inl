//================================================================
// File:               : CnAnimNodeContainer.inl
// Related Header File : CnAnimNodeContainer.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 4
//================================================================
//----------------------------------------------------------------
inline
void CnAnimContainer::SetOwner( CnNode* Owner )
{
	m_Owner = Owner;
}

//----------------------------------------------------------------
inline
const CnAnimContainer::NodeMap&
CnAnimContainer::GetNodes() const
{
	return m_Nodes;
}

//----------------------------------------------------------------
/** @brief		Node 갯수
	@remakrs	사용 빈도가 높다면, m_Nodes.size()를 바꾸자.
*/
//----------------------------------------------------------------
inline
uint CnAnimContainer::Size()
{
	return (uint)m_Nodes.size();
}
