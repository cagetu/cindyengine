//================================================================
// File:               : CnSkeletonInstance.cpp
// Related Header File : CnSkeletonInstance.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnSkeletonInstance.h"
#include "CnAnimationChannel.h"
#include "CnBoneNode.h"

#include "Util/CnLog.h"
#include "Graphics/Dx9/CnDx9Renderer.h"

//#define _LOG_SKELETON_

namespace Cindy
{
	//================================================================
	__ImplementClass( Cindy, CnSkeletonInstance, CnAnimContainer );
	//----------------------------------------------------------------
	/// Const/Dest
	CnSkeletonInstance::CnSkeletonInstance()
		: m_bNeedUpdate( false )
	{
	}
	CnSkeletonInstance::~CnSkeletonInstance()
	{
		Clear();

		m_Owner = 0;
	}

	//----------------------------------------------------------------
	/** Setup
		@brief	데이터 설정
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::Setup( const SkelDataPtr& SkeletonResource )
	{
		m_spResource = SkeletonResource;

		Math::Matrix44 invTM;
		bool hasParent = false;

		Export::BoneNode* bone = NULL;
		CnBoneNode* boneNode = NULL;
		CnBoneNode* parentBone = NULL;

#ifdef _LOG_SKELETON_
		CnPrint( L"[SkeletonInstance::Setup]" );
#endif

		//! 바이패드 정보 세팅
		const CnSkeletonData::BoneDataMap BoneDatas = m_spResource->GetBones();
		CnSkeletonData::BoneDataConstIter i, iend;
		iend = BoneDatas.end();
		for (i = BoneDatas.begin(); i != iend; ++i)
		{
			bone = (i->second);

			if (bone->HasParent())
			{	//! 부모가 있다면,
				parentBone = DynamicCast<CnBoneNode>( m_Owner->GetChild( bone->parent_name, true ) );
				if (parentBone)
				{
					boneNode = CnNew CnBoneNode( bone->name.c_str(), bone->id );
					boneNode->SetType( CnBoneNode::Child );
					parentBone->AttachChild( boneNode );

#ifdef _LOG_SKELETON_
					CnPrint( L"\t[Parent:%s] name: %s", bone->parent_name.c_str(), bone->name.c_str() );
#endif
				}
			}
			else
			{	//! 부모가 없다면...
				boneNode = CnNew CnBoneNode( bone->name.c_str(), bone->id );
				boneNode->SetType( CnBoneNode::Root );
				m_Owner->AttachChild( boneNode );

#ifdef _LOG_SKELETON_
				CnPrint( L"[Root] name: %s", bone->name.c_str() );
#endif
			}
			if (!boneNode)
				continue;

			//! 노드 정보 세팅
			bone->worldTM.Inverse( invTM );
			boneNode->SetInverseTransfrom( invTM );
			boneNode->SetPosition( bone->local_pos );
			boneNode->SetRotation( bone->local_rot );
			boneNode->SetScale( bone->local_scl );

			if (boneNode->GetType() != CnBoneNode::Root)
			{
				if (0 == boneNode->GetNumOfChildren())
				{
					boneNode->SetType( CnBoneNode::Leaf );
				}
				else
				{
					boneNode->SetType( CnBoneNode::Child );
				}
			}

			//! 노드 설정
			this->AddNode( boneNode, !bone->HasParent() );
		}
	}

	//----------------------------------------------------------------
	/** Add Node
	    @brief      노드 추가
	*/
	//----------------------------------------------------------------
	bool CnSkeletonInstance::AddNode( CnTransformable* pNode, bool IsRoot )
	{
		bool success = CnAnimContainer::AddNode( pNode, IsRoot );
		if (success)
		{
			CnBoneNode* bone = DynamicCast<CnBoneNode>(pNode);
			bone->SetSkeleton( this );

			if (IsRoot)
				m_RootNodes.push_back( bone );
			return true;
		}
		return false;
	}

	//----------------------------------------------------------------
	/** Remove Node
	    @brief      노드 제거
	*/
	//----------------------------------------------------------------
	bool CnSkeletonInstance::RemoveNode( ushort usIndex )
	{
		NodeIter iter = m_Nodes.find( usIndex );
		if (iter == m_Nodes.end())
			return false;

		CnTransformable* node = iter->second;
		CnBoneNode* bone = DynamicCast<CnBoneNode>(node);
		bone->SetSkeleton( 0 );

		m_Nodes.erase( iter );

		RootNodeArray::iterator ir = std::find( m_RootNodes.begin(), m_RootNodes.end(), bone );
		if (ir != m_RootNodes.end())
			m_RootNodes.erase( ir );

		return true;
	}

	//----------------------------------------------------------------
	/** Remove All Nodes
	    @brief      모든 노드 제거
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::RemoveAllNodes( bool bDelete )
	{
		bool deleted = false;
		if (bDelete)
		{
			RootNodeArray::iterator irend = m_RootNodes.end();
			for (RootNodeArray::iterator ir = m_RootNodes.begin(); ir != irend; ++ir)
			{
				m_Owner->DeleteChild( (*ir)->GetName() );
				deleted = true;
			}
		}
		m_RootNodes.clear();

		if (bDelete && !deleted)
		{
			CnBoneNode* bone = NULL;
			NodeIter iend = m_Nodes.end();
			for (NodeIter i = m_Nodes.begin(); i != iend; ++i)
			{
				//bone = StaticCast<CnBoneNode>(i->second.GetPtr());
				bone = i->second.Cast<CnBoneNode>();
				bone->SetSkeleton(0);
			}
		}
		m_Nodes.clear();
	}

	//----------------------------------------------------------------
	/**	@brief	애니메이션 적용된 Node들 업데이트 시키자
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::_Animate( CnAnimChannel* pChannel )
	{
		//CnAnimContainer::_Animate( pChannel );
		CnBoneNode* node = NULL;

		RootNodeArray::iterator i, iend;
		iend = m_RootNodes.end();
		for (i=m_RootNodes.begin(); i!=iend; ++i)
		{
			node = (*i);
			node->_Animate( pChannel );
		}
	}
	//void CnSkeletonInstance::_Animate( CnAnimController* pController )
	//{
	//	//CnAnimContainer::_UpdateNodes( pController );
	//	CnBoneNode* node = NULL;

	//	RootNodeArray::iterator i, iend;
	//	iend = m_RootNodes.end();
	//	for (i=m_RootNodes.begin(); i!=iend; ++i)
	//	{
	//		node = (*i);
	//		node->_UpdateNodes( pController );
	//	}
	//}

	//----------------------------------------------------------------
	/** _UpdateTransform
	    @brief		노드 변환 업데이트
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::_UpdateTransform()
	{
		RootNodeArray::iterator i, iend;
		iend = m_RootNodes.end();
		for (i = m_RootNodes.begin(); i != iend; ++i)
			(*i)->UpdateTransform( false, false );
	}

	//----------------------------------------------------------------
	/** _UpdateBounds
	    @brief		Custom하게 설정되어 있는 바운딩 박스가 있다면, 업데이트
		@param		none
		@return		none
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::_UpdateBounds()
	{
		OBBIter i, iend;
		iend = m_OBBs.end();

		ushort idx = 0;
		Math::Matrix44 mat;
		Math::OBB obb;
		for( i = m_OBBs.begin(); i != iend; ++i )
		{
			mat = (*i)->boneNode->GetWorldXForm();
			if( (*i)->lastXForm != mat )
			{
				obb = m_spResource->GetOBB( idx );
				obb.Transform( &((*i)->OBB), &mat );
				(*i)->lastXForm = mat;
			}

			++idx;
		}
	}

	//----------------------------------------------------------------
	/** Update
	    @brief		관리하고 있는 뼈대를 업데이트 한다.
		@param		none
		@return		none
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::Update()
	{
		CnBoneNode::NeedUpdateTransform( true );

		_UpdateTransform();
		_UpdateBounds();

		CnBoneNode::NeedUpdateTransform( false );
	}

	//----------------------------------------------------------------
	/** Clear
	    @brief	모두 제거
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::Clear()
	{
		RemoveAllNodes( true );

		m_OBBs.clear();
	}

	//----------------------------------------------------------------
	/** @brief	Draw
	*/
	//----------------------------------------------------------------
	void CnSkeletonInstance::DrawForDebug()
	{
		LPDIRECT3DDEVICE9 pDevice;
		RenderDevice->GetCurrentDevice( &pDevice );

		Math::Matrix44 mat;
		D3DXMatrixIdentity( &mat );
		pDevice->SetTransform( D3DTS_WORLD, &mat );

		//!
		struct Vertex 
		{ 
 			Math::Vector3	v;
			DWORD			color;

			enum { FVF = D3DFVF_XYZ | D3DFVF_DIFFUSE };
		}; 

		pDevice->SetFVF( Vertex::FVF );

		Vertex line[2];
		NodeIter iend = m_Nodes.end();
		for( NodeIter i = m_Nodes.begin(); i != iend; ++i )
		{
			CnTransformable* node = (i->second);

			Export::BoneNode* boneData = m_spResource->GetBone( node->GetID() );
			if( boneData->HasParent() )
			{
				CnTransformable* parent = GetNode( boneData->parent_id );

				Math::Vector3 vScale;

				CnBoneNode* bone = (CnBoneNode*)node;

				DWORD color = 0xFF0000FF;
				//if (node->GetName() == L"Bip01 Head")
				//{
				//	color = 0xFFFF0000;
				//}

				line[0].v = parent->GetWorldPosition();
				line[0].color = 0xFF00FF00;
				line[1].v = node->GetWorldPosition();
				line[1].color = color;

				pDevice->DrawPrimitiveUP( D3DPT_LINESTRIP, 1, line, sizeof( Vertex ) );

				pDevice->DrawPrimitiveUP( D3DPT_POINTLIST, 2, line, sizeof( Math::Vector3 ) );
				//pDevice->DrawPrimitiveUP( D3DPT_POINTLIST, 1, &line[0], sizeof( Math::Vector3 ) );
				//pDevice->DrawPrimitiveUP( D3DPT_POINTLIST, 1, &line[1], sizeof( Math::Vector3 ) );
			}
			else
			{
				Math::Vector3 vPoint = node->GetWorldPosition();
				pDevice->DrawPrimitiveUP( D3DPT_POINTLIST, 1, &vPoint, sizeof( Math::Vector3 ) );
			}
		}

	}
}