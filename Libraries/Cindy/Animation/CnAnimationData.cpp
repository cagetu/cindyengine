//================================================================
// File:               : CnAnimationData.cpp
// Related Header File : CnAnimationData.h
// Original Author     : changhee
// Creation Date       : 2007. 8. 29
//================================================================
#include "../Cindy.h"
#include "CnAnimationData.h"
#include "CnAnimationDataManager.h"
#include "CnAnimationInstance.h"

#include "../Util/CnLog.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnAnimationData, CnResource );

	//================================================================
	/** 생성자
		@brief			이 리소스를 관리하는 매니져만 생성 가능하다.
		@param pParent		부모 관리자
	*/
	//================================================================
	CnAnimationData::CnAnimationData( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_usStartFrame(0)
		, m_usEndFrame(0)
		, m_usTotalFrame(0)
		, m_ulFrameRate(0)
		, m_ulPriority(0)
	{
	}
	CnAnimationData::~CnAnimationData()
	{
		Unload();
	}

	//================================================================
	/** @brief	인스턴스 생성 */
	//================================================================
	CnAnimInstance* CnAnimationData::CreateInstance()
	{
		CnAnimInstance* newInst = CnAnimInstance::Create();
		newInst->Setup( AnimDataPtr(this) );
		return newInst;
	}

	//================================================================
	/** OnLost
		@brief	디바이스를 잃었을 때..
	*/
	//================================================================
	void CnAnimationData::OnLost()
	{
	}

	//================================================================
	/** OnLost
		@brief	디바이스를 복구 했을 때
	*/
	//================================================================
	void CnAnimationData::OnRestore()
	{
	}

	//================================================================
	/** Load
	    @brief      메쉬 로딩
		@param        strFileName : 파일 이름
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnAnimationData::Load( const wchar* strFileName )
	{
		using namespace MoCommon;

		if (m_pParent->FindData( strFileName, m_DataMemStream ))
		{
			m_ulTotalDataSize = m_DataMemStream.GetSize();

			m_strName = strFileName;
			if( !LoadData() )
			{
				m_DataMemStream.Close();
				m_strName = L"";
				return false;
			}

			SetState( Loaded );

			return true;
		}

		CnError( ToStr(L"file not found: %s", strFileName) );
		return true;
	}

	//================================================================
	/** Unload
	    @brief      메쉬 해제
		@param        none
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnAnimationData::Unload()
	{
		NodeDataIter iend = m_NodeDatas.end();
		for( NodeDataIter i = m_NodeDatas.begin(); i != iend; ++i )
			delete (i->second);
		m_NodeDatas.clear();

		m_usNumNodeDatas = 0;

		SetState( Unloaded );
		return true;
	}

	//================================================================
	/** @brief	Load Data */
	//================================================================
	bool CnAnimationData::LoadData()
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = m_ulTotalDataSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_ANIM_MAGIC:
				{
					if (!ReadMagicNumber())
						return false;
				}
				break;

			case RD_ANIM_VERSION:
				{
					if (!ReadFileVersion())
						return false;
				}
				break;

			case RD_ANIM_TOTALFRAME:
				{
					m_DataMemStream.Read( &m_usTotalFrame, sizeof(ushort), 1 );

					m_usStartFrame = 0;
					m_usEndFrame = m_usTotalFrame;
				}
				break;

			case RD_ANIM_FRAMERATE:
				{
					m_DataMemStream.Read( &m_ulFrameRate, sizeof(ulong), 1 );
				}
				break;

			case RD_ANIM_NODELIST:
				{
					ReadAnim( chunk.ulSize );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}

		return true;
	}
	
	//================================================================
	/**	Read Magic Number
		@brief
			파일 고유 번호를 읽어온다.
	*/
	//================================================================
	bool CnAnimationData::ReadMagicNumber()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if ( RD_ANIMFILE_MAGICNUM != result )
		{
			Assert( RD_SKELFILE_MAGICNUM == result, L"[CnAnimationData::ReadMagicNumber] different file unique number" );
			return false;
		}

		return true;
	}

	//================================================================
	/** Read File Version
		@brief
			파일 버전을 읽어온다.
	*/
	//================================================================
	bool CnAnimationData::ReadFileVersion()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if ( RD_ANIMFILE_VERSION != result )
		{
			Assert( RD_SKELFILE_VERSION == result, L"[CnAnimationData::ReadFileVersion] different file version" );
			return false;
		}

		return true;
	}

	//------------------------------------------------------------------
	void CnAnimationData::ReadAnim( ulong ulDataSize )
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_ANIM_NODE:
				{
					Export::AnimNode* pNode = CnNew Export::AnimNode();
					ReadAnimNode( chunk.ulSize, pNode );
					if (false == AddNodeData( pNode ))
					{
						delete pNode;
					}
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - MoCommon::ChunkHeaderL::HeaderSize );
			}

			leftsize -= chunk.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnAnimationData::ReadAnimNode( ulong ulDataSize, Export::AnimNode* pNode )
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if (!m_DataMemStream.ReadChunkHeaderL( &chunk ))
				break;

			switch (chunk.ulTag)
			{
			case RD_ANIM_NODE_ID:
				{
					m_DataMemStream.Read( &pNode->id, sizeof(ushort), 1 );
				}
				break;

			case RD_ANIM_NODE_NAME:
				{
					ReadNameData( pNode->name );
				}
				break;

			case RD_ANIM_NODE_POSTRACK:
				{
					ReadPosTrack( chunk.ulSize, pNode );
				}
				break;

			case RD_ANIM_NODE_ROTTRACK:
				{
					ReadRotTrack( chunk.ulSize, pNode );
				}
				break;

			case RD_ANIM_NODE_SCLTRACK:
				{
					ReadSclTrack( chunk.ulSize, pNode );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - MoCommon::ChunkHeaderL::HeaderSize );
			}

			leftsize -= chunk.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnAnimationData::ReadPosTrack( ulong ulDataSize, Export::AnimNode* pNode )
	{
		ushort count = 0;
		m_DataMemStream.Read( &count, sizeof(ushort), 1 );

		pNode->numPosKeys = count;
		pNode->posKeys = new Export::LinearKey[ count ];
		for ( ushort i = 0; i < count; ++i )
		{
			m_DataMemStream.Read( &pNode->posKeys[i].frame, sizeof(ushort), 1 );
			m_DataMemStream.Read( &pNode->posKeys[i].value, sizeof(float), 3 );
		}
	}

	//------------------------------------------------------------------
	void CnAnimationData::ReadRotTrack( ulong ulDataSize, Export::AnimNode* pNode )
	{
		ushort count = 0;
		m_DataMemStream.Read( &count, sizeof(ushort), 1 );

		pNode->numRotKeys = count;
		pNode->rotKeys = new Export::RotKey[ count ];
		for ( ushort i = 0; i < count; ++i )
		{
			m_DataMemStream.Read( &pNode->rotKeys[i].frame, sizeof(ushort), 1 );
			m_DataMemStream.Read( &pNode->rotKeys[i].value, sizeof(float), 4 );
		}
	}

	//------------------------------------------------------------------
	void CnAnimationData::ReadSclTrack( ulong ulDataSize, Export::AnimNode* pNode )
	{
		ushort count = 0;
		m_DataMemStream.Read( &count, sizeof(ushort), 1 );

		pNode->numScaleKeys = count;
		pNode->scaleKeys = new Export::LinearKey[ count ];
		for ( ushort i = 0; i < count; ++i )
		{
			m_DataMemStream.Read( &pNode->scaleKeys[i].frame, sizeof(ushort), 1 );
			m_DataMemStream.Read( &pNode->scaleKeys[i].value, sizeof(float), 3 );
		}
	}
	//================================================================
	/** Read Name Data
	    @brief      이름 정보를 읽어온다.
	*/
	//================================================================
	void CnAnimationData::ReadNameData( CnString& rResult )
	{
		ushort length = 0;
		m_DataMemStream.Read( &length, sizeof(ushort), 1 );
		if( length > 0 )
		{
			wchar buffer[128];
			memset( buffer, 0, sizeof(wchar) * 128 );
			m_DataMemStream.Read( buffer, sizeof(wchar), (ulong)length );

			rResult = buffer;
		}
	}

	//================================================================
	/** Add NodeData
		@brief	애니메이션 노드 데이터 추가하기	
	*/
	//================================================================
	bool CnAnimationData::AddNodeData( Export::AnimNode* pNode )
	{
		pair<NodeDataIter, bool> result = m_NodeDatas.insert( NodeDataMap::value_type( pNode->name, pNode ) );
#ifdef _DEBUG
		assert( result.second == true );
#endif
		if (result.second == false)
		{
			CnError( ToStr(L"[CnAnimationData::AddNodeData] Alreay exist %s", pNode->name.c_str()) );
			return false;
		}

		++m_usNumNodeDatas;
		return true;
	}

	//================================================================
	/** GetNodeData
		@brief	애니메이션 노드 데이터 얻어오기	
	*/
	//================================================================
	Export::AnimNode* CnAnimationData::GetNodeData( const CnString& NodeName )
	{
		NodeDataIter iter = m_NodeDatas.find( NodeName );
		if (iter == m_NodeDatas.end())
			return NULL;

		return iter->second;
	}

	////------------------------------------------------------------------
	//bool CnAnimationData::Save( const CnString& strFileName )
	//{
	//	char filename[256];

	//	WideCharToMultiByte( CP_ACP, 0, strFileName.c_str(), -1, filename, 256, NULL, NULL );
	//	FILE* pFile = fopen( filename, "wb" );
	//	if( NULL == pFile )
	//		return false;

	//	ulong ulBegin, ulPtr;

	//	ulong ulNum = RD_ANIMFILE_MAGICNUM;
	//	YoChunk::ChunkBegin( pFile, RD_ANIM_MAGIC, ulPtr, ulBegin );
	//	{
	//		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	//	}
	//	YoChunk::ChunkEnd( pFile, ulPtr, ulBegin );

	//	ulNum = RD_ANIMFILE_VERSION;
	//	YoChunk::ChunkBegin( pFile, RD_ANIM_VERSION, ulPtr, ulBegin );
	//	{
	//		fwrite( &ulNum, sizeof(ulong), 1, pFile );
	//	}
	//	YoChunk::ChunkEnd( pFile, ulPtr, ulBegin );

	//	YoChunk::ChunkBegin( pFile, RD_ANIM_TOTALFRAME, ulPtr, ulBegin );
	//	{
	//		fwrite( &m_usTotalFrame, sizeof(ushort), 1, pFile );
	//	}
	//	YoChunk::ChunkEnd( pFile, ulPtr, ulBegin );

	//	YoChunk::ChunkBegin( pFile, RD_ANIM_FRAMERATE, ulPtr, ulBegin );
	//	{
	//		fwrite( &m_ulFrameRate, sizeof(ulong), 1, pFile );
	//	}
	//	YoChunk::ChunkEnd( pFile, ulPtr, ulBegin );

	//	YoChunk::ChunkBegin( pFile, RD_ANIM_NODELIST, ulPtr, ulBegin );
	//	{
	//		WriteNodes( pFile );
	//	}
	//	YoChunk::ChunkEnd( pFile, ulPtr, ulBegin );

	//	fclose( pFile );

	//	return true;
	//}

	////------------------------------------------------------------------
	//void CnAnimationData::WriteNodes( FILE* pFile )
	//{
	//	ulong ulPtr, ulBegin, ulSubPtr, ulSubBegin;
	//	
	//	YoAniNodeData* pNode = NULL;
	//	
	//	AniNodeDataIter iend = m_AniNodeDatas.end();
	//	for( AniNodeDataIter i = m_AniNodeDatas.begin(); i != iend; ++i )
	//	{
	//		pNode = (i->second);
	//		
	//		YoChunk::ChunkBegin( pFile, RD_ANIM_NODE, ulPtr, ulBegin );
	//		{
	//			YoChunk::ChunkBegin( pFile, RD_ANIM_NODE_ID, ulSubPtr, ulSubBegin );
	//			{
	//				ushort id = pNode->GetID();
	//				fwrite( &id, sizeof(ushort), 1, pFile );
	//			}
	//			YoChunk::ChunkEnd( pFile, ulSubPtr, ulSubBegin );

	//			YoChunk::ChunkBegin( pFile, RD_ANIM_NODE_NAME, ulSubPtr, ulSubBegin );
	//			{
	//				CnString name = pNode->GetName();
	//				ushort len = (ushort)name.size();

	//				fwrite( &len, sizeof(ushort), 1, pFile );
	//				fwrite( name.c_str(), sizeof(WCHAR), len, pFile );
	//			}
	//			YoChunk::ChunkEnd( pFile, ulSubPtr, ulSubBegin );

	//			ushort usKeyCount = 0;
	//			YoChunk::ChunkBegin( pFile, RD_ANIM_NODE_POSTRACK, ulSubPtr, ulSubBegin );
	//			{
	//				usKeyCount = pNode->GetPosKeyCount();

	//				fwrite( &usKeyCount, sizeof(ushort), 1, pFile );

	//				PosKey* pPos = NULL;
	//				for( ushort i = 0; i < usKeyCount; ++i )
	//				{
	//					pPos = pNode->GetPosKey(i);
	//					
	//					fwrite( &pPos->frame, sizeof(ushort), 1, pFile );
	//					fwrite( &pPos->value, sizeof(float), 3, pFile );
	//				}
	//			}
	//			YoChunk::ChunkEnd( pFile, ulSubPtr, ulSubBegin );

	//			YoChunk::ChunkBegin( pFile, RD_ANIM_NODE_ROTTRACK, ulSubPtr, ulSubBegin );
	//			{
	//				usKeyCount = pNode->GetRotKeyCount();

	//				fwrite( &usKeyCount, sizeof(ushort), 1, pFile );

	//				RotKey* pRot = NULL;
	//				for( ushort i = 0; i < usKeyCount; ++i )
	//				{
	//					pRot = pNode->GetRotKey(i);
	//					
	//					fwrite( &pRot->frame, sizeof(ushort), 1, pFile );
	//					fwrite( &pRot->value, sizeof(float), 4, pFile );
	//				}
	//			}
	//			YoChunk::ChunkEnd( pFile, ulSubPtr, ulSubBegin );

	//			YoChunk::ChunkBegin( pFile, RD_ANIM_NODE_SCLTRACK, ulSubPtr, ulSubBegin );
	//			{
	//				usKeyCount = pNode->GetScalekeyCount();

	//				fwrite( &usKeyCount, sizeof(ushort), 1, pFile );

	//				ScaleKey* pScale = NULL;
	//				for( ushort i = 0; i < usKeyCount; ++i )
	//				{
	//					pScale = pNode->GetScaleKey(i);

	//					fwrite( &pScale->frame, sizeof(ushort), 1, pFile );
	//					fwrite( &pScale->value, sizeof(float), 3, pFile );
	//				}
	//			}
	//			YoChunk::ChunkEnd( pFile, ulSubPtr, ulSubBegin );

	//		}
	//		YoChunk::ChunkEnd( pFile, ulPtr, ulBegin );
	//	}
	//}
} // end of namespace