//================================================================
// File:           : CnSkeletonData.h
// Original Author : changhee
// Creation Date   : 2007. 8. 29
//================================================================
#pragma once

#include "../Resource/CnResource.h"
#include "../Resource/CnExportedDataDefines.h"

namespace Cindy
{
	//================================================================
	/** Skeleton Resource Data
	    @author    changhee
		@since     2007. 8. 29
		@brief	   Skeleton 리소스 데이터
	*/
	//================================================================	
	class CN_DLL CnSkeletonData : public CnResource
	{
		__DeclareRtti;
	public:
		typedef std::map<ushort, Export::BoneNode*>	BoneDataMap;
		typedef BoneDataMap::iterator				BoneDataIter;
		typedef BoneDataMap::const_iterator			BoneDataConstIter;

		typedef std::vector<Export::BoundingBox*>	BBoxArray;
		typedef BBoxArray::iterator					BBoxIter;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual ~CnSkeletonData();

		// Load/Unload
		bool				Load( const wchar* strFileName ) override;
		bool				Unload() override;

		// Device Lost/Restore
		void				OnLost() override;
		void				OnRestore() override;

		Export::BoneNode*	GetBone( ushort usIndex );
		const BoneDataMap&	GetBones() const;

		// OBB 구하기
		const Math::OBB&		GetOBB( ushort usIndex );
	private:
		friend class CnSkeletonDataManager;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnSkeletonData( CnResourceManager* pParent );

		bool		LoadData();

		// 파일 정보 읽기
		bool		ReadMagicNumber();
		bool		ReadFileVersion();

		// 본 노드 데이터 읽기
		bool		ReadSkeleton( ulong ulDataSize );
		bool		ReadBone( ulong ulDataSize, Export::BoneNode* pBoneData );

		// BBox 읽어오기
		void		ReadBoundingBoxList( ulong ulDataSize );
		void		ReadBoundingBox( ulong ulDataSize, Export::BoundingBox* pBBox );

		void		ReadBoneDefineList( ulong ulDataSize );

		void		ReadNameData( CnString& rResult );

		void		AddBone( Export::BoneNode* pBoneData );
		void		RemoveAllBones();

		void		AddBoundingBox( Export::BoundingBox* pBox );
		void		RemoveAllBoxes();

		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		BoneDataMap			m_BoneDatas;
		BBoxArray				m_BoundingBoxes;

		MoCommon::MoMemStream	m_DataMemStream;
		ulong					m_ulTotalDataSize;
	};

	//==================================================================
	/** SkeletonDataPtr
		@author			cagetu
		@since			2006년 10월 9일
		@remarks		Mesh 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL SkelDataPtr : public Ptr< CnSkeletonData >
	{
	public:
		SkelDataPtr();
		explicit SkelDataPtr( CnSkeletonData* pObject );
		SkelDataPtr( const SkelDataPtr& spObject );
		SkelDataPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		SkelDataPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnSkeletonData.inl"
}