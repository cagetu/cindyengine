//================================================================
// File:           : CnAnimationController.h
// Original Author : changhee
// Creation Date   : 2007. 2. 1
//================================================================
#ifndef __CN_ANIMATION_CONTROLLER_H__
#define __CN_ANIMATION_CONTROLLER_H__

#include "CnFrameController.h"
#include "CnAnimationData.h"
#include "CnAnimatedNodeController.h"

namespace Cindy
{
	class CnXFormObject;
	//------------------------------------------------------
	typedef float SECOND;
	typedef float FRAME;

	//================================================================
	/** Animation Controller
	    @author    changhee
		@since     2007. 2. 1
		@remarks   애니메이션 제어기 
	*/
	//================================================================
	class CnAnimCtrl : public CnFrameCtrl
	{
		CN_DECLARE_RTTI;
	public:
		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnAnimCtrl();
		virtual ~CnAnimCtrl();

		// 컨트롤러 ID 구하기
		ControllerID	GetID() const override;

		// 갱신
		void			Update() override;

		// 제어
		void			Play() override;
		void			Stop() override;
		void			Reset() override;

		//
		void	SetAnimatedNodeCtrl( CnAnimatedNodeCtrl* pController );
	private:
		static ControllerID		ms_nID;

		CnSmartPtr<CnAnimatedNodeCtrl>	m_AnimatedNodeCtrl;

		//------------------------------------------------------
		// Internal Methods
		//------------------------------------------------------
	};

#include "CnAnimationCtrl.inl"
}

#endif	// __CN_ANIMATION_CONTROLLER_H__