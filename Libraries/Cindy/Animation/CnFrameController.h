//================================================================
// File:           : CnFrameController.h
// Original Author : changhee
// Creation Date   : 2007. 2. 1
//================================================================
#ifndef __CN_FRAMECONTROLLER_H__
#define __CN_FRAMECONTROLLER_H__

#include "../Scene/CnController.h"

namespace Cindy
{
	typedef int	ControllerStatus;

	//================================================================
	/** Frame Controller
	    @author    changhee
		@since     2007. 2. 1
		@brief	   프레임 제어기
		@remarks   프레임 별로 동작하는 것들에 대한 Base
	*/
	//================================================================
	class CnFrameController : public CnController
	{
		__DeclareRtti;
	public:
		static const ControllerStatus		STOP;
		static const ControllerStatus		PLAY;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnFrameController();
		virtual ~CnFrameController();

		ControllerStatus	GetCurrentStatus() const;

		// 제어기
		virtual void		Play() abstract;
		virtual void		Stop() abstract;
		virtual void		Reset() abstract;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		ControllerStatus	m_nStatus;	

		void		_SetCurrentStatus( ControllerStatus nStatus ); 
	};

#include "CnFrameController.inl"
}

#endif	// __CN_FRAMECONTROLLER_H__