//================================================================
// File:               : CnAnimationInstance.cpp
// Related Header File : CnAnimationInstance.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 2
//================================================================
#include "../Cindy.h"
#include "CnAnimationInstance.h"

#include "../Util/CnLog.h"

namespace Cindy
{
	//================================================================
	__ImplementClass(Cindy, CnAnimInstance, CnObject);
	// Const/Dest
	CnAnimInstance::CnAnimInstance()
		: m_fTotalTime(0.0f)
		, m_fSecondPerFrame(0.0f)
		//, m_fFramePerSecond(0.0f)
		//, m_bRepeat(false)
		//, m_bIsEnd(false)
	{
	}
	CnAnimInstance::~CnAnimInstance()
	{
	}

	//================================================================
	/** @brief	Setup */
	//================================================================
	void CnAnimInstance::Setup( AnimDataPtr& animResource )
	{
		if (animResource.IsNull())
			return;

		m_spAnimResource = animResource;
		//m_bIsEnd = false;

		SetFrameRate( (float)animResource->GetFrameRate() );
	}

	//----------------------------------------------------------------
	void CnAnimInstance::SetFrameRate( float fFrameRate )
	{
		m_fSecondPerFrame = 1.0f/fFrameRate;
		m_fTotalTime = (float)m_spAnimResource->GetTotalFrame() * m_fSecondPerFrame;
		//m_fFramePerSecond = 1.0f/fFrameRate;
		//m_fTotalTime = (float)m_spAnimResource->GetTotalFrame() / m_fFramePerSecond;
	}

	//----------------------------------------------------------------
	bool CnAnimInstance::GetKey( AnimKey* Output, const CnString& NodeName, SECOND CurrentTime )
	{
		Export::AnimNode* KeyData = m_spAnimResource->GetNodeData( NodeName );
		if (!KeyData)
		{
			//CnError( L"[CnAnimInstance::GetKey] %s was not found.\n", NodeName.c_str() );
			return false;
		}

		GetPosKey( KeyData, CurrentTime, Output->pos );
		GetRotKey( KeyData, CurrentTime, Output->rot );
		GetSclKey( KeyData, CurrentTime, Output->scl );
		return true;
	}

	//----------------------------------------------------------------
	void CnAnimInstance::GetPosKey( Export::AnimNode* KeyData, SECOND Time, Math::Vector3& Output )
	{
		// 첫 프레임
		if (1 == KeyData->numPosKeys)
		{
			Output = KeyData->posKeys[0].value;
			return;
		}

		// 마지막 프레임
		ushort lastKey = KeyData->numPosKeys - 1;
		if (Time >= KeyData->posKeys[lastKey].frame * this->GetSecondPerFrame())
		{
			Output = KeyData->posKeys[lastKey].value;
			return;
		}

		// 보간
		ushort curKey = 0;
		for (; curKey<lastKey; ++curKey)
		{
			if (KeyData->posKeys[curKey+1].frame*this->GetSecondPerFrame() >= Time)
				break;
		}

		Export::LinearKey* prevKey = &KeyData->posKeys[curKey];
		Export::LinearKey* nextKey = &KeyData->posKeys[curKey+1];

		float time = (Time - (float)prevKey->frame*this->GetSecondPerFrame()) /
					(this->GetSecondPerFrame() * (float)(nextKey->frame - prevKey->frame));

		Output.Lerp( prevKey->value, nextKey->value, time );
	}

	//----------------------------------------------------------------
	void CnAnimInstance::GetRotKey( Export::AnimNode* KeyData, SECOND Time, Math::Quaternion& Output )
	{
		// 첫 프레임
		if (1 == KeyData->numRotKeys)
		{
			Output = KeyData->rotKeys[0].value;
			return;
		}
		// 마지막 프레임
		ushort lastKey = KeyData->numRotKeys - 1;
		if (Time >= KeyData->rotKeys[lastKey].frame*this->GetSecondPerFrame())
		{
			Output = KeyData->rotKeys[lastKey].value;
			return;
		}

		// 보간
		ushort curKey = 0;
		for (; curKey<lastKey; ++curKey)
		{
			if (KeyData->rotKeys[curKey+1].frame*this->GetSecondPerFrame() >= Time)
				break;
		}

		Export::RotKey* prevKey = &KeyData->rotKeys[curKey];
		Export::RotKey* nextKey = &KeyData->rotKeys[curKey+1];

		float time = (Time - (float)prevKey->frame*this->GetSecondPerFrame()) /
					(this->GetSecondPerFrame() * (float)(nextKey->frame - prevKey->frame));

		Output.Slerp( prevKey->value, nextKey->value, time );
	}

	//----------------------------------------------------------------
	void CnAnimInstance::GetSclKey( Export::AnimNode* KeyData, SECOND Time, Math::Vector3& Output )
	{
		// 첫 프레임
		if (1 == KeyData->numScaleKeys)
		{
			Output = KeyData->scaleKeys[0].value;
			return;
		}

		// 마지막 프레임
		ushort lastKey = KeyData->numScaleKeys - 1;
		if (Time >= KeyData->scaleKeys[lastKey].frame * this->GetSecondPerFrame())
		{
			Output = KeyData->scaleKeys[lastKey].value;
			return;
		}

		// 보간
		ushort curKey = 0;
		for (; curKey<lastKey; ++curKey)
		{
			if (KeyData->scaleKeys[curKey+1].frame*this->GetSecondPerFrame() >= Time)
				break;
		}

		Export::LinearKey* prevKey = &KeyData->scaleKeys[curKey];
		Export::LinearKey* nextKey = &KeyData->scaleKeys[curKey+1];

		float time = (Time - (float)prevKey->frame*this->GetSecondPerFrame()) /
					(this->GetSecondPerFrame() * (float)(nextKey->frame - prevKey->frame));

		Output.Lerp( prevKey->value, nextKey->value, time );
	}

	//================================================================
	//	Lock
	//================================================================
	//----------------------------------------------------------------
	void CnAnimInstance::Lock( const CnString& BeginNode, const CnString& EndNode )
	{
		Locker lock;
		lock.begin = BeginNode;
		lock.end = EndNode;
		m_Lockers.push_back( lock );
	}
	//----------------------------------------------------------------
	CnAnimInstance::Locker::Result
	CnAnimInstance::IsLocked( const CnString& NodeName )
	{
		if (m_Lockers.empty())
			return Locker::Ignore;

		LockerArray::iterator i, iend;
		iend = m_Lockers.end();
		for (i=m_Lockers.begin(); i!=iend; ++i)
		{
			if ((*i).begin == NodeName)
				return Locker::Lock;
			else if ((*i).end == NodeName)
				return Locker::Free;
		}

		return Locker::Ignore;
	}
}