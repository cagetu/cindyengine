//================================================================
// File:               : CnBoneNode.inl
// Related Header File : CnBoneNode.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
//----------------------------------------------------------------
/** @brief	본 노드의 타입
*/
//----------------------------------------------------------------
inline
void CnBoneNode::SetType( CnBoneNode::Type eType )
{
	m_Type = eType;
}
//----------------------------------------------------------------
inline
CnBoneNode::Type CnBoneNode::GetType() const
{
	return m_Type;
}

//----------------------------------------------------------------
inline
void CnBoneNode::SetInverseTransfrom( const Math::Matrix44& rMatrix )
{
	m_mInverseTransform = rMatrix;
}

//----------------------------------------------------------------
inline
const Math::Matrix44& CnBoneNode::GetInverseTransfrom() const
{
	return m_mInverseTransform;
}
