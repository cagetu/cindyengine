//================================================================
// File:               : CnAnimationMixer.cpp
// Related Header File : CnAnimationMixer.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 4
//================================================================
#include "../Cindy.h"
#include "CnAnimationMixer.h"
#include "CnAnimationInstance.h"

namespace Cindy
{
	//================================================================
	//	Blending
	//================================================================
	//----------------------------------------------------------------
	void CnAnimationMixer::Blend( AnimKey* Prev, AnimKey* Current, AnimKey* Output, float Delta )
	{
		Output->pos.Lerp( Prev->pos, Current->pos, Delta );
		Output->rot.Slerp( Prev->rot, Current->rot, Delta );
		Output->scl.Lerp( Prev->scl, Current->scl, Delta );		
	}
}