//================================================================
// File:           : CnLookAtController.h
// Original Author : changhee
// Creation Date   : 2008. 12. 26
//================================================================
#ifndef __CN_LOOKAT_CONTROLLER_H__
#define __CN_LOOKAT_CONTROLLER_H__

#include "../Scene/CnController.h"

namespace Cindy
{
	class CnTransformable;

	//================================================================
	/** LookAt Controller
	    @author	changhee
		@brief	쳐다보기 컨트롤러
		@desc	원하는 방향을 쳐다보도록 회전시킨다.
	*/
	//================================================================
	class CnLookAtController : public CnController
	{
		__DeclareClass(CnLookAtController);
	public:
		static const ControllerID ID = 5;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnLookAtController();
		virtual ~CnLookAtController();

		// Get
		ControllerID	GetID() const override	{	return CnLookAtController::ID;	}

		// 
		void	Update() override;
		void	Clear() override;

		// Target
		void	SetTarget( CnTransformable* Target );

	private:
		Ptr<CnTransformable>	m_TargetNode;
	};
}

#endif	// __CN_LOOKAT_CONTROLLER_H__