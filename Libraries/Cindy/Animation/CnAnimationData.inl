//================================================================
// File:               : CnAnimationData.inl
// Related Header File : CnAnimationData.h
// Original Author     : changhee
//================================================================

//----------------------------------------------------------------
inline
ushort CnAnimationData::GetTotalFrame() const
{
	return m_usTotalFrame;
}

//----------------------------------------------------------------
inline
ulong CnAnimationData::GetFrameRate() const
{
	return m_ulFrameRate;
}

//----------------------------------------------------------------
inline
void CnAnimationData::SetFrameRate( ulong ulFrameRate )
{
	m_ulFrameRate = ulFrameRate;
}

//----------------------------------------------------------------
inline
ushort CnAnimationData::GetNumOfNodes() const
{
	return m_usNumNodeDatas;
}

//----------------------------------------------------------------
//	AnimDataPtr Class
//----------------------------------------------------------------
inline
AnimDataPtr::AnimDataPtr()
: Ptr< CnAnimationData >()
{
}

//----------------------------------------------------------------
inline
AnimDataPtr::AnimDataPtr( CnAnimationData* pObject )
: Ptr< CnAnimationData >( pObject )
{
}

//----------------------------------------------------------------
inline
AnimDataPtr::AnimDataPtr( const AnimDataPtr& spObject )
: Ptr< CnAnimationData >( spObject )
{
}

//----------------------------------------------------------------
inline
AnimDataPtr::AnimDataPtr( const RscPtr& spRsc )
: Ptr< CnAnimationData >()
{
	m_pObject = (CnAnimationData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->AddRef();
	}
}

//----------------------------------------------------------------
inline
AnimDataPtr& AnimDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnAnimationData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->Release();

		if( !spRsc.IsNull() )
			spRsc->AddRef();

		m_pObject = (CnAnimationData*)spRsc.GetPtr();
	}

	return *this;
}