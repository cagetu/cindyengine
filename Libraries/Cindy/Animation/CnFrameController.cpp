//================================================================
// File:               : CnFrameController.cpp
// Related Header File : CnFrameController.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnFrameController.h"

namespace Cindy
{
	//================================================================
	//defines
	__ImplementRtti( Cindy, CnFrameController, CnController );
	//----------------------------------------------------------------
	//static
	const ControllerStatus	CnFrameController::STOP = 0;
	const ControllerStatus	CnFrameController::PLAY = 1;

	/// Const/Dest
	CnFrameController::CnFrameController()
		: m_nStatus( STOP )
	{
	}
	CnFrameController::~CnFrameController()
	{
	}
}