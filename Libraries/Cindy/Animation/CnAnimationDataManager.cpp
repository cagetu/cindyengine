//================================================================
// File:               : CnAnimationDataManager.cpp
// Related Header File : CnAnimationDataManager.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 12
//================================================================
#include "../Cindy.h"
#include "CnAnimationDataManager.h"
#include "CnAnimationData.h"

namespace Cindy
{
	//================================================================
	__ImplementClass(Cindy, CnAnimationDataManager, CnResourceManager);
	__ImplementSingleton(CnAnimationDataManager);
	///Const/Dest
	CnAnimationDataManager::CnAnimationDataManager()
	{
		__ConstructSingleton;
	}
	CnAnimationDataManager::~CnAnimationDataManager()
	{
		__DestructSingleton;
	}

	//--------------------------------------------------------------
	/** 리소스 읽기
		@brief
			리소스를 읽는다.
	*/
	//--------------------------------------------------------------
	RscPtr CnAnimationDataManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = Get( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnAnimationData* animation = CnNew CnAnimationData( this );
		if( animation->Load( strFileName ) )
		{
			Add( animation );
		}
		else
		{
			SAFEDEL( animation );
		}

		return animation;
	}
}