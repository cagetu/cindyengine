//================================================================
// File:           : CnSkeletonDataManager.h
// Original Author : changhee
// Creation Date   : 2007. 4. 12
//================================================================
#pragma once

#include "Resource/CnResourceManager.h"
#include "Util/CnSingleton.h"

namespace Cindy
{
	//================================================================
	/** Skeleton Data Manager
	    @author    changhee
		@since     2007. 4. 12
		@brief	   焕措 府家胶 包府磊
	*/
	//================================================================
	class CN_DLL CnSkeletonDataManager : public CnResourceManager
	{
		__DeclareClass(CnSkeletonDataManager);
		__DeclareSingleton(CnSkeletonDataManager);
	public:
		CnSkeletonDataManager();
		virtual ~CnSkeletonDataManager();

		// 府家胶 积己/佬扁
		RscPtr	Load( const wchar* strFileName ) override;
	};

//#include "CnSkeletonDataManager.inl"
}

#define SkeletonDataMgr	Cindy::CnSkeletonDataManager::Instance()
