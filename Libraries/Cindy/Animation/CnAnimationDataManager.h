//================================================================
// File:           : CnAnimationDataManager.h
// Original Author : changhee
//================================================================
#ifndef __CN_ANIMATION_DATA_MANAGER_H__
#define __CN_ANIMATION_DATA_MANAGER_H__

#include "Resource/CnResourceManager.h"
#include "Util/CnSingleton.h"

namespace Cindy
{
	//================================================================
	/** Animation Data Group
	    @author    changhee
		@brief	   局聪皋捞记 府家胶 包府磊
	*/
	//================================================================
	class CN_DLL CnAnimationDataManager : public CnResourceManager
	{
		__DeclareClass(CnAnimationDataManager);
		__DeclareSingleton(CnAnimationDataManager);
	public:
		CnAnimationDataManager();
		virtual ~CnAnimationDataManager();

		// 府家胶 积己/佬扁
		RscPtr	Load( const wchar* strFileName ) override;
	};

//#include "CnAnimationDataManager.inl"
// Macro
#define AnimDataMgr	CnAnimationDataManager::Instance()
}

#endif	// __CN_ANIMATION_DATA_MANAGER_H__