//================================================================
// File:           : CnBoneNode.h
// Original Author : changhee
// Creation Date   : 2007. 2. 1
//================================================================
#ifndef __CN_BONENODE_H__
#define __CN_BONENODE_H__

#include "../Scene/CnAnimNode.h"
#include "CnLookAtController.h"
#include "CnIKController.h"

namespace Cindy
{
	class CnSkeletonInstance;
	class CnAnimChannel;
	class CnAnimController;
	class CnRenderQueue;
	class CnCamera;

	//================================================================
	/** BoneNode
	    @author    changhee
		@since     2007. 2. 1
		@brief	   ���ٱ��� �����ϴ� �� �� ����(Bone)
	*/
	//================================================================
	class CN_DLL CnBoneNode : public CnAnimNode
	{
		__DeclareRtti;
	public:
		enum Type
		{
			None = 0,
			Root,
			Child,
			Leaf,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnBoneNode( const wchar* strName, ushort nIndex );
		virtual ~CnBoneNode();

		// BoneType
		void	SetType( Type eType );
		Type	GetType() const;

		// Skeleton
		void	SetSkeleton( CnSkeletonInstance* pSkeleton );

		// Transform
		void	GetLocalXform( Math::Matrix44& rOut );
		void	SetInverseTransfrom( const Math::Matrix44& rMatrix );
		const Math::Matrix44&	GetInverseTransfrom() const;

		// Update
		void	UpdateTransform( bool bUpdateChildren, bool bChangedParent ) override;
		void	UpdateScene( CnCamera* pCamera, CnVisibleSet* pVisibleSet ) override;

		static void		NeedUpdateTransform( bool bNeed );
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		Type	m_Type;

		CnSkeletonInstance*	m_pSkeleton;
		Math::Matrix44	m_mInverseTransform;

		static bool		ms_NeedUpdateTransform;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnNode*	_CreateChildImpl( const CnString& strName, ushort nIndex ) override;
	};

#include "CnBoneNode.inl"
}

#endif	// __CN_BONENODE_H__