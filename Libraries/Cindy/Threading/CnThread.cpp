// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnThread.h"
#include "../System/CnSystemInfo.h"
#include <process.h>

namespace Cindy
{
namespace Threading
{
#ifdef DEBUG_THREADING
	Win32CriticalSection Win32Thread::ms_CriticalSection;
	std::vector<Win32Thread*> Win32Thread::ms_Threads;
#endif
	//Const
	Win32Thread::Win32Thread()
		: m_Handle(0)
		, m_Priority(Normal)
		, m_StackSize(4096)
		, m_ThreadId(0xffffffff)
		, m_CoreId(System::Win32Cpu::InvalidCoreId)
	{
	// register with thread list
	#ifdef DEBUG_THREADING
		Win32Thread::ms_CriticalSection.Enter();
		ms_Threads.push_back( this );
		Win32Thread::ms_CriticalSection.Leave();
	#endif
	}
	//Const
	Win32Thread::Win32Thread( const CnString& Name )
		: m_Name(Name)
		, m_Handle(0)
		, m_Priority(Normal)
		, m_StackSize(4096)
		, m_ThreadId(0xffffffff)
		, m_CoreId(System::Win32Cpu::InvalidCoreId)
	{
	// register with thread list
	#ifdef DEBUG_THREADING
		Win32Thread::ms_CriticalSection.Enter();
		ms_Threads.push_back( this );
		Win32Thread::ms_CriticalSection.Leave();
	#endif
	}
	// Dest
	Win32Thread::~Win32Thread()
	{
		if (IsRunning())
		{
			Stop();
		}
		
	// register with thread list
	#ifdef DEBUG_THREADING
		Win32Thread::ms_CriticalSection.Enter();
		std::vector<Win32Thread*>::iterator iter = std::find( ms_Threads.begin(), ms_Threads.end(), this );
		ms_Threads.erase( iter );
		Win32Thread::ms_CriticalSection.Leave();
	#endif
	}

	//------------------------------------------------------------------
	/** @desc	재시작 */
	//------------------------------------------------------------------
	bool Win32Thread::Resume()
	{
		return ResumeThread( m_Handle ) != 0xffffffff;
	}

	//------------------------------------------------------------------
	/** @desc	일시 정지 */
	//------------------------------------------------------------------
	bool Win32Thread::Suspend()
	{
		return SuspendThread( m_Handle ) != 0xffffffff;
	}

	//------------------------------------------------------------------
	/** @desc	API로 우선 순위 설정	*/
	//------------------------------------------------------------------
	void Win32Thread::SettingPriority()
	{
		if (0 == m_Handle)
			return;

		switch (m_Priority)
		{
		case Low:
			SetThreadPriority( m_Handle, THREAD_PRIORITY_BELOW_NORMAL );
			break;
		case Normal:
			SetThreadPriority( m_Handle, THREAD_PRIORITY_NORMAL );
			break;
		case High:
			SetThreadPriority( m_Handle, THREAD_PRIORITY_ABOVE_NORMAL );
			break;
		}
	}

	//------------------------------------------------------------------
	/** @brief	쓰레드 루프
		@desc	작업을 재정의 하시오
	*/
	//------------------------------------------------------------------
	void Win32Thread::DoWork()
	{	// empty. override in subClass
	}

	//------------------------------------------------------------------
	/** @desc	이 함수는 stopRequest event가 세팅된 후와 쓰레드를 끝내기 위해 대기하기 전에
				Thread::Stop() 에 의해 호출된다.
				끝내기 전에 thread loop가 wakeup 호출을 해야할 필요가 있다면,
				이 함수를 재정의 하시오
	*/
	//------------------------------------------------------------------
	void Win32Thread::EmitWakeupSignal()
	{	// empty. override in subClass
	}

	//------------------------------------------------------------------
	/** @brief	쓰레드 시작 */
	//------------------------------------------------------------------
	void Win32Thread::Start()
	{
		assert(0 == m_Handle);
		m_Handle = CreateThread( NULL, m_StackSize, ThreadProc, this, CREATE_SUSPENDED, &m_ThreadId );
		assert(0 != m_Handle);

		SettingPriority();

		// thread에 대해 good processor를 선택한다.
		System::Win32SystemInfo systemInfo;
		DWORD threadIdealProc = 0;
		if (System::Win32Cpu::InvalidCoreId != m_CoreId)
		{
			uint numCpuCores = systemInfo.GetNumCpuCores();
			threadIdealProc = m_CoreId % numCpuCores;
		} // if

		SetThreadIdealProcessor( m_Handle, threadIdealProc );

		Resume();

		// Thread 시작 대기
		m_StartedEvent.Wait();
	}

	//------------------------------------------------------------------
	//** @brief		쓰레드 끝내기 */
	//------------------------------------------------------------------
	void Win32Thread::Stop()
	{
		// 쓰레드 종료 요청
		m_StopRequestEvent.Signal();

		// 쓰레드가 깨어날 것들이 있으면 재정의
		this->EmitWakeupSignal();

		/// 쓰레드를 종료하기 위해 무한 대기
		WaitForSingleObject( m_Handle, INFINITE );
		CloseHandle(m_Handle);
		m_Handle = 0;
	}

	//------------------------------------------------------------------
	/** @desc	Thread Processing */
	//------------------------------------------------------------------
	DWORD WINAPI Win32Thread::ThreadProc( LPVOID lpContext )
	{
		assert(0 != lpContext);
		Win32Thread* thread = (Win32Thread*)lpContext;
		thread->m_StartedEvent.Signal();
		thread->DoWork();
		return 0;
	}

#ifdef DEBUG_THREADING
	//------------------------------------------------------------------
	/** @desc	쓰레드 디버깅 정보 얻어오기 */
	//------------------------------------------------------------------
	std::vector<Win32Thread::DebugInfo>
	Win32Thread::GetRunningThreadDebugInfos()
	{
		std::vector<Win32Thread::DebugInfo> output;

		Win32CriticalSection::Scope csLock( &ms_CriticalSection );

		std::vector<Win32Thread*>::iterator i, iend;
		iend = ms_Threads.end();
		for (i = ms_Threads.begin(); i!=iend; ++i)
		{
			Win32Thread* cur = (*i);
			if (cur->IsRunning())
			{
				Win32Thread::DebugInfo info;
				info.name = cur->GetName();
				info.priority = cur->GetPriority();
				info.stackSize = cur->GetStackSize();
				info.threadId = cur->GetThreadId();
				info.coreId = cur->GetCoreId();
				output.push_back( info );
			}
		} // for (i = ms_Threads.begin(); i!=iend; ++i)

		return output;
	}
#endif	// DEBUG_THREADING

}
}