// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CNTHREAD_H__
#define __CNTHREAD_H__

#include "CnEvent.h"
#include "CnCriticalSection.h"
#include "../System/CnCpuInfo.h"

//#define DEBUG_THREADING

namespace Cindy
{
namespace Threading
{
	typedef DWORD ThreadId;
	static const ThreadId InvalidThreadId = 0xffffffff;
}

namespace Threading
{
	//==================================================================
	/** Win32Thread
		@author		cagetu
		@brief		Win32Thread Control Class
		@see		Nebula3 Win360Thread 참고
	*/
	//==================================================================
	class CN_DLL Win32Thread
	{
	public:
		// 우선순위
		enum Priority
		{
			Low,
			Normal,
			High,
		};

		//------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------
		Win32Thread();
		Win32Thread( const CnString& Name );
		virtual ~Win32Thread();

		void	SetName( const CnString& Name );
		const CnString&	GetName() const;

		void	SetPriority( Priority ePriority );
		Priority GetPriority() const;

		void	SetStackSize( ulong StackSize );
		ulong	GetStackSize() const;

		DWORD	GetThreadId() const;
		System::Win32Cpu::CoreId	GetCoreId() const;

		bool	IsRunning() const;

		void	Start();
		void	Stop();

	#ifdef DEBUG_THREADING
		struct DebugInfo
		{
			CnString					name;
			Win32Thread::Priority		priority;
			ulong						threadId;
			System::Win32Cpu::CoreId	coreId;
			ulong						stackSize;
		};
		static std::vector<DebugInfo>	GetRunningThreadDebugInfos();
	#endif // DEBUG_THREADING

	protected:
		virtual void	DoWork();
		virtual void	EmitWakeupSignal();
		
		bool			IsStopRequested() const;	

	private:
		CnString	m_Name;			//!< 쓰레드 이름
		HANDLE		m_Handle;		//!< 쓰레드 핸들
		Priority	m_Priority;		//!< 쓰레드 우선순위
		ulong		m_StackSize;	//!< 쓰레드 스택 사이즈

		ulong		m_ThreadId;					//!< 쓰레드 ID
		System::Win32Cpu::CoreId	m_CoreId;	//!< 코어 ID

		Win32Event	m_StartedEvent;		//!< 쓰레드 시작 이벤트
		Win32Event	m_StopRequestEvent;	//!< 쓰레드 끝내기 이벤트

	#ifdef DEBUG_THREADING
		static Win32CriticalSection	ms_CriticalSection;
		static std::vector<Win32Thread*> ms_Threads;
	#endif // DEBUG_THREADING

		//------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------
		void	SettingPriority();

		bool	Suspend();
		bool	Resume();

		// 기본 Thread 루틴
		static DWORD WINAPI	ThreadProc( LPVOID lpContext );
	};

	#include "CnThread.inl"
}
}

#endif	// __CNTHREAD_H__