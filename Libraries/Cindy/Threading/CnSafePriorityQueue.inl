/// Const
template<class PRITYPE, class TYPE>
SafePriorityQueue<PRITYPE,TYPE>::SafePriorityQueue()
	: _enabled(true)
{
}
template<class PRITYPE, class TYPE>
SafePriorityQueue<PRITYPE,TYPE>::SafePriorityQueue( const SafePriorityQueue<PRITYPE,TYPE>& rhs )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	_queue = rhs._queue;
	_enabled = rhs._enabled;
}

//------------------------------------------------------------------
/** @desc	operator = */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::operator =( const SafePriorityQueue<PRITYPE,TYPE>& rhs )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	_queue = rhs._queue;
	_enabled = rhs._enabled;
}

//------------------------------------------------------------------
/** @desc	활성화 */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::Enable( bool bEnable )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	_enabled = bEnable;
}

//------------------------------------------------------------------
/** @desc	활성화 여부 */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
bool SafePriorityQueue<PRITYPE,TYPE>::IsEnabled() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _enabled;
}

//------------------------------------------------------------------
/** @desc	사이즈 얻기 */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
uint SafePriorityQueue<PRITYPE,TYPE>::Size() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return (uint)_queue.size();
}

//------------------------------------------------------------------
/** @desc	비어 있나요? */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
bool SafePriorityQueue<PRITYPE,TYPE>::IsEmpty() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _queue.empty();
}

//------------------------------------------------------------------
/** @desc	Element 추가 */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::Push( PRITYPE Pri, const TYPE& Element )
{
	{
		Win32CriticalSection::Scope lock( &_criticalSection );
		_queue.insert( ElemMap::value_type(Pri, Element) );
	}

	if (_enabled)
	{
		_workEvent.Signal();
	}
}

//------------------------------------------------------------------
/** @desc	Element 뽑아낸다 */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
TYPE SafePriorityQueue<PRITYPE,TYPE>::Pop()
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	ElemMap::iterator iter = _queue.begin();
	TYPE elem = iter->second();
	_queue.erase(iter);
	return elem;
}
//------------------------------------------------------------------
/** @desc	Element를 지운다(중복된 것이 있으면 모두 지운다.) */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::Erase( const TYPE& Element )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	ElemMap::iterator iter = _queue.begin();
	while (iter != _queue.end())
	{
		if (iter->second() == Element)
		{
			iter = _queue.erase(i);
		}
		else
		{
			iter++;
		}
	}
}

//------------------------------------------------------------------
/** @desc	모두 제거 */
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::Clear()
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _queue.clear();
}

//------------------------------------------------------------------
/** @desc	맨 위에 있는 Element의 값을 구한다.
			(컨테이너에 남겨둔다.)
*/
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
TYPE SafePriorityQueue<PRITYPE,TYPE>::Peek() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	ElemMap::iterator iter = _queue.begin();
	return iter->second();
}

//------------------------------------------------------------------
/** @desc	대기
*/
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::Wait()
{
	if (_enabled && _queue.empty())
	{
		_workEvent.Wait();
	}
}

//------------------------------------------------------------------
/** @desc	대기 + Timeout
*/
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::WaitTimeout( DWORD ms )
{
	if (_enabled && _queue.empty())
	{
		_workEvent.WaitTimeout( ms );
	}
}

//------------------------------------------------------------------
/** @desc	깨우기
*/
//------------------------------------------------------------------
template<class PRITYPE, class TYPE>
void SafePriorityQueue<PRITYPE,TYPE>::Signal()
{
	_workEvent.Signal();
}