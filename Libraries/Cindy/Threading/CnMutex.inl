//================================================================
// File:               : CnMutex.inl
// Related Header File : CnMutex.h
// Original Author     : changhee
//================================================================
//----------------------------------------------------------------
inline
Win32Mutex::Win32Mutex()
{
#ifndef _NO_THREADS_
	m_hMutex = CreateMutex( NULL, FALSE, NULL );
#endif
}

//----------------------------------------------------------------
inline 
Win32Mutex::~Win32Mutex()
{
#ifndef _NO_THREADS_
	CloseHandle( m_hMutex );
#endif
	m_hMutex = NULL;
}

//----------------------------------------------------------------
inline
void Win32Mutex::Lock() const
{
#ifndef _NO_THREADS_
	WaitForSingleObject( m_hMutex, INFINITE );
#endif
}

//----------------------------------------------------------------
inline
void Win32Mutex::Release() const
{
#ifndef _NO_THREADS_
	ReleaseMutex( m_hMutex );
#endif
}