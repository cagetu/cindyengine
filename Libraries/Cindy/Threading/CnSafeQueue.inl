/// Const
template<class TYPE>
SafeQueue<TYPE>::SafeQueue()
	: _enabled(true)
{
}
template<class TYPE>
SafeQueue<TYPE>::SafeQueue( const SafeQueue<TYPE>& rhs )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	_queue = rhs._queue;
	_enabled = rhs._enabled;
}

//------------------------------------------------------------------
/** @desc	operator = */
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::operator =( const SafeQueue<TYPE>& rhs )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	_queue = rhs._queue;
	_enabled = rhs._enabled;
}

//------------------------------------------------------------------
/** @desc	활성화 */
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::Enable( bool bEnable )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	_enabled = bEnable;
}

//------------------------------------------------------------------
/** @desc	활성화 여부 */
//------------------------------------------------------------------
template<class TYPE>
bool SafeQueue<TYPE>::IsEnabled() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _enabled;
}

//------------------------------------------------------------------
/** @desc	사이즈 얻기 */
//------------------------------------------------------------------
template<class TYPE>
uint SafeQueue<TYPE>::Size() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return (uint)_queue.size();
}

//------------------------------------------------------------------
/** @desc	비어 있나요? */
//------------------------------------------------------------------
template<class TYPE>
bool SafeQueue<TYPE>::IsEmpty() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _queue.empty();
}

//------------------------------------------------------------------
/** @desc	Element 추가 */
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::Push( const TYPE& Element )
{
	{
		Win32CriticalSection::Scope lock( &_criticalSection );
		_queue.push_back( Element );
	}

	if (_enabled)
	{
		_workEvent.Signal();
	}
}

//------------------------------------------------------------------
/** @desc	Element 뽑아낸다 */
//------------------------------------------------------------------
template<class TYPE>
TYPE SafeQueue<TYPE>::Pop()
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _queue.pop_front();
}

//------------------------------------------------------------------
/** @desc	모든 것을 뽑아낸다. */
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::PopAll( typename SafeQueue<TYPE>::ElemList& outElements )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	outElements = _queue;
	_queue.clear();
}

//------------------------------------------------------------------
/** @desc	Element를 지운다(중복된 것이 있으면 모두 지운다.) */
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::Erase( const TYPE& Element )
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	ElemList::iterator iter = _queue.begin();
	while (iter != _queue.end())
	{
		if ((*iter) == Element)
		{
			iter = _queue.erase(i);
		}
		else
		{
			iter++;
		}
	}
}

//------------------------------------------------------------------
/** @desc	모두 제거 */
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::Clear()
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _queue.clear();
}

//------------------------------------------------------------------
/** @desc	맨 위에 있는 Element의 값을 구한다.
			(컨테이너에 남겨둔다.)
*/
//------------------------------------------------------------------
template<class TYPE>
TYPE SafeQueue<TYPE>::Peek() const
{
	Win32CriticalSection::Scope lock( &_criticalSection );
	return _queue.front();
}

//------------------------------------------------------------------
/** @desc	대기
*/
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::Wait()
{
	if (_enabled && _queue.empty())
	{
		_workEvent.Wait();
	}
}

//------------------------------------------------------------------
/** @desc	대기 + Timeout
*/
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::WaitTimeout( DWORD ms )
{
	if (_enabled && _queue.empty())
	{
		_workEvent.WaitTimeout( ms );
	}
}

//------------------------------------------------------------------
/** @desc	깨우기
*/
//------------------------------------------------------------------
template<class TYPE>
void SafeQueue<TYPE>::Signal()
{
	_workEvent.Signal();
}