// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CN_WIN32CRITICAL_SECTION_H__
#define __CN_WIN32CRITICAL_SECTION_H__

#include "../Cindy.h"

namespace Cindy
{
namespace Threading
{
	//==================================================================
	/** Win32CriticalSection
		@author		cagetu
		@brief		Win32CriticalSection 객체
	*/
	//==================================================================
	class Win32CriticalSection
	{
	public:
		Win32CriticalSection();
		~Win32CriticalSection();

		void	Enter();
		void	Leave();

		//------------------------------------------------------------------
		//	Structs
		//------------------------------------------------------------------
		// Scope
		class Scope
		{
		public:
			Scope( Win32CriticalSection* cs )
				: _cs(cs)
			{
				_cs->Enter();
			}
			~Scope()
			{
				_cs->Leave();
			}
		private:
			Win32CriticalSection*	_cs;
		};

	private:
		CRITICAL_SECTION	m_CriticalSection;
	};

	//------------------------------------------------------------------
	inline
	Win32CriticalSection::Win32CriticalSection()
	{
	    InitializeCriticalSectionAndSpinCount(&m_CriticalSection, 1024);
	}
	inline
	Win32CriticalSection::~Win32CriticalSection()
	{
		DeleteCriticalSection(&m_CriticalSection);
	}

	//------------------------------------------------------------------
	/** @desc	임계 영역 진입 */
	//------------------------------------------------------------------
	inline
	void Win32CriticalSection::Enter()
	{
		EnterCriticalSection(const_cast<LPCRITICAL_SECTION>(&m_CriticalSection));
	}

	//------------------------------------------------------------------
	/** @desc	임계 영역 나오기 */
	//------------------------------------------------------------------
	inline
	void Win32CriticalSection::Leave()
	{
		LeaveCriticalSection(const_cast<LPCRITICAL_SECTION>(&m_CriticalSection));
	}
}
}

#endif	// __CN_WIN32CRITICAL_SECTION_H__