//================================================================
// File:           : CnSafeQueue.h
// Original Author : changhee
//================================================================
#ifndef __CN_SAFEQUEUE_H__
#define __CN_SAFEQUEUE_H__

#include "CnCriticalSection.h"
#include "CnEvent.h"

namespace Cindy
{
namespace Threading
{
	//==================================================================
	/** SafeQueue
		@author		cagetu
		@brief		Thread 처리를 위한 Queue
		@see		Nubula3 - Threading::SafeQueue
	*/
	//==================================================================
	template<class TYPE>
	class SafeQueue
	{
	public:
		typedef std::deque<TYPE>	ElemList;

		//------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------
		SafeQueue();
		SafeQueue( const SafeQueue<TYPE>& rhs );

		void operator =( const SafeQueue<TYPE>& rhs );

		void	Enable( bool bEnable );
		bool	IsEnabled() const;

		uint	Size() const;
		bool	IsEmpty() const;

		void	Push( const TYPE& Element );
		TYPE	Pop();
		void	PopAll( typename ElemList& outElements );
		void	Erase( const TYPE& Element );
		void	Clear();

		TYPE	Peek() const;

		void	Wait();
		void	WaitTimeout( DWORD ms );
		void	Signal();

	protected:
		typename ElemList	_queue;
		bool				_enabled;
		Win32Event			_workEvent;
		Win32CriticalSection	_criticalSection;
	};

	#include "CnSafeQueue.inl"
}
}

#endif	// __CN_SAFEQUEUE_H__