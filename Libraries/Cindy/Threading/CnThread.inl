// Copyright (c) 2006~. cagetu
//
//******************************************************************

//------------------------------------------------------------------
inline
void Win32Thread::SetName( const CnString& Name )
{
	m_Name = Name;
}
//------------------------------------------------------------------
inline
const CnString& Win32Thread::GetName() const
{
	return m_Name;
}

//------------------------------------------------------------------
/** @desc	살아 있냐? */
//------------------------------------------------------------------
inline bool
Win32Thread::IsRunning() const
{
    return (0 != this->m_Handle);
}

//------------------------------------------------------------------
inline
void Win32Thread::SetPriority( Win32Thread::Priority ePriority )
{
	m_Priority = ePriority;
}
//------------------------------------------------------------------
inline
Win32Thread::Priority Win32Thread::GetPriority() const
{
	return m_Priority;
}

//------------------------------------------------------------------
inline
void Win32Thread::SetStackSize( ulong StackSize )
{
	m_StackSize = StackSize;
}

//------------------------------------------------------------------
inline
ulong Win32Thread::GetStackSize() const
{
	return m_StackSize;
}

//------------------------------------------------------------------
inline
DWORD Win32Thread::GetThreadId() const
{
	return m_ThreadId;
}

//------------------------------------------------------------------
inline
System::Win32Cpu::CoreId Win32Thread::GetCoreId() const
{
	return m_CoreId;
}

//------------------------------------------------------------------
/** @brief	Thread 종료 요청이 있었느냐? */
//------------------------------------------------------------------
inline
bool Win32Thread::IsStopRequested() const
{
	return m_StopRequestEvent.Peek();	
}