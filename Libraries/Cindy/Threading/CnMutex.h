//================================================================
// File:           : CnMutex.h
// Original Author : changhee
// Creation Date   : 2007. 3. 5
//================================================================
#ifndef __CN_WIN32MUTEX_H__
#define __CN_WIN32MUTEX_H__

#include "../Cindy.h"
#include <windows.h>

namespace Cindy
{
namespace Threading
{
	//==================================================================
	/** Mutex Class
		@author		cagetu
		@brief		Mutex를 이용한 동기화 클래스
		@see		"http://www.flipcode.com/articles/article_multithreading02.shtml"
					Nebula2::nMutex
	*/
	//==================================================================
	class Win32Mutex
	{
	public:
		Win32Mutex();
		~Win32Mutex();

		void	Lock() const;		//!< Lock
		void	Release() const;	//!< Release

		//------------------------------------------------------------------
		//	Structs
		//------------------------------------------------------------------
		class Scope
		{
		public:
			Scope( Win32Mutex* mutex )
				: _mutex(mutex)
			{
				_mutex->Lock();
			}
			~Scope()
			{
				_mutex->Unlock();
			}

		private:
			Win32Mutex*	_mutex;
		};

	private:
#ifndef _NO_THREADS_
		HANDLE	m_hMutex;		//!< Mutex Handle
#endif
	};

	#include "CnMutex.inl"
}
}

#endif // __CN_WIN32MUTEX_H__