//================================================================
// File:           : CnSafePriorityQueue.h
// Original Author : changhee
//================================================================
#ifndef __CN_SAFEPRIORITYQUEUE_H__
#define __CN_SAFEPRIORITYQUEUE_H__

#include "CnCriticalSection.h"
#include "CnEvent.h"

namespace Cindy
{
namespace Threading
{
	//==================================================================
	/** SafePriorityQueue
		@author		cagetu
		@brief		Thread 처리를 위한 우선 순위 Queue
		@see		Nubula3 - Threading::SafePriorityQueue
	*/
	//==================================================================
	template<class PRITYPE, class TYPE>
	class SafePriorityQueue
	{
	public:
		typedef std::map<PRITYPE, TYPE>	ElemMap;

		//------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------
		SafePriorityQueue();
		SafePriorityQueue( const SafePriorityQueue<PRITYPE,TYPE>& rhs );

		void operator =( const SafePriorityQueue<PRITYPE,TYPE>& rhs );

		void	Enable( bool bEnable );
		bool	IsEnabled() const;

		uint	Size() const;
		bool	IsEmpty() const;

		void	Push( PRITYPE Pri, const TYPE& Element );
		TYPE	Pop();
		void	Erase( const TYPE& Element );
		void	Clear();

		TYPE	Peek() const;

		void	Wait();
		void	WaitTimeout( DWORD ms );
		void	Signal();

	protected:
		typename ElemMap	_queue;
		bool				_enabled;
		Win32Event			_workEvent;
		Win32CriticalSection	_criticalSection;
	};

	#include "CnSafePriorityQueue.inl"
}
}

#endif	// __CN_SAFEPRIORITYQUEUE_H__