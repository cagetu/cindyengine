// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnThread.h"
#include <process.h>

namespace Cindy
{
	//----------------------------------------------------------------------------
	CnThread::CnThread( const CnString& strName )
		: m_strName( strName )
		, m_hThread( 0 )
		, m_hWakeEvent( 0 )
		, m_ulThreadID( 0xffff )
		, m_bTerminated( false )
		, m_eStatus( STATUS_NONE )
	{
	}

	//----------------------------------------------------------------------------
	CnThread::~CnThread()
	{
		CloseHandle( m_hWakeEvent );
		CloseHandle( m_hThread );
	}

	//----------------------------------------------------------------------------
	bool CnThread::CreateThread( bool bSuspend )
	{
		return CreateThread( NULL, NULL, bSuspend );
	}

	//----------------------------------------------------------------------------
	bool CnThread::CreateThread( LPTHREAD_START_ROUTINE lpStartRoutine, LPVOID lpParam, bool bSuspend )
	{
		m_hWakeEvent = ::CreateEvent( NULL, FALSE, FALSE, NULL );
		m_lpThreadRotine = lpStartRoutine;
		m_lpParameter = lpParam;

		m_hThread = ::CreateThread( NULL, 0, DefaultThreadFunc, this, CREATE_SUSPENDED, &m_ulThreadID );
		if( NULL == m_hThread )
		{
			m_eStatus = STATUS_EXIT;
			return false;
		}

		m_eStatus = STATUS_NONE;
		m_bTerminated = false;

		if( !bSuspend )
			Resume();

		return true;
	}

	//----------------------------------------------------------------------------
	void CnThread::SetPriority( ulong ulPriority )
	{
		SetThreadPriority( m_hThread, ulPriority );
	}

	//----------------------------------------------------------------------------
	ulong CnThread::GetPriority() const
	{
		return GetThreadPriority( m_hThread );
	}

	//----------------------------------------------------------------------------
	void CnThread::Terminate()
	{
		m_bTerminated = true;
	}

	//----------------------------------------------------------------------------
	ulong CnThread::Execute()
	{
		return 0;
	}

	//----------------------------------------------------------------------------
	bool CnThread::Play()
	{
		return ( 0 != SetEvent( m_hWakeEvent ) );
	}

	//----------------------------------------------------------------------------
	ulong CnThread::Stop( ulong ulWaitTime )
	{
		m_eStatus = STATUS_STOP;
		ulong result = WaitForSingleObject( m_hWakeEvent ,ulWaitTime );
		m_eStatus = STATUS_PROCESS;

		return result;
	}

	//----------------------------------------------------------------------------
	bool CnThread::Resume()
	{
		return ResumeThread( m_hThread ) !=  (DWORD)-1;
	}

	//----------------------------------------------------------------------------
	bool CnThread::Suspend()
	{
		return SuspendThread( m_hThread ) !=  (DWORD)-1;
	}

	//----------------------------------------------------------------------------
	ulong CnThread::WaitForExit( ulong ulWaitTime )
	{
		return WaitForSingleObject( m_hThread , ulWaitTime );
	}

	//============================================================================
	ulong __stdcall CnThread::DefaultThreadFunc( LPVOID lpContext )
	{
		CnThread *pThread = (CnThread*)lpContext;
		if( NULL == pThread )
			return 0xffffffff;	

		if( pThread->m_lpThreadRotine )
			pThread->m_lpThreadRotine( pThread->m_lpParameter );
		else
			pThread->Execute();

		pThread->m_eStatus = STATUS_EXIT;
		pThread->m_bTerminated = false;

		return 0;
	}
}