//================================================================
// File:           : CnMutex.h
// Original Author : changhee
// Creation Date   : 2007. 3. 5
//================================================================
#ifndef __CN_MUTEX_H__
#define __CN_MUTEX_H__

#include "../Cindy.h"
#include <windows.h>

namespace Cindy
{
	//==================================================================
	/** Mutex Class
		@author
			cagetu
		@since
			2007년 3월 5일
		@remarks
			Mutex를 이용한 동기화 클래스

			"http://www.flipcode.com/articles/article_multithreading02.shtml"
			Nebula2::nMutex
	*/
	//==================================================================
	class CnMutex
	{
	private:
#ifndef _NO_THREADS_
		HANDLE			m_hMutex;			//!< Mutex Handle
#endif

	public:
		CnMutex();
		~CnMutex();

		void		Lock() const;		//!< Lock
		void		Unlock() const;		//!< Release
	};

	//==================================================================
	/** Mutex Lock Class
		@author
			cagetu
		@since
			2007년 3월 5일
		@remarks
			함수 Block에서 작동하는 mutex를 만드는 객체
	*/
	//==================================================================
	class CnMutexLock
	{
	private:
		CnMutex*		m_pMutex;
	public:
		CnMutexLock( CnMutex* mutex ) : m_pMutex( mutex )
		{
			m_pMutex->Lock();
		}
		~CnMutexLock()
		{
			m_pMutex->Unlock();
		}
	};

#include "CnMutex.inl"
}

#endif // __CN_MUTEX_H__