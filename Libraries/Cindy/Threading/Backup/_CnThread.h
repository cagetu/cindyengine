// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNTHREAD_H__
#define __CNTHREAD_H__

#include "../Cindy.h"
#include <windows.h>

namespace Cindy
{
	//==================================================================
	/** CnThread
		@author
			cagetu
		@since
			2006년 9월 25일
		@remarks
			Thread Control Class
	*/
	//==================================================================
	class CN_DLL CnThread
	{
	public:
		// 쓰레드 상태 정보
		enum STATUS
		{
			STATUS_NONE = 0,			//!< 아무 상태도 아님
			STATUS_PROCESS,				//!< 쓰레드 작업 중
			STATUS_STOP,				//!< 쓰레드 멈춤
			STATUS_EXIT					//!< 쓰레드 종료
		};

	private:
		CnString				m_strName;			//!< 쓰레드 이름

		HANDLE					m_hThread;			//!< 쓰레드 핸들
		HANDLE					m_hWakeEvent;		//!< 쓰레드 작업 이벤트

		ulong					m_ulThreadID;		//!< 쓰레드 ID

		bool					m_bTerminated;		//!< 쓰레드 종료 

		STATUS					m_eStatus;			//!< 쓰레드 상태

		LPTHREAD_START_ROUTINE	m_lpThreadRotine;
		LPVOID					m_lpParameter;

		/** 기본 Thread 루틴
		*/
		static ulong __stdcall		DefaultThreadFunc( LPVOID lpContext );

	protected:
		/** 쓰레드 루프
		*/
		virtual ulong		Execute();

		/** 쓰레드 종료 메세지가 들어왔나요?
		*/
		bool				IsTerminated() const;

		/** 쓰레드를 재워야 하나?
		*/
		ulong				Stop( ulong ulWaitTime = INFINITE );
	public:
		CnThread( const CnString& strName );
		virtual ~CnThread();

		/** Thread 생성
			@param lpStartRoutine
				외부에서 쓰레드 루틴을 넘겨준다. NULL 이면 기본적으로 Execute()를 사용한다.
			@param lpParam
				외부에서 넘겨진 쓰레드 루틴의 파라미터로 사용된다.
		*/
		bool				CreateThread( LPTHREAD_START_ROUTINE lpStartRoutine, LPVOID lpParam, bool bSuspend = false );
		bool				CreateThread( bool bSuspend = false );

		/** 쓰레드 정보
		*/
		const CnString&		GetName() const;
		ulong				GetID() const;
		STATUS				GetStatus() const;

		/** 쓰레드 우선 순위
		*/
		void				SetPriority( ulong ulPriority );
		ulong				GetPriority() const;

		/** 쓰레드 종료
		*/
		virtual void		Terminate();

		/** 쓰레드 명령
		*/
		bool				Play();
		bool				Resume();
		bool				Suspend();

		/** 쓰레드가 종료 될 때까지 대기한다.
			@remarks
			사용예) thread->Terminate();
					thread->WaiteForExit();
					-> 여기까지 오면 쓰레드가 확실히 죽었다고 판정
		*/
		ulong				WaitForExit( ulong ulWaitTime = INFINITE );
	};
}

#endif	// __CNTHREAD_H__