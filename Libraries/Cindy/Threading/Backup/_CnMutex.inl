//================================================================
// File:               : CnMutex.inl
// Related Header File : CnMutex.h
// Original Author     : changhee
// Creation Date       : 2007. 3. 5
//================================================================
//----------------------------------------------------------------
inline
CnMutex::CnMutex()
{
#ifndef _NO_THREADS_
	m_hMutex = CreateMutex( NULL, FALSE, NULL );
#endif
}

//----------------------------------------------------------------
inline 
CnMutex::~CnMutex()
{
#ifndef _NO_THREADS_
	CloseHandle( m_hMutex );
#endif
	m_hMutex = NULL;
}

//----------------------------------------------------------------
inline
void CnMutex::Lock() const
{
#ifndef _NO_THREADS_
	WaitForSingleObject( m_hMutex, INFINITE );
#endif
}

//----------------------------------------------------------------
inline
void CnMutex::Unlock() const
{
#ifndef _NO_THREADS_
	ReleaseMutex( m_hMutex );
#endif
}