// Copyright (c) 2006~. cagetu
//
//******************************************************************

//----------------------------------------------------------------------------
inline bool CnThread::IsTerminated() const
{
	return m_bTerminated;
}

//----------------------------------------------------------------------------
inline const CnString& CnThread::GetName() const
{
	return m_strName;
}

//----------------------------------------------------------------------------
inline ulong CnThread::GetID() const
{
	return m_ulThreadID;
}

//----------------------------------------------------------------------------
inline CnThread::STATUS CnThread::GetStatus() const
{
	return m_eStatus;
}