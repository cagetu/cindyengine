//================================================================
// File:           : CnTheadVariable.h
// Original Author : changhee
// Creation Date   : 2007. 3. 5
//================================================================
#pragma once

#include "CnMutex.h"

namespace Cindy
{
	//================================================================
	/** ThreadVariable
	    @author    changhee
		@since     2007. 3. 5
		@remarks   Thread Safe 한 변수를 만들기 위함
				   (Nebula2 참고)
	*/
	//================================================================
	template<class TYPE>
	class CnThreadVariable
	{
	private:
		CnMutex		m_Mutex;
		TYPE		m_Variable;

	public:
		CnThreadVariable();
		CnThreadVariable( const CnThreadVariable& src );
		CnThreadVariable( TYPE val );

		void operator =( const CnThreadVariable& rhs );
		void operator =( TYPE rhs );

		void Set( TYPE val );
		TYPE Get();
	};

	template<class TYPE>
	CnThreadVariable<TYPE>::CnThreadVariable()
	{
	}

	template<class TYPE>
	CnThreadVariable<TYPE>::CnThreadVariable( const CnThreadVariable& src )
	{
		//this->Set( src.Get() );
	}

	template<class TYPE>
	CnThreadVariable<TYPE>::CnThreadVariable( TYPE val )
	{
		this->Set( val );
	}

	template<class TYPE>
	void CnThreadVariable<TYPE>::operator =( const CnThreadVariable<TYPE>& rhs )
	{
		//this->Set( rhs.Get() );
	}

	template<class TYPE>
	void CnThreadVariable<TYPE>::operator =( TYPE rhs )
	{
		this->Set( rhs );
	}

	template<class TYPE>
	void CnThreadVariable<TYPE>::Set( TYPE val )
	{
		CnMutexLock lock( &m_Mutex );
		m_Variable = val;
	}

	template<class TYPE>
	TYPE CnThreadVariable<TYPE>::Get()
	{
		CnMutexLock lock( &m_Mutex );
		return m_Variable;
	}
}