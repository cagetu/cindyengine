// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CN_INTERLOCKED_H__
#define __CN_INTERLOCKED_H__

#include "../Cindy.h"

namespace Cindy
{
namespace Threading
{
	//==================================================================
	/** Win32Interlocked
		@author		cagetu
		@brief		Win32Interlocked Control Class
					(Nebula3 Win360Interlocked 참고)
		@desc		상호잠금 함수(interlocked function)들은 다른 동기화 방법들에 비해 간단하고 빠르며
					스레드를 차단하지도 않는다는 장점을 가지고 있다.
					Windows 의 상호잠금 함수는 InterlockedIncrement() 와 InterlockedDecrement() 이다.
					이들은 32비트 부호 있는 정수들에 적용된다.

					상호잠금 함수를 사용할 때, 주의할 점은 아래와 같다.
						* InterlockedIncrement 함수를 호출한 쓰레드가 N 의 새 값을 사용하기 전에
						  다른 쓰레드가 N 의 값을 변경할 가능성이 존재한다.
						* N 을 2 증가하기 위해서 InterlockedIncrement 함수를 연달아 두 번 호출하는 것은 위험하다.
						  이는 그 두 호출 사이에 다른 스레드가 실행될 가능성이 존재하기 때문이다.
						  -> InterlockedExchangeAdd 함수를 사용하여 한다.
	*/
	//==================================================================
	class Win32Interlocked
	{
	public:
		static LONG		Increment( int volatile& val );
		static LONG		Decrement( int volatile& val );

		static void		Add( int volatile& val, int add );

		static LONG		Exchange( int volatile* dest, int value );
		static LONG		CompareExchange( int volatile* dest, int exchange, int comparand );
	};

	//------------------------------------------------------------------
	/** @brief	interlocked increment */
	//------------------------------------------------------------------
	inline LONG
	Win32Interlocked::Increment( int volatile& val )
	{
		return ::InterlockedIncrement( (volatile LONG*)&val );
	}

	//------------------------------------------------------------------
	/** @brief	interlocked decrement */
	//------------------------------------------------------------------
	inline LONG
	Win32Interlocked::Decrement( int volatile& val )
	{
		return ::InterlockedDecrement( (volatile LONG*)&val );
	}

	//------------------------------------------------------------------
	/** @brief	interlocked add */
	//------------------------------------------------------------------
	inline void
	Win32Interlocked::Add( int volatile& val, int add )
	{
		::InterlockedExchangeAdd((volatile LONG*)&val, add);
	}

	//------------------------------------------------------------------
	/**	*/
	//------------------------------------------------------------------
	inline LONG
	Win32Interlocked::Exchange(int volatile* dest, int value)
	{
		return ::InterlockedExchange((volatile LONG*)dest, value);
	}

	//------------------------------------------------------------------
	/**	*/
	//------------------------------------------------------------------
	inline LONG
	Win32Interlocked::CompareExchange(int volatile* dest, int exchange, int comparand)
	{
		return ::InterlockedCompareExchange((volatile LONG*)dest, exchange, comparand);
	}

}
}

#endif	// __CN_INTERLOCKED_H__
