//================================================================
// File:           : CnThreadObject.h
// Original Author : changhee
// Creation Date   : 2007. 4. 9
//================================================================
#pragma once

namespace Cindy
{
	//================================================================
	/** CnThreadObject
	    @author    changhee
		@since     2007. 4. 9
		@remarks   MultiThread's syncronization - critical_section( by Win32 )
					cf) "Modern C++ Design" book's Loki library - thread
	*/
	//================================================================
	template <class TYPE>
	class CnThreadObject
	{
		mutable CRITICAL_SECTION mtx_;
	public:
		CnThreadObject()
		{
			::InitializeCriticalSectionAndSpinCount( &mtx_, 4000 );
		}
		~CnThreadObject()
        {
            ::DeleteCriticalSection(&mtx_);
        }

        class Lock;
        friend class Lock;
        
        class Lock
        {
            CnThreadObject const& host_;
            
            Lock(const Lock&);
            Lock& operator=(const Lock&);
        public:
            explicit Lock( const CnThreadObject& host ) : host_(host)
            {
                ::EnterCriticalSection(&host_.mtx_);
            }
            ~Lock()
            {
                ::LeaveCriticalSection(&host_.mtx_);
            }
        };

        typedef volatile TYPE VolatileType;

        typedef LONG IntType; 

        static IntType AtomicIncrement(volatile IntType& lval)
        { return InterlockedIncrement(&const_cast<IntType&>(lval)); }
        
        static IntType AtomicDecrement(volatile IntType& lval)
        { return InterlockedDecrement(&const_cast<IntType&>(lval)); }
        
        static void AtomicAssign(volatile IntType& lval, IntType val)
        { InterlockedExchange(&const_cast<IntType&>(lval), val); }
        
        static void AtomicAssign(IntType& lval, volatile IntType& val)
        { InterlockedExchange(&lval, val); }
	};
}