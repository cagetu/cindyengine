//================================================================
// File:               : CnTaskManager.h
// Original Author     : changhee
//================================================================
#ifndef __CN_TRHEAD_TASK_MANAGER_H__
#define __CN_TRHEAD_TASK_MANAGER_H__

#include "CnTask.h"
#include "CnTaskScheduler.h"
#include "CnTaskWorkingThread.h"
#include "../../Foundation/CnSmartPointer.h"

namespace Cindy
{
	namespace Threading
	{
		//==================================================================
		/** Task Manager Class
			@author		cagetu
			@brief		Thread의 기본 작업 단위인 Task의 관리 객체
			@see		GPG7. Multithreaded job system
		*/
		//==================================================================
		class TaskManager
		{
		public:
			TaskManager();
			virtual ~TaskManager();

			void Open( uint NumThreads = 2 );
			void Close();

			// Worker
			std::vector<Pointer<TaskWorkingThread> >& GetWorkThreads();

			// Task
			TaskHandle CreateTask( TaskFunction Function, void* Context, TaskPriority Priority );
			TaskHandle CreateAndScheduleTask( TaskFunction Function, void* Context, TaskPriority Priority );

			void ScheduleTask( TaskHandle Handle );
			bool GetNextTask( Task& Result );

			void WakeUpScheduler();

			static TaskManager* Instance();
		private:
			TaskManager( const TaskManager& Other );
			TaskManager& operator =( const TaskManager& Other );

			TaskScheduler	m_Scheduler;	//!< 작업 스케쥴러
			std::vector<Pointer<TaskWorkingThread> >	m_WorkingThreads;	//!< 작업 쓰레드들

			int		m_NextTaskIndex;	//!< 다음 처리할 작업 인덱스
			Win32CriticalSection	m_CriticalSection;

			static TaskManager*	ms_Instance;
		};

		//------------------------------------------------------------------
		inline
		TaskManager* TaskManager::Instance()
		{
			return ms_Instance;
		}

		//------------------------------------------------------------------
		inline
		std::vector<Pointer<TaskWorkingThread> >& TaskManager::GetWorkThreads()
		{
			return m_WorkingThreads;
		}
	}
}

#endif	// __CN_TRHEAD_TASK_MANAGER_H__