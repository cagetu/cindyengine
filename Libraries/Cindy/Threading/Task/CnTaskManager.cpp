//================================================================
// File:               : CnTaskManager.cpp
// Related Header File : CnTaskManager.h
// Original Author     : changhee
// Creation Date       : 2009. 2. 19
//================================================================
#include "../../Cindy.h"
#include "CnTaskManager.h"

namespace Cindy
{
	namespace Threading
	{
		TaskManager* TaskManager::ms_Instance = 0;
		/// Const
		TaskManager::TaskManager()
		{
			assert( !ms_Instance );
			ms_Instance = this;
		}
		/// Dest
		TaskManager::~TaskManager()
		{
			Close();
			ms_Instance = 0;
		}

		//------------------------------------------------------------------
		/**	@brief	초기화
		*/
		void TaskManager::Open( uint NumThreads )
		{
			uint numThreads = NumThreads - 1; 
			m_WorkingThreads.reserve( numThreads );

			for (uint i=0; i< numThreads; ++i)
			{
				m_WorkingThreads[i] = new TaskWorkingThread();
				m_WorkingThreads[i]->Start();
			}

			m_Scheduler.Start();
		}

		//------------------------------------------------------------------
		/** @brief	끝
		*/
		void TaskManager::Close()
		{
			m_Scheduler.Stop();

			uint numThreads = (uint)m_WorkingThreads.size();
			for (uint i=0; i< numThreads; ++i)
			{
				m_WorkingThreads[i]->Stop();
			}
			m_WorkingThreads.clear();
		}

		//------------------------------------------------------------------
		/**	@brief	Task 생성
		*/
		TaskHandle TaskManager::CreateTask( TaskFunction Function, void* Context, TaskPriority Priority )
		{
			return TaskHandle();
		}

		//------------------------------------------------------------------
		/**	@brief	Task 생성 및 Schedule
		*/
		TaskHandle TaskManager::CreateAndScheduleTask( TaskFunction Function, void* Context, TaskPriority Priority )
		{
			return TaskHandle();
		}

		//------------------------------------------------------------------
		/**	@brief	Task Schedule
		*/
		void TaskManager::ScheduleTask( TaskHandle Handle )
		{
			Win32CriticalSection::Scope lock( &m_CriticalSection );
		}

		//------------------------------------------------------------------
		/** @brief	다음 작업을 얻어온다.
		*/
		bool TaskManager::GetNextTask( Task& Result )
		{
			Win32CriticalSection::Scope lock( &m_CriticalSection );

			return true;
		}

		//------------------------------------------------------------------
		/**	@brief	스케쥴러를 활성화 시킨다.
		*/
		void TaskManager::WakeUpScheduler()
		{
			m_Scheduler.WakeUp();
		}
	}
}
