//================================================================
// File:               : CnTaskWorkingThread.h
// Original Author     : changhee
//================================================================
#ifndef __CN_TRHEAD_TASK_WORKING_THREAD_H__
#define __CN_TRHEAD_TASK_WORKING_THREAD_H__

#include "../../Foundation/CnRefCount.h"
#include "../CnThread.h"
#include "CnTask.h"

namespace Cindy
{
	namespace Threading
	{
		//==================================================================
		/** TaskWorkingThread Class
			@author		cagetu
			@brief		Thread Task 작업 쓰레드
			@see		GPG7. Multithreaded job system
		*/
		//==================================================================
		class TaskWorkingThread : public Win32Thread, public CnRefCount
		{
		public:
			TaskWorkingThread();
			virtual ~TaskWorkingThread();

			void SetAssignedTask( const Task& AssignedTask );
			const Task& GetAssgiendTask() const;

			void SetAssignedTaskType( const wchar* AssignedTaskType );
			const CnString& GetAssignedTaskType() const;

			Win32Event& GetDataIsReadyEvent();
			Win32Event& GetWaitForDataEvent();
		protected:
			void	DoWork() override;

		private:
			TaskWorkingThread( const TaskWorkingThread& Other );
			TaskWorkingThread& operator =( const TaskWorkingThread& Other );

			Task		m_AssignedTask;
			CnString	m_AssignedTaskType;

			Win32Event	m_DataIsReadyEvent;
			Win32Event	m_WaitForDataEvent;
		};

		//------------------------------------------------------------------
		inline
		void TaskWorkingThread::SetAssignedTask( const Task& AssignedTask )
		{
			m_AssignedTask = AssignedTask;
		}
		//------------------------------------------------------------------
		inline
		const Task& TaskWorkingThread::GetAssgiendTask() const
		{
			return m_AssignedTask;
		}

		//------------------------------------------------------------------
		inline
		void TaskWorkingThread::SetAssignedTaskType( const wchar* AssignedTaskType )
		{
			m_AssignedTaskType = AssignedTaskType;
		}
		//------------------------------------------------------------------
		inline
		const CnString& TaskWorkingThread::GetAssignedTaskType() const
		{
			return m_AssignedTaskType;
		}

		//------------------------------------------------------------------
		inline
		Win32Event& TaskWorkingThread::GetDataIsReadyEvent()
		{
			return m_DataIsReadyEvent;
		}

		//------------------------------------------------------------------
		inline
		Win32Event& TaskWorkingThread::GetWaitForDataEvent()
		{
			return m_WaitForDataEvent;
		}
	}
}

#endif	// __CN_TRHEAD_TASK_WORKING_THREAD_H__