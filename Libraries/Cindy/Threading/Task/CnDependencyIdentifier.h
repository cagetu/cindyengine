//================================================================
// File:               : CnDependencyIdentifier.h
// Original Author     : changhee
//================================================================
#ifndef __CN_THREAD_DEPENDENCY_IDENTIFIER_H__
#define __CN_THREAD_DEPENDENCY_IDENTIFIER_H__

namespace Cindy
{
	namespace Threading
	{
		//==================================================================
		/** DependencyIdentifier Class
			@author		cagetu
			@brief		Task�� Dependency Id ��ü
			@see		GPG7. Multithreaded job system
		*/
		//==================================================================
		class DependencyIdentifier
		{
		public:
			DependencyIdentifier();
			DependencyIdentifier( const int TableIndex, const int UniqueIndex );

			bool operator ==( const DependencyIdentifier& Other ) const;

			void	SetTableIndex( const int Index );
			int		GetTableIndex() const;

			void	SetUniqueIndex( const int Index );
			int		GetUniqueIndex() const;
		private:
			int m_TableIndex;
			int	m_UniqueIndex;
		};

		//------------------------------------------------------------------
		inline
		DependencyIdentifier::DependencyIdentifier()
			: m_TableIndex(-1)
			, m_UniqueIndex(-1)
		{
		}
		//------------------------------------------------------------------
		inline
		DependencyIdentifier::DependencyIdentifier( const int TableIndex, const int UniqueIndex )
			: m_TableIndex(TableIndex)
			, m_UniqueIndex(UniqueIndex)
		{
		}

		//------------------------------------------------------------------
		inline
		bool DependencyIdentifier::operator ==( const DependencyIdentifier& Other ) const
		{
			return m_TableIndex == Other.m_TableIndex && m_UniqueIndex == Other.m_UniqueIndex;
		}

		//------------------------------------------------------------------
		inline
		void DependencyIdentifier::SetTableIndex( int Index )
		{
			m_TableIndex = Index;
		}
		//------------------------------------------------------------------
		inline
		int DependencyIdentifier::GetTableIndex() const
		{
			return m_TableIndex;
		}

		//------------------------------------------------------------------
		inline
		void DependencyIdentifier::SetUniqueIndex( int Index )
		{
			m_UniqueIndex = Index;
		}
		//------------------------------------------------------------------
		inline
		int DependencyIdentifier::GetUniqueIndex() const
		{
			return m_UniqueIndex;
		}
	}
}

#endif	// __CN_THREAD_DEPENDENCY_IDENTIFIER_H__