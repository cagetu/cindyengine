//================================================================
// File:               : CnDependencyManager.cpp
// Related Header File : CnDependencyManager.h
// Original Author     : changhee
// Creation Date       : 2009. 2. 19
//================================================================
#include "../../Cindy.h"
#include "CnDependencyManager.h"

namespace Cindy
{
	namespace Threading
	{
		DependencyManager* DependencyManager::ms_Instance = 0;
		// Const
		DependencyManager::DependencyManager()
		{
			assert( !ms_Instance );
			ms_Instance = this;
		}
		// Dest
		DependencyManager::~DependencyManager()
		{
			ms_Instance = 0;
		}

		//------------------------------------------------------------------
		void DependencyManager::OnFinishTask( const TaskHandle& Handle )
		{
			Win32CriticalSection::Scope lock(&m_CriticalSection);
		}

		//------------------------------------------------------------------

		//------------------------------------------------------------------
	}
}
