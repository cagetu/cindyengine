//================================================================
// File:               : CnTaskWorkingThread.cpp
// Related Header File : CnTaskWorkingThread.h
// Original Author     : changhee
// Creation Date       : 2009. 2. 26
//================================================================
#include "../../Cindy.h"
#include "CnTaskWorkingThread.h"
#include "CnTaskManager.h"
#include "CnDependencyManager.h"

namespace Cindy
{
	namespace Threading
	{
		TaskWorkingThread::TaskWorkingThread()
		{
#pragma TODO("Event 설정을 잘 확인해야 한다")
		}
		TaskWorkingThread::~TaskWorkingThread()
		{
		}

		//------------------------------------------------------------------
		/**	@desc	할당받은 작업을 처리하고, 테스크 관리자의 스케쥴러에게 새로운
					작업을 전달하고, Dependency관리자에게도 작업의 끝을 알린다.
		*/
		void TaskWorkingThread::DoWork()
		{
			while (this->IsRunning())
			{
				m_DataIsReadyEvent.Wait();

				m_AssignedTask.Execute();

				DependencyManager::Instance()->OnFinishTask( m_AssignedTask.GetHandle() );
				TaskManager::Instance()->WakeUpScheduler();

				m_WaitForDataEvent.Signal();
			}
		}
	}
}
