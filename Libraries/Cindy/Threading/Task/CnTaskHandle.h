//================================================================
// File:               : CnTaskHandler.h
// Original Author     : changhee
//================================================================
#ifndef __CN_THREAD_TASK_HANDLE_H__
#define __CN_THREAD_TASK_HANDLE_H__

#include "CnDependencyIdentifier.h"

namespace Cindy
{
	namespace Threading
	{
		typedef int	TaskID;

		//==================================================================
		/** Handle Class
			@author		cagetu
			@brief		Task�� Handle ��ü
			@see		GPG7. Multithreaded job system
		*/
		//==================================================================
		class TaskHandle
		{
		public:
			TaskHandle();
			TaskHandle( const TaskHandle& Other );
			TaskHandle( const TaskID Index, const DependencyIdentifier& DependencyId ); 

			TaskHandle& operator =( const TaskHandle& Other );
			bool operator ==( const TaskHandle& Other ) const;

			void SetIndex( const TaskID Index );
			TaskID GetIndex() const;

			void SetDependencyId( const DependencyIdentifier& DependencyId );
			const DependencyIdentifier& GetDependencyId() const;
		private:
			TaskID	m_Index;	//!< �۾� �ε���
			DependencyIdentifier m_DependencyId;
		};

		//------------------------------------------------------------------
		inline
		TaskHandle::TaskHandle()
			: m_Index(-1)
			, m_DependencyId()
		{
		}
		//------------------------------------------------------------------
		inline
		TaskHandle::TaskHandle( const TaskHandle& Other )
			: m_Index(Other.m_Index)
			, m_DependencyId(Other.m_DependencyId)
		{
		}
		//------------------------------------------------------------------
		inline
		TaskHandle::TaskHandle( const TaskID Index, const DependencyIdentifier& DependencyId )
			: m_Index(Index)
			, m_DependencyId(DependencyId)
		{
		}

		//------------------------------------------------------------------
		inline
		TaskHandle& TaskHandle::operator =( const TaskHandle& Other )
		{
			m_Index = Other.m_Index;
			m_DependencyId = Other.m_DependencyId;

			return *this;
		}
		//------------------------------------------------------------------
		inline
		bool TaskHandle::operator ==( const TaskHandle& Other ) const
		{
			return m_Index == Other.m_Index && m_DependencyId == Other.m_DependencyId;
		}

		//------------------------------------------------------------------
		inline
		void TaskHandle::SetIndex( const TaskID Index )
		{
			m_Index = Index;
		}
		//------------------------------------------------------------------
		inline
		TaskID TaskHandle::GetIndex() const
		{
			return m_Index;
		}

		//------------------------------------------------------------------
		inline
		void TaskHandle::SetDependencyId( const DependencyIdentifier& DependencyId )
		{
			m_DependencyId = DependencyId;
		}
		//------------------------------------------------------------------
		inline
		const DependencyIdentifier& TaskHandle::GetDependencyId() const
		{
			return m_DependencyId;
		}
	}
}

#endif	// __CN_THREAD_TASK_HANDLE_H__