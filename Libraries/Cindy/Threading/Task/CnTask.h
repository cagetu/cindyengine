//================================================================
// File:               : CnTask.h
// Original Author     : changhee
//================================================================
#ifndef __CN_TRHEAD_TASK_H__
#define __CN_TRHEAD_TASK_H__

#include "CnTaskHandle.h"
#include "CnTaskPriority.h"
#include "CnTaskFunction.h"

namespace Cindy
{
	namespace Threading
	{
		//==================================================================
		/** Task Class
			@author		cagetu
			@brief		Thread의 기본 작업 단위인 Task 객체
			@todo		Task를 어떻게 구성할지를 다시한번 고민해봐야 한다.

						대략 이렇게 처리하는 것이 좋을지도...
						class AbstractTask
						{
						public:
							virtual void	Execute() = 0;
						};
		*/
		//==================================================================
		class Task
		{
		public:
			Task();
			Task( const Task& Other );
			virtual ~Task();

			// Operator
			Task& operator =( const Task& Other );
			bool operator <( const Task& Other ) const;

			// Handle
			void SetHandle( const TaskHandle& Handle );
			const TaskHandle& GetHandle() const;

			// Priority
			void SetPriority( const TaskPriority& Priority );
			const TaskPriority& GetPriority() const;

			// Function
			void SetFunction( const TaskFunction& Function );
			const TaskFunction& GetFunction() const;

			// Function's Parameter
			void SetContext( void* Context );
			void* GetContext() const;

			void Execute();
		private:
			TaskHandle		m_Handle;	//!< 핸들
			TaskPriority	m_Priority;	//!< 우선순위
			TaskFunction	m_Function;	//!< 처리 함수
			void*			m_Context;	//!< 함수의 매개변수
		};

		//------------------------------------------------------------------
		inline
		bool Task::operator <( const Task& Other ) const
		{
			return m_Priority < Other.m_Priority;
		}

		//------------------------------------------------------------------
		/** @brief	Task함수를 실행한다. */
		inline
		void Task::Execute()
		{
			m_Function( m_Context );
		}

		//------------------------------------------------------------------
		inline
		void Task::SetHandle( const TaskHandle& Handle )
		{
			m_Handle = Handle;
		}
		//------------------------------------------------------------------
		inline
		const TaskHandle& Task::GetHandle() const
		{
			return m_Handle;
		}

		//------------------------------------------------------------------
		inline
		void Task::SetPriority( const TaskPriority& Priority )
		{
			m_Priority = Priority;
		}
		//------------------------------------------------------------------
		inline
		const TaskPriority& Task::GetPriority() const
		{
			return m_Priority;
		}

		//------------------------------------------------------------------
		inline
		void Task::SetFunction( const TaskFunction& Function )
		{
			m_Function = Function;
		}
		//------------------------------------------------------------------
		inline
		const TaskFunction& Task::GetFunction() const
		{
			return m_Function;
		}

		//------------------------------------------------------------------
		inline
		void Task::SetContext( void* Context )
		{
			m_Context = Context;
		}
		//------------------------------------------------------------------
		inline
		void* Task::GetContext() const
		{
			return m_Context;
		}
	}
}

#endif	// __CN_TRHEAD_TASK_H__