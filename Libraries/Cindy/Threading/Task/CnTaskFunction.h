//================================================================
// File:               : CnTaskFunction.h
// Original Author     : changhee
//================================================================
#ifndef __CN_TRHEAD_TASK_FUNCTION_H__
#define __CN_TRHEAD_TASK_FUNCTION_H__

namespace Cindy
{
	namespace Threading
	{
		typedef void (*TaskFunction)( void* pObject );
	}
}

#endif	// __CN_TRHEAD_TASK_FUNCTION_H__