//================================================================
// File:               : CnDependencyManager.h
// Original Author     : changhee
//================================================================
#ifndef __CN_THREAD_DEPENDENCY_MANAGER_H__
#define __CN_THREAD_DEPENDENCY_MANAGER_H__

#include "CnDependencyIdentifier.h"
#include "CnTaskHandle.h"
#include "../CnCriticalSection.h"

namespace Cindy
{
	namespace Threading
	{
		//==================================================================
		/** DependencyManager Class
			@author		cagetu
			@brief		Task�� Dependency Id ��ü
			@see		GPG7. Multithreaded job system
		*/
		//==================================================================
		class DependencyManager
		{
		public:
			DependencyManager();
			virtual ~DependencyManager();

			void OnFinishTask( const TaskHandle& Handle );

			static DependencyManager* Instance();
		private:
			DependencyManager( const DependencyManager& Other );
			DependencyManager& operator =( const DependencyManager& Other );

			Win32CriticalSection	m_CriticalSection;

			static DependencyManager*	ms_Instance;
		};

		//------------------------------------------------------------------
		inline
		DependencyManager* DependencyManager::Instance()
		{
			return ms_Instance;
		}
	}
}

#endif	// __CN_THREAD_DEPENDENCY_MANAGER_H__