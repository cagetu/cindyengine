//================================================================
// File:               : CnTaskScheduler.cpp
// Related Header File : CnTaskScheduler.h
// Original Author     : changhee
// Creation Date       : 2009. 2. 19
//================================================================
#include "CnTaskScheduler.h"
#include "CnTaskManager.h"

namespace Cindy
{
	namespace Threading
	{
		TaskScheduler::TaskScheduler()
		{
		}
		TaskScheduler::~TaskScheduler()
		{
		}

		//------------------------------------------------------------------
		/**	@brief	깨운다.
					원래는 Event가 Pulse로 되어 있는데, 일단 Signal로 처리
		*/
		void TaskScheduler::WakeUp()
		{
			m_WaitForTaskEvent.Signal();
		}

		//------------------------------------------------------------------
		/**	@brief	스케쥴러 처리
		*/
		void TaskScheduler::DoWork()
		{
			std::vector<Pointer<TaskWorkingThread> >& workers = TaskManager::Instance()->GetWorkThreads();
			ushort numWorkers = (ushort)workers.size();

			std::vector<HANDLE>	waitForDataTable;
			waitForDataTable.resize( numWorkers );

			for (ushort i=0; i < numWorkers; ++i)
			{
				waitForDataTable[i] = workers[i]->GetWaitForDataEvent();
			}

			DWORD numOfWaitForDataTable = (DWORD)waitForDataTable.size();
			while (this->IsRunning())
			{
				int objectIndex = ::WaitForMultipleObjects( numOfWaitForDataTable,
															&waitForDataTable[0],
															false,
															INFINITE );

				objectIndex -= WAIT_OBJECT_0;

				Task nextTask;
				if (TaskManager::Instance()->GetNextTask(nextTask))
				{
					workers[objectIndex]->SetAssignedTask( nextTask );
					workers[objectIndex]->GetWaitForDataEvent().NonSignal();
					workers[objectIndex]->GetDataIsReadyEvent().Signal();
				}
				else
				{
					m_WaitForTaskEvent.Wait();
				}
			}

		}

	}
}
