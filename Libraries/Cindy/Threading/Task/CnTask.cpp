//================================================================
// File:               : CnTask.cpp
// Related Header File : CnTask.h
// Original Author     : changhee
// Creation Date       : 2009. 2. 19
//================================================================
#include "CnTask.h"

namespace Cindy
{
	namespace Threading
	{
		// Const
		Task::Task()
			: m_Handle()
			, m_Priority( TaskPriority_Default )
			, m_Function( 0 )
			, m_Context( 0 )
		{
		}
		Task::Task( const Task& Other )
			: m_Handle( Other.m_Handle )
			, m_Priority( Other.m_Priority )
			, m_Function( Other.m_Function )
			, m_Context( Other.m_Context )
		{
		}
		// Dest
		Task::~Task()
		{
		}

		//------------------------------------------------------------------
		Task& Task::operator =( const Task& Other )
		{
			m_Handle = Other.m_Handle;
			m_Priority = Other.m_Priority;
			m_Function = Other.m_Function;
			m_Context = Other.m_Context;
			return *this;
		}

		//------------------------------------------------------------------

		//------------------------------------------------------------------
	}
}
