//================================================================
// File:               : CnTaskPriority.h
// Original Author     : changhee
//================================================================
#ifndef __CN_TRHEAD_TASK_PRIORITY_H__
#define __CN_TRHEAD_TASK_PRIORITY_H__

namespace Cindy
{
	namespace Threading
	{
		enum TaskPriority
		{
			TaskPriority_High = 0,
			TaskPriority_Normal,
			TaskPriority_Low,
			TaskPriority_Idle,
			TaskPriority_Count,
			TaskPriority_Default = TaskPriority_Normal,
			TaskPriority_None = -1
		};
	}
}

#endif	// __CN_TRHEAD_TASK_PRIORITY_H__