//================================================================
// File:               : CnTaskScheduler.h
// Original Author     : changhee
//================================================================
#ifndef __CN_TRHEAD_TASK_SCHEDULER_H__
#define __CN_TRHEAD_TASK_SCHEDULER_H__

#include "../CnThread.h"

namespace Cindy
{
	namespace Threading
	{
		//==================================================================
		/** TaskScheduler Class
			@author		cagetu
			@brief		Thread Task �����췯..
			@see		GPG7. Multithreaded job system
		*/
		//==================================================================
		class TaskScheduler : public Win32Thread
		{
		public:
			TaskScheduler();
			virtual ~TaskScheduler();

			Win32Event&	GetWaitForTaskEvent();

			void	WakeUp();
		protected:
			virtual void	DoWork() override;

		private:
			TaskScheduler( const TaskScheduler& Other );
			TaskScheduler& operator =( const TaskScheduler& Other );

			Win32Event	m_WaitForTaskEvent;
		};

		inline
		Win32Event& TaskScheduler::GetWaitForTaskEvent()
		{
			return m_WaitForTaskEvent;
		}
	}
}

#endif	// __CN_TRHEAD_TASK_SCHEDULER_H__