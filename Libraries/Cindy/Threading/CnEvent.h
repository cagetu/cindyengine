// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CN_WIN32EVENT_H__
#define __CN_WIN32EVENT_H__

#include "../Cindy.h"

namespace Cindy
{
namespace Threading
{
	//==================================================================
	/** Win32Event
		@author		cagetu
		@brief		Win32Event 객체
	*/
	//==================================================================
	class Win32Event
	{
	public:
		Win32Event();
		~Win32Event();

		void	Signal();
		void	NonSignal();

		void	Wait();
		bool	WaitTimeout( DWORD timeOut ) const;
		bool	Peek() const;

		operator HANDLE();

	private:
		HANDLE	m_Event;
	};

	/// Const
	inline
	Win32Event::Win32Event()
	{
		m_Event = CreateEvent( NULL, FALSE, FALSE, NULL );
		assert(0 != m_Event);
	}
	/// Dest
	inline
	Win32Event::~Win32Event()
	{
		assert(0 != m_Event);
		CloseHandle(m_Event);
		m_Event = 0;
	}

	//------------------------------------------------------------------------------
	/** @desc	Handle 반환 */
	//------------------------------------------------------------------------------
	inline
	Win32Event::operator HANDLE()
	{
		return m_Event;
	}

	//------------------------------------------------------------------------------
	/** @desc	신호 보내기, 깨우기 */
	//------------------------------------------------------------------------------
	inline
	void Win32Event::Signal()
	{
		assert(0 != m_Event);
		SetEvent( m_Event );
	}

	//------------------------------------------------------------------------------
	/** @desc	NonSignal 상태로 변환, 재우기 */
	//------------------------------------------------------------------------------
	inline
	void Win32Event::NonSignal()
	{
		assert(0 != m_Event);
		::ResetEvent( m_Event );
	}

	//------------------------------------------------------------------------------
	/** @desc	무제한 기다렷~!! */
	//------------------------------------------------------------------------------
	inline
	void Win32Event::Wait()
	{
		assert(0 != m_Event);
		WaitForSingleObject( m_Event, INFINITE );
	}

	//------------------------------------------------------------------------------
	/** @desc	timeout이 되면, false를 반환하고,
				signal이 되면, true를 반환한다
	*/
	//------------------------------------------------------------------------------
	inline
	bool Win32Event::WaitTimeout( DWORD timeOut ) const
	{
		assert(0 != m_Event);
		DWORD res = WaitForSingleObject( m_Event, timeOut );
		return (WAIT_TIMEOUT == res) ? false : true;
	}

	//------------------------------------------------------------------------------
	/** @desc	이벤트가 반환되었는지 체크하고 바로 반환 */
	//------------------------------------------------------------------------------
	inline
	bool Win32Event::Peek() const
	{
		assert(0 != m_Event);
		DWORD res = WaitForSingleObject( m_Event, 0 );
		return (WAIT_TIMEOUT == res) ? false : true;
	}
}
}

#endif	// __CN_WIN32EVENT_H__