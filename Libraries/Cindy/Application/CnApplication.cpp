//================================================================
// File:               : CnApplication.cpp
// Related Header File : CnApplication.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 23
//================================================================
#include "Cindy.h"
#include "CnApplication.h"
#include "Util/CnLog.h"
#include "Scene/Component/CindySceneComponents.h"
#include "Graphics/Base/CnRenderWindow.h"
#include "Graphics/CnTextRenderer.h"
#include "Material/CnShaderManager.h"
#include "Material/CnTextureManager.h"
#include "Material/CnMaterialDataManager.h"

#include <MoCommon/Memory/MoMemoryManager.h>

namespace Cindy
{
	//----------------------------------------------------------------
	/// Const / Dest
	CnApplication::CnApplication()
		: m_bPaused(false)
		, m_pLogPtr(0)
		, m_pTextRenderer( NULL )
	{
		MoCommon::MoMemoryManager::Initialize();
		m_pCindy = CnNew CnCindy();
		m_pLogPtr = new CnLogProxy();
	}
	//----------------------------------------------------------------
	CnApplication::~CnApplication()
	{
		//Close();
		SAFEDEL( m_pLogPtr );

		MoCommon::MoMemoryManager::OutputReport( L"MemoryLog.txt" );
		MoCommon::MoMemoryManager::Shutdown();
	}

	//----------------------------------------------------------------
	/** Startup
		@brief      프레임웍을 초기화한다.
		@param        none
		@return       true/false : 성공 여부
	*/
	bool CnApplication::Open()
	{
		m_pCindy->Open();

		m_Timer.Start();
		m_FrameTime = 0;
		m_CurTime = 0;
		return true;
	}

	//----------------------------------------------------------------
	/** Shutdown
		@brief      프레임웍의 구성물을 삭제한다.
		@param        none
		@return       none
	*/
	void CnApplication::Close()
	{
		m_Timer.Stop();

		m_pWorld = 0;
		m_pSceneGraph = 0;

		m_pCindy->Close();
		SAFEDEL( m_pCindy );
	}

	//----------------------------------------------------------------
	/**
	*/
	bool CnApplication::Initialize()
	{
		if (m_pTextRenderer == 0)
		{
			m_pTextRenderer = new CnTextRendererProxy();
			m_pTextRenderer->Open();
		}

		//m_pMaterialManager = new CnMaterialDataManager();
		m_pShaderManager = CnNew CnShaderManager();
		m_pTextureManager = CnNew CnTextureManager();

		m_FrameManager.Initialize();
		return true;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnApplication::DeInitialize()
	{
		m_Frame = 0;
		m_FrameManager.DeInitialize();

		SAFEDEL(m_pTextRenderer);

		//SAFEDEL(m_pMaterialManager);
		SAFEDEL(m_pShaderManager);
		SAFEDEL(m_pTextureManager);
	}

	//----------------------------------------------------------------
	/** Run
		@brief      프레임웍 실행
		@param        none
		@return       none
	*/
	void CnApplication::Run()
	{
		if (!m_bPaused)
		{
			// game thread를 지키기 위해, graphcis 쓰레드에 앞서서 실행하기 위해,
			// 그래픽 프레임과 동기화.

			// 입력 처리
			OnProcessInput();

			// 시간 갱신
			UpdateTime();

			// 게임 실행
			OnUpdateFrame();

			// Rendering
			OnBeginFrame();
				OnFrame();
			OnEndFrame();
		}
	}

	//----------------------------------------------------------------
	/** AttachWindow
		@brief      프레임웍을 초기화한다.
		@param        hWnd : 윈도우 핸들
		@param		  pStrTitle : 윈도우 타이틀
		@param		  nWidth : 윈도우 가로
		@param		  nHeight : 윈도우 세로
		@param		  usColorDepth : 칼라 비트
		@param		  usRefreshRate : 화면 주사율
		@param		  bFullScreen : 풀 스크린 모드
		@param		  bThreadSafe : 멀티 쓰레드 사용여부
		@param		  bSetCurrentTarget : 현재 윈도우를 사용할 것이냐..
		@return       none
	*/
	CnRenderWindow* CnApplication::AttachWindow( HWND hWnd,
												 const wchar* pStrTitle,
												 int nWidth,
												 int nHeight,
												 ushort usColorDepth, 
												 ushort usRefreshRate,
												 bool bFullScreen,
												 bool bThreadSafe,
												 bool bSetCurrentTarget )
	{
		CnRenderWindow* pRenderWindow = RenderDevice->CreateRenderWindow( hWnd,
																		  pStrTitle,
																		  nWidth,
																		  nHeight,
																		  usColorDepth,
																		  usRefreshRate,
																		  bFullScreen,
																		  bThreadSafe,
																		  bSetCurrentTarget );
		return pRenderWindow;
	}

	//----------------------------------------------------------------
	/** Set Scene
		@brief      장면 설정
		@param        strName : 장면 이름
		@return       none
	*/
	void CnApplication::SetScene( const CnString& strName )
	{
		// Setup Scene..
		m_pWorld = m_pCindy->AddScene( L"Scene" );

		// 장면 그래프
		m_pSceneGraph = CnSceneGraph::Create();
		m_pSceneGraph->SetName( L"MainSceneGraph" );

		Scene::UpdateScene* sceneUpdator = Scene::UpdateScene::Create();
		sceneUpdator->AddSceneGraph( m_pSceneGraph );

		//Scene::RenderModel* modelRenderer = Scene::RenderModel::Create();
		//modelRenderer->Link( sceneUpdator );
		Scene::RenderGeometry* modelRenderer = Scene::RenderGeometry::Create();
		modelRenderer->Link( sceneUpdator );

		m_pWorld->AddComponent( sceneUpdator );
		m_pWorld->AddComponent( Scene::BeginScene::Create() );
		m_pWorld->AddComponent( modelRenderer );
		m_pWorld->AddComponent( Scene::EndScene::Create() );
	}

	//----------------------------------------------------------------
	/** Pause
		@brief      멈춤 상태 설정
		@param        bPause : 멈충 여부 설정
		@return       none
	*/
	void CnApplication::Pause( bool bPause )
	{
		m_bPaused = bPause;
		if (bPause)
		{
			m_Timer.Stop();
		}
		else
		{
			m_Timer.Start();
		}
	}

	//----------------------------------------------------------------
	/** Is Paused ?
		@brief	  현재 멈춤 상태   
		@param        none
		@return       bool : m_bPaused
	*/
	bool CnApplication::IsPaused() const
	{
		return m_bPaused;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnApplication::SetLogFile( const MoString& strName, const MoString& strOutput, bool bOutputDebug, bool bOutputConsole )
	{
		m_pLogPtr->Add( strName, strOutput, bOutputDebug, bOutputConsole );
	}

	//----------------------------------------------------------------
	/** @brief	시간 갱신
	*/
	void CnApplication::UpdateTime()
	{
		System::Second curTime = m_Timer.GetTime();
		m_FrameTime = curTime - m_CurTime;
		m_CurTime = curTime;

		CnString fps = ToStr(L"Fps: %.2f", 1/m_FrameTime);
		_debug_text(fps, Math::Vector3(0.0f,0.0f,0.0f), CnColor(1.0f, 0.0f, 0.0f, 1.0f));
	}

	//----------------------------------------------------------------
	/** @brief	입력을 갱신한다. Rendering 되기 전에 처리... 
	*/
	void CnApplication::OnProcessInput()
	{
	}

	//----------------------------------------------------------------
	/** @brief	Application(게임에서 Logic)을 갱신한다. 
	*/
	void CnApplication::OnUpdateFrame()
	{
	}

	//----------------------------------------------------------------
	/** Begin
	*/
	void CnApplication::OnBeginFrame()
	{
		m_FrameManager.Begin();
	}

	//----------------------------------------------------------------
	/** Doing
	*/
	void CnApplication::OnFrame()
	{
		if (m_Frame.IsValid())
		{
			m_Frame->Run();
		}
	}

	//----------------------------------------------------------------
	/** End
	*/
	void CnApplication::OnEndFrame()
	{
		m_FrameManager.End();
	}

	//----------------------------------------------------------------
	/** Mesasge Proc
		@brief      메세지 프로시져 처리
		@return       true/false : 성공 여부
	*/
	bool CnApplication::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
	{
		switch (message)
		{
		case WM_LBUTTONDOWN:
			{
			}
			break;
		case WM_LBUTTONUP:
			{
			}
			break;

		case WM_RBUTTONDOWN:
			{
			}
			break;

		case WM_RBUTTONUP:
			{
			}
			break;

		case WM_MBUTTONDOWN:
			{
			}
			break;
		case WM_MBUTTONUP:
			{
			}
			break;

		case WM_MOUSEMOVE:
			{
			}
			break;

		case WM_MOUSEWHEEL:
			{
			}
			break;
		}

		return false;
	}
}