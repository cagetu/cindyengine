//================================================================
// File:           : CnApplication.h
// Original Author : changhee
// Creation Date   : 2007. 1. 23
//================================================================
#ifndef __CN_APPLICATION_H__
#define __CN_APPLICATION_H__

#include "Scene/CindyScene.h"
#include "Util/CnTimer.h"
#include "CnCindy.h"

namespace Cindy
{
	class CnLogProxy;
	class CnTextRendererProxy;
	class CnShaderManager;
	class CnTextureManager;
	class CnMeshDataManager;

	//================================================================
	/** Application Class
		@author		changhee
		@since      2007. 1. 23
		@remarks	어플리케이션
	*/
	//================================================================
	class CN_DLL CnApplication
	{
		__DeclareUnCopy(CnApplication);
	public:
		CnCindy*			m_pCindy;

		Ptr<CnScene>		m_pWorld;
		SceneGraphPtr		m_pSceneGraph;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		CnApplication();
		virtual ~CnApplication();

		// Common
		bool					Open();
		void					Close();

		// Initialize
		virtual bool			Initialize();
		virtual void			DeInitialize();

		// Run
		void					Run();

		// Window
		virtual CnRenderWindow*	AttachWindow( HWND hWnd, const wchar* pStrTitle,
											  int nWidth, int nHeight, ushort usColorDepth, 
											  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
											  bool bSetCurrentTarget = false );

		// Pause
		void					Pause( bool bPause = true );
		bool					IsPaused() const;

		// Log
		virtual void			SetLogFile( const MoString& strName, const MoString& strOutput, bool bOutputDebug, bool bOutputConsole );

		// MsgProc
		virtual bool			MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

	protected:
		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		bool					m_bPaused;
		CnLogProxy*				m_pLogPtr;
		CnTextRendererProxy*	m_pTextRenderer;

		CnTimer					m_Timer;
		System::Second			m_CurTime;
		System::Second			m_FrameTime;

		CnFrameManager			m_FrameManager;
		Ptr<CnFrame>			m_Frame;
		
		// Resource Managers
		CnShaderManager*		m_pShaderManager;
		CnTextureManager*		m_pTextureManager;

		// Rotation
		short					m_nRotMouseX;
		short					m_nRotMouseY;

		// Zoom
		short					m_nZoomMouseX;
		short					m_nZoomMouseY;

		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		// 장면 설정
		virtual void	SetScene( const CnString& strName );

		// Input
		virtual void	OnProcessInput();

		// Update World
		virtual void	OnUpdateFrame();
		// Begin
		virtual void	OnBeginFrame();
		// Doing
		virtual void	OnFrame();
		// End
		virtual void	OnEndFrame();

	private:
		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		void	UpdateTime();			
	};
}

#endif	// __CN_APPLICATION_H__