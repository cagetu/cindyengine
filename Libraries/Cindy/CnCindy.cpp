//================================================================
// File:               : CnCindy.cpp
// Related Header File : CnCindy.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 24
//================================================================
#include "Cindy.h"
#include "CnCindy.h"
#include "Scene/CnCamera.h"
#include "Scene/CnScene.h"
#include "Graphics/Dx9/CnDx9Renderer.h"

namespace Cindy
{
	__ImplementSingleton(CnCindy);
	//--------------------------------------------------------------
	// Const / Dest
	//--------------------------------------------------------------
	CnCindy::CnCindy()
		: m_pRenderer( NULL )
	{
		__ConstructSingleton;
	}
	CnCindy::~CnCindy()
	{
		__DestructSingleton;
	}

	//--------------------------------------------------------------
	void CnCindy::Open()
	{
		if (m_pRenderer == 0)
		{
			m_pRenderer = new CnRenderer();
			m_pRenderer->Open( CnRenderer::D3D9 );
		}
	}

	//--------------------------------------------------------------
	void CnCindy::Close()
	{
		m_VisibleResolver.Clear();

		m_Cameras.clear();
		m_Scenes.clear();

		m_pRenderer->Close();
		SAFEDEL(m_pRenderer);
	}

	//--------------------------------------------------------------
	/** 카메라를 생성한다.
		@param		strName : 카메라 이름
		@return		CnCamera : 카메라 객체
	*/
	void CnCindy::AddCamera( const Ptr<CnCamera>& Camera )
	{
		CameraMap::iterator iter = m_Cameras.find( Camera->GetName() );
		if (iter == m_Cameras.end())
		{
			if (m_Cameras.empty())
			{
				m_DefaultCamera = Camera;
			}
			else
			{
				if (Camera->IsDefault())
				{
					m_DefaultCamera = Camera;
				}
			}

			m_Cameras.insert( CameraMap::value_type( Camera->GetName(), Camera ) );
		}
	}

	//--------------------------------------------------------------
	/** 카메라를 삭제한다.
		@param		strName : 카메라 이름
		@return		none
	*/
	void CnCindy::RemoveCamera( const wchar* strName )
	{
		CameraMap::iterator iter = m_Cameras.find( strName );
		if (iter == m_Cameras.end())
			return;

		m_Cameras.erase(iter);
	}

	//--------------------------------------------------------------
	/**	카메라 얻어오기
	*/
	Ptr<CnCamera> CnCindy::GetCamera( const wchar* strName ) const
	{
		CameraMap::const_iterator iter = m_Cameras.find( strName );
		if (iter == m_Cameras.end())
			return 0;

		return iter->second;
	}

	//--------------------------------------------------------------
	/** 장면을 생성한다.
		@param		strName : 장면의 이름
		@return		CnScene : 장면 객체
	*/
	Ptr<CnScene> CnCindy::AddScene( const wchar* strName )
	{
		SceneMap::iterator iter = m_Scenes.find( strName );
		if( iter == m_Scenes.end() )
		{
			CnScene* scene = CnNew CnScene( strName );
			m_Scenes.insert( SceneMap::value_type( strName, scene ) );

			return scene;
		}
		return iter->second;
	}

	//--------------------------------------------------------------
	/** 장면을 삭제한다.
		@param		strName : 장면 이름
		@return		none
	*/
	void CnCindy::RemoveScene( const wchar* strName )
	{
		SceneMap::iterator iter = m_Scenes.find( strName );
		if( iter == m_Scenes.end() )
			return;

		m_Scenes.erase(iter);
	}

	//--------------------------------------------------------------
	/**	장면을 얻어오기
	*/
	Ptr<CnScene> CnCindy::GetScene( const wchar* strName ) const
	{
		SceneMap::const_iterator iter = m_Scenes.find( strName );
		if( iter == m_Scenes.end() )
			return 0;

		return iter->second;
	}

	//--------------------------------------------------------------
	/**	장면 그래프 추가
	*/
	Ptr<CnSceneGraph> CnCindy::AddSceneGraph( const wchar* strName )
	{
		SceneGraphMap::iterator iter = m_SceneGraphs.find( strName );
		if( iter == m_SceneGraphs.end() )
		{
			CnSceneGraph* sceneGraph = CnNew CnSceneGraph( strName );
			m_SceneGraphs.insert( SceneGraphMap::value_type( strName, sceneGraph ) );

			return sceneGraph;
		}
		return iter->second;
	}

	//--------------------------------------------------------------
	/**	장면 그래프 제거
	*/
	void CnCindy::RemoveSceneGraph( const wchar* strName )
	{
		SceneGraphMap::iterator iter = m_SceneGraphs.find( strName );
		if( iter == m_SceneGraphs.end() )
			return;

		m_SceneGraphs.erase(iter);
	}

	//--------------------------------------------------------------
	/**	장면 그래프 얻어오기
	*/
	Ptr<CnSceneGraph> CnCindy::GetSceneGraph( const wchar* strName ) const
	{
		SceneGraphMap::const_iterator iter = m_SceneGraphs.find( strName );
		if( iter == m_SceneGraphs.end() )
			return 0;

		return iter->second;
	}
}
