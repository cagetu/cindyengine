//================================================================
// File:               : CnVertexData.inl
// Related Header File : CnVertexData.h
// Original Author     : changhee
// Creation Date       : 2008. 8. 11
//================================================================
//----------------------------------------------------------------
/** @brief	GetVertexDeclaration
*/
inline const Ptr<CnVertexDeclaration>&
CnVertexData::GetDeclaration() const
{
	return m_pDeclaration;
}

//----------------------------------------------------------------
/** @brief	BufferBinder
*/
inline const Ptr<CnVertexBufferBinder>&
CnVertexData::GetBufferBinder() const
{
	return m_pBufferBinder;
}

//----------------------------------------------------------------
/** @brief	AddBuffer
*/
inline void 
CnVertexData::AddBuffer( ushort StreamID, Ptr<CnVertexBuffer>& pBuffer )
{
	m_pBufferBinder->Bind( StreamID, pBuffer );
}

//----------------------------------------------------------------
/** @brief	GetBuffer
*/
inline const Ptr<CnVertexBuffer>&
CnVertexData::GetBuffer( uint StreamID )
{
	return m_pBufferBinder->GetBuffer( StreamID );
}

//----------------------------------------------------------------
/** Set Number Of Vertices
    @brief      버텍스 개수 설정
*/
inline void
CnVertexData::SetNumVertices( uint Number )
{
	m_nNumVertices = Number;
}

//----------------------------------------------------------------
/** Get Number Of Vertices
    @brief      버텍스 개수 반환
*/
inline uint
CnVertexData::GetNumVertices() const
{
	return m_nNumVertices;
}
