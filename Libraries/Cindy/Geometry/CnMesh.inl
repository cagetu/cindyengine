//================================================================
// File:               : CnMesh.inl
// Related Header File : CnMesh.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================
//----------------------------------------------------------------
inline bool
CnMesh::IsSkinned() const
{
	return (m_pSkinnedData != NULL);
}

inline ushort
CnMesh::GetNumSubMeshes() const
{
	return m_usNumSubMeshes;
}

//----------------------------------------------------------------
inline
void CnMesh::SetOccluder( bool bOccluder )
{
	m_bOccluder = bOccluder;
}
inline
bool CnMesh::IsOccluder() const
{
	return m_bOccluder;
}

//----------------------------------------------------------------
/** @brief	영향 받는 라이트들 얻기 */
//----------------------------------------------------------------
inline const CnMesh::LightArray&
CnMesh::GetInfluencedLights() const
{
	return m_InfluencedLights;
}

//----------------------------------------------------------------
/** @brief	버텍스 정보 */
//----------------------------------------------------------------
inline const Ptr<CnVertexData>&
CnMesh::GetVertexData()
{
	return m_VertexData;
}

//----------------------------------------------------------------
/** @brief	카메라와의 거리 */
//----------------------------------------------------------------
inline float
CnMesh::GetDistanceToCamera() const
{
	return m_fDistanceToCamera;
}

//----------------------------------------------------------------
/** @brief	카메라와의 거리가 변경되었는가? */
//----------------------------------------------------------------
inline bool
CnMesh::IsChangedDistanceToCamera() const
{
	return m_IsChangedDistanceToCamera;
}

//----------------------------------------------------------------
/** @brief	카메라와의 거리가 변경되었는가? */
//----------------------------------------------------------------
inline const Math::Matrix44&
CnMesh::GetWorldViewProjection() const
{
	return m_WorldViewProjection;
}
