//================================================================
// File:               : CnMesh.cpp
// Related Header File : CnMesh.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================
#include "Cindy.h"

#include "Scene/CnSceneNode.h"
#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Graphics/CnRenderer.h"
#include "Material/CnMaterial.h"
#include "Material/CnShaderManager.h"
#include "Resource/CnExportedDataDefines.h"
#include "Animation/CnSkeletonInstance.h"
#include "Animation/CnBoneNode.h"
#include "Util/CnLog.h"

#include "CnMesh.h"
#include "CnSubMesh.h"
#include "CnVertexData.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnMesh, CnSceneEntity );

	/// Const/Dest
	CnMesh::CnMesh( const CnString& strName )
		: CnSceneEntity(strName)
		, m_usNumSubMeshes(0)
		, m_bNeedUpdateLights(false)
		, m_pSkeletonInst(0)
		, m_VertexData(0)
		, m_pSkinnedData(0)
		//, m_ShadowMapBuilder(0)
		, m_bOccluder(true)
		, m_fDistanceToCamera(0.0f)
		, m_IsChangedDistanceToCamera(false)
	{
	}
	CnMesh::~CnMesh()
	{
		RemoveAllSubMeshes();
	}

	//================================================================
	/** Add Sub Mesh
		@brief	하위 메쉬 추가
	*/
	//================================================================
	void CnMesh::AddSubMesh( CnSubMesh* pSubMesh )
	{
		pSubMesh->SetParent( this );
		m_SubMeshes.push_back( pSubMesh );

		m_usNumSubMeshes = (ushort)m_SubMeshes.size();
	}

	//----------------------------------------------------------------
	void CnMesh::RemoveSubMesh( CnSubMesh* pSubMesh )
	{
		SubMeshIter i = std::find( m_SubMeshes.begin(), m_SubMeshes.end(), pSubMesh );
		if( i == m_SubMeshes.end() )
			return;

		m_SubMeshes.erase( i );
	}

	//----------------------------------------------------------------
	void CnMesh::RemoveAllSubMeshes()
	{
		std::for_each( m_SubMeshes.begin(), m_SubMeshes.end(), DeleteObject() );
		m_SubMeshes.clear();
	}

	//----------------------------------------------------------------
	CnSubMesh* CnMesh::GetSubMesh( int nIndex ) const
	{
		return m_SubMeshes[nIndex];
	}

	//----------------------------------------------------------------
	/** @brief	매터리얼 설정 */
	//----------------------------------------------------------------
	void CnMesh::SetMaterial( const Ptr<CnMaterialData>& pMaterialData )
	{
		SubMeshIter iend = m_SubMeshes.end();
		for (SubMeshIter i=m_SubMeshes.begin(); i!=iend; ++i)
		{
			CnMaterialData::MtlInfo* mtl = pMaterialData->GetMtl( (*i)->GetMtlID() );
			if (mtl)
			{
				Ptr<CnMaterial> instance = mtl->CreateInstance();
				(*i)->SetMaterial( instance );

				//if (IsSkinned())
				//{
				//	instance->AddFeatureBits( ShaderMgr->FeatureStringToMask(L"Skinned") );
				//}
			}
		}
	}

	//----------------------------------------------------------------
	/** @brief	매터리얼 설정 */
	//----------------------------------------------------------------
	void CnMesh::SetMaterial( const Ptr<CnMaterial>& Material )
	{
		SubMeshIter iend = m_SubMeshes.end();
		for (SubMeshIter i = m_SubMeshes.begin(); i != iend; ++i)
		{
			(*i)->SetMaterial( Material );
		}
	}

	//----------------------------------------------------------------
	/** Set CnVertexData
		@brief		버텍스 버퍼 데이터
	*/
	//----------------------------------------------------------------
	void CnMesh::SetVertexData( const Ptr<CnVertexData>& pVertexData )
	{
		m_VertexData = pVertexData;
	}

	//----------------------------------------------------------------
	/** Set Skinned Data
		@brief		스킨 데이터 세팅
	*/
	//----------------------------------------------------------------
	void CnMesh::SetSkinnedData( Ptr<Export::Skinned>& pSkinnedData )
	{
		m_pSkinnedData = pSkinnedData;
	}

	//----------------------------------------------------------------
	/** Set Skeleton Controller
		@brief		스켈레톤 데이터 세팅
	*/
	//----------------------------------------------------------------
	void CnMesh::SetSkeleton( const Ptr<CnSkeletonInstance>& pSkeleton )
	{
		m_pSkeletonInst = pSkeleton;
	}

	//----------------------------------------------------------------
	/** Set VertexDeclaration
		@brief		Declaration 설정
	*/
	//----------------------------------------------------------------
	void CnMesh::SetVertexDeclaration()
	{
		const VertDeclPtr& declaration = m_VertexData->GetDeclaration();
		RenderDevice->SetVertexDeclaration( declaration );
	}

	//----------------------------------------------------------------
	/** Set Vertices
		@brief		버텍스 버퍼 설정
	*/
	//----------------------------------------------------------------
	bool CnMesh::SetStreamSource( uint nStreamID, uint nOffsetInBytes, bool bSplit )
	{
		if (bSplit)
		{
			Ptr<CnVertexBuffer> buffer = m_VertexData->GetBuffer( nStreamID );
		#ifdef _DEBUG
			Assert( buffer != NULL, L"[CnMesh::SetVertices] Failed 'buffer == null'" );
		#else
			if(buffer == NULL)
				return false;
		#endif
			return RenderDevice->SetStreamSource( nStreamID, nOffsetInBytes, buffer );
		}
		else
		{
#ifdef _MULTIPLE_STREAM_
			return RenderDevice->SetStreamSource( m_VertexData );
#endif // _MULTIPLE_STREAM_
		}

		return false;
	}

	//----------------------------------------------------------------
	uint CnMesh::GetNumVertices() const
	{
		return m_VertexData->GetNumVertices();
	}

	//----------------------------------------------------------------
	/** Get Blend
		@brief		버텍스 버퍼 설정
	*/
	//----------------------------------------------------------------
	ushort CnMesh::GetNumBlend() const
	{
		if (IsSkinned())
			return m_pSkinnedData->num_vertex_blend;

		return 0;
	}

	//----------------------------------------------------------------
	/** Get World Transform Matrix
		@brief		월드 변환 행렬을 구한다.
	*/
	//----------------------------------------------------------------
	void CnMesh::GetWorldXForm( Math::Matrix44& rOut, bool bForceFromParent )
	{
		if (!bForceFromParent && IsSkinned())
		{
			rOut.Identity();
		}
		else
		{
			rOut = m_pParent->GetWorldXForm();
		}
	}

	//----------------------------------------------------------------
	/** Get World Transform Matrix
		@brief		월드 변환 행렬을 구한다.
	*/
	//----------------------------------------------------------------
	void CnMesh::GetWorldXForms( Math::Matrix44 *aXForms, int& rNumXForms )
	{
		rNumXForms = 0;

		if (IsSkinned())
		{
			ushort* boneIndices = m_pSkinnedData->influence_bone_indices;
			rNumXForms = m_pSkinnedData->num_influence_bones;
			for (ushort i=0; i<rNumXForms; ++i)
			{
				if (m_pSkeletonInst != NULL)
				{
					CnBoneNode* bone = StaticCast<CnBoneNode>(m_pSkeletonInst->GetNode( boneIndices[i] ));
					if (bone)
					{
						bone->GetLocalXform( aXForms[i] );
					}
					else
					{
						CnPrint( L"[CnMesh::GetWorldXForms] Mesh: %s, Link: %d, LinkedInddex: %d" ,
									GetName().c_str(), boneIndices[i], i );
					}
				}
				else
				{
					aXForms[i] = GetParent()->GetWorldXForm();
				}
			}

			//CnPrint( L"Entity: %s, link: %d", m_strName.c_str(), rNumXForms );
			//for( ushort i = 0; i < rNumXForms; ++i )
			//{
			//	CnPrint( L"Id: %d, BoneIndex[%d]", i, boneIndices[i] );
			//	CnPrint( L"%f %f %f %f", aXForms[i]._11, aXForms[i]._12, aXForms[i]._13, aXForms[i]._14 );
			//	CnPrint( L"%f %f %f %f", aXForms[i]._21, aXForms[i]._22, aXForms[i]._23, aXForms[i]._24 );
			//	CnPrint( L"%f %f %f %f", aXForms[i]._31, aXForms[i]._32, aXForms[i]._33, aXForms[i]._34 );
			//	CnPrint( L"%f %f %f %f", aXForms[i]._41, aXForms[i]._42, aXForms[i]._33, aXForms[i]._44 );
			//}
			//CnPrint( L"\n" );
		}
	}

	//----------------------------------------------------------------
	/** Set AABB
		@brief		로컬 좌표계의 AABB를 세팅한다.
	*/
	//----------------------------------------------------------------
	void CnMesh::SetAABB( const Math::AABB& Aabb )
	{
		m_LocalAabb = Aabb;
		m_WorldAabb = Aabb;
	}

	//----------------------------------------------------------------
	/** Get AABB
		@brief		월드 좌표계의 AABB를 구한다.
	*/
	//----------------------------------------------------------------
	void CnMesh::GetAABB( Math::AABB &rOut )
	{
		Math::Matrix44 matrix = m_pParent->GetWorldXForm();
		if( m_lastXAABB != matrix )
		{
			const Math::Vector3* vertices = m_LocalAabb.GetCorners();

			Math::Vector3 min, max;

			min.Multiply( vertices[0], matrix );
			max = min;

			Math::Vector3 vertex;
			for( uint i = 1; i < 8; ++i )
			{
				vertex.Multiply( vertices[i], matrix );
				
				if( min.x > vertex.x )
					min.x = vertex.x;
				
				if( max.x < vertex.x )
					max.x = vertex.x;

				if( min.y > vertex.y )
					min.y = vertex.y;
				
				if( max.y < vertex.y )
					max.y = vertex.y;

				if( min.z > vertex.z )
					min.z = vertex.z;

				if( max.z < vertex.z )
					max.z = vertex.z;
			}
			m_WorldAabb.SetExtents( min, max );

			m_lastXAABB = matrix;
		}
		rOut = m_WorldAabb;
	}

	//----------------------------------------------------------------
	/** Set OBB
		@brief		로컬 좌표계의 OBB를 세팅한다.
	*/
	//----------------------------------------------------------------
	void CnMesh::SetOBB( const Math::OBB& Obb )
	{
		m_LocalObb = Obb;
		m_WorldObb = Obb;
	}
	//----------------------------------------------------------------
	void CnMesh::GetOBB( Math::OBB& rOut )
	{
		Math::Matrix44 world = m_pParent->GetWorldXForm();
		if (m_lastXOBB != world)
		{
			m_LocalObb.Transform( &m_WorldObb, &world );
			m_lastXOBB = world;
		}
		rOut = m_WorldObb;
	}

	//----------------------------------------------------------------
	bool CnMesh::Cull( CnCamera* pCamera )
	{
		static Math::AABB aabb;
		GetAABB( aabb );

		m_bCulled = pCamera->IsCulled( aabb );
		return m_bCulled;
	}

	//----------------------------------------------------------------
	/** @brief	그림자를 드리우기 사용 */
	//----------------------------------------------------------------
	void CnMesh::SetCastShadow( bool bEnable )
	{
		CnSceneEntity::SetCastShadow( bEnable );

#pragma TODO( "그림자 관련해서는 추후에 다시 체크" )
		if (false == IsSkinned() && m_pParent)
		{
			m_pParent->EnableCulling( !bEnable );
		}
	}

	//----------------------------------------------------------------
	/** @brief	관계 형성
				1. Mesh - Light
				- 메쉬의 경우, 자신이 영향을 받는 라이트에 대한 정보를 알고 있어야 한다.
	*/
	//----------------------------------------------------------------
	void CnMesh::RelationTo( CnSceneEntity* pEntity, CnCamera* pCamera )
	{
		CnLight* light = DynamicCast<CnLight>( pEntity );
		if (!light)
			return;

		switch (light->GetType())
		{
		case CnLight::Global:
		case CnLight::Ambient:
		case CnLight::Directional:
			{
				m_InfluencedLights.push_back( light );
			}
			break;

		case CnLight::Point:
		case CnLight::Spot:
			{
				static Math::AABB aabb;
				GetAABB( aabb );

				if (aabb.Intersect( light->GetWorldPosition(), light->GetRange() ))
				{
					m_InfluencedLights.push_back( light );
				}
			}
			break;
		}
	}
	//----------------------------------------------------------------
	void CnMesh::ClearRelation()
	{
		m_InfluencedLights.clear();
	}

	//----------------------------------------------------------------
	/**	SubMesh의 Material 업데이트!!!
	*/
	//----------------------------------------------------------------
	void CnMesh::Apply( CnCamera* pCamera )
	{
		// 하위 메쉬 업데이트
		SubMeshIter iend = m_SubMeshes.end();
		for (SubMeshIter i=m_SubMeshes.begin(); i!=iend; ++i)
		{
			(*i)->UpdateMaterial();
		}
	}

	//----------------------------------------------------------------
	/** @brief	메쉬 갱신 */
	//----------------------------------------------------------------
	bool CnMesh::Update( CnCamera* pCamera )
	{
		m_IsChangedDistanceToCamera = false;

		//@add by cagetu - 10/01/12 : CnVisibleSet::Register에서 위치 변경
		ClearRelation();

		if (!IsVisible())
			return false;

		if (!IsCastShadow())
		{
			if (Cull( pCamera ))
				return false;
		}

		// Camera Distance 갱신
		Math::Vector3 length = pCamera->GetPosition() - GetWorldPosition();
		float lengthSq = length.LengthSq();
		if (lengthSq != m_fDistanceToCamera)
		{
			m_fDistanceToCamera = lengthSq;
			m_IsChangedDistanceToCamera = true;
		}

		// 하위 메쉬 업데이트
		SubMeshIter iend = m_SubMeshes.end();
		for (SubMeshIter i=m_SubMeshes.begin(); i!=iend; ++i)
		{
			(*i)->Update( pCamera );
		}

		// mvp 구하기
		Math::Matrix44 worldTM;
		GetWorldXForm(worldTM);
		m_WorldViewProjection = worldTM * pCamera->GetView() * pCamera->GetProjection();

		return true;
	}

	//----------------------------------------------------------------
	/** @brief	그리기 */
	//----------------------------------------------------------------
	void CnMesh::Draw( CnCamera* pCamera )
	{
		//SetVertexDeclaration();
		SetStreamSource();

		SubMeshIter iend = m_SubMeshes.end();
		for (SubMeshIter i=m_SubMeshes.begin(); i!=iend; ++i)
		{
			// RenderState
			//RenderDevice->SetRenderState( *((*i)->GetMaterial()->GetRenderState()) );

			(*i)->Draw( pCamera );
		}
	}


#if 0
	CnMesh* CnMesh::CreateBox(const CnString& name, int w, int h, int d)
	{
		CnMesh* mesh = CnNew CnMesh(name);

		CnVertexData* vertexdata = new CnVertexData();
		mesh->SetVertexData(vertexdata);

		CnVertexDeclaration::UsageComponent vertexComponent;
		vertexComponent.Add( CnVertexDeclaration::Position );
		vertexComponent.Add( CnVertexDeclaration::Uv0 );
		vertexComponent.Add( CnVertexDeclaration::Normal );

		VertDeclPtr vertexDeclaration = vertexdata->GetDeclaration();
		CnVertexDeclaration::VertexElement vertElement = vertexDeclaration->AddElement( 0, 0, vertexComponent );
		vertexDeclaration->Build();

		Ptr<CnVertexBuffer> vb = RenderDevice->CreateVertexBuffer();
		if (!vb->Create(4*6, vertElement.byteSize, CnVertexBuffer::USAGE_WRITEONLY, CnVertexBuffer::POOL_MANAGED))
		{
			delete mesh;
			return NULL;
		}

		struct VertexFormat
		{
			Math::Vector3 pos;
			Math::Vector2 uv;
			Math::Vector3 norm;
		};
		VertexFormat* vert = (VertexFormat*)vb->Lock( 0, 0, 0 );
		if (vert)
		{
			static unsigned short _idx[][4] =
			{
				0, 2, 3, 1, // -z
				1, 3, 7, 5, // +x
				3, 2, 6, 7, // +y
				2, 0, 4, 6, // -x
				0, 1, 5, 4, // -y
				7, 6, 4, 5, // +z
			} ;

			static float _uv[4][2] =
			{
				0, 0,
				1, 0,
				1, 1,
				0, 1,
			} ;

			static float _norm[][3] =
			{
				 0.0f,  0.0f,  1.0f,	// +z
				-1.0f,  0.0f,  0.0f,	// -x
				 0.0f, -1.0f,  0.0f,	// -y
				 1.0f,  0.0f,  0.0f,	// +x
				 0.0f,  1.0f,  0.0f,	// +y
				 0.0f,  0.0f, -1.0f,	// -z
			} ;
			
			/*	평면구성순서
				(+z, -x, -y, +x, +y, -z)
				(앞, 우, 밑, 좌, 위, 뒤)
			*/
			int i=0;
			for(i=0; i<4*6; i++)
			{
				vert[i].pos.x = (_idx[i/4][i%4]&1) ? -w/2 : w/2;
				vert[i].pos.y = (_idx[i/4][i%4]&2) ? -h/2 : h/2;
				vert[i].pos.z = (_idx[i/4][i%4]&4) ? -d/2 : d/2;
				vert[i].uv.x = _uv[i%4][0];
				vert[i].uv.y = _uv[i%4][1];
			}

			for(i=0; i<6; i++)
			{
				Math::Vector3 nn(_norm[i][0], _norm[i][1], _norm[i][2]);
				nn.Normalize();
				for(int j=0; j<4; j++)
					vert[i*4+j].norm = nn;
			}
		}
		vb->Unlock();

		vertexdata->AddBuffer( 0, vb );

		Ptr<CnIndexBuffer> ib = RenderDevice->CreateIndexBuffer();
		if(false == ib->Create( 3*2*6, CnIndexBuffer::USAGE_WRITEONLY, CnIndexBuffer::FMT_INDEX16, CnIndexBuffer::POOL_MANAGED ))
		{
			ib->Release();
			ib = NULL;
			return NULL;
		}

		ushort* indices = ib->Lock( 0, 0, 0 );
		if (indices)
		{
			for(int i=0; i<6; i++)
			{
				indices[i*6+0] = 4*i + 0;
				indices[i*6+1] = 4*i + 2;
				indices[i*6+2] = 4*i + 1;
				indices[i*6+3] = 4*i + 0;
				indices[i*6+4] = 4*i + 3;
				indices[i*6+5] = 4*i + 2;
			}
		}
		ib->Unlock();

		CnSubMesh* submesh = CnNew CnSubMesh();
		submesh->SetIndexBuffer(ib);

		Ptr<CnMaterial> mtl = CnNew CnMaterial();
		mtl->SetShader(L"template_solid.fx", 1);
		submesh->SetMaterial(mtl);

		mesh->AddSubMesh(submesh);
		return mesh;
	}
#endif

}