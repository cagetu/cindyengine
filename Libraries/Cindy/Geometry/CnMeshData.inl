//================================================================
// File:               : CnMeshData.inl
// Related Header File : CnMeshData.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 16
//================================================================
//----------------------------------------------------------------
//	MeshData Class
//----------------------------------------------------------------
inline
int CnMeshData::GetNumMeshNodeDatas() const
{
	return m_nNumMeshNodeDatas;
}

//----------------------------------------------------------------
inline
const CnMeshData::MeshNodeArray& CnMeshData::GetMeshNodeDatas() const
{
	return m_MeshNodeDatas;
}

//----------------------------------------------------------------
//	MeshDataPtr Class
//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr()
: Ptr<CnMeshData>()
{
}

//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr( CnMeshData* pObject )
: Ptr<CnMeshData>( pObject )
{
}

//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr( const MeshDataPtr& spObject )
: Ptr<CnMeshData>( spObject )
{
}

//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr( const RscPtr& spRsc )
: Ptr<CnMeshData>()
{
	m_pObject = (CnMeshData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->AddRef();
	}
}

//----------------------------------------------------------------
inline
MeshDataPtr& MeshDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnMeshData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->Release();

		if( !spRsc.IsNull() )
			spRsc->AddRef();

		m_pObject = (CnMeshData*)spRsc.GetPtr();
	}

	return *this;
}