//================================================================
// File:               : CnSubMesh.cpp
// Related Header File : CnSubMesh.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================
#include "Cindy.h"
#include "CnCindy.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/CnTransformDevice.h"
#include "Graphics/Base/CnIndexBuffer.h"
#include "Graphics/Base/CnRenderWindow.h"
#include "Resource/CnExportedDataDefines.h"
#include "Material/CnShaderManager.h"
#include "Material/CnTextureManager.h"
#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Lighting/ShadowMap/CnShadowMapSystem.h"
#include "Math/CnMatrix44.h"
#include "Util/CnLog.h"
#include "CnSubMesh.h"
#include "CnMesh.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnSubMesh, CnObject);
	///Const/Dest
	CnSubMesh::CnSubMesh()
		: m_Parent( 0 )
		, m_bNeedToBuildKey( true )
		, m_MaterialID(0)
		, m_bFeatureDirty(true)
	{
	}
	CnSubMesh::~CnSubMesh()
	{
		m_pIndexBuffer = NULL;
	}

	//----------------------------------------------------------------
	/** Get Key
		@brief	Key Value For RenderSorting.
	*/
	//----------------------------------------------------------------
	const RenderGroup::SortKey& CnSubMesh::GetSortKey() const
	{
		MakeKey();

		return m_Key;
	}

	//----------------------------------------------------------------
	/** @brief	정렬 키 만들기 */
	//----------------------------------------------------------------
	void CnSubMesh::MakeKey() const
	{
		if (GetParent()->IsChangedDistanceToCamera())
		{
			m_bNeedToBuildKey = true;
		}

		if (m_bNeedToBuildKey)
		{
			ushort distance = f32_16( GetParent()->GetDistanceToCamera() );

			if (this->GetMaterial()->IsTransparent())
			{
				/*	1. 거리(Back To Front) - 16bits
					2. material&technique id - 32bits
					3. 
				*/
				RenderGroup::TransparentKey key( m_Key );
				{
					uint16 textureId = 0;
					TexPtr diffuseMap = GetMaterial()->GetTexture( MapType::DiffuseMap0 );
					if (diffuseMap.IsValid())
						textureId = diffuseMap->GetRscID();

					key.depth = distance;
					//key.material = unicode::GetHashCode( GetMaterial()->GetName().c_str() );
					key.material = GetMaterial()->GetShader()->GetRscID();
					key.renderState = GetMaterial()->GetRenderState()->GetFlags();
					key.textureID = textureId;
				}
				m_Key = key;
			}
			else
			{
				/*	1. 매터리얼 id
					2. 텍스쳐 id
					3. 거리(Front To Back) - 16bits
				*/
				RenderGroup::OpaqueKey key( m_Key );
				{
					/** 매터리얼 id
							shader 이름 + technique 이름 + feature 이름
					*/
					CnString material = GetMaterial()->GetName();
					CnString feature = unicode::ToString( GetMaterial()->GetFeatureBits() );
					material += L"@" + feature;

					uint16 textureId = 0;
					TexPtr diffuseMap = GetMaterial()->GetTexture( MapType::DiffuseMap0 );
					if (diffuseMap.IsValid())
						textureId = diffuseMap->GetRscID();

					//key.material = unicode::GetHashCode( GetMaterial()->GetName().c_str() );
					//key.materialID = GetMaterial()->GetShader()->GetRscID();
					//key.renderState = GetMaterial()->GetRenderState()->GetFlags();

					key.material = unicode::GetHashCode( material.c_str() );
					key.depth = distance;
					key.textureID = textureId;
				}
				m_Key = key;
			}

			m_bNeedToBuildKey = false;
		}
	}

	//----------------------------------------------------------------
	/** Get World Transform Matrix
		@brief		월드 매트릭스 구하기
	*/
	//----------------------------------------------------------------
	void CnSubMesh::GetWorldXForm( Math::Matrix44& rOut )
	{
		GetParent()->GetWorldXForm( rOut );
	}
	
	//----------------------------------------------------------------
	/**	@brief	Shader State 적용.
	*/
	//----------------------------------------------------------------
	void CnSubMesh::ApplyState( CnCamera* pCamera )
	{
		ApplyModelView( pCamera );

		ApplyTextures();
		ApplyGeometry();

		if (!m_Material->HasFeature(ShaderFeature::Depth) &&
			!m_Material->HasFeature(ShaderFeature::SMDepth) &&
			!m_Material->HasFeature(ShaderFeature::GBuffer))
		{
			ApplyMaterial();

			if (RenderDevice.GetShadeType() == CnRenderer::Forward)
			{
				ApplyLight();
			}

			ApplyShadow();
		}
	}

	//----------------------------------------------------------------
	/** Set WorldTransforms
		@brief		쉐이더 상수 세팅
	*/
	//----------------------------------------------------------------
	void CnSubMesh::ApplyModelView( CnCamera* pCamera )
	{
		static Math::Matrix44 worldTM;
		static Math::Matrix44 localTM;
		static Math::Matrix44 modelView;
		static Math::Vector3 eyePosition;
		ShaderHandle handle = NULL;

		CnShaderEffect* shader = RenderDevice->GetActiveShader();

		this->GetWorldXForm( worldTM );

		// ModelViewProjection
		if (handle = shader->GetParameter( ShaderState::ModelViewProjection ))
		{
			//Math::Matrix44 mvp = this->GetParent()->GetWorldViewProjection();
			Math::Matrix44 mvp = worldTM * TransformDevice->GetViewProjTransform();
			shader->SetMatrix( handle, mvp );
		}
		if (handle = shader->GetParameter( ShaderState::ModelView ))
		{
			modelView = worldTM * pCamera->GetView();	//modelView = pCamera->GetView();	// TransformDevice->GetViewTransform(); //
			shader->SetMatrix( handle, modelView );
		}
		if (handle = shader->GetParameter( ShaderState::Model ))
		{
			shader->SetMatrix( handle, worldTM );
		}
		if (handle = shader->GetParameter( ShaderState::InvTransposedView ))
		{
			modelView = worldTM * pCamera->GetView();
			Math::Matrix44 invView = pCamera->GetView();
			modelView.Transpose( invView );
			invView.Inverse( invView );
			shader->SetMatrix( handle, invView );
		}

		//if (handle = pPass->GetParameter( ShaderState::ViewProjection ))
		//{
		//	shader->SetMatrix( handle, viewProjection );
		//}

		// EyePosition
		if (handle = shader->GetParameter( ShaderState::EyePosition ))
		{	// 시점 벡터
			// 오브젝트의 로컬 좌표계에서 바라본 카메라의 위치
			worldTM.Inverse( localTM );

			eyePosition.Multiply( pCamera->GetPosition(), localTM );
			shader->SetVector3( handle, eyePosition );
		}
		if (handle = shader->GetParameter( ShaderState::WorldEyePosition ))
		{
			shader->SetVector3(handle, pCamera->GetPosition());
		}
		if (handle = shader->GetParameter( ShaderState::ViewDirection ))
		{
		}

		///
		float windowWidth = (float)RenderDevice->GetCurrentRenderWindow()->GetWidth();
		float windowHeight = (float)RenderDevice->GetCurrentRenderWindow()->GetHeight();

		// 화면 해상도
		if (handle = shader->GetParameter( ShaderState::DisplayResolution ))
		{
			Math::Vector2 res( windowWidth, windowHeight );
			shader->SetValue( handle, res, sizeof(Math::Vector2) );
		}
		if (handle = shader->GetParameter(ShaderState::ScreenUVAdjust))
		{
			Math::Vector2 uvAdjust( 0.5f/windowWidth, 0.5f/windowHeight );
			shader->SetValue( handle, uvAdjust, sizeof(Math::Vector2) );
		}

		if (handle = shader->GetParameter( ShaderState::ViewInfo ))
		{
			// tanFOV
			float fov = pCamera->GetFovY();
			float tanFOV = 1.0f / Math::Tan(fov*0.5f);

			Math::Vector4 viewInfo;
			viewInfo.x = pCamera->GetAspect();
			viewInfo.y = tanFOV;
			viewInfo.z = pCamera->GetNear();
			viewInfo.w = pCamera->GetFar();
			shader->SetValue( handle, viewInfo, sizeof(Math::Vector4) );
		}
	}

	//----------------------------------------------------------------
	/** @brief	매터리얼 설정 */
	//----------------------------------------------------------------
	void CnSubMesh::ApplyMaterial()
	{
		ShaderHandle handle = NULL;
		CnShaderEffect* shader = RenderDevice->GetActiveShader();

		// Material
		CnMaterialState* materialState = GetMaterial()->GetColorState();
		if (handle = shader->GetParameter( ShaderState::MatAmbient ))
		{
			shader->SetColor( handle, materialState->GetAmbient() );
		}
		if (handle = shader->GetParameter( ShaderState::MatDiffuse ))
		{
			shader->SetColor( handle, materialState->GetDiffuse() );
		}
		if (handle = shader->GetParameter( ShaderState::MatSpecular ))
		{
			CnColor specular( materialState->GetSpecular() );
			specular.a = materialState->GetShininess();
			shader->SetColor( handle, specular );
		}
		if (handle = shader->GetParameter( ShaderState::MatEmissive ))
		{
			shader->SetColor( handle, materialState->GetEmissive() );
		}
		if (handle = shader->GetParameter( ShaderState::MatSpecularIntensity ))
		{
			shader->SetFloat( handle, materialState->GetSpecularIntensity() );
		}
		if (handle = shader->GetParameter( ShaderState::MatEmissiveIntensity ))
		{
			shader->SetFloat( handle, materialState->GetEmissiveIntensity() );
		}
		if (handle = shader->GetParameter( ShaderState::MatGlowIntensity ))
		{
			shader->SetFloat( handle, materialState->GetGlowIntensity() );
		}
	}

	//----------------------------------------------------------------
	/** Set Textures
		@brief		쉐이더 상수 세팅
	*/
	//----------------------------------------------------------------
	void CnSubMesh::ApplyTextures()
	{
		TexPtr texture;
		ShaderHandle handle = NULL;
		CnShaderEffect* shader = RenderDevice->GetActiveShader();

		for(int i=0; i<MapType::NumMapTypes; i++)
		{
			int shaderparam = MapType::ToShaderState( (MapType::Define)i );
			if (handle = shader->GetParameter( (ShaderState::Param)shaderparam ))
			{
				texture = m_Material->GetTexture( (MapType::Define)i );
				shader->SetTexture(handle, texture);
			}
		}
#if 0
		if (handle = shader->GetParameter( ShaderState::DiffuseMap0 ))
		{
			texture = m_Material->GetTexture( MapType::DiffuseMap0 );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::SpecularMap ))
		{
			texture = m_Material->GetTexture( MapType::SpecularMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::SpecularExpMap))
		{
			texture = m_Material->GetTexture( MapType::SpecularExpMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::EmissiveMap ))
		{
			texture = m_Material->GetTexture( MapType::EmissiveMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::GlowMap ))
		{
			texture = m_Material->GetTexture( MapType::GlowMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::NormalMap ))
		{
			texture = m_Material->GetTexture( MapType::NormalMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::ToonMap ))
		{
			texture = m_Material->GetTexture( MapType::CelShadeMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::OcclusionMap ))
		{
			texture = m_Material->GetTexture( MapType::OcclusionMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::CubeMap0 ))
		{
			texture = m_Material->GetTexture( MapType::CubeMap0 );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::ReflectMap ))
		{
			texture = m_Material->GetTexture( MapType::ReflectMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::AlphaMap ))
		{
			texture = m_Material->GetTexture( MapType::AlphaMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::RandomMap ))
		{
			texture = m_Material->GetTexture( MapType::RandomMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::HairShiftMap ))
		{
			texture = m_Material->GetTexture( MapType::HairShiftMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::HairSpecMaskMap ))
		{
			texture = m_Material->GetTexture( MapType::HairSpecMaskMap );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::GBuffer ))
		{
			texture = m_Material->GetTexture( MapType::GBuffer );
			shader->SetTexture( handle, texture );
		}
#endif

		if (handle = shader->GetParameter( ShaderState::LightBuffer ))
		{
			texture = TextureMgr->Load( L"LightPass0" );
			assert( texture.IsValid() );
			shader->SetTexture( handle, texture );
		}
		if (handle = shader->GetParameter( ShaderState::NormalBuffer ))
		{
			texture = TextureMgr->Load( L"NormalBuffer" );
			assert( texture.IsValid() );
			shader->SetTexture( handle, texture );
		}
	}

	//----------------------------------------------------------------
	/** @brief	Light 와 정보 */
	//----------------------------------------------------------------
	void CnSubMesh::ApplyLight()
	{
		static const int		_MAX_LIGHTS = 8;
		static int				numLights;
		static Math::Vector4	lightPos[_MAX_LIGHTS];
		static Math::Vector4	lightDiffuse[_MAX_LIGHTS];
		static CnColor			lightSpecular[_MAX_LIGHTS];
		static Math::Vector4	lightAtten[_MAX_LIGHTS];
		static float			lightRange[_MAX_LIGHTS];
		
		ShaderHandle handle = NULL;
		CnShaderEffect* shader = RenderDevice->GetActiveShader();

		// 최대 8개 사용하자
		// Directional: 2개까지
		// Point : 4개
		// Spot : 2개
		numLights = 0;

		Math::Matrix44 toLocal;
		this->GetWorldXForm( toLocal );
		toLocal.Inverse( toLocal );

		// Directional, Point, Spot을 어떻게 구분 지어 줄테냐??
		CnColor lightAmbient( 0.0f, 0.0f, 0.0f, 0.0f );

		CnLight* light = NULL;
		CnMesh* mesh = this->GetParent();
		CnMesh::LightArray lights = mesh->GetInfluencedLights();
		CnMesh::LightArray::const_iterator i, iend;
		iend = lights.end();
		for (i = lights.begin(); i != iend; ++i)
		{
			light = (*i);

#pragma TODO("최적화 및 정리는 나중에")
			if (light->GetType() == CnLight::Directional ||	light->GetType() == CnLight::Global)
			{
				// direction
				if (handle = shader->GetParameter(ShaderState::LightDirection))
				{
					Math::Vector3 localLightDir;
					localLightDir.MultiplyNormal( light->GetDirection(), toLocal );
					shader->SetVector3(handle, localLightDir);
					//shader->SetVector3(handle, light->GetDirection());
				}
				if (handle = shader->GetParameter(ShaderState::LightDiffuse))
				{
					shader->SetColor( handle, light->GetDiffuse());
				}
				//Math::Vector3 pos = light->GetWorldPosition();
				//pos.Multiply( pos, toLocal );
				//lightPos[numLights] = pos;
				//lightDiffuse[numLights] = light->GetDiffuse();
				//lightSpecular[numLights] = light->GetSpecular();

				//++numLights;
			}
			else if (light->GetType() == CnLight::Point)
			{
				// position/Attenuation
				Math::Vector3 pos = light->GetWorldPosition();
				pos.Multiply( pos, toLocal );
				lightPos[numLights] = Math::Vector4(pos.x, pos.y, pos.z, light->GetRange());
				lightDiffuse[numLights] = Math::Vector4(light->GetDiffuse().r, light->GetDiffuse().g, light->GetDiffuse().b, light->GetIntensity());
				lightAtten[numLights].FromVec3( light->GetAttenuation() );

				++numLights;
			}
			else if (light->GetType() == CnLight::Spot)
			{
			}

			//lightAmbient += (light->GetAmbient() * materialState.GetAmbient());
			lightAmbient += light->GetAmbient();
		}

		// Local Lights
		if (handle = shader->GetParameter( ShaderState::LightCount ))
			shader->SetInt( handle, numLights );

		if (numLights > 0 && m_Material->HasFeature(ShaderFeature::MultipleVertexLight))
		{
			if (handle = shader->GetParameter(ShaderState::LightPosArray))
			{
				shader->SetVector4Array(handle, lightPos, _MAX_LIGHTS);
			}
			if (handle = shader->GetParameter(ShaderState::LightColorArray))
			{
				shader->SetVector4Array(handle, lightDiffuse, _MAX_LIGHTS);
			}
			//if (handle = shader->GetParameter(ShaderState::LightRangeArray))
			//{
			//	shader->SetFloatArray(handle, lightRange, _MAX_LIGHTS);
			//}
			//if (handle = shader->GetParameter( ShaderState::LightPosition ))
			//{
			//	shader->SetVector3( handle, lightPos[0] );
			//}
			//if (handle = shader->GetParameter( ShaderState::LightDiffuse ))
			//{
			//	shader->SetColor( handle, lightDiffuse[0] );
			//}
			//if (handle = shader->GetParameter( ShaderState::LightSpecular ))
			//{
			//	shader->SetColor( handle, lightSpecular[0] );
			//}
			//if (handle = shader->GetParameter( ShaderState::LightAttenuation ))
			//{
			//	shader->SetVector4( handle, lightAtten[0] );
			//}
		} // if (numLights ...)

		// Global Ambient
		// Ambient Lighting = Material_ambient* Global_ambient + sum( Light_ambient* Material_ambient ) )
		if (handle = shader->GetParameter( ShaderState::GlobalAmbient ))
		{
			//RGBA color;5
			//RenderDevice->GetAmbient( color );
			//CnColor globalAmbient( color );
			//lightAmbient += ( materialState.GetEmissive() + (globalAmbient*materialState.GetAmbient()) );
			//lightAmbient += materialState.GetEmissive();
			shader->SetColor( handle, lightAmbient );
		}
	}

	//----------------------------------------------------------------
	/**	Apply Shadow State
	*/
	//----------------------------------------------------------------
	void CnSubMesh::ApplyShadow()
	{
		CnShadowMappable* glboalShadowMapper = ShadowMapSystem->GetGlobalShadowMapper();
		if (glboalShadowMapper == NULL)
			return;

		Math::Matrix44 worldTM;
		this->GetWorldXForm( worldTM );

		glboalShadowMapper->GetShadowBuilder()->SetShaderVariables( worldTM );
	}

	//----------------------------------------------------------------
	/** Set Geometry
		@brief		쉐이더 상수 세팅
	*/
	//----------------------------------------------------------------
	void CnSubMesh::ApplyGeometry()
	{
		ShaderHandle handle = NULL;
		CnShaderEffect* shader = RenderDevice->GetActiveShader();
		if (handle = shader->GetParameter( ShaderState::JointPalette ))
		{
			int numJoints = 0;
			static Math::Matrix44 s_Joints[27];

			GetParent()->GetWorldXForms( s_Joints, numJoints );
			shader->SetMatrixArray( handle, s_Joints, numJoints );
		}

		if (handle = shader->GetParameter( ShaderState::SkinBlend ))
		{
			shader->SetInt( handle, GetParent()->GetNumBlend() );
		}
	}

	//----------------------------------------------------------------
	/** @brief	정렬 키 만들기 */
	//----------------------------------------------------------------
	void CnSubMesh::UpdateMaterial()
	{
		if (m_bFeatureDirty)
		{
			//! Skinned 정보
			if (GetParent()->IsSkinned())
			{
				GetMaterial()->AddFeature( ShaderFeature::Skinned );
			}

			//! VertexColor 여부
			if (GetParent()->GetVertexData()->HasVertexComponent( CnVertexDeclaration::Color ))
			{
				GetMaterial()->AddFeature( ShaderFeature::VertexColor );
			}

			if (GetParent()->IsReceiveShadow())
			{
				GetMaterial()->AddFeature( ShaderFeature::ShadowMap );
			}

			m_bFeatureDirty = false;
		}

		// Material에 변경사항을 적용한다.
		GetMaterial()->Apply();
	}
	
	//----------------------------------------------------------------
	/**	@brief	갱신
	*/
	//----------------------------------------------------------------
	bool CnSubMesh::Update( CnCamera* pCamera )
	{
		//@cagetu -09/11/30 : 매터리얼 업데이트 타이밍을 Culling 이후로!!!
		//if (m_bFeatureDirty)
		//{
		//	//! Skinned 정보
		//	if (GetParent()->IsSkinned())
		//	{
		//		GetMaterial()->AddFeature( ShaderFeature::Skinned );
		//	}

		//	//! VertexColor 여부
		//	if (GetParent()->GetVertexData()->HasVertexComponent( CnVertexDeclaration::Color ))
		//	{
		//		GetMaterial()->AddFeature( ShaderFeature::VertexColor );
		//	}
		//	m_bFeatureDirty = false;
		//}

		//// Material에 변경사항을 적용한다.
		//GetMaterial()->Apply();
		//this->UpdateMaterial();
		return true;
	}

	//----------------------------------------------------------------
	/** @brief	그리기
	*/
	//----------------------------------------------------------------
	bool CnSubMesh::Draw( CnCamera* pCamera )
	{
		//m_pIndexBuffer->SetIndices();

		uint count = m_pIndexBuffer->GetNumIndices() / 3;
		return RenderDevice->DrawIndexedPrimitive( IRenderer::TRIANGLELIST, m_Parent->GetNumVertices(), count );
	}
}