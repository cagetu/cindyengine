//================================================================
// File:           : CnMeshData.h
// Original Author : changhee
// Creation Date   : 2007. 4. 10
//================================================================
#pragma once

#include "Resource/CnResource.h"
#include "Resource/CnExportedDataDefines.h"

#include <MoCommon/IO/MoMemStream.h>

namespace Cindy
{
	class CnVertexBufferBinder;
	class CnMesh;

	//================================================================
	/** Mesh Resource Data
	    @author    changhee
		@since     2007. 4. 10
		@remarks   메쉬 리소스 데이터
	*/
	//================================================================
	class CN_DLL CnMeshData : public CnResource
	{
		__DeclareRtti;

		friend class CnMeshDataManager;
	public:
		typedef std::vector<Export::MeshNode*>	MeshNodeArray;
		typedef MeshNodeArray::iterator			MeshNodeIter;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		virtual ~CnMeshData();

		// Load/Unload
		bool					Load( const wchar* strFileName ) override;
		bool					Unload() override;

		// Device Lost/Restore
		void					OnLost() override;
		void					OnRestore() override;

		// Datas
		int						GetNumMeshNodeDatas() const;
		const MeshNodeArray&	GetMeshNodeDatas() const;

	private:
		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		MeshNodeArray			m_MeshNodeDatas;
		int						m_nNumMeshNodeDatas;

		MoCommon::MoMemStream	m_DataMemStream;
		ulong					m_ulTotalDataSize;

		bool					m_bIsPackedVertex;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		CnMeshData( CnResourceManager* pParent );

		// Data 읽기 시작
		bool			LoadData();

		// 파일 정보 읽기
		bool			ReadMagicNumber();
		bool			ReadFileVersion();

		// 메쉬 정보 읽기
		bool			ReadMeshNodeArray( ulong ulDataSize );
		bool			ReadMeshNode( ulong ulDataSize );
		bool			ReadMeshNodeVertices( ulong ulDataSize, Export::MeshNode* pMeshNodeData );
		bool			ReadMeshNodeFaces( ulong ulDataSize, Export::MeshNode* pMeshNodeData );
		bool			ReadMeshNodeFaceGroup( ulong ulDataSize, Export::MeshNode* pMeshNodeData );

		// 충돌 박스
		void			ReadAABB( Export::MeshNode* pMeshNodeData );
		void			ReadOBB( Export::MeshNode* pMeshNodeData );

		// 기타
		Export::Node*	CreateNodeData( BYTE Type );
		CnVertexBuffer*	CreateVertexBuffer( uint nNumVertices, uint nSizeOfVertex );
		CnIndexBuffer*	CreateIndexBuffer( ushort usNumIndices );

		void			ReadNameData( CnString& rResult );
	};

	//==================================================================
	/** MeshDataPtr
		@author			cagetu
		@since			2006년 10월 9일
		@remarks		Mesh 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL MeshDataPtr : public Ptr<CnMeshData>
	{
	public:
		MeshDataPtr();
		explicit MeshDataPtr( CnMeshData* pObject );
		MeshDataPtr( const MeshDataPtr& spObject );
		MeshDataPtr( const RscPtr& spRsc );

		// operator = 
		MeshDataPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnMeshData.inl"
} // end of namespace