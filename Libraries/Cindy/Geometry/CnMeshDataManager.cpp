//================================================================
// File:               : CnMeshDataManager.cpp
// Related Header File : CnMeshDataManager.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 12
//================================================================
#include "../Cindy.h"
#include "CnMeshDataManager.h"
#include "CnMeshData.h"

namespace Cindy
{
	//================================================================
	__ImplementClass(Cindy, CnMeshDataManager, CnResourceManager);
	__ImplementSingleton(CnMeshDataManager);
	///Const/Dest
	CnMeshDataManager::CnMeshDataManager()
	{
		__ConstructSingleton;
	}
	CnMeshDataManager::~CnMeshDataManager()
	{
		__DestructSingleton;
	}

	//--------------------------------------------------------------
	/** 리소스 읽기
		@remarks
			리소스를 읽는다.
	*/
	//--------------------------------------------------------------
	RscPtr CnMeshDataManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = Get( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnMeshData* mesh = CnNew CnMeshData( this );
		if( mesh->Load( strFileName ) )
		{
			Add( mesh );
		}
		else
		{
			SAFEDEL( mesh );
		}

		return mesh;
	}
}