//================================================================
// File:               : CnSubMesh.inl
// Related Header File : CnSubMesh.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================

//----------------------------------------------------------------
/**	@brief	부모 메쉬 설정/얻어오기
*/
//----------------------------------------------------------------
inline
void 
CnSubMesh::SetParent( CnMesh* Parent )
{
	m_Parent = Parent;
}
inline
CnMesh*
CnSubMesh::GetParent() const
{
	return m_Parent;
}

//----------------------------------------------------------------
/** Set Material Index
	@brief	매터리얼 ID 저장
*/
//----------------------------------------------------------------
inline
void
CnSubMesh::SetMtlID( ushort Index ) 
{
	m_MaterialID = Index;
}

//----------------------------------------------------------------
/** Get Material Index
	@brief	매터리얼 ID
*/
//----------------------------------------------------------------
inline
ushort 
CnSubMesh::GetMtlID() const
{
	return m_MaterialID;
}

//----------------------------------------------------------------
/** Set Material
	@brief	매터리얼 세팅
*/
//----------------------------------------------------------------
inline
void
CnSubMesh::SetMaterial( const Ptr<CnMaterial>& rMaterial )
{
	m_Material = rMaterial;

	m_bNeedToBuildKey = true;
	//m_bFeatureDirty = true;
}

//----------------------------------------------------------------
inline
const Ptr<CnMaterial>&
CnSubMesh::GetMaterial() const
{
	return m_Material;
}

//----------------------------------------------------------------
/** Set IndexBuffer
	@brief		인덱스 버퍼 설정
*/
//----------------------------------------------------------------
inline
void 
CnSubMesh::SetIndexBuffer( const Ptr<CnIndexBuffer>& pIndexBuffer )
{
	m_pIndexBuffer = pIndexBuffer;
}

//----------------------------------------------------------------
inline
const Ptr<CnIndexBuffer>&
CnSubMesh::GetIndexBuffer() const
{
	return m_pIndexBuffer;
}
