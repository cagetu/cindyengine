//================================================================
// File:               : CnMeshData.cpp
// Related Header File : CnMeshData.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 10
//================================================================
#include "../Cindy.h"
#include "CnMeshData.h"
#include "CnMeshDataManager.h"
#include "CnVertexData.h"
#include "CnMesh.h"
#include "Graphics/Base/CnVertexDeclaration.h"
#include "Graphics/Base/CnVertexBuffer.h"
#include "Graphics/Base/CnIndexBuffer.h"
#include "Graphics/CnRenderer.h"
#include "Util/CnLog.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnMeshData, CnResource );
	//================================================================
	/** 생성자
		@remarks			이 리소스를 관리하는 매니져만 생성 가능하다.
		@param pParent		부모 관리자
	*/
	//================================================================
	CnMeshData::CnMeshData( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_nNumMeshNodeDatas(0)
		, m_bIsPackedVertex(false)
	{
	}
	CnMeshData::~CnMeshData()
	{
		Unload();
	}

	//================================================================
	/** OnLost
		@remarks	디바이스를 잃었을 때..
	*/
	//================================================================
	void CnMeshData::OnLost()
	{
	}

	//================================================================
	/** OnLost
		@remarks	디바이스를 복구 했을 때
	*/
	//================================================================
	void CnMeshData::OnRestore()
	{
	}

	//================================================================
	/** Load
	    @remarks      메쉬 로딩
		@param        strFileName : 파일 이름
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::Load( const wchar* strFileName )
	{
		using namespace MoCommon;

		if (m_pParent->FindData( strFileName, m_DataMemStream ))
		{
			m_ulTotalDataSize = m_DataMemStream.GetSize();

			m_strName = strFileName;
			if( !LoadData() )
			{
				m_DataMemStream.Close();
				m_strName = L"";
				return false;
			}

			SetState( Loaded );

			return true;
		}

		CnError( ToStr(L"file not found: %s", strFileName) );
		return false;
	}

	//================================================================
	/** Unload
	    @remarks      메쉬 해제
		@param        none
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::Unload()
	{
		std::for_each( m_MeshNodeDatas.begin(), m_MeshNodeDatas.end(), DeleteObject() );

		SetState( Unloaded );
		m_strName.clear();

		return true;
	}

	//================================================================
	/** LoadData
	    @remarks      데이터 읽기 시작
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::LoadData()
	{
		using namespace MoCommon;

		ChunkHeaderL chunk;
		ulong leftsize = m_ulTotalDataSize;
		while( leftsize )
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch( chunk.ulTag )
			{
			case RD_MESH_MAGIC:
				{
					if( !ReadMagicNumber() )
						return false;
				}
				break;

			case RD_MESH_VERSION:
				{
					if( !ReadFileVersion() )
						return false;
				}
				break;

			case RD_MESH_NODELIST:
				{
					if( !ReadMeshNodeArray( chunk.ulSize ) )
						return false;
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}

		return true;
	}

	//================================================================
	/**	Read Magic Number
		@remarks
			파일 고유 번호를 읽어온다.
	*/
	//================================================================
	bool CnMeshData::ReadMagicNumber()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if( RD_MESHFILE_MAGICNUM != result )
		{
			Assert( RD_MESHFILE_MAGICNUM == result, L"[CnMeshData::ReadMagicNumber] different file unique number" );
			return false;
		}

		return true;
	}

	//================================================================
	/** Read File Version
		@remarks
			파일 버전을 읽어온다.
	*/
	//================================================================
	bool CnMeshData::ReadFileVersion()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if( RD_MESHFILE_VERSION != result )
		{
			Assert( RD_MESHFILE_VERSION == result, L"[CnMeshData::ReadFileVersion] different file version" );
			return false;
		}

		return true;
	}

	//================================================================
	/** Read Mesh Node List
	    @remarks      리소스 메쉬 노드 목록을 읽어온다.
		@param        ulDataSize : 읽어야 할 데이터 사이즈
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeArray( ulong ulDataSize )
	{
		MoCommon::ChunkHeaderL chunk;

		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag)
			{
			case RD_MESH_NODE:
				{
					ReadMeshNode( chunk.ulSize );
				}
				break;

			//case RD_MESH_NFNODE:
			//	{
			//	}
			//	break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - MoCommon::ChunkHeaderL::HeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}

		return true;
	}

	//================================================================
	/** Read Mesh Node
	    @remarks      리소스 메쉬 노드를 읽어온다.
		@param        ulDataSize : 읽어야 할 데이터 사이즈
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::ReadMeshNode( ulong ulDataSize )
	{
		MoCommon::ChunkHeaderL chunk;

		BYTE nodetype = 0;
		m_DataMemStream.Read( &nodetype, sizeof(BYTE), 1 );
		Export::Node* node = CreateNodeData( nodetype );

		ulong leftsize = ulDataSize - sizeof(BYTE) - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag)
			{
			// 현재 노드 정보
			case RD_MESH_NODE_ID:
				{
					m_DataMemStream.Read( &node->id, sizeof(ushort), 1 );
				}
				break;
			case RD_MESH_NODE_NAME:
				{
					if( node->id == INVALID_NODE_ID )
						break;

					//! Unique한 이름을 위해, 파일이름 + 노드이름
					CnString filename, name;
					filename = unicode::SplitPathFileName( m_strName );
					ReadNameData( name );
					node->name = filename + L"@" + name;
				}
				break;

			// 부모 노드 정보
			case RD_MESH_NODE_PARENTID:
				{
					m_DataMemStream.Read( &node->parent_id, sizeof(ushort), 1 );
				}
				break;
			case RD_MESH_NODE_PARENTNAME:
				{
					if (node->id == INVALID_NODE_ID)
						break;

					//! 우리는 1000번 이상 부터 MeshNode 이다..... 기억하라~!!!!
					if( node->parent_id >= 1000 )
					{
						//! Unique한 이름을 위해, 파일이름 + 노드이름
						CnString filename, name;
						filename = unicode::SplitPathFileName( m_strName );
						ReadNameData( name );

						if( node->parent_id != INVALID_NODE_ID )
						{
							node->parent_name = filename + L"@" + name;
						}
					}
					else
					{
						ReadNameData( node->parent_name );
					}
				}
				break;

			// 노드 변환 행렬
			case RD_MESH_NODE_LOCALTM:
				{
					m_DataMemStream.Read( &node->local_pos, sizeof(float), 3 );
					m_DataMemStream.Read( &node->local_rot, sizeof(float), 4 );
					m_DataMemStream.Read( &node->local_scl, sizeof(float), 3 );
				}
				break;
			case RD_MESH_NODE_WORLDTM:
				{
					float matrix[12];		// 3x4
					m_DataMemStream.Read( matrix, sizeof(float), 12 );

					node->worldTM._11 = matrix[0];
					node->worldTM._12 = matrix[1];
					node->worldTM._13 = matrix[2];
					node->worldTM._14 = 0.0f;

					node->worldTM._21 = matrix[3];
					node->worldTM._22 = matrix[4];
					node->worldTM._23 = matrix[5];
					node->worldTM._24 = 0.0f;

					node->worldTM._31 = matrix[6];
					node->worldTM._32 = matrix[7];
					node->worldTM._33 = matrix[8];
					node->worldTM._34 = 0.0f;

					node->worldTM._41 = matrix[9];
					node->worldTM._42 = matrix[10];
					node->worldTM._43 = matrix[11];
					node->worldTM._44 = 1.0f;
				}
				break;

			// Skin mesh의 경우, 본 정보
			case RD_MESH_NODE_LINKEDBONE:
				{
					Assert( Export::Node::MESH == node->type, L"Invalid Node Data" );
					Export::MeshNode* meshnode = static_cast<Export::MeshNode*>(node);

					if (meshnode->skindata == 0)
						meshnode->skindata = new Export::Skinned();

					m_DataMemStream.Read( &meshnode->skindata->num_influence_bones, sizeof(ushort), 1 );

					if (meshnode->skindata->num_influence_bones > 0)
					{
						meshnode->skindata->influence_bone_indices = new ushort [meshnode->skindata->num_influence_bones];
						m_DataMemStream.Read( meshnode->skindata->influence_bone_indices, sizeof(ushort), meshnode->skindata->num_influence_bones );
					}
				}
				break;
			case RD_MESH_NODE_MAXLINK:
				{
					Assert( Export::Node::MESH == node->type, L"Invalid Node Data" );
					Export::MeshNode* meshnode = static_cast<Export::MeshNode*>(node);

					if (meshnode->skindata == 0)
						meshnode->skindata = new Export::Skinned();

					m_DataMemStream.Read( &meshnode->skindata->num_vertex_blend, sizeof(BYTE), 1 );
				}
				break;

			// 버텍스 정보
			case RD_MESH_NODE_VERTEXCOLOR:
				{
					BYTE enable = false;

					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
				}
				break;

			case RD_MESH_NODE_PACKEDVERTEX:
				{
					BYTE enable = 0;
					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );

					m_bIsPackedVertex = (enable == 1) ? true : false;
				}
				break;

			case RD_MESH_NODE_VERTEX:
				{
					Assert( Export::Node::MESH == node->type, L"Invalid Node Data" );
					Export::MeshNode* meshnode = static_cast<Export::MeshNode*>(node);

					ReadMeshNodeVertices( chunk.ulSize, meshnode );
				}
				break;

			// 인덱스 정보
			case RD_MESH_NODE_FACELIST:
				{
					Assert( Export::Node::MESH == node->type, L"Invalid Node Data" );
					Export::MeshNode* meshnode = static_cast<Export::MeshNode*>(node);

					ReadMeshNodeFaces( chunk.ulSize, meshnode );
				}
				break;

			// BoundingBox
			case RD_MESH_NODE_AABB:
				{
					Assert( Export::Node::MESH == node->type, L"Invalid Node Data" );
					Export::MeshNode* meshnode = static_cast<Export::MeshNode*>(node);

					ReadAABB( meshnode );
				}
				break;
			case RD_MESH_NODE_OBB:
				{
					Assert( Export::Node::MESH == node->type, L"Invalid Node Data" );
					Export::MeshNode* meshnode = static_cast<Export::MeshNode*>(node);

					ReadOBB( meshnode );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - MoCommon::ChunkHeaderL::HeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		Assert( Export::Node::MESH == node->type, L"Invalid Node Data" );
		Export::MeshNode* meshnode = static_cast<Export::MeshNode*>(node);

		//CnPrint( L"[MeshNode]: %s", meshnode->name.c_str() );

		m_MeshNodeDatas.push_back( meshnode );
		return true;
	}

	//================================================================
	/** Read Mesh Node Vertices
	    @remarks      메쉬 노드 버텍스 정보
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeVertices( ulong ulDataSize, Export::MeshNode* pMeshNodeData )
	{
		pMeshNodeData->vertexData = new CnVertexData();

		// 버텍스 개수
		ushort num_vertices = 0;
		m_DataMemStream.Read( &num_vertices, sizeof(ushort), 1 );
		pMeshNodeData->vertexData->SetNumVertices( num_vertices );

		CnPrint(L"Num Vertices: %d\n", num_vertices);

		//
		Math::Vector3* positions = NULL;
		Math::Vector3* normals = NULL;
		Math::Vector2* texcoords = NULL;
		Math::Vector3* colors = NULL;
#ifdef TANGENT_FLOAT4
		Math::Vector4* tangents = NULL;
#else
		Math::Vector3* tangents = NULL;
#endif
		Math::Vector3* binormals = NULL;

		struct VertexBlend
		{
			ushort		links;
			float		weight[4];
			ushort		bone[4];
		};

		VertexBlend* blendDatas = NULL;

		CnVertexDeclaration::UsageComponent vertexComponents;
		CnVertexDeclaration::UsageComponent posComponents;
		CnVertexDeclaration::UsageComponent normalComponents;
		CnVertexDeclaration::UsageComponent colorComponents;
		CnVertexDeclaration::UsageComponent uvComponents;
		CnVertexDeclaration::UsageComponent skinComponents;
		CnVertexDeclaration::UsageComponent bumpComponents;

		bool useVertexColor = false;

		MoCommon::ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - sizeof(ushort) - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag)
			{
			case RD_MESH_NODE_VERTEX_POS:
				{
					positions = new Math::Vector3[num_vertices];
					m_DataMemStream.Read( positions, sizeof(float), 3*num_vertices );

					vertexComponents.Add( CnVertexDeclaration::Position );
					posComponents.Add( CnVertexDeclaration::Position );
				}
				break;
			case RD_MESH_NODE_VERTEX_NORM:
				{
					normals = new Math::Vector3[num_vertices];
					m_DataMemStream.Read( normals, sizeof(float), 3*num_vertices );

#ifdef __PACK_NORMAL__
					vertexComponents.Add( CnVertexDeclaration::NormalUB4N );
					normalComponents.Add( CnVertexDeclaration::NormalUB4N );
#else
					vertexComponents.Add( CnVertexDeclaration::Normal );
					normalComponents.Add( CnVertexDeclaration::Normal );
#endif	// 
				}
				break;
			case RD_MESH_NODE_VERTEX_UV:
				{
					texcoords = new Math::Vector2[num_vertices];
					m_DataMemStream.Read( texcoords, sizeof(float), 2*num_vertices );

#ifdef __PACK_UV__
					vertexComponents.Add( CnVertexDeclaration::Uv0S2 );
					uvComponents.Add( CnVertexDeclaration::Uv0S2 );
#else
					vertexComponents.Add( CnVertexDeclaration::Uv0 );
					uvComponents.Add( CnVertexDeclaration::Uv0 );
#endif
				}
				break;
			case RD_MESH_NODE_VERTEX_BLEND:
				{
					BYTE count = 0;
					blendDatas = new VertexBlend[num_vertices];
					for ( ulong i=0; i < num_vertices; ++i)
					{
						m_DataMemStream.Read( &count, sizeof(BYTE), 1 );
						blendDatas[i].links = (ushort)count;

						for (ushort j=0; j<4; ++j)
						{
							blendDatas[i].bone[j] = 0;
							blendDatas[i].weight[j] = 0.0f;
						}
						for (ushort j=0; j<count; ++j)
						{
							m_DataMemStream.Read( &blendDatas[i].bone[j], sizeof(ushort), 1 );
							m_DataMemStream.Read( &blendDatas[i].weight[j], sizeof(float), 1 );
						}
					}

#ifdef __PACK_SKIN__
					vertexComponents.Add( CnVertexDeclaration::BlendWeightsUB4N );
					vertexComponents.Add( CnVertexDeclaration::BlendIndicesUB4 );

					skinComponents.Add( CnVertexDeclaration::BlendWeightsUB4N );
					skinComponents.Add( CnVertexDeclaration::BlendIndicesUB4 );
#else
					vertexComponents.Add( CnVertexDeclaration::BlendWeights );
					vertexComponents.Add( CnVertexDeclaration::BlendIndices );

					skinComponents.Add( CnVertexDeclaration::BlendWeights );
					skinComponents.Add( CnVertexDeclaration::BlendIndices );
#endif
				}
				break;
			case RD_MESH_NODE_VERTEX_COLOR:
				{
					colors = new Math::Vector3[num_vertices];
					m_DataMemStream.Read( colors, sizeof(float), 3*num_vertices );

#ifdef __PACK_COLOR__
					vertexComponents.Add( CnVertexDeclaration::ColorUB4N );
					colorComponents.Add( CnVertexDeclaration::ColorUB4N );
#else
					vertexComponents.Add( CnVertexDeclaration::Color );
					colorComponents.Add( CnVertexDeclaration::Color );
#endif

					useVertexColor = true;
					pMeshNodeData->enableVertexColor = true;
				}
				break;
			case RD_MESH_NODE_VERTEX_UV2:
				{
					Assert(0, "RD_MESH_NODE_VERTEX_UV2 is not implement");
				}
				break;

			case RD_MESH_NODE_VERTEX_TANGENT:
				{
#ifdef TANGENT_FLOAT4
					tangents = new Math::Vector4[num_vertices];
					m_DataMemStream.Read( tangents, sizeof(float), 4*num_vertices );
#else
					tangents = new Math::Vector3[num_vertices];
					m_DataMemStream.Read( tangents, sizeof(float), 3*num_vertices );
#endif

#ifdef __PACK_TANGENT__
					vertexComponents.Add( CnVertexDeclaration::TangentUB4N );
					bumpComponents.Add( CnVertexDeclaration::TangentUB4N );
#else
					vertexComponents.Add( CnVertexDeclaration::Tangent );
					bumpComponents.Add( CnVertexDeclaration::Tangent );
#endif
				}
				break;
			case RD_MESH_NODE_VERTEX_BINORMAL:
				{
					binormals = new Math::Vector3[num_vertices];
					m_DataMemStream.Read( binormals, sizeof(float), 3*num_vertices );

#ifdef __PACK_TANGENT__
					vertexComponents.Add( CnVertexDeclaration::BinormalUB4N );
					bumpComponents.Add( CnVertexDeclaration::BinormalUB4N );
#else
					vertexComponents.Add( CnVertexDeclaration::Binormal );
					bumpComponents.Add( CnVertexDeclaration::Binormal );
#endif
				}
				break;
			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - MoCommon::ChunkHeaderL::HeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		// 임시로 강제로 VertexColor를 사용하도록 한다.
		//if (!useVertexColor)
		//{
		//	vertexComponents.Add( CnVertexDeclaration::Color );
		//	colorComponents.Add( CnVertexDeclaration::Color );

		//	useVertexColor = true;
		//	pMeshNodeData->enableVertexColor = true;
		//}

		CnVertexDeclaration* vertexDeclaration = pMeshNodeData->vertexData->GetDeclaration();

		ushort elementID = 0;
		ushort sourceID = 0;

#ifdef _MULTIPLE_STREAM_

		Ptr<CnVertexBuffer> vertexBuffer;
		Ptr<CnVertexBufferBinder> vertexbufferBinder;

		// position Stream
		if (!posComponents.Empty())
		{
			CnVertexDeclaration::VertexElement posElement = vertexDeclaration->AddElement( elementID, sourceID, posComponents );

			vertexBuffer = CreateVertexBuffer( num_vertices, posElement.byteSize );
			Assert( vertexBuffer != NULL, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );

			vertexbufferBinder = pMeshNodeData->vertexData->GetBufferBinder();
			vertexbufferBinder->Bind( sourceID, vertexBuffer );

			float* buffer = (float*)vertexBuffer->Lock( 0, 0, 0 );
			if (buffer)
			{
				int nComponent = posElement.usageComponent.Get();
				for( ushort i=0; i < num_vertices; ++i )
				{
					if (nComponent & CnVertexDeclaration::Position)
					{
						memcpy( buffer, positions[i], sizeof(float)*3 );
						buffer += 3;
					}
				}
			}
			vertexBuffer->Unlock();

			elementID++;
			sourceID++;
		}

		// normal
		if (!normalComponents.Empty())
		{
			CnVertexDeclaration::VertexElement normElement = vertexDeclaration->AddElement( elementID, sourceID, normalComponents );

			vertexBuffer = CreateVertexBuffer( num_vertices, normElement.byteSize );
			Assert( vertexBuffer != NULL, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );

			vertexbufferBinder = pMeshNodeData->vertexData->GetBufferBinder();
			vertexbufferBinder->Bind( sourceID, vertexBuffer );

			uchar* buffer = (unsigned char*)vertexBuffer->Lock( 0, 0, 0 );
			float* floatBuffer = (float*)buffer;
			if (buffer)
			{
				int nComponent = normElement.usageComponent.Get();
				for( ushort i=0; i < num_vertices; ++i )
				{
					if (nComponent & CnVertexDeclaration::Normal)
					{
						memcpy( floatBuffer, normals[i], sizeof(float)*3 );
						floatBuffer += 3;
					}
					else if (nComponent & CnVertexDeclaration::NormalUB4N)
					{
						Math::Vector3 scaled;
						scaled.x = (normals[i].x+1.0f) * 0.5f;
						scaled.y = (normals[i].y+1.0f) * 0.5f;
						scaled.z = (normals[i].z+1.0f) * 0.5f;

						*buffer++ = (uchar)(scaled.x * 255.0f);
						*buffer++ = (uchar)(scaled.y * 255.0f);
						*buffer++ = (uchar)(scaled.z * 255.0f);
						*buffer++ = 0;
					}
					
				}
			}
			vertexBuffer->Unlock();

			elementID++;
			sourceID++;
		}

		// uv
		if (!uvComponents.Empty())
		{
			CnVertexDeclaration::VertexElement uvElement = vertexDeclaration->AddElement( elementID, sourceID, uvComponents );

			vertexBuffer = CreateVertexBuffer( num_vertices, uvElement.byteSize );
			Assert( vertexBuffer != NULL, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );

			vertexbufferBinder = pMeshNodeData->vertexData->GetBufferBinder();
			vertexbufferBinder->Bind( sourceID, vertexBuffer );

			short* buffer = (short*)vertexBuffer->Lock( 0, 0, 0 );
			float* floatBuffer = (float*)buffer;
			if (buffer)
			{
				int nComponent = uvElement.usageComponent.Get();
				for( ushort i=0; i < num_vertices; ++i )
				{
					if (nComponent & CnVertexDeclaration::Uv0)
					{
						memcpy( floatBuffer, texcoords[i], sizeof(float)*2 );
						floatBuffer += 2;
					}
					else if (nComponent & CnVertexDeclaration::Uv0S2)
					{
						*buffer++ = (short)(Math::Round(texcoords[i].x * 4096.0f));
						*buffer++ = (short)(Math::Round(texcoords[i].y * 4096.0f));
					}
					//if (nComponent & Uv1)      size += 2;
					//if (nComponent & Uv2)      size += 2;
					//if (nComponent & Uv3)      size += 2;
				}
			}
			vertexBuffer->Unlock();

			elementID++;
			sourceID++;
		}

		// color
		if (!colorComponents.Empty())
		{
			CnVertexDeclaration::VertexElement colorElement = vertexDeclaration->AddElement( elementID, sourceID, colorComponents );

			vertexBuffer = CreateVertexBuffer( num_vertices, colorElement.byteSize );
			Assert( vertexBuffer != NULL, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );

			vertexbufferBinder = pMeshNodeData->vertexData->GetBufferBinder();
			vertexbufferBinder->Bind( sourceID, vertexBuffer );

			float* buffer = (float*)vertexBuffer->Lock( 0, 0, 0 );
			if (buffer)
			{
				float color[4];

				int nComponent = colorElement.usageComponent.Get();
				for( ushort i=0; i < num_vertices; ++i )
				{
					if (nComponent & CnVertexDeclaration::Color)
					{ 
						if (colors)
						{
							color[0] = colors[i].x;
							color[1] = colors[i].y;
							color[2] = colors[i].z;
							color[3] = 1.0f;
						}
						else
						{
							color[0] = 1.0f;
							color[1] = 1.0f;
							color[2] = 1.0f;
							color[3] = 1.0f;
						}

						memcpy( buffer, color, sizeof(float)*4 );
						buffer += 4;
					}
					else if(nComponent & CnVertexDeclaration::ColorUB4N)
					{
					}
				}
			}
			vertexBuffer->Unlock();

			elementID++;
			sourceID++;
		}

		// normalMap
		if (!bumpComponents.Empty())
		{
			/// tangent 구하기
			//Math::Vector3* tan1 = new Math::Vector3[num_vertices];
			//Math::Vector3* tan2 = new Math::Vector3[num_vertices];

			//pMeshNodeData->
			//ZeroMemory( tan1, sizeof(Math::Vector3)*num_vertices );
			//ZeroMemory( tan1, sizeof(Math::Vector3)*num_vertices );

			//delete[] tan2;
			//delete[] tan1;

			CnVertexDeclaration::VertexElement bumpElement = vertexDeclaration->AddElement( elementID, sourceID, bumpComponents );

			vertexBuffer = CreateVertexBuffer( num_vertices, bumpElement.byteSize );
			Assert( vertexBuffer != NULL, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );

			vertexbufferBinder = pMeshNodeData->vertexData->GetBufferBinder();
			vertexbufferBinder->Bind( sourceID, vertexBuffer );

			uchar* buffer = (uchar*)vertexBuffer->Lock( 0, 0, 0 );
			float* floatBuffer = (float*)buffer;
			if (buffer)
			{
				int nComponent = bumpElement.usageComponent.Get();
				for( ushort i=0; i < num_vertices; ++i )
				{
					// tangent / Binormal
					if (nComponent & CnVertexDeclaration::Tangent)
					{
#ifdef TANGENT_FLOAT4
						memcpy( floatBuffer, tangents[i], sizeof(float)*4 );
						floatBuffer += 4;
#else
						memcpy( floatBuffer, tangents[i], sizeof(float)*3 );
						floatBuffer += 3;
#endif
					}
					else if (nComponent & CnVertexDeclaration::TangentUB4N)
					{
						Math::Vector3 scaled;
						scaled.x = (tangents[i].x+1.0f) * 0.5f;
						scaled.y = (tangents[i].y+1.0f) * 0.5f;
						scaled.z = (tangents[i].z+1.0f) * 0.5f;

						*buffer++ = (uchar)(scaled.x * 255.0f);
						*buffer++ = (uchar)(scaled.y * 255.0f);
						*buffer++ = (uchar)(scaled.z * 255.0f);
						*buffer++ = 0;
					}

					if (nComponent & CnVertexDeclaration::Binormal)
					{
						memcpy( floatBuffer, binormals[i], sizeof(float)*3 );
						floatBuffer += 3;
					}
					else if (nComponent & CnVertexDeclaration::BinormalUB4N)
					{
						Math::Vector3 scaled;
						scaled.x = (binormals[i].x+1.0f) * 0.5f;
						scaled.y = (binormals[i].y+1.0f) * 0.5f;
						scaled.z = (binormals[i].z+1.0f) * 0.5f;

						*buffer++ = (uchar)(scaled.x * 255.0f);
						*buffer++ = (uchar)(scaled.y * 255.0f);
						*buffer++ = (uchar)(scaled.z * 255.0f);
						*buffer++ = 0;
					}
				}
			}
			vertexBuffer->Unlock();

			elementID++;
			sourceID++;
		}

		// skin
		if (!skinComponents.Empty())
		{
			CnVertexDeclaration::VertexElement skinElement = vertexDeclaration->AddElement( elementID, sourceID, skinComponents );

			vertexBuffer = CreateVertexBuffer( num_vertices, skinElement.byteSize );
			Assert( vertexBuffer != NULL, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );

			vertexbufferBinder = pMeshNodeData->vertexData->GetBufferBinder();
			vertexbufferBinder->Bind( sourceID, vertexBuffer );

			uchar* buffer = (uchar*)vertexBuffer->Lock(0, 0, 0);
			float* floatBuffer = (float*)buffer;
			if (buffer)
			{
				float indices[4];
				int nComponent = skinElement.usageComponent.Get();
				for (ushort i=0; i < num_vertices; ++i)
				{
					// 4-link blend
					if (nComponent & CnVertexDeclaration::BlendWeights)
					{
						memcpy( floatBuffer, blendDatas[i].weight, sizeof(float)*4 );
						floatBuffer += 3;
					}
					else if (nComponent & CnVertexDeclaration::BlendWeightsUB4N)
					{
						// weight 값은 0.0~1.0 사이의 값이므로, scale하지 않고 그대로 pack한다.
						*buffer++ = (uchar)(blendDatas[i].weight[0] * 255.0f);
						*buffer++ = (uchar)(blendDatas[i].weight[1] * 255.0f);
						*buffer++ = (uchar)(blendDatas[i].weight[2] * 255.0f);
						*buffer++ = (uchar)(blendDatas[i].weight[3] * 255.0f);
					}

					if (nComponent & CnVertexDeclaration::BlendIndices)
					{
						for (int j=0; j<4; ++j)
							indices[j] = (float)blendDatas[i].bone[j];

						memcpy( floatBuffer, indices, sizeof(float)*4 );
						floatBuffer += 4;
					}
					else if (nComponent & CnVertexDeclaration::BlendIndicesUB4)
					{
						*buffer++ = (uchar)(blendDatas[i].bone[0]);
						*buffer++ = (uchar)(blendDatas[i].bone[1]);
						*buffer++ = (uchar)(blendDatas[i].bone[2]);
						*buffer++ = (uchar)(blendDatas[i].bone[3]);
					}
				}
			}
			vertexBuffer->Unlock();

			elementID++;
			sourceID++;
		}

		vertexDeclaration->Build();

#else
		CnVertexDeclaration::VertexElement vertexElement = vertexDeclaration->AddElement( elementID++, sourceID++, vertexComponents );

			vertexDeclaration->Build();

		//@<
		Ptr<CnVertexBuffer> vertexBuffer = CreateVertexBuffer( num_vertices, vertexElement.byteSize );
		Assert( vertexBuffer != NULL, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );
		//@>

		Ptr<CnVertexBufferBinder> vertexbufferBinder = pMeshNodeData->vertexData->GetBufferBinder();
		vertexbufferBinder->Bind( 0, vertexBuffer );

#pragma NOTE( "버텍스 버퍼를 생성할 때, 선언한 데이터 형이 아닌, 다른 형을 넣으면, 어떤 식으로 돌아갈지 장담할 수 없다")
		float* buffer = vertexBuffer->Lock( 0, 0, 0 );
		if (buffer)
		{
			float color[4];

			int nComponent = vertexElement.usageComponent.Get();
			for( ushort i=0; i < num_vertices; ++i )
			{
				if (nComponent & CnVertexDeclaration::Position)
				{
					memcpy( buffer, positions[i], sizeof(float)*3 );
					buffer += 3;
				}
				if (nComponent & CnVertexDeclaration::Normal)
				{
					memcpy( buffer, normals[i], sizeof(float)*3 );
					buffer += 3;
				}
				if (nComponent & CnVertexDeclaration::Uv0)
				{
					memcpy( buffer, texcoords[i], sizeof(float)*2 );
					buffer += 2;
				}
				//if (nComponent & Uv1)      size += 2;
				//if (nComponent & Uv2)      size += 2;
				//if (nComponent & Uv3)      size += 2;
				if (nComponent & CnVertexDeclaration::Color)
				{ 
					if (colors)
					{
						color[0] = colors[i].x;
						color[1] = colors[i].y;
						color[2] = colors[i].z;
						color[3] = 1.0f;
					}
					else
					{
						color[0] = 1.0f;
						color[1] = 1.0f;
						color[2] = 1.0f;
						color[3] = 1.0f;
					}

					memcpy( buffer, color, sizeof(float)*4 );
					buffer += 4;
				}

				// tangent / Binormal
				if (nComponent & CnVertexDeclaration::Tangent)
				{
					memcpy( buffer, tangents[i], sizeof(float)*3 );
					buffer += 3;
				}
				if (nComponent & CnVertexDeclaration::Binormal)
				{
					memcpy( buffer, binormals[i], sizeof(float)*3 );
					buffer += 3;
				}

				// 4-link blend
				if (nComponent & CnVertexDeclaration::BlendWeights)
				{
					memcpy( buffer, blendDatas[i].weight, sizeof(float)*4 );
					buffer += 4;
				}
				if (nComponent & CnVertexDeclaration::BlendIndices)
				{
					float indices[4];
					for (int j=0; j<4; ++j)
						indices[j] = (float)blendDatas[i].bone[j];

					memcpy( buffer, indices, sizeof(float)*4 );
					buffer += 4;
				}
			}
		}
		vertexBuffer->Unlock();
#endif	// _MULTIPLE_STREAM_

		SAFEDELS( positions );
		SAFEDELS( normals );
		SAFEDELS( texcoords );
		SAFEDELS( colors );
		SAFEDELS( blendDatas );
		SAFEDELS( tangents );
		SAFEDELS( binormals );

		return true;
	}

	//================================================================
	/** Read Mesh Node Indices
	    @remarks      메쉬 노드 인덱스 정보
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeFaces( ulong ulDataSize, Export::MeshNode* pMeshNodeData )
	{
		MoCommon::ChunkHeaderL chunk;

		ulong leftsize = ulDataSize - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag) 
			{
			case RD_MESH_NODE_FACE:
				{
					ReadMeshNodeFaceGroup( chunk.ulSize, pMeshNodeData );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - MoCommon::ChunkHeaderL::HeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		return true;
	}

	//================================================================
	/** Read Mesh Node Indices
	    @remarks      메쉬 노드 인덱스 정보
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeFaceGroup( ulong ulDataSize, Export::MeshNode* pMeshNodeData )
	{
		ushort num_faces = 0;
		m_DataMemStream.Read( &num_faces, sizeof(ushort), 1 );
		if( num_faces <= 0 )
			return false;

		Export::SubMesh* submeshdata = new Export::SubMesh();
		submeshdata->indexbuffer = CreateIndexBuffer( num_faces*3 );
		Assert( submeshdata->indexbuffer != NULL, L"[CnMeshData::ReadMeshNodeFaceGroup] Could not create indexbuffer" );

		MoCommon::ChunkHeaderL chunk;
		ulong leftsize = ulDataSize - sizeof(ushort) - MoCommon::ChunkHeaderL::HeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag) 
			{
			case RD_MESH_NODE_FACE_INDICES:
				{
					ushort* indices = submeshdata->indexbuffer->Lock( 0, 0, 0 );
					if( indices )
					{
						m_DataMemStream.Read( indices, sizeof(ushort), submeshdata->indexbuffer->GetNumIndices() );
					}
					submeshdata->indexbuffer->Unlock();

					//CnPrint( L"Submesh IndexBuffer: %s", GetName().c_str() );
					//for( ushort i = 0; i < num_faces; ++i )
					//{
					//	CnPrint( L"[%d] %d %d %d", i, indices[i*3+0], indices[i*3+1], indices[i*3+2] );
					//}
				}
				break;

			case RD_MESH_NODE_FACE_DIMAP:
				{
					CnString map, filename;
					ReadNameData( map );

					filename = unicode::SplitPathFileName( map );
					submeshdata->material.diffuseMap = filename + L".dds";
				}
				break;
			case RD_MESH_NODE_FACE_SPMAP:
				{
					CnString map, filename;
					ReadNameData( map );

					filename = unicode::SplitPathFileName( map );
					submeshdata->material.specularMap = filename + L".dds";
				}
				break;

			case RD_MESH_NODE_FACE_TWOSIDE:
				{
					BYTE enable = 0;
					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
					submeshdata->material.twoside = (enable==1) ? true : false;
				}
				break;
			case RD_MESH_NODE_FACE_OPACITY:
				{
					BYTE enable = 0;
					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
					submeshdata->material.opacity = (enable==1) ? true : false;
				}
				break;
			case RD_MESH_NODE_FACE_BLENDING:
				{
					BYTE enable = 0;
					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
					submeshdata->material.blending = (enable==1) ? true : false;
				}
				break;

			case RD_MESH_NODE_FACE_NRMAP:
				{
					CnString map, filename;
					ReadNameData( map );

					filename = unicode::SplitPathFileName( map );
					submeshdata->material.bumpMap = filename;
				}
				break;

			case RD_MESH_NODE_FACE_MTLID:
				{
					ushort mtlid = 0xffff;
					m_DataMemStream.Read( &mtlid, sizeof(ushort), 1 );
					submeshdata->materialID = mtlid;
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - MoCommon::ChunkHeaderL::HeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		pMeshNodeData->submeshdatas.push_back( submeshdata );
		return true;
	}

	//================================================================
	/** ReadAABB
	    @remarks      AABB 정보 설정
	*/
	//================================================================
	void CnMeshData::ReadAABB( Export::MeshNode* pMeshNodeData )
	{
		Math::Vector3 min, max;
		m_DataMemStream.Read( &min, sizeof(float), 3 );
		m_DataMemStream.Read( &max, sizeof(float), 3 );

		pMeshNodeData->aabb.SetExtents( min, max );
	}

	//================================================================
	/** ReadOBB
	    @remarks      OBB 정보 설정
	*/
	//================================================================
	void CnMeshData::ReadOBB( Export::MeshNode* pMeshNodeData )
	{
		Math::Vector3 center;
		Math::Vector3 axis[3];
		float extent[3];

		m_DataMemStream.Read( &center, sizeof(float), 3 );

		ushort i = 0;
		for( i = 0; i < 3; ++i )
			m_DataMemStream.Read( &axis[i], sizeof(float), 3 );

		for( i = 0; i < 3; ++i )
			m_DataMemStream.Read( &extent[i], sizeof(float), 1 );

		pMeshNodeData->obb.SetCenter( center );
		pMeshNodeData->obb.SetAxis( axis );
		pMeshNodeData->obb.SetExtent( extent );
	}

	//================================================================
	/** Create Node Data
	    @remarks      리소스 노드 데이터를 생성한다.
	*/
	//================================================================
	Export::Node* CnMeshData::CreateNodeData( BYTE Type )
	{
		Export::Node* result = NULL;

		switch (Type)
		{
		case Export::Node::MESH:
			{
				result = CnNew Export::MeshNode();
				result->type = Export::Node::MESH;
			}
			break;

		case Export::Node::DUMMY:
			{
				result = CnNew Export::Node();
				result->type = Export::Node::DUMMY;
			}
			break;

		case Export::Node::BONE:
			{
				result = CnNew Export::Node();
				result->type = Export::Node::BONE;
			}
			break;
		}
		return result;
	}

	//================================================================
	/** Create VerteBuffer
	    @remarks      버텍스 버퍼를 생성한다.
	*/
	//================================================================
	CnVertexBuffer* CnMeshData::CreateVertexBuffer( uint nNumVertices, uint nSizeOfVertex )
	{
		CnVertexBuffer* vertexBuffer = RenderDevice->CreateVertexBuffer();

		//! 풀 메쉬일 경우, System 메모리에 버퍼를 잡는다.
		CnVertexBuffer::POOL pool;
		DWORD usage = 0;
		//if( m_bSystemMem )
		//{
		//	pool = D3DPOOL_SYSTEMMEM;
		//	usage = D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC;
		//}
		//else
		{
			pool = CnVertexBuffer::POOL_MANAGED;
			usage = CnVertexBuffer::USAGE_WRITEONLY;
		}

		if( vertexBuffer->Create( nNumVertices, nSizeOfVertex, usage, pool ) )
		{
			return vertexBuffer;
		}

		delete vertexBuffer;
		return NULL;
	}

	//================================================================
	/** Create IndexBuffer
	    @remarks      인덱스 버퍼를 생성한다.
	*/
	//================================================================
	CnIndexBuffer* CnMeshData::CreateIndexBuffer( ushort usNumIndices )
	{
		CnIndexBuffer* buffer = RenderDevice->CreateIndexBuffer();

		CnIndexBuffer::POOL pool;
		DWORD usage = 0;
		//if( m_bSystemMem )
		//{
		//	pool = D3DPOOL_SYSTEMMEM;
		//	usage = D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC;
		//}
		//else
		{
			pool = CnIndexBuffer::POOL_MANAGED;
			usage = CnIndexBuffer::USAGE_WRITEONLY;
		}

		if( buffer->Create( usNumIndices, usage, CnIndexBuffer::FMT_INDEX16, pool ) )
			return buffer;

		delete buffer;
		return NULL;
	}

	//================================================================
	/** Read Name Data
	    @remarks      이름 정보를 읽어온다.
	*/
	//================================================================
	void CnMeshData::ReadNameData( CnString& rResult )
	{
		ushort length = 0;
		m_DataMemStream.Read( &length, sizeof(ushort), 1 );
		if( length > 0 )
		{
			wchar buffer[128];
			memset( buffer, 0, sizeof(wchar) * 128 );
			m_DataMemStream.Read( buffer, sizeof(wchar), (ulong)length );

			rResult = buffer;
		}
	}
}