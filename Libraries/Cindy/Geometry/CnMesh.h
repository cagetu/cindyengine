//================================================================
// File:           : CnMesh.h
// Original Author : changhee
// Creation Date   : 2007. 1. 31
//================================================================
#ifndef __CN_MESH_H__
#define __CN_MESH_H__

#include "Math/CindyMath.h"
#include "Scene/CnSceneEntity.h"
#include "Material/CnMaterialData.h"
#include "Resource/CnExportedDataDefines.h"
#include "Animation/CnSkeletonInstance.h"
#include "Lighting/ShadowMap/CnBuildSM.h"

#include "CnSubMesh.h"
#include "CnVertexData.h"

namespace Cindy
{
	class CnSubMesh;
	class CnLight;

	//================================================================
	/** Mesh Class
	    @author    changhee
		@since     2007. 1. 31
		@remarks   Mesh 객체 
				   한 Mesh에 대한 정보를 가지고 있다고 보면 된다.
	*/
	//================================================================
	class CnMesh : public CnSceneEntity
	{
		__DeclareRtti;
	public:
		typedef std::vector<CnLight*>	LightArray;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		CnMesh( const CnString& strName );
		virtual ~CnMesh();

		//static CnMesh*				CreateBox(const CnString& name, int w, int h, int d);

		// SubMesh
		void						AddSubMesh( CnSubMesh* pSubMesh );
		void						RemoveSubMesh( CnSubMesh* pSubMesh );
		void						RemoveAllSubMeshes();
		CnSubMesh*					GetSubMesh( int nIndex ) const;
		ushort						GetNumSubMeshes() const;

		// Skeleton
		void						SetSkeleton( const Ptr<CnSkeletonInstance>& pSkeleton );
		// Skin
		void						SetSkinnedData( Ptr<Export::Skinned>& pSkinnedData );
		ushort						GetNumBlend() const;

		bool						IsSkinned() const;

		// Material
		void						SetMaterial( const Ptr<CnMaterialData>& pMaterialData );
		void						SetMaterial( const Ptr<CnMaterial>& Material );

		// Vertex
		void						SetVertexData( const Ptr<CnVertexData>& pVertexData );
		const Ptr<CnVertexData>&	GetVertexData();

		bool						SetStreamSource( uint nStreamID = 0, uint nOffsetInBytes = 0, bool bSplit = false );
		void						SetVertexDeclaration();
		uint						GetNumVertices() const;

		// WorldXForm
		void						GetWorldXForm( Math::Matrix44& rOut, bool bForceFromParent = false );
		void						GetWorldXForms( Math::Matrix44* aXForms, int& rNumXForms );
		const Math::Matrix44&		GetWorldViewProjection() const;

		// Distance
		float						GetDistanceToCamera() const;
		bool						IsChangedDistanceToCamera() const;

		// Lights
		const LightArray&			GetInfluencedLights() const;

		// Shadow
		void						SetCastShadow( bool bEnable ) override;

		// Occlusion
		void						SetOccluder( bool bOccluder );
		bool						IsOccluder() const;

		// Bounding Volume
		void						SetAABB( const Math::AABB& Aabb );
		void						GetAABB( Math::AABB& rOut ) override;

		void						SetOBB( const Math::OBB& Obb );
		void						GetOBB( Math::OBB& rOut );

		// Relation
		void						RelationTo( CnSceneEntity* pEntity, CnCamera* pCamera ) override;
		void						ClearRelation() override;

		// Culling
		bool						Cull( CnCamera* pCamera ) override;
		//
		void						Apply( CnCamera* pCamera ) override;

		// Update
		bool						Update( CnCamera* pCamera ) override;
		// Draw
		void						Draw( CnCamera* pCamera ) override;

	private:
		typedef std::vector<CnSubMesh*>		SubMeshArray;
		typedef SubMeshArray::iterator		SubMeshIter;

		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		SubMeshArray				m_SubMeshes;
		unsigned short				m_usNumSubMeshes;

		// Vertex
		Ptr<CnVertexData>			m_VertexData;
		Ptr<Export::Skinned>		m_pSkinnedData;
		Ptr<CnSkeletonInstance>		m_pSkeletonInst;			//! 영향을 받는 Skeleton

		// Lights
		bool						m_bNeedUpdateLights;
		LightArray					m_InfluencedLights;

		// BBox
		Math::AABB					m_WorldAabb;
		Math::AABB					m_LocalAabb;
		Math::OBB					m_WorldObb;
		Math::OBB					m_LocalObb;

		Math::Matrix44				m_lastXAABB;
		Math::Matrix44				m_lastXOBB;

		Math::Matrix44				m_WorldViewProjection;

		// Occlusion
		bool						m_bOccluder;

		// Distance
		float						m_fDistanceToCamera;
		bool						m_IsChangedDistanceToCamera;
	};

	#include "CnMesh.inl"
}

#endif	// __CN_MESH_H__