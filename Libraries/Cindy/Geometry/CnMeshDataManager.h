//================================================================
// File:           : CnMeshDataManager.h
// Original Author : changhee
// Creation Date   : 2007. 4. 12
//================================================================
#pragma once

#include "Resource/CnResourceManager.h"
#include "Util/CnSingleton.h"

namespace Cindy
{
	//================================================================
	/** Mesh Data Manager
	    @author    changhee
		@since     2007. 4. 12
		@remarks   皋浆 府家胶 包府磊
	*/
	//================================================================
	class CN_DLL CnMeshDataManager : public CnResourceManager
	{
		__DeclareClass(CnMeshDataManager);
		__DeclareSingleton(CnMeshDataManager);
	public:
		CnMeshDataManager();
		virtual ~CnMeshDataManager();

		// 府家胶 积己/佬扁
		RscPtr	Load( const wchar* strFileName ) override;
	};

//#include "CnMeshDataManager.inl"
// Macro
#define MeshDataMgr	CnMeshDataManager::Instance()
}

