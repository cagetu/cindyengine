//================================================================
// File:               : CnVertexData.cpp
// Related Header File : CnVertexData.h
// Original Author     : changhee
// Creation Date       : 2008. 8. 11
//================================================================
#include "Cindy.h"
#include "Graphics/CnRenderer.h"
#include "CnVertexData.h"

namespace Cindy
{
	/// Constructor
	CnVertexData::CnVertexData()
		: m_nNumVertices(0)
	{
		m_pDeclaration = RenderDevice->CreateVertexDeclaration();
		m_pBufferBinder = CnNew CnVertexBufferBinder();
	}
	/// Destructor
	CnVertexData::~CnVertexData()
	{
		m_pBufferBinder = 0;
		m_pDeclaration = 0;
	}

	//--------------------------------------------------------------
	/**	@brief	Vertex Component�� �ִ��� �ľ�
	*/
	bool CnVertexData::HasVertexComponent( CnVertexDeclaration::USAGE eComponent )
	{
		return m_pDeclaration->HasComponent( eComponent );
	}
}