//================================================================
// File:           : CnSubMesh.h
// Original Author : changhee
// Creation Date   : 2007. 1. 31
//================================================================
#ifndef __CN_SUBMESH_H__
#define __CN_SUBMESH_H__

#include "Material/CnMaterial.h"
#include "Graphics/Base/CnIndexBuffer.h"
#include "Graphics/CnRenderSortKey.h"

namespace Cindy
{
	class CnMesh;
	class CnCamera;

	namespace Math
	{
		class Matrix44;
	}

	//================================================================
	/** SubMesh Class
	    @author    changhee
		@since     2007. 1. 31
		@remarks   하위 객체 클래스.
				   3D Max의 한 단위 매터리얼 단위로 구성된 하위 메쉬 객체
				   Material(텍스쳐) 별로 나뉜 하위 객체
	*/
	//================================================================
	class CnSubMesh : public CnObject
	{
		__DeclareRtti;
	public:
		CnSubMesh();
		virtual ~CnSubMesh();

		// Parent
		void						SetParent( CnMesh* Parent );
		CnMesh*						GetParent() const;

		// Key
		const RenderGroup::SortKey&	GetSortKey() const;

		// Material
		void						SetMtlID( ushort Index );
		ushort						GetMtlID() const;

		void						SetMaterial( const Ptr<CnMaterial>& rMaterial );
		const Ptr<CnMaterial>&		GetMaterial() const;

		// IndexBuffer
		void						SetIndexBuffer( const Ptr<CnIndexBuffer>& pIndexBuffer );
		const Ptr<CnIndexBuffer>&	GetIndexBuffer() const;

		// XForm
		void						GetWorldXForm( Math::Matrix44& rOut );

		///
		void						UpdateMaterial();

		//
		void						ApplyState( CnCamera* pCamera );

		// 그리기
		bool						Update( CnCamera* pCamera );
		bool						Draw( CnCamera* pCamera );

	private:
		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		CnMesh*							m_Parent;

		mutable bool					m_bNeedToBuildKey;
		mutable RenderGroup::SortKey	m_Key;

		Ptr<CnIndexBuffer>				m_pIndexBuffer;

		// Material
		ushort							m_MaterialID;
		Ptr<CnMaterial>					m_Material;
		mutable bool					m_bFeatureDirty;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		void	MakeKey() const;

		void	ApplyModelView( CnCamera* pCamera );
		void	ApplyGeometry();
		void	ApplyMaterial();
		void	ApplyLight();
		void	ApplyTextures();
		void	ApplyShadow();
	};

	#include "CnSubMesh.inl"
}

#endif	// __CN_SUBMESH_H__