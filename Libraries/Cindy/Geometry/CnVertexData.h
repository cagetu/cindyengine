//================================================================
// File:               : CnVertexData.h
// Original Author     : changhee
// Creation Date       : 2008. 8. 11
//================================================================
#ifndef __CN_VERTEXDATA_H__
#define __CN_VERTEXDATA_H__

#include "Graphics/Base/CnVertexBuffer.h"
#include "Graphics/Base/CnVertexDeclaration.h"

namespace Cindy
{
#define _MULTIPLE_STREAM_
	//================================================================
	/** CnVertexData Class
	    @author    changhee
		@since     2008. 8. 11
		@brief	   메쉬의 버텍스 데이터에 대한 구조체
	*/
	//================================================================
	class CnVertexData : public CnRefCount
	{
	public:
		CnVertexData();
		virtual ~CnVertexData();

		// Vertex Count
		void								SetNumVertices( uint Number );
		uint								GetNumVertices() const;

		// To Renderer
		const Ptr<CnVertexDeclaration>&		GetDeclaration() const;

		// check vertex component
		bool								HasVertexComponent( CnVertexDeclaration::USAGE eComponent );

		// Buffer
		void								AddBuffer( ushort StreamID, Ptr<CnVertexBuffer>& pBuffer );
		const Ptr<CnVertexBuffer>&			GetBuffer( uint StreamID ); 
		const Ptr<CnVertexBufferBinder>&	GetBufferBinder() const;

	private:
		uint						m_nNumVertices;
		Ptr<CnVertexDeclaration>	m_pDeclaration;
		Ptr<CnVertexBufferBinder>	m_pBufferBinder;
	};

	#include "CnVertexData.inl"
}

#endif	// __CN_VERTEXDATA_H__