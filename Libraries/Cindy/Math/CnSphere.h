// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

#include "CnVector3.h"

namespace Cindy
{
namespace Math
{
	class AABB;

	//================================================================
	/** Sphere
		@author    changhee
		@since     2008. 2. 12
		@remarks   경계 구
	*/
	//================================================================
	class CN_DLL Sphere
	{
	public:
		Sphere();
		Sphere( const Sphere& sphere );
		explicit Sphere( const Math::Vector3* points, int size );
		explicit Sphere( Math::AABB* aabb );

		// 교차 판정
		bool		Intersect( const Sphere& sphere );
		bool		Intersect( const Math::Vector3& rayOrig, const Math::Vector3& rayDir );

	public:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		Math::Vector3		center;
		float			radius;
	};
}
}
