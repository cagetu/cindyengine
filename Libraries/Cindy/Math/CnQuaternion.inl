// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
inline Quaternion::Quaternion()
: D3DXQUATERNION()
{
	Identity();
}

//--------------------------------------------------------------
inline Quaternion::Quaternion( CONST FLOAT* pArray )
: D3DXQUATERNION( pArray )
{
}

//--------------------------------------------------------------
inline Quaternion::Quaternion( FLOAT fX, FLOAT fY, FLOAT fZ, FLOAT fW )
: D3DXQUATERNION( fX, fY, fZ, fW )
{
}

//--------------------------------------------------------------
inline Quaternion::Quaternion( const Quaternion& Q_ )
: D3DXQUATERNION( Q_.x, Q_.y, Q_.z, Q_.w )
{
}

//--------------------------------------------------------------
inline Quaternion::Quaternion( const D3DXQUATERNION& Q_ )
: D3DXQUATERNION( Q_.x, Q_.y, Q_.z, Q_.w )
{
}

//--------------------------------------------------------------
inline Quaternion::Quaternion( const Math::Vector3& vAxis, Radian fRadian )
{
	D3DXQuaternionRotationAxis( this, &vAxis, fRadian );
}

//--------------------------------------------------------------
inline void Quaternion::Identity()
{
	D3DXQuaternionIdentity( this );
}

//--------------------------------------------------------------
inline float Quaternion::Length() const
{
	return D3DXQuaternionLength( this );
}

//--------------------------------------------------------------
inline float Quaternion::LengthSq() const
{
	return D3DXQuaternionLengthSq( this );
}

//--------------------------------------------------------------
inline float Quaternion::Dot( const Quaternion& rQuat ) const
{
	return D3DXQuaternionDot( this, &rQuat );
}

//--------------------------------------------------------------
inline void Quaternion::Normalize()
{
	D3DXQuaternionNormalize( this, this );
}

//--------------------------------------------------------------
inline void Quaternion::Inverse( Quaternion& rResult ) const
{
	D3DXQuaternionInverse( &rResult, this );
}

//--------------------------------------------------------------
inline void Quaternion::Exp( Quaternion& rResult ) const
{
	D3DXQuaternionExp( &rResult, this );
}

//--------------------------------------------------------------
inline void Quaternion::Log( Quaternion& rResult ) const
{
    // If q = cos(A)+sin(A)*(x*i+y*j+z*k) where (x,y,z) is unit length, then
    // log(q) = A*(x*i+y*j+z*k).  If sin(A) is near zero, use log(q) =
    // sin(A)*(x*i+y*j+z*k) since sin(A)/A has limit 1.

	rResult.x = 0.0f;

	int i = 0;

	if( fabsf( x ) < 1.0f )
	{
		float fAngle = cosf( x );
		float fSin = sinf( fAngle );

		if( fabsf( fSin ) >= Math::ZERO_TOLERANCE )
		{
			float fCoeff = fAngle / fSin;

			rResult.x = fCoeff * x;
			rResult.y = fCoeff * y;
			rResult.z = fCoeff * z;
			return;
		}
	}

	rResult.x = x;
	rResult.y = y;
	rResult.z = z;
}

//--------------------------------------------------------------
inline void Quaternion::FromAxisAngle( const Math::Vector3& rAxis, Radian fAngle )
{
	D3DXQuaternionRotationAxis( this, &rAxis, fAngle );
}

//--------------------------------------------------------------
inline void Quaternion::ToAxisAngle( Math::Vector3& rAxis, Radian& fAngle ) const
{
	D3DXQuaternionToAxisAngle( this, &rAxis, &fAngle );
}

//--------------------------------------------------------------
/**	@desc	Euler각도로 사원수 변환

				  ^ y(yaw)
				  |
				  +--> z(roll)
				 /
				v x(pitch)

			quat pitch(vec3(1,0,0), anglePitch);
			quat yaw(vec3(0,1,0), angleYaw);
			quat roll(vec3(0,0,1), angleRoll);

			quat result = yaw * pitch * roll;
*/
//--------------------------------------------------------------
inline
void Quaternion::FromEuler( Radian Yaw, Radian Pitch, Radian Roll )
{
	D3DXQuaternionRotationYawPitchRoll( this, Yaw, Pitch, Roll );
}

//--------------------------------------------------------------
inline void Quaternion::Slerp( const Quaternion& rP, const Quaternion& rQ, float fT )
{
	D3DXQuaternionSlerp( this, &rP, &rQ, fT );
}

//--------------------------------------------------------------
inline void Quaternion::Multiply( const Quaternion& q1, const Quaternion& q2 )
{
	D3DXQuaternionMultiply( this, &q1, &q2 );
}

//--------------------------------------------------------------
/** @brief	사원수 압축
	@desc	w값 생략
			w의 크기는 x, y, z 값으로 계산 가능
			q = -q 성질을 이용하여 w값을 양수로
*/
//--------------------------------------------------------------
inline
void Quaternion::Encode( Quaternion::Encoded& Output )
{
	if (this->w < 0.0f)
	{
		Output.x = -x;
		Output.y = -y;
		Output.z = -z;
	}
	else
	{
		Output.x = x;
		Output.y = y;
		Output.z = z;
	}
}

//--------------------------------------------------------------
/** @brief	사원수 압축
	@desc	w값 생략
			w의 크기는 x, y, z 값으로 계산 가능
			q = -q 성질을 이용하여 w값을 양수로
*/
//--------------------------------------------------------------
inline
void Quaternion::Decode( const Quaternion::Encoded& Input )
{
	x = Input.x;
	y = Input.y;
	z = Input.z;
	w = sqrt(1.0f - (Input.x*Input.x + Input.y*Input.y + Input.z*Input.z));
}

//--------------------------------------------------------------
/** @brief	사원수 압축
*/
//--------------------------------------------------------------
inline
void Quaternion::Encode( float16 Output[4] )
{
	Output[0] = f32Tof16( x );
	Output[1] = f32Tof16( y );
	Output[2] = f32Tof16( z );
	Output[3] = f32Tof16( w );
}

//--------------------------------------------------------------
/** @brief	사원수 압축
*/
//--------------------------------------------------------------
inline
void Quaternion::Decode( const float16 Output[4] )
{
	x = f16Tof32( Output[0] );
	y = f16Tof32( Output[1] );
	z = f16Tof32( Output[2] );
	w = f16Tof32( Output[3] );
}