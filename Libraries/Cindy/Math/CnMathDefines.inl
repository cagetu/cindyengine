// Copyright (c) 2006~. cagetu
//
//******************************************************************

namespace Math
{
	//---------------------------------------------------------------
	inline
	float Cos( float fAngle )
	{
		return cosf( fAngle );
	}
	inline
	float Sin( float fAngle )
	{
		return sinf( fAngle );
	}
	inline
	float Tan( float fAngle )
	{
		return tanf( fAngle );
	}

	//---------------------------------------------------------------
	inline
	float ACos( float fCos )
	{
		if (-1.0f < fCos)
		{
			if (fCos < 1.0f)
				return float(acos(fCos));
			else
				return 0.0f;
		}

		return Math::PI;
	}
	inline
	float ASin( float fSin )
	{
		if (-1.0f < fSin)
		{
			if (fSin < 1.0f)
				return float( asin(fSin) );
			else
				return Math::HALF_PI;
		}

		return -Math::HALF_PI;
	}
	inline
	float ATan( float fTan )
	{
		return atanf( fTan );
	}
	inline
	float ATan2( float fTan, float fTan2 )
	{
		return atan2f( fTan, fTan2 );
	}

	//---------------------------------------------------------------
	inline
	float Abs( float fVal )
	{
		return fabsf(fVal);
	}
	inline
	float Sqrt( float fVal )
	{
		return sqrtf(fVal);
	}

	//---------------------------------------------------------------
	inline
	float Min( float a, float b )
	{
		return min( a, b );
	}
	//---------------------------------------------------------------
	inline
	float Max( float a, float b )
	{
		return max( a, b );
	}

	//---------------------------------------------------------------
	/** @desc	0 ~ 1 사이의 값으로 범위 변경
	*/
	inline
	float Saturate( float value )
	{
		if (value < 0.0f)		return 0.0f;
		else if (value > 1.0f)	return 1.0f;

		return value;
	}

	//---------------------------------------------------------------
	/**	@desc	제한된 범위로 제한한다.
	*/
	//---------------------------------------------------------------
	inline
	float Clamp( float target, float minValue, float maxValue )
	{
		assert( minValue < maxValue );
		float result( target );

		if (target > maxValue)
			result = maxValue;
		else if (target < minValue)
			result = minValue;

		return result;
	}

	//---------------------------------------------------------------
	/**	@desc	Pow
	*/
	//---------------------------------------------------------------
	inline
	float Pow( float x, float y )
	{
		return powf( x, y );
	}

	//---------------------------------------------------------------
	/** @desc	Degree를 Radian으로 변환 */
	//---------------------------------------------------------------
	inline
	Radian DegreeToRadian( Degree Deg )
	{
		return Deg * Deg2Rad;
	}

	//---------------------------------------------------------------
	/** @desc	Radian을 Degree로 변환 */
	//---------------------------------------------------------------
	inline
	Degree RadianToDegree( Radian Rad )
	{
		return Rad * Rad2Deg;
	}

	//---------------------------------------------------------------
	// Name: GaussianDistribution
	// Desc: Helper function for GetSampleOffsets function to compute the 
	//       2 parameter Gaussian distrubution using the given standard deviation
	//       rho
	//---------------------------------------------------------------
	inline
	float GaussianDistribution( float x, float y, float rho )
	{
		float g = 1.0f / sqrtf( 2.0f * Math::PI * rho * rho );
		g *= expf( -(x*x + y*y)/(2*rho*rho) );

		return g;
	}

	//---------------------------------------------------------------
	inline
	int Round( float value )
	{
		 return ((value - floor(value) > 0.5) ? (int)ceil(value) : (int)floor(value));
	}

	//---------------------------------------------------------------
	/**
	*/
	inline
	float Lerp( float x, float y, float l )
	{
		return x + l * (y - x);
	}

	//---------------------------------------------------------------
	/**
	*/
	inline
	double Lerp( double x, double y, double l )
	{
		return x + l * (y - x);
	}

	//---------------------------------------------------------------
	/**
	*/
	inline
	float Rand()
	{
		return (float)rand() / (float)RAND_MAX;
	}

	//---------------------------------------------------------------
	/**
	*/
	inline
	float Rand( float min, float max )
	{
		float unit = (float)rand() / (float)RAND_MAX;
		float diff = max - min;
		return min + unit * diff;
	}
}