// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNMATHDEFINES_H__
#define __CNMATHDEFINES_H__

#include <float.h>
#include <math.h>
#include <windows.h>
#include <assert.h>

namespace Cindy
{
typedef float	Radian;
typedef float	Degree;

namespace Math
{
	const static float EPSILON = FLT_EPSILON;
	const static float ZERO_TOLERANCE = 1e-06f;
	const static float PI = 3.14159265f;
	const static float HALF_PI = 1.570796327f;
	const static float Rad2Deg = 180.0f/PI;
	const static float Deg2Rad = PI/180.0f;

	float	Cos( float fAngle );
	float	Sin( float fAngle );
	float	Tan( float fAngle );

	float	ACos( float fCos );
	float	ASin( float fSin );
	float	ATan( float fTan );
	float	ATan2( float fTan, float fTan2 );

	float	Abs( float fVal );
	float	Sqrt( float fVal );

	float	Max( float a, float b );
	float	Min( float a, float b );

	float	Saturate( float value );
	float	Clamp( float target, float minValue, float maxValue );
	float	Pow( float x, float y );

	Radian	DegreeToRadian( Degree Deg );
	Degree	RadianToDegree( Radian Rad );

	float	GaussianDistribution( float x, float y, float rho );

	int		Round( float value );

	double	Lerp( double x, double y, double l );
	float	Lerp( float x, float y, float l );

	float	Rand();
	float	Rand( float min, float max );
}

#define FLT_AS_DW(F) (*(DWORD*)&(F))
#define ALMOST_ZERO(F) ((FLT_AS_DW(F) & 0x7f800000L)==0)
#define IS_SPECIAL(F)  ((FLT_AS_DW(F) & 0x7f800000L)==0x7f800000L)

#include "CnMathDefines.inl"
}

#endif	// __CNMATHDEFINES_H__