//================================================================
// File:           : CnVector2.h
// Original Author : changhee
// Creation Date   : 2007. 4. 12
//================================================================
#pragma once

#include "CnMathDefines.h"
#include "../Cindy.h"

namespace Cindy
{
namespace Math
{
	//================================================================
	/** Vector2
		@author    changhee
		@since     2007. 4. 12
		@brief	   2차원 vector
	*/
	//================================================================
	class CN_DLL Vector2 : public D3DXVECTOR2
	{
	public:
		// 생성자
		Vector2();
		Vector2( CONST FLOAT* pVector );
		Vector2( CONST D3DXVECTOR2& rVector );
		Vector2( CONST D3DXFLOAT16* pVector );
		Vector2( FLOAT x, FLOAT y );
		Vector2( const Vector2& rVector );

		Vector2& operator = ( const Vector2& vInput );

		// 비교 연산자
		bool operator == ( const Vector2& vInput ) const;
		bool operator != ( const Vector2& vInput ) const;

		float	Length() const;
		float	LengthSq() const;

		float	Dot( const Vector2& rVector ) const;
		void	Normalize();

	private:
		int _Compare( const Vector2& rVector ) const;

	};

#include "CnVector2.inl"

}
}