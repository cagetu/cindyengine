// Copyright (c) 2006~. cagetu
//
//******************************************************************

//==============================================================
//	Vector4
//==============================================================
inline
Vector4::Vector4()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
	w = 0.0f;
}

//--------------------------------------------------------------
inline
Vector4::Vector4( CONST FLOAT* f )
: D3DXVECTOR4( f )
{
}

//--------------------------------------------------------------
inline
Vector4::Vector4( CONST D3DXVECTOR4& v )
: D3DXVECTOR4( v )
{
}

//--------------------------------------------------------------
inline
Vector4::Vector4( CONST D3DXFLOAT16* f )
: D3DXVECTOR4( f )
{
}

//--------------------------------------------------------------
inline
Vector4::Vector4( FLOAT x, FLOAT y, FLOAT z, FLOAT w )
: D3DXVECTOR4( x, y, z, w )
{
}

//--------------------------------------------------------------
inline
Vector4::Vector4( const Vector4& v )
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;
}

//--------------------------------------------------------------
inline
Vector4& Vector4::operator = ( const Vector4& v )
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;

	return *this;
}

//--------------------------------------------------------------
inline
int Vector4::_Compare( const Vector4& v ) const
{
	return memcmp( this, &v, sizeof(Vector4) );
}

//--------------------------------------------------------------
inline
bool Vector4::operator == ( const Vector4& v ) const
{
//	return ( ( x == v.x ) && ( y == v.y ) && ( z == v.z ) );
	return _Compare( v ) == 0;
}

//--------------------------------------------------------------
inline
bool Vector4::operator != ( const Vector4& v ) const
{
//	return ( ( x != v.x ) || ( y != v.y ) || ( z != v.z ) );
	return _Compare( v ) != 0;
}

//--------------------------------------------------------------
inline
float Vector4::Length() const
{
	return D3DXVec4Length( this );
}

//--------------------------------------------------------------
inline
float Vector4::LengthSq() const
{
	return D3DXVec4LengthSq( this );
}

//--------------------------------------------------------------
inline
float Vector4::Dot( const Vector4& v ) const
{
	return D3DXVec4Dot( this, &v );
}

//--------------------------------------------------------------
inline
void Vector4::Normalize()
{
	D3DXVec4Normalize( this, this );
}

//--------------------------------------------------------------
/**	@brief	uByte4N -> singed float4
*/
inline
void Vector4::FromUbyte4n( const void* ptr, float w )
{
	float mul = (1.0f / 255.0f);
	const uchar* ucharPtr = (const uchar*)ptr;
	this->x = ((float(ucharPtr[0]) * mul) * 2.0f) - 1.0f;
	this->y = ((float(ucharPtr[1]) * mul) * 2.0f) - 1.0f;
	this->z = ((float(ucharPtr[2]) * mul) * 2.0f) - 1.0f;
	this->w = w;
}
