// Copyright (c) 2006~. cagetu
//
//******************************************************************

//==============================================================
//	Plane
//==============================================================
//--------------------------------------------------------------
inline
Plane::Plane()
{
}
inline
Plane::Plane( float xNormal, float yNormal, float zNormal, float fDistance )
: normal(xNormal, yNormal, zNormal)
, distance(fDistance)
{
}

//--------------------------------------------------------------
inline
Plane::Plane( const Math::Vector3& vNormal, float fDistance )
: normal(vNormal)
, distance(fDistance)
{
}

//--------------------------------------------------------------
inline
Plane::Plane( const Math::Vector3& vNormal, const Math::Vector3& vPoint )
: normal(vNormal)
{
	//distance = -D3DXVec3Dot( &vNormal, &vPoint );
	distance = D3DXVec3Dot( &vNormal, &vPoint );
}

//--------------------------------------------------------------
inline
Plane::Plane( const Math::Vector3& vP0, const Math::Vector3& vP1, const Math::Vector3& vP2 )
{
	Math::Vector3 a = vP1 - vP0;
	Math::Vector3 b = vP2 - vP0;
	
	D3DXVec3Cross( &normal, &a, &b );
	D3DXVec3Normalize( &normal, &normal );

	distance = -D3DXVec3Dot( &normal, &vP0 );
}

//--------------------------------------------------------------
inline
Plane Plane::operator +() const
{
	return *this;
}
inline
Plane Plane::operator -() const
{
	return Plane( -normal, -distance );
}

//--------------------------------------------------------------
inline
bool Plane::operator == ( const Plane& rPlane ) const
{
	return ( ( rPlane.normal == normal ) && ( rPlane.distance == distance ) );
}

//--------------------------------------------------------------
inline
void Plane::Normalize()
{
	//float length = D3DXVec3Length( &normal );
	//D3DXVec3Normalize( &normal, &normal );
	//distance = 1/length * distance;

	D3DXPlaneNormalize((D3DXPLANE*)this, (const D3DXPLANE*)this);
}

//--------------------------------------------------------------
inline
Plane::SIDE Plane::GetSide( const Math::Vector3& vPoint ) const
{
	float fDist = GetDistance( vPoint );

	if( fDist < -Math::EPSILON )
		return NEGATIVE_SIDE;
	
	if( fDist > Math::EPSILON )
		return POSITIVE_SIDE;

	return NO_SIDE;
}
inline
Plane::SIDE Plane::GetSide( const Math::Vector3& vCenter, const Math::Vector3& vHalfSize ) const
{
	float distance = GetDistance(vCenter);
	float maxAbsDist = fabsf(normal.x*vHalfSize.x) + fabsf(normal.y*vHalfSize.y) + fabsf(normal.z*vHalfSize.z);

	if (distance < -maxAbsDist)
		return NEGATIVE_SIDE;

	if (distance > maxAbsDist)
		return POSITIVE_SIDE;

	return NO_SIDE;
}

//--------------------------------------------------------------
inline
float Plane::GetDistance( const Math::Vector3& vPoint ) const
{
	return ( D3DXVec3Dot( &normal, &vPoint ) + distance );
}

//--------------------------------------------------------------
inline
float Plane::Dot( const Math::Vector4& vPoint ) const
{
	return ((normal.x * vPoint.x) +
			(normal.y * vPoint.y) +
			(normal.z * vPoint.z) +
			(distance * vPoint.w));
}
