// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNMATRIX44_H__
#define __CNMATRIX44_H__

#include "CnVector3.h"
#include "CnVector4.h"
#include "CnQuaternion.h"

namespace Cindy
{
namespace Math
{
	//==================================================================
	/** Matrix44
		@author			cagetu
		@since			2006년 9월 31일
		@brief			4x4 행렬
	*/
	//==================================================================
	__declspec(align(16)) 
	class CN_DLL Matrix44 : public D3DXMATRIXA16
	{
	public:
		static const Matrix44	ZERO;
		static const Matrix44	IDENTITY;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		// 생성자
		Matrix44();
		Matrix44( CONST FLOAT* pArray );
		Matrix44( CONST D3DMATRIX& m );
		Matrix44( const Matrix44& m );
		Matrix44( FLOAT _11, FLOAT _12, FLOAT _13, FLOAT _14,
				  FLOAT _21, FLOAT _22, FLOAT _23, FLOAT _24,
				  FLOAT _31, FLOAT _32, FLOAT _33, FLOAT _34,
				  FLOAT _41, FLOAT _42, FLOAT _43, FLOAT _44 );

		// 초기값 설정
		void	MakeZero();
		void	Identity();

		// Get Values..
		void	SetRow( int iRow, const Math::Vector4& rValue );
		void	GetRow( int iRow, Math::Vector4& rResult );
		void	SetColumn( int iCol, const Math::Vector4& rValue );
		void	GetColumn( int iCol, Math::Vector4& rResult );
		void	GetColumn( int iCol, Math::Vector3& rResult );

		// 행렬 구성 성분
		void	SetPosition( const Math::Vector3& vPos );
		void	GetPosition( Math::Vector3& rResult ) const;
		void	SetRotation( const Math::Quaternion& rRot );
		void	GetRotation( Math::Quaternion& rResult ) const;
		void	SetScale( const Math::Vector3& vScale );
		void	GetScale( Math::Vector3& rResult ) const;
		void	SetAxis( const Math::Vector3* pAxis );
		void	GetAxis( Math::Vector3* pResult ) const;

		//void	FromRotation( const Math::Quaternion& rRot );
		//void	ToRotation( Math::Quaternion& rResult ) const;

		void	Build( const Math::Vector3& vPos, const Math::Quaternion& qRot, const Math::Vector3& vScale );
		void	Decompose( Math::Vector3& outScale, Math::Quaternion& outRotation, Math::Vector3& outTranslation ) const;

		void	Translation( float x, float y, float z );
		void	Scaling( float x, float y, float z);

		void	LookAtLH( const Math::Vector3& vEye, const Math::Vector3& vLookAt, const Math::Vector3& vUp );
		void	LookAtRH( const Math::Vector3& vEye, const Math::Vector3& vLookAt, const Math::Vector3& vUp );

		void	PerspLH( float fNearWidth, float fNearHeight, float fZNear, float fZFar );
		void	PerspRH( float fNearWidth, float fNearHeight, float fZNear, float fZFar );
		void	PerspFovLH( float fFov, float fAspect, float fNear, float fFar );
		void	PerspFovRH( float fFov, float fAspect, float fNear, float fFar );
		void	perspOffCenterLH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ );
		void	perspOffCenterRH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ );

		void	OrthoLH( float fWidth, float fHeight, float fZNear, float fZFar );
		void	OrthoRH( float fWidth, float fHeight, float fZNear, float fZFar );
		void	OrthoOffCenterLH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ );
		void	OrthoOffCenterRH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ );

		void	Inverse( Matrix44& rResult ) const;
		void	Transpose( Matrix44& rResult ) const;

		// 행렬 곱
		void	Multiply( const Matrix44& m1, const Matrix44& m2 );
	};

#include "CnMatrix44.inl"
} // namespace Math
} // namespace Cindy

#endif	// __CNMATRIX44_H__
