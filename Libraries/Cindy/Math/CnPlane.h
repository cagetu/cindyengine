// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNPLNAE_H__
#define __CNPLNAE_H__

#include "CnVector3.h"
#include "CnVector4.h"

namespace Cindy
{
namespace Math
{
	class Sphere;

	//==================================================================
	/** Plane Class
		@author				cagetu
		@since				2006년 11월 30일
		@brief				평면 클래스
	*/
	//==================================================================
	__declspec(align(16)) 
	class CN_DLL Plane
	{
	public:
		enum SIDE
		{
			NO_SIDE = 0,
			NEGATIVE_SIDE,
			POSITIVE_SIDE,
		};

		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		Math::Vector3	normal;
		float			distance;
														
		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		Plane();
		Plane( float xNormal, float yNormal, float zNormal, float fDistance );
		Plane( const Math::Vector3& vNormal, float fDistance );
		Plane( const Math::Vector3& vNormal, const Math::Vector3& vPoint );
		Plane( const Math::Vector3& vP0, const Math::Vector3& vP1, const Math::Vector3& vP2 );

		Plane operator +() const;
		Plane operator -() const;

		bool operator == ( const Plane& rPlane ) const;

		void	Normalize();

		SIDE	GetSide( const Math::Vector3& vPoint ) const;
		SIDE	GetSide( const Math::Vector3& vCenter, const Math::Vector3& vHalfSize ) const;
		float	GetDistance( const Math::Vector3& vPoint ) const;

		float	Dot( const Math::Vector4& vPoint ) const;

		bool	Intersect( const Math::Vector3& vRayOrig, const Math::Vector3& vRayDir, float* hitDist );
		bool	Intersect( Math::Vector3* intersectPt, const Plane& p1, const Plane& p2 );
		bool	IntersectSweptSphere( float* t0, float* t1, const Math::Sphere& sphere, const Math::Vector3& sweepDir );
	};

#include "CnPlane.inl"
}
}

#endif	// __CNPLNAE_H__