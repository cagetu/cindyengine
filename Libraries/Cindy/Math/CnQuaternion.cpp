// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnQuaternion.h"
#include "CnVector4.h"
#include "CnMatrix44.h"

namespace Cindy
{
namespace Math
{
	//==================================================================
	//	Quaternion
	//==================================================================
	const Quaternion Quaternion::ZERO( 0.0f, 0.0f, 0.0f, 0.0f );
	const Quaternion Quaternion::IDENTITY( 0.0f, 0.0f, 0.0f, 1.0f );

	//--------------------------------------------------------------
	void Quaternion::Rotate( const Math::Vector3& rVector, Math::Vector3& rResult )
	{
		D3DXMATRIXA16 rotation;
		D3DXMatrixRotationQuaternion( &rotation, this );

		Math::Vector4 result;
		D3DXVec3Transform( &result, &rVector, &rotation );

		rResult.x = result.x;
		rResult.y = result.y;
		rResult.z = result.z;
	}

	//--------------------------------------------------------------
	/** @brief	회전 행렬로 부터 Quaternion 얻기 */
	//--------------------------------------------------------------
	void Quaternion::FromMatrix( const Math::Matrix44& mRotation )
	{
		D3DXQuaternionRotationMatrix( this, &mRotation );
	}

	//--------------------------------------------------------------
	/** @brief	Quaternion부터 회전 행렬 얻기 */
	//--------------------------------------------------------------
	void Quaternion::ToMatrix( Math::Matrix44& outResult ) const
	{
		D3DXMatrixRotationQuaternion( &outResult, this );
	}

	//--------------------------------------------------------------
	/** @brief	Euler각도로 사원수 변환
		@desc	convert euler angle(in radians) to quaternion
				D3D coordinate system (left-hand):

					  ^ y(yaw)
					  |
					  +--> z(roll)
					 /
					v x(pitch)

		@see	http://gpgstudy.com/forum/viewtopic.php?t=2718&start=0&postdays=0&postorder=asc&highlight=
	*/
	//--------------------------------------------------------------
	void Quaternion::FromEuler( const Math::Vector3& Euler )
	{
		FromEuler( Euler.x, Euler.y, Euler.z );
	}

	//--------------------------------------------------------------
	/** @brief	Euler 각으로 Quaternion을 변환한다.
		@desc	convert quaternion to euler(in radians)
				order = (roll(z)->pitch(x)->yaw(y)) in d3d coordinate system
				yaw(y-axis), pitch(x-axis), roll(z-axis)

					RotationMatrix = 
					Ry * Rx * Rz = 
					[ c2c3+s2s1s3  -c2s3+s2s1c3  s2c1]
					[        c1s3          c1c3   -s1]
					[-s2c3+c2s1s3   s2s3+c2s1c3  c2c1]

					where,
					c1 = cos(theta_x), c2 = cos(theta_y), c3 = cos(theta_z)
					s1 = sin(theta_x), s2 = sin(theta_y), s3 = sin(theta_z).

					quaternion matrix = 
					[1-2xx-2zz   2(xy-wz)   2(xz+wy)]
					[ 2(xy+wz)  1-2xx-2zz   2(yz-wx)]
					[ 2(xz-wy)   2(yz+wx)  1-2xx-2yy]

					x-axis-rotation = asin(s1) = asin(-2(yz-wx))
					y-axis-rotation = atan(s2c1, c2c1) = atan(2(xz+wy), 1-2xx-2yy) = atan(2(xz+wy), xx+yy+zz+ww-2xx-2yy)
   	   	   							   = atan(2(xz+wy), -xx-yy+zz+ww)
					z-axis-rotation = atan(c1s3, c1c3) = atan(2(xy+wz), 1-2xx-2zz) = atan(2(xy+wz), xx+yy+zz+ww-2xx-2zz)
   	   	   							   = atan(2(xy+wz), -xx+yy-zz+ww)

	   @see		http://gpgstudy.com/forum/viewtopic.php?t=2718&start=0&postdays=0&postorder=asc&highlight=
	*/
	//--------------------------------------------------------------
	void Quaternion::ToEuler( Math::Vector3& outEuler )
	{
		float sqx = this->x*this->x;
		float sqy = this->y*this->y;
		float sqz = this->z*this->z;
		float sqw = this->w*this->w;

		// Radian
		outEuler.x = Math::ASin(2.0f * (x*w - y*z));								// x축 회전	pitch
		outEuler.y = Math::ATan2(2.0f * (x*z + y*w), (-sqx - sqy + sqz + sqw) );	// y축 회전 yaw
		outEuler.z = Math::ATan2(2.0f * (x*y + z*w), (-sqx + sqy - sqz + sqw) );	// z축 회전 roll

		// Degree로...
		//outEuler.x = Math::ASin(2.0f * (x*w - y*z)) * Math::PI;								// x축 회전
		//outEuler.y = Math::ATan2(2.0f * (x*z + y*w), (-sqx - sqy + sqz + sqw) ) * Math::PI;	// y축 회전
		//outEuler.z = Math::ATan2(2.0f * (x*y + z*w), (-sqx + sqy - sqz + sqw) ) * Math::PI;	// z축 회전
	}
}
}