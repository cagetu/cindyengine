// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNVECTOR4_H__
#define __CNVECTOR4_H__

#include "../Cindy.h"

namespace Cindy
{
class CnColor;

namespace Math
{
	class Vector3;
	class Matrix44;

	//==================================================================
	/** Vector4
		@author				cagetu
		@since				2006년 9월 30일
		@brief
			4차원 vector. dx의 함수를 래핑한다.
	*/
	//==================================================================
	__declspec(align(16)) 
	class CN_DLL Vector4 : public D3DXVECTOR4 
	{
	public:
		static const Vector4 ZERO;
		static const Vector4 UNIT;
		static const Vector4 UNIT_X;
		static const Vector4 UNIT_Y;
		static const Vector4 UNIT_Z;
		static const Vector4 UNIT_W;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		// 생성자
		Vector4();
		Vector4( CONST FLOAT* f );
		Vector4( CONST D3DXVECTOR4& v );
		Vector4( CONST D3DXFLOAT16* f );
		Vector4( FLOAT x, FLOAT y, FLOAT z, FLOAT w );
		Vector4( const Vector4& v );

		Vector4& operator = ( const Vector4& v );
//		Vector4& operator = ( const Math::Vector3& vInput );

		// 비교 연산자
		bool operator == ( const Vector4& v ) const;
		bool operator != ( const Vector4& v ) const;

		float	Length() const;
		float	LengthSq() const;
		float	Dot( const Vector4& v ) const;
		void	Normalize();

		// Convert
		void	FromVec3( const Math::Vector3& v );
		void	FromColor( const CnColor& v );
		void	FromUbyte4n( const void* ptr, float w );

		// Multiply
		void	Multiply( const Math::Vector3& v, const Math::Matrix44& m );
		void	Multiply( const Vector4& v, const Math::Matrix44& m );

	private:
		int _Compare( const Vector4& v ) const;
	};

#include "CnVector4.inl"
}
}

#endif	// __CNVECTOR4_H__