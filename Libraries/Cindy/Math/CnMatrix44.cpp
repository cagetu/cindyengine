// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnMatrix44.h"

namespace Cindy
{
namespace Math
{
	//==================================================================
	//	Matrix44
	//==================================================================
	const Matrix44 Matrix44::ZERO(0.0f, 0.0f, 0.0f, 0.0f,
								  0.0f, 0.0f, 0.0f, 0.0f,
								  0.0f, 0.0f, 0.0f, 0.0f,
								  0.0f, 0.0f, 0.0f, 0.0f);

	const Matrix44 Matrix44::IDENTITY(1.0f, 0.0f, 0.0f, 0.0f,
									  0.0f, 1.0f, 0.0f, 0.0f,
									  0.0f, 0.0f, 1.0f, 0.0f,
									  0.0f, 0.0f, 0.0f, 1.0f);

	//--------------------------------------------------------------
	//	View Matrix..
	//--------------------------------------------------------------
	/*
		zaxis = normal(At - Eye)
		xaxis = normal(cross(Up, zaxis))
		yaxis = cross(zaxis, xaxis)
    
		 xaxis.x           yaxis.x           zaxis.x          0
		 xaxis.y           yaxis.y           zaxis.y          0
		 xaxis.z           yaxis.z           zaxis.z          0
		-dot(xaxis, eye)  -dot(yaxis, eye)  -dot(zaxis, eye)  1
	*/
	void Matrix44::LookAtLH( const Math::Vector3& vEye, const Math::Vector3& vLookAt, const Math::Vector3& vUp )
	{
		D3DXMatrixLookAtLH( this, &vEye, &vLookAt, &vUp );
	}
	void Matrix44::LookAtRH( const Math::Vector3& vEye, const Math::Vector3& vLookAt, const Math::Vector3& vUp )
	{
		D3DXMatrixLookAtRH( this, &vEye, &vLookAt, &vUp );
	}

	//==================================================================
	//	Projection Matrix..
	//==================================================================
	//--------------------------------------------------------------
	/**	@desc
			2*zn/w  0       0              0
			0       2*zn/h  0              0
			0       0       zf/(zf-zn)     1
			0       0       zn*zf/(zn-zf)  0
	*/
	//--------------------------------------------------------------
	void Matrix44::PerspLH( float fNearWidth, float fNearHeight, float fZNear, float fZFar )
	{
		D3DXMatrixPerspectiveLH( this, fNearWidth, fNearHeight, fZNear, fZFar );
	}

	//--------------------------------------------------------------
	/** @desc
			2*zn/w  0       0              0
			0       2*zn/h  0              0
			0       0       zf/(zn-zf)    -1
			0       0       zn*zf/(zn-zf)  0
	*/
	//--------------------------------------------------------------
	void Matrix44::PerspRH( float fNearWidth, float fNearHeight, float fZNear, float fZFar )
	{
		D3DXMatrixPerspectiveRH( this, fNearWidth, fNearHeight, fZNear, fZFar );
	}
	//--------------------------------------------------------------
	/**	@desc
			xScale     0          0               0
			0        yScale       0               0
			0          0       zf/(zf-zn)         1
			0          0       -zn*zf/(zf-zn)     0
			where:
			yScale = cot(fovY/2)
			xScale = aspect ratio / yScale
	*/
	//--------------------------------------------------------------
	void Matrix44::PerspFovLH( float fFov, float fAspect, float fNear, float fFar )
	{
		D3DXMatrixPerspectiveFovLH( this, fFov, fAspect, fNear, fFar );
	}

	//--------------------------------------------------------------
	/**	@desc
			xScale     0          0              0
			0        yScale       0              0
			0        0        zf/(zn-zf)        -1
			0        0        zn*zf/(zn-zf)      0
		where:
			yScale = cot(fovY/2)
			xScale = aspect ratio / yScale
	*/
	//--------------------------------------------------------------
	void Matrix44::PerspFovRH( float fFov, float fAspect, float fNear, float fFar )
	{
		D3DXMatrixPerspectiveFovRH( this, fFov, fAspect, fNear, fFar );
	}
	//--------------------------------------------------------------
	void Matrix44::perspOffCenterLH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ )
	{
		D3DXMatrixPerspectiveOffCenterLH( this, fMinX, fMaxX, fMinY, fMaxY, fMinZ, fMaxZ );
	}
	//--------------------------------------------------------------
	void Matrix44::perspOffCenterRH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ )
	{
		D3DXMatrixPerspectiveOffCenterRH( this, fMinX, fMaxX, fMinY, fMaxY, fMinZ, fMaxZ );
	}

	//--------------------------------------------------------------
	//	Orthogonal Matrix
	//--------------------------------------------------------------
	/*
			2/w  0    0           0
			0    2/h  0           0
			0    0    1/(zf-zn)   0
			0    0    zn/(zn-zf)  1
	*/
	void Matrix44::OrthoLH( float fWidth, float fHeight, float fZNear, float fZFar )
	{
		D3DXMatrixOrthoLH( this, fWidth, fHeight, fZNear, fZFar );
	}
	//--------------------------------------------------------------
	void Matrix44::OrthoRH( float fWidth, float fHeight, float fZNear, float fZFar )
	{
		D3DXMatrixOrthoRH( this, fWidth, fHeight, fZNear, fZFar );
	}
	//--------------------------------------------------------------
	/*
			2/(r-l)      0            0           0
			0            2/(t-b)      0           0
			0            0            1/(zf-zn)   0
			(l+r)/(l-r)  (t+b)/(b-t)  zn/(zn-zf)  l
		where:
			l = -w/2, 
			r = w/2, 
			b = -h/2, and 
			t = h/2.
	*/
	void Matrix44::OrthoOffCenterLH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ )
	{
		D3DXMatrixOrthoOffCenterLH( this, fMinX, fMaxX, fMinY, fMaxY, fMinZ, fMaxZ );
	}
	//--------------------------------------------------------------
	void Matrix44::OrthoOffCenterRH( float fMinX, float fMaxX, float fMinY, float fMaxY, float fMinZ, float fMaxZ )
	{
		D3DXMatrixOrthoOffCenterRH( this, fMinX, fMaxX, fMinY, fMaxY, fMinZ, fMaxZ );
	}

	//--------------------------------------------------------------
	void Matrix44::Decompose( Math::Vector3& outScale, Math::Quaternion& outRotation, Math::Vector3& outTranslation ) const
	{
		D3DXMatrixDecompose( &outScale, &outRotation, &outTranslation, this );
	}

	//--------------------------------------------------------------
}
}