// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNOBJECTORIENTEDBOX_H__
#define __CNOBJECTORIENTEDBOX_H__

#include "CnVector3.h"

namespace Cindy
{
namespace Math
{
	class Matrix44;

	//==================================================================
	/** OBB
		@author				cagetu
		@since				2006년 9월 31일
		@brief
			Object Oritented Bounding Box
	*/
	//==================================================================
	class CN_DLL OBB
	{
	public:
		OBB();
		OBB( const Math::Vector3& vCenter, const Math::Vector3* aAxis, const float* fExtent );
		OBB( const Math::Vector3& vCenter,
			 const Math::Vector3& vAxisX, const Math::Vector3& vAxisY, const Math::Vector3& vAxisZ,
			 float fExtentX, float fExtentY, float fExtentZ );

		// 구성 정보 설정
		void	SetCenter( const Math::Vector3& vCenter );
		const Math::Vector3&	GetCenter() const;

		void	SetAxis( const Math::Vector3* aAxis );
		const Math::Vector3*	GetAxis() const;

		void	SetExtent( const float* aExtent );
		const float*	GetExtents() const;

		void	ComputeVertices( Math::Vector3* pResult );
		void	Build( int iSize, const Math::Vector3* pVertices );

		void	Merge( OBB& Obb );

		void	Transform( OBB* pOut, Math::Matrix44* pTransform );

		// Intersect
		bool	Intersect( const Math::Vector3& rOrigin, const Math::Vector3& rDir, float& fResultDistance );
		bool	Intersect( const Math::Vector3& rOrigin, const Math::Vector3& rDir );
		bool	Intersect( const OBB& Obb );

	private:
		Math::Vector3	m_vMin;				//!< 최소 좌표
		Math::Vector3	m_vMax;				//!< 최대 좌표

		Math::Vector3	m_vCenter;			//!< 중심 좌표
		Math::Vector3	m_vAxis[3];			//!< 축
		float			m_fExtent[3];		//!< 축의 최대 길이의 1/2
	};

#include "CnObjectOrientedBox.inl"
}
}

#endif	//	__CNOBJECTORIENTEDBOX_H__