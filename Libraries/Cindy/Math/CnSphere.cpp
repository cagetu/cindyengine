// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnSphere.h"
#include "CnAxisAlignedBox.h"

namespace Cindy
{
namespace Math
{
	//--------------------------------------------------------------
	Sphere::Sphere()
		: radius(0.0f)
	{
	}
	Sphere::Sphere( const Sphere &sphere )
		: center(sphere.center)
		, radius(sphere.radius)
	{
	}
	Sphere::Sphere( const Math::Vector3* points, int size )
	{
		radius = 0.0f;

		center = points[0];

		Math::Vector3 v;
		for (int i=1; i<size; ++i)
		{
			v = points[i] - center;
			float d = v.Dot(v);
			if (d > radius*radius)
			{
				d = Math::Sqrt(d);
				float r = 0.5f * (d+radius);
				float scale = (r-radius) / d;
				center = center + scale*v;
				radius = r;
			}
		}
	}
	Sphere::Sphere( Math::AABB* aabb )
	{
		center = aabb->GetCenter();
		Math::Vector3 length = aabb->GetMax() - center;
		radius = length.Length();
	}

	//--------------------------------------------------------------
	bool Sphere::Intersect( const Sphere& sphere )
	{
		Math::Vector3 centerDiff = center - sphere.center;
		float radiusSum = radius + sphere.radius;
		return ( centerDiff.Dot(centerDiff) <= radiusSum*radiusSum );
	}

	//--------------------------------------------------------------
	/** Ray와 구의 교차
		하나의 구 줌심과 반직선 사이의 거리를 평가한다. 만약 거리가 그 구의 반지름과
		같거나 작다면, 그 때 반직선은 그 구와 교차한다.
	*/
	//--------------------------------------------------------------
	bool Sphere::Intersect( const Math::Vector3& rayOrig, const Math::Vector3& rayDir )
	{
		// 중간값 계산
		Math::Vector3 w = center - rayOrig;
		float wsq = w.Dot(w);
		float proj = w.Dot(rayDir);
		float rsq = radius*radius;

		// 만약 구가 반직선 뒤에 있다면, 교차가 발생하지 않음
		if (proj<0.0f && wsq>rsq)
			return false;

		float vsq = rayDir.Dot(rayDir);

		// 차이의 길이와 반지름 평가
		return (vsq*wsq - proj*proj <= vsq*rsq);
	}
}
}