// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNVECTOR3_H__
#define __CNVECTOR3_H__

#include "CnMathDefines.h"
#include "../Cindy.h"

namespace Cindy
{
namespace Math
{
	class Matrix44;
	class Quaternion;

	//==================================================================
	/** Vector3
		@author			cagetu
		@since			2006년 9월 30일
		@brief
			3차원 vector. dx의 함수를 래핑한다.
	*/
	//==================================================================
	class CN_DLL Vector3 : public D3DXVECTOR3 
	{
	public:
		static const Vector3 ZERO;		
		static const Vector3 UNIT;
		static const Vector3 UNIT_X;
		static const Vector3 UNIT_Y;
		static const Vector3 UNIT_Z;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		// 생성자
		Vector3();
		Vector3( CONST FLOAT* pVector );
		Vector3( CONST D3DXVECTOR3& rVector );
		Vector3( CONST D3DXFLOAT16* pVector );
		Vector3( FLOAT x, FLOAT y, FLOAT z );
		Vector3( const Vector3& rVector );

		Vector3& operator = ( const Vector3& vInput );

		// 비교 연산자
		bool operator == ( const Vector3& vInput ) const;
		bool operator != ( const Vector3& vInput ) const;

		// 곱
		//Vector3 operator *( const Vector3& rhs ) const;
		//friend Vector3 operator *( const float, const Vector3& rhs );

		// 
		void	Set( float x, float y, float z );

		// 노말
		void	Normalize();
		void	Normalize( Vector3* Output );

		// 길이
		float	Length() const;
		float	LengthSq() const;

		// 내적
		float	Dot( const Vector3& v ) const;
		Radian	GetAngleBetween( const Vector3& v ) const;

		// 외적
		void	Cross( const Vector3& v1, const Vector3& v2 );
		void	UnitCross( const Vector3& v1, const Vector3& v2 );

		// 변환
		void	Rotate( const Vector3& v, const Math::Quaternion& q );
		void	Scale( const Vector3& v, float fScale );

		// 보간
		void	Lerp( const Vector3& v1, const Vector3& v2, float delta ); 
	
		// Multiply
		void	Multiply( const Vector3& v );
		void	Multiply( const Vector3& v, const Math::Matrix44& m );
		void	MultiplyNormal( const Vector3& v, const Math::Matrix44& m );

		static void	MultipleArray( Vector3* output, unsigned int outStride,
								   const Vector3* input, unsigned int inputStride,
								   const Math::Matrix44* m, unsigned int size );
		static void	MultipleNormalArray( Vector3* output, unsigned int outStride,
										 const Vector3* input, unsigned int inputStride,
										 const Math::Matrix44* m, unsigned int size );

	private:
		int _Compare( const Vector3& rVector ) const;
	};

	#include "CnVector3.inl"
}
}

#endif	// __CNVECTOR3_H__