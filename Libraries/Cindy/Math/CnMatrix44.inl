// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
inline
Matrix44::Matrix44()
: D3DXMATRIXA16()
{
	Identity();
}

//--------------------------------------------------------------
inline
Matrix44::Matrix44( CONST FLOAT* pArray )
: D3DXMATRIXA16( pArray )
{
}

//--------------------------------------------------------------
inline
Matrix44::Matrix44( CONST D3DMATRIX& m )
: D3DXMATRIXA16( m )
{
}

//--------------------------------------------------------------
inline
Matrix44::Matrix44( const Matrix44& m )
: D3DXMATRIXA16( m._11, m._12, m._13, m._14, 
				 m._21, m._22, m._23, m._24, 
				 m._31, m._32, m._33, m._34, 
				 m._41, m._42, m._43, m._44 )
{
}

//--------------------------------------------------------------
inline
Matrix44::Matrix44(	FLOAT _11, FLOAT _12, FLOAT _13, FLOAT _14,
					FLOAT _21, FLOAT _22, FLOAT _23, FLOAT _24,
					FLOAT _31, FLOAT _32, FLOAT _33, FLOAT _34,
					FLOAT _41, FLOAT _42, FLOAT _43, FLOAT _44 )
: D3DXMATRIXA16( _11, _12, _13, _14,
				 _21, _22, _23, _24,
				 _31, _32, _33, _34,
				 _41, _42, _43, _44 )
{
}

//--------------------------------------------------------------
inline
void Matrix44::MakeZero()
{
	_11 = 0.0f;		_12 = 0.0f;		_13 = 0.0f;		_14 = 0.0f;
	_21 = 0.0f;		_22 = 0.0f;		_23 = 0.0f;		_24 = 0.0f;
	_31 = 0.0f;		_32 = 0.0f;		_33 = 0.0f;		_34 = 0.0f;
	_41 = 0.0f;		_42 = 0.0f;		_43 = 0.0f;		_44 = 0.0f;
}

//--------------------------------------------------------------
inline
void Matrix44::Identity()
{
	D3DXMatrixIdentity( this );
}

//--------------------------------------------------------------
inline
void Matrix44::SetRow( int iRow, const Math::Vector4& rValue )
{
	assert( 0 <= iRow && iRow < 4 );

	m[iRow][0] = rValue.x;
	m[iRow][1] = rValue.y;
	m[iRow][2] = rValue.z;
	m[iRow][3] = rValue.w;
}

//--------------------------------------------------------------
inline
void Matrix44::GetRow( int iRow, Math::Vector4& rResult )
{
	assert( 0 <= iRow && iRow < 4 );

	rResult.x = m[iRow][0];
	rResult.y = m[iRow][1];
	rResult.z = m[iRow][2];
	rResult.w = m[iRow][3];
}

//--------------------------------------------------------------
inline
void Matrix44::SetColumn( int iCol, const Math::Vector4& rValue )
{
	assert( 0 <= iCol && iCol < 4 );

	m[0][iCol] = rValue.x;
	m[1][iCol] = rValue.y;
	m[2][iCol] = rValue.z;
	m[3][iCol] = rValue.w;
}

//--------------------------------------------------------------
inline
void Matrix44::GetColumn( int iCol, Math::Vector4& rResult )
{
	assert( 0 <= iCol && iCol < 4 );

	rResult.x = m[0][iCol];
	rResult.y = m[1][iCol];
	rResult.z = m[2][iCol];
	rResult.w = m[3][iCol];
}
inline
void Matrix44::GetColumn( int iCol, Math::Vector3& rResult )
{
	assert( 0 <= iCol && iCol < 3 );

	rResult.x = m[0][iCol];
	rResult.y = m[1][iCol];
	rResult.z = m[2][iCol];
}

//--------------------------------------------------------------
inline
void Matrix44::SetPosition( const Math::Vector3& vPos )
{
	m[3][0] = vPos.x;
	m[3][1] = vPos.y;
	m[3][2] = vPos.z;
}

//--------------------------------------------------------------
inline
void Matrix44::GetPosition( Math::Vector3& rResult ) const
{
	rResult.x = _41;
	rResult.y = _42;
	rResult.z = _43;
}

//--------------------------------------------------------------
inline
void Matrix44::SetScale( const Math::Vector3& vScale )
{
	m[0][0] = vScale.x;
	m[1][1] = vScale.y;
	m[2][2] = vScale.z;
}

//--------------------------------------------------------------
inline
void Matrix44::GetScale( Math::Vector3& rResult ) const
{
	rResult.x = _11;
	rResult.y = _22;
	rResult.z = _33;
}

//--------------------------------------------------------------
inline
void Matrix44::SetAxis( const Math::Vector3* pAxis )
{
	assert( pAxis );

	m[0][0] = pAxis[0].x;
	m[0][1] = pAxis[0].y;
	m[0][2] = pAxis[0].z;
		  
	m[1][0] = pAxis[1].x;
	m[1][1] = pAxis[1].y;
	m[1][2] = pAxis[1].z;
		  
	m[2][0] = pAxis[2].x;
	m[2][1] = pAxis[2].y;
	m[2][2] = pAxis[2].z;
}

//--------------------------------------------------------------
inline
void Matrix44::GetAxis( Math::Vector3* pResult ) const
{
	assert( pResult );

	pResult[0].x = _11;
	pResult[0].y = _12;
	pResult[0].z = _13;

	pResult[1].x = _21;
	pResult[1].y = _22;
	pResult[1].z = _23;

	pResult[2].x = _31;
	pResult[2].y = _32;
	pResult[2].z = _33;
}

//--------------------------------------------------------------
inline
void Matrix44::Inverse( Matrix44& rResult ) const
{
	D3DXMatrixInverse( &rResult, NULL, this );
}

//--------------------------------------------------------------
inline
void Matrix44::Transpose( Matrix44& rResult ) const
{
	D3DXMatrixTranspose( &rResult, this );
}

//--------------------------------------------------------------
inline
void Matrix44::SetRotation( const Math::Quaternion& rRot )
{
	D3DXMatrixRotationQuaternion( this, &rRot );	
}

//--------------------------------------------------------------
inline
void Matrix44::GetRotation( Math::Quaternion& rResult ) const
{
	D3DXQuaternionRotationMatrix( &rResult, this );
}

////--------------------------------------------------------------
//inline
//void Matrix44::FromRotation( const Math::Quaternion& rRot )
//{
//	D3DXMatrixRotationQuaternion( this, &rRot );	
//}
//
////--------------------------------------------------------------
//inline
//void Matrix44::ToRotation( Math::Quaternion& rResult ) const
//{
//	D3DXQuaternionRotationMatrix( &rResult, this );
//}

//--------------------------------------------------------------
inline
void Matrix44::Translation( float x, float y, float z )
{
	D3DXMatrixTranslation( this, x, y, z );
}

//--------------------------------------------------------------
inline
void Matrix44::Scaling( float x, float y, float z )
{
	D3DXMatrixScaling( this, x, y, z );
}

//--------------------------------------------------------------
inline
void Matrix44::Build( const Math::Vector3& vPos, const Math::Quaternion& qRot, const Math::Vector3& vScale )
{
	// 행렬 생성 순서 :  회전 -> 크기 변환 -> 이동
	Matrix44 s, r;

	Identity();

	s.SetScale( vScale );
	r.SetRotation( qRot );

	Multiply( r, s );

	SetPosition( vPos );	
}

//--------------------------------------------------------------
inline
void Matrix44::Multiply( const Matrix44& m1, const Matrix44& m2 )
{
	D3DXMatrixMultiply( this, &m1, &m2 );
}
