// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnVector3.h"
#include "CnQuaternion.h"
#include "CnMatrix44.h"

namespace Cindy
{
namespace Math
{
	//==================================================================
	//	Vector3
	//==================================================================
	const Vector3 Vector3::ZERO( 0.0f, 0.0f, 0.0f );
	const Vector3 Vector3::UNIT( 1.0f, 1.0f, 1.0f );
	const Vector3 Vector3::UNIT_X( 1.0f, 0.0f, 0.0f );
	const Vector3 Vector3::UNIT_Y( 0.0f, 1.0f, 0.0f );
	const Vector3 Vector3::UNIT_Z( 0.0f, 0.0f, 1.0f );

	//-------------------------------------------------------------------------
	/**	@desc	newPosition = quaternion * currentPosition * quaternionInverse
				전개된 식을 계산한다.
	*/
	//-------------------------------------------------------------------------
	void Vector3::Rotate( const Vector3& v, const Math::Quaternion& q )
	{
		Vector3 uv, uuv;
		Vector3 qvec( q.x, q.y, q.z );

		uv.Cross( qvec, v );
		uuv.Cross( qvec, uv );

		uv *= (2.0f * q.w);
		uuv *= 2.0f;

		*this = v + uv;
		*this += uuv;
	}	

	//-------------------------------------------------------------------------
	/**	@desc	두 벡터 사이의 각도를 구한다.
				dot(a,b) = |A|*|B|*cosTheta
	*/
	//-------------------------------------------------------------------------
	Radian Vector3::GetAngleBetween( const Vector3& v ) const
	{
		float dot = this->Dot( v );
		float length = Math::Abs(v.Length()) + Math::Abs(this->Length());
		float angle = Math::ACos( dot/length );

		if (_isnan(angle))
			return 0.0f;

		return angle;
	}

	//-------------------------------------------------------------------------
	/**	@desc	vector3 -> vector4(v.x, v.y, v.z, 1.0f) * m	*/
	//-------------------------------------------------------------------------
	void Vector3::Multiply( const Vector3& v, const Math::Matrix44& m )
	{
		D3DXVec3TransformCoord( this, &v, &m );
	}

	//-------------------------------------------------------------------------
	/** vector3 -> vector4(v.x, v.y, v.z, 0.0f) * m
		@desc	This function transforms the vector normal (x, y, z, 0) of the vector, pV, by the matrix, pM.
				If you want to transform a normal, the matrix you pass to this function should be 
				the transpose of the inverse of the matrix you would use to transform a point
	*/
	//-------------------------------------------------------------------------
	void Vector3::MultiplyNormal( const Vector3& v, const Math::Matrix44& m )
	{
		D3DXVec3TransformNormal( this, &v, &m );
	}

	//-------------------------------------------------------------------------
	//	Transform Array
	//-------------------------------------------------------------------------
	void Vector3::MultipleArray( Vector3* output, unsigned int outStride,
								 const Vector3* input, unsigned int inputStride,
								 const Math::Matrix44* m, unsigned int size )
	{
		D3DXVec3TransformCoordArray( output, outStride, input, inputStride, m, size );
	}
	//-------------------------------------------------------------------------
	void Vector3::MultipleNormalArray( Vector3* output, unsigned int outStride,
									   const Vector3* input, unsigned int inputStride,
									   const Math::Matrix44* m, unsigned int size )
	{
		D3DXVec3TransformNormalArray( output, outStride, input, inputStride, m, size );
	}
}
}