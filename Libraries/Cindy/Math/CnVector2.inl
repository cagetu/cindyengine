//================================================================
// File:               : CnVector2.inl
// Related Header File : CnVector2.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 12
//================================================================
inline
Vector2::Vector2()
{
	x = 0.0f;
	y = 0.0f;
}

//--------------------------------------------------------------
inline
Vector2::Vector2( CONST FLOAT* pVector )
: D3DXVECTOR2( pVector )
{
}

//--------------------------------------------------------------
inline
Vector2::Vector2( CONST D3DXFLOAT16* pVector )
: D3DXVECTOR2( pVector )
{
}

//--------------------------------------------------------------
inline
Vector2::Vector2( FLOAT x, FLOAT y )
: D3DXVECTOR2( x, y )
{
}

//--------------------------------------------------------------
inline
Vector2::Vector2( CONST D3DXVECTOR2& rVector )
: D3DXVECTOR2( rVector )
{
}

//--------------------------------------------------------------
inline
Vector2::Vector2( const Vector2& rVector )
{
	x = rVector.x;
	y = rVector.y;
}

//--------------------------------------------------------------
inline
Vector2& Vector2::operator = ( const Vector2& vInput )
{
	x = vInput.x;
	y = vInput.y;

	return *this;
}

//--------------------------------------------------------------
inline
int Vector2::_Compare( const Vector2& rVector ) const
{
	return memcmp( this, &rVector, sizeof(Vector2) );
}

//--------------------------------------------------------------
inline
bool Vector2::operator == ( const Vector2& vInput ) const
{
//	return ( ( x == vInput.x ) && ( y == vInput.y ) && ( z == vInput.z ) );
	return _Compare( vInput ) == 0;
}

//--------------------------------------------------------------
inline
bool Vector2::operator != ( const Vector2& vInput ) const
{
//	return ( ( x != vInput.x ) || ( y != vInput.y ) || ( z != vInput.z ) );
	return _Compare( vInput ) != 0;
}

//--------------------------------------------------------------
inline
float Vector2::Length() const
{
	return D3DXVec2Length( this );
}

//--------------------------------------------------------------
inline
float Vector2::LengthSq() const
{
	return D3DXVec2LengthSq( this );
}

//--------------------------------------------------------------
inline
void Vector2::Normalize()
{
	D3DXVec2Normalize( this, this );
}

//--------------------------------------------------------------
inline
float Vector2::Dot( const Vector2& rVector ) const
{
	return D3DXVec2Dot( this, &rVector );
}