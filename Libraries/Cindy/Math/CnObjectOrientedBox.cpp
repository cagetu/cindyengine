// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnObjectOrientedBox.h"
#include "CnMathDefines.h"
#include "CnMatrix44.h"

namespace Cindy
{
namespace Math
{
	//////////////////////////////////////////////////////////////////////
	// OBB박스 만들기 
	//////////////////////////////////////////////////////////////////////
	//--------------------------------------------------------------
	void Rotate( float a[3][3], float s, float tau, int i, int j, int k, int l )
	{
		float h, g;
		g = a[i][j];
		h = a[k][l];
		a[i][j] = g - s * ( h + g *tau );
		a[k][l] = h + s * ( g - h *tau );
	}

	//--------------------------------------------------------------
	bool Jacobi( float a[3][3], float v[3][3], float d[3] )
	{
		int n = 3;
		int i, j, iq, ip;
		float tresh, theta, tau, t, sm, s, h, g, c, b[3], z[3];

		for( ip = 0; ip < n; ip++ ){
			for( iq = 0; iq < n; iq++ ) v[ip][iq] = 0.0f;
			v[ip][ip] = 1.0f;
		}
		for( ip = 0; ip < n; ip++ ){
			b[ip] = d[ip] = a[ip][ip];
			z[ip] = 0.0f;
		}
		for( i = 0; i < 50; i++ ){
			sm = 0.0f;
			for( ip = 0; ip < n - 1; ip++ ){
				for( iq = ip + 1; iq < n; iq++ ) sm += (float)fabs(a[ip][iq]);
			}

			if( sm == 0.0f ) return true;
			if( i < 3 ) tresh = 0.2f * sm / ( n * n );
			else tresh = 0.0f;
			for( ip = 0; ip < n - 1; ip++ ){
				for( iq = ip + 1; iq < n; iq++ ){
					g = 100.0f * (float)fabs( a[ip][iq] );
					if( i > 3 && ( fabs( d[ip] ) + g ) == fabs( d[ip] )
						&& ( fabs( d[iq] ) + g ) == fabs( d[iq] ) ) a[ip][iq] = 0.0f;
					else if( fabs( a[ip][iq] ) > tresh ){
						h = d[iq] - d[ip];
						if( ( fabs( h ) + g ) == fabs( h ) ) t = a[ip][iq] / h;
						else{
							theta = 0.5f * h / a[ip][iq];
							t = 1.0f / ( (float)fabs( theta ) + (float)sqrt( 1.0f + theta * theta ) );
							if( theta < 0.0f ) t = -t;
						}
						c = 1.0f / (float)sqrt( 1 + t * t );
						s = t * c;
						tau = s / ( 1.0f + c );
						h = t * a[ip][iq];
						z[ip] -= h;
						z[iq] += h;
						d[ip] -= h;
						d[iq] += h;
						a[ip][iq] = 0.0f;

						for( j = 0; j < ip; j++ ) Rotate( a, s, tau, j, ip, j, iq );
						for( j = ip + 1; j < iq; j++ ) Rotate( a, s, tau, ip, j, j, iq );
						for( j = iq + 1; j < n; j++ ) Rotate( a, s, tau, ip, j, iq, j );
						for( j = 0; j < n; j++ ) Rotate( v, s, tau, j, ip, j, iq );
					}
				}
			}
			for( ip = 0; ip < n; ip++ ){
				b[ip] += z[ip];
				d[ip] = b[ip];
				z[ip] = 0.0f;
			}
		}

		return false;
	}

	//--------------------------------------------------------------
	//	Class OBB
	//--------------------------------------------------------------
	OBB::OBB()
	{
	}

	//--------------------------------------------------------------
	OBB::OBB( const Math::Vector3& vCenter, const Math::Vector3* aAxis, const float* fExtent )
	{
		m_vCenter = vCenter;

		for( int i = 0; i < 3; ++i )
		{
			m_vAxis[i] = aAxis[i];
			m_fExtent[i] = fExtent[i];
		}
	}

	//--------------------------------------------------------------
	OBB::OBB( const Math::Vector3& vCenter, const Math::Vector3& vAxisX, const Math::Vector3& vAxisY, const Math::Vector3& vAxisZ,
				  float fExtentX, float fExtentY, float fExtentZ )
	{
		m_vCenter = vCenter;

		m_vAxis[0] = vAxisX;
		m_vAxis[1] = vAxisY;
		m_vAxis[2] = vAxisZ;

		m_fExtent[0] = fExtentX;
		m_fExtent[1] = fExtentY;
		m_fExtent[2] = fExtentZ;
	}

	//--------------------------------------------------------------
	void OBB::SetAxis( const Math::Vector3* aAxis )
	{
		for( ushort i = 0; i < 3; ++i )
		{
			m_vAxis[i] = aAxis[i];
		}
	}

	//--------------------------------------------------------------
	void OBB::SetExtent( const float* aExtent )
	{
		for( ushort i = 0; i < 3; ++i )
		{
			m_fExtent[i] = aExtent[i];
		}
	}

	//--------------------------------------------------------------
	/** Obb 박스의 8개의 버텍스를 만든다.
		@param pResult		:	Obb의 8개 버텍스
	*/
	//--------------------------------------------------------------
	void OBB::ComputeVertices( Math::Vector3* pResult )
	{
		Math::Vector3 aEAxis[3] =
		{
			m_fExtent[0]*m_vAxis[0],
			m_fExtent[1]*m_vAxis[1],
			m_fExtent[2]*m_vAxis[2]
		};

		pResult[0] = m_vCenter - aEAxis[0] - aEAxis[1] - aEAxis[2];
		pResult[1] = m_vCenter + aEAxis[0] - aEAxis[1] - aEAxis[2];
		pResult[2] = m_vCenter + aEAxis[0] + aEAxis[1] - aEAxis[2];
		pResult[3] = m_vCenter - aEAxis[0] + aEAxis[1] - aEAxis[2];
		pResult[4] = m_vCenter - aEAxis[0] - aEAxis[1] + aEAxis[2];
		pResult[5] = m_vCenter + aEAxis[0] - aEAxis[1] + aEAxis[2];
		pResult[6] = m_vCenter + aEAxis[0] + aEAxis[1] + aEAxis[2];
		pResult[7] = m_vCenter - aEAxis[0] + aEAxis[1] + aEAxis[2];
	}

	//--------------------------------------------------------------
	/** Obb 생성
		@param iSize		:	버텍스 개수
		@param pVertices	:	Obb 만들 버텍스
	*/
	//--------------------------------------------------------------
	void OBB::Build( int iSize, const Math::Vector3* pVertices )
	{
		// compute mean of points
		Math::Vector3 m( 0, 0, 0 );
		for( int i = 0; i < iSize; ++i )
		{
			m += pVertices[i];
		}

		m /= (float)iSize;

		// compute covariances of points
		float C11 = 0, C22 = 0, C33 = 0, C12 = 0, C13 = 0,  C23 = 0;
		for( int i = 0; i < iSize; ++i )
		{
			C11 += ( pVertices[i].x - m.x ) * ( pVertices[i].x - m.x );
			C22 += ( pVertices[i].y - m.y ) * ( pVertices[i].y - m.y );
			C33 += ( pVertices[i].z - m.z ) * ( pVertices[i].z - m.z );
			C12 += ( pVertices[i].x - m.x ) * ( pVertices[i].y - m.y );
			C13 += ( pVertices[i].x - m.x ) * ( pVertices[i].z - m.z );
			C23 += ( pVertices[i].y - m.y ) * ( pVertices[i].z - m.z );
		}

		C11 /= iSize;
		C22 /= iSize;
		C33 /= iSize;
		C12 /= iSize;
		C13 /= iSize;
		C23 /= iSize;

		// compute eigenvectors for covariance matrix
		float Matrix[3][3] = 
		{
			{ C11, C12, C13 },
			{ C12, C22, C23 },
			{ C13, C23, C33 },
		};

		float EigenVectors[3][3];
		float EigenValue[3];
		Jacobi( Matrix, EigenVectors, EigenValue );

		struct SORT
		{
			int ID;
			float Value;
		} Sort[3] = { { 0, EigenValue[0] }, { 1, EigenValue[1] }, { 2, EigenValue[2] } };

		// 축 구하기 
		for( int i = 0; i < 3; ++i )
		{
			m_vAxis[i].x = EigenVectors[0][Sort[i].ID];
			m_vAxis[i].y = EigenVectors[1][Sort[i].ID];
			m_vAxis[i].z = EigenVectors[2][Sort[i].ID];
		}

		float tmp_min[3] = {  FLT_MAX,  FLT_MAX,  FLT_MAX };
		float tmp_max[3] = { -FLT_MAX, -FLT_MAX, -FLT_MAX };

		// OBB 방향 벡터에 최소 볼록 집합들을 투영한 후 그 방향으로 최대, 최소값을 구한다.
		for( int j = 0; j < 3; ++j )
		{
			for( int i = 0; i < iSize; ++i )
			{
				float a = D3DXVec3Dot( &pVertices[i], &m_vAxis[j] );
				if( tmp_min[j] > a ) 
					tmp_min[j] = a;
				if( tmp_max[j] < a ) 
					tmp_max[j] = a;
			}
		}

		// 중심 구하기 
		m_vCenter = ( ( ( tmp_min[0] + tmp_max[0] ) * 0.5f ) * m_vAxis[0] ) +
					( ( ( tmp_min[1] + tmp_max[1] ) * 0.5f ) * m_vAxis[1] ) +
					( ( ( tmp_min[2] + tmp_max[2] ) * 0.5f ) * m_vAxis[2] );

		// 길이 구하기
		for( int i = 0; i < 3; ++i ) 
			m_fExtent[i] = 0.5f * ( tmp_max[i] - tmp_min[i] );
	}

	//--------------------------------------------------------------
	/** Obb 박스와 Obb 박스 합치기
		@param OBB	: OBB 박스
	*/
	//--------------------------------------------------------------
	void OBB::Merge( OBB& Obb )
	{
		Math::Vector3 akVertex[8], akVertex0[8];

		ComputeVertices( akVertex );
		Obb.ComputeVertices( akVertex0 );

		m_vCenter = 0.5f * ( m_vCenter + Obb.m_vCenter );

		Math::Matrix44 M, M0;
		M.SetAxis( m_vAxis );
		M0.SetAxis( Obb.m_vAxis );

		Math::Quaternion Q, Q0;
		D3DXQuaternionRotationMatrix( &Q, &M );
		D3DXQuaternionRotationMatrix( &Q0, &M0 );

		float QDot = D3DXQuaternionDot( &Q, &Q0 );
		if(QDot < 0.0f)
			Q0 = -Q0;

		Math::Quaternion qResult = Q + Q0;
		float Q0Dot = D3DXQuaternionDot( &qResult, &qResult );

		float InvLength = (float)( 1/sqrt(Q0Dot) );
		qResult = InvLength * qResult;
		Math::Matrix44 mResult;
		mResult.SetRotation( qResult );
		mResult.GetAxis( m_vAxis );

		int i, j;
		float fDot;
		float kMin[3], kMax[3];
		Math::Vector3 kDiff;

		for (i = 0; i < 3; i++)
		{
			kMin[i] = 0.0f;
			kMax[i] = 0.0f;
		}

		for (i = 0; i < 8; i++)
		{
			kDiff = akVertex[i] - m_vCenter;
			for (j = 0; j < 3; j++)
			{
				fDot = D3DXVec3Dot( &kDiff, &m_vAxis[j] );
				if ( fDot > kMax[j] )
					kMax[j] = fDot;
				else if ( fDot < kMin[j] )
					kMin[j] = fDot;
			}
		}

		for (i = 0; i < 8; i++)
		{
			kDiff = akVertex0[i] - m_vCenter;
			for (j = 0; j < 3; j++)
			{
				fDot = D3DXVec3Dot( &kDiff, &m_vAxis[j]);
				if ( fDot > kMax[j] )
					kMax[j] = fDot;
				else if ( fDot < kMin[j] )
					kMin[j] = fDot;
			}
		}

		for (j = 0; j < 3; j++)
		{
			m_vCenter		+= ( 0.5f * ( kMax[j] + kMin[j] ) ) * m_vAxis[j];
			m_fExtent[j]	=  ( 0.5f * ( kMax[j] - kMin[j] ) );
		}
	}

	//--------------------------------------------------------------
	/** 변환 행렬에 따른 OBB 변환 값을 반환한다.
		@param pOut			:	결과
		@param Math::Matrix44		:	변환 행렬
	*/
	//--------------------------------------------------------------
	void OBB::Transform( OBB* pOut, Math::Matrix44* pTransform )
	{
		Math::Matrix44 rotMat( *pTransform );
		rotMat._14 = 0.0f;
		rotMat._24 = 0.0f;
		rotMat._34 = 0.0f;
		rotMat._41 = 0.0f;
		rotMat._42 = 0.0f;
		rotMat._43 = 0.0f;

		Math::Vector3 translate;
		pTransform->GetPosition( translate );

		Math::Vector3 center;
		D3DXVec3TransformCoord( &center, &m_vCenter, &rotMat );
		center += translate;

		Math::Vector3 axis[3];
		float extent[3];

		for (int i = 0; i < 3; i++)
		{
			//D3DXVec3TransformCoord( &axis[i], &m_vAxis[i], &rotMat );
			//D3DXVec3Normalize( &axis[i], &axis[i] );			

			D3DXVec3TransformCoord( &axis[i], &m_vAxis[i], &rotMat );			
			extent[i] = m_fExtent[i] * D3DXVec3Length( &axis[i] );
			D3DXVec3Normalize( &axis[i], &axis[i] );			
		}

		pOut->SetCenter( center );
		pOut->SetAxis( axis );
		pOut->SetExtent( extent );
	}

	//--------------------------------------------------------------
	/** Obb 박스와 Ray 충돌
		@param rOrigin			:	Ray 원점(시작) 위치
		@param rDir				:	Ray 방향
		@param fResultDistance	:	거리
	*/
	//--------------------------------------------------------------
	bool OBB::Intersect( const Math::Vector3& rOrigin, const Math::Vector3& rDir, float& fResultDistance )
	{
		float t_min = -99999.0f;
		float t_max = 99999.0f;

		fResultDistance = 0.0f;
		Math::Vector3 p = m_vCenter - rOrigin;

		float e, f, h, t1, t2;
		for( int i = 0; i < 3; ++i )
		{
			e = m_vAxis[i].Dot( p );
			f = m_vAxis[i].Dot( rDir );

//			h = m_fExtent[i]*0.5f;
			h = m_fExtent[i];

			if( fabs(f) > Math::EPSILON )
			{
				t1 = ( e + h ) / f;
				t2 = ( e - h ) / f;

				if( t1 > t2 )		std::swap( t1, t2 );
				if( t1 > t_min )	t_min = t1;
				if( t2 < t_max )	t_max = t2;

				if( t_min > t_max )
				{
					return false;
				}

				if( t_max < 0.0f )
				{
					return false;
				}
			}
			else if( ( -e - h > 0 ) || ( -e + h < 0 ) )
			{
				return false;				
			}
		}

		if( t_min > 0.0f )
		{
			fResultDistance = t_min;
			return true;
		}
		else
		{
			fResultDistance = t_max;
			return true;
		}

		fResultDistance = 0.0f;
		return false;
	}

	//--------------------------------------------------------------
	/** Obb 박스와 Ray 충돌
		@param rOrigin			:	Ray 원점(시작) 위치
		@param rDir				:	Ray 방향
	*/
	//--------------------------------------------------------------
	bool OBB::Intersect( const Math::Vector3& rOrigin, const Math::Vector3& rDir )
	{
		float fWdU[3], fAWdU[3], fDdU[3], fADdU[3], fAWxDdU[3], fRhs;

		Math::Vector3 kDiff = rOrigin - m_vCenter;

		for( int i = 0; i < 3; ++i )
		{
			fWdU[i]		= rDir.Dot( m_vAxis[i] );
			fDdU[i]		= kDiff.Dot( m_vAxis[i] );
			fAWdU[i]	= fabs(fWdU[i]);
			fADdU[i]	= fabs(fDdU[i]);

			if ( fADdU[i] > m_fExtent[i] && fDdU[i]*fWdU[i] >= 0.0f )
				return false;
		}

		//
		D3DXVECTOR3 kWxD;
		D3DXVec3Cross( &kWxD, &rDir, &kDiff );

		fAWxDdU[0]	= fabs( D3DXVec3Dot( &kWxD, &m_vAxis[0]) );
		fRhs = m_fExtent[1]*fAWdU[2] + m_fExtent[2]*fAWdU[1];
		if ( fAWxDdU[0] > fRhs )
			return false;

		fAWxDdU[1] = fabs( D3DXVec3Dot( &kWxD, &m_vAxis[1]) );
		fRhs = m_fExtent[0]*fAWdU[2] + m_fExtent[2]*fAWdU[0];
		if ( fAWxDdU[1] > fRhs )
			return false;

		fAWxDdU[2]	= fabs(D3DXVec3Dot( &kWxD, &m_vAxis[2]) );
		fRhs		= m_fExtent[0]*fAWdU[1] + m_fExtent[1]*fAWdU[0];
		if ( fAWxDdU[2] > fRhs )
			return false;

		return true;
	}

	//--------------------------------------------------------------
	/** Obb 박스와 Obb 박스 충돌
		@param OBB				: OBB 박스
	*/
	//--------------------------------------------------------------
	bool OBB::Intersect( const OBB& Obb )
	{
		const D3DXVECTOR3* akA = GetAxis();
		const D3DXVECTOR3* akB = Obb.GetAxis();

		const float* afEA = GetExtents();
		const float* afEB = Obb.GetExtents();

		// compute difference of box centers, D = C1-C0
		D3DXVECTOR3 kD = GetCenter() - Obb.GetCenter();

		float aafC[3][3];     // matrix C = A^T B, c_{ij} = Dot(A_i,B_j)
		float aafAbsC[3][3];  // |c_{ij}|
		float afAD[3];        // Dot(A_i,D)
		float fOBB, fR1, fR;   // interval radii and distance between centers
		float fOBB1;           // = OBB + R1


		// axis C0+t*A0	
		aafC[0][0] = D3DXVec3Dot(&akA[0],&akB[0]);
		aafC[0][1] = D3DXVec3Dot(&akA[0],&akB[1]);
		aafC[0][2] = D3DXVec3Dot(&akA[0],&akB[2]);

		afAD[0] = D3DXVec3Dot(&akA[0],&kD);

		aafAbsC[0][0] = (float)fabs(aafC[0][0]);
		aafAbsC[0][1] = (float)fabs(aafC[0][1]);
		aafAbsC[0][2] = (float)fabs(aafC[0][2]);
		fR			  = (float)fabs(afAD[0]);
		fR1 = afEB[0]*aafAbsC[0][0]+afEB[1]*aafAbsC[0][1]+afEB[2]*aafAbsC[0][2];
		fOBB1 = afEA[0] + fR1;
		if ( fR > fOBB1 )
			return false;


		// axis C0+t*A1
		aafC[1][0] = D3DXVec3Dot(&akA[1],&akB[0]);
		aafC[1][1] = D3DXVec3Dot(&akA[1],&akB[1]);
		aafC[1][2] = D3DXVec3Dot(&akA[1],&akB[2]);
		afAD[1]    = D3DXVec3Dot(&akA[1],&kD);

		aafAbsC[1][0] = (float)fabs(aafC[1][0]);
		aafAbsC[1][1] = (float)fabs(aafC[1][1]);
		aafAbsC[1][2] = (float)fabs(aafC[1][2]);
		fR			  = (float)fabs(afAD[1]);

		fR1 = afEB[0]*aafAbsC[1][0]+afEB[1]*aafAbsC[1][1]+afEB[2]*aafAbsC[1][2];
		fOBB1 = afEA[1] + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A2
		aafC[2][0] = D3DXVec3Dot(&akA[2],&akB[0]);
		aafC[2][1] = D3DXVec3Dot(&akA[2],&akB[1]);
		aafC[2][2] = D3DXVec3Dot(&akA[2],&akB[2]);
		afAD[2]    = D3DXVec3Dot(&akA[2],&kD);

		aafAbsC[2][0] = (float)fabs(aafC[2][0]);
		aafAbsC[2][1] = (float)fabs(aafC[2][1]);
		aafAbsC[2][2] = (float)fabs(aafC[2][2]);
		fR			  = (float)fabs(afAD[2]);

		fR1 = afEB[0]*aafAbsC[2][0]+afEB[1]*aafAbsC[2][1]+afEB[2]*aafAbsC[2][2];
		fOBB1 = afEA[2] + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*B0
		fR = (float)fabs(D3DXVec3Dot(&akB[0],&kD));
		fOBB = afEA[0]*aafAbsC[0][0]+afEA[1]*aafAbsC[1][0]+afEA[2]*aafAbsC[2][0];
		fOBB1 = fOBB + afEB[0];
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*B1
		fR = (float)fabs(D3DXVec3Dot(&akB[1],&kD));
		fOBB = afEA[0]*aafAbsC[0][1]+afEA[1]*aafAbsC[1][1]+afEA[2]*aafAbsC[2][1];
		fOBB1 = fOBB + afEB[1];
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*B2
		fR = (float)fabs(D3DXVec3Dot(&akB[2],&kD));
		fOBB = afEA[0]*aafAbsC[0][2]+afEA[1]*aafAbsC[1][2]+afEA[2]*aafAbsC[2][2];
		fOBB1 = fOBB + afEB[2];
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A0xB0
		fR = (float)fabs(afAD[2]*aafC[1][0]-afAD[1]*aafC[2][0]);
		fOBB = afEA[1]*aafAbsC[2][0] + afEA[2]*aafAbsC[1][0];
		fR1 = afEB[1]*aafAbsC[0][2] + afEB[2]*aafAbsC[0][1];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A0xB1
		fR = (float)fabs(afAD[2]*aafC[1][1]-afAD[1]*aafC[2][1]);
		fOBB = afEA[1]*aafAbsC[2][1] + afEA[2]*aafAbsC[1][1];
		fR1 = afEB[0]*aafAbsC[0][2] + afEB[2]*aafAbsC[0][0];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A0xB2
		fR = (float)fabs(afAD[2]*aafC[1][2]-afAD[1]*aafC[2][2]);
		fOBB = afEA[1]*aafAbsC[2][2] + afEA[2]*aafAbsC[1][2];
		fR1 = afEB[0]*aafAbsC[0][1] + afEB[1]*aafAbsC[0][0];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A1xB0
		fR = (float)fabs(afAD[0]*aafC[2][0]-afAD[2]*aafC[0][0]);
		fOBB = afEA[0]*aafAbsC[2][0] + afEA[2]*aafAbsC[0][0];
		fR1 = afEB[1]*aafAbsC[1][2] + afEB[2]*aafAbsC[1][1];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A1xB1
		fR = (float)fabs(afAD[0]*aafC[2][1]-afAD[2]*aafC[0][1]);
		fOBB = afEA[0]*aafAbsC[2][1] + afEA[2]*aafAbsC[0][1];
		fR1 = afEB[0]*aafAbsC[1][2] + afEB[2]*aafAbsC[1][0];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A1xB2
		fR = (float)fabs(afAD[0]*aafC[2][2]-afAD[2]*aafC[0][2]);
		fOBB = afEA[0]*aafAbsC[2][2] + afEA[2]*aafAbsC[0][2];
		fR1 = afEB[0]*aafAbsC[1][1] + afEB[1]*aafAbsC[1][0];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A2xB0
		fR = (float)fabs(afAD[1]*aafC[0][0]-afAD[0]*aafC[1][0]);
		fOBB = afEA[0]*aafAbsC[1][0] + afEA[1]*aafAbsC[0][0];
		fR1 = afEB[1]*aafAbsC[2][2] + afEB[2]*aafAbsC[2][1];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A2xB1
		fR = (float)fabs(afAD[1]*aafC[0][1]-afAD[0]*aafC[1][1]);
		fOBB = afEA[0]*aafAbsC[1][1] + afEA[1]*aafAbsC[0][1];
		fR1 = afEB[0]*aafAbsC[2][2] + afEB[2]*aafAbsC[2][0];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		// axis C0+t*A2xB2
		fR = (float)fabs(afAD[1]*aafC[0][2]-afAD[0]*aafC[1][2]);
		fOBB = afEA[0]*aafAbsC[1][2] + afEA[1]*aafAbsC[0][2];
		fR1 = afEB[0]*aafAbsC[2][1] + afEB[1]*aafAbsC[2][0];
		fOBB1 = fOBB + fR1;
		if ( fR > fOBB1 )
			return false;

		return true;
	}
}
}