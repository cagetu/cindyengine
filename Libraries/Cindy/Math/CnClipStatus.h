// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CN_CLIPSTATUS_H__
#define __CN_CLIPSTATUS_H__

//------------------------------------------------------------------------------
namespace Cindy
{
namespace Math
{
class CN_DLL ClipStatus
{
public:
    // clip status enum
    enum Type
    {
        Inside = 0,
        Outside = 1,
        Clipped = 2,

        Invalid,
    };
};

} // namespace Math   
} // namespace Cindy
//------------------------------------------------------------------------------
#endif	// __CN_CLIPSTATUS_H__