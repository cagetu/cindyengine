// Copyright (c) 2006~. cagetu
//
//******************************************************************

//============================================================================
//	CnAxisAlignedBox
//============================================================================
//--------------------------------------------------------------
inline
void AABB::SetExtents( const Math::Vector3& vMin, const Math::Vector3& vMax )
{
	m_vMax = vMax;
	m_vMin = vMin;

	m_bIsEmpty = false;
	m_bNeedUpdate = true;
}
//--------------------------------------------------------------
inline
void AABB::GetExtents( Math::Vector3* Output )
{
	*Output = GetMax() - GetMin();
	*Output *= 0.5f;
}

//--------------------------------------------------------------
inline
const Math::Vector3& AABB::GetMax() const
{
	return m_vMax;
}

//--------------------------------------------------------------
inline
const Math::Vector3& AABB::GetMin() const
{
	return m_vMin;
}

//--------------------------------------------------------------
inline
const Math::Vector3& AABB::GetCenter() const
{
	Update();

	return m_vCenter;
}


//--------------------------------------------------------------
inline
void AABB::SetEmpty()
{
	m_vMin = Math::Vector3(3.3e33f, 3.3e33f, 3.3e33f);
	m_vMax = Math::Vector3(-3.3e33f, -3.3e33f, -3.3e33f);

	m_bIsEmpty = true;
	m_bNeedUpdate = true;
}

//--------------------------------------------------------------
inline
bool AABB::IsEmpty() const
{
	return m_bIsEmpty;
}

//--------------------------------------------------------------
inline
bool AABB::Contain( const AABB& Box ) const
{
	if ((m_vMin.x < Box.m_vMin.x) && (m_vMax.x >= Box.m_vMax.x) &&
		(m_vMin.y < Box.m_vMin.y) && (m_vMax.y >= Box.m_vMax.y) &&
		(m_vMin.z < Box.m_vMin.z) && (m_vMax.z >= Box.m_vMax.z))
	{
		return true;
	}
	return false;
}
inline
bool AABB::Contain( const Math::Vector3& Vec ) const
{
	if ((m_vMin.x < Vec.x) && (m_vMax.x >= Vec.x) &&
		(m_vMin.y < Vec.y) && (m_vMax.y >= Vec.y) &&
		(m_vMin.z < Vec.z) && (m_vMax.z >= Vec.z))
	{
		return true;
	}
	return false;
}

//--------------------------------------------------------------
inline
void AABB::Transform( const Math::Matrix44& m )
{
	Math::Vector3 n;
    Math::Vector3 minP(1000000, 1000000,1000000);
    Math::Vector3 maxP(-1000000, -1000000, -1000000);

	for (int i=0; i<8; ++i)
	{
		n.Multiply( m_avCorners[i], m );
		
        if (n.x > maxP.x)   maxP.x = n.x;
        if (n.y > maxP.y)   maxP.y = n.y;
        if (n.z > maxP.z)   maxP.z = n.z;
        if (n.x < minP.x)   minP.x = n.x;
        if (n.y < minP.y)   minP.y = n.y;
        if (n.z < minP.z)   minP.z = n.z;
	}

	SetExtents( minP, maxP );
}