// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CNFRUSTUM_H__
#define __CNFRUSTUM_H__

#include "CnPlane.h"

namespace Cindy
{
namespace Math
{
	class AABB;
	class OBB;
	class Matrix44;

	//==================================================================
	/** Frustum
		@author		cagetu
		@since		2008년 2월 11일
		@brief		View Frustum 클래스
	*/
	//==================================================================
	class Frustum
	{
	public:
		enum FRUSTUM_PLANE
		{
			PLANE_LEFT		= 0,
			PLANE_RIGHT,
			PLANE_TOP,
			PLANE_BOTTOM,
			PLANE_NEAR,
			PLANE_FAR,

			FRUSTUM_PLANE_COUNT		//!< number of frustum plane
		};

		struct Information
		{
			float	Left;
			float	Right;
			float	Top;
			float	Bottom;
			float	Near;
			float	Far;

			bool	IsOrtho;
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		Frustum();
		Frustum( const Math::Matrix44* matrix );

		// Planes
		void		BuildPlanes( const Math::Matrix44* matrix );
		void		BuildPlanes( const Information& Frustum_,
								 const Math::Vector3& Position_,
								 const Math::Vector3& Up_,
								 const Math::Vector3& Right_,
								 const Math::Vector3& Direction_ );
		Math::Plane* GetPlanes() const;

		// Points
		Math::Vector3* GetExtrema() const;

		// Intersect
		bool		Intersect( const Math::AABB& aabb ) const;
		bool		Intersect( const Math::OBB& obb ) const;
		bool		Intersect( const Math::Vector3& center, float radius ) const;
		bool		Intersect( const Math::Sphere& sphere, const Math::Vector3& sweepDir ) const;

	private:
		void	UpdatePoints(Math::Plane* planes, Math::Vector3* points);

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		mutable Math::Plane		m_aFrustumPlanes[FRUSTUM_PLANE_COUNT];
		mutable Math::Vector3	m_aFrustumPoints[8];
	};

#include "CnFrustum.inl"
}
}

#endif	// __CNFRUSTUM_H__