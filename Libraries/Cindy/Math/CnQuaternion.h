// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CNQUATERION_H__
#define __CNQUATERION_H__

#include "CnVector3.h"
#include "Util/CnPack.h"

namespace Cindy
{
namespace Math
{
	class Vector4;
	class Matrix44;

	//==================================================================
	/** Quaternion
		@author			cagetu
		@since			2006년 9월 31일
		@brief			쿼터니온..
	*/
	//==================================================================
	__declspec(align(16)) 
	class CN_DLL Quaternion : public D3DXQUATERNION
	{
	public:
		struct Encoded
		{
			float x, y, z;
		};

		static const Quaternion	ZERO;
		static const Quaternion	IDENTITY;

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		// 생성자
		Quaternion();
		Quaternion( CONST FLOAT* pArray );
		Quaternion( FLOAT fX, FLOAT fY, FLOAT fZ, FLOAT fW );
		Quaternion( const Quaternion& Q_ );
		Quaternion( const D3DXQUATERNION& Q_ );
		Quaternion( const Math::Vector3& vAxis, Radian fRadian );

		// 초기값 설정
		void	Identity();

		// Operations...
		float	Length() const;
		float	LengthSq() const;
		float	Dot( const Quaternion& rQuat ) const;
		void	Normalize();

		void	Inverse( Quaternion& rResult ) const;
		void	Exp( Quaternion& rResult ) const;					//! apply to quaternion with w = 0
		void	Log( Quaternion& rResult ) const;					//! apply to unit-length quaternion

		// 축과 각으로..
		void	FromAxisAngle( const Math::Vector3& rAxis, Radian fAngle );
		void	ToAxisAngle( Math::Vector3& rAxis, Radian& fAngle ) const;

		// 행렬
		void	FromMatrix( const Math::Matrix44& mRotation );
		void	ToMatrix( Math::Matrix44& outResult ) const;

		// Euler
		void	FromEuler( const Math::Vector3& Euler );
		void	FromEuler( Radian Yaw, Radian Pitch, Radian Roll );
		void	ToEuler( Math::Vector3& outEuler );

		// 회전하기
		void	Rotate( const Math::Vector3& rVector, Math::Vector3& rResult );

		// 쿼터니온 보간
		void	Slerp( const Quaternion& rP, const Quaternion& rQ, float fT );

		// 곱셈
		void	Multiply( const Quaternion& q1, const Quaternion& q2 );

		// 압축
		void	Encode( Encoded& Output );
		void	Decode( const Encoded& Input ); 

		void	Encode( float16 Output[4] );
		void	Decode( const float16 Output[4] );
	};

	#include "CnQuaternion.inl"
}
}

#endif	// __CNQUATERION_H__