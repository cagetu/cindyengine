// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnPlane.h"
#include "CnSphere.h"

namespace Cindy
{
namespace Math
{
	//--------------------------------------------------------------
	/** 축을 가진 sweep 구의 교차 판정
		@remarks	한정된 크기를 갖지 않는다.
	*/
	//--------------------------------------------------------------
	bool Plane::IntersectSweptSphere( float* t0, float* t1, const Math::Sphere& sphere, const Math::Vector3& sweepDir )
	{
		float dist = GetDistance( sphere.center );
		float cos = normal.Dot( sweepDir );

		// 축과 평면은 평행
		if (cos == 0.0f)
		{
			// 평행인데, 구의 반지름보다 구의 중점과의 거리가 짧다면 교차..
			if (dist <= sphere.radius)
			{
				// 한점에서 교차하는 것이 아니므로, 많다..
				*t0 = 0.f;
				*t1 = 1e32f;
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			float tmp0 = (sphere.radius - dist) / cos;
			float tmp1 = (-sphere.radius - dist) / cos;
			*t0 = min( tmp0, tmp1 );
			*t1 = max( tmp0, tmp1 );
			return true;
		}
	}

	//--------------------------------------------------------------
	/** 평면과 ray와의 교차
		@remarks	교점 P(t) = S + tV
					(t = -(N*S+D) / N*V)
	*/
	//--------------------------------------------------------------
	bool Plane::Intersect( const Math::Vector3& vRayOrig, const Math::Vector3& vRayDir, float* hitDist )
	{
		float dist = GetDistance( vRayOrig );
		float cosTheta = normal.Dot( vRayDir );

		*hitDist = 0.0f;
		if (ALMOST_ZERO(dist))	// 거리가 근사..
			return true;

		*hitDist = 1e33f;
		if (ALMOST_ZERO(cosTheta))	// 평행이면, skip..
			return false;

		*hitDist = -dist / cosTheta;
		return true;
	}

	//--------------------------------------------------------------
	/** 세 평면의 교차점
		P = -d1*cross(n2,n3) - d2*cross(n3,n1) - d3*cross(n1,n2) / dot( n1, cross(n2,n3) );

		dot( n1, cross(n2,n3) ) != 0 일 경우에, 한 점에서 교차한다.
								= 0 일 경우에, 평면에 대해 선으로 교차

		http://www.softsurfer.com/Archive/algorithm_0104/algorithm_0104B.htm#Intersection%20of%203%20Planes
	*/
	//--------------------------------------------------------------
	bool Plane::Intersect( Math::Vector3* intersectPt, const Plane& p1, const Plane& p2 )
	{
		Math::Vector3 n1_n2, n2_n0, n0_n1;

		n1_n2.Cross( p1.normal, p2.normal );
		n2_n0.Cross( p2.normal, normal );
		n0_n1.Cross( normal, p1.normal );

		float cosTheta = normal.Dot( n1_n2 );

		if (ALMOST_ZERO(cosTheta) || IS_SPECIAL(cosTheta))
			return false;

		float secTheta = 1.0f/cosTheta;

		n1_n2 = n1_n2 * distance;
		n2_n0 = n2_n0 * p1.distance;
		n0_n1 = n0_n1 * p2.distance;

		*intersectPt = -(n1_n2 + n2_n0 + n0_n1) * secTheta;
		return true;
	}
}
}