// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

#include "CnVector3.h"

namespace Cindy
{
namespace Math
{
	//==================================================================
	/** Ray Class
		@author			cagetu
		@since			2008년 2월 20일
		@brief			Ray 클래스
	*/
	//==================================================================
	class CN_DLL Ray
	{
	public:
		Ray();

		void	FromScreen( long ScreenX, long ScreenY,
							long ScreenTop, long ScreenLeft,
							long ScreenBottom, long ScreenRight,
							const Math::Matrix44& View, const Math::Matrix44& Projection );

	public:
		Math::Vector3	origin;
		Math::Vector3	direction;
		float			distance;
	};

#include "CnRay.inl"
}
}