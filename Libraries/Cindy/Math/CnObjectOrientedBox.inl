// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
inline
void OBB::SetCenter( const Math::Vector3& vCenter )
{
	m_vCenter = vCenter;
}

//--------------------------------------------------------------
inline
const Math::Vector3* OBB::GetAxis() const
{
	return m_vAxis;
}

//--------------------------------------------------------------
inline
const float* OBB::GetExtents() const
{
	return m_fExtent;
}

//--------------------------------------------------------------
inline
const Math::Vector3& OBB::GetCenter() const
{
	return m_vCenter;
}