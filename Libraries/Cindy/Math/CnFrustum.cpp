// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnFrustum.h"
#include "CnMatrix44.h"
#include "CnAxisAlignedBox.h"
#include "CnObjectOrientedBox.h"
#include "CnSphere.h"
#include "Util/CnString.h"

namespace Cindy
{
namespace Math
{
	//--------------------------------------------------------------
	Frustum::Frustum()
	{
	}
	Frustum::Frustum( const Math::Matrix44* matrix )
	{
		BuildPlanes( matrix );
	}

	//--------------------------------------------------------------
	/**	절두체의 평면 만들기
		평면의 방향이 시각 절두체 안으로 만들 것인지, 밖으로 만들 것인지를 잘 선택해야 한다.
		현재는 절두체 안으로 평면의 노말을 설정하였음.
	*/
	//--------------------------------------------------------------
	void Frustum::BuildPlanes( const Math::Matrix44* matrix )
	{
#if 0
		//  build a view frustum based on the current view & projection matrices...
		Math::Vector4 column4( matrix->_14, matrix->_24, matrix->_34, matrix->_44 );
		Math::Vector4 column1( matrix->_11, matrix->_21, matrix->_31, matrix->_41 );
		Math::Vector4 column2( matrix->_12, matrix->_22, matrix->_32, matrix->_42 );
		Math::Vector4 column3( matrix->_13, matrix->_23, matrix->_33, matrix->_43 );

		Math::Vector4 planes[6];
		planes[PLANE_LEFT] = column4 - column1;  // left
		planes[PLANE_RIGHT] = column4 + column1;  // right
		planes[PLANE_BOTTOM] = column4 - column2;  // bottom
		planes[PLANE_TOP] = column4 + column2;  // top
		planes[PLANE_NEAR] = column4 - column3;  // near
		planes[PLANE_FAR] = column4 + column3;  // far
		// ignore near & far plane
		
		int p=0;
		for (p=0; p<FRUSTUM_PLANE_COUNT; ++p)
		{
			float dot = planes[p].x*planes[p].x + planes[p].y*planes[p].y + planes[p].z*planes[p].z;
			dot = 1.f / sqrtf(dot);
			planes[p] = planes[p] * dot;
		}
		
		for (p=0; p<6; p++)
			m_aFrustumPlanes[p] = Math::Plane(Math::Vector3(planes[p].x, planes[p].y, planes[p].z), planes[p].w);
#else
		m_aFrustumPlanes[PLANE_LEFT].normal.x = matrix->_14 + matrix->_11;
		m_aFrustumPlanes[PLANE_LEFT].normal.y = matrix->_24 + matrix->_21;
		m_aFrustumPlanes[PLANE_LEFT].normal.z = matrix->_34 + matrix->_31;
		m_aFrustumPlanes[PLANE_LEFT].distance = matrix->_44 + matrix->_41;

		m_aFrustumPlanes[PLANE_RIGHT].normal.x = matrix->_14 - matrix->_11;
		m_aFrustumPlanes[PLANE_RIGHT].normal.y = matrix->_24 - matrix->_21;
		m_aFrustumPlanes[PLANE_RIGHT].normal.z = matrix->_34 - matrix->_31;
		m_aFrustumPlanes[PLANE_RIGHT].distance = matrix->_44 - matrix->_41;

		m_aFrustumPlanes[PLANE_TOP].normal.x = matrix->_14 - matrix->_12;
		m_aFrustumPlanes[PLANE_TOP].normal.y = matrix->_24 - matrix->_22;
		m_aFrustumPlanes[PLANE_TOP].normal.z = matrix->_34 - matrix->_32;
		m_aFrustumPlanes[PLANE_TOP].distance = matrix->_44 - matrix->_42;

		m_aFrustumPlanes[PLANE_BOTTOM].normal.x = matrix->_14 + matrix->_12;
		m_aFrustumPlanes[PLANE_BOTTOM].normal.y = matrix->_24 + matrix->_22;
		m_aFrustumPlanes[PLANE_BOTTOM].normal.z = matrix->_34 + matrix->_32;
		m_aFrustumPlanes[PLANE_BOTTOM].distance = matrix->_44 + matrix->_42;

		m_aFrustumPlanes[PLANE_NEAR].normal.x = matrix->_14 + matrix->_13;
		m_aFrustumPlanes[PLANE_NEAR].normal.y = matrix->_24 + matrix->_23;
		m_aFrustumPlanes[PLANE_NEAR].normal.z = matrix->_34 + matrix->_33;
		m_aFrustumPlanes[PLANE_NEAR].distance = matrix->_44 + matrix->_43;

		m_aFrustumPlanes[PLANE_FAR].normal.x = matrix->_14 - matrix->_13;
		m_aFrustumPlanes[PLANE_FAR].normal.y = matrix->_24 - matrix->_23;
		m_aFrustumPlanes[PLANE_FAR].normal.z = matrix->_34 - matrix->_33;
		m_aFrustumPlanes[PLANE_FAR].distance = matrix->_44 - matrix->_43;
		
		for (int i=0; i<FRUSTUM_PLANE_COUNT; ++i)
		{
			//m_aFrustumPlanes[i] = -m_aFrustumPlanes[i];
			m_aFrustumPlanes[i].Normalize();
		}
#endif

		UpdatePoints(m_aFrustumPlanes, m_aFrustumPoints);
	}

	//--------------------------------------------------------------
	/** 절두체의 평면 만들기
	*/
	//--------------------------------------------------------------
	void Frustum::BuildPlanes( const Frustum::Information& Frustum_,
							   const Math::Vector3& Position_,
							   const Math::Vector3& Up_,
							   const Math::Vector3& Right_,
							   const Math::Vector3& Direction_ )
	{
		// near Plane
		Math::Vector3 point = Position_ + Frustum_.Near * Direction_;
		m_aFrustumPlanes[PLANE_NEAR] = Math::Plane( Direction_, point );

		// far Plane
		point = Position_ + Frustum_.Far * Direction_;
		m_aFrustumPlanes[PLANE_FAR] = Math::Plane( -Direction_, point );

		if (Frustum_.IsOrtho)
		{
			point = Position_ + Frustum_.Left * Right_;
			m_aFrustumPlanes[PLANE_LEFT] = Math::Plane( Right_, point );

			point = Position_ + Frustum_.Right * Right_;
			m_aFrustumPlanes[PLANE_RIGHT] = Math::Plane( -Right_, point );

			point = Position_ + Frustum_.Top * Up_;
			m_aFrustumPlanes[PLANE_TOP] = Math::Plane( -Up_, point );

			point = Position_ + Frustum_.Bottom * Up_;
			m_aFrustumPlanes[PLANE_BOTTOM] = Math::Plane( Up_, point );
		}
		else
		{
			// 수평 시야각 a가 결정되었을 때, 투영평면까지의 거리를 e라고 한다.
			// e = 1.0f / tan(a/2) -> frustum.left
			float temp, inv, c0, c1;
			Math::Vector3 normal;

			// left Plane
			temp = Frustum_.Left * Frustum_.Left;
			inv = 1.0f / Math::Sqrt( 1.0f + temp );
			c0 = -Frustum_.Left * inv;
			c1 = inv;
			normal = c0 * Direction_ + c1 * Right_;
			m_aFrustumPlanes[PLANE_LEFT] = Math::Plane( normal, Position_ );

			temp = Frustum_.Right * Frustum_.Right;
			inv = 1.0f / Math::Sqrt( 1.0f + temp );
			c0 = Frustum_.Right * inv;
			c1 = -inv;
			normal = c0 * Direction_ + c1 * Right_;
			m_aFrustumPlanes[PLANE_RIGHT] = Math::Plane( normal, Position_ );

			temp = Frustum_.Top * Frustum_.Top;
			inv = 1.0f / Math::Sqrt( 1.0f + temp );
			c0 = Frustum_.Top * inv;
			c1 = -inv;
			normal = c0 * Direction_ + c1 * Up_;
			m_aFrustumPlanes[PLANE_TOP] = Math::Plane( normal, Position_ );

			temp = Frustum_.Bottom * Frustum_.Bottom;
			inv = 1.0f / Math::Sqrt( 1.0f + temp );
			c0 = -Frustum_.Bottom * inv;
			c1 = inv;
			normal = c0 * Direction_ + c1 * Up_;
			m_aFrustumPlanes[PLANE_BOTTOM] = Math::Plane( normal, Position_ );
		}

		UpdatePoints(m_aFrustumPlanes, m_aFrustumPoints);
	}

	void Frustum::UpdatePoints(Math::Plane* planes, Math::Vector3* points)
	{
		Math::Plane* p0;
		for (int i=0; i<8; ++i)
		{
			p0 = (i&4) ? &planes[PLANE_NEAR] : &planes[PLANE_FAR];
			const Math::Plane& p1 = (i&1) ? planes[PLANE_TOP] : planes[PLANE_BOTTOM];
			const Math::Plane& p2 = (i&2) ? planes[PLANE_LEFT] : planes[PLANE_RIGHT];

			bool ret = p0->Intersect( &points[i], p1, p2 );
			_ASSERT(ret);

			//const wchar* t0 = (i&1) ? L"PLANE_NEAR" : L"PLANE_FAR";
			//const wchar* t1 = (i&2) ? L"PLANE_TOP" : L"PLANE_BOTTOM";
			//const wchar* t2 = (i&4) ? L"PLANE_LEFT" : L"PLANE_RIGHT";
			//OutputDebugString(unicode::ToString(L"frustum points: %d, %s | %s | %s\n", i, t0, t1, t2).c_str());
		}
	}

	//--------------------------------------------------------------
	/** Frustum의 모든 극점들을 구한다.
	*/
	//--------------------------------------------------------------
	Math::Vector3* Frustum::GetExtrema() const
	{
		return m_aFrustumPoints;
	}

	//--------------------------------------------------------------
	/** AABB와 교차 체크를 한다.
		평면의 노말 방향이 어떻게 되어 있는지에 따라, Test 식을 선택한다.
	*/
	//--------------------------------------------------------------
	bool Frustum::Intersect( const Math::AABB& aabb ) const
	{
		//Math::Vector3 center = aabb->GetCenter();
		//Math::Vector3 max = aabb->GetMax();
		//Math::Vector3 min = aabb->GetMin();
		//Math::Vector3 halfSize = (max-min)*0.5f;

		//for (int i=0; i<FRUSTUM_PLANE_COUNT; ++i)
		//{
		//	if (i==PLANE_FAR /**&& m_fFarForFrustum == 0.0f*/)
		//		continue;

		//	Math::Plane::SIDE side = m_aFrustumPlanes[i].GetSide( center, halfSize );
		//	if (side == Math::Plane::NEGATIVE_SIDE)
		//		return false;
		//}

		// Frustum의 평면의 노말 방향이 Frustum 안쪽으로 향하도록 되어 있으므로,
		// 평면의 반대쪽에 있는 AABB는 보이지 않는것으로 간주한다.
		float whichSide;
		for (int i=0; i<FRUSTUM_PLANE_COUNT; ++i)
		{
			if (aabb.Intersect( m_aFrustumPlanes[i], whichSide ))
				return true;

			if (whichSide<0.0f)
				return false;
		}

		//Math::Vector3 vMax = rBox.GetMax();
		//Math::Vector3 vCenter = rBox.GetCenter();

		//Math::Vector3 vR = Math::Vector3( vMax.x, vCenter.y, vCenter.z ) - vCenter;
		//Math::Vector3 vS = Math::Vector3( vCenter.x, vCenter.y, vMax.z ) - vCenter;
		//Math::Vector3 vT = Math::Vector3( vCenter.x, vMax.y, vCenter.z ) - vCenter;

		//float fRadius = 0.0f;
		//for( int i = 0; i < FRUSTUM_PLANE_COUNT; ++i )
		//{
		//	fRadius = abs( vR.Dot( m_aFrustumPlanes[i].normal ) ) + 
		//			  abs( vS.Dot( m_aFrustumPlanes[i].normal ) ) + 
		//			  abs( vT.Dot( m_aFrustumPlanes[i].normal ) );

		//	if( m_aFrustumPlanes[i].GetDistance(vCenter) <= -fRadius )
		//		return false;
		//}

		return true;
	}

	//--------------------------------------------------------------
	/** OBB와 교차 판정
	*/
	//--------------------------------------------------------------
	bool Frustum::Intersect( const Math::OBB& obb ) const
	{
		const Math::Vector3* axis = obb.GetAxis();
		const float* extents = obb.GetExtents();

		Math::Vector3 vR, vS, vT;

		vR.Scale( axis[0], extents[0] /** * 0.5f */);
		vS.Scale( axis[1], extents[1] /** * 0.5f */);
		vT.Scale( axis[2], extents[2] /** * 0.5f */);

		float fRadius = 0.f;

		Math::Vector3 center = obb.GetCenter();

		for( int i = 0; i < FRUSTUM_PLANE_COUNT; ++i )
		{
			fRadius = abs( vR.Dot( m_aFrustumPlanes[i].normal ) ) + 
					  abs( vS.Dot( m_aFrustumPlanes[i].normal ) ) + 
					  abs( vT.Dot( m_aFrustumPlanes[i].normal ) );

			if( m_aFrustumPlanes[i].GetDistance( center ) <= -fRadius )
				return false;
		}

		return true;
	}

	//--------------------------------------------------------------
	/** Sphere와 교차 판정
		@remarks	평면의 노말이 절두체 안으로 향한다면, <= -radius 일 경우,
					교차가되지 않는다.
	*/
	//--------------------------------------------------------------
	bool Frustum::Intersect( const Math::Vector3& center, float radius ) const
	{
		for( int i = 0; i < FRUSTUM_PLANE_COUNT; ++i )
		{
			if( m_aFrustumPlanes[i].GetDistance(center) <= -radius )
				return false;
		}

		return true;
	}

	//--------------------------------------------------------------
	/** sweep구와 교차 판정 (한 축으로 무한한 구와의 교차 판정)
		@remarks	view frustum을 가지고, 교차점이 0보다 큰,
					swept 구와 모든 12개의 교차점을 구한다.
					sweep 방향에 따라 구를 이동시킨다.
					만약, 이동 된 구가 frustum 안에 있으면, 교차
	*/
	//--------------------------------------------------------------
	bool Frustum::Intersect( const Math::Sphere& sphere, const Math::Vector3& sweepDir ) const
	{
		float displacements[12];
		int numIntersectPt = 0;
		float a, b;
		bool inFrustum = false;

		for (int i=0; i<6; ++i)
		{
			if (m_aFrustumPlanes[i].IntersectSweptSphere( &a, &b, sphere, sweepDir ))
			{
				if (a>=0.0f)
					displacements[numIntersectPt++] = a;
				if (b>=0.0f)
					displacements[numIntersectPt++] = b;
			}
		}

		Math::Vector3 newCenter;
		float newRadius;

		for (int i=0; i<numIntersectPt; ++i)
		{
			newCenter = sphere.center + (sweepDir)*displacements[i];
			newRadius = sphere.radius * 1.1f;
			inFrustum |= this->Intersect( newCenter, newRadius );
		}
		return inFrustum;
	}
}
}
