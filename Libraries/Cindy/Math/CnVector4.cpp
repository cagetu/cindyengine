// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnVector4.h"
#include "CnVector3.h"
#include "CnMatrix44.h"

#include "Util/CnColor.h"

namespace Cindy
{
namespace Math
{
	//==================================================================
	//	Vector4
	//==================================================================
	const Vector4 Vector4::ZERO( 0.0f, 0.0f, 0.0f, 0.0f );
	const Vector4 Vector4::UNIT( 1.0f, 1.0f, 1.0f, 1.0f );
	const Vector4 Vector4::UNIT_X( 1.0f, 0.0f, 0.0f, 0.0f );
	const Vector4 Vector4::UNIT_Y( 0.0f, 1.0f, 0.0f, 0.0f );
	const Vector4 Vector4::UNIT_Z( 0.0f, 0.0f, 1.0f, 0.0f );
	const Vector4 Vector4::UNIT_W( 0.0f, 0.0f, 0.0f, 1.0f );

	//------------------------------------------------------------------
	/** Set
	*/
	//------------------------------------------------------------------
	void Vector4::FromVec3( const Math::Vector3& v )
	{
		x = v.x;
		y = v.y;
		z = v.z;
		w = 1.0f;
	}

	void Vector4::FromColor( const CnColor& v )
	{
		x = v.r;
		y = v.g;
		z = v.b;
		w = v.a;
	}

	//-------------------------------------------------------------------------
	void Vector4::Multiply( const Math::Vector3& v, const Math::Matrix44& m )
	{
		D3DXVec3Transform( this, &v, &m );
	}
	void Vector4::Multiply( const Vector4& v, const Math::Matrix44& m )
	{
		D3DXVec4Transform( this, &v, &m );
	}
}
}