// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CN_AXISALIGNEDBOX_H__
#define __CN_AXISALIGNEDBOX_H__

#include "CnVector3.h"
#include "CnMatrix44.h"
#include "CnClipStatus.h"

namespace Cindy
{
namespace Math
{
	class Plane;

	//==================================================================
	/** AABB
		@author				cagetu
		@since				2006년 9월 31일
		@brief
			Axis Aligned Bounding Box( 축 정렬 경계 상자 )
	*/
	//==================================================================
	class CN_DLL AABB
	{
	public:
		/// clip codes
		enum 
		{
			ClipLeft   = (1<<0),
			ClipRight  = (1<<1),
			ClipBottom = (1<<2),
			ClipTop    = (1<<3),
			ClipNear   = (1<<4),
			ClipFar    = (1<<5),
		};

		//---------------------------------------------------------
		//	Methods
		//---------------------------------------------------------
		AABB();
		AABB( const Math::Vector3& vMin, const Math::Vector3& vMax );
		AABB( const Math::Vector3* aPoints, unsigned int numPoints );
		AABB( const AABB* aBoxes, unsigned int numBoxes );

		// 최대/최소값..
		void					SetExtents( const Math::Vector3& vMin, const Math::Vector3& vMax );
		void					SetExtents( const Math::Vector3* aPoints, unsigned int numPoints );
		void					SetExtents( const AABB* aBoxes, unsigned int numBoxes );

		void					GetExtents( Math::Vector3* Output );
		const Math::Vector3&	GetMin() const;
		const Math::Vector3&	GetMax() const;
		const Math::Vector3&	GetCenter() const;

		// Merge
		void					Merge( const AABB& rBox );
		void					Merge( const Math::Vector3& Vec );

		// Transform
		void					Transform( const Math::Matrix44& m );

		// Box Corner Vertices..
		const Math::Vector3*	GetCorners() const;

		// Empty..
		void					SetEmpty();
		bool					IsEmpty() const;

		// Contain...
		bool					Contain( const AABB& Box ) const;
		bool					Contain( const Math::Vector3& Vec ) const;

		// Intersection...
		bool					Intersect( const AABB& rBox ) const;
		bool					Intersect( const AABB& rBox, AABB& rOverlap ) const;
		bool					Intersect( const Math::Vector3& vCenter, float fRadius ) const;
		bool					Intersect( const Math::Vector3& vRayOrig, const Math::Vector3& vRayDir, Math::Vector3* hitPoint );
		bool					Intersect( const Math::Vector3& vRayOrig, const Math::Vector3& vRayDir, float* hitDist );
		bool					Intersect( const Math::Plane& plane, float& whichSideNdist ) const;

		//
		ClipStatus::Type	ClipStatus( const Math::Matrix44& ViewProjection ) const;
	private:
		//---------------------------------------------------------
		// Variables
		//---------------------------------------------------------
		Math::Vector3			m_vMin;				//!< 최소값
		Math::Vector3			m_vMax;				//!< 최대값

		mutable Math::Vector3	m_vCenter;			//!< 중간값
		mutable Math::Vector3	m_avCorners[8];		//!< Box Corner Vertices
		mutable bool			m_bNeedUpdate;		//!< 업데이트 필요 여부

		bool					m_bIsEmpty;			//!< 비어 있느냐?

		//---------------------------------------------------------
		// Methods
		//---------------------------------------------------------
		void	Update() const;
	};

#include "CnAxisAlignedBox.inl"
} // namespace Math
} // namespace Cindy

#endif	// __CN_AXISALIGNEDBOX_H__