// Copyright (c) 2006~. cagetu
//
//******************************************************************

//==================================================================
//	Vector3
//==================================================================
__forceinline
Vector3::Vector3()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}

//--------------------------------------------------------------
__forceinline
Vector3::Vector3( CONST FLOAT* pVector )
: D3DXVECTOR3( pVector )
{
}

//--------------------------------------------------------------
__forceinline
Vector3::Vector3( CONST D3DXFLOAT16* pVector )
: D3DXVECTOR3( pVector )
{
}

//--------------------------------------------------------------
__forceinline
Vector3::Vector3( FLOAT x, FLOAT y, FLOAT z )
: D3DXVECTOR3( x, y, z )
{
}

//--------------------------------------------------------------
__forceinline
Vector3::Vector3( CONST D3DXVECTOR3& rVector )
: D3DXVECTOR3( rVector )
{
}

//--------------------------------------------------------------
__forceinline
Vector3::Vector3( const Vector3& rVector )
{
	x = rVector.x;
	y = rVector.y;
	z = rVector.z;
}

//--------------------------------------------------------------
__forceinline
void Vector3::Set( float x, float y, float z )
{
    this->x = x;
    this->y = y;
    this->z = z;
}

//--------------------------------------------------------------
/** @desc	Vector 곱 */
//--------------------------------------------------------------
//__forceinline
//Vector3 Vector3::operator *( const Vector3& rhs ) const
//{
//	return Vector3( x*rhs.x, y*rhs.y, z*rhs.z );
//}
//__forceinline
//Vector3 operator *( const float Scalar, const Vector3& rhs )
//{
//	return Vector3( Scalar*rhs.x, Scalar*rhs.y, Scalar*rhs.x );
//}

//--------------------------------------------------------------
__forceinline
Vector3& Vector3::operator = ( const Vector3& vInput )
{
	x = vInput.x;
	y = vInput.y;
	z = vInput.z;

	return *this;
}

//--------------------------------------------------------------
__forceinline
int Vector3::_Compare( const Vector3& rVector ) const
{
	return memcmp( this, &rVector, sizeof(Vector3) );
}

//--------------------------------------------------------------
__forceinline
bool Vector3::operator == ( const Vector3& vInput ) const
{
//	return ( ( x == vInput.x ) && ( y == vInput.y ) && ( z == vInput.z ) );
	return _Compare( vInput ) == 0;
}

//--------------------------------------------------------------
__forceinline
bool Vector3::operator != ( const Vector3& vInput ) const
{
//	return ( ( x != vInput.x ) || ( y != vInput.y ) || ( z != vInput.z ) );
	return _Compare( vInput ) != 0;
}

//--------------------------------------------------------------
__forceinline
float Vector3::Length() const
{
	return D3DXVec3Length( this );
}

//--------------------------------------------------------------
__forceinline
float Vector3::LengthSq() const
{
	return D3DXVec3LengthSq( this );
}

//--------------------------------------------------------------
/** 내적
	@desc	Dot(A,B) = |A|*|B|*cos@
*/
//--------------------------------------------------------------
__forceinline
float Vector3::Dot( const Vector3& v ) const
{
	return D3DXVec3Dot( this, &v );
}

//--------------------------------------------------------------
__forceinline
void Vector3::Normalize()
{
	D3DXVec3Normalize( this, this );
}
__forceinline
void Vector3::Normalize( Vector3* Output )
{
	D3DXVec3Normalize( Output, this );
}

//--------------------------------------------------------------
/** 외적 구하기
	@remarks
		오른손 좌표계를 기준으로 한다.
*/
//--------------------------------------------------------------
__forceinline
void Vector3::Cross( const Vector3& v1, const Vector3& v2 )
{
	D3DXVec3Cross( this, &v1, &v2 );
}

//--------------------------------------------------------------
__forceinline
void Vector3::UnitCross( const Vector3& v1, const Vector3& v2 )
{
	Cross( v1, v2 );
	Normalize();
}

//--------------------------------------------------------------
__forceinline
void Vector3::Multiply( const Vector3& v )
{
	x *= v.x;
	y *= v.y;
	z *= v.z;
}

//--------------------------------------------------------------
__forceinline
void Vector3::Scale( const Vector3& v, float fScale )
{
	D3DXVec3Scale( this, &v, fScale );
}

//--------------------------------------------------------------
//	보간
//--------------------------------------------------------------
__forceinline
void Vector3::Lerp( const Vector3& v1, const Vector3& v2, float delta )
{
	D3DXVec3Lerp( this, &v1, &v2, delta );
}