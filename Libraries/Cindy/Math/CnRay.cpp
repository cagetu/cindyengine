// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnRay.h"
#include "CnMatrix44.h"

namespace Cindy
{
namespace Math
{
	//--------------------------------------------------------------
	//	@brief	Screen으로 부터 Ray 생성
	//--------------------------------------------------------------
	void Ray::FromScreen( long ScreenX, long ScreenY,
						  long ScreenTop, long ScreenLeft,
						  long ScreenBottom, long ScreenRight,
						  const Math::Matrix44& View, const Math::Matrix44& Projection )
	{
		Math::Vector3 v;
		v.x = ((  (((ScreenX-ScreenLeft)*2.0f/ScreenRight ) - 1.0f)) - Projection._31 ) / Projection._11;
		v.y = ((- (((ScreenY-ScreenTop)*2.0f/ScreenBottom) - 1.0f)) - Projection._32 ) / Projection._22;
		v.z = 1.0f;

		Math::Matrix44 view;
		View.Inverse( view );

		// Transform the screen space pick ray into 3D space
		this->direction.x  = v.x*view._11 + v.y*view._21 + v.z*view._31;
		this->direction.y  = v.x*view._12 + v.y*view._22 + v.z*view._32;
		this->direction.z  = v.x*view._13 + v.y*view._23 + v.z*view._33;

		this->origin.x = view._41;
		this->origin.y = view._42;
		this->origin.z = view._43;
	}
}
}