// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnAxisAlignedBox.h"
#include "CnPlane.h"

namespace Cindy
{
namespace Math
{
	//--------------------------------------------------------------
	AABB::AABB()
	{
		SetEmpty();
	}
	AABB::AABB( const Math::Vector3& vMin, const Math::Vector3& vMax )
	{
		SetExtents( vMin, vMax );
	}
	AABB::AABB( const Math::Vector3* aPoints, unsigned int numPoints )
		: m_vMin(1e33f, 1e33f, 1e33f)
		, m_vMax(-1e33f, -1e33f, -1e33f)
	{
		//SetExtents( aPoints, numPoints );
		for (unsigned int i=0; i<numPoints; ++i)
		{
			Merge( aPoints[i] );
		}
	}
	AABB::AABB( const AABB* aBoxes, unsigned int numBoxes )
		: m_vMin(1e33f, 1e33f, 1e33f)
		, m_vMax(-1e33f, -1e33f, -1e33f)
	{
		for (unsigned int i=0; i<numBoxes; ++i)
		{
			Merge( aBoxes[i].m_vMin );
			Merge( aBoxes[i].m_vMax );
		}
		//SetExtents( aBoxes, numBoxes );
	}

	//--------------------------------------------------------------
	void AABB::Update() const
	{
		if (m_bNeedUpdate)
		{
			// update Corners
			m_avCorners[0] = m_vMin;
			m_avCorners[1].x = m_vMin.x;	m_avCorners[1].y = m_vMax.y;	m_avCorners[1].z = m_vMin.z;
			m_avCorners[2].x = m_vMax.x;	m_avCorners[2].y = m_vMax.y;	m_avCorners[2].z = m_vMin.z;
			m_avCorners[3].x = m_vMax.x;	m_avCorners[3].y = m_vMin.y;	m_avCorners[3].z = m_vMin.z;

			m_avCorners[4] = m_vMax;
			m_avCorners[5].x = m_vMin.x;	m_avCorners[5].y = m_vMax.y;	m_avCorners[5].z = m_vMax.z;
			m_avCorners[6].x = m_vMin.x;	m_avCorners[6].y = m_vMin.y;	m_avCorners[6].z = m_vMax.z;
			m_avCorners[7].x = m_vMax.x;	m_avCorners[7].y = m_vMin.y;	m_avCorners[7].z = m_vMax.z;

			// update Center
			m_vCenter = (m_vMin + m_vMax) * 0.5f;

			m_bNeedUpdate = false;
		}
	}

	//--------------------------------------------------------------
	const Math::Vector3* AABB::GetCorners() const
	{
		Update();

		return (const Math::Vector3*)m_avCorners;
	}

	//--------------------------------------------------------------
	void AABB::SetExtents( const Math::Vector3* aPoints, unsigned int numPoints )
	{
		m_vMin = aPoints[0];
		m_vMax = aPoints[0];

		for (unsigned int i=0; i<numPoints; ++i)
		{
			if (aPoints[i].x < m_vMin.x)
				m_vMin.x = aPoints[i].x;
			else if(aPoints[i].x > m_vMax.x)
				m_vMax.x = aPoints[i].x;

			if (aPoints[i].y < m_vMin.y)
				m_vMin.y = aPoints[i].y;
			else if(aPoints[i].y > m_vMax.y)
				m_vMax.y = aPoints[i].y;

			if (aPoints[i].z < m_vMin.z)
				m_vMin.z = aPoints[i].z;
			else if(aPoints[i].z > m_vMax.z)
				m_vMax.z = aPoints[i].z;
		}

		m_bIsEmpty = false;
		m_bNeedUpdate = true;
	}
	void AABB::SetExtents( const AABB* aBoxes, unsigned int numBoxes )
	{
		for (unsigned int i=0; i<numBoxes; ++i)
		{
			Merge( aBoxes[i] );
		}

		m_bIsEmpty = false;
		m_bNeedUpdate = true;
	}

	//--------------------------------------------------------------
	void AABB::Merge( const AABB& rBox )
	{
		if( rBox.IsEmpty() ) 
		{
			return;
		}
		else if( IsEmpty() )
		{
			SetExtents( rBox.m_vMin, rBox.m_vMax );
		}
		else
		{
			Math::Vector3 min = m_vMin;
			Math::Vector3 max = m_vMax;

			if( min.x > rBox.m_vMin.x )
				m_vMin.x = rBox.m_vMin.x;
			if( max.x < rBox.m_vMax.x )
				m_vMax.x = rBox.m_vMax.x;

			if( min.y > rBox.m_vMin.y )
				m_vMin.y = rBox.m_vMin.y;
			if( max.y < rBox.m_vMax.y )
				m_vMax.y = rBox.m_vMax.y;

			if( min.z > rBox.m_vMin.z )
				m_vMin.z = rBox.m_vMin.z;
			if( max.z < rBox.m_vMax.z )
				m_vMax.z = rBox.m_vMax.z;

			m_bNeedUpdate = true;
			m_bIsEmpty = false;
		}
	}
	void AABB::Merge( const Math::Vector3& Vec )
	{
		m_vMin.x = min( m_vMin.x, Vec.x );
		m_vMin.y = min( m_vMin.y, Vec.y );
		m_vMin.z = min( m_vMin.z, Vec.z );
		m_vMax.x = max( m_vMax.x, Vec.x );
		m_vMax.y = max( m_vMax.y, Vec.y );
		m_vMax.z = max( m_vMax.z, Vec.z );

		m_bNeedUpdate = true;
		m_bIsEmpty = false;
	}

	//--------------------------------------------------------------
	bool AABB::Intersect( const Math::Vector3& vCenter, float fRadius ) const
	{
		if( IsEmpty() )
			return false;

		// just test facing planes, early fail if sphere is totally outside
		if( vCenter.x < m_vMin.x && m_vMin.x - vCenter.x > fRadius )		return false;
		if( vCenter.x > m_vMax.x && vCenter.x - m_vMax.x > fRadius )		return false;

		if( vCenter.y < m_vMin.y && m_vMin.y - vCenter.y > fRadius )		return false;
		if( vCenter.y > m_vMax.y && vCenter.y - m_vMax.y > fRadius )		return false;

		if( vCenter.z < m_vMin.z && m_vMin.z - vCenter.z > fRadius )		return false;
		if( vCenter.z > m_vMax.z && vCenter.z - m_vMax.z > fRadius )		return false;

		return true;
	}

	//--------------------------------------------------------------
	bool AABB::Intersect( const AABB& rBox ) const
	{
		//if( GetMin().x < rBox.GetMin().x || GetMax().x > rBox.GetMax().x )
		//	return false;

		//if( GetMin().y < rBox.GetMin().y || GetMax().y > rBox.GetMax().y )
		//	return false;

		//if( GetMin().z < rBox.GetMin().z || GetMax().z > rBox.GetMax().z )
		//	return false;

		if (m_vMin.x > rBox.GetMax().x || rBox.GetMin().x > m_vMax.x)
			return false;

		if (m_vMin.y > rBox.GetMax().y || rBox.GetMin().y > m_vMax.y)
			return false;

		if (m_vMin.z > rBox.GetMax().z || rBox.GetMin().z > m_vMax.z)
			return false;

		return true;
	}

	//--------------------------------------------------------------
	// The return value is 'true' if there is overlap.  In this case the
	// intersection is stored in rOverlap.  If the return value is 'false',
	// if there is no overlap.  In this case rOverlap is undefined.
	//--------------------------------------------------------------
	bool AABB::Intersect( const AABB& rBox, AABB& rOverlap ) const
	{
		if( !Intersect( rBox ) )
			return false;

		//! x 축..
		if ( m_vMax.x <= rBox.m_vMax.x )
			rOverlap.m_vMax.x = m_vMax.x;
		else
			rOverlap.m_vMax.x = rBox.m_vMax.x;

		if ( m_vMin.x <= rBox.m_vMin.x )
			rOverlap.m_vMin.x = rBox.m_vMin.x;
		else
			rOverlap.m_vMin.x = m_vMin.x;

		//! y축
		if ( m_vMax.y <= rBox.m_vMax.y )
			rOverlap.m_vMax.y = m_vMax.y;
		else
			rOverlap.m_vMax.y = rBox.m_vMax.y;

		if ( m_vMin.y <= rBox.m_vMin.y )
			rOverlap.m_vMin.y = rBox.m_vMin.y;
		else
			rOverlap.m_vMin.y = m_vMin.y;

		//! z축
		if ( m_vMax.z <= rBox.m_vMax.z )
			rOverlap.m_vMax.z = m_vMax.z;
		else
			rOverlap.m_vMax.z = rBox.m_vMax.z;

		if ( m_vMin.z <= rBox.m_vMin.z )
			rOverlap.m_vMin.z = rBox.m_vMin.z;
		else
			rOverlap.m_vMin.z = m_vMin.z;

		return true;
	}

	//--------------------------------------------------------------
	/** Ray와 AABB와의 교차를 검출한다.
		@param vRayOrig : Ray의 시작점
		@param vRayDir	: Ray의 방향
		@param hitPoint	: Ray에서 교차되는 곳
		@return true/false	: 교차 여부

		참고: Real-Time Rending 2판 P630 Woo의 Ray-AABB Intersection
		http://tog.acm.org/GraphicsGems/gems/RayBox.c
	*/
	//--------------------------------------------------------------
	bool AABB::Intersect( const Math::Vector3& vRayOrig, const Math::Vector3& vRayDir, Math::Vector3* hitPoint )
	{
		bool inside = true;

		char quadrant[3];
		float candidatePlane[3];

		static const char RIGHT = 0;
		static const char LEFT = 1;
		static const char MIDDLE = 2;

		// 6개의 평면 중에 후보 3개의 평면을 찾는다.
		int i=0;
		for (i=0; i<3; ++i)
		{
			if (vRayOrig[i] < m_vMin[i])
			{
				quadrant[i] = LEFT;
				candidatePlane[i] = m_vMin[i];
				inside = false;
			}
			else if(vRayOrig[i] > m_vMax[i])
			{
				quadrant[i] = RIGHT;
				candidatePlane[i] = m_vMax[i];
				inside = false;
			}
			else
			{
				quadrant[i] = MIDDLE;
			}
		}

		// Ray Origin이 boundingBox의 안에 있다.
		if (inside)
		{
			*hitPoint = vRayOrig;
			return true;
		}

		Math::Vector3 maxT;

		// 후보 평면에서의 거리 T를 계산
		for (i=0; i<3; ++i)
		{
			if (quadrant[i] != MIDDLE && vRayDir[i] != 0.0f)
			{
				maxT[i] = (candidatePlane[i] - vRayOrig[i]) / vRayDir[i];
			}
			else
			{
				maxT[i] = -1.0f;
			}
		}

		// 교차점의 최종 선택을 위해, maxT들 중 가장 큰 값을 구한다.
		int whichPlane = 0;
		for (i=1; i<3; ++i)
		{
			if (maxT[whichPlane] < maxT[i])
				whichPlane = i;
		}

		// 실제 박스 안에 있는 최종 후보를 검출
		if (maxT[whichPlane] < 0.0f)
			return false;

		for (i=0; i<3; ++i)
		{
			if (whichPlane != i)
			{
				(*hitPoint)[i] = vRayOrig[i] + maxT[whichPlane] * vRayDir[i];
				if ((*hitPoint)[i] < m_vMin[i] || (*hitPoint)[i] > m_vMax[i])
					return false;
			}
			else
			{
				(*hitPoint)[i] = candidatePlane[i];
			}
		}

		return true;
	}
	bool AABB::Intersect( const Math::Vector3& vRayOrig, const Math::Vector3& vRayDir, float* hitDist )
	{
		Math::Plane sides[6] = { Math::Plane( 1, 0, 0,-m_vMin.x), Math::Plane(-1, 0, 0, m_vMax.x),
								 Math::Plane( 0, 1, 0,-m_vMin.y), Math::Plane( 0,-1, 0, m_vMax.y),
								 Math::Plane( 0, 0, 1,-m_vMin.z), Math::Plane( 0, 0,-1, m_vMax.z) };

		*hitDist = 0.0f;
		Math::Vector3 hitPoint = vRayOrig;

		bool inside = false;
		for (int i=0; (i<6) && !inside; ++i)
		{
			float cosTheta = sides[i].normal.Dot( vRayDir );
			float dist = sides[i].GetDistance( vRayOrig );

			if (ALMOST_ZERO(dist))	// 거리가 근사..
				return true;
			if (ALMOST_ZERO(cosTheta))	// 평행이면, skip..
				continue;

			*hitDist = -dist/cosTheta;
			if (*hitDist < 0.0f)
				continue;

			hitPoint = (*hitDist)*vRayDir + vRayOrig;

			inside = true;
			for (int j=0; (j<6) && inside; ++j)
			{
				if (j==i)
					continue;

				dist = sides[j].GetDistance( hitPoint );
				inside = ((dist + 0.00015) >= 0.f);
			}
		}

		return inside;
	}

	//--------------------------------------------------------------
	/** 평면과 축 정렬 경계 상자와의 교차 판정
		@remarks
			단일 상자의 변들이 그 평면을 통과하는지 아닌지를 알아본다. 즉, 만약 두 이웃
			정점들이 그 평면의 양쪽에 놓여 있다면, 교차가 있는 것이다.
			1. 상자의 중심을 통과하는 대각의 양쪽 끝에 놓여 있는 두 정점들에서만 평가한다.
			2. 대각선이 평면 법선과 가장 가깝게 정렬된다는 원리는 이용한다.

		@return true/false
			whichSideNdist == 0 이라면 교차
			whichSideNdist > 0 이라면 노말 방향으로의 떨어진 거리
			whichSideNdist < 0 이라면 노말의 반대 방향으로의 떨어진 거리
	*/
	//--------------------------------------------------------------
	bool AABB::Intersect( const Math::Plane& plane, float& whichSideNdist ) const
	{
		Math::Vector3 min, max;

		for (unsigned int i=0; i<3; ++i)
		{
			// 평면의 노말이 향하는 방향을 기준으로 최대, 최소값 결정
			if (plane.normal[i] >= 0.0f)
			{
				min[i] = m_vMin[i];
				max[i] = m_vMax[i];
			}
			else
			{
				min[i] = m_vMax[i];
				max[i] = m_vMin[i];
			}
		}

		/**			P
					|
			--------|-------- max
			|		|		|
			|		|		|
			|		| n		|
			|		|-->	|
			|		|		|
			|		|		|
		min	--------|--------
					|
		*/

		// 최소값이 평면의 양의 방향에 있다면, 상자가 양의 방향에 위치
		whichSideNdist = plane.GetDistance( min );
		if (whichSideNdist > 0.0f)
			return false;

		whichSideNdist = plane.GetDistance( max );

		// 최대값이 음의 방향에 있다면, 상자가 음의 방향에 위치
		if (whichSideNdist < 0.0f)
			return false;

		// 최소와 최대값이 서로 다른 방향에 있다면 교차
		whichSideNdist = 0.0f;
		return true;
	}

	//--------------------------------------------------------------
	ClipStatus::Type AABB::ClipStatus( const Matrix44& ViewProjection ) const
	{
		// @todo: needs optimization!
		int andFlags = 0xffff;
		int orFlags  = 0;

		// corner points
		Vector3 p[8];
		p[0] = m_vMin;
		p[1].Set(this->m_vMin.x, this->m_vMin.y, this->m_vMax.z);
		p[2].Set(this->m_vMax.x, this->m_vMin.y, this->m_vMax.z);
		p[3].Set(this->m_vMax.x, this->m_vMin.y, this->m_vMin.z);
		p[4].Set(this->m_vMin.x, this->m_vMax.y, this->m_vMin.z);
		p[5].Set(this->m_vMin.x, this->m_vMax.y, this->m_vMax.z);
		p[6].Set(this->m_vMax.x, this->m_vMax.y, this->m_vMin.z);
		p[7] = m_vMax;
	    
		// check each corner point
		Vector4 p1;
		int i;
		for (i = 0; i < 8; i++)
		{
			int clip = 0;
			p1.Multiply(p[i], ViewProjection);

			// @todo: vectorize compare operation!
			if (p1.x < -p1.w)		clip |= ClipLeft;
			else if (p1.x > p1.w)	clip |= ClipRight;
			if (p1.y < -p1.w)       clip |= ClipBottom;
			else if (p1.y > p1.w)   clip |= ClipTop;
			if (p1.z < -p1.w)       clip |= ClipFar;
			else if (p1.z > p1.w)   clip |= ClipNear;

			andFlags &= clip;
			orFlags  |= clip;
		}
		if (0 == orFlags)       return ClipStatus::Inside;
		else if (0 != andFlags) return ClipStatus::Outside;
		
		return ClipStatus::Clipped;
	}

} // namespace Math
} // namespace Cindy
