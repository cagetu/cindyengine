// Copyright (c) 2006~. cagetu
//
//******************************************************************
///	@mainpage	Cindy
///	@section intro 소개
///		- 소개 : Cindy Renderer 라이브러리
///	@section CREATEINFO 작성정보
///		- 작성자 : changhee
///		- e-mail : cagetu79@gmail.com
///		- blog : cagetu.egloos.com

#ifndef __CINDY_H__
#define __CINDY_H__

//==================================================================
// Window Define
//==================================================================
#ifndef _WIN32_WINNT
	#define	_WIN32_WINNT	0x0403
#endif

#ifndef _WIN32_WINDOWS		
	#define _WIN32_WINDOWS	0x0410 
#endif

//==================================================================
// Dll Define
//==================================================================
#ifdef CINDY_EXPORTS
	#define CN_DLL __declspec( dllexport )
#else
	#define CN_DLL __declspec( dllimport )
#endif

// d3d9 유니코드 Warning
#	pragma warning (disable : 4819 )
//=====================================

#include <exception>

// Common Include
#include <TCHAR.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
#include <mmsystem.h>
#include <mmreg.h>
#include <dsound.h>

#include <MoCommon/MoCommon.h>

//==================================================================
// Macro Define
//==================================================================
#ifndef SAFEDEL
	#define	SAFEDEL(x)		{	if(x){	delete (x);		(x) = NULL;		}	}
#endif
#ifndef SAFEDELS
	#define SAFEDELS(x)		{	if(x){	delete[] (x);	(x) = NULL;		}	}
#endif
#ifndef SAFEREL
	#define SAFEREL(x)		{	if(x){	(x)->Release();	(x) = NULL;		}	}
#endif

// Other Includes
#include "Util/CnStdDefines.h"
#include "CindyTypes.h"
//#include "Object/CnMemObject.h"

//! 작업 체크용 macro
//----------------------------------------------------------
// FIXMEs / TODOs / NOTE macros
//----------------------------------------------------------
#define _QUOTE(x)		# x 
#define QUOTE(x)		_QUOTE(x)
#define __FILE__LINE__	__FILE__ "(" QUOTE(__LINE__) ") : "

#define FILE_LINE	message( __FILE__LINE__ )

#define NOTE( x )	message( __FILE__LINE__" NOTE :   " #x "\n" ) 
#define TODO( x )	message( __FILE__LINE__" TODO :   " #x "\n" ) 
#define FIXME( x )  message( __FILE__LINE__" FIXME:   " #x "\n" ) 

namespace Cindy
{
	using namespace std;
	using namespace MoCommon;

	// Assert 
	#define Assert(a, b)		assert( a && b )

	// DX Debug용 
	#ifdef _DEBUG
		#define DX_ERROR_CHECK( msg )	\
			{\
				if( FAILED( ms_hResult ) ) \
				{\
					DXTrace( __FILE__, (DWORD)__LINE__, ms_hResult, #msg, TRUE ); \
					__asm	{int 3} \
				}\
			}

		#define DX_NULL_CHECK( value, msg )	\
			{\
				if( !value ) \
				{\
					DXTrace( __FILE__, (DWORD)__LINE__, 0, #msg, TRUE ); \
					__asm	{int 3}\
				}\
			}
	#else
		#define DX_ERROR_CHECK( msg )
		#define DX_NULL_CHECK( msg, hr )
	#endif
}

#endif	// __CINDY_H__