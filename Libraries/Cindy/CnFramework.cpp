//================================================================
// File:               : CnFramework.cpp
// Related Header File : CnFramework.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 23
//================================================================
#include "Cindy.h"
#include "CnFramework.h"
#include "CnCindy.h"

#include "Util/CnLog.h"
#include "Scene/CnSceneComponentImpl.h"

#include "Render/Dx9/CnDx9Renderer.h"
#include "Render/Dx9/CnDx9RenderWindow.h"

#include <MoCommon/Memory/MoMemoryManager.h>

namespace Cindy
{
	/// Const / Dest
	CnFramework::CnFramework()
		: m_bPaused(false)
		, m_pLogPtr(0)
	{
		MoCommon::MoMemoryManager::Initialize();
		m_pCindy = CnNew CnCindy();
		m_pLogPtr = new CnLogPtr();
	}

	CnFramework::~CnFramework()
	{
		Shutdown();

		SAFEDEL( m_pLogPtr );

		MoCommon::MoMemoryManager::OutputReport( L"MemoryLog.txt" );
		MoCommon::MoMemoryManager::Shutdown();
	}

	//================================================================
	/** Start
	    @remarks      프레임웍을 초기화한다.
		@param        none
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnFramework::Startup()
	{
		m_pCindy->Initialize();

		return false;
	}

	//================================================================
	/** Destroy
	    @remarks      프레임웍의 구성물을 삭제한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnFramework::Shutdown()
	{
		m_pSceneGraph.SetNull();
		SAFEDEL( m_pCindy );
	}

	//================================================================
	/** Run
	    @remarks      프레임웍 실행
		@param        none
		@return       none
	*/
	//================================================================
	void CnFramework::Run()
	{
		if( !m_bPaused )
		{
			m_pCindy->BeginFrame();
			{
				CnRenderer::Instance()->UpdateRenderTargets();
			}
			m_pCindy->EndFrame();
		}
	}

	//================================================================
	/** AttachWindow
	    @remarks      프레임웍을 초기화한다.
		@param        hWnd : 윈도우 핸들
		@param		  pStrTitle : 윈도우 타이틀
		@param		  nWidth : 윈도우 가로
		@param		  nHeight : 윈도우 세로
		@param		  usColorDepth : 칼라 비트
		@param		  usRefreshRate : 화면 주사율
		@param		  bFullScreen : 풀 스크린 모드
		@param		  bThreadSafe : 멀티 쓰레드 사용여부
		@param		  bSetCurrentTarget : 현재 윈도우를 사용할 것이냐..
		@return       none
	*/
	//================================================================
	CnRenderWindow* CnFramework::AttachWindow( HWND hWnd, const wchar* pStrTitle, int nWidth, int nHeight, ushort usColorDepth, 
												ushort usRefreshRate, bool bFullScreen, bool bThreadSafe, bool bSetCurrentTarget )
	{
		CnRenderWindow* pRenderWindow = CnRenderer::Instance()->CreateRenderWindow( hWnd, pStrTitle,
																					nWidth, nHeight, usColorDepth, usRefreshRate,
																					bFullScreen, bThreadSafe, bSetCurrentTarget );
		return pRenderWindow;
	}

	//================================================================
	/** Set Scene
	    @remarks      장면 설정
		@param        strName : 장면 이름
		@return       none
	*/
	//================================================================
	void CnFramework::SetScene( const CnString& strName )
	{
		// Setup Scene..
		m_pWorld = m_pCindy->CreateScene( L"Scene" );

		// 장면 그래프
		m_pSceneGraph = CnSceneGraph::NewObject();
		m_pSceneGraph->SetName( L"MainSceneGraph" );

		SceneComposite::UpdateScene* sceneUpdator = SceneComposite::UpdateScene::NewObject();
		sceneUpdator->Register( m_pSceneGraph );

		SceneComposite::RenderModel* modelRenderer = SceneComposite::RenderModel::NewObject();
		modelRenderer->Link( sceneUpdator );

		m_pWorld->Register( sceneUpdator );
		m_pWorld->Register( SceneComposite::BeginScene::NewObject() );
		m_pWorld->Register( modelRenderer );
		m_pWorld->Register( SceneComposite::EndScene::NewObject() );
	}

	//================================================================
	/** Pause
	    @remarks      멈춤 상태 설정
		@param        bPause : 멈충 여부 설정
		@return       none
	*/
	//================================================================
	void CnFramework::Pause( bool bPause )
	{
		m_bPaused = bPause;
	}

	//================================================================
	/** Is Paused ?
	    @remarks	  현재 멈춤 상태   
		@param        none
		@return       bool : m_bPaused
	*/
	//================================================================
	bool CnFramework::IsPaused() const
	{
		return m_bPaused;
	}

	//---------------------------------------------------------------------
	void CnFramework::SetLogFile( const MoString& strName, const MoString& strOutput, bool bOutputDebug, bool bOutputConsole )
	{
		m_pLogPtr->Add( strName, strOutput, bOutputDebug, bOutputConsole );
	}

	//================================================================
	/** Mesasge Proc
	    @remarks      메세지 프로시져 처리
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnFramework::MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
	{
		switch (message)
		{
		case WM_LBUTTONDOWN:
			{
			}
			break;
		case WM_LBUTTONUP:
			{
			}
			break;

		case WM_RBUTTONDOWN:
			{
			}
			break;

		case WM_RBUTTONUP:
			{
			}
			break;

		case WM_MBUTTONDOWN:
			{
			}
			break;
		case WM_MBUTTONUP:
			{
			}
			break;

		case WM_MOUSEMOVE:
			{
			}
			break;

		case WM_MOUSEWHEEL:
			{
			}
			break;
		}

		return false;
	}
}