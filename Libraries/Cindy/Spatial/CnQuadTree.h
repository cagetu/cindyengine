// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

#include "../Math/CnAxisAlignedBox.h"

namespace Cindy
{
	class CnSceneNode;

	//==================================================================
	/** Quad Tree
		@author			cagetu
		@since			2008년 2월 28일
		@remarks		Quad-Tree
						Quad-Tree는 자식으로 4개의 노드로 분할된다.
	*/
	//==================================================================
	class CnQuadTree
	{
	public:
		CnQuadTree();
		~CnQuadTree();

		void		Initialize( ushort nDepth, const Math::AABB& Box );

		int			GetNumNodes( ushort nLevel ) const;
		int			GetNodeID( ushort nLevel, ushort nCol, ushort nRow ) const;

		void		Add( CnSceneNode* pSceneNode );
		void		Remove( const CnString& strName );
		void		RemoveAll();

	protected:
		typedef HashMap<CnString, CnSceneNode*>		SceneMap;
		typedef SceneMap::iterator					SceneIter;

		//static const int	QUAD = 4;

		// QuadtreeNode
		// QuaddtreeNode는 자식의 노드 4개를 가진다.
		class Node
		{
			friend class CnQuadTree;
		private:
			Node*		children[4];

			Math::Vector3	center;
			float		halfSize;

			ushort		col;
			ushort		row;
			ushort		level;

			Math::AABB	boundBox;

			SceneMap	scenes;
		public:
			Node();
			~Node();

			void	Initialize( CnQuadTree* pTree, ushort nLevel, ushort nCol, ushort nRow );
		};

		Node*		m_pRootNode;		// 최상위 Quadtree Node

		ushort		m_nDepth;
		Math::AABB	m_BoundingBox;

		Node*		m_aNodes;
	};

#include "CnQuadTree.inl"
}