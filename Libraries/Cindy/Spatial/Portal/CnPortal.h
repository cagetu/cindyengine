//================================================================
// File:           : CnPortal.h
// Original Author : changhee
// Creation Date   : 2008. 3. 4
//================================================================
#pragma once

#include "../../Math/CnVector3.h"

namespace Cindy
{
	class CnCamera;
	class CnFrustum;

	//==================================================================
	/** Portal Class
		@author			cagetu
		@since			2008년 3월 4일
		@remarks		포탈 클래스
	*/
	//==================================================================
	class CnPortal
	{
	private:
		ushort		m_usNumVertices;
		CnVector3*	m_pModelVertices;
		CnVector3*	m_pWorldVertices;

	public:
		CnPortal( ushort usNumVertices, const CnVector3* pVertices );
		~CnPortal();

		bool	Update( const CnCamera& Camera, CnFrustum& Frustum );
	};
}