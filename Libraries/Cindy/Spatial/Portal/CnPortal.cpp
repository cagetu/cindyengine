//================================================================
// File:               : CnPortal.cpp
// Related Header File : CnPortal.h
// Original Author     : changhee
// Creation Date       : 2008. 3. 4
//================================================================
#include "CnPortal.h"
#include "../CnCamera.h"
#include "../../Math/CnFrustum.h"

namespace Cindy
{
	CnPortal::CnPortal( ushort usNumVertices, const CnVector3* pVertices )
	{
		m_usNumVertices = usNumVertices;
		m_pModelVertices = new CnVector3[usNumVertices];
		m_pWorldVertices = new CnVector3[usNumVertices];

		memcpy( m_pModelVertices, pVertices, usNumVertices );
	}
	CnPortal::~CnPortal()
	{
		SAFEDELS( m_pWorldVertices );
		SAFEDELS( m_pModelVertices );
	}

	//--------------------------------------------------------------
	bool CnPortal::Update( const CnCamera& Camera, CnFrustum& Frustum )
	{
		assert( m_usNumVertices >=3 );
		if (m_usNumVertices < 3)
			return false;

		// 1. 포탈의 방향과 카메라의 방향이 일치하는지를 비교해야 한다.
		//	  포탈은 카메라를 바라보는 방향으로 되어 있어야 한다.
		CnVector3 eyeToPoly = Camera.GetPosition() - m_pWorldVertices[0];
		CnVector3 edge10 = m_pWorldVertices[1] - m_pWorldVertices[0];
		CnVector3 edge20 = m_pWorldVertices[2] - m_pWorldVertices[0];
		CnVector3 cross;
		cross.UnitCross( edge10, edge20 );

		float order = eyeToPoly.Dot( cross );
		if (order<0.0f)
			return false;

		// 2. 
		if (Camera.IsPerspective())
		{
		}
		else
		{
		}

		return false;
	}

}