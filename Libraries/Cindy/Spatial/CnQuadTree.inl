// Copyright (c) 2006~. cagetu
//
//******************************************************************
inline
int CnQuadTree::GetNumNodes( ushort nLevel ) const
{
    return 0x55555555 & ((1 << nLevel * 2) - 1);
}
inline
int CnQuadTree::GetNodeID( ushort nLevel, ushort nCol, ushort nRow ) const
{
	return GetNumNodes( nLevel ) + (nRow << nLevel) + nCol;
}