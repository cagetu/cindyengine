// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnBspNode.h"

namespace Cindy
{
	//================================================================
	//	BspNode::Polygon Struct
	//================================================================
	CnBspNode::Polygon::Polygon()
		: selected(false)
	{
	}
	CnBspNode::Polygon::Polygon( bool bSelected, const CnVector3& v0, const CnVector3& v1, const CnVector3& v2, const CnVector3& n )
		: selected(bSelected)
	{
		vertices[0] = v0;
		vertices[1] = v1;
		vertices[2] = v2;
		normal = n;
	}

	//================================================================
	//	BspNode Class
	//================================================================
	CnBspNode::CnBspNode()
		: m_pFront(0)
		, m_pBack(0)
	{
	}
	CnBspNode::CnBspNode( CnBspNode::NodeType eType )
		: m_eType(eType)
		, m_pFront(0)
		, m_pBack(0)
	{
	}

	//--------------------------------------------------------------
	void CnBspNode::Build( const CnBspNode::Polygon* paPolygons, int nNumPolygons )
	{
		SAFEDEL( m_pFront );
		SAFEDEL( m_pBack );

		// 더 이상 Partition Plane으로 쓸 polygon이 없음
		if (GetSelectablePartition(paPolygons, nNumPolygons) == nNumPolygons)
		{
			if (nNumPolygons)
			{	// 폴리곤이 존재
				m_eType = Leaf;
			}
			else
			{	// 폴리곤이 없으면, Solid-Leaf
				m_eType = SolidLeaf;
			}
			return;
		}

		// Partition Plane으로 쓸 Polygon이 존재

	}

	//--------------------------------------------------------------
	int CnBspNode::GetSelectablePartition( const CnBspNode::Polygon* paPolygons, int nNumPolygons ) const
	{
		int curPoly = 0;
		for (curPoly=0; curPoly<nNumPolygons; ++curPoly)
		{
			if (false==paPolygons[curPoly].selected)
				return curPoly;
		}
		return curPoly;
	}

	//--------------------------------------------------------------
}