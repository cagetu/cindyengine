// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

#include "../../Math/CnPlane.h"

namespace Cindy
{
	//==================================================================
	/** Bsp Node
		@author			cagetu
		@since			2008년 2월 28일
		@remarks		Bsp Node 클래스
						2진 트리의 하나의 노드
	*/
	//==================================================================
	class CnBspNode
	{
		enum NodeType
		{
			SolidLeaf = 0,
			Leaf,
			Splitter,
		};

		struct Polygon
		{
			CnVector3	vertices[3];
			CnVector3	normal;
			bool		selected;

			Polygon();
			Polygon( bool bSelected, const CnVector3& v0, const CnVector3& v1, const CnVector3& v2, const CnVector3& n );
		};

		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		NodeType		m_eType;
		CnBspNode*		m_pFront;
		CnBspNode*		m_pBack;

		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		int		GetSelectablePartition( const Polygon* paPolygons, int nNumPolygons ) const;

	public:
		CnBspNode();
		CnBspNode( NodeType eType );

		void		Build( const Polygon* paPolygons, int nNumPolygons );
	};
}