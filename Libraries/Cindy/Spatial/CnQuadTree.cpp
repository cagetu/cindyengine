// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnQuadTree.h"
#include "../Scene/CnSceneNode.h"

namespace Cindy
{
	//================================================================
	//	QuadTree::Node Class
	//================================================================
	CnQuadTree::Node::Node()
	{
		memset( children, 0, sizeof(Node)*4 );
	}
	CnQuadTree::Node::~Node()
	{
		for (int i=0; i<4; ++i)
			delete children[i];
	}

	//----------------------------------------------------------------
	void CnQuadTree::Node::Initialize( CnQuadTree* pTree, ushort nLevel, ushort nCol, ushort nRow )
	{
		level = nLevel;
		col = nCol;
		row = nRow;

		unsigned char childLevel = level + 1;
		if (childLevel < pTree->m_nDepth)
		{
			ushort childCol, childRow;
			int childNodeID = -1;

			for (int i=0; i<4; ++i)
			{
				childCol = 2 * col + (i&1);
				childRow = 2 * row + ((i&2)>>1);

				childNodeID = pTree->GetNodeID( childLevel, childCol, childRow );
				children[i] = &(pTree->m_aNodes[childNodeID]);

				children[i]->Initialize( pTree, childLevel, childCol, childRow );
			}
		}
	}

	//================================================================
	//	QuadTree Class
	//================================================================
	CnQuadTree::CnQuadTree()
		: m_pRootNode(0)
	{
	}
	CnQuadTree::~CnQuadTree()
	{
		m_pRootNode = 0;
		SAFEDEL( m_aNodes );
	}

	//----------------------------------------------------------------
	/** Initialize
		@remarks	QuadTree의 Cell들을 분할한다.
	*/
	//----------------------------------------------------------------
	void CnQuadTree::Initialize( ushort nDepth, const Math::AABB& Box )
	{
		m_nDepth = nDepth;
		m_BoundingBox = Box;

		int baseDimemsion = 1 << (nDepth-1);

		int numNodes = GetNumNodes(nDepth);
		m_aNodes = new Node[numNodes];

		m_pRootNode = &m_aNodes[0];

		m_pRootNode->Initialize( this, 0, 0, 0 );
	}

	//----------------------------------------------------------------
	void CnQuadTree::Add( CnSceneNode* pSceneNode )
	{
		Math::AABB aabb;
		pSceneNode->GetBound( &aabb );
	}
	void CnQuadTree::Remove( const CnString& strName )
	{
	}
	void CnQuadTree::RemoveAll()
	{
	}
}