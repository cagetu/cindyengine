//================================================================
// File:           : CnCindy.h
// Original Author : changhee
// Creation Date   : 2007. 1. 24
//================================================================
#ifndef __CN_CINDY_H__
#define __CN_CINDY_H__

#include "Graphics/CindyRenderSystem.h"
#include "Foundation/CnMemObject.h"
#include "Frame/CnFrameManager.h"
#include "Scene/CnScene.h"
#include "Scene/CnCamera.h"
#include "Scene/CnSceneGraph.h"
#include "Util/CnUnCopyable.h"
#include "Util/CnSingleton.h"
#include "Scene/Component/CnSceneComponentManager.h"
#include "Scene/CnVisibleResolver.h"
#include "Lighting/ShadowMap/CnShadowMapSystem.h"

namespace Cindy
{
	class CnRenderer;

	//==================================================================
	/** CnCindy
		@author			cagetu
		@since			2006년 10월 12일
		@remarks		Cindy 라이브러리의 EntryPoint...
		
		09/08/11 cagetu : 이 녀석이 Scene, Camera, SceneGraph의 관리자 기능까지...
	*/
	//==================================================================
	class CN_DLL CnCindy : public CnMemLeakObject
	{
		__DeclareUnCopy(CnCindy);
		__DeclareSingleton(CnCindy);
	public:
		CnCindy();
		~CnCindy();

		// 초기화 / 종료
		void					Open();
		void					Close();

		// Camera
		void					AddCamera( const Ptr<CnCamera>& Camera );
		void					RemoveCamera( const wchar* strName );
		Ptr<CnCamera>			GetCamera( const wchar* strName ) const;
		const Ptr<CnCamera>&	GetDefaultCamera() const;

		// Scene
		Ptr<CnScene>			AddScene( const wchar* strName );
		void					RemoveScene( const wchar* strName );
		Ptr<CnScene>			GetScene( const wchar* strName ) const;

		// SceneGraph
		Ptr<CnSceneGraph>		AddSceneGraph( const wchar* strName );
		void					RemoveSceneGraph( const wchar* strName );
		Ptr<CnSceneGraph>		GetSceneGraph( const wchar* strName ) const;

	private:
		typedef HashMap<CnString, Ptr<CnCamera> >		CameraMap;
		typedef HashMap<CnString, Ptr<CnScene> >		SceneMap;
		typedef HashMap<CnString, Ptr<CnSceneGraph> >	SceneGraphMap;

		//--------------------------------------------------------------
		// Variables
		//--------------------------------------------------------------
		CnRenderer*			m_pRenderer;

		// 
		CameraMap			m_Cameras;
		Ptr<CnCamera>		m_DefaultCamera;

		SceneMap			m_Scenes;
		SceneGraphMap		m_SceneGraphs;

		//CnSceneManager			m_SceneManager;
		CnVisibleResolver		m_VisibleResolver;		
		Scene::ComponentManager	m_SceneComponentManager;
		CnShadowMapSystem		m_ShadowMapSystem;
	};

	//--------------------------------------------------------------
	/**
	*/
	inline
	const Ptr<CnCamera>& CnCindy::GetDefaultCamera() const
	{
		return m_DefaultCamera;
	}
}

#endif	// __CN_CINDY_H__