//================================================================
// File:           : CnFramework.h
// Original Author : changhee
// Creation Date   : 2007. 1. 23
//================================================================
#ifndef __CN_FRAMEWORK_H__
#define __CN_FRAMEWORK_H__

#include "Scene/CindyScene.h"
#include "CnCindy.h"
//#include "Foundation/CnProperty.h"

namespace Cindy
{
	class CnWindow;
	class CnRenderWindow;
	class CnLogPtr;

	//================================================================
	/** Framework Class
	    @author		changhee
		@since      2007. 1. 23
		@remarks	프레임 웍 클래스
	*/
	//================================================================
	class CN_DLL CnFramework
	{
	public:
		CnCindy*		m_pCindy;

		CnScene*		m_pWorld;
		SceneGraphPtr	m_pSceneGraph;
	
		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		CnFramework();
		virtual ~CnFramework();

		// Common
		bool					Startup();
		void					Shutdown();

		virtual void			Run();

		// Initialize
		virtual bool			Initialize() abstract;
		virtual void			DeInitialize() abstract;

		// Window
		virtual CnRenderWindow*	AttachWindow( HWND hWnd, const wchar* pStrTitle,
											  int nWidth, int nHeight, ushort usColorDepth, 
											  ushort usRefreshRate, bool bFullScreen, bool bThreadSafe,
											  bool bSetCurrentTarget = false );

		// Pause
		void					Pause( bool bPause = true );
		bool					IsPaused() const;

		// Log
		virtual void			SetLogFile( const MoString& strName, const MoString& strOutput, bool bOutputDebug, bool bOutputConsole );

		// MsgProc
		virtual bool			MsgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

	protected:
		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		bool			m_bPaused;
		CnLogPtr*		m_pLogPtr;

		// Rotation
		short			m_nRotMouseX;
		short			m_nRotMouseY;

		// Zoom
		short			m_nZoomMouseX;
		short			m_nZoomMouseY;

		//--------------------------------------------------------------
		//--------------------------------------------------------------
		CnFramework( const CnFramework& );
		CnFramework& operator = ( const CnFramework& );

		virtual void	SetScene( const CnString& strName );
	};
}

#endif	// __CN_FRAMEWORK_H__