//================================================================
// File:               : CnEventHandler.inl
// Related Header File : CnEventHandler.h
// Original Author     : changhee
//================================================================
namespace Cindy
{
	//-------------------------------------------------------------------------
	/** @brief	이벤트 처리 함수 등록 */
	//-------------------------------------------------------------------------
	template <class T, class EventT>
	void CnEventHandler::RegisterMethod( T* pObject, void (T::*Method)(EventT*) )
	{
		//m_Functions[TypeInfo(typeid(EventT))] = new MethodHandler<T, EventT>(pObject, Method);
		m_Functions[EventT::RTTI->GetName()] = new Event::MethodHandler<T, EventT>(pObject, Method);
	}

	//-------------------------------------------------------------------------
	/** @brief	비어 있는고? */
	//-------------------------------------------------------------------------
	inline
	bool CnEventHandler::IsEmpty() const
	{
		return m_Functions.empty();
	}
}