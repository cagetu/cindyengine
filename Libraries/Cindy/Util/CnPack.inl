// Copyright (c) 2006~. cagetu
//
//******************************************************************

namespace Cindy
{
	//---------------------------------------------------------------
	/** @desc	32bit 실수 -> 16bit 정수로 변환
				3.13-fixed point
	*/
	//---------------------------------------------------------------
	inline
	float16 f32Tof16( float32 Value )
	{
		return (short)(Value * 16384);
	}
	//---------------------------------------------------------------
	/** @desc	16bit 정수 -> 32bit 실수로 변환 */
	//---------------------------------------------------------------
	inline
	float32 f16Tof32( float16 Value )
	{
		return (float32)(Value / 16384.0f);
	}

	//---------------------------------------------------------------
	/** @desc	32bit 실수 -> 16bit 정수로 변환
				http://www.gamecode.org/bak/article.php3?no=2228&page=0&current=0&field=tip
	*/
	//---------------------------------------------------------------
	inline
	uint16 f32_16( float32 t )
	{
		int s = ((*((unsigned long*)&t) & 0x80000000) >> 31); 
		int e = ((*((unsigned long*)&t) & 0x7F800000) >> 23) - 127; 
		int m = (*((unsigned long*)&t) & 0x007FFFFF); 

		if (e < -7) 
			return 0; 

		return (s << 15) | ((e + 7) << 11) | (m >> (23 - 11)); 
	}
	//---------------------------------------------------------------
	/** @desc	16bit 정수 -> 32bit 실수로 변환 */
	//---------------------------------------------------------------
	inline
	float32 f16_32( uint16 t )
	{
		int s = ((t & 0x8000) >> 15); 
		int e = ((t & 0x7800) >> 11) - 7; 
		int m = (t & 0x007FF); 

		if (t == 0) 
			return 0.0f; 

		unsigned long f = (s << 31) | ((e + 127) << 23) | (m << (23 - 11)); 
		return *((float*)&f); 
	}

	//---------------------------------------------------------------
	/** Helper method to go from a float to packed char
		unsigned char ConvertChar(float value)
		{
		  //Scale and bias
		  value = (value + 1.0f) * 0.5f;
		  return (unsigned char)(value*255.0f);
		}
	*/
	//---------------------------------------------------------------
	inline
	uint8 f32_u8( float32 t )
	{
		// Scale and bias
		t = (t+1.0f) * 0.5f;

		return (uint8)(t*255.0f);
	}
}