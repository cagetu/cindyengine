//================================================================
// File:				: CnConsole.cpp
// Related Header File	: CnConsole.h
// Original Author		: changhee
// Creation Date		: 2007. 8. 29
//================================================================
#include "../Cindy.h"
#include "CnConsole.h"

#include <MoCommon/MoStringUtil.h>

namespace Cindy
{
	//----------------------------------------------------------------
	CnConsole::CnConsole()
		: m_bIsCreated(false)
		, m_hHandle(0)
	{
	}
	CnConsole::~CnConsole()
	{
	}

	//----------------------------------------------------------------
	bool CnConsole::Create()
	{
		if (!m_bIsCreated)
		{
			::AllocConsole();

			m_hHandle = ::GetStdHandle( STD_OUTPUT_HANDLE );
			m_bIsCreated = m_hHandle != NULL;
		}

		return m_bIsCreated;
	}

	//----------------------------------------------------------------
	void CnConsole::Destroy()
	{
		if (m_bIsCreated)
		{
			m_bIsCreated = false;
			::FreeConsole();
		}
	}

	//----------------------------------------------------------------
	void CnConsole::Clear()
	{
		if (m_bIsCreated)
		{
			CONSOLE_SCREEN_BUFFER_INFO scInfo;
			COORD pos = {0, 0};
			unsigned long written;

			if (::GetConsoleScreenBufferInfo( m_hHandle, &scInfo ))
			{
				::FillConsoleOutputCharacter( m_hHandle, ' ', scInfo.dwSize.X * scInfo.dwSize.Y, pos, &written );
			}
		}
	}

	//----------------------------------------------------------------
	void CnConsole::Locate( int nX, int nY )
	{
		if (m_bIsCreated)
		{
			COORD pos = { (short)nX, (short)nY };

			::SetConsoleCursorPosition( m_hHandle, pos );
		}
	}

	//----------------------------------------------------------------
	void CnConsole::Print( const CnString& strMsg )
	{
		if (m_bIsCreated)
		{
			static CnString string;
			static char buffer[1024];
			static unsigned long writesize;

			string = strMsg + L"\n";
			MoStringUtil::WidecharToMultibyte( string.c_str(), buffer );
			//unicode::ConvertWCharToChar( string, buffer );
			::WriteFile( m_hHandle, buffer, (unsigned long)strlen(buffer), &writesize, 0 );
		}
	}
	void CnConsole::Print( int nX, int nY, const CnString& strMsg )
	{
		if (m_bIsCreated)
		{
			static CnString string;
			static char buffer[1024];
			static unsigned long writesize;

			Locate( nX, nY );

			string = strMsg + L"\n";
			//unicode::ConvertWCharToChar( string, buffer );
			MoStringUtil::WidecharToMultibyte( string.c_str(), buffer );
			::WriteFile( m_hHandle, buffer, (unsigned long)strlen(buffer), &writesize, 0 );
		}
	}

	//----------------------------------------------------------------
	void CnConsole::SetColor( unsigned short usAttributes )
	{
		if (m_bIsCreated)
		{
			::SetConsoleTextAttribute( m_hHandle, usAttributes );
		}
	}

	//----------------------------------------------------------------
	void CnConsole::SetCursor( bool bOn )
	{
		if (m_bIsCreated)
		{
			CONSOLE_CURSOR_INFO info;

			::GetConsoleCursorInfo( m_hHandle, &info );

			info.bVisible = bOn;

			::GetConsoleCursorInfo( m_hHandle, &info );
		}
	}
}