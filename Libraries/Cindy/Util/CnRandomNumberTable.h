//================================================================
// File:           : CnRandomNumberTable.h
// Original Author : changhee
// Creation Date   : 2007. 1. 24
//================================================================
#ifndef __CN_RANDOMNUMBERTABLE_H__
#define __CN_RANDOMNUMBERTABLE_H__

namespace Cindy
{
	//------------------------------------------------------------------------------
	/**
		@class	RandomNumberTable
		@brief	테이블 기반 random-number 생성기. 주어진 키에 대해서는 항상 같은 랜덤값을 제공한다.
	    
		@see	Nebula3 - RandomNumberTable
	*/
	//------------------------------------------------------------------------------
	class CN_DLL CnRandomNumberTable
	{
	public:
		/// return a pseudo-random number between 0 and 1
		static float Rand(unsigned int key);
		/// return a pseudo random number between min and max
		static float Rand(unsigned int key, float minVal, float maxVal);

	private:
		static const unsigned int tableSize = 2048;
		static const float randTable[tableSize];
	};

	//------------------------------------------------------------------------------
	/**
	*/
	inline float
	CnRandomNumberTable::Rand(unsigned int key)
	{
		return randTable[key % tableSize];
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline float
	CnRandomNumberTable::Rand(unsigned int key, float minVal, float maxVal)
	{
		return minVal + (randTable[key % tableSize] * (maxVal - minVal));
	}

} // namespace Cindy
//------------------------------------------------------------------------------
#endif	// __CN_RANDOMNUMBERTABLE_H__
