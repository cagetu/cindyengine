//================================================================
// File:           : CnColor.h
// Original Author : changhee
// Creation Date   : 2007. 1. 29
//================================================================
#ifndef __CN_COLOR_H__
#define __CN_COLOR_H__

#include "../Cindy.h"

namespace Cindy
{
	//================================================================
	/** Color
	    @author    changhee
		@since     2007. 1. 29
		@remarks   r, g, b, a
	*/
	//================================================================
	class CN_DLL CnColor
	{
	public:
		static const CnColor	WHITE;
		static const CnColor	BLACK;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		float		r;
		float		g;
		float		b;
		float		a;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnColor( float fR = 1.0f, float fG = 1.0f, float fB = 1.0f, float fA = 1.0f );
		CnColor( RGBA color );

		bool operator == ( const CnColor& color ) const;
		bool operator != ( const CnColor& color ) const;

		CnColor operator + ( const CnColor& color ) const;
		CnColor operator - ( const CnColor& color ) const;
		CnColor operator * ( const CnColor& color ) const;
		CnColor operator * ( const float fValue ) const;
		CnColor operator / ( const CnColor& color ) const;
		CnColor operator / ( const float fValue ) const;

		CnColor operator -() const;
		CN_DLL friend CnColor operator * ( float fValue, const CnColor& color );

		CnColor& operator += ( const CnColor& color );
		CnColor& operator -= ( const CnColor& color );
		CnColor& operator *= ( const CnColor& color );
		CnColor& operator *= ( const float fValue );
		CnColor& operator /= ( const CnColor& color );
		CnColor& operator /= ( const float fValue );

		void	Clamp();
		void	Scale();

		void	SetRGBA( RGBA color );
		RGBA	GetRGBA() const;

		void	Lerp( const CnColor& c1, const CnColor& c2, float s );
	};

#include "CnColor.inl"
}

#endif	// __CN_COLOR_H__