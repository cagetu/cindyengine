#ifndef __CN_GUID_H__
#define __CN_GUID_H__

#include "../Cindy.h"

namespace Cindy
{
	//-----------------------------------------------------------------------------
	/**	Guid
		@author		cagetu

		@brief		전역 고유 식별자
					GUID는 16바이트, 즉 128비트.

					http://ko.wikipedia.org/wiki/GUID
					Nebula3 : Util/Guid
	*/
	//-----------------------------------------------------------------------------
	class Guid
	{
	public:
		Guid();
		Guid( const Guid& rhs );
		Guid( const unsigned char* ptr, uint size );

		void operator = (const Guid& rhs);
		void operator = (const CnString& rhs);

		bool operator == (const Guid& rhs) const;
		bool operator != (const Guid& rhs) const;
		bool operator < (const Guid& rhs) const;
		bool operator <= (const Guid& rhs) const;
		bool operator > (const Guid& rhs) const;
		bool operator >= (const Guid& rhs) const;

		bool IsValid() const;

		void Generate();

		CnString AsString() const;
		uint AsBinary(const unsigned char*& outPtr ) const;

		uint HashCode() const;
		
		static Guid	FromString( const CnString& str );
		static Guid FromBinary( const unsigned char* ptr, uint numBytes );
	private:
		UUID	uuid_;
	};
}

#endif	// __CN_GUID_H__