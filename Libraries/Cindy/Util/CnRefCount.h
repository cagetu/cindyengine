#ifndef __CNREFCOUNT_H__
#define __CNREFCOUNT_H__

#include "../Threading/CnInterlocked.h"

namespace Cindy
{
	//==================================================================
	/** RefCounter
		@author		cagetu
		@brief		참조 카운터
		@see		http://www.gpgstudy.com/forum/viewtopic.php?t=9089
	*/
	//==================================================================
	class CN_DLL CnRefCount
	{
	public:
		CnRefCount();

		// SmartPointer System
		void		AddRef();
		bool		Release();

		uint		GetReferenceCount() const;
	protected:
		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		virtual ~CnRefCount();

	private:
		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		volatile int	m_References;				//!< 참조 카운트
	};

#include "CnRefCount.inl"
}

#endif	// __CNREFCOUNT_H__