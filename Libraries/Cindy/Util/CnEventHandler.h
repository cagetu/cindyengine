#pragma once

#include "CnEventFunctionHandler.h"

namespace Cindy
{
	//-----------------------------------------------------------------------------
	/**	@class		Event Handler
		@author		cagetu

		@brief		이벤트(메시지)를 처리하기 위한 처리기이다.
					이벤트를 처리할 함수를 등록하고, 이벤트를 처리한다.

					http://www.swallowstudio.com/wiki/doku.php?id=technicalarticles%3Aeffectiveeventhandlingincpp
	*/
	//-----------------------------------------------------------------------------
	class CN_DLL CnEventHandler : public CnObject
	{
	public:
		CnEventHandler();
		virtual ~CnEventHandler();

		// 이벤트 처리 함수 등록
		template <class T, class EventT>
		void	RegisterMethod( T*, void (T::*Method)(EventT*) );

		// Message
		virtual void	Handle( IEvent* );

		// 유효성 판정
		virtual bool	IsValid( IEvent* pEvent );
		virtual bool	IsEmpty() const;

	private:
		// HandlerFunctions
		typedef HashMap<const std::string, Event::FuncHandler*>	FunctionList;
		FunctionList	m_Functions;
	};

	typedef Pointer<CnEventHandler>	EventHandlerPtr;
}

#include "CnEventHandler.inl"