//================================================================
// File:               : CnEvent.cpp
// Related Header File : CnEvent.h
// Original Author     : changhee
//================================================================
#include "CnEvent.h"
#include "CnEventHandler.h"

namespace Cindy
{
	//-------------------------------------------------------------------------
	CN_IMPLEMENT_RTTI(Cindy, IEvent, CnObject);
	//-------------------------------------------------------------------------
	//Destruct
	IEvent::~IEvent()
	{
	}

	////-------------------------------------------------------------------------
	//void Event::SendSync( Handler* pHandler )
	//{
	//	pHandler->HandleEvent( this );
	//}
	////-------------------------------------------------------------------------
	//void Event::SendASync( Handler* pHandler )
	//{
	//	pHandler->Put( this );
	//}
}