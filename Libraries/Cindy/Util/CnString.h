//================================================================
// File					: CnString.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 18.
//================================================================
#ifndef	__CN_STRING_H__
#define	__CN_STRING_H__

#include "../Cindy.h"

namespace Cindy
{
	//==================================================================
	// String Define
	//==================================================================
	#define CN_MAX_NAME_LENGTH 256		//!< 이름명의 최대길이를 256바이트로 한다.
	#define CN_MAX_PATH_LENGTH 1024		//!< 경로명의 최대길이를 1024바이트로 한다.
	#define CN_MAX_STRING_LENGTH 1024	//!< 스트링으로 읽고 쓸수 있는 최대 버퍼 크기

	//! CnStringArray - String을 리스트로 관리할 경우 사용
	typedef std::vector<CnString>		CnStringArray;
	typedef std::vector<std::string>	StringArray;

	//==================================================================
	/** String Utility Class
		@author	
			Kwan
		@since
			2004. 9. 20.
		@desc
			문자열에서 유용하게 쓸 수 있는 기능들을 묶어 놓은 클래스이다. 
			문자열에 적용되는 유틸리티이므로 모든 함수가 static이다.
	*/
	//==================================================================
	namespace unicode
	{
		//static const CnString BLANK;					//!< 빈 문자열.

		void			Trim( CnString& str );
		void			Trim( CnString& str, const CnString& strDel );

		void			LowerCase( CnString& str );
		void			UpperCase( CnString& str );

		CnStringArray	Tokenize( const CnString& str, const CnString& strDels = L" \t\n", ushort usMaxSplit = 0 );
		StringArray		Tokenize( const std::string& str, const std::string& strDels = " \t\n", ushort usMaxSplit = 0 );

		void			SplitPath( const CnString& strPath, CnString& strDrive, CnString& strDir, CnString& strFileName, CnString& strExt );
		CnString		SplitPathFileName( const CnString& strPath );
		CnString		SplitPathExt( const CnString& strPath );
		CnString		SplitPathDir( const CnString& strPath );
		CnString		SplitPathDrive( const CnString& strPath );
		CnString		SplitPathFullFileName( const CnString& strPath );

		CnString		ToString( const int nValue );
		CnString		ToString( const float fValue );
		CnString		ToString( const wchar* pFormat, ... );
		float			ToFloat( const CnString& str );
		int				ToInt( const CnString& str );
		bool			ToBool( const CnString& str );

		void			CharToWChar( const char* pString, CnString& rResult );
		void			WCharToChar( const CnString& strString, char* pResult );

		ulong			GetHashCode( const char* pString );
		ulong			GetHashCode( const wchar* pWString );

		bool			Compare( const wchar* src, const wchar* dst );
	};

	#define ToStr	unicode::ToString

} // end of namespace Cindy

#endif // __CN_STRING_H__