//================================================================
// File:               : CnColor.cpp
// Related Header File : CnColor.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 29
//================================================================
#include "CnColor.h"

namespace Cindy
{
	//================================================================
	/// define Static Variables..
	const CnColor CnColor::WHITE( 1.0f, 1.0f, 1.0f, 1.0f );
	const CnColor CnColor::BLACK( 0.0f, 0.0f, 0.0f, 0.0f );

	//-------------------------------------------------------------------------
	void CnColor::Clamp()
	{
		if( r > 1.0f )
			r = 1.0f;
		else if( r < 0.0f )
			r = 0.0f;

		if( g > 1.0f )
			g = 1.0f;
		else if( g < 0.0f )
			g = 0.0f;

		if( b > 1.0f )
			b = 1.0f;
		else if( b < 0.0f )
			b = 0.0f;

		if( a > 1.0f )
			a = 1.0f;
		else if( a < 0.0f )
			a = 0.0f;
	}

	//-------------------------------------------------------------------------
	void CnColor::Scale()
	{
		// Scale down by maximum component (preserves the final color).
		float fMax = r;
		if( g > fMax )
			fMax = g;
		if( b > fMax )
			fMax = b;

		if ( fMax > 1.0f )
		{
			float fInvMax = 1.0f/fMax;
			r *= fInvMax;
			g *= fInvMax;
			b *= fInvMax;
		}

		if ( a > 1.0f )
			a = 1.0f;
	}

	//----------------------------------------------------------------
	void CnColor::SetRGBA( RGBA color )
	{
        a = static_cast<uchar>(color >> 24) / 255.0f;
        r = static_cast<uchar>(color >> 16) / 255.0f;
        g = static_cast<uchar>(color >> 8) / 255.0f;
        b = static_cast<uchar>(color) / 255.0f;
	}

	//----------------------------------------------------------------
	RGBA CnColor::GetRGBA() const
	{
		// Ensure ambient lights are capped to 1.0f.
		unsigned int uiA = (unsigned int)(a * 255.0f);
		if (uiA > 255)
			uiA = 255;
		unsigned int uiR = (unsigned int)(r * 255.0f);
		if (uiR > 255)
			uiR = 255;
		unsigned int uiG = (unsigned int)(g * 255.0f);
		if (uiG > 255)
			uiG = 255;
		unsigned int uiB = (unsigned int)(b * 255.0f);
		if (uiB > 255)
			uiB = 255;

		ulong result =
			(((unsigned char)(uiA)) << 24) |
			(((unsigned char)(uiR)) << 16) |
			(((unsigned char)(uiG)) << 8) |
			((unsigned char)(uiB));

		return result;
	}

}