#pragma once

#include "CnEvent.h"

namespace Cindy
{
	namespace Event
	{
		//-----------------------------------------------------------------------------
		/**	@class		FunctionHandler
			@author		cagetu

			@brief		함수 포인터를 객체화 시킨다.
		*/
		//-----------------------------------------------------------------------------
		class FuncHandler
		{
		public:
			virtual ~FuncHandler() {};

			// 호출
			void	Invoke( IEvent* pEvent )		{	Call(pEvent);	}
		private:
			virtual void	Call( IEvent* ) abstract;
		};

		//-----------------------------------------------------------------------------
		/**	@class		MemberFunctionHandler
			@author		cagetu

			@brief		Methods 함수 포인터를 객체화 시킨다.
		*/
		//-----------------------------------------------------------------------------
		template<class T, class EventT>
		class MethodHandler : public FuncHandler
		{
		public:
			typedef void (T::*Method)(EventT*);

			MethodHandler( T* Instance, Method MemFn )
				: m_Instance(Instance), m_Method(MemFn) {}

		private:
			T*			m_Instance;		//!< 부모 인스턴스
			Method		m_Method;		//!< 처리 하고자 하는 메써드

			//-----------------------------------------------------------------------------
			//	Methods
			//-----------------------------------------------------------------------------
			void	Call( IEvent* pEvent ) override;
		};

		//-----------------------------------------------------------------------------
		// Implement
		template<class T, class EventT>
		void MethodHandler<T, EventT>::Call( IEvent* pEvent )
		{
			(m_Instance->*m_Method)(static_cast<EventT*>(pEvent));
		}
	}
}