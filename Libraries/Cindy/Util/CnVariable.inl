namespace Cindy
{
	//inline
	//CnVariable::CnVariable()
	//	: m_Type(Void)
	//{
	//}
	inline
	CnVariable::CnVariable( const CnVariable& Rhs )
		: m_Type(Void)
	{
		Copy( Rhs );
	}
	inline
	CnVariable::CnVariable( int value )
		: m_Type(Int)
	{
		m_Int = value;
	}
	inline
	CnVariable::CnVariable( float value )
		: m_Type(Float)
	{
		m_Float[0] = value;
	}
	inline
	CnVariable::CnVariable( bool value )
		: m_Type(Boolean)
	{
		m_Bool = value;
	}
	inline
	CnVariable::CnVariable( const wchar_t* value )
		: m_Type(String)
	{
		m_String = new std::wstring(value);
	}
	inline
	CnVariable::CnVariable( const Math::Vector3& value )
		: m_Type(Vector3)
	{
		m_Float[0] = value.x;
		m_Float[1] = value.y;
		m_Float[2] = value.z;
	}
	inline
	CnVariable::CnVariable( const Math::Vector4& value )
		: m_Type(Vector4)
	{
		m_Float[0] = value.x;
		m_Float[1] = value.y;
		m_Float[2] = value.z;
		m_Float[3] = value.w;
	}
	inline
	CnVariable::CnVariable( const Math::Matrix44& value )
		: m_Type(Matrix44)
	{
		m_Matrix = new Math::Matrix44( value );
	}
	inline
	CnVariable::CnVariable( CnObject* value )
		: m_Type(Object)
	{
		m_Object = value;
		if (m_Object)
		{
			m_Object->AddRef();
		}
	}
	inline
	CnVariable::~CnVariable()
	{
		Delete();
	}

	//-----------------------------------------------------------------------------
	/** @brief	지운다. Reset */
	//-----------------------------------------------------------------------------
	inline
	void CnVariable::Delete()
	{
		if (String == m_Type)
		{
			delete m_String;
			m_String = 0;
		}
		else if (Matrix44 == m_Type)
		{
			delete m_Matrix;
			m_Matrix = 0;
		}
		else if (Object == m_Type)
		{
			m_Object->Release();
			m_Object = 0;
		}
		m_Type = Void;
	}

	//-----------------------------------------------------------------------------
	/** @brief	Type */
	//-----------------------------------------------------------------------------
	inline
	void CnVariable::SetType( CnVariable::Type type )
	{
		m_Type = type;
	}
	inline
	CnVariable::Type CnVariable::GetType() const
	{
		return m_Type;
	}

	//-----------------------------------------------------------------------------
	//	operator =
	//-----------------------------------------------------------------------------
	inline
	void CnVariable::operator =( const CnVariable& Rhs )
	{
		Delete();
		Copy( Rhs );
	}
	inline
	void CnVariable::operator =( int value )
	{
		m_Type = Int;
		m_Int = value;
	}
	inline
	void CnVariable::operator =( float value )
	{
		m_Type = Float;
		m_Float[0] = value;
	}
	inline
	void CnVariable::operator =( bool value )
	{
		m_Type = Boolean;
		m_Bool = value;
	}
	inline
	void CnVariable::operator =( const wchar_t* value )
	{
		if (String == m_Type)
		{
			*m_String = value;
		}
		else
		{
			Delete();
			m_String = new std::wstring(value);
		}
		m_Type = String;
	}
	inline
	void CnVariable::operator =( const Math::Vector3& value )
	{
		m_Type = Vector3;
		m_Float[0] = value.x;
		m_Float[1] = value.y;
		m_Float[2] = value.z;
	}
	inline
	void CnVariable::operator =( const Math::Vector4& value )
	{
		m_Type = Vector4;
		m_Float[0] = value.x;
		m_Float[1] = value.y;
		m_Float[2] = value.z;
		m_Float[3] = value.w;
	}
	inline
	void CnVariable::operator =( const Math::Matrix44& value )
	{
		if (Matrix44 == m_Type)
		{
			*m_Matrix = value;
		}
		else
		{
			Delete();
			m_Matrix = new Math::Matrix44( value );
		}

		m_Type = Matrix44;
	}
	inline
	void CnVariable::operator =( CnObject* value )
	{
		Delete();
		m_Object = value;
		if (m_Object)
		{
			m_Object->AddRef();
		}
		m_Type = Object;
	}

	//-----------------------------------------------------------------------------
	//	operator ==
	//-----------------------------------------------------------------------------
	inline
	bool CnVariable::operator ==( const CnVariable& Rhs ) const
	{
		if (Rhs.m_Type == m_Type)
		{
			switch (Rhs.m_Type)
			{
			case Void:
				return true;
			case Int:
				return (m_Int == Rhs.m_Int);
			case Boolean:
				return (m_Bool == Rhs.m_Bool);
			case Float:
				return (m_Float[0] == Rhs.m_Float[0]);
			case Vector3:
				return ((m_Float[0] == Rhs.m_Float[0]) &&
						(m_Float[1] == Rhs.m_Float[1]) &&
						(m_Float[2] == Rhs.m_Float[2]));
			case Vector4:
				return ((m_Float[0] == Rhs.m_Float[0]) &&
						(m_Float[1] == Rhs.m_Float[1]) &&
						(m_Float[2] == Rhs.m_Float[2]) &&
						(m_Float[3] == Rhs.m_Float[3]));
			//case Matrix44:
			//	return (*m_Matrix == *Rhs.m_Matrix);
			default:
				return false;
			}
		}
		return false;
	}
	inline
	bool CnVariable::operator ==( int value ) const
	{
		assert(Int == m_Type);
		return (m_Int == value);
	}
	inline
	bool CnVariable::operator ==( float value ) const
	{
		assert(Float == m_Type);
		return (m_Float[0] == value);
	}
	inline
	bool CnVariable::operator ==( bool value ) const
	{
		assert(Boolean == m_Type);
		return (m_Bool == value);
	}
	inline
	bool CnVariable::operator ==( const wchar_t* value ) const
	{
		assert(String == m_Type);
		return (0 == wcscmp(value, m_String->c_str()));
	}
	inline
	bool CnVariable::operator ==( const Math::Vector3& value ) const
	{
		assert(Vector3 == m_Type);
		return ((m_Float[0] == value.x) &&
				(m_Float[1] == value.y) &&
				(m_Float[2] == value.z));
	}
	inline
	bool CnVariable::operator ==( const Math::Vector4& value ) const
	{
		assert(Vector4 == m_Type);
		return ((m_Float[0] == value.x) &&
				(m_Float[1] == value.y) &&
				(m_Float[2] == value.z) &&
				(m_Float[3] == value.w));
	}
	inline
	bool CnVariable::operator ==( CnObject *value ) const
	{
		assert (Object == m_Type);
		return m_Object == value;
	}

	//-----------------------------------------------------------------------------
	//	operator !=
	//-----------------------------------------------------------------------------
	inline
	bool CnVariable::operator !=( const CnVariable& Rhs ) const
	{
		if (Rhs.m_Type != m_Type)
		{
			switch (Rhs.m_Type)
			{
			case Void:
				return true;
			case Int:
				return (m_Int != Rhs.m_Int);
			case Boolean:
				return (m_Bool != Rhs.m_Bool);
			case Float:
				return (m_Float[0] != Rhs.m_Float[0]);
			case Vector3:
				return ((m_Float[0] != Rhs.m_Float[0]) &&
						(m_Float[1] != Rhs.m_Float[1]) &&
						(m_Float[2] != Rhs.m_Float[2]));
			case Vector4:
				return ((m_Float[0] != Rhs.m_Float[0]) &&
						(m_Float[1] != Rhs.m_Float[1]) &&
						(m_Float[2] != Rhs.m_Float[2]) &&
						(m_Float[3] != Rhs.m_Float[3]));
			//case Matrix44:
			//	return (*m_Matrix != *Rhs.m_Matrix);
			default:
				return false;
			}
		}
		return false;
	}
	inline
	bool CnVariable::operator !=( int value ) const
	{
		assert(Int == m_Type);
		return (m_Int != value);
	}
	inline
	bool CnVariable::operator !=( float value ) const
	{
		assert(Float == m_Type);
		return (m_Float[0] != value);
	}
	inline
	bool CnVariable::operator !=( bool value ) const
	{
		assert(Boolean == m_Type);
		return (m_Bool != value);
	}
	inline
	bool CnVariable::operator !=( const wchar* value ) const
	{
		assert(String != m_Type);
		return (0 != wcscmp(value, m_String->c_str()));
	}
	inline
	bool CnVariable::operator !=( const Math::Vector3& value ) const
	{
		assert(Vector3 == m_Type);
		return ((m_Float[0] != value.x) &&
				(m_Float[1] != value.y) &&
				(m_Float[2] != value.z));
	}
	inline
	bool CnVariable::operator !=( const Math::Vector4& value ) const
	{
		assert(Vector4 == m_Type);
		return ((m_Float[0] != value.x) &&
				(m_Float[1] != value.y) &&
				(m_Float[2] != value.z) &&
				(m_Float[3] != value.w));
	}
	inline
	bool CnVariable::operator !=( CnObject *value ) const
	{
		assert (Object == m_Type);
		return m_Object == value;
	}

	//-----------------------------------------------------------------------------
	/** @brief	다른 변수의 값을 복사한다. */
	//-----------------------------------------------------------------------------
	inline
	void CnVariable::Copy( const CnVariable& Rhs )
	{
		assert( Void != Rhs.m_Type );
		m_Type = Rhs.m_Type;
		switch (m_Type)
		{
		case Void:
			break;
		case Int:
			m_Int = Rhs.m_Int;
			break;
		case Float:
			m_Float[0] = Rhs.m_Float[0];
			break;
		case Boolean:
			m_Bool = Rhs.m_Bool;
			break;
		case Vector3:
			m_Float[0] = Rhs.m_Float[0];
			m_Float[1] = Rhs.m_Float[1];
			m_Float[2] = Rhs.m_Float[2];
			break;
		case Vector4:
			m_Float[0] = Rhs.m_Float[0];
			m_Float[1] = Rhs.m_Float[1];
			m_Float[2] = Rhs.m_Float[2];
			m_Float[3] = Rhs.m_Float[3];
			break;
		case Matrix44:
			m_Matrix = new Math::Matrix44(*Rhs.m_Matrix);
			break;
		case String:
			m_String = new std::wstring(*Rhs.m_String);
			break;
		case Object:
			m_Object = Rhs.m_Object;
			if (m_Object)
			{
				m_Object->Release();
			}
			break;
		default:
			assert(0);
			break;
		}
	}
	
	//-----------------------------------------------------------------------------
	/** @brief	값 설정 */
	//-----------------------------------------------------------------------------
	inline 
	void CnVariable::SetInt( int value )
	{
		*this = value;
	}
	inline
	int CnVariable::GetInt() const
	{
		assert(Int == m_Type);
		return m_Int;
	}

	// Float
	inline 
	void CnVariable::SetFloat( float value )
	{
		*this = value;
	}
	inline
	float CnVariable::GetFloat() const
	{
		assert(Float == m_Type);
		return m_Float[0];
	}

	// Bool
	inline
	void CnVariable::SetBool( bool value )
	{
		*this = value;
	}
	inline
	bool CnVariable::GetBool() const
	{
		assert( m_Type == Boolean );
		return m_Bool;
	}

	// String
	inline
	void CnVariable::SetString( const wchar_t* value )
	{
		*this = value;
	}
	inline
	const wchar_t* CnVariable::GetString() const
	{
		assert(m_Type == String);
		return m_String->c_str();
	}

	// Vector3
	inline
	void CnVariable::SetVector3( const Math::Vector3& value )
	{
		*this = value;
	}
	inline
	const Math::Vector3& CnVariable::GetVector3() const
	{
		assert(m_Type == Vector3);
		return *(Math::Vector3*)m_Float;
	}

	// Vector4
	inline
	void CnVariable::SetVector4( const Math::Vector4& value )
	{
		*this = value;
	}
	inline
	const Math::Vector4& CnVariable::GetVector4() const
	{
		assert(m_Type == Vector4);
		return *(Math::Vector4*)m_Float;
	}

	// Matrix44
	inline
	void CnVariable::SetMatrix44( const Math::Matrix44& value )
	{
		*this = value;
	}
	inline
	const Math::Matrix44& CnVariable::GetMatrix44() const
	{
		assert(m_Type == Matrix44);
		return *m_Matrix;
	}

	// Object
	inline
	void CnVariable::SetObject( CnObject* value )
	{
		*this = value;
	}
	inline
	CnObject* CnVariable::GetObject() const
	{
		assert(m_Type == Object);
		return m_Object;
	}

	//------------------------------------------------------------------------------
	/** @brief	Type <-> String */
	//------------------------------------------------------------------------------
	inline
	const wchar* CnVariable::TypeToString( Type type )
	{
		switch (type)
		{
		case Void:		return L"void";
		case Int:		return L"int";
		case Float:		return L"float";
		case Boolean:	return L"bool";
		case Vector3:	return L"vector3";
		case Vector4:	return L"vector4";
		case Matrix44:	return L"matrix44";
		case String:	return L"string";
		default:
			OutputDebugString(L"CnVariable::TypeToString()");
			return 0;
		}
	}
	inline
	CnVariable::Type CnVariable::StringToType(const wchar* str)
	{
		if (0 == wcscmp(str, L"void"))		return Void;
		if (0 == wcscmp(str, L"int"))		return Int;
		if (0 == wcscmp(str, L"float"))		return Float;
		if (0 == wcscmp(str, L"bool"))		return Boolean;
		if (0 == wcscmp(str, L"vector3"))	return Vector3;
		if (0 == wcscmp(str, L"vector4"))	return Vector4;
		if (0 == wcscmp(str, L"color"))		return Vector4; // NOT A BUG!
		if (0 == wcscmp(str, L"string"))	return String;
		if (0 == wcscmp(str, L"matrix44"))	return Matrix44;

		OutputDebugString(L"CnVariable::StringToType()");
		return Void;
	}
}