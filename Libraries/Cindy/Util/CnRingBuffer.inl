//================================================================
// File:               : CnRingBuffer.inl
// Related Header File : CnRingBuffer.h
// Original Author     : changhee
//================================================================

template <class TYPE>
RingBuffer<TYPE>::RingBuffer()
: m_Capacity(0)
, m_Size(0)
, m_BaseIndex(0)
, m_HeadIndex(0)
, m_Elements(0)
{
}
template <class TYPE>
RingBuffer<TYPE>::RingBuffer( uint newCapacity )
: m_Elements(0)
{
	Allocate(newCapacity);
}
template <class TYPE>
RingBuffer<TYPE>::RingBuffer( const RingBuffer<TYPE>& Other )
: m_Elements(0)
{
	Copy( Other );
}
/// Destructor
template <class TYPE>
RingBuffer<TYPE>::~RingBuffer()
{
	Delete();
}

//-----------------------------------------------------------------------------
/** @brief	할당 연산자 */
//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::operator =( const RingBuffer<TYPE>& Other )
{
	Delete();
	Copy( Other );
}

//-----------------------------------------------------------------------------
/** @brief	Index 연산자 */
//-----------------------------------------------------------------------------
template <class TYPE>
TYPE& RingBuffer<TYPE>::operator []( uint Index ) const
{
	uint absIndex = Index + m_BaseIndex;
	if (absIndex >= m_Capacity)
	{
		// wrap-around
		absIndex -= m_Capacity;
	}
	return m_Elements[absIndex];
}

//-----------------------------------------------------------------------------
/** @brief	전체 슬롯 설정(이전 내용물은 모두 지운다.) */
//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::SetCapacity( uint newCapacity )
{
	Delete();
	Allocate( newCapacity );
}

//-----------------------------------------------------------------------------
/** @brief	전체 슬롯 갯수 얻어오기 */
//-----------------------------------------------------------------------------
template <class TYPE>
uint RingBuffer<TYPE>::Capacity() const
{
	return m_Capacity;
}

//-----------------------------------------------------------------------------
/** @brief	현재 element 갯수 얻어오기 */
//-----------------------------------------------------------------------------
template <class TYPE>
uint RingBuffer<TYPE>::Size() const
{
	return m_Size;
}

//-----------------------------------------------------------------------------
/** @brief	ring buffer에 element 추가 */
//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::Add( const TYPE& Element )
{
	// 새로운 element
	m_Elements[m_HeadIndex++] = Element;

	// head index를 wrap around 해야 한다.
	if (m_HeadIndex >= m_Capacity)
	{
		m_HeadIndex = 0;
	}

	// 만약 꽉 차면, base index를 증가시킨다.
	if (m_Size == m_Capacity)
	{
		m_BaseIndex++;
		if (m_BaseIndex >= m_Capacity)
		{
			m_BaseIndex = 0;
		}
	}
	else
	{
		m_Size++;
	}
}

//-----------------------------------------------------------------------------
/** @brief	ring buffer 리셋. 단지 head/base 인덱스들만 reset */
//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::Reset()
{
	m_Size = 0;
	m_BaseIndex = 0;
	m_HeadIndex = 0;
}

//-----------------------------------------------------------------------------
template <class TYPE>
bool RingBuffer<TYPE>::IsEmpty() const
{
	return (0 == m_Size);
}

//-----------------------------------------------------------------------------
/** @brief	첫번째 element 참조 얻어오기 */
//-----------------------------------------------------------------------------
template <class TYPE>
TYPE& RingBuffer<TYPE>::Front() const
{
	return (*this)[0];
}

//-----------------------------------------------------------------------------
/** @brief	마지막 element 참조 얻어오기 */
//-----------------------------------------------------------------------------
template <class TYPE>
TYPE& RingBuffer<TYPE>::Back() const
{
	return (*this)[m_Size-1];
}

//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::AsArray( std::vector<TYPE>& Output )
{
	Output.clear();
	Output.reserver( m_Size );

	for (uint i=0; i<m_Size; ++i)
	{
		Output.push_back( (*this)[i] );
	}
}

//-----------------------------------------------------------------------------
/**	@brief	Element Buffer 할당 */
//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::Allocate( uint newCapacity )
{
	m_Capacity = newCapacity;
	m_Size = 0;
	m_BaseIndex = 0;
	m_HeadIndex = 0;
	m_Elements = new TYPE[newCapacity];
}

//-----------------------------------------------------------------------------
/** @brief	모든 Element 삭제 */
//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::Delete()
{
	m_Capacity = 0;
	m_Size = 0;
	m_BaseIndex = 0;
	m_HeadIndex = 0;

	SAFEDELS(m_Elements);
}

//-----------------------------------------------------------------------------
/** @brief	내용물 복사 */
//-----------------------------------------------------------------------------
template <class TYPE>
void RingBuffer<TYPE>::Copy( const RingBuffer<TYPE>& Other )
{
	Allocate( Other.m_Capacity );
	m_Size = Other.m_Size;
	m_BaseIndex = Other.m_BaseIndex;
	m_HeadIndex = Other.m_HeadIndex;

	for (uint i=0; i<m_Size; ++i)
	{
		(*this)[i] = Other[i];
	}
}
