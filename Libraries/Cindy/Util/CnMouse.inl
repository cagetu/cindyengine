//================================================================
// File:               : CnMouse.inl
// Related Header File : CnMouse.h
// Original Author     : changhee
//================================================================

//--------------------------------------------------------------
inline
void CnMouse::Press( Button eButton )
{
	m_nButtons |= eButton;
}
inline
void CnMouse::Release( Button eButton )
{
	m_nButtons &= ~eButton;
}
inline
bool CnMouse::IsPressed( int nButton )
{
	return (m_nButtons & nButton) ? true : false;
}

//--------------------------------------------------------------
inline
void CnMouse::SetSensitive( float fSensitive )
{
	m_fSensitive = fSensitive;
}
inline
float CnMouse::GetSensitive() const
{
	return m_fSensitive;
}