//================================================================
// File					: CnTimer.h			
// Original Author		: Changhee
// Creation Date		: 2009. 1. 29.
//================================================================
#ifndef	__CN_TIMER_H__
#define	__CN_TIMER_H__

#include "../Cindy.h"
#include "../System/CnTime.h"

namespace Cindy
{
	//----------------------------------------------------------------
	/**	@class		Timer
		@author		cagetu
		@brief		타이머
					Nebula3에 있는 타이머 객체
	*/
	//----------------------------------------------------------------
	class CnTimer
	{
	public:
		CnTimer();

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		void	Start();
		void	Stop();
		void	Reset();

		bool	IsRunning() const;

		float64	GetTime() const;
		ulong	GetTicks() const;
	private:
		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnTimer( const CnTimer& rhs );

		/// return internal time as 64 bit integer
		int64	 InternalTime() const;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		bool	m_Running;
		int64	m_DiffTime;  // accumulated time when the timer was not running
		int64	m_StopTime;  // when was the timer last stopped?
	};

	//----------------------------------------------------------------
	/**
	*/
	inline
	bool CnTimer::IsRunning() const
	{
		return m_Running;
	}
}

#endif	// __CN_TIMER_H__
