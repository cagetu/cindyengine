//================================================================
// File					: CnUnCopyable.h			
// Original Author		: Changhee
// Creation Date		: 2007. 8. 6.
//================================================================
#pragma once

namespace Cindy
{
	//==================================================================
	/** Uncopyable
		@author			cagetu
		@since			2007년 8월 6일
		@remarks		객체 복사 방지 클래스
						private 상속을 이용한다.
	*/
	//==================================================================
	class CN_DLL CnUncopyable
	{
	private:
		CnUncopyable( const CnUncopyable& );
		CnUncopyable& operator =( const CnUncopyable& );

	public:
		CnUncopyable()		{}
		~CnUncopyable()		{}
	};
}

//--------------------------------------------------------------
#define __DeclareUnCopy(classname)	\
private: \
	classname( const classname& ); \
	classname& operator =( const classname& ); \
private: