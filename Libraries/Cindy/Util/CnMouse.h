#ifndef __CN_MOUSE_H__
#define __CN_MOUSE_H__

namespace Cindy
{
	class CN_DLL CnMouse
	{
	public:
		enum Button
		{
			Left = (1<<0),
			Right = (1<<1),
			Middle = (1<<2),
		};

		struct Pos
		{
			union
			{
				struct
				{
					short	x;
					short	y;
				};
				float delta;
			};
		};

	protected:
		int		m_nButtons;
		float	m_fSensitive;

	public:
		CnMouse();

		void	Press( Button eButton );
		void	Release( Button eButton );
		bool	IsPressed( int nButton );

		void	SetSensitive( float fSensitive );
		float	GetSensitive() const;
	};

#include "CnMouse.inl"
}

#endif	// __CN_MOUSE_H__