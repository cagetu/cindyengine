#ifndef __CNVARIABLE_H__
#define __CNVARIABLE_H__

#include "../Math/CnMatrix44.h"
#include "../Foundation/CnObject.h"

namespace Cindy
{
	//-----------------------------------------------------------------------------
	/**	@class		Variable
		@author		cagetu

		@brief		유연하게 값을 주고 받을 수 있도록 가변 값 객체
					String은 객체의 크기가 커지는 것을 방지하여, 외부에서 관리하도록..
	*/
	//-----------------------------------------------------------------------------
	class CN_DLL CnVariable
	{
	public:
		enum Type
		{
			Void,
			Int,
			Float,
			Boolean,
			Vector3,
			Vector4,
			Matrix44,
			String,
			Object,
		};

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnVariable();
		explicit CnVariable( const CnVariable& Rhs );
		explicit CnVariable( int value );
		explicit CnVariable( float value );
		explicit CnVariable( bool value );
		explicit CnVariable( const Math::Vector3& value );
		explicit CnVariable( const Math::Vector4& value );
		explicit CnVariable( const Math::Matrix44& value );
		explicit CnVariable( const wchar* value );
		explicit CnVariable( CnObject* value );
		~CnVariable();

		/// assignment operator
		void operator =( const CnVariable& Rhs );
		void operator =( int value );
		void operator =( float value );
		void operator =( bool value );
		void operator =( const wchar* value );
		void operator =( const Math::Vector3& value );
		void operator =( const Math::Vector4& value );
		void operator =( const Math::Matrix44& value );
		void operator =( CnObject* value );

		/// equality operator
		bool operator ==( const CnVariable& Rhs ) const;
		bool operator ==( int value ) const;
		bool operator ==( float value ) const;
		bool operator ==( bool value ) const;
		bool operator ==( const wchar* value ) const;
		bool operator ==( const Math::Vector3& value ) const;
		bool operator ==( const Math::Vector4& value ) const;
		bool operator ==( CnObject* value ) const;

		/// inequality operator
		bool operator !=( const CnVariable& Rhs ) const;
		bool operator !=( int value ) const;
		bool operator !=( float value ) const;
		bool operator !=( bool value ) const;
		bool operator !=( const wchar* value ) const;
		bool operator !=( const Math::Vector3& value ) const;
		bool operator !=( const Math::Vector4& value ) const;
		bool operator !=( CnObject* value ) const;

		// Type
		void				SetType( Type type );
		Type				GetType() const;

		// 값 설정
		void				SetInt( int value );
		int					GetInt() const;

		void				SetFloat( float value );
		float				GetFloat() const;

		void				SetBool( bool value );
		bool				GetBool() const;

		void				SetString( const wchar* value );
		const wchar*		GetString() const;

		void				SetVector3( const Math::Vector3& value );
		const Math::Vector3&	GetVector3() const;

		void				SetVector4( const Math::Vector4& value );
		const Math::Vector4&	GetVector4() const;

		void				SetMatrix44( const Math::Matrix44& value );
		const Math::Matrix44&	GetMatrix44() const;

		void				SetObject( CnObject* value );
		CnObject*			GetObject() const;

		// Type To String
		static const wchar*	TypeToString( Type type );
		static Type			StringToType( const wchar* str );
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		union
		{
			int				m_Int;
			bool			m_Bool;
			float			m_Float[4];
			Math::Matrix44*	m_Matrix;
			std::wstring*	m_String;
			CnObject*		m_Object;
		};

		Type	m_Type;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void	Copy( const CnVariable& Rhs );
		void	Delete();
	};
}
#include "CnVariable.inl"

#endif	//__CNVARIABLE_H__