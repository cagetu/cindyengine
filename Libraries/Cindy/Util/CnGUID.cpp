//================================================================
// File:				: CnGUID.cpp
// Related Header File	: CnGUID.h
// Original Author		: changhee
//================================================================
#include "CnGUID.h"
#include <rpc.h>

#pragma comment(lib, "rpcrt4.lib")

namespace Cindy
{
	Guid::Guid()
	{
	}
	Guid::Guid( const Guid& rhs )
	{
		this->uuid_ = rhs.uuid_;
	}
	Guid::Guid( const unsigned char* ptr, uint size )
	{	
		assert( (0 != ptr) && (sizeof(UUID) == size) );
		memcpy( &(this->uuid_), ptr, sizeof(UUID) );
	}

	//-----------------------------------------------------------------------------
	void Guid::operator = (const Guid& rhs)
	{
		if (this != &rhs)
		{
			this->uuid_ = rhs.uuid_;
		}
	}

	//-----------------------------------------------------------------------------
	void Guid::operator = (const CnString& rhs)
	{
		assert( false == rhs.empty() );
		RPC_STATUS result = UuidFromString( (RPC_WSTR)rhs.c_str(), &(this->uuid_) );
		assert( RPC_S_INVALID_STRING_UUID != result );
	}

	//------------------------------------------------------------------------------
	bool Guid::operator == (const Guid& rhs) const
	{
		RPC_STATUS status;
		int result = UuidCompare( const_cast<UUID*>(&this->uuid_), const_cast<UUID*>(&rhs.uuid_), &status );
		return (0 == result);
	}

	//------------------------------------------------------------------------------
	bool Guid::operator != (const Guid& rhs) const
	{
		RPC_STATUS status;
		int result = UuidCompare( const_cast<UUID*>(&this->uuid_), const_cast<UUID*>(&rhs.uuid_), &status );
		return (0 != result);
	}

	//------------------------------------------------------------------------------
	bool Guid::operator < (const Guid& rhs) const
	{
		RPC_STATUS status;
		int result = UuidCompare( const_cast<UUID*>(&this->uuid_), const_cast<UUID*>(&rhs.uuid_), &status );
		return (-1 == result);
	}

	//------------------------------------------------------------------------------
	bool Guid::operator <= (const Guid& rhs) const
	{
		RPC_STATUS status;
		int result = UuidCompare( const_cast<UUID*>(&this->uuid_), const_cast<UUID*>(&rhs.uuid_), &status );
		return ((-1 == result) || (0 == result));
	}

	//------------------------------------------------------------------------------
	bool Guid::operator > (const Guid& rhs) const
	{
		RPC_STATUS status;
		int result = UuidCompare( const_cast<UUID*>(&this->uuid_), const_cast<UUID*>(&rhs.uuid_), &status );
		return (1 == result);
	}

	//------------------------------------------------------------------------------
	bool Guid::operator >= (const Guid& rhs) const
	{
		RPC_STATUS status;
		int result = UuidCompare( const_cast<UUID*>(&this->uuid_), const_cast<UUID*>(&rhs.uuid_), &status );
		return ((1 == result) || (0 == result));
	}

	//------------------------------------------------------------------------------
	bool Guid::IsValid() const
	{
		RPC_STATUS status;
		int result = UuidIsNil(const_cast<UUID*>(&this->uuid_), &status);
		return (TRUE != result);
	}

	//------------------------------------------------------------------------------
	void Guid::Generate()
	{
		UuidCreate( &this->uuid_ );
	}

	//------------------------------------------------------------------------------
	CnString Guid::AsString() const
	{
		const wchar_t* uuidStr;
		UuidToString( (UUID*)&this->uuid_, (RPC_WSTR*)&uuidStr );
		CnString result = uuidStr;
		RpcStringFree( (RPC_WSTR*)&uuidStr );
		return result;
	}

	//------------------------------------------------------------------------------
	uint Guid::AsBinary( const unsigned char*& outPtr ) const
	{
		outPtr = (const unsigned char*) &this->uuid_;
		return sizeof(UUID);
	}

	//------------------------------------------------------------------------------
	uint Guid::HashCode() const
	{
		RPC_STATUS status;
		unsigned short hashCode = UuidHash((UUID*)&this->uuid_, &status);
		return (uint) hashCode;
	}

	//------------------------------------------------------------------------------
	Guid Guid::FromString( const CnString& str )
	{
		Guid newGuid;
		RPC_STATUS success = UuidFromString( (RPC_WSTR)str.c_str(), &(newGuid.uuid_) );
		assert( RPC_S_OK == success );
		return newGuid;
	}

	//------------------------------------------------------------------------------
	Guid Guid::FromBinary( const unsigned char* ptr, uint numBytes )
	{
		assert( (0!=ptr) && (sizeof(UUID) == numBytes) );
		Guid newGuid;
		memcpy( &(newGuid.uuid_), ptr, sizeof(UUID) );
		return newGuid;
	}
}