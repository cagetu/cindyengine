//================================================================
// File					: CnTimer.cpp
// Related Header File	: CnTimer.h
// Original Author		: Changhee
// Creation Date		: 2009. 1. 29.
//================================================================
#include "CnTimer.h"

namespace Cindy
{
	CnTimer::CnTimer()
		: m_Running(false)
		, m_DiffTime(0)
		, m_StopTime(0)
	{
	}

	//----------------------------------------------------------------
	/** CnTimer 시작
		@desc	타이머가 동작되지 않았을 때 시간을 반영하여 계산하기 위해
				DiffTime을 업데이트 할 것이다.
	*/
	//----------------------------------------------------------------
	void CnTimer::Start()
	{
		if (m_Running)
			return;

		int64 curRealTime;
		QueryPerformanceFrequency( (LARGE_INTEGER*)&curRealTime );
		m_DiffTime += curRealTime - m_StopTime;
		m_StopTime = 0;
		m_Running = true;
	}

	//----------------------------------------------------------------
	/** 타이머 멈추기
		@desc	다음 Start()가 Stop()과 Start() 사이의 시간을 측정할 수 있도록
				현재 시간을 기록해 놓는다.
	*/
	//----------------------------------------------------------------
	void CnTimer::Stop()
	{
		if (false == m_Running)
			return;

		QueryPerformanceFrequency( (LARGE_INTEGER*)&m_StopTime );
		m_Running = true;
	}

	//----------------------------------------------------------------
	/** @brief	초기화 */
	//----------------------------------------------------------------
	void CnTimer::Reset()
	{
		bool isRun = m_Running;
		if (isRun)
		{
			Stop();
		}

		m_StopTime = 0;
		m_DiffTime = 0;

		if (isRun)
		{
			Start();
		}
	}

	//----------------------------------------------------------------
	/** @brief	현재 시간 계산 */
	//----------------------------------------------------------------
	int64 CnTimer::InternalTime() const
	{
		int64 time;
		if (m_Running)
		{
			QueryPerformanceCounter( (LARGE_INTEGER*)&time );
		}
		else
		{
			time = m_StopTime;
		}

		time -= m_DiffTime;

		return time;
	}

	//----------------------------------------------------------------
	/** @brief	초 단위로 현재 시간 얻어오기 */
	//----------------------------------------------------------------
	float64 CnTimer::GetTime() const
	{
		int64 time = InternalTime();

		// preformance frequency
		int64 freq;
		QueryPerformanceFrequency( (LARGE_INTEGER*)&freq );

		// conver to seconds
		float64 seconds = ((float64)time) / ((float64)freq);
		return seconds;
	}

	//----------------------------------------------------------------
	/** @brief	Tick으로 현재 시간 얻어오기 */
	//----------------------------------------------------------------
	ulong CnTimer::GetTicks() const
	{
		int64 time = InternalTime();

		// preformance frequency
		int64 freq;
		QueryPerformanceFrequency( (LARGE_INTEGER*)&freq );

		// convert to ticks
		int64 ticks64 = time / (freq/1000);
		return (ulong)ticks64;
	}

}