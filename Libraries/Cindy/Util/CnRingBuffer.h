// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

#include "../Cindy.h"

namespace Cindy
{
	//==================================================================
	/** RingBuffer
		@author		cagetu
		@since		2009년 3월 9일
		@brief		ring buffer는 원 형태로 최대 갯수의 element들을 저장한다.
					만약 buffer가 꽉 차면, 최신 element는 가장 오래된 element를 
					덮어쓴다.
		@see		Nebula3 - RingBuffer
	*/
	//==================================================================
	template <class TYPE>
	class CN_DLL RingBuffer
	{
	public:
		RingBuffer();
		RingBuffer( uint newCapacity );
		RingBuffer( const RingBuffer<TYPE>& Other );
		~RingBuffer();

		/// operator
		void operator =( const RingBuffer<TYPE>& Other );
		TYPE& operator []( uint Index ) const;

		/// Capacity
		void SetCapacity( uint newCapacity );
		uint Capacity() const;

		/// Size
		uint Size() const;

		/// 비어 있는지 확인
		bool IsEmpty() const;

		/// Element
		void Add( const TYPE& Element );
		void Reset();

		TYPE& Front() const;
		TYPE& Back() const;

		void AsArray( std::vector<TYPE>& Output );

	private:
		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		uint	m_Capacity;
		uint	m_Size;
		uint	m_BaseIndex;
		uint	m_HeadIndex;
		TYPE*	m_Elements;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		void	Allocate( uint newCapacity );
		void	Delete();
		void	Copy( const RingBuffer<TYPE>& Other );
	};

#include "CnRingBuffer.inl"
}