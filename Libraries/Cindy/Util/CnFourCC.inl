//================================================================
// File:               : CnFourCC.inl
// Related Header File : CnFourCC.h
// Original Author     : changhee
//================================================================

inline
FourCC::FourCC()
	: m_FourCC(0)
{
}
inline
FourCC::FourCC( uint f )
	: m_FourCC(f)
{
}
inline
FourCC::FourCC( const std::string& s )
{
	this->SetFromString( s );
}

//-----------------------------------------------------------------------------
inline
bool FourCC::operator ==(const FourCC& rhs) const
{
	return (m_FourCC == rhs.m_FourCC);
}

//-----------------------------------------------------------------------------
inline
bool FourCC::operator !=(const FourCC& rhs) const
{
	return (m_FourCC != rhs.m_FourCC);
}

//-----------------------------------------------------------------------------
inline
bool FourCC::operator <(const FourCC& rhs) const
{
	return (m_FourCC < rhs.m_FourCC);
}

//-----------------------------------------------------------------------------
inline
bool FourCC::operator <=(const FourCC& rhs) const
{
	return (m_FourCC <= rhs.m_FourCC);
}

//-----------------------------------------------------------------------------
inline
bool FourCC::operator >(const FourCC& rhs) const
{
	return (m_FourCC > rhs.m_FourCC);
}

//-----------------------------------------------------------------------------
inline
bool FourCC::operator >=(const FourCC& rhs) const
{
	return (m_FourCC >= rhs.m_FourCC);
}

//-----------------------------------------------------------------------------
inline
bool FourCC::IsValid() const
{
	return (0 != m_FourCC);
}

//-----------------------------------------------------------------------------
inline
void FourCC::SetFromUInt( uint f )
{
	assert(0 != f);
	m_FourCC = f;
}

//-----------------------------------------------------------------------------
inline
uint FourCC::AsUInt() const
{
	return m_FourCC;
}

//-----------------------------------------------------------------------------
inline
void FourCC::SetFromString( const std::string& s )
{
	*this = FromString( s );
}

//-----------------------------------------------------------------------------
inline
std::string FourCC::AsString() const
{
	return ToString(*this);
}

//-----------------------------------------------------------------------------
inline
std::string FourCC::ToString( const FourCC& f )
{
	assert(0 != f.m_FourCC);
	std::string str("xzyw");
	str[0] = (char)((f.m_FourCC & 0xFF000000) >> 24);
	str[1] = (char)((f.m_FourCC & 0x00FF0000) >> 16);
	str[2] = (char)((f.m_FourCC & 0x0000FF00) >> 8);
	str[3] = (char)(f.m_FourCC & 0x000000FF);
	return str;
}

//-----------------------------------------------------------------------------
inline
FourCC FourCC::FromString( const std::string& s )
{
	assert( false == s.empty() && (s.size() == 4) );
	return FourCC( uint(s[0] | s[1]<<8 | s[2]<<16 | s[3]<<24) );
}
