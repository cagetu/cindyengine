// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

namespace Cindy
{
	//==================================================================
	/** CnProxy
		@author			cagetu
		@since			2007년 5월 21일
		@remarks		기본적인 Proxy Pattern을 구현
	*/
	//==================================================================
	template <class T>
	class CN_DLL CnProxy
	{
	protected:
		T*		m_pObject;

	public:
		CnProxy();
		CnProxy( T* pObject );
		CnProxy( const CnProxy& rPtr );

		bool		IsNull() const;
		T*			GetPtr() const;

		CnProxy&	operator= ( const CnProxy& rPtr );
		T*			operator-> () const;
	};

#include "CnProxy.inl"
}
