//================================================================
// File					: CnProfile.cpp
// Related Header File	: CnProfile.h
// Original Author		: Changhee
// Creation Date		: 2009. 1. 29.
//================================================================
#include "CnProfile.h"
#include "../System/CnTime.h"

namespace Cindy
{
	namespace Profile
	{
		const uint MAX_ACCUMULATORS = 2048;
		static uint g_AccumulatorCount = 0;
		static Accumulator* g_Accumulators[ MAX_ACCUMULATORS ];

		//================================================================
		Accumulator::Accumulator()
			: m_Hits(0)
			, m_Total(0.0f)
			, m_Index(-1)
		{
		}
		Accumulator::Accumulator( const char* Name )
			: m_Hits(0)
			, m_Total(0.0f)
			, m_Index(-1)
			, m_Name(Name)
		{
			if (g_AccumulatorCount < MAX_ACCUMULATORS)
			{
				g_Accumulators[ g_AccumulatorCount ] = this;
				m_Index = g_AccumulatorCount++;
			}
		}
		Accumulator::Accumulator( const char* Function, const char* Name )
			: m_Hits(0)
			, m_Total(0.0f)
			, m_Index(-1)
			, m_Name(Function)
		{
			m_Name += Name;

			if (g_AccumulatorCount < MAX_ACCUMULATORS)
			{
				g_Accumulators[ g_AccumulatorCount ] = this;
				m_Index = g_AccumulatorCount++;
			}
		}
		// destructor
		Accumulator::~Accumulator()
		{
			if (m_Index >= 0)
			{
				g_Accumulators[m_Index] = NULL;
			}
		}

		//----------------------------------------------------------------
		/**
		*/
		void Accumulator::SetName( const char* Name )
		{
			m_Name = Name;

			if (g_AccumulatorCount < MAX_ACCUMULATORS)
			{
				g_Accumulators[ g_AccumulatorCount ] = this;
				m_Index = g_AccumulatorCount++;
			}
		}

		//----------------------------------------------------------------
		/**
		*/
		void Accumulator::Report()
		{
			//Platform::PrintProfile("[%12.3f] [%8d] [%s]\n", m_Total, m_Hits, m_Name);
			char buffer[512];
			sprintf( buffer, "[%12.3f] [%8d] [%s]\n", m_Total, m_Hits, m_Name.c_str() );
			OutputDebugStringA( buffer );
		}

		int CompareAccumulatorPtr( const void* ptr1, const void* ptr2 )
		{
			const Accumulator* left = *(const Accumulator**)ptr1;
			const Accumulator* right = *(const Accumulator**)ptr2;

			if (left && !right)
			{
				return -1;
			}

			if (!left && right)
			{
				return 1;
			}

			if (left && right)
			{
				if ((left)->m_Total > (right)->m_Total)
				{
					return -1;
				}
				else if ((left)->m_Total < (right)->m_Total)
				{
					return 1;
				}
			}

			return 0;
		}

		//----------------------------------------------------------------
		/**
		*/
		void Accumulator::ReportAll()
		{
			float totalTime = 0.f;
			for ( uint32 i = 0; i < g_AccumulatorCount; i++ )
			{
				if (g_Accumulators[i])
				{
					totalTime += g_Accumulators[i]->m_Total;
				}
			}

			if (totalTime > 0.f)
			{
				//Platform::PrintProfile("\nProfile Report:\n");
				OutputDebugStringA( "\nProfile Report:\n");

				qsort( g_Accumulators, g_AccumulatorCount, sizeof(Accumulator*), &CompareAccumulatorPtr );

				for ( uint32 i = 0; i < g_AccumulatorCount; i++ )
				{
					if (g_Accumulators[i] && g_Accumulators[i]->m_Total > 0.f)
					{
						g_Accumulators[i]->Report();
					}
				}
			}
		}

		//================================================================
		Description::Description (const char* fmt, ...)
		{
			if (fmt)
			{
				va_list args;
				va_start(args, fmt);
				vsnprintf(m_String, 256, fmt, args);
				va_end(args);
			}
			else
			{
				m_String[0] = '\0';
			}
		}

		int g_StackDepth = 0;
		//================================================================
		ScopeTimer::ScopeTimer( Accumulator& accum, const Description& desc, bool print )
			: m_Accum (&accum)
			, m_Description (desc)
			, m_Print (print)
		{
			g_StackDepth++;

			// capture time last
			m_StartTime = System::TimerGetClock();
		}
		ScopeTimer::ScopeTimer( const ScopeTimer& rhs )
			: m_Description ("")
		{
			assert(false);
		}
		ScopeTimer::~ScopeTimer()
		{
			g_StackDepth--;

			// compute time taken first
			uint64 taken = System::TimerGetClock() - m_StartTime;

			// compute in milliseconds
			float millis = System::CyclesToMillisec(taken);

			// do we have a description?
			if (m_Print && strlen(m_Description.m_String) != 0)
			{
				char buffer[512];
				sprintf( buffer, "[%12.3f] [%s]\n", millis, m_Description.m_String );
				OutputDebugStringA( buffer );
				//Platform::PrintProfile("[%12.3f] [%s]\n", millis, m_Description.m_String);
			}

			// log entry
			//g_Logger.Log(millis, m_Accum->m_Name, m_Description.m_String);

			// accumulate time
			m_Accum->m_Hits++;
			m_Accum->m_Total += millis;
		}
	}
}