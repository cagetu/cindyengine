//================================================================
// File					: CnProfile.h			
// Original Author		: Changhee
// Creation Date		: 2009. 1. 29.
//================================================================
#ifndef	__CN_PROFILE_H_
#define	__CN_PROFILE_H_

#include "../Cindy.h"

namespace Cindy
{
	namespace Profile
	{
		//-----------------------------------------------------------------------------
		/**	Accumulator ��ü
			@author		cagetu
		*/
		//-----------------------------------------------------------------------------
		class Accumulator
		{
		public:
			uint	m_Hits;
			float	m_Total;

			int			m_Index;
			std::string	m_Name;

			Accumulator();
			Accumulator( const char* Name );
			Accumulator( const char* Function, const char* Name );
			~Accumulator();

			void	SetName( const char* Name );

			void	Report();
			static void	ReportAll();
		};

		//-----------------------------------------------------------------------------
		/**	Description ��ü
			@author		cagetu
		*/
		//-----------------------------------------------------------------------------
		class Description
		{
		public:
			char	m_String[256];

			Description( const char* fmt, ... );
		};

		//-----------------------------------------------------------------------------
		/**	ScopeTimer ��ü
			@author		cagetu
		*/
		//-----------------------------------------------------------------------------
		class ScopeTimer
		{
		public:
			ScopeTimer( Accumulator& accum, const Description& desc = NULL, bool print = false );
			ScopeTimer( const ScopeTimer& rhs );
			~ScopeTimer();

		private:
			uint64	m_StartTime;
			Accumulator* m_Accum;
			Description m_Description;
			bool m_Print;
		};
	}

# define PROFILE_SCOPE_ACCUM(__Accum) Profile::ScopeTimer __ScopeAccum ( __Accum );
# define PROFILE_SCOPE_ACCUM_VERBOSE(__Accum, __Cond, __Str) Profile::ScopeTimer __ScopeAccum ( __Accum, __Cond ? Profile::Description __Str : Profile::Description (NULL), __Cond );
}

#endif	// __CN_PROFILE_H_
