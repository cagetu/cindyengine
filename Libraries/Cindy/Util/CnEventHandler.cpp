//================================================================
// File:               : CnEventHandler.cpp
// Related Header File : CnEventHandler.h
// Original Author     : changhee
//================================================================
#include "CnEventHandler.h"

namespace Cindy
{
	//-------------------------------------------------------------------------
	CnEventHandler::CnEventHandler()
	{
	}
	CnEventHandler::~CnEventHandler()
	{
		FunctionList::iterator i, iend;
		iend = m_Functions.end();
		for (i=m_Functions.begin(); i != iend; ++i)
			delete i->second;
		m_Functions.clear();
	}
	
	//-------------------------------------------------------------------------
	/** @brief	메시지 처리 가능 */
	//-------------------------------------------------------------------------
	bool CnEventHandler::IsValid( IEvent* pEvent )
	{
		//FunctionList::iterator it = m_Functions.find( TypeInfo(typeid(*pEvent)) );
		FunctionList::iterator it = m_Functions.find( pEvent->GetRTTI()->GetName() );
		if (it != m_Functions.end())
			return true;

		return false;
	}

	//-------------------------------------------------------------------------
	/** @brief	메시지 처리 */
	//-------------------------------------------------------------------------
	void CnEventHandler::Handle( IEvent* pEvent )
	{
		//FunctionList::iterator it = m_Functions.find( TypeInfo(typeid(*pEvent)) );
		FunctionList::iterator it = m_Functions.find( pEvent->GetRTTI()->GetName() );
		if(it != m_Functions.end())
		{
			it->second->Invoke( pEvent );
		}
	}
}