inline
CnRefCount::CnRefCount()
: m_References(0)
{
}
inline
CnRefCount::~CnRefCount()
{
	assert( 0 == m_References );
}

//------------------------------------------------------------------
// RefenceCount
//------------------------------------------------------------------
inline
void CnRefCount::AddRef()
{
	//++m_References;
	Threading::Win32Interlocked::Increment( m_References );
}

//--------------------------------------------------------------
inline
bool CnRefCount::Release()
{
	//if( --m_References == 0 )
	if (0 == Threading::Win32Interlocked::Decrement( m_References ))
	{
		delete this;
		return true;
	}
	return false;
}

//------------------------------------------------------------------
inline
uint CnRefCount::GetReferenceCount() const
{
	return m_References;
}
