//================================================================
// File:           : CnConsole.h
// Original Author : changhee
// Creation Date   : 2007. 8. 29
//================================================================
#ifndef __CN_COLSOLE_H__
#define __CN_COLSOLE_H__

#include <windows.h>

namespace Cindy
{
	//================================================================
	/** Console Class
	       @author cagetu
		   @remarks 
				Console로 출력을 해준다
				참고자료 "http://luna.sumomo.ne.jp/" luna library
	*/
	//================================================================	
	class CN_DLL CnConsole
	{
	public:
		CnConsole();
		~CnConsole();

		bool	Create();
		void	Destroy();
		void	Clear();

		void	Locate( int nX, int nY );

		void	Print( const CnString& strMsg );
		void	Print( int nX, int nY, const CnString& strMsg );

		void	SetColor( unsigned short usAttributes );
		void	SetCursor( bool bOn );

	protected:
		bool		m_bIsCreated;
		HANDLE		m_hHandle;
	};
}

#endif	// __CN_COLSOLE_H__