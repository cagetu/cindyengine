//================================================================
// File:               : CnLog.cpp
// Related Header File : CnLog.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 24
//================================================================
#include "../Cindy.h"
#include "CnLog.h"

namespace Cindy
{
	//================================================================
	//	Log Class
	//================================================================
	CnLog::CnLog( const MoString& strName, const MoString& strOutputPath, bool bOutputDebug, bool bOutputConsole, bool bOpen )
		: MoLog( strName, strOutputPath, bOutputDebug, bOutputConsole, bOpen )
	{
	}
	CnLog::~CnLog()
	{
	}

	//---------------------------------------------------------------------
	void CnLog::PutHeader()
	{
		PutLine( 2 );
		PutString( L"::::Begin YoYo Log [" + m_strFileName + L"]::::" );
		PutLine();
		PutDate();
		PutTime();
		PutLine();
	}
	void CnLog::PutFooter()
	{
		m_nIndentation = 0;

		PutLine( 2 );
		PutString( L"::::End YoYo Log [" + m_strFileName + L"]::::" );
		PutLine();
		PutDate();
		PutTime();
		PutLine( 2 );
	}

	//---------------------------------------------------------------------
	void CnLog::WriteString( const MoString& strMsg )
	{
		MoLog::WriteString( strMsg );
	}

	void CnLog::WriteError( const wchar* strFileName, int nLine, const MoString& strMsg )
	{
		this->m_OutputConsole.SetColor( BACKGROUND_RED | FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED );
		MoLog::WriteError( strFileName, nLine, strMsg );
		this->m_OutputConsole.SetColor( FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED );
	}

	void CnLog::WriteWarning( const MoString& strMsg )
	{
		this->m_OutputConsole.SetColor( BACKGROUND_GREEN | FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED );
		MoLog::WriteString( strMsg );
		this->m_OutputConsole.SetColor( FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED );
	}

	//================================================================
	//	Log Ptr
	//================================================================
	CnLogProxy* CnLogProxy::ms_pSingleton = NULL;
	//------------------------------------------------------------------
	CnLogProxy& CnLogProxy::Instance()
	{		
		assert( ms_pSingleton );
		return (*ms_pSingleton );
	}

	//------------------------------------------------------------------
	CnLogProxy::CnLogProxy()
		: m_pLog(NULL)
	{
		assert( !ms_pSingleton );
		ms_pSingleton = this;
	}
	CnLogProxy::~CnLogProxy()
	{
		Remove();

		assert( ms_pSingleton );
		ms_pSingleton = NULL;
	}

	//------------------------------------------------------------------
	//	Add/ Remove Log
	//------------------------------------------------------------------
	void CnLogProxy::Add( const MoString& strName, const MoString& strOutputPath, bool bOutputDebug, bool bOutputConsole, bool bOpen )
	{
		if (m_pLog)
			return;

		m_pLog = new CnLog( strName, strOutputPath, bOutputDebug, bOutputConsole, bOpen );
	}

	void CnLogProxy::Remove()
	{
		SAFEDEL( m_pLog );
	}

	//------------------------------------------------------------------
	bool CnLogProxy::IsNull() const
	{
		return (m_pLog == NULL) ? true : false;
	}

	CnLog* CnLogProxy::operator ->() const
	{
		return m_pLog;
	}

	//------------------------------------------------------------------
	//	wrapped methods
	//------------------------------------------------------------------
	void CnLogProxy::WriteString( const wchar* Msg, ... )
	{
		if (!m_pLog)
			return;

		static wchar buffer[1024];
		wmemset( buffer, 0x00, 1024 );

		va_list args;
		va_start( args, Msg );
		int len = vswprintf( buffer, Msg, args );
		va_end( args );

		buffer[len] = L'\0';

		m_pLog->WriteString( buffer );
	}

	//------------------------------------------------------------------
	void CnLogProxy::WriteWarning( const wchar* Msg, ... )
	{
		if (!m_pLog)
			return;

		static wchar buffer[1024];
		wmemset( buffer, 0x00, 1024 );

		va_list args;
		va_start( args, Msg );
		int len = vswprintf( buffer, Msg, args );
		va_end( args );

		buffer[len] = L'\0';

		m_pLog->WriteString( buffer );

		MessageBox( NULL, buffer, L"[Warning]", NULL );
	}

	//------------------------------------------------------------------
	void CnLogProxy::WriteError( const wchar* strFileName, int nLine, const CnString& Msg )
	{
		if(!m_pLog)
			return;

		m_pLog->WriteError(strFileName, nLine, Msg);

		//static wchar buffer[1024];
		//wmemset( buffer, 0x00, 1024 );

		//va_list args;
		//va_start( args, Msg );
		//int len = vswprintf( buffer, Msg, args );
		//va_end( args );

		//buffer[len] = L'\0';

		////m_pLog->WriteError( __WFILE__, __LINE__, buffer );
		//m_pLog->WriteError( strFileName, nLine, buffer );
	}
}