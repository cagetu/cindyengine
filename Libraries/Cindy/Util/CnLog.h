//================================================================
// File:           : CnLog.h
// Original Author : changhee
// Creation Date   : 2007. 1. 24
//================================================================
#ifndef __CN_LOG_H__
#define __CN_LOG_H__

#include "CnConsole.h"
#include "CnString.h"
#include <MoCommon/MoLog.h>

namespace Cindy
{
	//================================================================
	/** Log
	       @author cagetu
		   @remarks 
				YoYo Log에 관련된 클래스
		   @see
				MoLog
	*/
	//================================================================	
	class CnLog : public MoCommon::MoLog
	{
		friend class CnLogProxy;
	public:
		CnLog( const MoString& strName, const MoString& strOutputPath = L"", bool bOutputDebug = false, bool bOutputConsole = false, bool bOpen = true );
		virtual ~CnLog();

	protected:
		void	PutHeader() override;
		void	PutFooter() override;

		void	WriteString( const MoString& strMsg );
		void	WriteWarning( const MoString& strMsg );
		void	WriteError( const wchar* strFileName, int nLine, const MoString& strMsg );
	};

	//================================================================
	/** LogPtr
	       @author cagetu
		   @remarks 
				Log 대행 객체
	*/
	//================================================================	
	class CN_DLL CnLogProxy
	{
	public:
		CnLogProxy();
		~CnLogProxy();

		void	Add( const MoString& strName, const MoString& strOutputPath = L"", bool bOutputDebug = true, bool bOutputConsole = false, bool bOpen = true );
		void	Remove();

		bool	IsNull() const;
		CnLog*	operator ->() const;

		void	WriteString( const wchar* Msg, ... );
		void	WriteWarning( const wchar* Msg, ... );
		void	WriteError( const wchar* strFileName, int nLine, const CnString& strMsg );

		static CnLogProxy&	Instance();

	private:
		CnLog*		m_pLog;

		static CnLogProxy*	ms_pSingleton;
	};

	#define CnPrint			CnLogProxy::Instance().WriteString
	#define CnWarning		CnLogProxy::Instance().WriteWarning
	#define CnError(desc)	CnLogProxy::Instance().WriteError(__WFILE__, __LINE__, desc)
}

#endif	// __CN_LOG_H__