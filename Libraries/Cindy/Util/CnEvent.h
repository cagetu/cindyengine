#pragma once

#include "CnObject.h"

namespace Cindy
{
	class Handler;

	//-----------------------------------------------------------------------------
	/**	@class		Event Interface
		@author		cagetu

		@brief		이벤트 기본 객체
	*/
	//-----------------------------------------------------------------------------
	class CN_DLL IEvent : public CnObject
	{
		CN_DECLARE_RTTI;
	public:
		virtual ~IEvent();

		//virtual void	SendSync( Handler* );
		//virtual void	SendASync( Handler* );
	};

	typedef Pointer<IEvent>	EventPtr;
}
