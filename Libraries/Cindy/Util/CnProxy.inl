//================================================================
// File:               : CnProxy.inl
// Related Header File : CnProxy.h
// Original Author     : changhee
// Creation Date       : 2007. 7. 18
//================================================================
//----------------------------------------------------------------
template <class T>
inline
CnProxy<T>::CnProxy()
: m_pObject(0)
{
}
//----------------------------------------------------------------
template <class T>
inline
CnProxy<T>::CnProxy( T* pObject )
: m_pObject( pObject )
{
}

//----------------------------------------------------------------
template <class T>
inline
CnProxy<T>::CnProxy( const CnProxy& rPtr )
: m_pObject( rPtr.m_pObject )
{
}

//----------------------------------------------------------------
template <class T>
inline
bool CnProxy<T>::IsNull() const
{
	return (0 == m_pObject);
}

//----------------------------------------------------------------
template <class T>
inline
CnProxy<T>& CnProxy<T>::operator= ( const CnProxy& rPtr )
{
	if (m_pObject != rPtr.m_pObject)
		m_pObject = rPtr.m_pObject;

	return *this;
}

//----------------------------------------------------------------
template <class T>
inline
T* CnProxy<T>::operator-> () const
{
	assert(m_pObject);
	return m_pObject;
}
//----------------------------------------------------------------
template <class T>
inline
T* CnProxy<T>::GetPtr() const
{
	assert(m_pObject);
	return m_pObject;
}