//================================================================
// File					: CnSingleton.h			
// Original Author		: Changhee
// Creation Date		: 2007. 8. 6.
//================================================================
#pragma once

//------------------------------------------------------------------------------
#define __DeclareSingleton(type) \
public: \
    static type * Singleton; \
    static type * Instance() { assert(0 != Singleton); return Singleton; }; \
    static bool HasInstance() { return 0 != Singleton; }; \
private:

#define __ImplementSingleton(type) \
    type * type::Singleton = 0;

#define __ConstructSingleton \
    assert(0 == Singleton); Singleton = this;

#define __DestructSingleton \
    assert(Singleton); Singleton = 0;

//------------------------------------------------------------------------------
//@todo : dll에서는 __declspec(thread) 키워드를 사용할 수 없다. 나중에 정적라이브러리
//		  로 만든다면, 그 때 고려하자!!!
//#define __DeclareSingleton(type) \
//public: \
//    __declspec(thread) static type * Singleton; \
//    static type * Instance() { assert(0 != Singleton); return Singleton; }; \
//    static bool HasInstance() { return 0 != Singleton; }; \
//private:
//
//#define CN_DECLARE_INTERFACE_SINGLETON(type) \
//public: \
//    static type * Singleton; \
//    static type * Instance() { assert(0 != Singleton); return Singleton; }; \
//    static bool HasInstance() { return 0 != Singleton; }; \
//private:
//
//#define __ImplementSingleton(type) \
//    __declspec(thread) type * type::Singleton = 0;
//
//#define CN_IMPLEMENT_INTERFACESINGLETON(type) \
//    type * type::Singleton = 0;
//
//#define __ConstructSingleton \
//    assert(0 == Singleton); Singleton = this;
//
//#define __ConstructInterfaceSingleton \
//    assert(0 == Singleton); Singleton = this;
//
//#define __DestructSingleton \
//    assert(Singleton); Singleton = 0;
//
//#define __DestructInterfaceSingleton \
//    assert(Singleton); Singleton = 0;
//------------------------------------------------------------------------------
