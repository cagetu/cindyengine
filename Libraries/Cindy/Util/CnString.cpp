//================================================================
// File					: CnString.cpp
// Related Header File	: CnString.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 22.
//================================================================
#include "CnString.h"

namespace Cindy
{
namespace unicode
{
	//const CnString BLANK = CnString( L"" );
	//==================================================================
	static wchar drive[5];
	static wchar dir[1024];
	static wchar name[128];
	static wchar ext[8];

	//------------------------------------------------------------------
	/** 문자열안의 공백 제거
		@desc
			MoStirng 내 문자열의 공백을 제거한다.
		@param str
			공백을 제거할 문자열.
	*/
	void Trim( CnString& str )
	{
		static const CnString strDel = CnString( L" \t\n\r" );
		str.erase( str.find_last_not_of( strDel ) + 1 );			// trim right
		str.erase( 0, str.find_first_not_of( strDel ) );			// trim left
	}

	//------------------------------------------------------------------
	/** 문자열안의 원하는 문자 제거
		@desc
			MoStirng 내 문자열의 특정문자를 제거한다.
		@param str
			문자열.
		@param strDel
			지울 문자
	*/
	void Trim( CnString& str, const CnString& strDel )
	{
		str.erase( str.find_last_not_of( strDel ) + 1 );			// trim right
		str.erase( 0, str.find_first_not_of( strDel ) );			// trim left
	}

	//------------------------------------------------------------------
	/** 소문자 변환
		@desc
			모든 영어 문자열을 소문자로 바꾼다.			
		@param str
			변환할 문자열.
	*/
	void LowerCase( CnString& str )
	{
		std::transform( str.begin(), str.end(), str.begin(), tolower );
	}
	
	//------------------------------------------------------------------
	/** 대문자 변환
		@desc
			모든 영어 문자열을 대문자로 바꾼다.			
		@param rStr
			변환할 문자열.
	*/
	void UpperCase( CnString& str )
	{
		std::transform( str.begin(), str.end(), str.begin(), toupper );
	}

	//------------------------------------------------------------------
	/** 문자열 분리
		@desc
			문자열을 원하는 문자열을 기준으로 분리한다.
		@param str
			분리를 원하는 문자열.
		@param strDels
			분리 기준의 문자열 리스트.
		@param usMaxSplits
			최대 분리 개수. 0이면 무제한이고 0보다 크면 그 수만큼만 분리한다.
		@return
			분리한 것을 문자열 리스트로 내보낸다. 정확하게 몇개가 나올지 모르기 때문에 벡터로 묶는다.
	*/
	CnStringArray Tokenize( const CnString& str, const CnString& strDels, ushort usMaxSplit )
	{
		CnStringArray out;
		ushort usNumSplit = 0;

		size_t ulStart = 0;
		size_t ulCur;

		do
		{
			ulCur = str.find_first_of( strDels, ulStart );
			if( ulCur == ulStart )
			{
				ulStart = ulCur + 1;
			}
			else if( ulCur == CnString::npos || ( usMaxSplit && usNumSplit == usMaxSplit ) )
			{
				out.push_back( str.substr( ulStart ) );
				break;
			}
			else
			{
				out.push_back( str.substr( ulStart, ulCur - ulStart ) );
				ulStart = ulCur + 1;
			}

			ulStart = str.find_first_not_of( strDels, ulStart );
			++usNumSplit;
		}while( ulCur != CnString::npos );

		return out;
	}

	//------------------------------------------------------------------
	/** 문자열 분리
		@desc
			문자열을 원하는 문자열을 기준으로 분리한다.
		@param str
			분리를 원하는 문자열.
		@param strDels
			분리 기준의 문자열 리스트.
		@param usMaxSplits
			최대 분리 개수. 0이면 무제한이고 0보다 크면 그 수만큼만 분리한다.
		@return
			분리한 것을 문자열 리스트로 내보낸다. 정확하게 몇개가 나올지 모르기 때문에 벡터로 묶는다.
	*/
	StringArray Tokenize( const std::string& str, const std::string& strDels, ushort usMaxSplit )
	{
		StringArray out;
		ushort usNumSplit = 0;

		size_t ulStart = 0;
		size_t ulCur;

		do
		{
			ulCur = str.find_first_of( strDels, ulStart );
			if( ulCur == ulStart )
			{
				ulStart = ulCur + 1;
			}
			else if( ulCur == CnString::npos || ( usMaxSplit && usNumSplit == usMaxSplit ) )
			{
				out.push_back( str.substr( ulStart ) );
				break;
			}
			else
			{
				out.push_back( str.substr( ulStart, ulCur - ulStart ) );
				ulStart = ulCur + 1;
			}

			ulStart = str.find_first_not_of( strDels, ulStart );
			++usNumSplit;
		}while( ulCur != std::string::npos );

		return out;
	}

	//------------------------------------------------------------------
	/** 경로명 분리
		@desc
			문자열이 파일 경로명까지 되어 있는 경우 드라이브, 디렉토리, 파일이름, 확장자로 분리한다.
		@param strPath
			경로명.
		@param strDrive
			[out] 경로명 중 드라이브 이름.
		@param strDir
			[out] 경로명 중 디렉토리 부분.
		@param strFileName
			[out] 경로명 중 파일 이름 부분.
		@param strExt
			[out] 경로명 중 파일의 확장자.
	*/
	void SplitPath( const CnString& strPath,
								  CnString& strDrive,
								  CnString& strDir,
								  CnString& strFileName,
								  CnString& strExt )
	{
		// 버퍼를 초과할 가능성 제거. 다른 루틴이 필요하다면 추후 수정
		if( CN_MAX_PATH_LENGTH < strPath.size() )
		{
			return;
		}

		_wsplitpath( strPath.c_str(), drive, dir, name, ext );

		strDrive = CnString( drive );
		strDir = CnString( dir );
		strFileName = CnString( name );
		strExt = CnString( ext );
	}

	//------------------------------------------------------------------
	/** 파일이름 받아오기
		@desc
			문자열이 파일 경로인 경우 파일명을 분리해낸다.			
		@param strPath
			경로명.
		@return rResult
			파일 경로명 중 파일명을 리턴한다. 만약 문자열이 경로명이 아니면 빈문자열을 반환한다.
		@note
			확장자는 리턴되지 않는다. 
	*/
	CnString SplitPathFileName( const CnString& strPath )
	{
		if( CN_MAX_PATH_LENGTH < strPath.size() )
			return CnString( L"" );

		_wsplitpath( strPath.c_str(), drive, dir, name, ext );
		return CnString( name );
	}

	//------------------------------------------------------------------
	/** 파일 확장자 받아오기
		@desc
			문자열이 파일 경로인 경우 확장자명을 분리해낸다.			
		@param strPath
			경로명.
		@return rResult
			파일 경로명 중 확장자명을 리턴한다. 만약 문자열이 경로명이 아니면 빈문자열을 반환한다.
	*/
	CnString SplitPathExt( const CnString& strPath )
	{
		if( CN_MAX_PATH_LENGTH < strPath.size() )
			return CnString( L"" );

		_wsplitpath( strPath.c_str(), drive, dir, name, ext );
		return CnString( ext );
	}

	//------------------------------------------------------------------
	/** 디렉토리 받아오기
		@desc
			문자열이 파일(디렉토리) 경로인 경우 디렉토리명을 분리해낸다.
		@param strPath	
			경로명.
		@return rResult
			파일(디렉토리) 경로명 중 디렉토리를 리턴한다. 만약 문자열이 경로명이 아니면 빈문자열을 반환한다.
	*/
	CnString SplitPathDir( const CnString& strPath )
	{
		if( CN_MAX_PATH_LENGTH < strPath.size() )
			return CnString( L"" );

		_wsplitpath( strPath.c_str(), drive, dir, name, ext );
		return CnString( dir );
	}

	//------------------------------------------------------------------
	/** 드라이브 받아오기
	@desc
		문자열이 파일(디렉토리) 경로인 경우 드라이브 명을 분리해낸다.
	@param strPath
		경로명.
	@return rResult
		파일(디렉토리) 경로명 중 드라이브 명을 리턴한다. 만약 문자열이 경로명이 아니면 빈문자열을 반환한다.
	*/
	CnString  SplitPathDrive( const CnString& strPath )
	{
		if( CN_MAX_PATH_LENGTH < strPath.size() )
			return CnString( L"" );

		_wsplitpath( strPath.c_str(), drive, dir, name, ext );
		return CnString( drive );
	}

	//------------------------------------------------------------------
	/** 확장자까지 포함된 파일이름 받아오기
		@desc
			문자열이 파일(디렉토리) 경로일 경우 확장자가 포함된 파일명을 분리해낸다.
		@param strPath
			경로명.
		@param rResult
			파일(디렉토리) 경로명 중 파일명과 확장자명 리턴한다. 만약 문자열이 경로명이 아니면 빈문자열을 반환한다.
	*/
	CnString SplitPathFullFileName( const CnString& strPath )
	{
		if( CN_MAX_PATH_LENGTH < strPath.size() )
			return  CnString( L"" );

		_wsplitpath( strPath.c_str(), drive, dir, name, ext );
		return CnString(name) + CnString(ext);
	}

	//---------------------------------------------------------------------
	/** 정수를 스트링으로 변환
		@param value
			정수값을 넘겨준다.
		@param rResult
			문자열로 변환된 값을 넘겨준다.
	*/
	CnString ToString( const int nValue )
	{
		wchar_t buff[256];
		_itow( nValue, buff, 10 );
		return CnString( buff );
	}

	//---------------------------------------------------------------------
	/** 부동 소수를 스트링으로 변환
		@param value
			부동 소수값을 넘겨준다.
		@param rResult
			문자열로 변환된 값을 넘겨준다.
	*/
	CnString ToString( const float fValue )
	{
		wchar_t buff[256];
		swprintf( buff, L"%f", fValue );
		return CnString( buff );
	}

	//------------------------------------------------------------------
	/** 문자열(CnString) 변환기
	*/
	CnString ToString( const wchar* pFormat, ... )
	{
		va_list args;
		
		wchar buffer[128];
		wmemset( buffer, 0x00, 128 );

		va_start( args, pFormat );
		int len = vswprintf( buffer, pFormat, args );
		va_end( args );

		buffer[len] = L'\0';

		return CnString( buffer );
	}

	//------------------------------------------------------------------
	/** Float으로 변환
	*/
	float ToFloat( const CnString& str )
	{
		return float(::_wtof(str.c_str()));
	}

	//------------------------------------------------------------------
	/** int으로 변환
	*/
	int ToInt( const CnString& str )
	{
		return int(::_wtoi(str.c_str()));
	}

	//------------------------------------------------------------------
	/** bool으로 변환
	*/
	bool ToBool( const CnString& str )
	{
		//-----------------------------------------------*/
		static const wchar* bools[] = {
			L"no", L"yes", L"off", L"on", L"false", L"true", L"0", L"1", 0
		};
		//-----------------------------------------------

		unsigned int i = 0;
		while (bools[i] != 0)
		{
			if (0 == wcscmp(bools[i], str.c_str()))
			{
				return 1 == (i & 1);
			}
			i++;
		}
		//assert("Invalid string value for bool!");
		return false;
	}

	//------------------------------------------------------------------
	/** 문자열을 유니코드로 변환한다.
		@param pString
			멀티바이트 문자열
		@param rResult
			유니코드 문자열
	*/
	void CharToWChar( const char* pString, CnString& rResult )
	{
		static wchar tmp[2048];
		wmemset( tmp, 0x00, 2048 );
		int n = swprintf( tmp, L"%S", pString );
		tmp[n] = L'\0';

		rResult = CnString( tmp );
	}

	//------------------------------------------------------------------
	/** 유니코드 문자열을 멀티바이트 문자열로 변환한다.
		@param strString
			유니코드 문자열
	*/
	void WCharToChar( const CnString& strString, char* pResult )
	{
		int len = 0;
		len = (int)(wcslen( strString.c_str() ) + 1) * 2;

		WideCharToMultiByte( CP_ACP, 0, strString.c_str(), -1, (LPSTR)pResult, len, NULL, NULL);
	}

	//------------------------------------------------------------------
	/** Hash Code 생성하기
		@remarks
			변경된 해쉬 코드로 map을 이용하면, <B>'해쉬코드가 문자열의 순서와 일치하지는 않는다.'</B>
	*/
	ulong GetHashCode( const char* pString )
	{
		size_t i,len;
		unsigned long ch;
		unsigned long result;

		len = strlen( pString );
		result = 5381;
		for( i=0; i<len; i++ )
		{
			ch = (unsigned long)pString[i];
			result = ((result<< 5) + result) + ch; // hash * 33 + ch
		}

		return result;
	}

	//------------------------------------------------------------------
	/** Hash Code 생성하기
		@remarks
			변경된 해쉬 코드로 map을 이용하면, <B>'해쉬코드가 문자열의 순서와 일치하지는 않는다.'</B>
	*/
	ulong GetHashCode( const wchar* pWString )
	{
		static char buffer[512];
		memset( buffer, 0, 512 );

		WCharToChar( pWString, buffer );
		return GetHashCode( buffer );
	}

	//------------------------------------------------------------------
	/** 비교
	*/
	bool Compare( const wchar* src, const wchar* dst )
	{
		return (0 == wcscmp( src, dst )) ? true : false;
	}
}
}