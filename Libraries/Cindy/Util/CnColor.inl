//================================================================
// File:               : CnColor.inl
// Related Header File : CnColor.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 29
//================================================================

/// Const/Dest
inline
CnColor::CnColor( float fR, float fG, float fB, float fA )
: r(fR)
, g(fG)
, b(fB)
, a(fA)
{
}
inline
CnColor::CnColor( RGBA color )
{
	SetRGBA( color );
}

//================================================================
//================================================================
inline
bool CnColor::operator == ( const CnColor& color ) const
{
	return ( r == color.r && g == color.g && b == color.b && a == color.a );
}

//-------------------------------------------------------------------------
inline
bool CnColor::operator != ( const CnColor& color ) const
{
	return !(*this == color);
}

//================================================================
//================================================================
inline
CnColor CnColor::operator + ( const CnColor& color ) const
{
	CnColor c = (*this);
	c.r += color.r;
	c.g += color.g;
	c.b += color.b;
	c.a += color.a;
	return c;
}

//-------------------------------------------------------------------------
inline
CnColor CnColor::operator - ( const CnColor& color ) const
{
	CnColor c = (*this);
	c.r -= color.r;
	c.g -= color.g;
	c.b -= color.b;
	c.a -= color.a;
	return c;
}

//-------------------------------------------------------------------------
inline
CnColor CnColor::operator * ( const CnColor& color ) const
{
	CnColor c = (*this);
	c.r *= color.r;
	c.g *= color.g;
	c.b *= color.b;
	c.a *= color.a;
	return c;
}

//-------------------------------------------------------------------------
inline
CnColor CnColor::operator * ( float fValue ) const
{
	CnColor c = (*this);
	c.r *= fValue;
	c.g *= fValue;
	c.b *= fValue;
	c.a *= fValue;
	return c;
}

//-------------------------------------------------------------------------
inline
CnColor CnColor::operator / ( const CnColor& color ) const
{
	CnColor c = (*this);
	c.r /= color.r;
	c.g /= color.g;
	c.b /= color.b;
	c.a /= color.a;
	return c;
}

//-------------------------------------------------------------------------
inline
CnColor CnColor::operator / ( float fValue ) const
{
	CnColor c = (*this);
	c.r /= fValue;
	c.g /= fValue;
	c.b /= fValue;
	c.a /= fValue;
	return c;
}

//================================================================
//================================================================
inline
CnColor& CnColor::operator += ( const CnColor& color )
{
	r += color.r;
	g += color.g;
	b += color.b;
	a += color.a;
	return *this;
}

//-------------------------------------------------------------------------
inline
CnColor& CnColor::operator -= ( const CnColor& color )
{
	r -= color.r;
	g -= color.g;
	b -= color.b;
	a -= color.a;
	return *this;
}

//-------------------------------------------------------------------------
inline
CnColor& CnColor::operator *= ( const CnColor& color )
{
	r *= color.r;
	g *= color.g;
	b *= color.b;
	a *= color.a;
	return *this;
}

//-------------------------------------------------------------------------
inline
CnColor& CnColor::operator *= ( float fValue )
{
	r *= fValue;
	g *= fValue;
	b *= fValue;
	a *= fValue;
	return *this;
}

//-------------------------------------------------------------------------
inline
CnColor& CnColor::operator /= ( const CnColor& color )
{
	r /= color.r;
	g /= color.g;
	b /= color.b;
	a /= color.a;
	return *this;
}

//-------------------------------------------------------------------------
inline
CnColor& CnColor::operator /= ( float fValue )
{
	r /= fValue;
	g /= fValue;
	b /= fValue;
	a /= fValue;
	return *this;
}

//================================================================
//================================================================
inline
CnColor CnColor::operator - () const
{
	return CnColor( -r, -g, -b, -a );
}

//================================================================
//================================================================
inline
CnColor operator * ( float fValue, const CnColor& color )
{
    return color * fValue;
}

//================================================================
//================================================================
inline
void CnColor::Lerp( const CnColor& c1, const CnColor& c2, float s )
{
    this->r = c1.r + s * (c2.r - c1.r);
    this->g = c1.g + s * (c2.g - c1.g);
    this->b = c1.b + s * (c2.b - c1.b);
    this->a = c1.a + s * (c2.a - c1.a);	
}