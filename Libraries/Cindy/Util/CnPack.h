// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CN_PACK_H__
#define __CN_PACK_H__

#include "../CindyTypes.h"

namespace Cindy
{
	//---------------------------------------------------------------
	/** Fixed point / floating point / integer conversion macros
		4.12 fixed point란?
			4비트로 부호와 정수를 표현하고, 12비트로 소수점이하의 자리를 표현하는 16비트 실수
	*/
	//---------------------------------------------------------------
	#define inttof32(n)          ((n) << 12)
	#define f32toint(n)          ((n) >> 12)
	#define floattof32(n)        ((f32)((n) * (1 << 12)))
	#define f32tofloat(n)        (((float)(n)) / (float)(1<<12))

	typedef short int t16;       // text coordinate 12.4 fixed point
	#define f32tot16(n)          ((t16)(n >> 8))
	#define inttot16(n)          ((n) << 4)
	#define t16toint(n)          ((n) >> 4)
	#define floattot16(n)        ((t16)((n) * (1 << 4)))
	#define TEXTURE_PACK(u,v)    (((u) << 16) | (v & 0xFFFF))

	typedef short int v16;       // vertex 4.12 fixed format
	#define inttov16(n)          ((n) << 12)
	#define f32tov16(n)          (n)
	#define v16toint(n)          ((n) >> 12)
	#define floattov16(n)        ((v16)((n) * (1 << 12)))
	#define v16tofloat(n)		 (((float)(n)) / (float)(1 << 12))
	#define VERTEX_PACK(x,y)	 (((y) << 16) | ((x) & 0xFFFF))

	typedef short int v10;       // vertex .10 fixed point
	#define inttov10(n)          ((n) << 9)
	#define f32tov10(n)          ((v10)(n >> 3))
	#define v10toint(n)          ((n) >> 9)
	#define floattov10(n)        ((v10)((n) * (1 << 9)))
	#define NORMAL_PACK(x,y,z)   (((x) & 0x3FF) | (((y) & 0x3FF) << 10) | ((z) << 20))

	// float -> Fixed 3.13
	float16 f32Tof16( float32 Value );
	float32 f16Tof32( float16 Value );

	//
	uint16	f32_16( float32 t );
	float32 f16_32( uint16 t );

	// float -> unsigned char
	uint8	f32_u8( float32 t );

	// float4->ubyte4
}

#include "CnPack.inl"

#endif	// __CN_PACK_H__