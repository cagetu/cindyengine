//================================================================
// File:           : CnKey.h
// Original Author : changhee
// Creation Date   : 2009. 1. 15
//================================================================
#ifndef __CN_KEY_H__
#define __CN_KEY_H__

#include "../Cindy.h"

namespace Cindy
{
#pragma pack(push, 1)
	//struct Key16
	//{
	//	union
	//	{
	//		struct
	//		{
	//			u8 char1 : 1;
	//			u8 char2 : 1;
	//		};
	//		u16 key;
	//	};

	//	void operator =( u16 value )
	//	{
	//		key = value;
	//	}

	//	operator u16() const
	//	{
	//		return key;
	//	}
	//};

	//struct Key32
	//{
	//	union
	//	{
	//		struct
	//		{
	//			u16 short1 : 2;
	//			u16 short2 : 2;
	//		};
	//		u32 key;
	//	};

	//	void operator =( u32 value )
	//	{
	//		key = value;
	//	}

	//	operator u32() const
	//	{
	//		return key;
	//	}
	//};

//	struct Key64
//	{
//		union
//		{
////#pragma TODO("뒤의 비트 부터 채워나갈 수도 있다..... check해보세요." )
//			struct
//			{
//				unsigned short1 : 2;	// 2 bits
//				unsigned short2 : 2;	// 2 bits
//				unsigned short3 : 2;	// 2 bits
//				unsigned short4 : 2;	// 2 bits
//
//				//unsigned unused : 56;	// unused 56 bits
//			};
//			uint64	key;
//		};
//
//		void operator =( uint64 value )
//		{
//			key = value;
//		}
//
//		operator uint64()
//		{
//			return key;
//		}
//	};

#pragma pack(pop)

}

#endif	// __CN_KEY_H__