#ifndef __CN_FOURCC_H__
#define __CN_FOURCC_H__

#include "../Cindy.h"

namespace Cindy
{
	//-----------------------------------------------------------------------------
	/**	@class		Four Character Code 객체
		@author		cagetu

		@brief		영문 알파벳 네 글자로 데이터를 구분하므로 사람이 읽기 쉽고, 외우기도 쉽다.
					4 바이트로 구성되어, 아직까지는 32비트 시스템에 적합하다.
					하지만 엔디안 문제 때문에 읽기가 힘든 경우도 있다.
					예를 들면, FourCC가 'abcd'인 경우에 리틀 엔디안 시스템에서는 "0x64636261"로 표현한다 

					http://ko.wikipedia.org/wiki/FourCC
					Nebula3 : Util/fourCC
	*/
	//-----------------------------------------------------------------------------
	class CN_DLL FourCC
	{
	public:
		FourCC();
		FourCC( uint f );
		FourCC( const std::string& s );

		bool operator == (const FourCC& rhs) const;
		bool operator != (const FourCC& rhs) const;
		bool operator < (const FourCC& rhs) const;
		bool operator <= (const FourCC& rhs) const;
		bool operator > (const FourCC& rhs) const;
		bool operator >= (const FourCC& rhs) const;

		bool IsValid() const;

		void SetFromUInt( uint f );
		uint AsUInt() const;

		void SetFromString( const std::string& s );
		std::string AsString() const;

		static std::string ToString( const FourCC& f );
		static FourCC FromString( const std::string& s );

	private:
		uint	m_FourCC;
	};

#include "CnFourCC.inl"
}

#endif	// __CN_FOURCC_H__