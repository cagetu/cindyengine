//================================================================
// File:           : CnShaderDefines.h
// Original Author : changhee
// Creation Date   : 2007. 2. 12
//================================================================
#ifndef __CN_SHADER_DEFINES_H__
#define __CN_SHADER_DEFINES_H__

namespace Cindy
{
#ifndef ShaderHandle
	typedef const char*	ShaderHandle;
#endif

//================================================================
/** ShaderState
    @author    changhee
	@since     2007. 2. 12
	@remarks   쉐이더(.fx) 에서 사용되는 파라미터와 타입등을 정의한다.
			   Nebula2를 참고하였음..
*/
//================================================================
namespace ShaderState
{
	enum Param
	{
		// Transform
		ModelViewProjection = 0,	// matrix: model * view * projection matrix
		ModelView,					// matrix: model * view 
		Model,						// matrix: model
		View,						// matrix: view
		Projection,					// matrix: projection
		ViewProjection,				// matrix: view * projection matrix
		InvModelViewProjection,		// matrix: inverse (model * view * projection) matrix
		InvView,					// matrix: inverse view matrix
		InvTransposedView,			// matrix: inverse transposed view matrix
		ModelLightProjTexture,		// matrix: model * view * shadowMap matrix
		LightProjTexture,			// matrix: view * shadowMap matrix
		// View
		EyePosition,				// vector: the eye position in local space
		ViewDirection,				// vector: the view direction
		WorldEyePosition,			// vecotr :world camera pos
		// Ambient
		GlobalAmbient,				// color: Ambient Lighting = Material_ambient* Global_ambient + sum( Light_ambient* Material_ambient ) )
		// Material
		MatAmbient,					// color: material ambient
		MatDiffuse,					// color: material diffuse
		MatSpecular,				// color: material specular
		MatEmissive,				// color: material emissive
		MatSpecularIntensity,		// float
		MatEmissiveIntensity,		// float
		MatGlowIntensity,			// float
		// Sky(GlobalLight)
		LightDirection,				// vector : light direction
		// AreaLight
		LightCount,
		LightPosition,
		LightDiffuse,
		LightSpecular,
		LightAttenuation,
		LightRange,
		LightPosArray,
		LightColorArray,
		LightRangeArray,
		// AlphaBlend
		AlphaSrcBlend,              // int: Alpha Source Blend Factor
		AlphaDstBlend,              // int: Alpha Dest Blend Factor
		// Cull
		CullMode,                   // int: cull mode (1 = No culling, 2 = CW, 3 = CCW)
		// Skinned
		JointPalette,               // matrix array: joint palette for skinning
		SkinBlend,					// vertex blend count
		// ModelTextures
		DiffuseMap0,				// texture : texcoord 0 diffuse map
		DiffuseMap1,				// texture : texcoord 1 diffuse map
		DiffuseMap2,				// texture : texcoord 2 diffuse map
		DiffuseMap3,				// texture : texcoord 3 diffuse map
		DiffuseMap4,				// texture : texcoord 4 diffuse map
		DiffuseMap5,				// texture : texcoord 5 diffuse map
		DiffuseMap6,				// texture : texcoord 6 diffuse map
		DiffuseMap7,				// texture : texcoord 7 diffuse map
		SpecularMap,				// texture : specular map
		SpecularExpMap,				// texture : specular exponent Map
		EmissiveMap,				// texture : Emissive Map
		GlowMap,					// texture : glow map
		NormalMap,					// texture : normal map
		ShadowMap,					// texture : shadow map
		CelShadeMap,				// texture : toon shading map
		OcclusionMap,				// texture : AmbientOcclusion map
		CubeMap0,					// texture : cube map 
		ReflectMap,					// texture : 
		DepthBuffer,				// texture : depth buffer
		AlphaMap,					//
		RandomMap,					// texture : random map (3d or 2d)
		NoiseMap,					// texture : noise texture
		HairShiftMap,
		HairSpecMaskMap,
		HistoryAOBuffer,			// texture : 
		VextexTexture0,
		VextexTexture1,
		VextexTexture2,
		VextexTexture3,
		///
		LightBuffer,
		NormalBuffer,
		GBuffer,
		// PostEffect
		SourceBuffer,
		BloomBuffer,
		ColorBuffer,
		LuminanceBuffer,
		RenderTargetSize,			// color buffer size
		SourceSize,					// SourceSize
		// Filter
		FilterSampleOffsets,		// uv[]
		FilterSampleWeights,		// vector4[]
		// Res
		DisplayResolution,			// float2 : window resolution
		// DOF
		DofParameter,				// float4 : Depth of field 의 값
		// Deferred
		ViewInfo,					// float4 : ViewAspect, FOV, nearPlane, farPlane
		ScreenUVAdjust,				// float2 : 0.5 / screenDimension.xy
		ScreenProj,
		FrustumCorners,				// 
		// Object-MotionBlur
		ViewProjInv,				// 
		PrevViewProj,
		// GrainFilter
		TimeValue,					// float
		ElapsedTime,				// float
		// LightRays
		SunLightPosition,			// float4
		// SSAO
		SSAOBlurOffsets,			//
		SSAOBlurWeights,			//
		// PSSM
		PSSMNumSplits,
		GlobalPSSMDistances,
		GlobalPSSMTransforms,
		GlobalPSSMBuffer0,
		GlobalPSSMBuffer1,
		GlobalPSSMBuffer2,
		GlobalPSSMBuffer3,
		// Count
		NumParameters,
		InvalidParameter
	};

	enum Format
	{
		Void,
		Bool,
		Int,
		Float,
		Float4,
		Matrix44,
		Texture,

		NumTypes,
		InvalidType
	};

	// Type
//		Type			StringToType( const char* Type );
//		const char*		TypeToString();

	// Param
	Param			StringToParam( const char* name );
	const char*		ParamToString( Param param );
} // end of namespace ShaderState
} // end of namespace Cindy

#endif	// __CN_SHADER_DEFINES_H__