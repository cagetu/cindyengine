//================================================================
// File:           : CnTexture.h
// Original Author : changhee
// Creation Date   : 2007. 4. 9
//================================================================
#pragma once

#include "Resource/CnResource.h"
#include "Graphics/Base/CnPixelFormat.h"

namespace Cindy
{
	//================================================================
	/** CnTexture
	    @author    changhee
		@since     2007. 4. 9
		@remarks   
	*/
	//================================================================
	class CnTexture : public CnResource
	{
		__DeclareRtti;
	public:
		// texture type
		enum Style
		{
			TEXTURE_UNKNOWN,

			TEXTURE_2D,
			TEXTURE_3D,
			TEXTURE_CUBE,
		};

		// the sides of a cube map
		enum CubeFace
		{
			PosX = 0,
			NegX,
			PosY,
			NegY,
			PosZ,
			NegZ,
		};

		// usage flags
		enum Usage
		{
			CreateEmpty					= (1<<0),	// don't load from disk, instead create empty texture
			CreateFromDDSFile			= (1<<1),	// create from dds file inside a compound file
			RenderTargetColor			= (1<<2),	// is render target, has color buffer
			RenderTargetDepthStencil	= (1<<3),	// is render target, has depth buffer
			Dynamic						= (1<<4),	// is a dynamic texture (for write access with CPU)
			GenerateMipMaps				= (1<<5),	// is enabled mipmaps
		};

		// lock types
		enum LockType
		{
			ReadOnly,       // cpu will only read from texture
			WriteOnly,      // cpu will only write to texture (an overwrite everything!)
		};

		// lock information
		struct LockInfo
		{
			void* surfPointer;
			int   surfPitch;
		};

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual ~CnTexture();

		// Load/Unload
		bool				Load( const wchar*strFileName ) override;
		bool				Unload() override;

		// Lost/Restore
		void				OnLost() override;
		void				OnRestore() override;

		// Style
		void				SetStyle( Style eStyle );
		Style				GetStyle() const;

		// Format
		void				SetFormat( PixelFormat::Code eFormat );
		PixelFormat::Code	GetFormat() const;
		ushort				GetBytesPerPixel() const;

		// Information
		void				SetWidth( ushort usWidth );
		ushort				GetWidth() const;

		void				SetHeight( ushort usHeight );
		ushort				GetHeight() const;

		void				SetDepth( ushort usDepth );
		ushort				GetDepth() const;

		// Usage
		void				SetUsage( ushort usUsage );
		ushort				GetUsage() const;

		// Mip-Map
		ushort				GetNumMipLevels() const;
		virtual void		GenerateMipLevels();

		// Get Texture
		virtual void		GetBaseTexture( void* pBaseTexture ) {;}
		virtual void		GetTexture( void* pTexture ) {;}
		virtual void		GetSurface( void* pSurface ) {;}
		virtual void		GetDepthStencil( void* pStencil ) {;}

		// RenderTarget
		bool				IsRenderTarget() const;

	protected:
		friend class CnTextureManager;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		Style				m_eStyle;
		PixelFormat::Code	m_eFormat;

		ushort				m_usUsage;

		ushort				m_usWidth;				// 가로 사이즈
		ushort				m_usHeight;				// 세로 사이즈
		ushort				m_usDepth;				// 깊이

		ushort				m_usNumMipMaps;			// 밉맵 레벨

		ulong				m_ulColorKey;			// 칼라키 값(지정 된 색상을 뺀다는..)

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		// Constructor
		CnTexture( CnResourceManager* pParent );
	};
	
	//==================================================================
	/** TexPtr
		@author			cagetu
		@since			2007년 7월 18일
		@remarks		Texture 파일용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL TexPtr : public Ptr<CnTexture>
	{
	public:
		TexPtr();
		explicit TexPtr( CnTexture* pObject );
		TexPtr( const TexPtr& spObject );
		TexPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		TexPtr& operator= ( const RscPtr& spRsc );
	};
#include "CnTexture.inl"
}