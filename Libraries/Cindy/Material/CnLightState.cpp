//================================================================
// File:               : CnLightState.cpp
// Related Header File : CnLightState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnLightState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnLightState, CnGlobalState);
	// Const/Dest
	CnLightState::CnLightState()
		: m_nEnableLight(0)
	{
	}
}