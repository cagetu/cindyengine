//================================================================
// File:           : CnMaterialState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_MATERIAL_STATE_H__
#define __CN_MATERIAL_STATE_H__

#include "Util/CnColor.h"
#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** Material State
	    @author    changhee
		@since     2007. 2. 21
		@remarks   재질 속성에 대한 정보
				   ( Ambient, Diffuse, Specular, Shinness )
	*/
	//================================================================
	class CN_DLL CnMaterialState //: public CnGlobalState
	{
		//__DeclareClass(CnMaterialState);
	public:
		CnMaterialState();
		//virtual ~CnMaterialState();
		~CnMaterialState();

		CnGlobalState::TYPE			GetStateType() const	{	return CnGlobalState::MATERIAL;	}

		// Ambient
		void			SetAmbient( const CnColor& rColor );
		const CnColor&	GetAmbient() const;

		// Diffuse
		void			SetDiffuse( const CnColor& rColor );
		const CnColor&	GetDiffuse() const;

		// Specular
		void			SetSpecular( const CnColor& rColor );
		const CnColor&	GetSpecular() const;

		// Emissive
		void			SetEmissive( const CnColor& rColor );
		const CnColor&	GetEmissive() const;

		// Shinness
		void			SetShininess( float fValue );
		float			GetShininess() const;

		//
		void			SetSpecularIntensity( float fIntensity );
		float			GetSpecularIntensity() const;

		void			SetEmissiveIntensity( float fIntensity );
		float			GetEmissiveIntensity() const;

		void			SetGlowIntensity( float fIntensity );
		float			GetGlowIntensity() const;
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnColor		m_crAmbient;
		CnColor		m_crDiffuse;
		CnColor		m_crSpecular;
		CnColor		m_crEmissive;

		float		m_fShininess;

		float		m_fSpecularIntensity;
		float		m_fEmissiveIntensity;
		float		m_fGlowIntensity;
	};

#include "CnMaterialState.inl"
}

#endif	// __CN_MATERIAL_STATE_H__