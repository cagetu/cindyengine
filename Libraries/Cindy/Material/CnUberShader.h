//================================================================
// File:           : CnUberShader.h
// Original Author : changhee
// Creation Date   : 2009. 5. 19
//================================================================
#ifndef __CN_UBER_SHADER_H__
#define __CN_UBER_SHADER_H__

#include "CnShader.h"
#include "Foundation/CnObject.h"

namespace Cindy
{
	//================================================================
	/** UberShader
		@author		changhee
		@since		2009. 5. 19
		@brief		Shader Ŭ����
	*/
	//================================================================
	class CnUberShader : public CnShader
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------------------
		CnUberShader( CnResourceManager* pParent );
		virtual ~CnUberShader();

		bool			Load( const wchar* strFileName ) override;
		bool			Unload() override;

		void			Apply( ShaderFeature::Bits Mask ) override;

		CnShaderEffect* GetEffect( ShaderFeature::Bits Mask ) const override;

	private:
		typedef std::map<ShaderFeature::Bits, CnShaderEffect*> EffectMap;

		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		//uchar*	m_UberShaderCode;
		std::string m_UberShaderCode;
		EffectMap	m_ShaderEffects;

		//------------------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------------------
		CnShaderEffect*	Create( ShaderFeature::Bits Mask );

		void Clear();

	};

} // end of namespace

#endif	// __CN_UBER_SHADER_H__