//================================================================
// File:               : CnFogState.cpp
// Related Header File : CnFogState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnFogState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnFogState, CnGlobalState);
	// Const/Dest
	CnFogState::CnFogState()
		: m_nEnableFog(0)
	{
	}
}