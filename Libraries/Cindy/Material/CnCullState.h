//================================================================
// File:           : CnCullState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_CULL_STATE_H__
#define __CN_CULL_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** RenderState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnCullState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		enum CullMode
		{
			CULL_NONE	= 1,
			CULL_CW		= 2,
			CULL_CCW	= 3,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnCullState();

		// Type
		TYPE		GetStateType() const	{	return CULL;		}

		// Cull Mode
		void		SetCullMode( CullMode eMode );
		CullMode	GetCullMode() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CullMode	m_eCullMode;
	};

	//----------------------------------------------------------------
	inline
	void CnCullState::SetCullMode( CnCullState::CullMode eMode )
	{
		m_eCullMode = eMode;
	}
	inline
	CnCullState::CullMode CnCullState::GetCullMode() const
	{
		return m_eCullMode;
	}

}

#endif	// __CN_CULL_STATE_H__