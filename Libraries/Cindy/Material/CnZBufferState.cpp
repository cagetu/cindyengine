//================================================================
// File:               : CnZBufferState.cpp
// Related Header File : CnZBufferState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnZBufferState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnZBufferState, CnGlobalState);
	// Const/Dest
	CnZBufferState::CnZBufferState()
		: m_eZBuffer(ZB_USEZBUFFER)
		, m_eZFunc(FUNC_GREATEREQUAL)
		, m_nEnableZBuffer(1)
	{
	}
}