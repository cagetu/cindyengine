//================================================================
// File:               : CnTexture.cpp
// Related Header File : CnTexture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 9
//================================================================
#include "../Cindy.h"
#include "CnTexture.h"
#include "CnTextureManager.h"

#include "Util/CnLog.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnTexture, CnResource );

	//================================================================
	/** 생성자
		@remarks	이 리소스를 관리하는 매니져만 생성 가능하다.
		@param pParent :	부모 관리자
	*/
	//================================================================
	CnTexture::CnTexture( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_eStyle( TEXTURE_UNKNOWN )
		, m_eFormat( PixelFormat::NOFORMAT )
		, m_usUsage( CreateFromDDSFile )
		, m_usWidth( 0 )
		, m_usHeight( 0 )
		, m_usDepth( 0 )
		, m_usNumMipMaps( 0 )
		, m_ulColorKey( 0 )
	{
	}
	CnTexture::~CnTexture()
	{
	}

	//================================================================
	/** Load
	    @remarks      텍스쳐 로딩
		@param        strFileName : 파일 이름
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnTexture::Load( const wchar* strFileName )
	{
		return true;
	}

	//================================================================
	/** Unload
	    @remarks      텍스쳐 해제
		@param        none
		@return       none
	*/
	//================================================================
	bool CnTexture::Unload()
	{
		return true;
	}

	//================================================================
	/** OnLost
	    @remarks      윈도우 크기가 변경되는 등 디바이스가 리셋 되었을 때 처리
		@param		  none
		@return       none
	*/
	//================================================================
	void CnTexture::OnLost()
	{
	}

	//================================================================
	/** OnRestore
	    @remarks      디바이스가 복원되었을 경우 처리
		@param		  none
		@return       none
	*/
	//================================================================
	void CnTexture::OnRestore()
	{
	}

	//================================================================
	/**
	*/
	//================================================================
	void CnTexture::GenerateMipLevels()
	{
	}

	//================================================================
	/** Get Bytes Per Pixel
		@remarks		텍스쳐 의 픽셀 당 바이트 수를 얻어온다.
	*/
	//================================================================
	ushort CnTexture::GetBytesPerPixel() const
	{
		switch (m_eFormat)
		{
		case PixelFormat::X8R8G8B8:  
		case PixelFormat::A8R8G8B8:
			return 4;

		case PixelFormat::R5G6B5:
		case PixelFormat::A1R5G5B5:
		case PixelFormat::A4R4G4B4:
			return 2;

		case PixelFormat::P8:
		case PixelFormat::A8:
			return 1;

		case PixelFormat::DXT1:
		case PixelFormat::DXT2:
		case PixelFormat::DXT3:
		case PixelFormat::DXT4:
		case PixelFormat::DXT5:
			CnError( L"[CnTexture::GetBytesPerPixel] compressed pixel format!" );
			return 1;

		case PixelFormat::R16F:
			return 2;

		case PixelFormat::G16R16F:
			return 4;

		case PixelFormat::A16B16G16R16F:
			return 8;

		case PixelFormat::R32F:
			return 4;

		case PixelFormat::G32R32F:
			return 8;

		case PixelFormat::A32B32G32R32F:
			return 16;

		default:
			CnError( L"[CnTexture::GetBytesPerPixel] invalid pixel format!" );
			assert(0);
			return 1;
		}
	}
}