//================================================================
// File:           : CnStencilState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_STENCIL_STATE_H__
#define __CN_STENCIL_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** Stencil State
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnStencilState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnStencilState();

		// Type
		TYPE		GetStateType() const	{	return STENCIL;		}
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
	};

}

#endif	// __CN_STENCIL_STATE_H__