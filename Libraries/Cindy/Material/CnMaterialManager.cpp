//================================================================
// File:               : CnMaterialManager.cpp
// Related Header File : CnMaterialManager.h
// Original Author     : changhee
// Creation Date       : 2007. 5. 21
//================================================================
#include "../Cindy.h"
#include "CnMaterialManager.h"
#include "CnMaterial.h"

namespace Cindy
{
	//================================================================
	IMPLEMENT_RTTI( Cindy, CnMaterialManager, CnResourceManager );

	///Const/Dest
	CnMaterialManager::CnMaterialManager()
	{
	}
	CnMaterialManager::~CnMaterialManager()
	{
	}

	//----------------------------------------------------------------------------
	/** Load
		@remarks		리소스를 읽는다.
	*/
	//----------------------------------------------------------------------------
	RscPtr CnMaterialManager::Load( const wchar* strFileName )
	{
		Lock lock( *this );

		RscPtr spRsc = GetRsc( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnMaterial* rsc = new CnMaterial( this );
		if( rsc->Load( strFileName ) )
		{
			AddRsc( rsc );
		}
		else
		{
			SAFEDEL( rsc );
		}

		return rsc;
	}

	//----------------------------------------------------------------------------
	/** New
		@remarks		리소스를 생성
	*/
	//----------------------------------------------------------------------------
	RscPtr CnMaterialManager::New( const CnString& strFileName )
	{
		Lock lock( *this );

		RscPtr spRsc = GetRsc( strFileName );
		if ( !spRsc.IsNull() )
			return RscPtr();

		CnMaterial* rsc = new CnMaterial( this );
		AddRsc( rsc );

		return rsc;
	}
}