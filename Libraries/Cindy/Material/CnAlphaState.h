//================================================================
// File:           : CnAlphaState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_ALPHA_STATE_H__
#define __CN_ALPHA_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** AlphaState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성 - 알파 상태
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnAlphaState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		enum BlendMode
		{
			BLEND_ZERO               = 1,
			BLEND_ONE                = 2,
			BLEND_SRCCOLOR           = 3,
			BLEND_INVSRCCOLOR        = 4,
			BLEND_SRCALPHA           = 5,
			BLEND_INVSRCALPHA        = 6,
			BLEND_DESTALPHA          = 7,
			BLEND_INVDESTALPHA       = 8,
			BLEND_DESTCOLOR          = 9,
			BLEND_INVDESTCOLOR       = 10,
			BLEND_SRCALPHASAT        = 11,
			BLEND_BOTHSRCALPHA       = 12,
			BLEND_BOTHINVSRCALPHA    = 13,
			BLEND_BLENDFACTOR        = 14, /* Only supported if D3DPBLENDCAPS_BLENDFACTOR is on */
			BLEND_INVBLENDFACTOR     = 15, /* Only supported if D3DPBLENDCAPS_BLENDFACTOR is on */
		};

		enum BlendOp
		{
			BLENDOP_NONE			 = 0,
			BLENDOP_ADD              = 1,
			BLENDOP_SUBTRACT         = 2,
			BLENDOP_REVSUBTRACT      = 3,
			BLENDOP_MIN              = 4,
			BLENDOP_MAX              = 5,
		};

		enum ZFunc
		{
			FUNC_NEVER                = 1,
			FUNC_LESS                 = 2,
			FUNC_EQUAL                = 3,
			FUNC_LESSEQUAL            = 4,
			FUNC_GREATER              = 5,
			FUNC_NOTEQUAL             = 6,
			FUNC_GREATEREQUAL         = 7,
			FUNC_ALWAYS               = 8,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnAlphaState();

		// Type
		TYPE		GetStateType() const	{	return ALPHA;		}

		// Alpha Blend Enable
		void		SetAlphaBlendEnable( bool bEnable );
		bool		GetAlphaBlendEnable() const;

		void		SetAlphaSrcBlend( BlendMode eMode );
		BlendMode	GetAlphaSrcBlend() const;

		void		SetAlphaDstBlend( BlendMode eMode );
		BlendMode	GetAlphaDstBlend() const;

		void		SetTFactor( ulong ulFactor );
		ulong		GetTFactor() const;

		void		SetAlphaBlendOp( BlendOp eMode );
		BlendOp		GetAlphaBlendOp() const;

		// Alpha Test Enable
		void		SetAlphaTestEnable( bool bEnable );
		bool		GetAlphaTestEnable() const;
		
		void		SetAlphaFunc( ZFunc eFunc );
		ZFunc		GetAlphaFunc() const;

		void		SetAlphaRef( ulong ulRef );
		ulong		GetAlphaRef() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		// AlphaBlend
		bool		m_bAlphaBlendEnable;
		BlendMode	m_eAlphaSrcBlend;
		BlendMode	m_eAlphaDstBlend;
		ulong		m_ulTFactor;

		BlendOp		m_eAlphaBlendOp;

		// Alpha Test
		bool		m_bAlphaTestEnable;
		ZFunc		m_eAlphaFunc;
		ulong		m_ulAlphaRef;
	};

	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetAlphaBlendEnable( bool bEnable )
	{
		m_bAlphaBlendEnable = bEnable;
	}
	inline
	bool CnAlphaState::GetAlphaBlendEnable() const
	{
		return m_bAlphaBlendEnable;
	}

	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetAlphaSrcBlend( BlendMode eMode )
	{
		m_eAlphaSrcBlend = eMode;
	}
	inline
	CnAlphaState::BlendMode CnAlphaState::GetAlphaSrcBlend() const
	{
		return m_eAlphaSrcBlend;
	}

	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetAlphaDstBlend( BlendMode eMode )
	{
		m_eAlphaDstBlend = eMode;
	}
	inline
	CnAlphaState::BlendMode CnAlphaState::GetAlphaDstBlend() const
	{
		return m_eAlphaDstBlend;
	}

	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetTFactor( ulong ulFactor )
	{
		m_ulTFactor = ulFactor;
	}
	inline
	ulong CnAlphaState::GetTFactor() const
	{
		return m_ulTFactor;
	}

	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetAlphaBlendOp( CnAlphaState::BlendOp eMode )
	{
		m_eAlphaBlendOp = eMode;
	}
	inline
	CnAlphaState::BlendOp CnAlphaState::GetAlphaBlendOp() const
	{
		return m_eAlphaBlendOp;
	}

	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetAlphaTestEnable( bool bEnable )
	{
		m_bAlphaTestEnable = bEnable;
	}
	inline
	bool CnAlphaState::GetAlphaTestEnable() const
	{
		return m_bAlphaTestEnable;
	}

	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetAlphaFunc( CnAlphaState::ZFunc eFunc )
	{
		m_eAlphaFunc = eFunc;
	}
	inline
	CnAlphaState::ZFunc CnAlphaState::GetAlphaFunc() const
	{
		return m_eAlphaFunc;
	}

	//----------------------------------------------------------------
	/** Alpha Ref
		@remarks		알파 테스트를 사용할 경우, 빠질 알파 범위
						0x00000000 ~ 0x000000ff 까지 허용
	*/
	//----------------------------------------------------------------
	inline
	void CnAlphaState::SetAlphaRef( ulong ulRef )
	{
		m_ulAlphaRef = ulRef;
	}
	inline
	ulong CnAlphaState::GetAlphaRef() const
	{
		return m_ulAlphaRef;
	}
}

#endif	// __CN_ALPHA_STATE_H__