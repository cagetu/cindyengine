//================================================================
// File:           : CnMaterialDataManager.h
// Original Author : changhee
// Creation Date   : 2008. 1. 11
//================================================================
#pragma once

#include "Resource/CnResourceManager.h"
#include "Util/CnSingleton.h"

namespace Cindy
{
	//================================================================
	/** Material Manager
	    @author    changhee
		@since     2007. 5. 21
		@remarks   재질 Manager
	*/
	//================================================================
	class CN_DLL CnMaterialDataManager : public CnResourceManager
	{
		__DeclareClass(CnMaterialDataManager);
		__DeclareSingleton(CnMaterialDataManager);
	public:
		CnMaterialDataManager();
		virtual ~CnMaterialDataManager();

		// 리소스 읽기
		RscPtr	Load( const wchar* strFileName ) override;

		// 생성
		RscPtr	Create( const CnString& strFileName );
	};

//#include "CnMaterialDataManager.inl"
//Macro
#define MaterialDataMgr	CnMaterialDataManager::Instance()
}
