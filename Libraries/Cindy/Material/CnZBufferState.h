//================================================================
// File:           : CnZBufferState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_ZBUFFER_STATE_H__
#define __CN_ZBUFFER_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** ZBufferState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnZBufferState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		// ZBuffer
		enum ZBufferMode
		{
			ZB_DISABLE		= 0,
			ZB_USEZBUFFER	= 1,
			ZB_USEWBUFFER	= 2,
		};

		enum ZFunc
		{
			FUNC_NEVER                = 1,
			FUNC_LESS                 = 2,
			FUNC_EQUAL                = 3,
			FUNC_LESSEQUAL            = 4,
			FUNC_GREATER              = 5,
			FUNC_NOTEQUAL             = 6,
			FUNC_GREATEREQUAL         = 7,
			FUNC_ALWAYS               = 8,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnZBufferState();

		// Type
		TYPE		GetStateType() const	{	return ZBUFFER;		}

		// Z Buffer
		void		SetZBufferEnable( int nEnable );
		int			GetZBufferEnable() const;

		void		SetZBufferMode( ZBufferMode eMode );
		ZBufferMode	GetZBufferMode() const;

		void		SetZBufferFunc( ZFunc eFunc );
		ZFunc		GetZBufferFunc() const;
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		// Z-Buffer
		ZBufferMode	m_eZBuffer;
		ZFunc		m_eZFunc;
		int			m_nEnableZBuffer;
	};

	//----------------------------------------------------------------
	inline
	void CnZBufferState::SetZBufferEnable( int nEnable )
	{
		m_nEnableZBuffer = nEnable;
	}
	inline
	int CnZBufferState::GetZBufferEnable() const
	{
		return m_nEnableZBuffer;
	}
	//----------------------------------------------------------------
	inline
	void CnZBufferState::SetZBufferMode( CnZBufferState::ZBufferMode eMode )
	{
		m_eZBuffer = eMode;
	}
	inline
	CnZBufferState::ZBufferMode CnZBufferState::GetZBufferMode() const
	{
		return m_eZBuffer;
	}
	//----------------------------------------------------------------
	inline
	void CnZBufferState::SetZBufferFunc( CnZBufferState::ZFunc eFunc )
	{
		m_eZFunc = eFunc;
	}
	inline
	CnZBufferState::ZFunc CnZBufferState::GetZBufferFunc() const
	{
		return m_eZFunc;
	}

}

#endif	// __CN_ZBUFFER_STATE_H__