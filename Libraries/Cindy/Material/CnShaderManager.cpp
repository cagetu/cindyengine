//================================================================
// File:               : CnShaderManager.cpp
// Related Header File : CnShaderManager.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 30
//================================================================
#include "../Cindy.h"
#include "CnShaderManager.h"
#include "CnShader.h"
#include "CnUberShader.h"

#include "Graphics/CnRenderer.h"

namespace Cindy
{
	//==============================================================
	__ImplementClass(Cindy, CnShaderManager, CnResourceManager);
	__ImplementSingleton(CnShaderManager);
	///Const/Dest
	CnShaderManager::CnShaderManager()
	{
		__ConstructSingleton;
	}
	CnShaderManager::~CnShaderManager()
	{
		__DestructSingleton;
	}

	//--------------------------------------------------------------
	/** Load
		@brief	리소스를 읽는다.
	*/
	//--------------------------------------------------------------
	RscPtr CnShaderManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = Get( strFileName );
		if (!spRsc.IsNull())
			return spRsc.GetPtr();

		CnShader* rsc = CnNew CnShader( this );

		if (rsc->Load( strFileName ))
		{
			Add( rsc );
		}
		else
		{
			SAFEDEL( rsc );
		}

#ifdef _DEBUG
		//rsc->CreateInstance();
#endif
		return rsc;
	}
	//--------------------------------------------------------------
	RscPtr CnShaderManager::Load( const wchar* strFileName, int nType )
	{
		RscPtr spRsc = Get( strFileName );
		if (!spRsc.IsNull())
			return spRsc.GetPtr();

		CnShader* rsc = 0;
		switch (nType)
		{
		case 0:
			rsc = CnNew CnShader( this );
			break;
		case 1:
			rsc = CnNew CnUberShader( this );
			break;
		}

		if (rsc->Load( strFileName ))
		{
			Add( rsc );
		}
		else
		{
			SAFEDEL( rsc );
		}

		return rsc;
	}

	//--------------------------------------------------------------
	/** @brief	스트링을 Mask으로 변경 */
	//--------------------------------------------------------------
	CnShaderFeature::Mask
	CnShaderManager::FeatureStringToMask( const CnString& Features )
	{
		return m_ShaderFeature.StringToMask( Features );
	}

	//--------------------------------------------------------------
	/** @brief	Mask를 스트링으로 변경 */
	//--------------------------------------------------------------
	CnString
	CnShaderManager::FeatureMaskToString( CnShaderFeature::Mask Features )
	{
		return m_ShaderFeature.MaskToString( Features );
	}
}