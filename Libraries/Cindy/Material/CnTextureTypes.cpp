//================================================================
// File:               : CnTextureTypes.cpp
// Related Header File : CnTextureTypes.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 12
//================================================================
#include "../Cindy.h"
#include "CnTextureTypes.h"
#include "CnShaderDefines.h"

namespace Cindy
{
namespace MapType
{
	//----------------------------------------------------------------
	/**	gs_DefineTable
		@remarks		Texture의 타입들 명시!!
						계속 갱신해주어야 해욤..
	*/
	//----------------------------------------------------------------
	static struct
	{
		wchar* name;
		int shader;
	} gs_DefineTable[MapType::NumMapTypes] =
	{
		L"DiffuseMap0", ShaderState::DiffuseMap0,
		L"DiffuseMap1", ShaderState::DiffuseMap1,
		L"DiffuseMap2", ShaderState::DiffuseMap2,
		L"DiffuseMap3", ShaderState::DiffuseMap3,
		L"DiffuseMap4", ShaderState::DiffuseMap4,
		L"DiffuseMap5", ShaderState::DiffuseMap5,
		L"DiffuseMap6", ShaderState::DiffuseMap6,
		L"SpecularMap", ShaderState::SpecularMap,
		L"SpecularExpMap", ShaderState::SpecularExpMap,
		L"NormalMap", ShaderState::NormalMap,
		L"EmissiveMap", ShaderState::EmissiveMap,
		L"GlowMap", ShaderState::GlowMap,
		L"ReflectMap", ShaderState::ReflectMap,
		L"CelShadeMap", ShaderState::CelShadeMap,
		L"OcclusionMap", ShaderState::OcclusionMap,
		L"CubeMap0", ShaderState::CubeMap0,
		L"AlphaMap", ShaderState::AlphaMap,
		L"RandomMap", ShaderState::RandomMap,
		L"HairShiftMap", ShaderState::HairShiftMap,
		L"HairSpecMaskMap", ShaderState::HairSpecMaskMap,
		L"VextexTexture0", ShaderState::VextexTexture0,
		L"VextexTexture1", ShaderState::VextexTexture1,
		L"VextexTexture2", ShaderState::VextexTexture2,
		L"VextexTexture3", ShaderState::VextexTexture3,
		L"GBuffer", ShaderState::GBuffer,
		L"HistoryAOBuffer", ShaderState::HistoryAOBuffer,
		L"BloomBuffer", ShaderState::BloomBuffer,
		L"SourceBuffer", ShaderState::SourceBuffer,
		L"LuminanceBuffer", ShaderState::LuminanceBuffer,
	};

	//----------------------------------------------------------------
	/** StringToParam
		@remarks		미리 정해놓은 파라미터 이름을 인덱스로 변환해준다.
	*/
	//----------------------------------------------------------------
	MapType::Define MapType::StringToDefine( const wchar* name )
	{
		_ASSERT(_countof(gs_DefineTable) == MapType::NumMapTypes);

		for( uint i = 0; i < MapType::NumMapTypes; ++i )
		{
			if( 0 == wcscmp( gs_DefineTable[i].name, name ) )
				return (MapType::Define)i;
		}
		return MapType::InvalidType;
	}

	//----------------------------------------------------------------
	/** ParamToString
		@remarks		인덱스를 파라미터 이름으로 변경한다.
	*/
	//----------------------------------------------------------------
	const wchar* MapType::DefineToString( MapType::Define define )
	{
		return gs_DefineTable[define].name;
	}

	int MapType::ToShaderState(MapType::Define def)
	{
		return gs_DefineTable[def].shader;
	}
}
}