//================================================================
// File:           : CnScissorState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_SCISSOR_STATE_H__
#define __CN_SCISSOR_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** ScissorState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXScissorState 에 해당하는 녀석들)
	*/
	//================================================================
	class CnScissorState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnScissorState();

		// Type
		TYPE		GetStateType() const	{	return SCISSOR;		}

		// ScissorTest Enable
		void		SetScissorTestEnable( bool bEnable );
		bool		GetScissorTestEnable() const;

		void		SetScissorTestRect( const RECT& rect );
		const RECT&	GetScissorTestRect() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		bool		m_bScissorTestEnable;
		RECT		m_ScissorRect;
	};

	//----------------------------------------------------------------
	inline
	void CnScissorState::SetScissorTestEnable( bool bEnable )
	{
		m_bScissorTestEnable = bEnable;
	}
	inline
	bool CnScissorState::GetScissorTestEnable() const
	{
		return m_bScissorTestEnable;
	}

	//----------------------------------------------------------------
	inline
	void CnScissorState::SetScissorTestRect( const RECT& rect )
	{
		m_ScissorRect = rect;
	}
	inline
	const RECT& CnScissorState::GetScissorTestRect() const
	{
		return m_ScissorRect;
	}
}

#endif	// __CN_SCISSOR_STATE_H__