//================================================================
// File:           : CnShaderBase.h
// Original Author : changhee
// Creation Date   : 2009. 5. 19
//================================================================
#ifndef __CN_SHADERBASE_H__
#define __CN_SHADERBASE_H__

#include "CnShaderDefines.h"
#include "CnTechnique.h"
#include "../Math/CnMatrix44.h"

namespace Cindy
{
	class CnTexture;

	//================================================================
	/** Shader 
	    @author    changhee
		@since     2009. 5. 19
		@remarks   Base 쉐이더 클래스
	*/
	//================================================================
	class CN_DLL CnShaderBase
	{
	public:
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnShaderBase();
		virtual ~CnShaderBase();

		// Technique
		ushort			GetNumTechniques() const;
		CnTechnique*	GetTechnique( const CnString& strName ) const;
		CnTechnique*	GetTechnique( CnShaderFeature::Mask Mask ) const;

		// Parameters
		void			SetParemter( ShaderState::Param eParam, ShaderHandle hHandle );
		void			SetParameters( void* pParameters );
		ShaderHandle	GetParameter( ShaderState::Param eParam ) const;
		bool			HasParameter( ShaderState::Param eParam ) const;
		void			ClearParemters();

		// Shader Begin/End
		virtual int		Begin( bool bSaveState = true ) abstract;
		virtual void	End() abstract;

		// Device Lost/Reset
		virtual void	Clear() abstract;

		//
		virtual void	SetTechnique( ShaderHandle hTechnique ) abstract;

		// Pass
		virtual void	BeginPass( uint nPass ) abstract;
		virtual void	EndPass() abstract;
		virtual void	CommitChanges() abstract;

		// Set 'Boolean'
		virtual void	SetBool( ShaderHandle hHandle, bool val ) abstract;
		// Set 'Int'
		virtual void	SetInt( ShaderHandle hHandle, int val ) abstract;
		// Set 'Float'
		virtual void	SetFloat( ShaderHandle hHandle, float val ) abstract;
		//virtual void	SetFloat4( ShaderHandle hHandle, float* val ) abstract;
		// Set 'Vector'
		virtual void	SetVector3( ShaderHandle hHandle, const Math::Vector3& val ) abstract;
		virtual void	SetVector4( ShaderHandle hHandle, const Math::Vector4& val ) abstract;
		virtual void	SetVectorArray( ShaderHandle hHandle, Math::Vector4* aVectors, int nSize ) abstract;
		// Set 'Matrix'
		virtual void	SetMatrix( ShaderHandle hHandle, const Math::Matrix44& val ) abstract;
		virtual void	SetMatrixArray( ShaderHandle hHandle, Math::Matrix44* aMatrices, int nSize ) abstract;
		// Set 'Texture'
		virtual void	SetTexture( ShaderHandle hHandle, CnTexture* texture ) abstract;
		// Set 'Value'
		virtual void	SetValue( ShaderHandle hHandle, void* pData, unsigned int nBytes ) abstract;
		// Set 'Color'
		virtual void	SetColor( ShaderHandle hHandle, const CnColor& color ) abstract;

	protected:
		typedef HashMap<CnString, Pointer<CnTechnique> >		TechniqueList;
		typedef TechniqueList::iterator			TechniqueIter;
		typedef TechniqueList::const_iterator	TechniqueConstIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		TechniqueList	m_Techniques;					//!< Technique 리스트
		ushort			m_nNumTechniques;				//!< Technique 개수

		ShaderHandle	m_hParameters[ShaderState::NumParameters];			//!< 이펙트 파일 내에 파라미터 목록

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnTechnique*	_AddTechnique( const CnString& strName );
		void			_ClearTechniques();

		virtual void	_UpdateTechniques() abstract;
	};

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	ushort CnShaderBase::GetNumTechniques() const
	{
		return m_nNumTechniques;
	}

	//------------------------------------------------------------------------------
	/** Shader Parameter 설정
	*/
	inline
	void CnShaderBase::SetParemter( ShaderState::Param eParam, ShaderHandle hHandle )
	{
		m_hParameters[eParam] = hHandle;
	}
	//------------------------------------------------------------------------------
	/**
	*/
	inline
	void CnShaderBase::SetParameters( void* pParameters )
	{
		memcpy( m_hParameters, pParameters, sizeof(m_hParameters) );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	ShaderHandle CnShaderBase::GetParameter( ShaderState::Param eParam ) const
	{
		return m_hParameters[eParam];
	}
	//------------------------------------------------------------------------------
	/**
	*/
	inline
	bool CnShaderBase::HasParameter( ShaderState::Param eParam ) const
	{
		return m_hParameters[eParam] != NULL;
	}
	//------------------------------------------------------------------------------
	/**
	*/
	inline
	void CnShaderBase::ClearParemters()
	{
		memset( m_hParameters, 0, sizeof(m_hParameters) );
	}
}

#endif	// __CN_SHADERBASE_H__