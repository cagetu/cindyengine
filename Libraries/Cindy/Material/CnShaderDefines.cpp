//================================================================
// File:               : CnShaderDefines.cpp
// Related Header File : CnShaderDefines.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 12
//================================================================
#include "../Cindy.h"
#include "CnShaderDefines.h"

namespace Cindy
{
namespace ShaderState
{
	//----------------------------------------------------------------
	/**	gs_ParamTable
		@remarks		Shader 파일 내부에 사용할 파라미터 이름을 정의.
						계속 갱신해주어야 해욤..
	*/
	//----------------------------------------------------------------
	static const char* gs_ParamTable[ShaderState::NumParameters] =
	{
		// Transform
		"ModelViewProjection",
		"ModelView",
		"Model",
		"View",
		"Projection",
		"ViewProjection",
		"InvModelViewProjection",
		"InvView",
		"InvTransposedView",
		"ModelLightProjTexture",
		"LightProjTexture",
		// View
		"EyePosition",
		"ViewDirection",
		"WorldEyePosition",
		// GlobalAmbient
		"GlobalAmbient",
		// Mat
		"MatAmbient",
		"MatDiffuse",
		"MatSpecular",
		"MatEmissive",
		"MatSpecularIntensity",
		"MatEmissiveIntensity",
		"MatGlowIntensity",
		// GlobalLight
		"LightDirection",
		// LocalLight
		"LightCount",
		"LightPosition",
		"LightDiffuse",
		"LightSpecular",
		"LightAttenuation",
		"LightRange",
		"LightPosArray",
		"LightColorArray",
		"LightRangeArray",
		// Alpha
		"AlphaSrcBlend",
		"AlphaDstBlend",
		// CullMode
		"CullMode",
		// Skin
		"JointPalette",
		"SkinBlend",
		// Map
		"DiffuseMap0",
		"DiffuseMap1",
		"DiffuseMap2",
		"DiffuseMap3",
		"DiffuseMap4",
		"DiffuseMap5",
		"DiffuseMap6",
		"DiffuseMap7",
		"SpecularMap",
		"SpecularExpMap",
		"EmissiveMap",
		"GlowMap",
		"NormalMap",
		"ShadowMap",
		"ToonMap",
		"OcclusionMap",
		"CubeMap0",
		"ReflectMap",
		"DepthBuffer",
		"AlphaMap",
		"RandomMap",
		"NoiseMap",
		"HairShiftMap",
		"HairSpecMaskMap",
		"HistoryAOBuffer",
		"VextexTexture0",
		"VextexTexture1",
		"VextexTexture2",
		"VextexTexture3",
		///
		"LightBuffer",
		"NormalBuffer",
		"GBuffer",
		// PostEffect
		"SourceBuffer",
		"BloomBuffer",
		"ColorBuffer",
		"LuminanceBuffer",
		"RenderTargetSize",
		"SourceSize",
		// Filter
		"FilterSampleOffsets",
		"FilterSampleWeights",
		// Display
		"DisplayResolution",
		// DepthOfField
		"DofParameter",
		// Deferred
		"ViewInfo",
		"ScreenUVAdjust",
		"ScreenProj",
		"FrustumCorners",
		// MotionBlur
		"ViewProjInv",
		"PrevViewProj",
		// Filter
		"TimeValue",
		"ElapsedTime",
		// LightRays
		"SunLightPosition",
		// SSAO
		"SSAOBlurOffsets",			//
		"SSAOBlurWeights",			//
		// PSSM
		"PSSMNumSplits",
		"GlobalPSSMDistances",
		"GlobalPSSMTransforms",
		"GlobalPSSMBuffer0",
		"GlobalPSSMBuffer1",
		"GlobalPSSMBuffer2",
		"GlobalPSSMBuffer3",
	};

	//----------------------------------------------------------------
	/** StringToParam
		@remarks		미리 정해놓은 파라미터 이름을 인덱스로 변환해준다.
	*/
	//----------------------------------------------------------------
	ShaderState::Param ShaderState::StringToParam( const char* name )
	{
		for( uint i = 0; i < ShaderState::NumParameters; ++i )
		{
			if( 0 == strcmp( gs_ParamTable[i], name ) )
				return (ShaderState::Param)i;
		}
		return ShaderState::InvalidParameter;
	}

	//----------------------------------------------------------------
	/** ParamToString
		@remarks		인덱스를 파라미터 이름으로 변경한다.
	*/
	//----------------------------------------------------------------
	const char* ShaderState::ParamToString( ShaderState::Param param )
	{
		return gs_ParamTable[param];
	}
} // end of ShaderState
} // end of Cindy