//================================================================
// File:               : CnTextureManager.cpp
// Related Header File : CnTextureManager.h
// Original Author     : changhee
// Creation Date       : 2007. 5. 21
//================================================================
#include "../Cindy.h"
#include "CnTextureManager.h"

#include "Graphics/Dx9/CnDx9Texture.h"
#include "Graphics/CnRenderer.h"

namespace Cindy
{
	//================================================================
	__ImplementClass(Cindy, CnTextureManager, CnResourceManager);
	__ImplementSingleton(CnTextureManager);
	// Constructor
	CnTextureManager::CnTextureManager()
	{
		__ConstructSingleton;
	}
	// Destructor
	CnTextureManager::~CnTextureManager()
	{
		__DestructSingleton;
	}

	//================================================================
	/** Load
		@brief	텍스쳐 리소스를 읽는다.
	*/
	//================================================================
	RscPtr CnTextureManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = Get( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnTexture* pTexture = CreateTexture();
		if (NULL == pTexture)
			return NULL;

		if( pTexture->Load( strFileName ) )
		{
			Add( pTexture );
		}
		else
		{
			SAFEDEL( pTexture );
		}

		return pTexture;
	}

	//================================================================
	/**	Load
		@brief	텍스쳐를 읽는다.
	*/
	//================================================================
	RscPtr CnTextureManager::Load( const wchar* strFileName,
								   ushort usWidth,
								   ushort usHeight,
								   ushort usDepth,
								   PixelFormat::Code eFormat,
								   ushort usUsage )
	{
		RscPtr spRsc = Get( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnTexture* pTexture = CreateTexture();
		if (NULL == pTexture)
			return NULL;

		pTexture->SetWidth( usWidth );
		pTexture->SetHeight( usHeight );
		pTexture->SetDepth( usDepth );
		pTexture->SetFormat( eFormat );
		pTexture->SetUsage( usUsage );

		if (pTexture->Load( strFileName ))
		{
			Add( pTexture );
		}
		else
		{
			SAFEDEL( pTexture );
		}
		return pTexture;
	}

	//============================================================================
	/** CreateTexture
		@brief		텍스쳐 생성
	*/
	//============================================================================
	CnTexture* CnTextureManager::CreateTexture()
	{
		CnTexture* pTexture = NULL;
		switch ( RenderDevice.GetType() )
		{
		case CnRenderer::D3D9:
			pTexture = CnNew CnDx9Texture( this );
			break;

		default:

			//			pTexture = new CnTexture( this );
//			break;
			return NULL;
		}

		return pTexture;
	}
}