//================================================================
// File:           : CnMaterial.h
// Original Author : changhee
// Creation Date   : 2009. 1. 14
//================================================================
#pragma once

#include "../CnShader.h"

namespace Cindy
{
	namespace MaterialSystem
	{
		//================================================================
		/** Material
			@author    changhee
			@remarks   재질 - SubMesh와 영향을 준다.
					   Shader에 설정할 파라미터들을 세팅한다.
		*/
		//================================================================
		class Material : public CnObject
		{
			CN_DECLARE_RTTI;
		public:
			enum MapType
			{
				DiffuseMap = 0,
				DiffuseMap1,
				DiffuseMap2,
				DiffuseMap3,
				DiffuseMap4,
				DiffuseMap5,
				DiffuseMap6,
				SpecularMap,
				NormalMap,
				CelShadeMap,
				AmbientOcclusionMap,
				CubeMap0,
			};

			//------------------------------------------------------
			// Methods
			//------------------------------------------------------
			Material();
			virtual ~Material();

			// Name
			const CnString&	GetName() const;

			// Shader
			void	SetShader( const CnString& FileName );
			void	SelectTechnique( const CnString& Name );

			// Color
			void	SetColorState( const CnMaterialState& ColorState );
			CnMaterialState*	GetColorState();

			// RenderState
			void	SetRenderState( const CnRenderState& RenderState );
			CnRenderState*	GetRenderState();

			// Texture
			void	SetTexture( MapType Type, const CnString& FileName );
			const CnString&	GetTexture( MapType Type );

			void	SetTextureState( const CnTextureState& TextureState );
			CnTextureState*	GetTextureState();

		private:
			typedef std::map<MapType, CnString>	MapList;
			typedef MapList::iterator			MapIter;

			//------------------------------------------------------
			// Variables
			//------------------------------------------------------
			CnString	m_Name;		//!< 매터리얼 이름 (어떻게 할까?)
			ushort		m_ID;		//!< 매터리얼 ID (어떻게 할까? shaderID + techniqueID)

			// shader
			CnString	m_Shader;	//!< shader pointer
			CnString	m_SelectedTechnique;

			// textures
			MapList		m_Maps;

			CnMaterialState	m_ColorState;		//!< ColorState
			CnRenderState	m_RenderState;		//!< RenderState
			CnTextureState	m_TextureState;		//!< TextureState
		};

		//----------------------------------------------------------------
		inline
		void Material::SetColorState( const CnMaterialState& ColorState )
		{
			m_ColorState = ColorState;
		}
		inline
		CnMaterialState* Material::GetColorState()
		{
			return &m_ColorState;
		}

		//----------------------------------------------------------------
		inline
		void Material::SetRenderState( const CnRenderState& RenderState )
		{
			m_RenderState = RenderState;
		}
		inline
		CnRenderState* Material::GetRenderState()
		{
			return &m_RenderState;
		}

		//----------------------------------------------------------------
		inline
		void Material::SetTextureState( const CnTextureState& TextureState )
		{
			m_TextureState = TextureState;
		}
		inline
		CnTextureState* Material::GetTextureState()
		{
			return &m_TextureState;
		}
	}
}


