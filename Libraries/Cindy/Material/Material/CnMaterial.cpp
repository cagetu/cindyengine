//================================================================
// File:               : CnMaterial.cpp
// Related Header File : CnMaterial.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 30
//================================================================
#include "../../Cindy.h"
#include "CnMaterial.h"
#include "CnShaderManager.h"

#include "../../Util/CnLog.h"

namespace Cindy
{
	namespace MaterialSystem
	{
		//================================================================
		CN_IMPLEMENT_RTTI(Cindy, Material, CnObject);
		//----------------------------------------------------------------
		// Const
		Material::Material()
			: m_ShaderID(0xffff)
		{
		}
		// Dest
		Material::~Material()
		{
		}

		//----------------------------------------------------------------
		/** @brief	텍스쳐 정보 추가
		*/
		//----------------------------------------------------------------
		void Material::SetTexture( Material::MapType Type, const CnString& FileName )
		{
			m_Maps[Type] = FileName;
		}

		//----------------------------------------------------------------
		/** @brief	텍스쳐 정보 얻기
		*/
		//----------------------------------------------------------------
		const CnString& Material::GetTexture( Material::MapType Type )
		{
			MapIter iter = m_MapIDs.find(Type);
			return iter->second;
		}

		//----------------------------------------------------------------
		/** @brief	쉐이더 정보 설정 */
		//----------------------------------------------------------------
		void Material::SetShader( const CnString& FileName )
		{
			m_Shader = CnShaderManager::Instance()->Load( FileName.c_str() );
			m_ShaderID = m_Shader->GetRscID();
		}

		//----------------------------------------------------------------
		/** @brief	쉐이더 ID */
		//----------------------------------------------------------------
		ushort Material::GetShaderID() const
		{
			return m_ShaderID;
		}

		//----------------------------------------------------------------
		/** @brief	선택된 Technique */
		//----------------------------------------------------------------
		void Material::SelectTechnique( const CnString& Name )
		{
			m_SelectedTechnique = Name;
		}
	}
} // end of Cindy