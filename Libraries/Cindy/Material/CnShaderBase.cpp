//================================================================
// File:               : CnShaderBase.cpp
// Related Header File : CnShaderBase.h
// Original Author     : changhee
// Creation Date       : 2009. 5. 19
//================================================================
#include "../Cindy.h"
#include "CnShaderBase.h"

namespace Cindy
{
	// Constructor
	CnShaderBase::CnShaderBase()
	{
	}
	// Distructor
	CnShaderBase::~CnShaderBase()
	{
		_ClearTechniques();
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnTechnique* CnShaderBase::_AddTechnique( const CnString& strName )
	{
		TechniqueList::iterator iter = m_Techniques.find( strName );
		if (iter == m_Techniques.end())
		{
			CnTechnique* technique = CnNew CnTechnique( strName );
			technique->SetParent( this );

			m_Techniques.insert( TechniqueList::value_type(strName, technique) );
			++m_nNumTechniques;

			return technique;
		}
		return iter->second;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShaderBase::_ClearTechniques()
	{
		m_Techniques.clear();
		m_nNumTechniques = 0;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnTechnique* CnShaderBase::GetTechnique( const CnString& strName ) const
	{
		TechniqueList::const_iterator iter = m_Techniques.find( strName );
		if (iter == m_Techniques.end())
			return NULL;

		return iter->second;
	}

	//------------------------------------------------------------------------------
	/** Get Technique
		@param		CnShaderFeature::Mask	: ShaderFeature
		@remarks	테크닉을 찾는다.
	*/
	CnTechnique* CnShaderBase::GetTechnique( CnShaderFeature::Mask Mask ) const
	{
		TechniqueConstIter iend = m_Techniques.end();
		for (TechniqueConstIter i = m_Techniques.begin(); i != iend; ++i)
		{
			if ((i->second)->GetFeatureMask() == Mask)
				return (i->second);
		}
		return NULL;
	}
} // end of namespace