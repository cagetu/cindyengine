//================================================================
// File:               : CnShader.cpp
// Related Header File : CnShader.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "../Util/CnLog.h"
#include "../Graphics/CnRenderer.h"

#include "CnShader.h"
#include "CnShaderManager.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnShader, CnResource );
	/// Const/Dest
	CnShader::CnShader( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_pShaderEffect(0)
	{
	}
	CnShader::~CnShader()
	{
		Clear();
		//SAFEDEL( m_pShaderEffect );
	}

	//------------------------------------------------------------------------------
	/** Load
	    @remarks      로딩
		@param        strFileName : 파일 이름
		@return       true/false : 성공 여부
	*/
	bool CnShader::Load( const wchar* strFileName )
	{
		m_strName = strFileName;

		MoCommon::MoMemStream memStream;
		if (!m_pParent->FindData( strFileName, memStream ))
		{
			CnError( ToStr(L"file not found: %s", strFileName) );
			return false;
		}

		unsigned char* buffer = memStream.GetBuffer();
		ulong fileSize = memStream.GetSize();

		m_pShaderEffect = RenderDevice->CreateEffect();
		if (!m_pShaderEffect->Create( buffer, fileSize ))
		{
			SAFEDEL( m_pShaderEffect );
			return false;
		}
		return true;
	}

	//------------------------------------------------------------------------------
	/** Unload
	    @remarks      Shader 해제
		@param        none
		@return       none
	*/
	bool CnShader::Unload()
	{
		m_strName.clear();
		Clear();
		//SAFEDEL( m_pShaderEffect );
		return true;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::OnLost()
	{
		if (m_pShaderEffect)
		{
			m_pShaderEffect->OnLost();
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::OnRestore()
	{
		if (m_pShaderEffect)
		{
			m_pShaderEffect->OnRestore();
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::Apply( ShaderFeature::Bits Mask )
	{ // Blank... Blank... Blank...
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnTechnique* CnShader::GetTechnique( const CnString& strName ) const
	{
		if (!m_pShaderEffect)
			return NULL;

		return m_pShaderEffect->GetTechnique( strName );
		//TechniqueList::const_iterator iter = m_Techniques.find( strName );
		//if (iter == m_Techniques.end())
		//	return NULL;
		//return iter->second;
	}

	//------------------------------------------------------------------------------
	/** Get Technique
		@param		CnShaderFeature::Mask	: ShaderFeature
		@remarks	테크닉을 찾는다.
	*/
	CnTechnique* CnShader::GetTechnique( CnShaderFeature::Mask Mask ) const
	{
		return m_pShaderEffect->GetTechnique( Mask );
		//TechniqueConstIter iend = m_Techniques.end();
		//for (TechniqueConstIter i = m_Techniques.begin(); i != iend; ++i)
		//{
		//	if ((i->second)->GetFeatureMask() == Mask)
		//		return (i->second);
		//}
		//return NULL;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnTechnique* CnShader::GetDefaultTechnique() const
	{
		if (!m_pShaderEffect || m_pShaderEffect->m_Techniques.empty())
			return NULL;

		return m_pShaderEffect->m_Techniques.begin()->second;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	int CnShader::Begin( bool bSaveState )
	{
		return m_pShaderEffect->Begin( bSaveState );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::End()
	{
		m_pShaderEffect->End();
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::Clear()
	{
		//GetEffect()->Clear();
		SAFEDEL( m_pShaderEffect );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetTechnique( ShaderHandle hTechnique )
	{
		m_pShaderEffect->SetTechnique( hTechnique );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::BeginPass( uint nPass )
	{
		m_pShaderEffect->BeginPass( nPass );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::EndPass()
	{
		m_pShaderEffect->EndPass();
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::CommitChanges()
	{
		m_pShaderEffect->CommitChanges();
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetBool( ShaderHandle hHandle, bool val )
	{
		m_pShaderEffect->SetBool( hHandle, val );
	}


	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetInt( ShaderHandle hHandle, int val )
	{
		m_pShaderEffect->SetInt( hHandle, val );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetFloat( ShaderHandle hHandle, float val )
	{
		m_pShaderEffect->SetFloat( hHandle, val );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetFloatArray( ShaderHandle hHandle, float* aFloats, int nSize )
	{
		m_pShaderEffect->SetFloatArray( hHandle, aFloats, nSize );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetVector3( ShaderHandle hHandle, const Math::Vector3& val )
	{
		m_pShaderEffect->SetVector3( hHandle, val );
	}
	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetVector4( ShaderHandle hHandle, const Math::Vector4& val )
	{
		m_pShaderEffect->SetVector4( hHandle, val );
	}
	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetVector3Array( ShaderHandle hHandle, const Math::Vector3* aVectors, int nSize )
	{
		m_pShaderEffect->SetVector3Array( hHandle, aVectors, nSize );
	}
	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetVector4Array( ShaderHandle hHandle, const Math::Vector4* aVectors, int nSize )
	{
		m_pShaderEffect->SetVector4Array( hHandle, aVectors, nSize );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetMatrix( ShaderHandle hHandle, const Math::Matrix44& val )
	{
		m_pShaderEffect->SetMatrix( hHandle, val );
	}
	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetMatrixArray( ShaderHandle hHandle, Math::Matrix44* aMatrices, int nSize )
	{
		m_pShaderEffect->SetMatrixArray( hHandle, aMatrices, nSize );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetTexture( ShaderHandle hHandle, CnTexture* texture )
	{
		m_pShaderEffect->SetTexture( hHandle, texture );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetValue( ShaderHandle hHandle, void* pData, unsigned int nBytes )
	{
		m_pShaderEffect->SetValue( hHandle, pData, nBytes );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShader::SetColor( ShaderHandle hHandle, const CnColor& color )
	{
		m_pShaderEffect->SetColor( hHandle, color );
	}
}