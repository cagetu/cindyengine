//================================================================
// File:               : CnShaderManager.inl
// Related Header File : CnShaderManager.h
// Original Author     : changhee
// Creation Date       : 2007. 7. 18
//================================================================

//--------------------------------------------------------------
/** @brief	Shader Include Path */
//--------------------------------------------------------------
inline
void CnShaderManager::SetIncludePath( const wchar* Path )
{
	m_IncludePath = Path;
}
inline
const CnString& CnShaderManager::GetIncludePath() const
{
	return m_IncludePath;
}