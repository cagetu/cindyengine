//================================================================
// File:               : CnTexture.inl
// Related Header File : CnTexture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 9
//================================================================
//--------------------------------------------------------------
/** Style
	@brief		텍스쳐 타입
*/
//--------------------------------------------------------------
inline
void CnTexture::SetStyle( CnTexture::Style eStyle )
{
	m_eStyle = eStyle;
}
inline 
CnTexture::Style CnTexture::GetStyle() const
{
	return m_eStyle;
}

//--------------------------------------------------------------
/** Format
	@brief		텍스쳐 포맷 
*/
//--------------------------------------------------------------
inline 
void CnTexture::SetFormat( PixelFormat::Code eFormat )
{
	m_eFormat = eFormat;
}
inline
PixelFormat::Code CnTexture::GetFormat() const
{
	return m_eFormat;
}

//--------------------------------------------------------------
/** Width
	@brief	텍스쳐 가로 사이즈
*/
//--------------------------------------------------------------
inline 
void CnTexture::SetWidth( ushort usWidth )
{
	m_usWidth = usWidth;
}
inline 
ushort CnTexture::GetWidth() const
{
	return m_usWidth;
}

//--------------------------------------------------------------
/** Height
	@brief		텍스쳐 세로 사이즈
*/
//--------------------------------------------------------------
inline 
void CnTexture::SetHeight( ushort usHeight )
{
	m_usHeight = usHeight;
}
inline 
ushort CnTexture::GetHeight() const
{
	return m_usHeight;
}

//--------------------------------------------------------------
/** Depth
	@brief		텍스쳐 깊이
*/
//--------------------------------------------------------------
inline 
void CnTexture::SetDepth( ushort usDepth )
{
	m_usDepth = usDepth;
}
inline 
ushort CnTexture::GetDepth() const
{
	return m_usDepth;
}

//--------------------------------------------------------------
/** Usage
	@brief	텍스쳐 Usage
*/
//--------------------------------------------------------------
inline
void CnTexture::SetUsage( ushort usUsage )
{
	m_usUsage = usUsage;
}
inline 
ushort CnTexture::GetUsage() const
{
	return m_usUsage;
}

//--------------------------------------------------------------
/** Is RenderTarget ?!
	@brief		랜더 타겟 텍스쳐 인지..
*/
//--------------------------------------------------------------
inline
bool CnTexture::IsRenderTarget() const
{
	return (0 != (GetUsage() & (RenderTargetColor | RenderTargetDepthStencil)));
}

//--------------------------------------------------------------
/** Get Number Of Mip-map Levels
	@brief		텍스쳐 밉맵 레벨 수
*/
//--------------------------------------------------------------
inline 
ushort CnTexture::GetNumMipLevels() const
{
	return m_usNumMipMaps;
}

//----------------------------------------------------------------
//	TexPtr Class
//----------------------------------------------------------------
inline
TexPtr::TexPtr()
: Ptr< CnTexture >()
{
}

//----------------------------------------------------------------
inline
TexPtr::TexPtr( CnTexture* pObject )
: Ptr< CnTexture >( pObject )
{
}

//----------------------------------------------------------------
inline
TexPtr::TexPtr( const TexPtr& spObject )
: Ptr< CnTexture >( spObject )
{
}

//----------------------------------------------------------------
inline
TexPtr::TexPtr( const RscPtr& spRsc )
: Ptr< CnTexture >()
{
	m_pObject = (CnTexture*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->AddRef();
	}
}

//----------------------------------------------------------------
inline
TexPtr& TexPtr::operator= ( const RscPtr& spRsc )
{
	if( m_pObject != (CnTexture*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->Release();

		if( !spRsc.IsNull() )
			spRsc->AddRef();

		m_pObject = (CnTexture*)spRsc.GetPtr();
	}

	return *this;
}