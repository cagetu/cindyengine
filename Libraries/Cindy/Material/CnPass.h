//================================================================
// File:           : CnPass.h
// Original Author : changhee
// Creation Date   : 2007. 2. 5
//================================================================
#ifndef __CN_PASS_H__
#define __CN_PASS_H__

#include "CnShaderDefines.h"
#include "CnRenderState.h"
#include "CnMaterialState.h"
#include "CnTextureState.h"
#include "../Foundation/CnObject.h"

//#define _ENABLE_PASS_PARAM_

namespace Cindy
{
	//================================================================
	/** Pass
	    @author    changhee
		@since     2007. 2. 5
		@remarks   Shader를 사용할 경우, effect - pass에 해당
				   하나의 스타일은 하나의 랜더링 패스로 구현
	*/
	//================================================================
	class CnPass : public CnObject//CnMemAllocObject
	{
		__DeclareRtti;
	public:
		CnPass( const CnString& strName = L"" );
		virtual ~CnPass();

		void					SetName( const CnString& strName );
		const CnString&			GetName() const;

		void					SetHandle( ShaderHandle hHandle );
		ShaderHandle			GetHandle() const;

		// Index
		void					SetIndex( uint nIndex );
		uint					GetIndex() const;

#ifdef _ENABLE_PASS_PARAM_
		// Parameters
		void					SetParameters( void* pParameters );
		void					SetParemter( ShaderState::Param eParam, ShaderHandle hHandle );
		ShaderHandle			GetParameter( ShaderState::Param eParam ) const;
		bool					HasParameter( ShaderState::Param eParam ) const;
		void					ClearParemters();
#endif
		// RenderState
		//void					SetRenderState( const CnRenderState& rState );
		//const CnRenderState&	GetRenderState() const;

		//// MaterialState
		//void					SetMaterialState( const CnMaterialState& rState );
		//const CnMaterialState&	GetMaterialState() const;

		//// TextureState
		//void					SetTextureState( const CnTextureState& rState );
		//const CnTextureState&	GetTextureState() const;

		// Begin / End
		void					Begin();
		void					End();

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString			m_strName;
		ShaderHandle		m_hHandle;

		//CnRenderState		m_RenderState;
		//CnMaterialState		m_MaterialState;
		//CnTextureState		m_TextureState;

		uint				m_nIndex;

#ifdef _ENABLE_PASS_PARAM_
		ShaderHandle		m_hParameters[ShaderState::NumParameters];	//!< 이펙트 파일 내에 파라미터 목록
#endif
	};

#include "CnPass.inl"
}

#endif	// __CN_PASS_H__