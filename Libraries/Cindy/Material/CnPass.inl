//================================================================
// File:               : CnPass.inl
// Related Header File : CnPass.h
// Original Author     : changhee
// Creation Date       : 2007. 5. 3
//================================================================

//----------------------------------------------------------------
inline
void CnPass::SetName( const CnString& strName )
{
	m_strName = strName;
}
inline
const CnString& CnPass::GetName() const
{
	return m_strName;
}

//----------------------------------------------------------------
inline
void CnPass::SetHandle( ShaderHandle hHandle )
{
	m_hHandle = hHandle;
}
inline
ShaderHandle CnPass::GetHandle() const
{
	return m_hHandle;
}

//----------------------------------------------------------------
/** Index 설정
*/
//----------------------------------------------------------------
inline
void CnPass::SetIndex( uint nIndex )
{
	m_nIndex = nIndex;
}
inline
uint CnPass::GetIndex() const
{
	return m_nIndex;
}

#ifdef _ENABLE_PASS_PARAM_
//----------------------------------------------------------------
/** Shader Parameter 설정
*/
//----------------------------------------------------------------
inline
void CnPass::SetParameters( void* pParameters )
{
	memcpy( m_hParameters, pParameters, sizeof(m_hParameters) );
}
inline
void CnPass::SetParemter( ShaderState::Param eParam, ShaderHandle hHandle )
{
	m_hParameters[eParam] = hHandle;
}

inline
ShaderHandle CnPass::GetParameter( ShaderState::Param eParam ) const
{
	return m_hParameters[eParam];
}

inline
bool CnPass::HasParameter( ShaderState::Param eParam ) const
{
	return m_hParameters[eParam] != NULL;
}

inline
void CnPass::ClearParemters()
{
	memset( m_hParameters, 0, sizeof(m_hParameters) );
}
#endif // _ENABLE_PASS_PARAM_

//----------------------------------------------------------------
/** RenderState 설정
*/
//----------------------------------------------------------------
//inline
//void CnPass::SetRenderState( const CnRenderState& rState )
//{
//	m_RenderState = rState;
//}
//inline
//const CnRenderState& CnPass::GetRenderState() const
//{
//	return m_RenderState;
//}
//
////----------------------------------------------------------------
///** Material State
//	@remarks	재질값
//*/
////----------------------------------------------------------------
//inline
//void CnPass::SetMaterialState( const CnMaterialState& rState )
//{
//	m_MaterialState = rState;
//}
//inline
//const CnMaterialState& CnPass::GetMaterialState() const
//{
//	return m_MaterialState;
//}
//
////----------------------------------------------------------------
///** Texture State
//	@remarks	텍스쳐 설정값
//*/
////----------------------------------------------------------------
//inline
//void CnPass::SetTextureState( const CnTextureState& rState )
//{
//	m_TextureState = rState;
//}
//inline
//const CnTextureState& CnPass::GetTextureState() const
//{
//	return m_TextureState;
//}


