//================================================================
// File:           : CnTexture.h
// Original Author : changhee
// Creation Date   : 2007. 4. 9
//================================================================
#ifndef __CN_TEXTURE_TYPES_H__
#define __CN_TEXTURE_TYPES_H__

namespace Cindy
{
namespace MapType
{
	enum Define
	{
		DiffuseMap0 = 0,
		DiffuseMap1,
		DiffuseMap2,
		DiffuseMap3,
		DiffuseMap4,
		DiffuseMap5,
		DiffuseMap6,
		SpecularMap,
		SpecularExpMap,
		NormalMap,
		EmissiveMap,
		GlowMap,
		ReflectMap,
		CelShadeMap,
		OcclusionMap,
		CubeMap0,
		AlphaMap,
		RandomMap,
		HairShiftMap,
		HairSpecMaskMap,
		VextexTexture0,
		VextexTexture1,
		VextexTexture2,
		VextexTexture3,
		GBuffer,
		HistoryAOBuffer,
		BloomBuffer,
		SourceBuffer,
		LuminanceBuffer,

		NumMapTypes,
		InvalidType,
	};

	Define			StringToDefine( const wchar* name );
	const wchar*	DefineToString( Define define );

	int				ToShaderState(Define def);
}
}

#endif	// __CN_TEXTURE_TYPES_H__