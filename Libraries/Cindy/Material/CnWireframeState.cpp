//================================================================
// File:               : CnWireframeState.cpp
// Related Header File : CnWireframeState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnWireframeState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnWireframeState, CnGlobalState);
	// Const/Dest
	CnWireframeState::CnWireframeState()
		: m_eFillMode(FILL_SOLID)
	{
	}
}