//================================================================
// File:               : CnMaterial.cpp
// Related Header File : CnMaterial.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 30
//================================================================
#include "Cindy.h"
#include "CnMaterial.h"
#include "CnTechnique.h"
#include "CnPass.h"
#include "CnShaderManager.h"
#include "CnTextureManager.h"
#include "Util/CnLog.h"

#pragma TODO("Material LOD를 추가해야 한다")

namespace Cindy
{
	__ImplementRtti(Cindy, CnMaterial, CnObject);
	//----------------------------------------------------------------
	/**	Constructor
	*/
	CnMaterial::CnMaterial()
		: m_pCurrentTechnique(0)
		, m_bEnable(true)
		, m_bFeatureDirty(true)
		, m_nShaderType(0)
		, m_FeatureBits(0)
	{
	}
	//----------------------------------------------------------------
	/**	Destructor
	*/
	CnMaterial::~CnMaterial()
	{
	}

	//----------------------------------------------------------------
	/**	Apply 적용하기
	*/
	//----------------------------------------------------------------
	void CnMaterial::Apply()
	{
		if (IsEnable() == false)
			return;
		if (false == m_bFeatureDirty)
			return;

		if (HasTexture( MapType::DiffuseMap0 ))
		{
			this->AddFeature(ShaderFeature::DiffuseMap0);
		}
		if (HasTexture( MapType::NormalMap ))
		{
			this->AddFeature(ShaderFeature::NormalMap);
		}
		if (HasTexture( MapType::SpecularMap ))
		{
			this->AddFeature(ShaderFeature::SpecMap);
		}
		if (HasTexture( MapType::SpecularExpMap ))
		{
			this->AddFeature(ShaderFeature::SpecExpMap);
		}
		if (HasTexture( MapType::EmissiveMap ))
		{
			this->AddFeature(ShaderFeature::EmissiveMap);
		}
		if (HasTexture( MapType::GlowMap ))
		{
			this->AddFeature(ShaderFeature::GlowMap);
		}
		if (HasTexture( MapType::CelShadeMap ))
		{
			this->AddFeature(ShaderFeature::ToonMap);
		}
		if (HasTexture( MapType::ReflectMap ))
		{
			this->AddFeature(ShaderFeature::ReflectMap);
		}
		if (HasTexture( MapType::AlphaMap ))
		{
			this->AddFeature(ShaderFeature::AlphaMap);
		}

		if (IsTransparent())
		{
			this->AddFeature(ShaderFeature::BlendAlpha);
		}

		m_spShader->Apply( this->GetFeatureBits() );

		m_bFeatureDirty = false;
	}

	//----------------------------------------------------------------
	/** Set Current Technique
	    @brief		현재 사용될 technique를 설정한다.
		@desc		오직 하나의 technique만 실행시점에서 사용이 가능하다.
		@param		strName : technique 이름
	*/
	//----------------------------------------------------------------
	void CnMaterial::SetCurrentTechnique( const CnString& strName )
	{
		if (m_spShader.IsNull())
		{
			CnError( ToStr(L"%s technique is not found because shader is empty", strName.c_str()) );
			return;
		}

		m_pCurrentTechnique = m_spShader->GetTechnique( strName );

		// material 이름
		m_strName = m_spShader->GetName() + L"@" + strName;
	}

	//----------------------------------------------------------------
	/**	Dirty Technique
		@brief		현재 사용할 Technique이 비어있을 때, 그것을 설정한다.
	*/
	//----------------------------------------------------------------
	void CnMaterial::DirtyTechnique() const
	{
		if (!m_pCurrentTechnique)
		{
			m_pCurrentTechnique = m_spShader->GetDefaultTechnique();
		}
	}

	//----------------------------------------------------------------
	/** Set Shader
	    @brief		Shader 파일을 읽어온다.
		@param		pszFileName : 쉐이더 파일 이름
		@return		true/false
	*/
	//----------------------------------------------------------------
	void CnMaterial::SetShader( const wchar* pszFileName, int nType )
	{
		m_spShader = ShaderMgr->Load( pszFileName, nType );
	}

	//----------------------------------------------------------------
	/**	Transparency 여부 판정
	*/
	//----------------------------------------------------------------
	bool CnMaterial::IsTransparent()
	{
		return GetRenderState()->GetAlphaBlendEnable();
#ifdef _ENABLE_PASS_PARAM_
		//return GetRenderState()->GetAlphaBlendEnable();
		//if (NULL == m_pCurrentTechnique)
		//	return false;

		//int numPasses = m_pCurrentTechnique->GetNumPasses();
		//if (numPasses == 0)
		//	return false;

		//if (numPasses > 1)
		//{
		//	CnTechnique::PassList Passes = m_pCurrentTechnique->GetPasses();
		//	CnTechnique::PassList::const_iterator ip, ipend;
		//	ipend = Passes.end();
		//	for (ip = Passes.begin(); ip != ipend; ++ip)
		//	{
		//		if ((*ip)->GetRenderState().GetAlphaBlendEnable())
		//			return true;
		//	}
		//}
		//else
		//{
		//	return m_pCurrentTechnique->GetPass(0)->GetRenderState().GetAlphaBlendEnable();
		//}
		//return false;
#endif // _ENABLE_PASS_PARAM_
	}

	//----------------------------------------------------------------
	/** Set Texture
		@brief		텍스쳐 설정
	*/
	//----------------------------------------------------------------
	void CnMaterial::SetTexture( MapType::Define Type, const CnString& strFileName )
	{
		if (strFileName.empty())
			return;

		TexPtr texture = TextureMgr->Load( strFileName.c_str() );
		if (texture.IsNull())
		{
			CnError( ToStr(L"[CnMaterial::SetTexture] %s is invalid", strFileName.c_str()) );
			return;
		}

		std::pair<TextureMap::iterator, bool> result = m_Textures.insert( TextureMap::value_type( Type, texture ) );
		if (!result.second)
		{
			(*result.first).second = texture;
		}

		m_bFeatureDirty = true;
	}

	//----------------------------------------------------------------
	/** Get Texture
		@brief		텍스쳐 얻어오기
	*/
	//----------------------------------------------------------------
	TexPtr CnMaterial::GetTexture( MapType::Define Type )
	{
		TextureMap::iterator iter = m_Textures.find( Type );
		if (iter == m_Textures.end())
			return NULL;

		return iter->second;
	}

	//----------------------------------------------------------------
	/** Has Texture
		@brief	텍스쳐를 가지고 있는가?
	*/
	//----------------------------------------------------------------
	bool CnMaterial::HasTexture( MapType::Define Type ) const
	{
		TextureMap::const_iterator iter = m_Textures.find( Type );
		if (iter==m_Textures.end())
			return false;
		return true;
	}
} // end of Cindy