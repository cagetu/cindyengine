//================================================================
// File:               : CnScissorState.cpp
// Related Header File : CnScissorState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnScissorState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnScissorState, CnGlobalState);
	// Const/Dest
	CnScissorState::CnScissorState()
		: m_bScissorTestEnable(false)
	{
	}
}