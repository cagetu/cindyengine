//================================================================
// File:               : CnMaterialDataManager.cpp
// Related Header File : CnMaterialDataManager.h
// Original Author     : changhee
// Creation Date       : 2008. 1. 11
//================================================================
#include "../Cindy.h"
#include "CnMaterialDataManager.h"
#include "CnMaterialData.h"

namespace Cindy
{
	//================================================================
	__ImplementClass(Cindy, CnMaterialDataManager, CnResourceManager);
	__ImplementSingleton(CnMaterialDataManager);
	///Const/Dest
	CnMaterialDataManager::CnMaterialDataManager()
	{
		__ConstructSingleton;
	}
	CnMaterialDataManager::~CnMaterialDataManager()
	{
		__DestructSingleton;
	}

	//--------------------------------------------------------------
	/** Load
		@remarks		리소스를 읽는다.
	*/
	//--------------------------------------------------------------
	RscPtr CnMaterialDataManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = Get( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnMaterialData* rsc = CnNew CnMaterialData( this );
		if( rsc->Load( strFileName ) )
		{
			Add( rsc );
		}
		else
		{
			SAFEDEL( rsc );
		}

		return rsc;
	}

	//--------------------------------------------------------------
	/** Create
		@remarks		리소스를 생성
	*/
	//--------------------------------------------------------------
	RscPtr CnMaterialDataManager::Create( const CnString& strFileName )
	{
		RscPtr spRsc = Get( strFileName );
		if ( !spRsc.IsNull() )
			return RscPtr();

		CnMaterialData* rsc = CnNew CnMaterialData( this );
		Add( rsc );

		return rsc;
	}
}