//================================================================
// File:               : CnPass.cpp
// Related Header File : CnPass.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 5
//================================================================
#include "../Cindy.h"
#include "CnPass.h"
#include "CnShader.h"

#include "../Graphics/CnRenderer.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnPass, CnObject);
	/// Const/Dest
	CnPass::CnPass( const CnString& strName )
		: m_strName( strName )
		, m_hHandle( 0 )
		, m_nIndex( 0 )
	{
#ifdef _ENABLE_PASS_PARAM_
		memset( m_hParameters, 0, sizeof(m_hParameters) );
#endif // _ENABLE_PASS_PARAM_
	}
	CnPass::~CnPass()
	{
	}

	//================================================================
	/** Begin
		@remarks		Begin
	*/
	//================================================================
	void CnPass::Begin()
	{
		CnShaderEffect* pShaderEffect = RenderDevice->GetActiveShader();
		if (!pShaderEffect)
			return;

		pShaderEffect->BeginPass( GetIndex() );

		//RenderDevice->SetRenderState( GetRenderState() );
		//RenderDevice->SetTextureState( GetTextureState() );
		//RenderDevice->SetMaterialState( GetMaterialState() );
	}

	//================================================================
	/** End
		@remarks		End
	*/
	//================================================================
	void CnPass::End()
	{
		CnShaderEffect* pShaderEffect = RenderDevice->GetActiveShader();
		if (!pShaderEffect)
			return;

		pShaderEffect->EndPass();
	}
}