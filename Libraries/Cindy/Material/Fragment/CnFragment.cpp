//================================================================
// File:               : CnFragment.cpp
// Related Header File : CnFragment.h
// Original Author     : changhee
//================================================================
#include "Cindy.h"
#include "Util/CnLog.h"

#include "CnFragment.h"

namespace Cindy
{
namespace FragmentShader
{
	Fragment::Fragment()
		: m_End(false)
	{
		static ulong id = 0;
		m_UniqueID = id++;
	}
	Fragment::~Fragment()
	{
		RemoveChildren();
		m_Nodes.clear();
	}

	//================================================================
	/** Load
		@remarks		Load Fragment Node
	*/
	//================================================================
	bool Fragment::Load( const CnString& FileName )
	{
		CMiniXML doc;
		if (!doc.Open( FileName.c_str() ))
			return false;

		// Root
		CMiniXML::iterator xmlRoot = doc.GetRoot();
		if (xmlRoot == NULL)
			return false;

		m_Nodes.clear();
		ClearConditions();

		m_Name = xmlRoot.GetName();

		wchar buffer[256];
		_swprintf( buffer, L"%s%d", this->m_Name.c_str(), this->m_UniqueID );
		m_Code = buffer;

		const wchar* endPoint = xmlRoot.GetAttribute(_T("endpoint"));
		if (endPoint)
		{
			int end = _wtoi(endPoint);
			m_End = (1 == end) ? true : false;
		}

		ReadNode( &xmlRoot );
		return true;
	}

	//================================================================
	/** Generate Code
		@remarks		Code 생성
	*/
	//================================================================
	void Fragment::GenerateCode( CnString& Code )
	{
		// '$'가 되어 있는 녀석은 출력 데이터이므로,
		// 읽어들이면서, 변수를 만들어 놓는다.
		size_t start = 0;
		size_t cur = 0;

		do
		{
			cur = Code.find_first_of( _T("$") );
			if (cur == CnString::npos)
				break;

			CnString head = Code.substr( start, cur - start );
			CnString id = Code.substr( cur+1, 1 );
			CnString tail = Code.substr( cur + 2 );

			int index = _wtoi( id.c_str() );
			Node* node = this->Find( index );
			node->tempVar = m_Code + id;

			Code = head + m_Code + id + tail;
		}
		while (cur != CnString::npos);

		//CnPrint( Code.c_str() );
	}

	//================================================================
	/** SwapUniqueID
		@remarks		@를 교체한다.
	*/
	//================================================================
	void Fragment::SwapUniqueID( CnString& Code )
	{
		size_t start = 0;
		size_t cur = 0;

		do
		{
			cur = Code.find( _T("@") );
			if (cur == CnString::npos)
				break;

			CnString head = Code.substr( start, cur - start );
			CnString tail = Code.substr( cur + 1 );

			Code = head + m_Code + tail;
		}
		while (cur != CnString::npos);

		//CnPrint( Code.c_str() );
	}

	//================================================================
	/** Load Input Node
		@remarks		입력 노드 읽기
	*/
	//================================================================
	void Fragment::ReadNode( CMiniXML::iterator* Parent )
	{
		CMiniXML::iterator xmlNode = Parent->GetChild();
		for (; xmlNode != NULL; xmlNode=xmlNode.Next())
		{
			const wchar* name = xmlNode.GetName();
			if (0 == wcscmp(_T("node"), name))
			{
				Node node;
				node.parent = this;
				node.text = xmlNode.GetAttribute(_T("name"));
				node.nodeType = xmlNode.GetAttribute(_T("nodetype"));
				node.id = _wtoi( xmlNode.GetAttribute(_T("id")) );
				node.format = xmlNode.GetAttribute(_T("format"));
				node.shaderType = xmlNode.GetAttribute(_T("shadertype"));

				m_Nodes.push_back( node );
			}
			else if (0 == wcscmp(_T("shader"), name))
			{
				CMiniXML::iterator xmlShaderNode = xmlNode.GetChild();
				for (; xmlShaderNode != NULL;xmlShaderNode=xmlShaderNode.Next())
				{
					name = xmlShaderNode.GetName();
					if (0 == wcscmp(_T("header"), name))
					{
						this->m_Header = xmlShaderNode.GetValue();

						GenerateCode( m_Header );
						SwapUniqueID( m_Header );
					}
					else if (0 == wcscmp(_T("vertexinput"), name))
					{
						this->m_VertexInput = xmlShaderNode.GetValue();
					}
					else if (0 == wcscmp(_T("vertexcode"), name))
					{
						ReadCondition( &xmlShaderNode, m_VertexCodes );
					}
					else if (0 == wcscmp(_T("pixelinput"), name))
					{
						this->m_PixelInput = xmlShaderNode.GetValue();
					}
					else if (0 == wcscmp(_T("pixelcode"), name))
					{
						ReadCondition( &xmlShaderNode, m_PixelCodes );
					}
				} // for
			} // if
		} // for
	}
	
	//================================================================
	/** Read Condition Node
		@remarks		코드 노드상의 Condition 노드 읽기
	*/
	//================================================================
	void Fragment::ReadCondition( CMiniXML::iterator* Parent, Fragment::ConditionArray& Conditions)
	{
		CMiniXML::iterator xmlNode = Parent->GetChild();
		for (xmlNode=xmlNode.Find(_T("condition")); xmlNode != NULL; xmlNode=xmlNode.Find(_T("condition")))
		{
			Condition* condition = new Condition;
			const wchar* attr;
			attr = xmlNode.GetAttribute(_T("nulltest"));
			if (attr = xmlNode.GetAttribute(_T("nulltest")))
			{
				if (attr[0] == L'~')
				{
					condition->null = true;
					attr++;
				}
				else
				{
					condition->null = false;
				}

				condition->type = Condition::Type::NullCheck;
				condition->nodeid = _wtoi(attr);;
			}
			else if (attr = xmlNode.GetAttribute(_T("shadertype")))
			{
				condition->shaderType = (Condition::ShaderType::Enum)_wtoi(attr);
			}

			condition->code = xmlNode.GetValue();

			GenerateCode( condition->code );
			SwapUniqueID( condition->code );

			Conditions.push_back( condition );
		}
	}

	//================================================================
	/** Clear Conditions
		@remarks		코드 노드상의 Condition 노드 삭제
	*/
	//================================================================
	void Fragment::ClearConditions()
	{
		ConditionIter i, iend;
		iend = m_VertexCodes.end();
		for (i=m_VertexCodes.begin(); i!=iend; ++i)
			delete (*i);
		m_VertexCodes.clear();

		iend = m_PixelCodes.end();
		for (i=m_PixelCodes.begin(); i!=iend; ++i)
			delete (*i);
		m_PixelCodes.clear();
	}

	//================================================================
	/** Find
		@remarks		노드 찾기
	*/
	//================================================================
	Fragment::Node* Fragment::Find( int NodeID )
	{
		size_t size = m_Nodes.size();
		for (size_t i=0; i<size; ++i)
		{
			if (NodeID == m_Nodes[i].id)
				return &m_Nodes[i];
		}

		return NULL;
	}

	//================================================================
	/** Add Child
		@remarks	자식 추가
	*/
	//================================================================
	void Fragment::AddChild( Fragment* Child )
	{
		std::pair<ChildIter, bool> result = m_Children.insert( ChildMap::value_type( Child->m_Name, Child ) );
		CnError( ToStr(L"[Fragment::AddChild] %s is existed", Child->m_Name.c_str()) );
	}

	//================================================================
	/** Remove Children
		@remarks	자식들 제거
	*/
	//================================================================
	void Fragment::RemoveChildren()
	{
		m_Children.clear();
	}

	//================================================================
	/** Get Child
		@remarks	자식 얻어오기
	*/
	//================================================================
	Fragment* Fragment::GetChild( const CnString& Name )
	{
		ChildIter iter = m_Children.find( Name );
		if (m_Children.end() == iter)
			return NULL;

		return iter->second;
	}

	//================================================================
	/** Get Children
		@remarks	자식들 얻어오기
	*/
	//================================================================
	void Fragment::GetChildren( Fragment::ChildMap& Children )
	{
		Children = m_Children;
	}

	//================================================================
	/** Build List
		@remarks	모든 Compoent를 모은다.
	*/
	//================================================================
	void Fragment::BuildList( std::deque<Fragment*>& List )
	{
		Fragment* component = NULL;

		std::deque<Fragment*>::iterator iWhere;

		ChildIter i, iend;
		iend = m_Children.end();
		for (i=m_Children.begin(); i!=iend; ++i)
		{
			component = i->second;

			iWhere = std::find( List.begin(), List.end(), component );
			if (List.end() != iWhere)
			{
				List.erase( iWhere );
			}

			List.push_front( component );

			component->BuildList( List );
		}
	}
}
}
