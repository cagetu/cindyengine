#pragma once

#include "CnFragment.h"
#include "Foundation/CnObject.h"

namespace Cindy
{
namespace FragmentShader
{
	//================================================================
	/** Fragment Manager
		@author    changhee
		@remarks   Shader Fragment ������
	*/
	//================================================================
	class CN_DLL Manager : public CnObject
	{
		__DeclareClass(Manager);
	public:
		Manager( const CnString& DataPath = L"" );
		~Manager();

		void		SetDataPath( const CnString& DataPath );

		bool		Load( const CnString& FileName, bool bFullPath = false );
		Fragment*	Find( const CnString& Title );

		bool		Compile( const CnString& FileName );

	private:
		typedef HashMap<CnString, Fragment*>	FragmentMap;
		typedef FragmentMap::iterator			FragmentIter;
		
		typedef std::deque<Fragment*>	ComQueue;
		typedef ComQueue::iterator		ComIter;

		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		FragmentMap		m_Fragments;

		Fragment*		m_EndPoint;

		CnString		m_DataPath;

		ComQueue		m_Sorted;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		void	Clear();

		bool	MakeRelation( CMiniXML::iterator& Parent, Fragment* ParentComponent );
		void	MakeList();

		void	Parse();
		void	ParseCondition( CnString& Code, Fragment* Component, Fragment::ConditionArray& Condtions );
		void	ParseInputVar( CnString& Code, Fragment* Component );
	};
}
}
