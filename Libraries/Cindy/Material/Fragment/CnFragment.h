#pragma once

#include "../CnShaderDefines.h"
#include <MoCommon/External/minixml.h>

namespace Cindy
{
namespace FragmentShader
{
	//================================================================
	/** Fragment
		@author    changhee
		@remarks   Shader Fragment ���
	*/
	//================================================================
	class CN_DLL Fragment
	{
	public:
		//------------------------------------------------------
		//	Structures
		//------------------------------------------------------
		class Node
		{
		public:
			int			id;
			CnString	text;
			CnString	nodeType;
			CnString	shaderType;
			CnString	format;

			CnString	tempVar;

			Fragment*	parent;

			Node*		prev;
			Node*		next;

			Node() : parent(0), prev(0), next(0) {}

			bool	IsLinked() const	{	return (prev != NULL);	}
		};

		struct Condition
		{
			struct Type	{
				enum Enum
				{
					None = 0,
					NullCheck = 1,
					TypeCheck = 2,
				};
			};

			struct ShaderType {
				enum Enum	{ None, VS, PS };
			};

			Type::Enum			type;
			int					nodeid;
			bool				null;
			ShaderType::Enum	shaderType;

			CnString			code;

			Condition() : type(Type::None), null(true), shaderType(ShaderType::None) {}
		};

		//------------------------------------------------------
		//	Type
		//------------------------------------------------------
		typedef HashMap<CnString, Fragment*>	ChildMap;
		typedef ChildMap::iterator				ChildIter;

		typedef std::vector<Condition*>			ConditionArray;
		typedef ConditionArray::iterator		ConditionIter;

		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		CnString	m_Name;
		CnString	m_Code;
		ulong		m_UniqueID;

		bool		m_End;

		CnString		m_Header;
		CnString		m_VertexInput;
		CnString		m_PixelInput;

		ConditionArray	m_VertexCodes;
		ConditionArray	m_PixelCodes;
	private:
		typedef std::vector<Node>	NodeArray;
		typedef NodeArray::iterator	NodeIter;

		NodeArray	m_Nodes;
		ChildMap	m_Children;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		void	ReadNode( CMiniXML::iterator* Parent );
		void	ReadCondition( CMiniXML::iterator* Parent, ConditionArray& Conditions );

		void	GenerateCode( CnString& Code );
		void	SwapUniqueID( CnString& Code );

		void	ClearConditions();
	public:
		Fragment();
		~Fragment();

		bool		Load( const CnString& FileName );
		bool		IsEnd() const;

		// Node
		Node*		Find( int NodeID );

		// �ڽ� Fragment
		void		AddChild( Fragment* Child );
		Fragment*	GetChild( const CnString& Name );
		void		GetChildren( ChildMap& Children );
		void		RemoveChildren();
		
		void		BuildList( std::deque<Fragment*>& List );
	};
}

#include "CnFragment.inl"
}
