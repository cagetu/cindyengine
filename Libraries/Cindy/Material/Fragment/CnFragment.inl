namespace FragmentShader
{
	//================================================================
	/** Is End
		@remarks		마지막 Component인지를 체크 
	*/
	//================================================================
	inline
	bool Fragment::IsEnd() const
	{
		return m_End;
	}
}
