//================================================================
// File:               : CnFragmentManager.cpp
// Related Header File : CnFragmentManager.h
// Original Author     : changhee
//================================================================
#include "Cindy.h"
#include "Util/CnLog.h"

#include "CnFragmentManager.h"

namespace Cindy
{
namespace FragmentShader
{
	__ImplementClass( FragmentShader, Manager, CnObject );
	//Const/Dest
	Manager::Manager( const CnString& DataPath )
		: m_DataPath( DataPath )
		, m_EndPoint(0)
	{
	}
	Manager::~Manager()
	{
		Clear();
	}

	//================================================================
	/** @remarks		Data Path 설정	*/
	//================================================================
	void Manager::SetDataPath( const CnString& DataPath )
	{
		m_DataPath = DataPath;
	}

	//================================================================
	/** Load
		@remarks		Load Fragment File
	*/
	//================================================================
	bool Manager::Load( const CnString& FileName, bool bFullPath )
	{
		CnString fullPathName( FileName );
		if (false == bFullPath)
		{
			fullPathName = m_DataPath + FileName;
		}

		CnString fileName = unicode::SplitPathFullFileName( fullPathName );

		FragmentIter iter = m_Fragments.find( fileName );
		if (m_Fragments.end() != iter)
			return false;

		Fragment* fragment = new Fragment();
		if (false == fragment->Load( fullPathName ))
		{
			delete fragment;
			return false;
		}

		if (fragment->IsEnd())
		{
			m_EndPoint = fragment;
		}

		std::pair<FragmentIter, bool> result = m_Fragments.insert( FragmentMap::value_type( fileName, fragment ) );
		assert( result.second );
		return true;
	}

	//================================================================
	/** Find
		@remarks		찾기
		@param Title : Fragment의 Title (ex, "버텍스 입력", "출력" )
	*/
	//================================================================
	Fragment* Manager::Find( const CnString& Title )
	{
		FragmentIter iend = m_Fragments.end();
		for (FragmentIter i=m_Fragments.begin(); i!=iend; ++i)
		{
			if (i->second->m_Name == Title)
				return i->second;
		}
		return NULL;
	}

	//================================================================
	/** Clear
		@remarks	모두 삭제
	*/
	//================================================================
	void Manager::Clear()
	{
		FragmentIter iend = m_Fragments.end();
		for (FragmentIter i=m_Fragments.begin(); i!=iend; ++i)
			delete (i->second);

		m_Fragments.clear();
	}

	//================================================================
	/** Compile
		@remarks	Shader 생성 테스트
	*/
	//================================================================
	bool Manager::Compile( const CnString& FileName )
	{
		CMiniXML doc;
		if (!doc.Open( FileName.c_str() ))
			return false;

		// Root
		CMiniXML::iterator xmlRoot = doc.GetRoot();
		if (xmlRoot == NULL)
			return false;

		// Component들의 관계를 형성한다.
		CMiniXML::iterator xmlNode = xmlRoot.GetChild();
		MakeRelation( xmlNode, NULL );

		// Component들의 리스트를 만든다.
		MakeList();

		Parse();
		return false;
	}

	//================================================================
	/** Make Relation
		@remarks	Component들 간의 관계 형성
	*/
	//================================================================
	bool Manager::MakeRelation( CMiniXML::iterator& Parent, Fragment* ParentComponent )
	{
		CnStringArray tokens;

		const wchar* title = Parent.GetAttribute( _T("title") );
		Fragment* component = this->Find( title );
		if (0 == component)
			return false;

		// 부모에게 연결..
		if (ParentComponent)
		{
			ParentComponent->AddChild( component );
		}

		CMiniXML::iterator xmlChildNode = Parent.GetChild();
		for (; xmlChildNode != NULL; xmlChildNode=xmlChildNode.Next())
		{
			const wchar* nodeName = xmlChildNode.GetName();

			if (0==wcscmp( nodeName, _T("node")))
			{
				Fragment::Node* node = NULL;
				Fragment::Node* childNode = NULL;

				const wchar* name = xmlChildNode.GetAttribute( _T("node") );
				{
					tokens = unicode::Tokenize( name, L":" );
					node = component->Find( _wtoi(tokens[1].c_str()) );
				}
				const wchar* childName = xmlChildNode.GetAttribute( _T("link") );
				{
					tokens = unicode::Tokenize( childName, L":" );
					Fragment* childComponent = this->Find( tokens[0] );
					childNode = childComponent->Find( _wtoi(tokens[1].c_str()) );
				}

				if (node->shaderType != childNode->shaderType)
				{
					wchar buffer[256];
					swprintf( buffer, L"Invalid Linked: %s:%s, %s", title, name, childName ); 
					MessageBox(NULL, buffer, L"[Manager::MakeRelation]", NULL );
				}

				if (childNode)
				{
					childNode->next = node;
					node->prev = childNode;
				}
			}
			else if(0==wcscmp( nodeName, _T("component")))
			{
				MakeRelation( xmlChildNode, component );
			}
		}

		return true;
	}

	//================================================================
	/** Make List
		@remarks	연결된 Component들의 리스트를 만든다. 
					맨 앞에서 부터 순서대로 시작해서, 마지막까지 중복되는 내용을
					제거하고 만들어 낸다.
	*/
	//================================================================
	void Manager::MakeList()
	{
		m_Sorted.clear();
		m_Sorted.push_back( m_EndPoint );

		m_EndPoint->BuildList( m_Sorted );

		CnPrint( _T("[ComponentList]") );
		ComIter iend = m_Sorted.end();
		for (ComIter i=m_Sorted.begin(); i!=iend; ++i)
		{
			CnPrint( (*i)->m_Name.c_str() );
		}
	}

	//================================================================
	/** Print
		@remarks	Print
	*/
	//================================================================
	void Manager::Parse()
	{
		// Print...
		CnString result( L"" );

		ComIter iend = m_Sorted.end();
		for (ComIter i=m_Sorted.begin(); i!=iend; ++i)
		{
			result += (*i)->m_Header;
		}

		CnString a2v( L"" );
		{
			for (ComIter i=m_Sorted.begin(); i!=iend; ++i)
			{
				a2v += (*i)->m_VertexInput;
			}

			CnString head( _T("struct a2v{") );
			CnString tail( _T("}; \n") );

			result += (head + a2v + tail);
		}

		CnString v2f( L"" );
		{
			for (ComIter i=m_Sorted.begin(); i!=iend; ++i)
				v2f += (*i)->m_PixelInput;

			CnString head( _T("struct v2f{") );
			CnString tail( _T("}; \n") );

			result += (head + v2f + tail);
		}

		CnString vs( L"" );
		{
			Fragment* component = NULL;

			CnString temp;

			for (ComIter i=m_Sorted.begin(); i!=iend; ++i)
			{
				component = (*i);

				temp = _T("");

				ParseCondition( temp, component, component->m_VertexCodes );
				ParseInputVar( temp, component );

				vs += temp;
			}

			//CnPrint( vs.c_str() );
			wchar buffer[2048];
			wsprintf( buffer, L"\nv2f vs( a2v Input ) {\n\tv2f output = (v2f)0;\n\t%s return output;\n}\n", vs.c_str() );

			result += buffer;
		}

		CnString ps( L"" );
		{
			Fragment* component = NULL;

			CnString temp;

			for (ComIter i=m_Sorted.begin(); i!=iend; ++i)
			{
				component = (*i);

				temp = _T("");

				ParseCondition( temp, component, component->m_PixelCodes );
				ParseInputVar( temp, component );

				ps += temp;
			}

			wchar buffer[2048];
			wsprintf( buffer, L"\nfloat4 ps( v2f Input ) {\n\tfloat4 output = (float4)0;\n\t%s return output;\n}\n", ps.c_str() );

			result += buffer;

			CnPrint( buffer );
		}

		CnPrint( _T("") );
		CnPrint( _T("[HLSL]") );
		CnPrint( result.c_str() );
	}

	//================================================================
	/** Parse
		@remarks	Condition Code를 조합한다.
	*/
	//================================================================
	void Manager::ParseCondition( CnString& Code, Fragment* Component, Fragment::ConditionArray& Condtions )
	{
		Fragment::Condition* condition = NULL;

		Fragment::ConditionIter iv, ivend;
		iv = Condtions.begin();
		ivend = Condtions.end();
		for (; iv!=ivend; ++iv)
		{
			condition = (*iv);

			switch (condition->type)
			{
			case Fragment::Condition::Type::None:
				{
					Code += condition->code;
				}
				break;

			case Fragment::Condition::Type::NullCheck:
				{	// node에 연결된 노드가 있다면, 그 조건에 맞는 코드 블럭을 찾아서 복사.
					Fragment::Node* node = Component->Find( condition->nodeid );
					bool linked = (node && node->IsLinked()) ? true : false;
					if (linked == !condition->null)
						Code += condition->code;
				}
				break;

			case Fragment::Condition::Type::TypeCheck:
				{
				}
				break;

			default:
				{
					Code += condition->code;
				}
				break;
			}	//
		}
	}

	//================================================================
	/** Parse
		@remarks	입력 변수를 생성한다.
	*/
	//================================================================
	void Manager::ParseInputVar( CnString& Code, Fragment* Component )
	{
		// '%'가 되어 있는 녀석은 입력 데이터이므로,
		// 읽어들이면서, 변수를 만들어 놓는다.
		size_t start = 0;
		size_t cur = 0;

		do
		{
			cur = Code.find_first_of( _T("%") );
			if (cur == CnString::npos)
				break;

			CnString head = Code.substr( start, cur - start );
			CnString id = Code.substr( cur+1, 2 );
			CnString tail = Code.substr( cur + 2 );

			Fragment::Node* node = Component->Find( _wtoi(id.c_str() ) );

			if (node && node->prev)
			{
				Code = head + node->prev->tempVar + tail;
			}
			else
			{
				Code = head + Component->m_Code + id + tail;
			}
		}
		while (cur != CnString::npos);

	}
}
}
