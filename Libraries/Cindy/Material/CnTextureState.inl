//================================================================
// File:               : CnTextureState.inl
// Related Header File : CnTextureState.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 24
//================================================================

//----------------------------------------------------------------
inline
const CnTextureState::StageStateList& CnTextureState::GetStages() const
{
	return m_StageStates;
}
