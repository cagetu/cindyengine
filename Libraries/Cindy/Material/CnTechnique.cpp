//================================================================
// File:               : CnTechnique.cpp
// Related Header File : CnTechnique.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 14
//================================================================
#include "../Cindy.h"
#include "CnTechnique.h"
#include "CnShader.h"
#include "../Graphics/CnRenderer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnTechnique, CnObject);
	/// Const/Dest
	CnTechnique::CnTechnique( const CnString& strName )
		: m_strName( strName )
		, m_hHandle( 0 )
		, m_nNumPasses( 0 )
		, m_pShader(0)
	{
	}
	CnTechnique::~CnTechnique()
	{
		RemoveAllPasses();

		m_pShader = 0;
	}

	//----------------------------------------------------------------
	/**	@brief	부모 Shader 등록
	*/
	void CnTechnique::SetParent( CnShader* pShader )
	{
		m_pShader = pShader;
	}
	CnShader* CnTechnique::GetParent() const
	{
		return m_pShader;
	}

	//----------------------------------------------------------------
	/**	@brief	패스 추가
	*/
	Ptr<CnPass> CnTechnique::AddPass( const CnString& strName )
	{
		CnPass* pass = GetPass( strName );
		if (pass)
			return pass;

		pass = CnNew CnPass( strName );
		m_Passes.push_back( pass );

		m_nNumPasses++;
		return pass;
	}
	//----------------------------------------------------------------
	/** @brief	패스 얻어오기
	*/
	Ptr<CnPass> CnTechnique::GetPass( const CnString& strName )
	{
		PassIter iend = m_Passes.end();
		for (PassIter i=m_Passes.begin(); i != iend; ++i)
		{
			if (strName == (*i)->GetName())
				return (*i);
		}
		return NULL;
	}
	Ptr<CnPass> CnTechnique::GetPass( int nIndex )
	{
		if (nIndex >= m_nNumPasses)
			return NULL;

		return m_Passes[nIndex];
	}

	//----------------------------------------------------------------
	/**	@brief	모든 패스 제거
	*/
	void CnTechnique::RemoveAllPasses()
	{
		m_Passes.clear();

		m_nNumPasses = 0;
	}

	//----------------------------------------------------------------
	/** Begin
		@brief	Technique를 그리기 시작
	*/
	int CnTechnique::Begin( bool bSaveState )
	{
		CnShaderEffect* shaderEffect = RenderDevice->GetActiveShader();
		if (!shaderEffect)
			return -1;

#pragma NOTE("Technique의 End하 호출되기 전에 Begin이 호출되면, 처리가 불가능해준다.")
		shaderEffect->End();

		shaderEffect->SetTechnique( GetHandle() );
		return shaderEffect->Begin( bSaveState );
	}

	//----------------------------------------------------------------
	/** End
		@brief	Technique를 그리기 끝
	*/
	void CnTechnique::End()
	{
		CnShaderEffect* shaderEffect = RenderDevice->GetActiveShader();
		if (!shaderEffect)
			return;

		shaderEffect->End();
	}
}