//================================================================
// File:               : CnShaderEffect.cpp
// Related Header File : CnShaderEffect.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnShaderEffect.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnShaderEffect, CnObject );
	/// Const/Dest
	CnShaderEffect::CnShaderEffect()
	{
	}
	CnShaderEffect::~CnShaderEffect()
	{
		_ClearTechniques();
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnTechnique* CnShaderEffect::_AddTechnique( const CnString& strName )
	{
		TechniqueMap::iterator iter = m_Techniques.find( strName );
		if (iter == m_Techniques.end())
		{
			CnTechnique* technique = CnNew CnTechnique( strName );
			//technique->SetParent( this );

			m_Techniques.insert( TechniqueMap::value_type(strName, technique) );
			++m_nNumTechniques;

			return technique;
		}
		return iter->second;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnShaderEffect::_ClearTechniques()
	{
		m_Techniques.clear();
		m_nNumTechniques = 0;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnTechnique* CnShaderEffect::GetTechnique( const CnString& strName ) const
	{
		TechniqueMap::const_iterator iter = m_Techniques.find( strName );
		if (iter == m_Techniques.end())
			return NULL;

		return iter->second;
	}

	//------------------------------------------------------------------------------
	/** Get Technique
		@param		CnShaderFeature::Mask	: ShaderFeature
		@remarks	테크닉을 찾는다.
	*/
	CnTechnique* CnShaderEffect::GetTechnique( CnShaderFeature::Mask Mask ) const
	{
		TechniqueMap::const_iterator iend = m_Techniques.end();
		for (TechniqueMap::const_iterator i = m_Techniques.begin(); i != iend; ++i)
		{
			if ((i->second)->GetFeatureMask() == Mask)
				return (i->second);
		}
		return NULL;
	}
}