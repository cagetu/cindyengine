//================================================================
// File:               : CnShader.inl
// Related Header File : CnShader.h
// Original Author     : changhee
// Creation Date       : 2007. 5. 3
//================================================================

//================================================================
/** Get Number Of Techniques
    @remarks      테크닉의 개수
	@param		  none
	@return       int
*/
//================================================================
inline
int CnShader::GetNumTechniques() const
{
	return m_nNumTechniques;
}

inline
const CnShader::Technique* CnShader::GetTechnique( int nIndex ) const
{
	return m_Techniques[nIndex];
}

//================================================================
/** Has Parameter
    @remarks      쉐이더 내에 변수를 가지고 있는지 판별
	@param		  eParam : 검색할 파라미터
	@return       bool : 검색 결과
*/
//================================================================
inline
bool CnShader::HasParameter( ShaderState::Param eParam ) const
{
	return (m_pParameterHandles[eParam] == NULL) ? false : true;
}

//----------------------------------------------------------------
//	ShaderPtr Class
//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr()
: CnSmartPtr< CnShader >()
{
}

//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr( CnShader* pObject )
: CnSmartPtr< CnShader >( pObject )
{
}

//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr( const ShaderPtr& spObject )
: CnSmartPtr< CnShader >( spObject )
{
}

//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr( const RscPtr& spRsc )
: CnSmartPtr< CnShader >()
{
	m_pObject = (CnShader*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->IncreaseReferences();
	}
}

//----------------------------------------------------------------
inline
ShaderPtr& ShaderPtr::operator= ( const RscPtr& spRsc )
{
	if( m_pObject != (CnShader*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->DecreaseReferences();

		if( !spRsc.IsNull() )
			spRsc->IncreaseReferences();

		m_pObject = (CnShader*)spRsc.GetPtr();
	}

	return *this;
}