//================================================================
// File:               : CnMaterial.cpp
// Related Header File : CnMaterial.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 30
//================================================================
#include "../Cindy.h"
#include "CnMaterial.h"
#include "CnMaterialGroup.h"

#include "CnTechnique.h"
#include "CnPass.h"
#include "CnShaderGroup.h"

#include "../System/CnString.h"
#include "../System/CnLog.h"

#include <MoCommon/External/MoMiniXml.h>
#include <MoCommon/Stream/MoMemStream.h>

namespace Cindy
{
	//================================================================
	IMPLEMENT_RTTI( Cindy, CnMaterial, CnResource );

	/// Const/Dest
	CnMaterial::CnMaterial( CnResourceGroup* pParent )
		: CnResource( pParent )
		, m_pCurrentTechnique(0)
		, m_nNumTechniques(0)
	{
	}
	CnMaterial::~CnMaterial()
	{
		RemoveAllTechniques();
	}

	//================================================================
	bool CnMaterial::Load( const wchar* strFileName )
	{
		if (IsLoaded())
			return true;

		MoMemStream memStream;

		if (m_pParent->FindData( strFileName, memStream ))
		{
			unsigned char* buffer = memStream.GetBuffer();
			ulong size = memStream.GetSize();

			if (!LoadData( buffer, size ))
			{
				memStream.Close();
				return false;
			}

			SetState( Loaded );

			m_strName = strFileName;
			return true;
		}

		CNERROR( CnStringUtil::ToString( L"file not found: %s", strFileName ).c_str() );
		return false;
	}
	bool CnMaterial::Unload()
	{
		RemoveAllTechniques();

		SetState( Unloaded );

		m_strName.clear();

		return false;
	}

	//================================================================
	void CnMaterial::OnLost()
	{
	}
	void CnMaterial::OnRestore()
	{
	}

	//================================================================
	/** Add Technique
		@remarks	테크닉을 추가한다.
	*/
	//================================================================
	CnTechnique* CnMaterial::AddTechnique( const CnString& strName )
	{
		TechniqueIter iter = m_Techniques.find( strName );
		if( iter == m_Techniques.end() )
		{
			CnTechnique* pTechnique = CnNew CnTechnique( strName );
			pTechnique->SetParent( this );

			m_Techniques.insert( TechniqueList::value_type( strName, pTechnique ) );
//			m_nNumTechniques = (int)m_Techniques.size();
			++m_nNumTechniques;

			return pTechnique;
		}
		return iter->second;
	}

	//----------------------------------------------------------------
	CnTechnique* CnMaterial::GetTechnique( const CnString& strName )
	{
		TechniqueIter iter = m_Techniques.find( strName );
		if( iter == m_Techniques.end() )
			return NULL;

		return (iter->second);
	}
	const CnMaterial::TechniqueList& CnMaterial::GetTechniques() const
	{
		return m_Techniques;
	}

	//----------------------------------------------------------------
	void CnMaterial::RemoveAllTechniques()
	{
		TechniqueIter iend = m_Techniques.end();
		for( TechniqueIter i = m_Techniques.begin(); i != iend; ++i )
			delete (i->second);

		m_Techniques.clear();
		m_pCurrentTechnique = NULL;
		m_nNumTechniques = 0;
	}

	//================================================================
	/** Set Current Technique
	    @remarks      현재 사용될 technique를 설정한다. 오직 하나의 technique만 실행시점에서 사용이 가능하다.
		@param        strName : technique 이름
		@return       none
	*/
	//================================================================
	void CnMaterial::SetCurrentTechnique( const CnString& strName )
	{
		m_pCurrentTechnique = GetTechnique( strName );
	}

	//----------------------------------------------------------------
	CnTechnique* CnMaterial::GetCurrentTechnique() const
	{
		return m_pCurrentTechnique;
	}

	//================================================================
	/** Set Shader
	    @remarks      Shader 파일을 읽어온다.
		@param        pszName : 쉐이더 파일 이름
		@return       true/false
	*/
	//================================================================
	bool CnMaterial::SetShader( const wchar* pszFileName )
	{
		ShaderPtr shaderPtr = CnShaderGroup::Instance()->Load( pszFileName );
		if (shaderPtr.IsNull())
			return false;

		return SetShader( shaderPtr );
	}
	bool CnMaterial::SetShader( const ShaderPtr& pShader )
	{
		CnString strName;

		int numTechnique = pShader->GetNumTechniques();
		for (int tech=0; tech < numTechnique; ++tech)
		{
			const CnShader::Technique* pTechniqueData = pShader->GetTechnique(tech);

			CnStringUtil::ConvertCharToWChar( pTechniqueData->name.c_str(), strName );
			CnTechnique* pTechnique = AddTechnique( strName );
			if (pTechnique)
			{
				pTechnique->SetHandle( pTechniqueData->handle );

				for (int pass=0; pass < pTechniqueData->numpasses; ++pass)
				{
					CnShader::Pass* pPassData = pTechniqueData->passes[pass];

					CnStringUtil::ConvertCharToWChar( pPassData->name.c_str(), strName );
					CnPass* pPass = pTechnique->AddPass( strName );
					pPass->SetHandle( pPassData->handle );
					pPass->SetIndex( pass );

					pPass->SetParameters( pPassData->parameters );
				}
			} // if (pTechnique)
		} // for

		m_spShader = pShader;
		return true;
	}

	//----------------------------------------------------------------
	bool CnMaterial::IsTransparent()
	{
		if (NULL == m_pCurrentTechnique)
			return false;

		int numPasses = m_pCurrentTechnique->GetNumPasses();
		if (numPasses == 0)
			return false;

		if (numPasses > 1)
		{
			CnTechnique::PassList Passes = m_pCurrentTechnique->GetPasses();
			CnTechnique::PassList::const_iterator ip, ipend;
			ipend = Passes.end();
			for (ip = Passes.begin(); ip != ipend; ++ip)
			{
				if ((*ip)->GetRenderState().GetAlphaBlendEnable())
					return true;
			}
		}
		else
		{
			return m_pCurrentTechnique->GetPass(0)->GetRenderState().GetAlphaBlendEnable();
		}

		return false;
	}

	//================================================================
	/** operator ==
	    @remarks      같은 메터리얼인지 비교 판단
	*/
	//================================================================
	bool CnMaterial::operator ==( const CnMaterial* pMaterial ) const
	{
		return ( GetName() == pMaterial->GetName() );
	}

} // end of Cindy