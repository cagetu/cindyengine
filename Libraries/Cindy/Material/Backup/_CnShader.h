//================================================================
// File:           : CnShader.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_SHADER_H__
#define __CN_SHADER_H__

#include "CnShaderDefines.h"
#include "../Math/CnMatrix4x4.h"
#include "../Resource/CnResource.h"

#include "CnTechnique.h"

namespace Cindy
{
	class CnTexture;

	//================================================================
	/** Shader 
	    @author    changhee
		@since     2007. 2. 21
		@remarks   쉐이더 클래스
	*/
	//================================================================
	class CN_DLL CnShader abstract : public CnResource
	{
		DECLARE_RTTI;
		friend class CnShaderGroup;
	public:
		struct Pass
		{
			ShaderHandle		handle;
			std::string			name;

			ShaderHandle		parameters[ShaderState::NumParameters];	//!< 이펙트 파일 내에 파라미터 목록

			Pass();
			~Pass();
		};

		struct Technique
		{
			ShaderHandle		handle;
			std::string			name;
			int					numpasses;
			std::vector<Pass*>	passes;

			Technique();
			~Technique();
		};

	protected:
		typedef std::vector<Technique*>		TechniqueList;
		typedef TechniqueList::iterator		TechniqueIter;

		typedef HashMap<CnString, CnTechnique*>		TechniqueList;
		typedef TechniqueList::iterator				TechniqueIter;
		typedef TechniqueList::const_iterator		TechniqueConstIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		TechniqueList		m_Techniques;					//!< Technique 리스트
		int					m_nNumTechniques;				//!< Technique 개수

		ShaderHandle		m_pParameterHandles[ShaderState::NumParameters];	//!< 이펙트 파일 내에 파라미터 목록

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual void	_UpdateTechniques() abstract;
		void			_RemoveAllTechniques();

	protected:
		CnShader( CnResourceGroup* pParent );

	public:
		virtual ~CnShader();

		// Load/Unload
		bool					Load( const wchar* strFileName ) override;
		bool					Unload() override;

		// Lost/Restore
		void					OnLost() override;
		void					OnRestore() override;

		// Technique
		int						GetNumTechniques() const;
		const Technique*		GetTechnique( int nIndex ) const;

		// Parameters
		bool					HasParameter( ShaderState::Param eParam ) const;

		// Technique
		virtual void			SetTechnique( ShaderHandle hTechnique ) abstract;
		//virtual ShaderHandle	GetTechnique( uint nIndex ) const abstract;
		//virtual ShaderHandle	GetCurrentTechnique() const abstract;

		// Shader Begin/End
		virtual int				Begin( bool bSaveState = true ) abstract;
		virtual void			End() abstract;

		// Device Lost/Reset
		virtual void			Clear() abstract;

		// Pass
		virtual void			BeginPass( uint nPass ) abstract;
		virtual void			EndPass() abstract;
		virtual void			CommitChanges() abstract;

		// Set
		virtual void			SetBool( ShaderHandle hHandle, bool val ) abstract;
		virtual void			SetInt( ShaderHandle hHandle, int val ) abstract;
		virtual void			SetFloat( ShaderHandle hHandle, float val ) abstract;
		//virtual void			SetFloat4( ShaderHandle hHandle, float* val ) abstract;

		virtual void			SetVector3( ShaderHandle hHandle, const CnVector3& val ) abstract;
		virtual void			SetVector4( ShaderHandle hHandle, const CnVector4& val ) abstract;
		virtual void			SetMatrix( ShaderHandle hHandle, const CnMatrix4x4& val ) abstract;

		virtual void			SetVectorArray( ShaderHandle hHandle, CnVector4* aVectors, int nSize ) abstract;
		virtual void			SetMatrixArray( ShaderHandle hHandle, CnMatrix4x4* aMatrices, int nSize ) abstract;

		virtual void			SetTexture( ShaderHandle hHandle, CnTexture* texture ) abstract;
	};

	//==================================================================
	/** ShaderPtr
		@author			cagetu
		@since			2007년 7월 18일
		@remarks		Shader 파일용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL ShaderPtr : public CnSmartPtr<CnShader>
	{
	public:
		ShaderPtr();
		explicit ShaderPtr( CnShader* pObject );
		ShaderPtr( const ShaderPtr& spObject );
		ShaderPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		ShaderPtr& operator= ( const RscPtr& spRsc );
	};

#include "CnShader.inl"
}

#endif	// __CN_SHADER_H__