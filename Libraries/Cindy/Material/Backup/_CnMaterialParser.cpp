//================================================================
// File:               : CnMaterial.cpp
// Related Header File : CnMaterial.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 30
//================================================================
#include "../Cindy.h"
#include "CnMaterial.h"

#include "CnTechnique.h"
#include "CnPass.h"
#include "CnShaderGroup.h"

#include "../System/CnString.h"
//#include "../Resource/CnResourceManager.h"

#include <MoCommon/Stream/MoFileStream.h>
#include <MoCommon/External/MoMiniXml.h>

namespace Cindy
{
	//================================================================
	/** Load Data
		@remarks	실제 데이터를 읽는다.

<Material>
	<Date date="7/23/2007 15:26:16"></Date>
	
	<!-- 쉐이더 정보 -->
	<Shader name="NormalGeometry.fx" technique="Geometry"></Shader>

	<Technique name="Geometry">
		<Pass id="0">
			<!-- 매터리얼 상태값 -->
			<ColorState>
				<Ambient r="255" g="255" b="255" a="255"></Ambient>
				<Diffuse r="255" g="255" b="255" a="255"></Diffuse>
				<Specular r="255" g="255" b="255" a="255"></Specular>
				<Emissive r="255" g="255" b="255" a="255"></Emissive>
				<Shininess value="10"></Shininess>
			</ColorState>

			<!-- RenderState -->
			<RenderState>
				<LightEnable enable="false"></LightEnable>
				<CullMode mode="none"></CullMode>
				<FillMode mode="solid"></FillMode>
				<AlphaBlend enable="false" src ="SrcBlend" dest="InvSrcBlend"></AlphaBlend>
				<AlphaTest enable="true" ref="64"></AlphaTest>
				<TFactor r="255" g="255" b="255" a="255"></TFactor>
				<!--<ZBuffer enable="true" write="true" mode="LessEqual"></ZBuffer>-->
			</RenderState>

			<!-- 텍스쳐 정보 
			<TextureState>
				<Diffuse name="ddd"></Diffuse>
				<Specular name="ddd"></Specular>
				<Normal name="ddd"></Normal>
			</TextureState>
			-->
		</Pass>
	</Technique>
	
</Material>
	*/
	//================================================================
	bool CnMaterial::LoadData( unsigned char* pbuffer, int size )
	{
		using namespace MoCommon;

		MoMiniXML doc;
		if (!doc.ReadFromMemory( pbuffer, size ))
			return false;

		// "Material"
		MoMiniXML::CNode materialnode = doc.GetRoot();
		if (materialnode == NULL)
			return false;

		CnString nodename;
		CnString string;

		MoMiniXML::CNode subnode = materialnode.GetChildNode();
		for (; subnode != NULL; subnode = subnode.Next())
		{
			nodename = subnode.GetName();

			// "Shader"
			if (nodename == L"Shader")
			{
				// "name"
				string = subnode.GetAttribute( _T("name") );
				string = string;
				SetShader( string.c_str() );

				// "technique"
				string = subnode.GetAttribute( _T("technique") );
				SetCurrentTechnique( string );
			}
			// "Technique"
			else if(nodename == L"Technique")
			{
				CnString techniquename = subnode.GetAttribute( L"name" );
				CnTechnique* pTechnique = GetTechnique( techniquename );
				if (!pTechnique)
					continue;

				// Temporary Values
				CnString statename;
				CnString buffer;
				int value;
				float red, green, blue, alpha;

				int index = 0;
				MoMiniXML::CNode passnode = subnode.GetChildNode();
				for (passnode=passnode.Find(_T("Pass")); passnode != NULL; passnode = passnode.Find(_T("Pass")), ++index)
				{
					CnPass* pPass = pTechnique->GetPass(index);
					if (!pPass)
						continue;

					MoMiniXML::CNode passvaluenode = passnode.GetChildNode();
					for (; passvaluenode != NULL; passvaluenode = passvaluenode.Next())
					{
						CnString nodename = passvaluenode.GetName();
						if (nodename == L"ColorState")
						{
							CnMaterialState materialstate;

							MoMiniXML::CNode mtlnode = passvaluenode.GetChildNode();
							for (; mtlnode != NULL; mtlnode = mtlnode.Next())
							{
								statename = mtlnode.GetName();
								if (statename == L"Ambient")
								{
									buffer = mtlnode.GetAttribute( L"r");
									value = _wtoi( buffer.c_str() );
									red = (float)value / 255.0f;
									
									buffer = mtlnode.GetAttribute( L"g");
									value = _wtoi( buffer.c_str() );
									green = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"b");
									value = _wtoi( buffer.c_str() );
									blue = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"a");
									value = _wtoi( buffer.c_str() );
									alpha = (float)value / 255.0f;

									materialstate.SetAmbient( CnColor( red, green, blue, alpha ) );
								}
								else if (statename == L"Diffuse")
								{
									buffer = mtlnode.GetAttribute( L"r");
									value = _wtoi( buffer.c_str() );
									red = (float)value / 255.0f;
									
									buffer = mtlnode.GetAttribute( L"g");
									value = _wtoi( buffer.c_str() );
									green = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"b");
									value = _wtoi( buffer.c_str() );
									blue = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"a");
									value = _wtoi( buffer.c_str() );
									alpha = (float)value / 255.0f;

									materialstate.SetDiffuse( CnColor( red, green, blue, alpha ) );
								}
								else if (statename == L"Specular")
								{
									buffer = mtlnode.GetAttribute( L"r");
									value = _wtoi( buffer.c_str() );
									red = (float)value / 255.0f;
									
									buffer = mtlnode.GetAttribute( L"g");
									value = _wtoi( buffer.c_str() );
									green = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"b");
									value = _wtoi( buffer.c_str() );
									blue = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"a");
									value = _wtoi( buffer.c_str() );
									alpha = (float)value / 255.0f;

									materialstate.SetSpecular( CnColor( red, green, blue, alpha ) );
								}
								else if (statename == L"Emissive")
								{
									buffer = mtlnode.GetAttribute( L"r");
									value = _wtoi( buffer.c_str() );
									red = (float)value / 255.0f;
									
									buffer = mtlnode.GetAttribute( L"g");
									value = _wtoi( buffer.c_str() );
									green = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"b");
									value = _wtoi( buffer.c_str() );
									blue = (float)value / 255.0f;

									buffer = mtlnode.GetAttribute( L"a");
									value = _wtoi( buffer.c_str() );
									alpha = (float)value / 255.0f;

									materialstate.SetEmissive( CnColor( red, green, blue, alpha ) );
								}
								else if (statename == L"Shininess")
								{
									buffer = mtlnode.GetAttribute( L"value");
									value = _wtoi( buffer.c_str() );

									materialstate.SetShininess( (float)value );
								}
							}

							pPass->SetMaterialState( materialstate );
						}
						else if (nodename == L"RenderState")
						{
							CnRenderState renderstate;

							MoMiniXML::CNode rsnode = passvaluenode.GetChildNode();
							for (; rsnode != NULL; rsnode = rsnode.Next())
							{
								statename = rsnode.GetName();
								if (statename == L"CullMode")
								{
									CnRenderState::CullMode cullmode;

									buffer = rsnode.GetAttribute( L"mode" );
									CnStringUtil::LowerCase( buffer );
									if (buffer == L"ccw")					cullmode = CnRenderState::CULL_CCW;	
									else if(buffer == L"cw")				cullmode = CnRenderState::CULL_CW;
									else if(buffer == L"none")				cullmode = CnRenderState::CULL_NONE;

									renderstate.SetCullMode( cullmode );
								}
								else if (statename == L"FillMode")
								{
									CnRenderState::FillMode fillmode;

									buffer = rsnode.GetAttribute( L"mode" );
									CnStringUtil::LowerCase( buffer );
									if (buffer == L"point")					fillmode = CnRenderState::FILL_POINT;	
									else if(buffer == L"wireframe")			fillmode = CnRenderState::FILL_WIREFRAME;
									else if(buffer == L"solid")				fillmode = CnRenderState::FILL_SOLID;

									renderstate.SetFillMode( fillmode );
								}
								else if (statename == L"LightEnable")
								{
									buffer = rsnode.GetAttribute( L"enable" );
									if (buffer == L"true")					renderstate.SetLightEnable( 1 );
									else									renderstate.SetLightEnable( 0 );
								}
								else if (statename == L"AlphaBlend")
								{
									buffer = rsnode.GetAttribute( L"enable" );
									CnStringUtil::LowerCase( buffer );
									if (buffer == L"true")
									{
										// AlphaBlendEnable
										renderstate.SetAlphaBlendEnable( true );

										// SrcBlend
										CnRenderState::BlendMode srcmode;
										{
											buffer = rsnode.GetAttribute( L"src" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"zero")					srcmode = CnRenderState::BLEND_ZERO;
											else if (buffer == L"one")				srcmode = CnRenderState::BLEND_ONE;
											else if (buffer == L"srccolor")			srcmode = CnRenderState::BLEND_SRCCOLOR;
											else if (buffer == L"invsrccolor")		srcmode = CnRenderState::BLEND_INVSRCCOLOR;
											else if (buffer == L"srcalpha")			srcmode = CnRenderState::BLEND_SRCALPHA;
											else if (buffer == L"invsrcalpha")		srcmode = CnRenderState::BLEND_INVSRCALPHA;
											else if (buffer == L"destalpha")		srcmode = CnRenderState::BLEND_DESTALPHA;
											else if (buffer == L"invdestalpha")		srcmode = CnRenderState::BLEND_INVDESTALPHA;
											else if (buffer == L"destcolor")		srcmode = CnRenderState::BLEND_DESTCOLOR;
											else if (buffer == L"invdestcolor")		srcmode = CnRenderState::BLEND_INVDESTCOLOR;
											else if (buffer == L"srcalphasat")		srcmode = CnRenderState::BLEND_SRCALPHASAT;
											else if (buffer == L"bothsrcalpha")		srcmode = CnRenderState::BLEND_BOTHSRCALPHA;
											else if (buffer == L"bothinvsrcalpha")	srcmode = CnRenderState::BLEND_BOTHINVSRCALPHA;
											else if (buffer == L"blendfactor")		srcmode = CnRenderState::BLEND_BLENDFACTOR;
											else if (buffer == L"invblendfactor")	srcmode = CnRenderState::BLEND_INVBLENDFACTOR;
											else									Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState SrcBlend'");
										}
										renderstate.SetAlphaSrcBlend( srcmode );

										// DestBlend
										CnRenderState::BlendMode destmode;
										{
											buffer = rsnode.GetAttribute( L"dest" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"zero")					destmode = CnRenderState::BLEND_ZERO;
											else if (buffer == L"one")				destmode = CnRenderState::BLEND_ONE;
											else if (buffer == L"srccolor")			destmode = CnRenderState::BLEND_SRCCOLOR;
											else if (buffer == L"invsrccolor")		destmode = CnRenderState::BLEND_INVSRCCOLOR;
											else if (buffer == L"srcalpha")			destmode = CnRenderState::BLEND_SRCALPHA;
											else if (buffer == L"invsrcalpha")		destmode = CnRenderState::BLEND_INVSRCALPHA;
											else if (buffer == L"destalpha")		destmode = CnRenderState::BLEND_DESTALPHA;
											else if (buffer == L"invdestalpha")		destmode = CnRenderState::BLEND_INVDESTALPHA;
											else if (buffer == L"destcolor")		destmode = CnRenderState::BLEND_DESTCOLOR;
											else if (buffer == L"invdestcolor")		destmode = CnRenderState::BLEND_INVDESTCOLOR;
											else if (buffer == L"srcalphasat")		destmode = CnRenderState::BLEND_SRCALPHASAT;
											else if (buffer == L"bothsrcalpha")		destmode = CnRenderState::BLEND_BOTHSRCALPHA;
											else if (buffer == L"bothinvsrcalpha")	destmode = CnRenderState::BLEND_BOTHINVSRCALPHA;
											else if (buffer == L"blendfactor")		destmode = CnRenderState::BLEND_BLENDFACTOR;
											else if (buffer == L"invblendfactor")	destmode = CnRenderState::BLEND_INVBLENDFACTOR;
											else									Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState DestBlend'");
										}
										renderstate.SetAlphaDstBlend( destmode );
									}
									else
									{
										renderstate.SetAlphaBlendEnable( false );
									}
								}
								else if (statename == L"AlphaTest")
								{
									buffer = rsnode.GetAttribute( L"enable" );
									CnStringUtil::LowerCase( buffer );
									if (buffer == L"true")
									{
										// AlphaTestEnable
										renderstate.SetAlphaTestEnable( true );

										// AlphaRef
										buffer = rsnode.GetAttribute( L"ref" );
										long ref = _wtol( buffer.c_str() );
										renderstate.SetAlphaRef( (ulong)ref );

										// AlpaFunc
										CnRenderState::ZFunc alphafunc;
										{
											buffer = rsnode.GetAttribute( L"mode" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"less")				alphafunc = CnRenderState::FUNC_LESS;
											else if(buffer == L"equal")			alphafunc = CnRenderState::FUNC_EQUAL;
											else if(buffer == L"lessequal")		alphafunc = CnRenderState::FUNC_LESSEQUAL;
											else if(buffer == L"greater")		alphafunc = CnRenderState::FUNC_GREATER;
											else if(buffer == L"notequal")		alphafunc = CnRenderState::FUNC_NOTEQUAL;
											else if(buffer == L"greaterequal")	alphafunc = CnRenderState::FUNC_GREATEREQUAL;
											else if(buffer == L"always")		alphafunc = CnRenderState::FUNC_ALWAYS;
											else if(buffer == L"never")			alphafunc = CnRenderState::FUNC_NEVER;
											else								Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState AlphaFunc'");
										}
										renderstate.SetAlphaFunc( alphafunc );
									}
									else
									{
										renderstate.SetAlphaTestEnable( false );
									}
								}
								else if (statename == L"ZBuffer")
								{
									// ZBufferEnable
									buffer = rsnode.GetAttribute( L"enable" );
									CnStringUtil::LowerCase( buffer );
									if (buffer == L"true")				renderstate.SetZBufferEnable( 1 );
									else								renderstate.SetZBufferEnable( 0 );

									// ZWriteEnable
									CnRenderState::ZBufferMode zbuffermode;
									{
										buffer = rsnode.GetAttribute( L"write" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"true")				zbuffermode = CnRenderState::ZB_USEWBUFFER;
										else								zbuffermode = CnRenderState::ZB_DISABLE;

										buffer = rsnode.GetAttribute( L"mode" );
									}
									renderstate.SetZBufferMode( zbuffermode );

									// ZFunc
									CnRenderState::ZFunc zfunc;
									{
										buffer = rsnode.GetAttribute( L"mode" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"less")					zfunc = CnRenderState::FUNC_LESS;
										else if(buffer == L"equal")				zfunc = CnRenderState::FUNC_EQUAL;
										else if(buffer == L"lessequal")			zfunc = CnRenderState::FUNC_LESSEQUAL;
										else if(buffer == L"greater")			zfunc = CnRenderState::FUNC_GREATER;
										else if(buffer == L"notequal")			zfunc = CnRenderState::FUNC_NOTEQUAL;
										else if(buffer == L"greaterequal")		zfunc = CnRenderState::FUNC_GREATEREQUAL;
										else if(buffer == L"always")			zfunc = CnRenderState::FUNC_ALWAYS;
										else if(buffer == L"never")				zfunc = CnRenderState::FUNC_NEVER;
										else									Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState ZFunc'");
									}
									renderstate.SetZBufferFunc( zfunc );
								}
								else if (statename == L"TFactor")
								{
									buffer = rsnode.GetAttribute( L"r");
									value = _wtoi( buffer.c_str() );
									red = (float)value / 255.0f;

									buffer = rsnode.GetAttribute( L"g");
									value = _wtoi( buffer.c_str() );
									green = (float)value / 255.0f;

									buffer = rsnode.GetAttribute( L"b");
									value = _wtoi( buffer.c_str() );
									blue = (float)value / 255.0f;

									buffer = rsnode.GetAttribute( L"a");
									value = _wtoi( buffer.c_str() );
									alpha = (float)value / 255.0f;

									CnColor color( red, green, blue, alpha );
									renderstate.SetTFactor( color.GetRGBA() );
								}
							}

							pPass->SetRenderState( renderstate );
						}
						else if (nodename == L"TextureState")
						{
							CnTextureState texturestate;

							MoMiniXML::CNode stagenode = passvaluenode.GetChildNode();
							for (stagenode=stagenode.Find(_T("Stage")); stagenode != NULL; stagenode=stagenode.Find(_T("Stage")))
							{
								buffer = stagenode.GetAttribute( L"id" );
								int nNumStage = _wtoi( buffer.c_str() );

								CnTextureState::StageState stagestate;

								MoMiniXML::CNode opnode = stagenode.GetChildNode();
								for (; opnode != NULL; opnode = opnode.Next())
								{
									nodename = opnode.GetName();
									if (nodename == L"Color")
									{
										CnTextureState::TextureOp opmode;

										buffer = opnode.GetAttribute( L"type" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"disable")							opmode = CnTextureState::TOP_DISABLE;
										else if (buffer == L"selectarg1")					opmode = CnTextureState::TOP_SELECTARG1;
										else if (buffer == L"selectarg2")					opmode = CnTextureState::TOP_SELECTARG2;
										else if (buffer == L"modulate")						opmode = CnTextureState::TOP_MODULATE;
										else if (buffer == L"modulate2x")					opmode = CnTextureState::TOP_MODULATE2X;
										else if (buffer == L"modulate4x")					opmode = CnTextureState::TOP_MODULATE4X;
										else if (buffer == L"add")							opmode = CnTextureState::TOP_ADD;
										else if (buffer == L"addsigned")					opmode = CnTextureState::TOP_ADDSIGNED;
										else if (buffer == L"addsigned2x")					opmode = CnTextureState::TOP_ADDSIGNED2X;
										else if (buffer == L"subtract")						opmode = CnTextureState::TOP_SUBTRACT;
										else if (buffer == L"addsmooth")					opmode = CnTextureState::TOP_ADDSMOOTH;
										else if (buffer == L"blenddiffusealpha")			opmode = CnTextureState::TOP_BLENDDIFFUSEALPHA;
										else if (buffer == L"blendtexturealpha")			opmode = CnTextureState::TOP_BLENDTEXTUREALPHA;
										else if (buffer == L"blendfactoralpha")				opmode = CnTextureState::TOP_BLENDFACTORALPHA;
										else if (buffer == L"blendtexturealphapm")			opmode = CnTextureState::TOP_BLENDTEXTUREALPHAPM;
										else if (buffer == L"blendcurrentalpha")			opmode = CnTextureState::TOP_BLENDCURRENTALPHA;
										else if (buffer == L"premodulate")					opmode = CnTextureState::TOP_PREMODULATE;
										else if (buffer == L"modulatealpha_addcolor")		opmode = CnTextureState::TOP_MODULATEALPHA_ADDCOLOR;
										else if (buffer == L"modulatecolor_addalpha")		opmode = CnTextureState::TOP_MODULATECOLOR_ADDALPHA;
										else if (buffer == L"modulateinvalpha_addcolor")	opmode = CnTextureState::TOP_MODULATEINVALPHA_ADDCOLOR;
										else if (buffer == L"modulateinvcolor_addalpha")	opmode = CnTextureState::TOP_MODULATEINVCOLOR_ADDALPHA;
										else if (buffer == L"bumpenvmap")					opmode = CnTextureState::TOP_BUMPENVMAP;
										else if (buffer == L"bumpenvmapluminance")			opmode = CnTextureState::TOP_BUMPENVMAPLUMINANCE;
										else if (buffer == L"dotproduct3")					opmode = CnTextureState::TOP_DOTPRODUCT3;
										else if (buffer == L"multiplyadd")					opmode = CnTextureState::TOP_MULTIPLYADD;
										else if (buffer == L"lerp")							opmode = CnTextureState::TOP_LERP;
										else												Assert( 0, L"[CnMaterial::LoadData] Failed 'Color Op'");

										CnTextureState::TextureAlpha alphaarg1, alphaarg2;
										buffer = opnode.GetAttribute( L"arg1" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"selectmark")						alphaarg1 = CnTextureState::TA_SELECTMASK;
										else if (buffer == L"diffuse")						alphaarg1 = CnTextureState::TA_DIFFUSE;
										else if (buffer == L"current")						alphaarg1 = CnTextureState::TA_CURRENT;
										else if (buffer == L"texture")						alphaarg1 = CnTextureState::TA_TEXTURE;
										else if (buffer == L"tfactor")						alphaarg1 = CnTextureState::TA_TFACTOR;
										else if (buffer == L"specular")						alphaarg1 = CnTextureState::TA_SPECULAR;
										else if (buffer == L"temp")							alphaarg1 = CnTextureState::TA_TEMP;
										else if (buffer == L"constant")						alphaarg1 = CnTextureState::TA_CONSTANT;
										else if (buffer == L"complement")					alphaarg1 = CnTextureState::TA_COMPLEMENT;
										else if (buffer == L"alphareplicate")				alphaarg1 = CnTextureState::TA_ALPHAREPLICATE;
										else												Assert( 0, L"[CnMaterial::LoadData] Failed 'Color arg1'");

										buffer = opnode.GetAttribute( L"arg2" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"selectmark")						alphaarg2 = CnTextureState::TA_SELECTMASK;
										else if (buffer == L"diffuse")						alphaarg2 = CnTextureState::TA_DIFFUSE;
										else if (buffer == L"current")						alphaarg2 = CnTextureState::TA_CURRENT;
										else if (buffer == L"texture")						alphaarg2 = CnTextureState::TA_TEXTURE;
										else if (buffer == L"tfactor")						alphaarg2 = CnTextureState::TA_TFACTOR;
										else if (buffer == L"specular")						alphaarg2 = CnTextureState::TA_SPECULAR;
										else if (buffer == L"temp")							alphaarg2 = CnTextureState::TA_TEMP;
										else if (buffer == L"constant")						alphaarg2 = CnTextureState::TA_CONSTANT;
										else if (buffer == L"complement")					alphaarg2 = CnTextureState::TA_COMPLEMENT;
										else if (buffer == L"alphareplicate")				alphaarg2 = CnTextureState::TA_ALPHAREPLICATE;
										else												Assert( 0, L"[CnMaterial::LoadData] Failed 'Color arg2'");

										stagestate.color.top = opmode;
										stagestate.color.arg1 = alphaarg1;
										stagestate.color.arg2 = alphaarg2;
									}
									else if(nodename == L"Alpha")
									{
										CnTextureState::TextureOp opmode;

										buffer = opnode.GetAttribute( L"type" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"disable")							opmode = CnTextureState::TOP_DISABLE;
										else if (buffer == L"selectarg1")					opmode = CnTextureState::TOP_SELECTARG1;
										else if (buffer == L"selectarg2")					opmode = CnTextureState::TOP_SELECTARG2;
										else if (buffer == L"modulate")						opmode = CnTextureState::TOP_MODULATE;
										else if (buffer == L"modulate2x")					opmode = CnTextureState::TOP_MODULATE2X;
										else if (buffer == L"modulate4x")					opmode = CnTextureState::TOP_MODULATE4X;
										else if (buffer == L"add")							opmode = CnTextureState::TOP_ADD;
										else if (buffer == L"addsigned")					opmode = CnTextureState::TOP_ADDSIGNED;
										else if (buffer == L"addsigned2x")					opmode = CnTextureState::TOP_ADDSIGNED2X;
										else if (buffer == L"subtract")						opmode = CnTextureState::TOP_SUBTRACT;
										else if (buffer == L"addsmooth")					opmode = CnTextureState::TOP_ADDSMOOTH;
										else if (buffer == L"blenddiffusealpha")			opmode = CnTextureState::TOP_BLENDDIFFUSEALPHA;
										else if (buffer == L"blendtexturealpha")			opmode = CnTextureState::TOP_BLENDTEXTUREALPHA;
										else if (buffer == L"blendfactoralpha")				opmode = CnTextureState::TOP_BLENDFACTORALPHA;
										else if (buffer == L"blendtexturealphapm")			opmode = CnTextureState::TOP_BLENDTEXTUREALPHAPM;
										else if (buffer == L"blendcurrentalpha")			opmode = CnTextureState::TOP_BLENDCURRENTALPHA;
										else if (buffer == L"premodulate")					opmode = CnTextureState::TOP_PREMODULATE;
										else if (buffer == L"modulatealpha_addcolor")		opmode = CnTextureState::TOP_MODULATEALPHA_ADDCOLOR;
										else if (buffer == L"modulatecolor_addalpha")		opmode = CnTextureState::TOP_MODULATECOLOR_ADDALPHA;
										else if (buffer == L"modulateinvalpha_addcolor")	opmode = CnTextureState::TOP_MODULATEINVALPHA_ADDCOLOR;
										else if (buffer == L"modulateinvcolor_addalpha")	opmode = CnTextureState::TOP_MODULATEINVCOLOR_ADDALPHA;
										else if (buffer == L"bumpenvmap")					opmode = CnTextureState::TOP_BUMPENVMAP;
										else if (buffer == L"bumpenvmapluminance")			opmode = CnTextureState::TOP_BUMPENVMAPLUMINANCE;
										else if (buffer == L"dotproduct3")					opmode = CnTextureState::TOP_DOTPRODUCT3;
										else if (buffer == L"multiplyadd")					opmode = CnTextureState::TOP_MULTIPLYADD;
										else if (buffer == L"lerp")							opmode = CnTextureState::TOP_LERP;
										else												Assert( 0, L"[CnMaterial::LoadData] Failed 'Alpha Op'");

										CnTextureState::TextureAlpha alphaarg1, alphaarg2;
										buffer = opnode.GetAttribute( L"arg1" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"selectmark")						alphaarg1 = CnTextureState::TA_SELECTMASK;
										else if (buffer == L"diffuse")						alphaarg1 = CnTextureState::TA_DIFFUSE;
										else if (buffer == L"current")						alphaarg1 = CnTextureState::TA_CURRENT;
										else if (buffer == L"texture")						alphaarg1 = CnTextureState::TA_TEXTURE;
										else if (buffer == L"tfactor")						alphaarg1 = CnTextureState::TA_TFACTOR;
										else if (buffer == L"specular")						alphaarg1 = CnTextureState::TA_SPECULAR;
										else if (buffer == L"temp")							alphaarg1 = CnTextureState::TA_TEMP;
										else if (buffer == L"constant")						alphaarg1 = CnTextureState::TA_CONSTANT;
										else if (buffer == L"complement")					alphaarg1 = CnTextureState::TA_COMPLEMENT;
										else if (buffer == L"alphareplicate")				alphaarg1 = CnTextureState::TA_ALPHAREPLICATE;
										else												Assert( 0, L"[CnMaterial::LoadData] Failed 'Alpha arg1'");

										buffer = opnode.GetAttribute( L"arg2" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"selectmark")						alphaarg2 = CnTextureState::TA_SELECTMASK;
										else if (buffer == L"diffuse")						alphaarg2 = CnTextureState::TA_DIFFUSE;
										else if (buffer == L"current")						alphaarg2 = CnTextureState::TA_CURRENT;
										else if (buffer == L"texture")						alphaarg2 = CnTextureState::TA_TEXTURE;
										else if (buffer == L"tfactor")						alphaarg2 = CnTextureState::TA_TFACTOR;
										else if (buffer == L"specular")						alphaarg2 = CnTextureState::TA_SPECULAR;
										else if (buffer == L"temp")							alphaarg2 = CnTextureState::TA_TEMP;
										else if (buffer == L"constant")						alphaarg2 = CnTextureState::TA_CONSTANT;
										else if (buffer == L"complement")					alphaarg2 = CnTextureState::TA_COMPLEMENT;
										else if (buffer == L"alphareplicate")				alphaarg2 = CnTextureState::TA_ALPHAREPLICATE;
										else												Assert( 0, L"[CnMaterial::LoadData] Failed 'Alpha arg2'");

										stagestate.alpha.top = opmode;
										stagestate.alpha.arg1 = alphaarg1;
										stagestate.alpha.arg2 = alphaarg2;
									}
									else if (nodename == L"Sampler")
									{
										buffer = opnode.GetAttribute( L"type" );
										CnStringUtil::LowerCase( buffer );
										if (buffer == L"address")
										{
											CnTextureState::TextureAddress u, v, w;

											buffer = opnode.GetAttribute( L"u" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"wrap")				u = CnTextureState::TADDRESS_WRAP;
											else if (buffer == L"mirror")		u = CnTextureState::TADDRESS_MIRROR;
											else if (buffer == L"clamp")		u = CnTextureState::TADDRESS_CLAMP;
											else if (buffer == L"border")		u = CnTextureState::TADDRESS_BORDER;
											else if (buffer == L"mirroronce")	u = CnTextureState::TADDRESS_MIRRORONCE;
											else								Assert( 0, L"[CnMaterial::LoadData] Failed 'Address u'");
											
											buffer = opnode.GetAttribute( L"v" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"wrap")				v = CnTextureState::TADDRESS_WRAP;
											else if (buffer == L"mirror")		v = CnTextureState::TADDRESS_MIRROR;
											else if (buffer == L"clamp")		v = CnTextureState::TADDRESS_CLAMP;
											else if (buffer == L"border")		v = CnTextureState::TADDRESS_BORDER;
											else if (buffer == L"mirroronce")	v = CnTextureState::TADDRESS_MIRRORONCE;
											else								Assert( 0, L"[CnMaterial::LoadData] Failed 'Address v'");

											buffer = opnode.GetAttribute( L"w" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"wrap")				w = CnTextureState::TADDRESS_WRAP;
											else if (buffer == L"mirror")		w = CnTextureState::TADDRESS_MIRROR;
											else if (buffer == L"clamp")		w = CnTextureState::TADDRESS_CLAMP;
											else if (buffer == L"border")		w = CnTextureState::TADDRESS_BORDER;
											else if (buffer == L"mirroronce")	w = CnTextureState::TADDRESS_MIRRORONCE;
											else								Assert( 0, L"[CnMaterial::LoadData] Failed 'Address w'");

											stagestate.address.u = u;
											stagestate.address.v = v;
											stagestate.address.w = w;
										}
										else if (buffer == L"filter")
										{
											CnTextureState::TextureFilter mag, min, mip;

											buffer = opnode.GetAttribute( L"mag" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"none")					mag = CnTextureState::TEXF_NONE;
											else if (buffer == L"point")			mag = CnTextureState::TEXF_POINT;
											else if (buffer == L"linear")			mag = CnTextureState::TEXF_LINEAR;
											else if (buffer == L"anisotropic")		mag = CnTextureState::TEXF_ANISOTROPIC;
											else if (buffer == L"pyramidalquad")	mag = CnTextureState::TEXF_PYRAMIDALQUAD;
											else if (buffer == L"gaussianquad")		mag = CnTextureState::TEXF_GAUSSIANQUAD;
											else									Assert( 0, L"[CnMaterial::LoadData] Failed 'filter mag'");

											buffer = opnode.GetAttribute( L"min" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"none")					min = CnTextureState::TEXF_NONE;
											else if (buffer == L"point")			min = CnTextureState::TEXF_POINT;
											else if (buffer == L"linear")			min = CnTextureState::TEXF_LINEAR;
											else if (buffer == L"anisotropic")		min = CnTextureState::TEXF_ANISOTROPIC;
											else if (buffer == L"pyramidalquad")	min = CnTextureState::TEXF_PYRAMIDALQUAD;
											else if (buffer == L"gaussianquad")		min = CnTextureState::TEXF_GAUSSIANQUAD;
											else									Assert( 0, L"[CnMaterial::LoadData] Failed 'filter min'");

											buffer = opnode.GetAttribute( L"mip" );
											CnStringUtil::LowerCase( buffer );
											if (buffer == L"none")					mip = CnTextureState::TEXF_NONE;
											else if (buffer == L"point")			mip = CnTextureState::TEXF_POINT;
											else if (buffer == L"linear")			mip = CnTextureState::TEXF_LINEAR;
											else if (buffer == L"anisotropic")		mip = CnTextureState::TEXF_ANISOTROPIC;
											else if (buffer == L"pyramidalquad")	mip = CnTextureState::TEXF_PYRAMIDALQUAD;
											else if (buffer == L"gaussianquad")		mip = CnTextureState::TEXF_GAUSSIANQUAD;
											else									Assert( 0, L"[CnMaterial::LoadData] Failed 'filter mip'");

											stagestate.filter.mag = mag;
											stagestate.filter.min = min;
											stagestate.filter.mip = mip;
										}
										//else if (buffer == L"bordercolor")
										else if (buffer == L"lodbias")	
										{
											buffer = opnode.GetAttribute( L"value" );
											float bias = (float)_wtof( buffer.c_str() );
											stagestate.mipmaplodbias = bias;
										}
										else if (buffer == L"miplevel")		
										{
											buffer = opnode.GetAttribute( L"value" );
											int level = _wtoi( buffer.c_str() );
											stagestate.maxmipmaplevel = level;
										}
										//else if (buffer == L"maxanisotropy")	
										//else if (buffer == L"srgbtexture")		
										//else if (buffer == L"elementindex")		
										//else if (buffer == L"dmapoffset")		
										else									Assert( 0, L"[CnMaterial::LoadData] Failed 'Sampler type'");
									}
								}

								texturestate.SetStage( nNumStage, stagestate );
							}

							pPass->SetTextureState( texturestate );
						} // if
					} // for
				} // for
			} // if
		} // for
		return true;
	}

}