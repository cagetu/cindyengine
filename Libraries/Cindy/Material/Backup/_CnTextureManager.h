//================================================================
// File:           : CnTextureManager.h
// Original Author : changhee
// Creation Date   : 2007. 5. 21
//================================================================
#pragma once

#include "../Resource/CnResourceManager.h"

namespace Cindy
{
	//================================================================
	/** TextureManager 
	    @author    changhee
		@since     2007. 5. 21
		@remarks   텍스쳐 관리자 클래스
	*/
	//================================================================
	class CnTextureManager : public CnResourceManager,
							 public BaseCore::TBcSingleton<CnTextureManager>
	{
		DECLARE_RTTI;
		friend class BaseCore::TBcSingleton<CnTextureManager>;
	public:
		CnTextureManager();
		virtual ~CnTextureManager();

		RscPtr		Load( const wchar* strFileName ) override;
	};
} // end of namespace 