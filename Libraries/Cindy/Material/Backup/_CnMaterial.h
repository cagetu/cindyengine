//================================================================
// File:           : CnMaterial.h
// Original Author : changhee
// Creation Date   : 2007. 1. 30
//================================================================
#ifndef __CN_MATERIAL_H__
#define __CN_MATERIAL_H__

#include "../Resource/CnResource.h"

#include "CnShader.h"
#include "CnTechnique.h"

namespace Cindy
{
	//================================================================
	/** Material
	    @author    changhee
		@since     2007. 1. 30
		@remarks   재질
				   Shader에 설정할 파라미터들을 세팅한다.
	*/
	//================================================================
	class CN_DLL CnMaterial : public CnResource
	{
		DECLARE_RTTI;
		friend class CnMaterialGroup;

	public:
		typedef HashMap<CnString, CnTechnique*>		TechniqueList;
		typedef TechniqueList::iterator				TechniqueIter;
		typedef TechniqueList::const_iterator		TechniqueConstIter;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		// technique
		int						m_nNumTechniques;
		TechniqueList			m_Techniques;
		CnTechnique*			m_pCurrentTechnique;

		// shader
		ShaderPtr				m_spShader;

		// Load
		bool		LoadData( unsigned char* pbuffer, int size );

	protected:
		CnMaterial( CnResourceGroup* pParent );

	public:
		virtual ~CnMaterial();

		// Load/Unload
		bool					Load( const wchar* strFileName ) override;
		bool					Unload() override;

		// Device Lost/Restore
		void					OnLost() override;
		void					OnRestore() override;

		// shader
		bool					SetShader( const wchar* pszFileName );
		bool					SetShader( const ShaderPtr& pShader );
		ShaderPtr				GetShader() const;

		// technique
		CnTechnique*			AddTechnique( const CnString& strName );
		CnTechnique*			GetTechnique( const CnString& strName );
		const TechniqueList&	GetTechniques() const;
		void					RemoveAllTechniques();
		int						GetNumTechniques() const;

		void					SetCurrentTechnique( const CnString& strName );
		CnTechnique*			GetCurrentTechnique() const;

		// Transparent
		bool					IsTransparent();

		// 비교
		bool		operator== ( const CnMaterial* pMaterial) const;
	};

	//==================================================================
	/** MaterialPtr
		@author			cagetu
		@since			2007년 5월 21일
		@remarks		Material 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL MaterialPtr : public CnSmartPtr< CnMaterial >
	{
	public:
		MaterialPtr();
		explicit MaterialPtr( CnMaterial* pObject );
		MaterialPtr( const MaterialPtr& spObject );
		MaterialPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		MaterialPtr& operator = ( const RscPtr& spRsc );
	};
#include "CnMaterial.inl"
}

#endif	// __CN_MATERIAL_H__