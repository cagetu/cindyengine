//================================================================
// File:               : CnMaterial.inl
// Related Header File : CnMaterial.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 30
//================================================================

//----------------------------------------------------------------
//	Material Class
//----------------------------------------------------------------
inline
ShaderPtr CnMaterial::GetShader() const
{
	return m_spShader;
}

//----------------------------------------------------------------
/** Get Num Techniques
	@remarks	테크닉의 개수를 반환
*/
//----------------------------------------------------------------
inline
int CnMaterial::GetNumTechniques() const
{
	return m_nNumTechniques;
}

//----------------------------------------------------------------
//	MaterialPtr Class
//----------------------------------------------------------------
inline
MaterialPtr::MaterialPtr()
: CnSmartPtr< CnMaterial >()
{
}

//----------------------------------------------------------------
inline
MaterialPtr::MaterialPtr( CnMaterial* pObject )
: CnSmartPtr< CnMaterial >( pObject )
{
}

//----------------------------------------------------------------
inline
MaterialPtr::MaterialPtr( const MaterialPtr& spObject )
: CnSmartPtr< CnMaterial >( spObject )
{
}

//----------------------------------------------------------------
inline
MaterialPtr::MaterialPtr( const RscPtr& spRsc )
: CnSmartPtr< CnMaterial >()
{
	m_pObject = (CnMaterial*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->IncreaseReferences();
	}
}

//----------------------------------------------------------------
inline
MaterialPtr& MaterialPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnMaterial*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->DecreaseReferences();

		if( !spRsc.IsNull() )
			spRsc->IncreaseReferences();

		m_pObject = (CnMaterial*)spRsc.GetPtr();
	}

	return *this;
}