//================================================================
// File:               : CnTextureManager.cpp
// Related Header File : CnTextureManager.h
// Original Author     : changhee
// Creation Date       : 2007. 5. 21
//================================================================
#include "../Cindy.h"
#include "CnTextureManager.h"
#include "CnTexture.h"

#include "../RenderDx9/CnDx9Texture.h"
#include "../Render/CnRenderer.h"

namespace Cindy
{
	//================================================================
	IMPLEMENT_RTTI( Cindy, CnTextureManager, CnResourceManager );

	CnTextureManager::CnTextureManager()
	{
	}
	CnTextureManager::~CnTextureManager()
	{
	}

	//================================================================
	/** Load
		@remarks	텍스쳐 리소스를 읽는다.
	*/
	//================================================================
	RscPtr CnTextureManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = GetRsc( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnTexture* pTexture = NULL;
		switch ( CnRenderer::Instance().GetType() )
		{
		case CnRenderer::D3D9:
			pTexture = new CnDx9Texture( this );
			break;

		default:
			pTexture = new CnTexture( this );
			break;
		}

		if( pTexture->Load( strFileName ) )
		{
			AddRsc( pTexture );
		}
		else
		{
			SAFEDEL( pTexture );
		}
		return pTexture;
	}
}