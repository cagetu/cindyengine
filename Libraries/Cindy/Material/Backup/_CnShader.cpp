//================================================================
// File:               : CnShader.cpp
// Related Header File : CnShader.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnShader.h"

namespace Cindy
{
	//================================================================
	IMPLEMENT_RTTI( Cindy, CnShader, CnResource );

	//================================================================
	//	struct CnShader::Pass
	//================================================================
	CnShader::Pass::Pass()
	{
		memset( parameters, 0, sizeof(parameters) );
	}
	CnShader::Pass::~Pass()
	{
	}

	//================================================================
	//	struct CnShader::Technique
	//================================================================
	CnShader::Technique::Technique()
		: numpasses(0)
	{
	}
	CnShader::Technique::~Technique()
	{
		std::for_each( passes.begin(), passes.end(), DeleteObject() );
	}

	//================================================================
	//	class CnShader
	//================================================================
	/// Const/Dest
	CnShader::CnShader( CnResourceGroup* pParent )
		: CnResource( pParent )
	{
	}
	CnShader::~CnShader()
	{
		_RemoveAllTechniques();
	}

	//================================================================
	/** Remove All Techniques
		@remarks	모든 테크닉을 삭제한다.
	*/
	//================================================================
	void CnShader::_RemoveAllTechniques()
	{
		std::for_each( m_Techniques.begin(), m_Techniques.end(), DeleteObject() );
		m_Techniques.clear();
	}

	//================================================================
	/** Load
	    @remarks      로딩
		@param        strFileName : 파일 이름
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnShader::Load( const wchar* strFileName )
	{
		m_strName = strFileName;

		return true;
	}

	//================================================================
	/** Unload
	    @remarks      텍스쳐 해제
		@param        none
		@return       none
	*/
	//================================================================
	bool CnShader::Unload()
	{
		m_strName.clear();
		return true;
	}
	//================================================================
	/** OnLost
	    @remarks      윈도우 크기가 변경되는 등 디바이스가 리셋 되었을 때 처리
		@param		  none
		@return       none
	*/
	//================================================================
	void CnShader::OnLost()
	{
	}

	//================================================================
	/** OnRestore
	    @remarks      디바이스가 복원되었을 경우 처리
		@param		  none
		@return       none
	*/
	//================================================================
	void CnShader::OnRestore()
	{
	}
}