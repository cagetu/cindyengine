//================================================================
// File:           : CnMaterial.h
// Original Author : changhee
// Creation Date   : 2008. 1. 11
//================================================================
#ifndef __CN_MATERIAL_DATA_H__
#define __CN_MATERIAL_DATA_H__

#include "Resource/CnResource.h"
#include "CnMaterialState.h"
#include "CnRenderState.h"
#include "CnTextureState.h"
#include "CnTextureTypes.h"

namespace Cindy
{
	class CnMaterial;

	//================================================================
	/** Material Data
	    @author    changhee
		@since     2008. 1. 11
		@remarks   재질 파일 정보
				   한 단위 Mesh Data에 상응하는 Material Data File에 대한 정보
	*/
	//================================================================
	class CN_DLL CnMaterialData : public CnResource
	{
		__DeclareRtti;
	public:
		// 내부 매터리얼
		class MtlInfo
		{
			typedef HashMap<MapType::Define, CnString> TextureMap;
		public:
			//------------------------------------------------------
			//	Variables
			//------------------------------------------------------
			int				id;

			// shader
			CnString		shaderFile;
			CnString		shaderTargetTech;

			// textures
			TextureMap		textures;

			// state
			CnMaterialState	colorState;
			CnRenderState	renderState;
			CnTextureState	textureState;

			bool			enableRimLight;

			bool			enableSubSurface;
			CnColor			subSurfaceColor;
			float			subSurfaceRollOff;

			bool			enableMultiLights;

			// enable
			bool			enable;
			uint			type;	// 0: (effect), 1:(uber)

			//------------------------------------------------------
			//	Methods
			//------------------------------------------------------
			MtlInfo();

			/// add textures
			void		AddMap(MapType::Define type, const CnString& name);

			// instance
			CnMaterial*	CreateInstance();
		};

		typedef std::vector<MtlInfo*>	MaterialList;
		typedef MaterialList::iterator	MaterialIter;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		virtual ~CnMaterialData();

		// Load/Unload
		bool	Load( const wchar* strFileName ) override;
		bool	Unload() override;

		// Device Lost/Restore
		void	OnLost() override;
		void	OnRestore() override;

		// get mtl information
		MtlInfo* GetMtl( ushort Index );
		// get mtl list
		const MaterialList&	GetMtls() const;

	private:
		friend class CnMaterialDataManager;

		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		MaterialList	m_Materials;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnMaterialData( CnResourceManager* pParent );

		bool	_Load( unsigned char* pBuffer, int nBufferSize );
		void	_Unload();
	};

	//==================================================================
	/** MaterialPtr
		@author			cagetu
		@since			2007년 5월 21일
		@remarks		Material 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL MtlDataPtr : public Ptr<CnMaterialData>
	{
	public:
		MtlDataPtr();
		explicit MtlDataPtr( CnMaterialData* pObject );
		MtlDataPtr( const MtlDataPtr& spObject );
		MtlDataPtr( const RscPtr& spRsc );

		// operator = 
		MtlDataPtr& operator = ( const RscPtr& spRsc );
	};
#include "CnMaterialData.inl"
}

#endif	// __CN_MATERIAL_DATA_H__
