//================================================================
// File:           : CnTextureState.h
// Original Author : changhee
// Creation Date   : 2007. 4. 24
//================================================================
#ifndef __CN_TEXTURE_STATE_H__
#define __CN_TEXTURE_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** Texture State
	    @author    changhee
		@since     2007. 4. 24
		@remarks   텍스쳐 상태 설정
				   하나의 텍스쳐 스테이지에 대한 정보를 설정한다.
	*/
	//================================================================
	class CN_DLL CnTextureState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		enum TextureOp
		{
		   // Control
			TOP_DISABLE              = 1,		// disables stage
			TOP_SELECTARG1           = 2,		// the default
			TOP_SELECTARG2           = 3,

			// Modulate
			TOP_MODULATE             = 4,		// multiply args together
			TOP_MODULATE2X           = 5,		// multiply and  1 bit
			TOP_MODULATE4X           = 6,		// multiply and  2 bits

			// Add
			TOP_ADD                  =  7,		// add arguments together
			TOP_ADDSIGNED            =  8,		// add with -0.5 bias
			TOP_ADDSIGNED2X          =  9,		// as above but left  1 bit
			TOP_SUBTRACT             = 10,		// Arg1 - Arg2, with no saturation
			TOP_ADDSMOOTH            = 11,		// add 2 args, subtract product
												// Arg1 + Arg2 - Arg1*Arg2
												// = Arg1 + (1-Arg1)*Arg2

			// Linear alpha blend: Arg1*(Alpha) + Arg2*(1-Alpha)
			TOP_BLENDDIFFUSEALPHA    = 12,		// iterated alpha
			TOP_BLENDTEXTUREALPHA    = 13,		// texture alpha
			TOP_BLENDFACTORALPHA     = 14,		// alpha from D3DRS_TEXTUREFACTOR

			// Linear alpha blend with pre-multiplied arg1 input: Arg1 + Arg2*(1-Alpha)
			TOP_BLENDTEXTUREALPHAPM  = 15,		// texture alpha
			TOP_BLENDCURRENTALPHA    = 16,		// by alpha of current color

			// Specular mapping
			TOP_PREMODULATE            = 17,    // modulate with next texture before use
			TOP_MODULATEALPHA_ADDCOLOR = 18,    // Arg1.RGB + Arg1.A*Arg2.RGB
													// COLOROP only
			TOP_MODULATECOLOR_ADDALPHA = 19,    // Arg1.RGB*Arg2.RGB + Arg1.A
													// COLOROP only
			TOP_MODULATEINVALPHA_ADDCOLOR = 20, // (1-Arg1.A)*Arg2.RGB + Arg1.RGB
													// COLOROP only
			TOP_MODULATEINVCOLOR_ADDALPHA = 21, // (1-Arg1.RGB)*Arg2.RGB + Arg1.A
												// COLOROP only

			// Bump mapping
			TOP_BUMPENVMAP           = 22,		// per pixel env map perturbation
			TOP_BUMPENVMAPLUMINANCE  = 23,		// with luminance channel

			// This can do either diffuse or specular bump mapping with correct input.
			// Performs the function (Arg1.R*Arg2.R + Arg1.G*Arg2.G + Arg1.B*Arg2.B)
			// where each component has been scaled and offset to make it signed.
			// The result is replicated into all four (including alpha) channels.
			// This is a valid COLOROP only.
			TOP_DOTPRODUCT3          = 24,

			// Triadic ops
			TOP_MULTIPLYADD          = 25,		// Arg0 + Arg1*Arg2
			TOP_LERP                 = 26,		// (Arg0)*Arg1 + (1-Arg0)*Arg2
		};

		enum TextureAlpha
		{
			TA_SELECTMASK      = 0x0000000f,  // mask for arg selector
			TA_DIFFUSE         = 0x00000000,  // select diffuse color (read only)
			TA_CURRENT         = 0x00000001,  // select stage destination register (read/write)
			TA_TEXTURE         = 0x00000002,  // select texture color (read only)
			TA_TFACTOR         = 0x00000003,  // select D3DRS_TEXTUREFACTOR (read only)
			TA_SPECULAR        = 0x00000004,  // select specular color (read only)
			TA_TEMP            = 0x00000005,  // select temporary register color (read/write)
			TA_CONSTANT        = 0x00000006,  // select texture stage constant
			TA_COMPLEMENT      = 0x00000010,  // take 1.0 - x (read modifier)
			TA_ALPHAREPLICATE  = 0x00000020,  // replicate alpha to color components (read modifier)
		};

		struct StageOp
		{
			TextureOp		top;
			TextureAlpha	arg1;
			TextureAlpha	arg2;
		};

		// Sampler State Type

		typedef int TextureAddress;
		const static int	TADDRESS_WRAP		= 1;
		const static int	TADDRESS_MIRROR		= 2;
		const static int	TADDRESS_CLAMP		= 3;
		const static int	TADDRESS_BORDER		= 4;
		const static int	TADDRESS_MIRRORONCE = 5;

		typedef int TextureFilter;
		const static int	TEXF_NONE			= 0;
		const static int	TEXF_POINT			= 1;
		const static int	TEXF_LINEAR			= 2;
		const static int	TEXF_ANISOTROPIC	= 3;
		const static int	TEXF_PYRAMIDALQUAD	= 6;
		const static int	TEXF_GAUSSIANQUAD	= 7;

		struct StageAddress
		{
			TextureAddress		u;
			TextureAddress		v;
			TextureAddress		w;
		};

		struct StageFilter
		{
			TextureFilter		mag;
			TextureFilter		min;
			TextureFilter		mip;
		};

		struct CN_DLL StageState
		{
			StageOp			color;
			StageOp			alpha;
			StageAddress	address;
			StageFilter		filter;
			float			mipmaplodbias;
			int				maxmipmaplevel;

			StageState();
		};

		typedef int		Stage;

		typedef std::map<Stage, StageState>		StageStateList;
		typedef StageStateList::iterator		StageStateIter;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnTextureState();
		virtual ~CnTextureState();

		TYPE	GetStateType() const	{	return TEXTURE;		}

		void	SetStage( Stage nStage, const StageState& rState );
		bool	GetStage( Stage nStage, StageState& rResult );
		const StageStateList&		GetStages() const;

	private:
		StageStateList			m_StageStates;
	};

#include "CnTextureState.inl"
}

#endif	// __CN_TEXTURE_STATE_H__