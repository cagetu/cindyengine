//================================================================
// File:               : CnRenderState.inl
// Related Header File : CnRenderState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
//----------------------------------------------------------------
inline
void CnRenderState::AddFlags( ushort Flags )
{
	m_StateFlags |= Flags;
}
inline
void CnRenderState::RemoveFlags( ushort Flags )
{
	m_StateFlags &= Flags;
}
inline
void CnRenderState::ResetFlags()
{
	m_StateFlags = 0;
}
inline
void CnRenderState::SetFlags( ushort Flags )
{
	m_StateFlags = Flags;
}
inline
ushort CnRenderState::GetFlags() const
{
	return m_StateFlags;
}
inline
bool CnRenderState::IsFlags( ushort Flags ) const
{
	return (m_StateFlags & Flags) == Flags;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetCullMode( CnRenderState::CullMode eMode )
{
	m_eCullMode = eMode;
}
inline
CnRenderState::CullMode CnRenderState::GetCullMode() const
{
	return m_eCullMode;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetFillMode( CnRenderState::FillMode eMode )
{
	m_eFillMode = eMode;
}
inline
CnRenderState::FillMode CnRenderState::GetFillMode() const
{
	return m_eFillMode;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetZBufferEnable( int nEnable )
{
	m_nEnableZBuffer = nEnable;
}
inline
int CnRenderState::GetZBufferEnable() const
{
	return m_nEnableZBuffer;
}
//----------------------------------------------------------------
inline
void CnRenderState::SetZBufferMode( CnRenderState::ZBufferMode eMode )
{
	m_eZBuffer = eMode;
}
inline
CnRenderState::ZBufferMode CnRenderState::GetZBufferMode() const
{
	return m_eZBuffer;
}
//----------------------------------------------------------------
inline
void CnRenderState::SetZBufferFunc( CnRenderState::ZFunc eFunc )
{
	m_eZFunc = eFunc;
}
inline
CnRenderState::ZFunc CnRenderState::GetZBufferFunc() const
{
	return m_eZFunc;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetAlphaBlendEnable( bool bEnable )
{
	m_bAlphaBlendEnable = bEnable;
}
inline
bool CnRenderState::GetAlphaBlendEnable() const
{
	return m_bAlphaBlendEnable;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetAlphaSrcBlend( BlendMode eMode )
{
	m_eAlphaSrcBlend = eMode;
}
inline
CnRenderState::BlendMode CnRenderState::GetAlphaSrcBlend() const
{
	return m_eAlphaSrcBlend;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetAlphaDstBlend( BlendMode eMode )
{
	m_eAlphaDstBlend = eMode;
}
inline
CnRenderState::BlendMode CnRenderState::GetAlphaDstBlend() const
{
	return m_eAlphaDstBlend;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetTFactor( ulong ulFactor )
{
	m_ulTFactor = ulFactor;
}
inline
ulong CnRenderState::GetTFactor() const
{
	return m_ulTFactor;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetAlphaBlendOp( CnRenderState::BlendOp eMode )
{
	m_eAlphaBlendOp = eMode;
}
inline
CnRenderState::BlendOp CnRenderState::GetAlphaBlendOp() const
{
	return m_eAlphaBlendOp;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetAlphaTestEnable( bool bEnable )
{
	m_bAlphaTestEnable = bEnable;
}
inline
bool CnRenderState::GetAlphaTestEnable() const
{
	return m_bAlphaTestEnable;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetAlphaFunc( CnRenderState::ZFunc eFunc )
{
	m_eAlphaFunc = eFunc;
}
inline
CnRenderState::ZFunc CnRenderState::GetAlphaFunc() const
{
	return m_eAlphaFunc;
}

//----------------------------------------------------------------
/** Alpha Ref
	@remarks		알파 테스트를 사용할 경우, 빠질 알파 범위
					0x00000000 ~ 0x000000ff 까지 허용
*/
//----------------------------------------------------------------
inline
void CnRenderState::SetAlphaRef( ulong ulRef )
{
	m_ulAlphaRef = ulRef;
}
inline
ulong CnRenderState::GetAlphaRef() const
{
	return m_ulAlphaRef;
}

//----------------------------------------------------------------
/** Enable Light
	@remarks	라이트 사용여부 설정
	@param		nEnable : 0(false), 1(true)
*/
//----------------------------------------------------------------
inline
void CnRenderState::SetLightEnable( int nEnable )
{
	m_nEnableLight = nEnable;
}
inline
int CnRenderState::GetLightEnable() const
{
	return m_nEnableLight;
}

//----------------------------------------------------------------
/** Enable Fog
	@remarks	Fog 사용여부 설정
	@param		nEnable : 0(false), 1(true)
*/
//----------------------------------------------------------------
inline
void CnRenderState::SetFogEnable( int nEnable )
{
	m_nEnableFog = nEnable;
}
inline
int CnRenderState::GetFogEnable() const
{
	return m_nEnableFog;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetScissorTestEnable( bool bEnable )
{
	m_bScissorTestEnable = bEnable;
}
inline
bool CnRenderState::GetScissorTestEnable() const
{
	return m_bScissorTestEnable;
}

//----------------------------------------------------------------
inline
void CnRenderState::SetScissorTestRect( const RECT& rect )
{
	m_ScissorRect = rect;
}
inline
const RECT& CnRenderState::GetScissorTestRect() const
{
	return m_ScissorRect;
}