//================================================================
// File:           : CnMaterialManager.h
// Original Author : changhee
// Creation Date   : 2007. 5. 21
//================================================================
#pragma once

#include "../Resource/CnResourceManager.h"

namespace Cindy
{
	//================================================================
	/** Material Manager
	    @author    changhee
		@since     2007. 5. 21
		@remarks   재질 관리자
	*/
	//================================================================
	class CN_DLL CnMaterialManager : public CnResourceManager,
									 public BaseCore::TBcSingleton<CnMaterialManager>
	{
		DECLARE_RTTI;
		friend class BaseCore::TBcSingleton<CnMaterialManager>;
	public:
		CnMaterialManager();
		virtual ~CnMaterialManager();

		RscPtr		Load( const wchar* strFileName ) override;
		RscPtr		New( const CnString& strFileName );
	};
}
