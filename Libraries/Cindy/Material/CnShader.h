//================================================================
// File:           : CnShader.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_SHADER_H__
#define __CN_SHADER_H__

#include "CnShaderEffect.h"
#include "Resource/CnResource.h"

namespace Cindy
{
	//================================================================
	/** Shader 
	    @author    changhee
		@since     2007. 2. 21
		@remarks   쉐이더 클래스
	*/
	//================================================================
	class CN_DLL CnShader : public CnResource
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual ~CnShader();

		// Load/Unload
		bool			Load( const wchar* strFileName ) override;
		bool			Unload() override;

		// Device Lost/Restore
		virtual void	OnLost() override;
		virtual void	OnRestore() override;

		// 적용 
		virtual void	Apply( ShaderFeature::Bits Mask );

		// ShaderEffect
		virtual CnShaderEffect* GetEffect( ShaderFeature::Bits Mask = -1 ) const;

		// Technique
		virtual void	SetTechnique( ShaderHandle hTechnique );
		ushort			GetNumTechniques() const;
		CnTechnique*	GetTechnique( const CnString& strName ) const;
		CnTechnique*	GetTechnique( CnShaderFeature::Mask Mask ) const;
		CnTechnique*	GetDefaultTechnique() const;

		// Parameters
		void			SetParemter( ShaderState::Param eParam, ShaderHandle hHandle );
		void			SetParameters( void* pParameters );
		ShaderHandle	GetParameter( ShaderState::Param eParam ) const;
		bool			HasParameter( ShaderState::Param eParam ) const;
		void			ClearParameters();

		// Shader Begin/End
		virtual int		Begin( bool bSaveState = true );
		virtual void	End();

		// Device Lost/Reset
		virtual void	Clear();

		// Pass
		virtual void	BeginPass( uint nPass );
		virtual void	EndPass();
		virtual void	CommitChanges();

		// Set 'Boolean'
		virtual void	SetBool( ShaderHandle hHandle, bool val );
		// Set 'Int'
		virtual void	SetInt( ShaderHandle hHandle, int val );
		// Set 'Float'
		virtual void	SetFloat( ShaderHandle hHandle, float val );
		//virtual void	SetFloat4( ShaderHandle hHandle, float* val );
		virtual void	SetFloatArray( ShaderHandle hHandle, float* aFloats, int nSize );
		// Set 'Vector'
		virtual void	SetVector3( ShaderHandle hHandle, const Math::Vector3& val );
		virtual void	SetVector4( ShaderHandle hHandle, const Math::Vector4& val );
		virtual void	SetVector3Array( ShaderHandle hHandle, const Math::Vector3* aVectors, int nSize );
		virtual void	SetVector4Array( ShaderHandle hHandle, const Math::Vector4* aVectors, int nSize );
		// Set 'Matrix'
		virtual void	SetMatrix( ShaderHandle hHandle, const Math::Matrix44& val );
		virtual void	SetMatrixArray( ShaderHandle hHandle, Math::Matrix44* aMatrices, int nSize );
		// Set 'Texture'
		virtual void	SetTexture( ShaderHandle hHandle, CnTexture* texture );
		// Set 'Value'
		virtual void	SetValue( ShaderHandle hHandle, void* pData, unsigned int nBytes );
		// Set 'Color'
		virtual void	SetColor( ShaderHandle hHandle, const CnColor& color );

	protected:
		friend class CnShaderManager;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnShaderEffect*	m_pShaderEffect;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnShader( CnResourceManager* pParent );
	};

	//==================================================================
	/** ShaderPtr
		@author			cagetu
		@since			2007년 7월 18일
		@remarks		Shader 파일용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL ShaderPtr : public Ptr<CnShader>
	{
	public:
		ShaderPtr();
		explicit ShaderPtr( CnShader* pObject );
		ShaderPtr( const ShaderPtr& spObject );
		ShaderPtr( const RscPtr& spRsc );

		// operator = 
		ShaderPtr& operator= ( const RscPtr& spRsc );
	};

#include "CnShader.inl"
}

#endif	// __CN_SHADER_H__