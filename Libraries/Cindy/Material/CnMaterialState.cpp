//================================================================
// File:               : CnMaterialState.cpp
// Related Header File : CnMaterialState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnMaterialState.h"

namespace Cindy
{
	//__ImplementClass(Cindy, CnMaterialState, CnGlobalState);
	//----------------------------------------------------------------
	CnMaterialState::CnMaterialState()
		: m_fSpecularIntensity(1.0f)
		, m_fEmissiveIntensity(0.2f)
		, m_fGlowIntensity(4.0f)
		, m_fShininess(32.0f)
	{
	}

	/// Destructor
	CnMaterialState::~CnMaterialState()
	{
	}
}