//================================================================
// File:               : CnMaterialDataParser.cpp
// Related Header File : CnMaterialData.h
// Original Author     : changhee
// Creation Date       : 2008. 1. 1
//================================================================
#include "../Cindy.h"
#include "../Util/CnString.h"
#include "CnMaterialData.h"

#include <MoCommon/External/minixml.h>

namespace Cindy
{
	//================================================================
	/** @brief	데이터를 읽어 들이는 내부 함수
	*/
	//================================================================
	bool CnMaterialData::_Load( unsigned char* pBuffer, int nBufferSize )
	{
		CMiniXML doc;
		if (!doc.Parse( pBuffer, nBufferSize ))
			return false;

		// "Material" - Root
		CMiniXML::iterator xmlRoot = doc.GetRoot();
		if (xmlRoot == NULL)
			return false;

		CnString name;
		CnString buffer;
		int value;
		float red, green, blue, alpha;

		CMiniXML::iterator xmlMtl = xmlRoot.GetChild();
		for (xmlMtl=xmlMtl.Find(_T("Material")); xmlMtl != NULL; xmlMtl=xmlMtl.Find(_T("Material")))
		{
			value = _wtoi(xmlMtl.GetAttribute(_T("id")));
			name = xmlMtl.GetAttribute(_T("name"));
			const wchar* enable = xmlMtl.GetAttribute(_T("enable"));

			bool active = true;
			if (enable != 0)
			{
				buffer = enable;
				if (buffer == L"false")
				{
					active = false;
				}
			}

			MtlInfo* newMtl = new MtlInfo();
			m_Materials.push_back( newMtl );

			// material id
			newMtl->id = value;
			newMtl->enable = active;

			CMiniXML::iterator xmlState = xmlMtl.GetChild();
			for (; xmlState != NULL; xmlState=xmlState.Next())
			{
				name = xmlState.GetName();

				if (name == _T("Shader"))
				{
					newMtl->shaderFile = xmlState.GetAttribute(_T("name"));
					newMtl->shaderTargetTech = xmlState.GetAttribute(_T("technique"));
					newMtl->type = 0;

					const wchar* type = xmlState.GetAttribute(_T("type"));
					if (type != 0)
					{
						buffer = type;
						if (buffer == _T("uber"))
						{
							newMtl->type = 1;
						}
					}
					continue;
				} // if
				else if (name == _T("ShaderFeature"))
				{
					CMiniXML::iterator item = xmlState.GetChild();
					for (; item != NULL; item=item.Next())
					{
						const wchar* itemname = item.GetName();

						if (unicode::Compare(itemname, _T("RimLighting")))
						{
							newMtl->enableRimLight = true;
						}
						else if (unicode::Compare(itemname, _T("MultiLights")))
						{
							newMtl->enableMultiLights = true;
						}
						else if (unicode::Compare(itemname, _T("SubSurface")))
						{
							newMtl->enableSubSurface = true;
					
							CMiniXML::iterator xmlChild = item.GetChild();
							for (; xmlChild != NULL; xmlChild=xmlChild.Next())
							{
								 name = xmlChild.GetName();
								 unicode::LowerCase(name);

								 if (name == _T("color"))
								 {
									value = _wtoi( xmlChild.GetAttribute(_T("r")) );
									red = (float)value / 255.0f;

									value = _wtoi( xmlChild.GetAttribute( L"g") );
									green = (float)value / 255.0f;

									value = _wtoi( xmlChild.GetAttribute( L"b") );
									blue = (float)value / 255.0f;

									value = _wtoi( xmlChild.GetAttribute( L"a") );
									alpha = (float)value / 255.0f;

									newMtl->subSurfaceColor = CnColor(red, green, blue, alpha);
									continue;
								 }
								 else if (name == _T("rolloff"))
								 {
									float rollOff = (float)_wtof(xmlChild.GetAttribute(_T("f")));
									newMtl->subSurfaceRollOff = rollOff;
									continue;
								 }
							}
						}
					}
				}
				else if (name == _T("Texture"))
				{
					CnString fileName;

					CMiniXML::iterator xmlTexture = xmlState.GetChild();
					for (; xmlTexture != NULL; xmlTexture=xmlTexture.Next())
					{
						 name = xmlTexture.GetName();
						 unicode::LowerCase(name);

						 if (name == _T("diffusemap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::DiffuseMap0, fileName);
							 continue;
						 }
						 else if (name == _T("specularmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::SpecularMap, fileName);
							 continue;
						 }
						 else if (name == _T("specularexpmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::SpecularExpMap, fileName);
							 continue;
						 }
						 else if (name == _T("normalmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::NormalMap, fileName);
							 continue;
						 }
						 else if (name == _T("glowmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::GlowMap, fileName);
							 continue;
						 }
						 else if (name == _T("emissivemap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::EmissiveMap, fileName);
							 continue;
						 }
						 else if (name == _T("celshademap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::CelShadeMap, fileName);
							 continue;
						 }
						 else if (name == _T("occulsionmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::OcclusionMap, fileName);
							 continue;
						 }
						 else if (name == _T("cubemap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::CubeMap0, fileName);
							 continue;
						 }
						 else if (name == _T("alphamap"))
						 {
 							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::AlphaMap, fileName);
							 continue;
						 }
						 else if (name == _T("reflectmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::ReflectMap, fileName);
							 continue;
						 }
						 else if (name == _T("randommap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::RandomMap, fileName);
							 continue;
						 }
						 else if (name == _T("hairshiftmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::HairShiftMap, fileName);
							 continue;
						 }
						 else if (name == _T("hairspecmaskmap"))
						 {
							 fileName = xmlTexture.GetAttribute(_T("name"));
							 newMtl->AddMap(MapType::HairSpecMaskMap, fileName);
							 continue;
						 }
					}
					continue;
				} // if
				else if (name == _T("ColorState"))
				{
					CnMaterialState* colorState = &newMtl->colorState;

					CMiniXML::iterator xmlColor = xmlState.GetChild();
					for (; xmlColor != NULL; xmlColor = xmlColor.Next())
					{
						name = xmlColor.GetName();
						if (name == L"Ambient")
						{
							value = _wtoi( xmlColor.GetAttribute(_T("r")) );
							red = (float)value / 255.0f;
							
							value = _wtoi( xmlColor.GetAttribute( L"g") );
							green = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"b") );
							blue = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"a") );
							alpha = (float)value / 255.0f;

							colorState->SetAmbient( CnColor( red, green, blue, alpha ) );
							continue;
						}
						else if (name == L"Diffuse")
						{
							value = _wtoi( xmlColor.GetAttribute( L"r") );
							red = (float)value / 255.0f;
							
							value = _wtoi( xmlColor.GetAttribute( L"g") );
							green = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"b") );
							blue = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"a") );
							alpha = (float)value / 255.0f;

							colorState->SetDiffuse( CnColor( red, green, blue, alpha ) );
							continue;
						}
						else if (name == L"Specular")
						{
							value = _wtoi( xmlColor.GetAttribute( L"r") );
							red = (float)value / 255.0f;
							
							value = _wtoi( xmlColor.GetAttribute( L"g") );
							green = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"b") );
							blue = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"a") );
							alpha = (float)value / 255.0f;

							colorState->SetSpecular( CnColor( red, green, blue, alpha ) );
							continue;
						}
						else if (name == L"Emissive")
						{
							value = _wtoi( xmlColor.GetAttribute( L"r") );
							red = (float)value / 255.0f;
							
							value = _wtoi( xmlColor.GetAttribute( L"g") );
							green = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"b") );
							blue = (float)value / 255.0f;

							value = _wtoi( xmlColor.GetAttribute( L"a") );
							alpha = (float)value / 255.0f;

							colorState->SetEmissive( CnColor( red, green, blue, alpha ) );
							continue;
						}
						else if (name == L"Shininess")
						{
							value = _wtoi( xmlColor.GetAttribute( L"value") );

							colorState->SetShininess( (float)value );
							continue;
						}
					}
					continue;
				} // if
				//else if (name == _T("TextureState"))
				//{
				//	CnTextureState* textureState = &newMtl->textureState;
				//	CMiniXML::iterator xmlTS = xmlState.GetChild();
				//	for (; xmlTS != NULL; xmlTS=xmlTS.Next())
				//	{
				//		name = xmlTS.GetName();
				//		unicode::LowerCase(name);

				//		if (name == L"minmaplodbias")
				//		{
				//			buffer = xmlTS.GetAttribute(L"bias");
				//			float bias = unicode::ToFloat(buffer);
				//			CnTextureState::StageState state;
				//			state.mipmaplodbias = bias;
				//			textureState->SetStage(0, state);
				//		} // if
				//	} // for 
				//}
				else if (name == _T("RenderState"))
				{
					CnRenderState* renderState = &newMtl->renderState;

					CMiniXML::iterator xmlRS = xmlState.GetChild();
					for (; xmlRS != NULL; xmlRS=xmlRS.Next())
					{
						name = xmlRS.GetName();

						if (name == L"CullMode")
						{
							CnRenderState::CullMode cullmode;
							ushort flags = 0;

							buffer = xmlRS.GetAttribute( L"mode" );
							unicode::LowerCase( buffer );

							if (buffer == L"ccw") {
								cullmode = CnRenderState::CULL_CCW;
								flags = CnRenderState::_CULL_CCW;
							}
							else if(buffer == L"cw"){
								cullmode = CnRenderState::CULL_CW;
							}
							else if(buffer == L"none"){
								cullmode = CnRenderState::CULL_NONE;
								flags = CnRenderState::_CULL_NONE;
							}

							renderState->SetCullMode( cullmode );
							renderState->AddFlags( flags );
							continue;
						}
						else if (name == L"FillMode")
						{
							CnRenderState::FillMode fillmode;
							ushort flags = 0;

							buffer = xmlRS.GetAttribute( L"mode" );
							unicode::LowerCase( buffer );

							if (buffer == L"point")					fillmode = CnRenderState::FILL_POINT;	
							else if(buffer == L"wireframe") {
								fillmode = CnRenderState::FILL_WIREFRAME;
								flags = CnRenderState::_FILL_WIRE;
							}
							else if(buffer == L"solid")	{
								fillmode = CnRenderState::FILL_SOLID;
								flags = CnRenderState::_FILL_SOLID;
							}

							renderState->SetFillMode( fillmode );
							renderState->AddFlags( flags );
							continue;
						}
						else if (name == L"LightEnable")
						{
							int nEnable = 0;

							buffer = xmlRS.GetAttribute( L"enable" );
							if (buffer == L"true")					nEnable = 1;
							else									nEnable = 0;

							renderState->SetLightEnable( nEnable );
							continue;
						}
						else if (name == L"AlphaBlend")
						{
							buffer = xmlRS.GetAttribute( L"enable" );
							unicode::LowerCase( buffer );
							if (buffer == L"true")
							{
								// AlphaBlendEnable
								renderState->SetAlphaBlendEnable( true );
								renderState->AddFlags( CnRenderState::_ALPHABLEND );

								// SrcBlend
								const wchar* srcAttr = xmlRS.GetAttribute( L"src" );
								if (srcAttr)
								{
									CnRenderState::BlendMode srcmode;

									buffer = srcAttr;
									unicode::LowerCase( buffer );
									if (buffer == L"zero")					srcmode = CnRenderState::BLEND_ZERO;
									else if (buffer == L"one")				srcmode = CnRenderState::BLEND_ONE;
									else if (buffer == L"srccolor")			srcmode = CnRenderState::BLEND_SRCCOLOR;
									else if (buffer == L"invsrccolor")		srcmode = CnRenderState::BLEND_INVSRCCOLOR;
									else if (buffer == L"srcalpha")			srcmode = CnRenderState::BLEND_SRCALPHA;
									else if (buffer == L"invsrcalpha")		srcmode = CnRenderState::BLEND_INVSRCALPHA;
									else if (buffer == L"destalpha")		srcmode = CnRenderState::BLEND_DESTALPHA;
									else if (buffer == L"invdestalpha")		srcmode = CnRenderState::BLEND_INVDESTALPHA;
									else if (buffer == L"destcolor")		srcmode = CnRenderState::BLEND_DESTCOLOR;
									else if (buffer == L"invdestcolor")		srcmode = CnRenderState::BLEND_INVDESTCOLOR;
									else if (buffer == L"srcalphasat")		srcmode = CnRenderState::BLEND_SRCALPHASAT;
									else if (buffer == L"bothsrcalpha")		srcmode = CnRenderState::BLEND_BOTHSRCALPHA;
									else if (buffer == L"bothinvsrcalpha")	srcmode = CnRenderState::BLEND_BOTHINVSRCALPHA;
									else if (buffer == L"blendfactor")		srcmode = CnRenderState::BLEND_BLENDFACTOR;
									else if (buffer == L"invblendfactor")	srcmode = CnRenderState::BLEND_INVBLENDFACTOR;
									else									Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState SrcBlend'");

									renderState->SetAlphaSrcBlend( srcmode );
								}

								// DestBlend
								const wchar* destAttr = xmlRS.GetAttribute( L"dest" );
								if (destAttr)
								{
									CnRenderState::BlendMode destmode;

									buffer = destAttr;
									unicode::LowerCase( buffer );
									if (buffer == L"zero")					destmode = CnRenderState::BLEND_ZERO;
									else if (buffer == L"one")				destmode = CnRenderState::BLEND_ONE;
									else if (buffer == L"srccolor")			destmode = CnRenderState::BLEND_SRCCOLOR;
									else if (buffer == L"invsrccolor")		destmode = CnRenderState::BLEND_INVSRCCOLOR;
									else if (buffer == L"srcalpha")			destmode = CnRenderState::BLEND_SRCALPHA;
									else if (buffer == L"invsrcalpha")		destmode = CnRenderState::BLEND_INVSRCALPHA;
									else if (buffer == L"destalpha")		destmode = CnRenderState::BLEND_DESTALPHA;
									else if (buffer == L"invdestalpha")		destmode = CnRenderState::BLEND_INVDESTALPHA;
									else if (buffer == L"destcolor")		destmode = CnRenderState::BLEND_DESTCOLOR;
									else if (buffer == L"invdestcolor")		destmode = CnRenderState::BLEND_INVDESTCOLOR;
									else if (buffer == L"srcalphasat")		destmode = CnRenderState::BLEND_SRCALPHASAT;
									else if (buffer == L"bothsrcalpha")		destmode = CnRenderState::BLEND_BOTHSRCALPHA;
									else if (buffer == L"bothinvsrcalpha")	destmode = CnRenderState::BLEND_BOTHINVSRCALPHA;
									else if (buffer == L"blendfactor")		destmode = CnRenderState::BLEND_BLENDFACTOR;
									else if (buffer == L"invblendfactor")	destmode = CnRenderState::BLEND_INVBLENDFACTOR;
									else									Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState DestBlend'");

									renderState->SetAlphaDstBlend( destmode );
								}
							}
							else
							{
								renderState->SetAlphaBlendEnable( false );
								renderState->AddFlags( CnRenderState::_NO_ALPHABLEND );
							}
							continue;
						}
						else if (name == L"AlphaTest")
						{
							buffer = xmlRS.GetAttribute( L"enable" );
							unicode::LowerCase( buffer );
							if (buffer == L"true")
							{
								// AlphaTestEnable
								renderState->SetAlphaTestEnable( true );
								renderState->AddFlags( CnRenderState::_ALPHATEST );

								// AlphaRef
								buffer = xmlRS.GetAttribute( L"ref" );
								long ref = _wtol( buffer.c_str() );
								renderState->SetAlphaRef( (ulong)ref );

								// AlpaFunc
								CnRenderState::ZFunc alphafunc;
								{
									buffer = xmlRS.GetAttribute( L"mode" );
									unicode::LowerCase( buffer );
									if (buffer == L"less")				alphafunc = CnRenderState::FUNC_LESS;
									else if(buffer == L"equal")			alphafunc = CnRenderState::FUNC_EQUAL;
									else if(buffer == L"lessequal")		alphafunc = CnRenderState::FUNC_LESSEQUAL;
									else if(buffer == L"greater")		alphafunc = CnRenderState::FUNC_GREATER;
									else if(buffer == L"notequal")		alphafunc = CnRenderState::FUNC_NOTEQUAL;
									else if(buffer == L"greaterequal")	alphafunc = CnRenderState::FUNC_GREATEREQUAL;
									else if(buffer == L"always")		alphafunc = CnRenderState::FUNC_ALWAYS;
									else if(buffer == L"never")			alphafunc = CnRenderState::FUNC_NEVER;
									else								Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState AlphaFunc'");
								}
								renderState->SetAlphaFunc( alphafunc );
							}
							else
							{
								renderState->SetAlphaTestEnable( false );
								renderState->AddFlags( CnRenderState::_NO_ALPHATEST );
							} // if
						}
						else if (name == L"ZBuffer")
						{
							// ZBufferEnable
							buffer = xmlRS.GetAttribute( L"enable" );
							unicode::LowerCase( buffer );
							if (buffer == L"true") {
								renderState->SetZBufferEnable( 1 );
								renderState->AddFlags( CnRenderState::_Z_ENABLE );
							}
							else {
								renderState->SetZBufferEnable( 0 );
								renderState->AddFlags( CnRenderState::_Z_DISABLE );
							}

							// ZWriteEnable
							CnRenderState::ZBufferMode zbuffermode;
							{
								buffer = xmlRS.GetAttribute( L"write" );
								unicode::LowerCase( buffer );
								if (buffer == L"true") {
									zbuffermode = CnRenderState::ZB_USEWBUFFER;
									renderState->AddFlags( CnRenderState::_ZWRITE_ENABLE );
								}
								else {
									zbuffermode = CnRenderState::ZB_DISABLE;
									renderState->AddFlags( CnRenderState::_ZWRITE_DISABLE );
								}

								buffer = xmlRS.GetAttribute( L"mode" );
							}
							renderState->SetZBufferMode( zbuffermode );

							// ZFunc
							CnRenderState::ZFunc zfunc;
							{
								buffer = xmlRS.GetAttribute( L"mode" );
								unicode::LowerCase( buffer );
								if (buffer == L"less")					zfunc = CnRenderState::FUNC_LESS;
								else if(buffer == L"equal")				zfunc = CnRenderState::FUNC_EQUAL;
								else if(buffer == L"lessequal")			zfunc = CnRenderState::FUNC_LESSEQUAL;
								else if(buffer == L"greater")			zfunc = CnRenderState::FUNC_GREATER;
								else if(buffer == L"notequal")			zfunc = CnRenderState::FUNC_NOTEQUAL;
								else if(buffer == L"greaterequal")		zfunc = CnRenderState::FUNC_GREATEREQUAL;
								else if(buffer == L"always")			zfunc = CnRenderState::FUNC_ALWAYS;
								else if(buffer == L"never")				zfunc = CnRenderState::FUNC_NEVER;
								else									Assert( 0, L"[CnMaterial::LoadData] Failed 'RenderState ZFunc'");
							}
							renderState->SetZBufferFunc( zfunc );
						}
						else if (name == L"TFactor")
						{
							value = _wtoi( xmlRS.GetAttribute( L"r") );
							red = (float)value / 255.0f;

							value = _wtoi( xmlRS.GetAttribute( L"g") );
							green = (float)value / 255.0f;

							value = _wtoi( xmlRS.GetAttribute( L"b") );
							blue = (float)value / 255.0f;

							value = _wtoi( xmlRS.GetAttribute( L"a") );
							alpha = (float)value / 255.0f;

							CnColor color( red, green, blue, alpha );
							renderState->SetTFactor( color.GetRGBA() );
						}
					}
					continue;
				} // if
			} // for

		} // for

		return true;
	}
}