//================================================================
// File:               : CnTechnique.inl
// Related Header File : CnTechnique.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 14
//================================================================
//----------------------------------------------------------------
inline
void CnTechnique::SetName( const CnString& strName )
{
	m_strName = strName;
}
inline
const CnString& CnTechnique::GetName() const
{
	return m_strName;
}

//----------------------------------------------------------------
inline
void CnTechnique::SetHandle( ShaderHandle hHandle )
{
	m_hHandle = hHandle;
}
inline
ShaderHandle CnTechnique::GetHandle() const
{
	return m_hHandle;
}

//----------------------------------------------------------------
inline
const CnTechnique::PassArray& CnTechnique::GetPasses() const
{
	return m_Passes;
}

//----------------------------------------------------------------
inline
int CnTechnique::GetNumPasses() const
{
	return m_nNumPasses;
}


//----------------------------------------------------------------
/** @brief		Feature Mask
*/
inline
void CnTechnique::SetFeatureMask( CnShaderFeature::Mask Mask )
{
	m_FeatureMask = Mask;
}
//----------------------------------------------------------------
inline
CnShaderFeature::Mask
CnTechnique::GetFeatureMask() const
{
	return m_FeatureMask;
}