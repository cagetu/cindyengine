//================================================================
// File:               : CnShader.inl
// Related Header File : CnShader.h
// Original Author     : changhee
// Creation Date       : 2007. 5. 3
//================================================================

//------------------------------------------------------------------------------
/**
*/
inline
CnShaderEffect* CnShader::GetEffect( ShaderFeature::Bits Mask ) const
{
	return m_pShaderEffect;
}

//------------------------------------------------------------------------------
/**
*/
inline
ushort CnShader::GetNumTechniques() const
{
	return GetEffect()->GetNumTechniques();
	//return m_nNumTechniques;
}

//------------------------------------------------------------------------------
/** Shader Parameter ����
*/
inline
void CnShader::SetParemter( ShaderState::Param eParam, ShaderHandle hHandle )
{
	GetEffect()->SetParameter( eParam, hHandle );
	//m_hParameters[eParam] = hHandle;
}
//------------------------------------------------------------------------------
/**
*/
inline
void CnShader::SetParameters( void* pParameters )
{
	GetEffect()->SetParameters( pParameters );
	//memcpy( m_hParameters, pParameters, sizeof(m_hParameters) );
}

//------------------------------------------------------------------------------
/**
*/
inline
ShaderHandle CnShader::GetParameter( ShaderState::Param eParam ) const
{
	return GetEffect()->GetParameter( eParam );
	//return m_hParameters[eParam];
}
//------------------------------------------------------------------------------
/**
*/
inline
bool CnShader::HasParameter( ShaderState::Param eParam ) const
{
	return GetEffect()->HasParameter( eParam );
	//return m_hParameters[eParam] != NULL;
}
//------------------------------------------------------------------------------
/**
*/
inline
void CnShader::ClearParameters()
{
	GetEffect()->ClearParameters();
	//memset( m_hParameters, 0, sizeof(m_hParameters) );
}

//----------------------------------------------------------------
//	ShaderPtr Class
//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr()
: Ptr<CnShader>()
{
}

//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr( CnShader* pObject )
: Ptr<CnShader>( pObject )
{
}

//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr( const ShaderPtr& spObject )
: Ptr<CnShader>( spObject )
{
}

//----------------------------------------------------------------
inline
ShaderPtr::ShaderPtr( const RscPtr& spRsc )
: Ptr<CnShader>()
{
	m_pObject = (CnShader*)spRsc.GetPtr();
	if (m_pObject)
	{
		m_pObject->AddRef();
	}
}

//----------------------------------------------------------------
inline
ShaderPtr& ShaderPtr::operator= ( const RscPtr& spRsc )
{
	if (m_pObject != (CnShader*)spRsc.GetPtr())
	{
		if (m_pObject)
			m_pObject->Release();

		if (!spRsc.IsNull())
			spRsc->AddRef();

		m_pObject = (CnShader*)spRsc.GetPtr();
	}

	return *this;
}