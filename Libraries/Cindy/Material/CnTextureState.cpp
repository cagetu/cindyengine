//================================================================
// File:               : CnTextureState.cpp
// Related Header File : CnTextureState.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 24
//================================================================
#include "../Cindy.h"
#include "CnTextureState.h"

namespace Cindy
{
	//================================================================
	// TextureState::StageState struct
	//================================================================
	CnTextureState::StageState::StageState()
		: mipmaplodbias(0.0f)
		, maxmipmaplevel(0)
	{
		color.top	= TOP_MODULATE;
		color.arg1	= TA_TEXTURE;
		color.arg2	= TA_DIFFUSE;
		alpha.top	= TOP_MODULATE;
		alpha.arg1	= TA_TEXTURE;
		alpha.arg2	= TA_DIFFUSE;
		address.u	= TADDRESS_WRAP;
		address.v	= TADDRESS_WRAP;
		address.w	= TADDRESS_WRAP;
		filter.mag	= TEXF_LINEAR;
		filter.min	= TEXF_LINEAR;
		filter.mip	= TEXF_LINEAR;
	}

	//================================================================
	// TextureState Class
	//================================================================
	__ImplementRtti(Cindy, CnTextureState, CnGlobalState);
	/// Const/Dest
	CnTextureState::CnTextureState()
	{
	}
	CnTextureState::~CnTextureState()
	{
		m_StageStates.clear();
	}

	//================================================================
	/** Set Stage
		@remarks	텍스쳐 스테이지 설정 정보를 저장한다.
	*/
	//================================================================
	void CnTextureState::SetStage( CnTextureState::Stage nStage, const CnTextureState::StageState& rState )
	{
		std::pair<StageStateIter, bool> result = m_StageStates.insert( StageStateList::value_type( nStage, rState ) );
		if (!result.second)
		{
			(*result.first).second = rState;
		}
	}

	//================================================================
	/** Get Stage
		@remarks	텍스쳐 스테이지 설정 정보를 얻어온다.
	*/
	//================================================================
	bool CnTextureState::GetStage( CnTextureState::Stage nStage, CnTextureState::StageState& rResult )
	{
		StageStateIter iter = m_StageStates.find( nStage );
		if (iter == m_StageStates.end())
			return false;

		rResult = iter->second;
		return true;
	}
}