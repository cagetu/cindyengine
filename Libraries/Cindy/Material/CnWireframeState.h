//================================================================
// File:           : CnWireframeState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_WIREFRAME_STATE_H__
#define __CN_WIREFRAME_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** RenderState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnWireframeState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		enum FillMode
		{
			FILL_POINT		= 1,
			FILL_WIREFRAME	= 2,
			FILL_SOLID		= 3,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnWireframeState();

		// Type
		TYPE		GetStateType() const	{	return FILL;	}

		// Wireframe Mode
		void		SetFillMode( FillMode eMode );
		FillMode	GetFillMode() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		FillMode	m_eFillMode;
	};

}

#endif	// __CN_WIREFRAME_STATE_H__