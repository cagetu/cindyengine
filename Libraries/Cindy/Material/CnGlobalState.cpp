//================================================================
// File:               : CnGlobalState.cpp
// Related Header File : CnGlobalState.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 1
//================================================================
#include "../Cindy.h"
#include "CnGlobalState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnGlobalState, CnObject);
	// Const/Dest
	CnGlobalState::CnGlobalState()
	{
	}
	CnGlobalState::~CnGlobalState()
	{
	}
}