//================================================================
// File:           : CnGlobalState.h
// Original Author : changhee
// Creation Date   : 2008. 12. 1
//================================================================
#ifndef __CN_GLOBAL_STATE_H__
#define __CN_GLOBAL_STATE_H__

#include "../Foundation/CnObject.h"

namespace Cindy
{
	//================================================================
	/** Global State
	    @author    changhee
		@remarks   RenderState, ColorState, TextureState등 랜더러에 설정해야할
				   상태를 정의
	*/
	//================================================================
	class CnGlobalState : public CnObject
	{
		__DeclareRtti;
	public:
		enum TYPE
		{
			MATERIAL = 0,
			ALPHA,
			CULL,
			FILL,
			ZBUFFER,
			FOG,
			LIGHT,
			STENCIL,
			SCISSOR,
			RENDER,
			TEXTURE,

			MAX_STATE,
		};

		virtual ~CnGlobalState();

		virtual TYPE	GetStateType() const = 0;
	protected:
		CnGlobalState();
	};

	typedef Ptr<CnGlobalState>	GlobalStatePtr;
}

#endif	// __CN_GLOBAL_STATE_H__