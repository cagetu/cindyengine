//================================================================
// File:           : CnShaderEffect.h
// Original Author : changhee
// Creation Date   : 2009. 5. 21
//================================================================
#ifndef __CN_SHADER_EFFECT_H__
#define __CN_SHADER_EFFECT_H__

#include "CnShaderDefines.h"
#include "CnTechnique.h"
#include "Math/CnMatrix44.h"

namespace Cindy
{
	class CnTexture;

	//================================================================
	/** ShaderEffect
	    @author    changhee
		@since     2007. 2. 21
		@remarks   쉐이더 Effect 클래스
	*/
	//================================================================
	class CN_DLL CnShaderEffect : public CnObject
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnShaderEffect();
		virtual ~CnShaderEffect();

		// 생성
		virtual bool	Create( unsigned char* shaderCode, ulong fileSize ) abstract;

		// Technique
		virtual void	SetTechnique( ShaderHandle hTechnique ) abstract;
		ushort			GetNumTechniques() const;
		CnTechnique*	GetTechnique( const CnString& strName ) const;
		CnTechnique*	GetTechnique( CnShaderFeature::Mask Mask ) const;

		// Parameters
		void			SetParameter( ShaderState::Param eParam, ShaderHandle hHandle );
		void			SetParameters( void* pParameters );
		ShaderHandle	GetParameter( ShaderState::Param eParam ) const;
		bool			HasParameter( ShaderState::Param eParam ) const;
		void			ClearParameters();

		// Shader Begin/End
		virtual int		Begin( bool bSaveState = true ) abstract;
		virtual void	End() abstract;

		// Device Lost/Reset
		virtual void	OnLost() abstract;
		virtual void	OnRestore() abstract;
		virtual void	Clear() abstract;

		// Pass
		virtual void	BeginPass( uint nPass ) abstract;
		virtual void	EndPass() abstract;
		virtual void	CommitChanges() abstract;

		// Set 'Boolean'
		virtual void	SetBool( ShaderHandle hHandle, bool val ) abstract;
		// Set 'Int'
		virtual void	SetInt( ShaderHandle hHandle, int val ) abstract;
		// Set 'Float'
		virtual void	SetFloat( ShaderHandle hHandle, float val ) abstract;
		//virtual void	SetFloat4( ShaderHandle hHandle, float* val ) abstract;
		virtual void	SetFloatArray( ShaderHandle hHandle, float* aFloats, int nSize ) abstract;
		// Set 'Vector'
		virtual void	SetVector3( ShaderHandle hHandle, const Math::Vector3& val ) abstract;
		virtual void	SetVector4( ShaderHandle hHandle, const Math::Vector4& val ) abstract;

		virtual void	SetVector3Array( ShaderHandle hHandle, const Math::Vector3* aVectors, int nSize ) abstract;
		virtual void	SetVector4Array( ShaderHandle hHandle, const Math::Vector4* aVectors, int nSize ) abstract;
		// Set 'Matrix'
		virtual void	SetMatrix( ShaderHandle hHandle, const Math::Matrix44& val ) abstract;
		virtual void	SetMatrixArray( ShaderHandle hHandle, const Math::Matrix44* aMatrices, int nSize ) abstract;
		// Set 'Texture'
		virtual void	SetTexture( ShaderHandle hHandle, CnTexture* texture ) abstract;
		virtual void	SetTextures( ShaderHandle hHandle, CnTexture** textures, int nSize ) abstract;
		// Set 'Value'
		virtual void	SetValue( ShaderHandle hHandle, void* pData, unsigned int nBytes ) abstract;
		// Set 'Color'
		virtual void	SetColor( ShaderHandle hHandle, const CnColor& color ) abstract;

	protected:
		friend class CnShader;

		typedef HashMap<CnString, Ptr<CnTechnique> > TechniqueMap;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		TechniqueMap	m_Techniques;					//!< Technique 리스트
		ushort			m_nNumTechniques;				//!< Technique 개수

		ShaderHandle	m_hParameters[ShaderState::NumParameters];			//!< 이펙트 파일 내에 파라미터 목록

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnTechnique*	_AddTechnique( const CnString& strName );
		void			_ClearTechniques();

		virtual void	_UpdateTechniques() abstract;
	};

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	ushort CnShaderEffect::GetNumTechniques() const
	{
		return m_nNumTechniques;
	}

	//------------------------------------------------------------------------------
	/** Shader Parameter 설정
	*/
	inline
	void CnShaderEffect::SetParameter( ShaderState::Param eParam, ShaderHandle hHandle )
	{
		m_hParameters[eParam] = hHandle;
	}
	//------------------------------------------------------------------------------
	/**
	*/
	inline
	void CnShaderEffect::SetParameters( void* pParameters )
	{
		memcpy( m_hParameters, pParameters, sizeof(m_hParameters) );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	ShaderHandle CnShaderEffect::GetParameter( ShaderState::Param eParam ) const
	{
		return m_hParameters[eParam];
	}
	//------------------------------------------------------------------------------
	/**
	*/
	inline
	bool CnShaderEffect::HasParameter( ShaderState::Param eParam ) const
	{
		return m_hParameters[eParam] != NULL;
	}
	//------------------------------------------------------------------------------
	/**
	*/
	inline
	void CnShaderEffect::ClearParameters()
	{
		memset( m_hParameters, 0, sizeof(m_hParameters) );
	}
}

#endif	// __CN_SHADER_EFFECT_H__