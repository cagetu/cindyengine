//================================================================
// File:               : CnShaderFeature.cpp
// Related Header File : CnShaderFeature.h
// Original Author     : changhee
// Creation Date       : 2009. 5. 11
//================================================================
#include "../Cindy.h"
#include "CnShaderFeature.h"
#include "Util/CnString.h"
#include "Util/CnLog.h"

namespace Cindy
{
namespace ShaderFeature
{
	//----------------------------------------------------------------
	/**
	*/
	//----------------------------------------------------------------
	static const wchar* gs_FeatureTable[ShaderFeature::NumFeature] =
	{
		L"Depth",
		L"SMDepth",
		L"GBuffer",
		L"Skinned",
		L"VertexColor",
		L"DiffuseMap0",
		L"NormalMap",
		L"SpecMap",
		L"SpecExpMap",
		L"EmissiveMap",
		L"GlowMap",
		L"ToonMap",	
		L"ReflectMap",
		L"AlphaMap",
		L"ShadowMap",
		L"Unlit",
		L"BlendAlpha",
		L"RimLighting",
		L"SkinLighting",
		L"MultipleVertexLight",
	};

	//----------------------------------------------------------------
	/** StringToParam
		@remarks		미리 정해놓은 파라미터 이름을 인덱스로 변환해준다.
	*/
	//----------------------------------------------------------------
	ShaderFeature::Bits
	ShaderFeature::ToBit( ShaderFeature::Feature feature )
	{
		return (1<<feature);
	}

	//----------------------------------------------------------------
	/** StringToParam
		@remarks		미리 정해놓은 파라미터 이름을 인덱스로 변환해준다.
	*/
	//----------------------------------------------------------------
	ShaderFeature::Feature 
	ShaderFeature::StringToFeature( const wchar* name )
	{
		for( uint i = 0; i < ShaderFeature::NumFeature; ++i )
		{
			if( 0 == wcscmp( gs_FeatureTable[i], name ) )
				return (ShaderFeature::Feature)i;
		}
		return ShaderFeature::InvalidFeature;
	}

	//----------------------------------------------------------------
	/** ParamToString
		@remarks		인덱스를 파라미터 이름으로 변경한다.
	*/
	//----------------------------------------------------------------
	const wchar* 
	ShaderFeature::FeatureToString( ShaderFeature::Feature feature )
	{
		return gs_FeatureTable[feature];
	}
}

	//=============================================================================
	//
	//=============================================================================
	CnShaderFeature::CnShaderFeature()
	{
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnShaderFeature::Mask
	CnShaderFeature::StringToMask( const CnString& str )
	{
		CnPrint( L"[CnShaderFeature::StringToMask] %s", str.c_str() );

		Mask mask = 0;

		CnStringArray tokens = unicode::Tokenize( str, L"\t |" );
		int tokenSize = (int)tokens.size();
		for (int tokenIndex=0; tokenIndex<tokenSize; tokenIndex++)
		{
			Name featureName = tokens[tokenIndex];
			HashMap<Name, int>::iterator iter = m_StringToIndex.find(featureName);
			if (m_StringToIndex.end() != iter)
			{
				// feature name already has bit number assigned
				mask |= (1<<(*iter).second);
			}
			else
			{
				// new feature name, assign a new bit number
				int bitIndex = m_Ticket++;
				if (m_Ticket >= MaxId)
				{
					assert(L"ShaderFeature: more then 32 unqiue shader features requested!");
					return 0;
				}
				m_StringToIndex.insert( HashMap<Name, int>::value_type( featureName, bitIndex ) );
				m_IndexToString.push_back( featureName );

				// finally update the mask
				mask |= (1<<bitIndex);
			}
		}
		return mask;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnShaderFeature::Name
	CnShaderFeature::MaskToString( CnShaderFeature::Mask mask )
	{
		CnString str;
		int bitIndex;
		for (bitIndex = 0; bitIndex < MaxId; bitIndex++)
		{
			if (0 != (mask & (1<<bitIndex)))
			{
				if (!str.empty())
				{
					str.append(L"|");
				}
				str.append(m_IndexToString[bitIndex]);
			}
		}
		return str;
	}
}