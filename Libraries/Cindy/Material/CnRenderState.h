//================================================================
// File:           : CnRenderState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_RENDER_STATE_H__
#define __CN_RENDER_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** RenderState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnRenderState// : public CnGlobalState
	{
		//__DeclareRtti;
	public:
		enum Bits
		{
			_NO_ALPHABLEND		= 0,
			_ALPHABLEND			= (1<<0),
			_NO_ALPHATEST		= 0,
			_ALPHATEST			= (1<<1),
			_FILL_SOLID			= 0,
			_FILL_WIRE			= (1<<2),
			_CULL_CCW			= 0,
			_CULL_NONE			= (1<<3),
			_Z_ENABLE			= 0,
			_Z_DISABLE			= (1<<4),
			_ZWRITE_ENABLE		= 0,
			_ZWRITE_DISABLE		= (1<<5),
			_FOG_DISABLE		= 0,
			_FOG_ENABLE			= (1<<6),
			_NO_SCISSORTEST		= 0,
			_SCISSORTEST		= (1<<7),
		};

		enum CullMode
		{
			CULL_NONE	= 1,
			CULL_CW		= 2,
			CULL_CCW	= 3,
		};

		enum FillMode
		{
			FILL_POINT		= 1,
			FILL_WIREFRAME	= 2,
			FILL_SOLID		= 3,
		};

		enum BlendMode
		{
			BLEND_ZERO               = 1,
			BLEND_ONE                = 2,
			BLEND_SRCCOLOR           = 3,
			BLEND_INVSRCCOLOR        = 4,
			BLEND_SRCALPHA           = 5,
			BLEND_INVSRCALPHA        = 6,
			BLEND_DESTALPHA          = 7,
			BLEND_INVDESTALPHA       = 8,
			BLEND_DESTCOLOR          = 9,
			BLEND_INVDESTCOLOR       = 10,
			BLEND_SRCALPHASAT        = 11,
			BLEND_BOTHSRCALPHA       = 12,
			BLEND_BOTHINVSRCALPHA    = 13,
			BLEND_BLENDFACTOR        = 14, /* Only supported if D3DPBLENDCAPS_BLENDFACTOR is on */
			BLEND_INVBLENDFACTOR     = 15, /* Only supported if D3DPBLENDCAPS_BLENDFACTOR is on */
		};

		enum BlendOp
		{
			BLENDOP_NONE			 = 0,
			BLENDOP_ADD              = 1,
			BLENDOP_SUBTRACT         = 2,
			BLENDOP_REVSUBTRACT      = 3,
			BLENDOP_MIN              = 4,
			BLENDOP_MAX              = 5,
		};

		// ZBuffer
		enum ZBufferMode
		{
			ZB_DISABLE		= 0,
			ZB_USEZBUFFER	= 1,
			ZB_USEWBUFFER	= 2,
		};

		enum ZFunc
		{
			FUNC_NEVER                = 1,
			FUNC_LESS                 = 2,
			FUNC_EQUAL                = 3,
			FUNC_LESSEQUAL            = 4,
			FUNC_GREATER              = 5,
			FUNC_NOTEQUAL             = 6,
			FUNC_GREATEREQUAL         = 7,
			FUNC_ALWAYS               = 8,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnRenderState();
		virtual ~CnRenderState();

		// Type
		CnGlobalState::TYPE		GetStateType() const	{	return CnGlobalState::RENDER;		}

		void		AddFlags( ushort Flags );
		void		RemoveFlags( ushort Flags );
		void		ResetFlags();
		void		SetFlags( ushort Flags );
		ushort		GetFlags() const;
		bool		IsFlags( ushort Flags ) const;
		
		// Cull Mode
		void		SetCullMode( CullMode eMode );
		CullMode	GetCullMode() const;

		// Wireframe Mode
		void		SetFillMode( FillMode eMode );
		FillMode	GetFillMode() const;

		// Z Buffer
		void		SetZBufferEnable( int nEnable );
		int			GetZBufferEnable() const;

		void		SetZBufferMode( ZBufferMode eMode );
		ZBufferMode	GetZBufferMode() const;

		void		SetZBufferFunc( ZFunc eFunc );
		ZFunc		GetZBufferFunc() const;

		// Alpha Blend Enable
		void		SetAlphaBlendEnable( bool bEnable );
		bool		GetAlphaBlendEnable() const;

		void		SetAlphaSrcBlend( BlendMode eMode );
		BlendMode	GetAlphaSrcBlend() const;

		void		SetAlphaDstBlend( BlendMode eMode );
		BlendMode	GetAlphaDstBlend() const;

		void		SetTFactor( ulong ulFactor );
		ulong		GetTFactor() const;

		void		SetAlphaBlendOp( BlendOp eMode );
		BlendOp		GetAlphaBlendOp() const;

		// Alpha Test Enable
		void		SetAlphaTestEnable( bool bEnable );
		bool		GetAlphaTestEnable() const;
		
		void		SetAlphaFunc( ZFunc eFunc );
		ZFunc		GetAlphaFunc() const;

		void		SetAlphaRef( ulong ulRef );
		ulong		GetAlphaRef() const;

		// Light
		void		SetLightEnable( int nEnable );
		int			GetLightEnable() const;

		// Fog
		void		SetFogEnable( int nEnable );
		int			GetFogEnable() const;

		// ScissorTest Enable
		void		SetScissorTestEnable( bool bEnable );
		bool		GetScissorTestEnable() const;

		void		SetScissorTestRect( const RECT& rect );
		const RECT&	GetScissorTestRect() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		// Vertex Render State
		CullMode	m_eCullMode;
		FillMode	m_eFillMode;

		// Z-Buffer
		ZBufferMode	m_eZBuffer;
		ZFunc		m_eZFunc;
		int			m_nEnableZBuffer;

		// Pixel Render State
		// AlphaBlend
		bool		m_bAlphaBlendEnable;
		BlendMode	m_eAlphaSrcBlend;
		BlendMode	m_eAlphaDstBlend;
		ulong		m_ulTFactor;

		BlendOp		m_eAlphaBlendOp;

		// Alpha Test
		bool		m_bAlphaTestEnable;
		ZFunc		m_eAlphaFunc;
		ulong		m_ulAlphaRef;

		int			m_nEnableLight;
		int			m_nEnableFog;

		// SCISSOR TEST
		bool		m_bScissorTestEnable;
		RECT		m_ScissorRect;

		ushort		m_StateFlags;
	};

#include "CnRenderState.inl"
}

#endif	// __CN_RENDER_STATE_H__