//================================================================
// File:               : CnRenderState.cpp
// Related Header File : CnRenderState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnRenderState.h"

namespace Cindy
{
	//----------------------------------------------------------------
	/**	32bit�� 
	*/
	//----------------------------------------------------------------
	//__ImplementRtti(Cindy, CnRenderState, CnGlobalState);
	// Const/Dest
	CnRenderState::CnRenderState()
		: m_eCullMode(CULL_CCW)
		, m_eFillMode(FILL_SOLID)
		, m_eZBuffer(ZB_USEZBUFFER)
		, m_eZFunc(FUNC_LESSEQUAL)
		, m_nEnableZBuffer(1)
		// AlphaBlend
		, m_bAlphaBlendEnable(false)
		, m_eAlphaSrcBlend(BLEND_SRCALPHA)
		, m_eAlphaDstBlend(BLEND_INVSRCALPHA)
		, m_ulTFactor(0xffffffff)
		, m_eAlphaBlendOp(BLENDOP_NONE)
		// AlphaTest
		, m_bAlphaTestEnable(false)
		, m_eAlphaFunc(FUNC_GREATEREQUAL)
		, m_ulAlphaRef(0x000000ff)
		// Light
		, m_nEnableLight(1)
		// Fog
		, m_nEnableFog(0)
		// SCISSORTESTENABLE
		, m_bScissorTestEnable(false)
		, m_StateFlags(0)
	{
	}
	CnRenderState::~CnRenderState()
	{
	}
}