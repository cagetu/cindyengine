//================================================================
// File:           : CnShaderManager.h
// Original Author : changhee
// Creation Date   : 2007. 1. 30
//================================================================
#pragma once

#include "Resource/CnResourceManager.h"
#include "Util/CnSingleton.h"
#include "CnShaderFeature.h"

namespace Cindy
{
	//================================================================
	/** Shader Manager
	    @author    changhee
		@since     2007. 7. 18
		@remarks   쉐이더 Manager
	*/
	//================================================================
	class CN_DLL CnShaderManager : public CnResourceManager
	{
		__DeclareClass(CnShaderManager);
		__DeclareSingleton(CnShaderManager);
	public:
		CnShaderManager();
		virtual ~CnShaderManager();

		// 리소스 생성/읽기
		RscPtr	Load( const wchar* strFileName, int Type = 0 );

		// Path
		void SetIncludePath( const wchar* Path );
		const CnString&	GetIncludePath() const;

		// Feature
		CnShaderFeature::Mask FeatureStringToMask( const CnString& Features );
		CnString FeatureMaskToString( CnShaderFeature::Mask Features );

	private:
		CnShaderFeature		m_ShaderFeature;
		CnString			m_IncludePath;

		// 리소스 생성/읽기
		RscPtr	Load( const wchar* strFileName ) override;
	};

#include "CnShaderManager.inl"
// Macro
#define ShaderMgr	CnShaderManager::Instance()
}	// end of namespace
