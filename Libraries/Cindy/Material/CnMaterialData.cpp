//================================================================
// File:               : CnMaterialData.cpp
// Related Header File : CnMaterialData.h
// Original Author     : changhee
// Creation Date       : 2008. 1. 11
//================================================================
#include "../Cindy.h"
#include "CnMaterialData.h"
#include "CnMaterialDataManager.h"
#include "CnMaterial.h"

#include "Util/CnLog.h"

namespace Cindy
{
	//================================================================
	//	struct MaterialData::MtlInfo
	//================================================================
	CnMaterialData::MtlInfo::MtlInfo()
		: id(-1)
		, enableRimLight(true)
		, enableSubSurface(false)
		, enableMultiLights(false)
		, subSurfaceColor(0.9f, 1.0f, 0.9f, 1.0f)
		, subSurfaceRollOff(0.4f)
	{
	}
	//----------------------------------------------------------------
	/**	@brief	Instance Material
	*/
	CnMaterial* 
	CnMaterialData::MtlInfo::CreateInstance()
	{
		CnMaterial* mtl = CnNew CnMaterial();
		{
			mtl->Enable( this->enable );

			mtl->SetShader( this->shaderFile.c_str(), this->type );
			mtl->SetCurrentTechnique( this->shaderTargetTech );

			mtl->SetColorState( this->colorState );
			mtl->SetRenderState( this->renderState );
			//mtl->SetTextureState( this->textureState );

			if (this->enableRimLight)
			{
				mtl->AddFeature( ShaderFeature::RimLighting );
			}

			if (this->enableMultiLights)
			{
				mtl->AddFeature( ShaderFeature::MultipleVertexLight );
			}

			if (this->enableSubSurface)
			{
				mtl->AddFeature( ShaderFeature::SkinLighting );
				mtl->SetSubSurfaceColor( this->subSurfaceColor );
				mtl->SetSubSurfaceRollOff( this->subSurfaceRollOff );
			}

			TextureMap::iterator iend = this->textures.end();
			TextureMap::iterator i = this->textures.begin();
			for(; i != iend; i++)
			{
				mtl->SetTexture( i->first, i->second );
			}
		}
		return mtl;
	}

	//----------------------------------------------------------------
	/**	@brief	
	*/
	void
	CnMaterialData::MtlInfo::AddMap(MapType::Define type, const CnString& name)
	{
		this->textures.insert(TextureMap::value_type(type, name));
	}

	//================================================================
	//	Class Material Data
	//================================================================
	__ImplementRtti( Cindy, CnMaterialData, CnResource );
	/// Const/Dest
	CnMaterialData::CnMaterialData( CnResourceManager* pParent )
		: CnResource( pParent )
	{
	}
	CnMaterialData::~CnMaterialData()
	{
		_Unload();
	}

	//----------------------------------------------------------------
	bool CnMaterialData::Load( const wchar* strFileName )
	{
		if (IsLoaded())
			return true;

		MoMemStream memStream;

		if (m_pParent->FindData( strFileName, memStream ))
		{
			unsigned char* buffer = memStream.GetBuffer();
			ulong size = memStream.GetSize();

			if (!_Load( buffer, size ))
			{
				memStream.Close();
				return false;
			}

			SetState( Loaded );

			m_strName = strFileName;
			return true;
		}

		CnError( ToStr(L"file not found: %s", strFileName) );
		return false;
	}
	bool CnMaterialData::Unload()
	{
		SetState( Unloaded );

		_Unload();
		m_strName.clear();
		return false;
	}

	//----------------------------------------------------------------
	void CnMaterialData::OnLost()
	{
	}
	void CnMaterialData::OnRestore()
	{
	}

	//----------------------------------------------------------------
	/** @brief	Unload ���� */
	//----------------------------------------------------------------
	void CnMaterialData::_Unload()
	{
		MaterialIter i, iend;
		i = m_Materials.begin();
		iend = m_Materials.end();
		for (; i!=iend; ++i)
			delete (*i);
		m_Materials.clear();
	}
}