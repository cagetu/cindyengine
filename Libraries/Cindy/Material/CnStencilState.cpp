//================================================================
// File:               : CnStencilState.cpp
// Related Header File : CnStencilState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnStencilState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnStencilState, CnGlobalState);
	// Const/Dest
	CnStencilState::CnStencilState()
	{
	}
}