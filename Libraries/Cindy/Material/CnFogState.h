//================================================================
// File:           : CnFogState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_FOG_STATE_H__
#define __CN_FOG_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** RenderState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnFogState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		CnFogState();

		// Type
		TYPE	GetStateType() const	{	return FOG;		}

		// Fog
		void	SetFogEnable( int nEnable );
		int		GetFogEnable() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		int			m_nEnableFog;
	};

	//----------------------------------------------------------------
	/** Enable Fog
		@remarks	Fog 사용여부 설정
		@param		nEnable : 0(false), 1(true)
	*/
	//----------------------------------------------------------------
	inline
	void CnFogState::SetFogEnable( int nEnable )
	{
		m_nEnableFog = nEnable;
	}
	inline
	int CnFogState::GetFogEnable() const
	{
		return m_nEnableFog;
	}

}

#endif	// __CN_FOG_STATE_H__