//================================================================
// File:           : CnTechnique.h
// Original Author : changhee
// Creation Date   : 2007. 2. 14
//================================================================
#ifndef __CN_TECHNIQUE_H__
#define __CN_TECHNIQUE_H__

#include "CnPass.h"
#include "CnShaderFeature.h"

namespace Cindy
{
	class CnShader;

	//================================================================
	/** Technique
	    @author    changhee
		@since     2007. 2. 14
		@remarks   Shader를 사용할 경우 하나의 effect의 technique에 해당.
				   같은 기능을 서로 다른 하드웨어 장치에서 사용할 수 있도록,
				   technique를 각각 작성해 놓는다.
	*/
	//================================================================
	class CnTechnique : public CnObject
	{
		__DeclareRtti;
	public:
		typedef std::vector<Ptr<CnPass> >	PassArray;
		typedef PassArray::iterator				PassIter;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnTechnique( const CnString& strName = L"" );
		virtual ~CnTechnique();

		// 부모 - Shader
		void					SetParent( CnShader* pShader );
		CnShader*				GetParent() const;

		// Name
		void					SetName( const CnString& strName );
		const CnString&			GetName() const;

		// Handle
		void					SetHandle( ShaderHandle hHandle );
		ShaderHandle			GetHandle() const;

		void					SetFeatureMask( CnShaderFeature::Mask Mask );
		CnShaderFeature::Mask	GetFeatureMask() const;

		// Passes
		Ptr<CnPass>				AddPass( const CnString& strName );
		Ptr<CnPass>				GetPass( const CnString& strName );
		Ptr<CnPass>				GetPass( int nIndex );
		const PassArray&		GetPasses() const;
		void					RemoveAllPasses();

		int						GetNumPasses() const;

		// Begin/End
		int 					Begin( bool bSaveState = true );
		void					End();

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString		m_strName;
		ShaderHandle	m_hHandle;

		CnShader*		m_pShader;

		PassArray		m_Passes;
		int				m_nNumPasses;

		CnShaderFeature::Mask	m_FeatureMask;
	};

#include "CnTechnique.inl"
}

#endif	// __CN_TECHNIQUE_H__