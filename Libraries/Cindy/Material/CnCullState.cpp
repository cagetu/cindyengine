//================================================================
// File:               : CnCullState.cpp
// Related Header File : CnCullState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnCullState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnCullState, CnGlobalState);
	// Const/Dest
	CnCullState::CnCullState()
		: m_eCullMode(CULL_CCW)
	{
	}
}