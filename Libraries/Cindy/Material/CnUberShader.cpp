//================================================================
// File:               : CnUberShader.cpp
// Related Header File : CnUberShader.h
// Original Author     : changhee
// Creation Date       : 2009. 5. 19
//================================================================
#include "../Cindy.h"
#include "../Util/CnLog.h"
#include "../Graphics/CnRenderer.h"

#include "CnUberShader.h"
#include "CnShaderManager.h"

namespace Cindy
{
	//------------------------------------------------------------------------------
	__ImplementRtti(Cindy, CnUberShader, CnShader);
	// Constructor
	CnUberShader::CnUberShader( CnResourceManager* pParent )
		: CnShader( pParent )
	{
	}
	// Distructor
	CnUberShader::~CnUberShader()
	{
		Clear();
	}

	//------------------------------------------------------------------------------
	/**
	*/
	bool CnUberShader::Load( const wchar* strFileName )
	{
		MoCommon::MoMemStream memStream;
		if (!m_pParent->FindData( strFileName, memStream ))
		{
			CnError( ToStr(L"file not found: %s", strFileName) );
			return false;
		}

		m_UberShaderCode = (char*)memStream.GetBuffer();

		m_strName = strFileName;
		return true;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	bool CnUberShader::Unload()
	{
		Clear();
		m_strName.clear();
		return true;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnUberShader::Apply( ShaderFeature::Bits Mask )
	{
		CnShaderEffect* sh = GetEffect( Mask );
		if (sh == NULL)
		{
			sh = Create(Mask);
		}

		m_pShaderEffect = sh;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnShaderEffect* CnUberShader::Create( ShaderFeature::Bits Mask )
	{
#ifdef _DEBUG
		CnString featureLog( L"[CnUberShader::Create] Feature:" );
#endif
		std::string defines;
		if (ShaderFeature::ToBit(ShaderFeature::Depth) & Mask)
		{
			defines += "#define _DEPTH_\r\n";
#ifdef _DEBUG
			featureLog += L",Depth";
#endif
		}
		if (ShaderFeature::ToBit(ShaderFeature::SMDepth) & Mask)
		{
			defines += "#define _SMDEPTH_\r\n";
#ifdef _DEBUG
			featureLog += L",ShadowMapDepth";
#endif
		}
		if (ShaderFeature::ToBit(ShaderFeature::GBuffer) & Mask)
		{
			defines += "#define _GBUFFER_\r\n";
#ifdef _DEBUG
			featureLog += L",G-Buffer";
#endif
		}
		if (ShaderFeature::ToBit(ShaderFeature::Skinned) & Mask)
		{
			defines += "#define _SKINNED_\r\n";
#ifdef _DEBUG
			featureLog += L",Skinned";
#endif
		}
		if (ShaderFeature::ToBit(ShaderFeature::DiffuseMap0) & Mask)
		{
			defines += "#define _DIFFUSE_MAP_\r\n";
#ifdef _DEBUG
			featureLog += L",DiffMap";
#endif
		}

		if (ShaderFeature::ToBit(ShaderFeature::AlphaMap) & Mask)
		{
			defines += "#define _ALPHA_MAP_\r\n";
#ifdef _DEBUG
			featureLog += L",AlphaMap";
#endif
		}

		//@cagetu 10/01/26 : 그림자 만들 때는 다른건 필요없어~!!!
		if (!(ShaderFeature::ToBit(ShaderFeature::Depth) & Mask) &&
			!(ShaderFeature::ToBit(ShaderFeature::SMDepth) & Mask))
		{
			if (ShaderFeature::ToBit(ShaderFeature::NormalMap) & Mask)
			{
				defines += "#define _NORMAL_MAP_\r\n";
	#ifdef _DEBUG
				featureLog += L",NormMap";
	#endif
			}

			//@cagetu 10/02/22 : G-Buffer에 NormalMap은 필요하다.
			if (!(ShaderFeature::ToBit(ShaderFeature::GBuffer) & Mask))
			{
				if (ShaderFeature::ToBit(ShaderFeature::SpecMap) & Mask)
				{
					defines += "#define _SPEC_MAP_\r\n";
		#ifdef _DEBUG
					featureLog += L",SpecMap";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::SpecExpMap) & Mask)
				{
					defines += "#define _SPEC_EXP_MAP_\r\n";
		#ifdef _DEBUG
					featureLog += L",SpecExpMap";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::ToonMap) & Mask)
				{
					defines += "#define _TOON_MAP_\r\n";
		#ifdef _DEBUG
					featureLog += L",ToonMap";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::GlowMap) & Mask)
				{
					defines += "#define _GLOW_MAP_\r\n";
		#ifdef _DEBUG
					featureLog += L",GlowMap";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::EmissiveMap) & Mask)
				{
					defines += "#define _EMISSIVE_MAP_\r\n";
		#ifdef _DEBUG
					featureLog += L",EmissiveMap";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::ReflectMap) & Mask)
				{
					defines += "#define _REFLECT_MAP_\r\n";
		#ifdef _DEBUG
					featureLog += L",ReflectMap";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::ShadowMap) & Mask)
				{
					defines += "#define _SHADOW_MAP_\r\n";
		#ifdef _DEBUG
					featureLog += L",ShadowMap";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::Unlit) & Mask)
				{
					defines += "#define _UN_LIT_\r\n";
		#ifdef _DEBUG
					featureLog += L",Unlit";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::BlendAlpha) & Mask)
				{
					defines += "#define _BLEND_ALPHA_\r\n";
		#ifdef _DEBUG
					featureLog += L",BlendAlpha";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::VertexColor) & Mask)
				{
					defines += "#define _VERTEXCOLOR_\r\n";
		#ifdef _DEBUG
					featureLog += L",VColor";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::RimLighting) & Mask)
				{
					defines += "#define _RIM_LIGHT_\r\n";
		#ifdef _DEBUG
					featureLog += L",Rim";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::SkinLighting) & Mask)
				{
					defines += "#define _SKIN_LIGHTING_\r\n";
		#ifdef _DEBUG
					featureLog += L",SubSurface";
		#endif
				}
				if (ShaderFeature::ToBit(ShaderFeature::MultipleVertexLight) & Mask)
				{
					defines += "#define _MULTIVERTEXLIGHTS_\r\n";
		#ifdef _DEBUG
					featureLog += L",Multiple Vertex Light";
		#endif
				}
			}
		}

#ifdef _DEBUG
		CnPrint( featureLog.c_str() );
#endif

		CnShaderEffect* shaderEffect = RenderDevice->CreateEffect();

		std::string shaderCode = defines + m_UberShaderCode;
		if (!shaderEffect->Create( (uchar*)shaderCode.c_str(), (ulong)shaderCode.size() ))
		{
			SAFEDEL( shaderEffect );
			return NULL;
		}

		m_ShaderEffects.insert( EffectMap::value_type( Mask, shaderEffect ) );
		return shaderEffect;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	CnShaderEffect* CnUberShader::GetEffect( ShaderFeature::Bits Mask ) const
	{
		//@cagetu -09/11/30 : 현재 설정된 녀석을 얻어온다!!!
		if (Mask == -1)
			return m_pShaderEffect;

		EffectMap::const_iterator iter = m_ShaderEffects.find( Mask );
		if (m_ShaderEffects.end() == iter)
			return NULL;

		return iter->second;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void CnUberShader::Clear()
	{
		EffectMap::iterator i, iend;

		i = m_ShaderEffects.begin();
		iend = m_ShaderEffects.end();
		for (; i != iend; i++)
			delete (i->second);

		m_ShaderEffects.clear();
		m_pShaderEffect = 0;
	}

} // end of namespace