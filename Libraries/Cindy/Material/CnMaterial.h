//================================================================
// File:           : CnMaterial.h
// Original Author : changhee
// Creation Date   : 2007. 1. 30
//================================================================
#ifndef __CN_MATERIAL_H__
#define __CN_MATERIAL_H__

#include "CnShader.h"
#include "CnTechnique.h"
#include "CnTexture.h"
#include "CnTextureTypes.h"

namespace Cindy
{
	//================================================================
	/** Material
	    @author    changhee
		@since     2007. 1. 30
		@remarks   재질
				   Shader에 설정할 파라미터들을 세팅한다.
	*/
	//================================================================
	class CN_DLL CnMaterial : public CnObject
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnMaterial();
		virtual ~CnMaterial();

		// Name
		const CnString&			GetName() const;

		// Shader
		void					SetShader( const wchar* pszFileName, int nType = 0 );
		const ShaderPtr&		GetShader() const;

		// Technique
		void					SetCurrentTechnique( const CnString& strName );
		CnTechnique*			GetCurrentTechnique() const;
		const CnString&			GetCurrentTechniqueName() const;

		// Features
		//void					AddFeatureBits( CnShaderFeature::Mask Mask );
		//void					RemoveFeatureBits( CnShaderFeature::Mask Mask );
		//void					ResetFeatureBits();
		//CnShaderFeature::Mask	GetFeatureBits() const;

		// 
		void					AddFeature( ShaderFeature::Feature Feature );
		void					RemoveFeature( ShaderFeature::Feature Feature );
		void					ResetFeatures();

		bool					HasFeature( ShaderFeature::Feature Feature ) const;

		void					AddFeatureBits( ShaderFeature::Bits Mask );
		void					RemoveFeatureBits( ShaderFeature::Bits Mask );

		void					SetFeatureBits( ShaderFeature::Bits Mask );
		ShaderFeature::Bits		GetFeatureBits() const;

		// 적용하기
		void					Apply();

		// Transparent
		bool					IsTransparent();

		// Color
		void					SetColorState( const CnMaterialState& ColorState );
		CnMaterialState*		GetColorState();

		// RenderState
		void					SetRenderState( const CnRenderState& RenderState );
		CnRenderState*			GetRenderState();

		// Texture
		void					SetTexture( MapType::Define Type, const CnString& FileName );
		TexPtr					GetTexture( MapType::Define Type );
		bool					HasTexture( MapType::Define Type ) const;

		void					SetTextureState( const CnTextureState& TextureState );
		CnTextureState*			GetTextureState();

		///
		void					SetSubSurfaceColor( const CnColor& Color );
		const CnColor&			GetSubSurfaceColor() const;

		void					SetSubSurfaceRollOff( float RollOff );
		float					GetSubSurfaceRollOff() const;

		// Enable
		void					Enable( bool bEnable );
		bool					IsEnable() const;
	private:
		typedef std::map<MapType::Define, TexPtr>	TextureMap;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString				m_strName;			//!< 이름

		// shader
		int						m_nShaderType;		//!< 0:normal(.fx), 1:uber
		ShaderPtr				m_spShader;

		ShaderFeature::Bits		m_FeatureBits;
		bool					m_bFeatureDirty;

		// technique
		mutable CnTechnique*	m_pCurrentTechnique;

		// state
		CnRenderState			m_RenderState;		//!< RenderState
		CnTextureState			m_TextureState;		//!< TextureState
		CnMaterialState			m_ColorState;		//!< ColorState

		CnColor					m_SubSurfaceColor;
		float					m_RollOff;

		// textures
		TextureMap				m_Textures;

		// Enable
		bool					m_bEnable;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void DirtyTechnique() const;
	};

#include "CnMaterial.inl"
}

#endif	// __CN_MATERIAL_H__