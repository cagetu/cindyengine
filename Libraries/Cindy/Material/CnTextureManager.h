//================================================================
// File:           : CnTextureManager.h
// Original Author : changhee
// Creation Date   : 2007. 5. 21
//================================================================
#pragma once

#include "Resource/CnResourceManager.h"
#include "Util/CnSingleton.h"
#include "CnTexture.h"

namespace Cindy
{
	//================================================================
	/** TextureManager 
	    @author    changhee
		@since     2007. 5. 21
		@remarks   텍스쳐 관리자 클래스
	*/
	//================================================================
	class CN_DLL CnTextureManager : public CnResourceManager
	{
		__DeclareClass(CnTextureManager);
		__DeclareSingleton(CnTextureManager);
	public:
		CnTextureManager();
		virtual ~CnTextureManager();

		// 리소스 생성/읽기
		RscPtr		Load( const wchar* strFileName,
						  ushort usWidth,
						  ushort usHeight,
						  ushort usDepth = 32,
						  PixelFormat::Code eFormat = PixelFormat::A8R8G8B8,
						  ushort usUsage = CnTexture::CreateFromDDSFile );
		RscPtr		Load( const wchar* strFileName ) override;
	private:
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnTexture* CreateTexture();
	};

//#include "CnTextureManager.inl"
// Macro
#define TextureMgr	CnTextureManager::Instance()
} // end of namespace 
