//================================================================
// File:               : CnMaterialData.inl
// Related Header File : CnMaterialData.h
// Original Author     : changhee
// Creation Date       : 2008. 1. 11
//================================================================
//----------------------------------------------------------------
/** @brief	매터리얼 데이터 얻기 */
//----------------------------------------------------------------
inline
CnMaterialData::MtlInfo* 
CnMaterialData::GetMtl( ushort Index )
{
	return m_Materials[Index];
}

//----------------------------------------------------------------
/** @brief	모든 매터리얼 데이터 */
//----------------------------------------------------------------
inline
const CnMaterialData::MaterialList&	
CnMaterialData::GetMtls() const
{
	return m_Materials;
}

//----------------------------------------------------------------
//	MtlDataPtr Class
//----------------------------------------------------------------
inline
MtlDataPtr::MtlDataPtr()
: Ptr<CnMaterialData>()
{
}

//----------------------------------------------------------------
inline
MtlDataPtr::MtlDataPtr( CnMaterialData* pObject )
: Ptr<CnMaterialData>( pObject )
{
}

//----------------------------------------------------------------
inline
MtlDataPtr::MtlDataPtr( const MtlDataPtr& spObject )
: Ptr<CnMaterialData>( spObject )
{
}

//----------------------------------------------------------------
inline
MtlDataPtr::MtlDataPtr( const RscPtr& spRsc )
: Ptr<CnMaterialData>()
{
	m_pObject = (CnMaterialData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->AddRef();
	}
}

//----------------------------------------------------------------
inline
MtlDataPtr& MtlDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnMaterialData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->Release();

		if( !spRsc.IsNull() )
			spRsc->AddRef();

		m_pObject = (CnMaterialData*)spRsc.GetPtr();
	}

	return *this;
}