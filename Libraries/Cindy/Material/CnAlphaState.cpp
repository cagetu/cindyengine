//================================================================
// File:               : CnAlphaState.cpp
// Related Header File : CnAlphaState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
#include "../Cindy.h"
#include "CnAlphaState.h"

namespace Cindy
{
	__ImplementRtti(Cindy, CnAlphaState, CnGlobalState);
	// Const/Dest
	CnAlphaState::CnAlphaState()
		: m_bAlphaBlendEnable(false)
		, m_eAlphaSrcBlend(BLEND_SRCALPHA)
		, m_eAlphaDstBlend(BLEND_INVSRCALPHA)
		, m_ulTFactor(0xffffffff)
		, m_eAlphaBlendOp(BLENDOP_NONE)
		, m_bAlphaTestEnable(false)
		, m_eAlphaFunc(FUNC_GREATEREQUAL)
		, m_ulAlphaRef(0x000000ff)
	{
	}
}