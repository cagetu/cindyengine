//================================================================
// File:           : CnShaderFeature.h
// Original Author : changhee
// Creation Date   : 2009. 5. 11
//================================================================
#ifndef __CN_SHADER_FEATURE_H__
#define __CN_SHADER_FEATURE_H__

namespace Cindy
{
namespace ShaderFeature
{
	typedef int Bits;

	enum Feature
	{
		Depth,
		SMDepth,
		GBuffer,
		Skinned,	
		VertexColor,	
		DiffuseMap0,	
		NormalMap,	
		SpecMap,		
		SpecExpMap,	
		EmissiveMap,	
		GlowMap,	
		ToonMap,
		ReflectMap,
		AlphaMap,
		ShadowMap,
		Unlit,
		BlendAlpha,
		RimLighting,
		SkinLighting,
		MultipleVertexLight,

		NumFeature,
		InvalidFeature,
	};

	/// Type
	Bits			ToBit( ShaderFeature::Feature feature );
	Feature			StringToFeature( const wchar* name );
	const wchar*	FeatureToString( Feature feature );
}

	//================================================================
	/** ShaderFeature 
	    @author	changhee
		@since	2009. 5. 11
		@brief	쉐이더 Feature 클래스
				Shader의 Technique을 선택할 수 있는 Feature.   
		@see	Nebula3 ShaderFeature
	*/
	//================================================================
	class CnShaderFeature
	{
	public:
		typedef unsigned int	Mask;
		typedef CnString		Name;

		enum Type
		{
			None	= 0,
			Skinned,	// "Skinned"
			Depth,		// "Depth"
			// LOD
			LOD_High,
			LOD_Medium,
			LOD_Low,
			// 
		};
	private:
		friend class CnShaderManager;

		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		CnShaderFeature();

		// Mask-String
		Mask StringToMask( const CnString& str );
		Name MaskToString( Mask mask );

		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		static const int MaxId = 32;
		int m_Index;
		int m_Ticket;
		std::vector<Name> m_IndexToString;
		HashMap<Name, int> m_StringToIndex;
	};
}

#endif	// __CN_SHADER_FEATURE_H__