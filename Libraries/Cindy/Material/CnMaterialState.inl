//================================================================
// File:               : CnMaterialState.inl
// Related Header File : CnMaterialState.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 21
//================================================================
//----------------------------------------------------------------
inline
void CnMaterialState::SetAmbient( const CnColor& rColor )
{
	m_crAmbient = rColor;
}
inline
const CnColor& CnMaterialState::GetAmbient() const
{
	return m_crAmbient;
}

//----------------------------------------------------------------
inline
void CnMaterialState::SetDiffuse( const CnColor& rColor )
{
	m_crDiffuse = rColor;
}
inline
const CnColor& CnMaterialState::GetDiffuse() const
{
	return m_crDiffuse;
}

//----------------------------------------------------------------
inline
void CnMaterialState::SetSpecular( const CnColor& rColor )
{
	m_crSpecular = rColor;
}
inline
const CnColor& CnMaterialState::GetSpecular() const
{
	return m_crSpecular;
}

//----------------------------------------------------------------
inline
void CnMaterialState::SetShininess( float fValue )
{
	m_fShininess = fValue;
}
inline
float CnMaterialState::GetShininess() const
{
	return m_fShininess;
}

//----------------------------------------------------------------
inline
void CnMaterialState::SetEmissive( const CnColor& rColor )
{
	m_crEmissive = rColor;
}
inline
const CnColor& CnMaterialState::GetEmissive() const
{
	return m_crEmissive;
}

//----------------------------------------------------------------
inline
void CnMaterialState::SetSpecularIntensity( float fIntensity )
{
	m_fSpecularIntensity = fIntensity;
}

//----------------------------------------------------------------
inline
float CnMaterialState::GetSpecularIntensity() const
{
	return m_fSpecularIntensity;
}

//----------------------------------------------------------------
inline
void CnMaterialState::SetEmissiveIntensity( float fIntensity )
{
	m_fEmissiveIntensity = fIntensity;
}

//----------------------------------------------------------------
inline
float CnMaterialState::GetEmissiveIntensity() const
{
	return m_fEmissiveIntensity;
}

//----------------------------------------------------------------
inline
void CnMaterialState::SetGlowIntensity( float fIntensity )
{
	m_fGlowIntensity = fIntensity;
}

//----------------------------------------------------------------
inline
float CnMaterialState::GetGlowIntensity() const
{
	return m_fGlowIntensity;
}
