//================================================================
// File:           : CnLightState.h
// Original Author : changhee
// Creation Date   : 2007. 2. 21
//================================================================
#ifndef __CN_LIGHT_STATE_H__
#define __CN_LIGHT_STATE_H__

#include "CnGlobalState.h"

namespace Cindy
{
	//================================================================
	/** RenderState
	    @author    changhee
		@since     2007. 2. 21
		@remarks   랜더링 속성
				   (D3DXRENDERSTATE 에 해당하는 녀석들)
	*/
	//================================================================
	class CnLightState : public CnGlobalState
	{
		__DeclareRtti;
	public:
		CnLightState();

		// Type
		TYPE	GetStateType() const	{	return LIGHT;	}

		// Light
		void	SetLightEnable( int nEnable );
		int		GetLightEnable() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		int			m_nEnableLight;
	};

	//----------------------------------------------------------------
	/** Enable Light
		@remarks	Light 사용여부 설정
		@param		nEnable : 0(false), 1(true)
	*/
	//----------------------------------------------------------------
	inline
	void CnLightState::SetLightEnable( int nEnable )
	{
		m_nEnableLight = nEnable;
	}
	inline
	int CnLightState::GetLightEnable() const
	{
		return m_nEnableLight;
	}

}

#endif	// __CN_LIGHT_STATE_H__