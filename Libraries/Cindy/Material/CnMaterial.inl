//================================================================
// File:               : CnMaterial.inl
// Related Header File : CnMaterial.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 30
//================================================================

//----------------------------------------------------------------
inline
const CnString& CnMaterial::GetName() const
{
	return m_strName;
}

//----------------------------------------------------------------
inline
const ShaderPtr& CnMaterial::GetShader() const
{
	return m_spShader;
}

//----------------------------------------------------------------
inline
CnTechnique* CnMaterial::GetCurrentTechnique() const
{
	DirtyTechnique();
	return m_pCurrentTechnique;
}

//----------------------------------------------------------------
inline
const CnString& CnMaterial::GetCurrentTechniqueName() const
{
	return GetCurrentTechnique()->GetName();
}

//----------------------------------------------------------------
inline
void CnMaterial::SetColorState( const CnMaterialState& ColorState )
{
	m_ColorState = ColorState;
}
inline
CnMaterialState* CnMaterial::GetColorState()
{
	return &m_ColorState;
}

//----------------------------------------------------------------
inline
void CnMaterial::SetRenderState( const CnRenderState& RenderState )
{
	m_RenderState = RenderState;
}
inline
CnRenderState* CnMaterial::GetRenderState()
{
	return &m_RenderState;
}

//----------------------------------------------------------------
inline
void CnMaterial::SetTextureState( const CnTextureState& TextureState )
{
	m_TextureState = TextureState;
}
inline
CnTextureState* CnMaterial::GetTextureState()
{
	return &m_TextureState;
}

//----------------------------------------------------------------
inline
void CnMaterial::SetSubSurfaceColor( const CnColor& Color )
{
	m_SubSurfaceColor = Color;
}
inline
const CnColor& CnMaterial::GetSubSurfaceColor() const
{
	return m_SubSurfaceColor;
}

//----------------------------------------------------------------
inline
void CnMaterial::SetSubSurfaceRollOff( float RollOff )
{
	m_RollOff = RollOff;
}
inline
float CnMaterial::GetSubSurfaceRollOff() const
{
	return m_RollOff;
}

//----------------------------------------------------------------
inline
void CnMaterial::Enable( bool bEnable )
{
	m_bEnable = bEnable;
}
inline
bool CnMaterial::IsEnable() const
{
	return m_bEnable;
}

//----------------------------------------------------------------
inline
void CnMaterial::AddFeature( ShaderFeature::Feature Feature )
{
	m_FeatureBits |= ShaderFeature::ToBit(Feature);
	m_bFeatureDirty = true;
}

//----------------------------------------------------------------
inline
void CnMaterial::RemoveFeature( ShaderFeature::Feature Feature )
{
	m_FeatureBits &= ~ShaderFeature::ToBit(Feature);
	m_bFeatureDirty = true;
}

//----------------------------------------------------------------
inline
void CnMaterial::ResetFeatures()
{
	m_FeatureBits = 0;
	m_bFeatureDirty = true;
}

//----------------------------------------------------------------
inline
bool CnMaterial::HasFeature( ShaderFeature::Feature Feature ) const
{
	ShaderFeature::Bits bit = ShaderFeature::ToBit(Feature);
	return ( bit == (m_FeatureBits & bit) );
}

//----------------------------------------------------------------
inline
void CnMaterial::SetFeatureBits( ShaderFeature::Bits Mask )
{
	m_FeatureBits = Mask;
	m_bFeatureDirty = true;
}

//----------------------------------------------------------------
inline
void CnMaterial::AddFeatureBits( ShaderFeature::Bits Mask )
{
	m_FeatureBits |= Mask;
	m_bFeatureDirty = true;
}

//----------------------------------------------------------------
inline
void CnMaterial::RemoveFeatureBits( ShaderFeature::Bits Mask )
{
	m_FeatureBits &= ~Mask;
	m_bFeatureDirty = true;
}

//----------------------------------------------------------------
inline
ShaderFeature::Bits CnMaterial::GetFeatureBits() const
{
	return m_FeatureBits;
}

//----------------------------------------------------------------
//inline
//void CnMaterial::AddFeatureBits( CnShaderFeature::Mask Mask )
//{
//	m_ShaderFeatureBits |= Mask;
//}
//inline
//void CnMaterial::RemoveFeatureBits( CnShaderFeature::Mask Mask )
//{
//	m_ShaderFeatureBits &= ~Mask;
//}
//inline
//void CnMaterial::ResetFeatureBits()
//{
//	m_ShaderFeatureBits = 0;
//}
//inline
//CnShaderFeature::Mask CnMaterial::GetFeatureBits() const
//{
//	return m_ShaderFeatureBits;
//}