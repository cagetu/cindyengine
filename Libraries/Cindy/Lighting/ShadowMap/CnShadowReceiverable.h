//================================================================
// File:           : CnShadowMappable.h
// Original Author : changhee
// Creation Date   : 2009. 12. 7
//================================================================
#pragma once

#include "Material/CnTexture.h"
#include "Math/CnMatrix44.h"

namespace Cindy
{
	//================================================================
	/** Shadow Receiver Class
	    @author    changhee
		@since     2010. 1. 21
		@remarks   그림자 받는 녀석
	*/
	//================================================================
	class CN_DLL CnShadowReceiverable
	{
	public:
		CnShadowReceiverable();
		virtual ~CnShadowReceiverable();

		void					SetShadowMap( const TexPtr& ShadowMap );
		const TexPtr&			GetShadowMap() const;

		void					SetShadowTransform( const Math::Matrix44& Transform );
		const Math::Matrix44&	GetShadowTransform() const;
	protected:
		TexPtr				m_ShadowMap;		// shadow map
		Math::Matrix44		m_ShadowTransform;	// lightViewProjection
	};

	//----------------------------------------------------------------------------
	/**
	*/
	inline void
	CnShadowReceiverable::SetShadowMap( const TexPtr& ShadowMap )
	{
		m_ShadowMap = ShadowMap;
	}

	//----------------------------------------------------------------------------
	/**
	*/
	inline const TexPtr&
	CnShadowReceiverable::GetShadowMap() const
	{
		return m_ShadowMap;
	}

	//----------------------------------------------------------------------------
	/**
	*/
	inline void
	CnShadowReceiverable::SetShadowTransform( const Math::Matrix44& Transform )
	{
		m_ShadowTransform = Transform;
	}

	//----------------------------------------------------------------------------
	/**
	*/
	inline const Math::Matrix44&
	CnShadowReceiverable::GetShadowTransform() const
	{
		return m_ShadowTransform;
	}
}
