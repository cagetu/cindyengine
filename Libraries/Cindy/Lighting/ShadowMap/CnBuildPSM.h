//================================================================
// File:           : CnBuildPSM.h
// Original Author : changhee
// Creation Date   : 2008. 2. 4
//================================================================
#pragma once

#include "CnBuildSSM.h"

namespace Cindy
{
namespace ShadowMap
{
	//================================================================
	/** Perspective Shadow Map Projector
		@author    changhee
		@since     2008. 2. 4
		@remarks   투영 그림자 맵을 만들어 주는 투영기
	*/
	//================================================================
	class CN_DLL PSM : public ShadowMap::SSM
	{
		__DeclareClass(PSM);
	public:
		PSM();
		virtual ~PSM();

	private:
		enum POSTPROJECTIVE_LIGHT
		{
			TO_PALLEL = 0,
			TO_SPOT,
			TO_SINK,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		float	GetNearOffset( float zNear, float zFar );
		void	GetVirtualCamera( CnCamera* pCamera, float zNear, float zFar, float fSlideBack,
								  Math::Matrix44* outView, Math::Matrix44* outProjection );

		void	GetPostProjectiveLight( CnCamera* pCamera, const Math::Matrix44& virtualCameraProj,
										Math::Vector4* outPostProjectLight,
										POSTPROJECTIVE_LIGHT* outLightType );

		void	GetUniformSMMatrix( const Math::Vector4& postProjectiveLight,
									const Math::Matrix44& virtualCameraViewProj );

		void	GetPerspSMMatrix( const Math::Vector4& postProjectiveLight,
								  POSTPROJECTIVE_LIGHT postProjectiveLightType,
								  const Math::Matrix44& eyeToPostProjectiveVirtualCamera,
								  const Math::Matrix44& virtualCameraViewProj );

		void	BuildSMMatrix( CnCamera* pCamera ) override;

	};
}
}