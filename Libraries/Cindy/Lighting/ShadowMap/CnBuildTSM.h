//================================================================
// File:           : CnBuildTSM.h
// Original Author : changhee
// Creation Date   : 2008. 2. 11
//================================================================
#pragma once

#include "CnBuildLisPSM.h"

namespace Cindy
{
namespace ShadowMap
{
	//================================================================
	/** Trapezoidal Space Shadow Map
		@author    changhee
		@since     2008. 2. 22
		@remarks   라이트 공간에서 trapezoid 형태로 투영한 그림자 맵
	*/
	//================================================================
	class CN_DLL TSM : public ShadowMap::LisPSM
	{
		__DeclareClass(TSM);
	public:
		TSM();
		virtual ~TSM();

	private:
		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		void	BuildSMMatrix( CnCamera* pCamera ) override;

		void	CalcTSMMatrix( CnCamera* pCamera );

		void	GetLightSpaceBasis( CnCamera* pCamera, Math::Matrix44* output ) override;

		void	GetTrapezoidSpace( CnCamera* pCamera, Math::Matrix44* output );

		void	TransformFrustumPointsToLightSpace( CnCamera* pCamera, const Math::Matrix44* lightSpaceBasis,
													Math::Vector3* outFrustumPoints );

		void	GetLightSpaceOrtho( Math::Vector3* aFrustumPoints,
									Math::Matrix44* lightSpaceBasis,
									Math::Matrix44* outLightSpaceOrtho );
	};
}
}