//================================================================
// File:           : CnBuildSM.h
// Original Author : changhee
// Creation Date   : 2008. 2. 4
//================================================================
#pragma once

#include "Graphics/Base/CnRenderTexture.h"
#include "Math/CnMatrix44.h"
#include "Frame/CnFrame.h"

namespace Cindy
{
class CnSceneEntity;
class CnLight;
class CnCamera;

namespace ShadowMap
{
	//================================================================
	/** Interface
		@author    changhee
		@since     2008. 2. 4
		@remarks   ������
	*/
	//================================================================
	class CN_DLL Builder : public CnObject
	{
		__DeclareRtti;
	public:
		Builder();
		virtual ~Builder();

		virtual void			Initialize() abstract;
		virtual void			Reset() abstract;

		virtual void			Add( CnSceneEntity* pEntity, CnCamera* pCamera ) abstract;
		virtual void			Test( CnSceneEntity* pEntity, CnCamera* pCamera ) abstract;

		virtual void			Build( CnCamera* pCamera ) abstract;
		virtual void			Debug() abstract;

		// light
		virtual void			SetLight( CnLight* pLight );
		// frame
		virtual void			SetFrame( const Ptr<CnFrame>& Frame ) abstract;
		// shader variables
		virtual void			SetShaderVariables( const Math::Matrix44& WorldTM ) abstract;

		virtual bool			IsEmpty() const abstract;
	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnLight*		m_pLight;
	};
}
}