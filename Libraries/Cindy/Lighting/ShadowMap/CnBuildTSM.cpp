//================================================================
// File:               : CnBuildTSM.cpp
// Related Header File : CnBuildTSM.h
// Original Author     : changhee
// Creation Date       : 2008. 2. 22
//================================================================
#include "Cindy.h"
#include "CnBuildTSM.h"
#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Geometry/CnMesh.h"
#include "Geometry/CnSubMesh.h"

namespace Cindy
{
namespace ShadowMap
{
	//============================================================================
	__ImplementClass( Cindy, TSM, ShadowMap::LisPSM );
	// const
	TSM::TSM() 
		: ShadowMap::LisPSM()
	{
	}
	// dest
	TSM::~TSM()
	{
	}

	//================================================================
	/** Build ShadowMap Matrix
		@remarks	그림자 맵 매트릭스를 생성한다.
	*/
	//================================================================
	void TSM::BuildSMMatrix( CnCamera* pCamera )
	{
		m_fCosGamma = GetAngleBetweenLightAndEyeView( pCamera );

		// 이것은 TSM에 대해서는 엄밀하게 말하면 필요가 없다.
		// 하지만, 'light space' matrix가 view==light 일때 퇴화된다.
		// 이 문제를 피하기 위해,
		// lightDirection 과 viewDirection이 수직이라면,
		// uniform shadow map으로..
		if (Math::Abs(m_fCosGamma) >= 0.999f)
		{
			BuildUniformSMProjectionMatrix( pCamera );
		}
		else
		{
			CalcTSMMatrix( pCamera );
		}
	}

	//================================================================
	/** Calculate Trapezoidal Shadow Map Matrix
		@remarks	TSM 매트릭스를 만든다.
	*/
	//================================================================
	void TSM::CalcTSMMatrix( CnCamera* pCamera )
	{
		Math::Matrix44 trapezoidSpace;
		GetTrapezoidSpace( pCamera, &trapezoidSpace );

		ApplyUnitCubeClip( &trapezoidSpace );

		m_LightViewProjection.Multiply( pCamera->GetView(), trapezoidSpace );
	}

	//================================================================
	/** light Space 기저 행렬을 구한다.
		@remarks	light의 원근 투영 이후 공간으로 eye를 변환할 필요가 있다.
					하지만, 태양은 directional light이다. 그래서 ortho projection을 만들기 전에,
					적당한 rotate/translate matrix를 찾을 필요가 있다.
					이 matrix는 LispSM부터 Y와 Z 축이 변경된 "light space"의 변수이다. 
	*/
	//================================================================
	void TSM::GetLightSpaceBasis( CnCamera* pCamera, Math::Matrix44* output )
	{
		// eye Vector는 eye 공간에서 항상 -Z
		static const Math::Vector3 eyeVector( 0.0f, 0.0f, -1.0f );	

		Math::Vector3 leftVector, upVector, viewVector;

		upVector.MultiplyNormal( m_pLight->GetDirection(), pCamera->GetView() );

		leftVector.Cross( upVector, eyeVector );
		leftVector.Normalize();

		viewVector.Cross( upVector, leftVector );

		// view->world인데??
		output->_11 = leftVector.x; output->_12 = viewVector.x; output->_13 = -upVector.x; output->_14 = 0.0f;
		output->_21 = leftVector.y; output->_22 = viewVector.y; output->_23 = -upVector.y; output->_24 = 0.0f;
		output->_31 = leftVector.z; output->_32 = viewVector.z; output->_33 = -upVector.z; output->_34 = 0.0f;
		output->_41 = 0.0f;			output->_42 = 0.0f;			output->_43 = 0.0f;		   output->_44 = 1.0f;
	}

	//================================================================
	/** lightSpace로 frustum의 포인트들을 변환한다.
	*/
	//================================================================
	void TSM::TransformFrustumPointsToLightSpace( CnCamera* pCamera,
												  const Math::Matrix44* lightSpaceBasis,
												  Math::Vector3* outFrustumPoints )
	{
		// eye space에서 near와 far plane(points)를 구한다.
		Math::Matrix44 projection = pCamera->GetProjection();
		Math::Frustum eyeFrustum( &projection );
		
		Math::Vector3* frustumExtrema = eyeFrustum.GetExtrema();

		/*	frustumExtrema					outFrustumPoints
			6 ----------------- 2			3 ----------------- 1
			|Far				|			|Far				|
			|	7 --------- 3	|			|	7 --------- 5	|
			|	|Near		|	|	-->		|	|Near		|	|
			|	|			|	|			|	|			|	|
			|	5 ---------	1	|			|	6 ---------	4	|
			|					|			|					|
			4 ----------------- 0			2 ----------------- 0
		*/
		for (int i=0; i<4; ++i)
		{
			outFrustumPoints[i] = frustumExtrema[(i<<1)];			// far plane
			outFrustumPoints[i+4] = frustumExtrema[(i<<1) | 0x1];	// near plane
		}

		// light space로 view frustum을 회전한다.
		Math::Vector3::MultipleArray( outFrustumPoints, sizeof(Math::Vector3),
									  outFrustumPoints, sizeof(Math::Vector3),
									  lightSpaceBasis, 8 );
	}

	//================================================================
	/** unit cube를 위해 eye frustum의 3D AABB를 이동/회전한 off-center ortho projection를 만든다.
		@param	lightSpaceBasis : light space의 기저
				outLightSpaceOrtho : 결과 light Space ortho
				outFrusutmPoints : light space ortho로 변환된 새로운 frustum points
	*/
	//================================================================
	void TSM::GetLightSpaceOrtho( Math::Vector3* aFrustumPoints,
								  Math::Matrix44* lightSpaceBasis,
								  Math::Matrix44* outLightSpaceOrtho )
	{
		Math::AABB frustumBox( aFrustumPoints, 8 );

		unsigned int numPoints = (unsigned int)(m_ShadowCasterPoints.size()*8);

		PointArray casterPoints;
		casterPoints.reserve( numPoints );

		AABBIter biend = m_ShadowCasterPoints.end();
		for (AABBIter bi=m_ShadowCasterPoints.begin(); bi<biend; ++bi)
		{
			const Math::Vector3* corners = (*bi).GetCorners();
			for (int i=0; i<8; ++i)
				casterPoints.push_back( corners[i] );
		}

		// 또한 - light projective space안으로 shadow caster 경계상자들(bounding boxes)을 변환한다.
		// 우리는 모든 shadow caster들이 near plane의 앞쪽에 있기 위해 z축에 따라 이동되기를 원한다.
		Math::Vector3::MultipleArray( &casterPoints[0], sizeof(Math::Vector3),
									  &casterPoints[0], sizeof(Math::Vector3),
									  lightSpaceBasis, numPoints );

		Math::AABB casterBox( &casterPoints[0], numPoints );

		float min_z = Math::Min( casterBox.GetMin().z, frustumBox.GetMin().z );
		float max_z = Math::Max( casterBox.GetMax().z, frustumBox.GetMax().z );

		if (min_z <= 0.0f)
		{
			Math::Matrix44 lightSpaceTranslate;
			lightSpaceTranslate.Translation( 0.0f, 0.0f, -min_z+1.0f );

			max_z = -min_z + max_z + 1.0f;
			min_z = 1.0f;

			lightSpaceBasis->Multiply( *lightSpaceBasis, lightSpaceTranslate );

			Math::Vector3::MultipleArray( aFrustumPoints, sizeof(Math::Vector3),
										  aFrustumPoints, sizeof(Math::Vector3),
										  &lightSpaceTranslate, 8 );

			frustumBox = Math::AABB( aFrustumPoints, 8 );
		}

		Math::Vector3 minFrustumAABB( frustumBox.GetMin() );
		Math::Vector3 maxFrustumAABB( frustumBox.GetMax() );

		outLightSpaceOrtho->OrthoOffCenterLH( minFrustumAABB.x, maxFrustumAABB.x,
											  minFrustumAABB.y, maxFrustumAABB.y,
											  min_z, max_z );

		// 새로운 매트릭스에 의해 view frustum이 변환된다.
		Math::Vector3::MultipleArray( aFrustumPoints, sizeof(Math::Vector3),
									  aFrustumPoints, sizeof(Math::Vector3),
									  outLightSpaceOrtho, 8 );
	}

#pragma warning (disable : 4244)
	//================================================================
	/** Trapezoid Space 변환 행렬 NT를 구한다.
	*/
	//================================================================
	void TSM::GetTrapezoidSpace( CnCamera* pCamera, Math::Matrix44* output )
	{
		Math::Matrix44 lightSpaceBasis;
		GetLightSpaceBasis( pCamera, &lightSpaceBasis );

		// eye space에서 near/ far plane(points)를 구한다.
		Math::Vector3 aFrustumPoints[8];
		TransformFrustumPointsToLightSpace( pCamera, &lightSpaceBasis, aFrustumPoints );

		// unit cube로 변환
		Math::Matrix44 lightSpaceOrtho;
		GetLightSpaceOrtho( aFrustumPoints, &lightSpaceBasis, &lightSpaceOrtho );

		// unit cube의 origin으로 top edge의 center를 변환한다.
		Math::Vector2 centerPoints[2];
		{
			// near plane
			centerPoints[0].x = 0.25 * (aFrustumPoints[4].x + aFrustumPoints[5].x + aFrustumPoints[6].x + aFrustumPoints[7].x);
			centerPoints[0].y = 0.25 * (aFrustumPoints[4].y + aFrustumPoints[5].y + aFrustumPoints[6].y + aFrustumPoints[7].y);

			// far plane
			centerPoints[1].x = 0.25 * (aFrustumPoints[0].x + aFrustumPoints[1].x + aFrustumPoints[2].x + aFrustumPoints[3].x);
			centerPoints[1].y = 0.25 * (aFrustumPoints[0].y + aFrustumPoints[1].y + aFrustumPoints[2].y + aFrustumPoints[3].y);
		}
		Math::Vector2 centerOrigin = (centerPoints[0] + centerPoints[1]) * 0.5f;

		Math::Matrix44 translateCenter( 1.0f, 0.0f, 0.0f, 0.0f,
									    0.0f, 1.0f, 0.0f, 0.0f,
									    0.0f, 0.0f,	1.0f, 0.0f,
									    -centerOrigin.x, -centerOrigin.y, 0.0f, 1.0f );

		// trapezoid T는 top edge가 x-축에 동일 선상에 있도록, origin 주위로 R을 적용하여 회전한다. 
		Math::Vector2 length = centerPoints[1] - centerOrigin;
		float halfCenterLength = length.Length();
		float x_length = centerPoints[1].x - centerOrigin.x;
		float y_length = centerPoints[1].y - centerOrigin.y;

		float cosTheta = x_length / halfCenterLength;
		float sinTheta = y_length / halfCenterLength;

		Math::Matrix44 rotateCenter( cosTheta, -sinTheta, 0.0f, 0.0f,
									 sinTheta, cosTheta, 0.0f, 0.0f,
									 0.0f, 0.0f, 1.0f, 0.0f,
									 0.0f, 0.0f, 0.0f, 1.0f );

		Math::Matrix44 trapezoidSpace;
		trapezoidSpace.Multiply( translateCenter, rotateCenter );

		// 이 매트릭스는 y=0으로 center line을 변환한다.
		// Top과 Base는 Center에 직교하기 때문에, 우리는 convex hull 계산을 건너 뛸 수 있고,
		// 대신에 view frustum X-축 극점을 찾아야 한다. 대부분 음수(negative)는 Top이고,
		// 대부분 양수(positive)는 Base Point Q (trapezoid projection point)는 y=0 line 위에 포인트가 될 것이다.
		Math::Vector3::MultipleArray( aFrustumPoints, sizeof(Math::Vector3),
								      aFrustumPoints, sizeof(Math::Vector3),
								      &trapezoidSpace, 8 );

		Math::AABB frustumAABB2D( aFrustumPoints, 8 );

		Math::Vector3 minfrustumAABB2D = frustumAABB2D.GetMin();
		Math::Vector3 maxfrustumAABB2D = frustumAABB2D.GetMax();

		// 두 side edge를 포함하는 두 라인 사이의 각도가 90 degree가 되고, top Edge와 x-축사이의 거리가 1이 되도록 Scale을 적용한다.
		float x_scale = Math::Max( Math::Abs(maxfrustumAABB2D.x), Math::Abs(minfrustumAABB2D.x) );
		float y_scale = Math::Max( Math::Abs(maxfrustumAABB2D.y), Math::Abs(minfrustumAABB2D.y) );
		x_scale = 1.0f/x_scale;
		y_scale = 1.0f/y_scale;

		// bounding box에 의해 최대한의 영역을 차지하도록 한다.
		Math::Matrix44 scaleCenter( x_scale, 0.0f, 0.0f, 0.0f,
								    0.0f, y_scale, 0.0f, 0.0f,
								    0.0f, 0.0f, 1.0f, 0.0f,
								    0.0f, 0.0f, 0.0f, 1.0f );

		trapezoidSpace.Multiply( trapezoidSpace, scaleCenter );

		// 이 값들로 frustum AABB를 스케일한다. ( 같은 공간에서 모든 값을 유지하도록 )
		minfrustumAABB2D.x *= x_scale;
		maxfrustumAABB2D.x *= x_scale;
		minfrustumAABB2D.y *= y_scale;
		maxfrustumAABB2D.y *= y_scale;

		// compute eta.
		//****************************************************************
		/*	lamda: top line과 base line 사의의 거리
			deltaProj : top line으로부터 pL사의의 거리 (tsmDelta값으로 간단히 조절한다.)
			xi: 80% rule을 만들기 위한 값
			eta: top line으로부터 q까지의 거리
			q: top line + eta

			80% rule을 적용하기 위해서, base(y=-1), top(y=1)에 상대적으로 80% line (y=xi)(xi=-0.6)
			인 포인트로 pL을 사상하기 위해 투영의 중심으로 가질 q를 계산하기 위해 원근 투영 문제를
			공식화 한다.
			eta = (lamda*deltaProj + lamda*deltaProj*xi) / (lamda - 2*deltaProj - lamda*xi)
		*/
		//****************************************************************
		static const float tsmDelta = 0.52f;
		static const float xi = -0.6f;		// 80% line

		float lamda = maxfrustumAABB2D.x - minfrustumAABB2D.x;
		float deltaProj = tsmDelta * lamda;		//focusPoint.x - frustumAABB2D.GetMin().x;

		float eta = (lamda*deltaProj * (1.0f+xi)) / (lamda*(1.0f-xi) - 2.0f*deltaProj);

		// 이 포인트는 center line으로 y=0이다.
		Math::Vector2 projectionPointQ( maxfrustumAABB2D.x + eta, 0.0f );

		Math::Matrix44 translatePointQ( -1.0f, 0.0f, 0.0f, 0.0f,
									    0.0f, 1.0f, 0.0f, 0.0f,
									    0.0f, 0.0f, 1.0f, 0.0f,
									    projectionPointQ.x, 0.0f, 0.0f, 1.0f );

		trapezoidSpace.Multiply( trapezoidSpace, translatePointQ );

		//****************************************************************
		/*	q를 통과하고, trapezoid의 6기의 edge를 포함한 side line들이 되도록 구성한다.
			projection Point Q에서 어떤 point 까지의 최대 경사를 찾는다.
			이것은 projection fov가 될 것이다.

			y=0 축 주위로 "trapezoid"를 균형이 맞도록 자른다.
			trapezoid를 재구성하기 때문에, projection fov에 영향을 준다.
			(shear_amt)
		*/
		//****************************************************************
		float maxSlope = -1e32f;
		float minSlope = 1e32f;

		Math::Vector3 tmp;

		for( int i=0; i<8; ++i)
		{
			tmp.x = aFrustumPoints[i].x * x_scale;
			tmp.y = aFrustumPoints[i].y * y_scale;

			float x_dist = tmp.x - projectionPointQ.x;

			if (!ALMOST_ZERO(tmp.y) || ALMOST_ZERO(x_dist))
			{
				maxSlope = max( maxSlope, tmp.y/x_dist );
				minSlope = min( minSlope, tmp.y/x_dist );
			}
		}

		float shear_amt = (maxSlope + Math::Abs(minSlope))*0.5f - maxSlope;

		Math::Matrix44 trapezoidShear( 1.0f, shear_amt, 0.0f, 0.0f,
									   0.0f, 1.0f, 0.0f, 0.0f,
									   0.0f, 0.0f, 1.0f, 0.0f,
									   0.0f, 0.0f, 0.0f, 1.0f );

		trapezoidSpace.Multiply( trapezoidSpace, trapezoidShear );

		//****************************************************************
		// top line에 'unsqueeze'하기 위해 2DH projection을 수행한다.
		//****************************************************************
		const float zAspect = (maxfrustumAABB2D.z - minfrustumAABB2D.z) / (maxfrustumAABB2D.y - minfrustumAABB2D.y);

		float xn = eta;
		float xf = lamda + eta;

		Math::Matrix44 trapezoidProjection(	xf/(xf-xn), 0.0f, 0.0f, 1.0f,
										    0.0f, 1.0f/maxSlope, 0.0f, 0.0f,
											0.0f, 0.0f, 1.0f/(zAspect*maxSlope), 0.0f,
										    -xn*xf/(xf-xn), 0.0f, 0.0f, 1.0f );

		trapezoidSpace.Multiply( trapezoidSpace, trapezoidProjection );

		//****************************************************************
		// x축이 projection의 결과로 [0, 1]로 압축되어 있다. 그래서 [-1, 1]로 확장한다.
		//****************************************************************
		static const Math::Matrix44 biasScaleX( 2.0f, 0.0f, 0.0f, 0.0f,
											    0.0f, 1.0f, 0.0f, 0.0f,
												0.0f, 0.0f, 1.0f, 0.0f,
												-1.0f, 0.0f, 0.0f, 1.0f );

		trapezoidSpace.Multiply( trapezoidSpace, biasScaleX );

		trapezoidSpace.Multiply( lightSpaceOrtho, trapezoidSpace );
		trapezoidSpace.Multiply( lightSpaceBasis, trapezoidSpace );

		*output = trapezoidSpace;
	}
}
}