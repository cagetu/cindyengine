//================================================================
// File:               : CnBuildPSSM.cpp
// Related Header File : CnBuildPSSM.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 18
//================================================================
#include "Cindy.h"
#include "CnBuildPSSM.h"
#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Geometry/CnMesh.h"
#include "Scene/CnVisibleResolver.h"
#include "Graphics/CnTransformDevice.h"
#include "Graphics/CnRenderer.h"
#include "Material/CnTextureManager.h"
#include "Util/CnLog.h"

namespace Cindy
{
namespace ShadowMap
{
#pragma TODO("어떻게 하면, 텍스쳐와 ShadowMatrix를 깔끔하게 넘길 수 있을까?\n1. 4장의 ShadowMap과 Transform을 넘기고, 깊이값으로 선택!!")

	const float PSSM::lightFov = 90.0f;
	const float PSSM::lightNear = 10.0f;
	const float PSSM::lightFarMax = 800.0f;
	//============================================================================
	__ImplementClass( Cindy, PSSM, ShadowMap::SSM );
	//============================================================================
	PSSM::PSSM()
		: m_SplitSchemeLambda(0.5f)
		, m_CameraNear(0.0f)
		, m_CameraFar(0.0f)
		, m_FrustumScale(1.1f)
	{
	}
	PSSM::~PSSM()
	{
	}

	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::Initialize()
	{
		SSM::Initialize();

		m_VisibleMeshes = CnVisibleSet::Create();
	}

	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::SetFrame( const Ptr<CnFrame>& Frame )
	{
		m_Frame = Frame;

		m_RenderTarget = static_cast<CnRenderTexture*>(RenderDevice->GetRenderTarget(L"FinalSM"));
		if (m_RenderTarget.IsValid())
		{
			m_SplitShadowMaps[0] = m_RenderTarget->GetTexture();

			ushort usage = CnTexture::RenderTargetColor | CnTexture::RenderTargetDepthStencil;
			for (ushort i = 1; i < NumSplits; i++)
			{
				CnString name(L"pssm");
				name += unicode::ToString(i);
				m_SplitShadowMaps[i] = TextureMgr->Load( name.c_str(),
														 m_SplitShadowMaps[0]->GetWidth(),
														 m_SplitShadowMaps[0]->GetHeight(),
														 m_SplitShadowMaps[0]->GetDepth(),
														 m_SplitShadowMaps[0]->GetFormat(),
														 usage );
			}

			m_spShadowMap = m_SplitShadowMaps[NumSplits-1];

			
			float offsetX = 0.5f + (0.5f/m_spShadowMap->GetWidth());
			float offsetY = 0.5f + (0.5f/m_spShadowMap->GetHeight());
			float range = 1.0f;
			const Math::Matrix44 texGen( 0.5f,	  0.0f,	   0.0f,  0.0f,
										 0.0f,	 -0.5f,    0.0f,  0.0f,
										 0.0f,	  0.0f,    range, 0.0f,
										 offsetX, offsetY, 0.0f,  1.0f );

			m_TexMatrix = texGen;
		}
	}

	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::SetShaderVariables( const Math::Matrix44& WorldTM )
	{
		if (IsEmpty())
			return;
//		if (m_spShadowMap.IsNull())
//			return;

		ShaderHandle handle = NULL;
		CnShaderEffect* shader = RenderDevice->GetActiveShader();

		if (handle = shader->GetParameter( ShaderState::GlobalPSSMDistances ))
		{
			shader->SetFloatArray( handle, m_SplitDistances, NumSplitDistances );
		}
		if (handle = shader->GetParameter( ShaderState::GlobalPSSMTransforms ))
		{
			Math::Matrix44 shadowTransforms[NumSplits];
			Math::Matrix44 lightview;

			for (ushort i = 0; i < NumSplits; i++)
			{
				lightview.Multiply( WorldTM, m_LightViewProjs[i] );
				shadowTransforms[i].Multiply( lightview, m_TexMatrix );
			}

			shader->SetMatrixArray( handle, shadowTransforms, NumSplits );
		}

		if (handle = shader->GetParameter( ShaderState::GlobalPSSMBuffer0 ))
		{
			shader->SetTexture( handle, m_SplitShadowMaps[0] );
		}
		if (handle = shader->GetParameter( ShaderState::GlobalPSSMBuffer1 ))
		{
			shader->SetTexture( handle, m_SplitShadowMaps[1] );
		}
		if (handle = shader->GetParameter( ShaderState::GlobalPSSMBuffer2 ))
		{
			shader->SetTexture( handle, m_SplitShadowMaps[2] );
		}
		if (handle = shader->GetParameter( ShaderState::GlobalPSSMBuffer3 ))
		{
			shader->SetTexture( handle, m_SplitShadowMaps[3] );
		}
	}

	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::BuildSMMatrix( CnCamera* pCamera )
	{
		/// 1.
		AdjustCameraPlanes( pCamera );

		/// 2. 
		CalculateSplitDistances();

		Math::Vector3 camPos = pCamera->GetPosition();
		Math::Vector3 camLookAt = pCamera->GetLookAt();
		Math::Vector3 camUp = pCamera->GetUp();
		Math::Vector3 camDir = camLookAt - camPos;
		camDir.Normalize();

		Math::Vector3 camRight;
		camRight.Cross( camUp, camDir );
		camRight.Normalize();

		camUp.Cross( camDir, camRight );
		camUp.Normalize();

		/// 3.
		for (ushort i = 0; i < NumSplits; i++)
		{
			CalculateFrustumCorners( i, camPos, camDir, camRight, camUp, pCamera->GetAspect(), pCamera->GetFovY() );
			CalculateTransforms( i );
		}
	}

	//----------------------------------------------------------------------------
	/**
		가능하면 tight하게 보이는 장면을 포함하기 위해서, 카메라의 planes를 설정한다.
		(완벽하기란 불가능!!!)
	*/
	void PSSM::AdjustCameraPlanes( CnCamera* pCamera )
	{
		m_CameraNear = pCamera->GetNear();
		m_CameraFar = pCamera->GetFar();

		float nearPlane;
		float farPlane;
		GetPerspectiveFrustumRange( pCamera, &nearPlane, &farPlane );

		m_CameraNear = nearPlane;
		m_CameraFar = farPlane;
	}

	//----------------------------------------------------------------------------
	/**	frustum 분할 거리들을 계산한다. 아니면, 반대로, 각 분할된 near와 far plane을 구한다.

		Practical split scheme:

			CLi = n*(f/n)^(i/numsplits)
			CUi = n + (f-n)*(i/numsplits)
			Ci = CLi*(lambda) + CUi*(1-lambda)

		lambda scales between logarithmic and uniform
	*/
	void PSSM::CalculateSplitDistances()
	{
		m_SplitSchemeLambda = Math::Clamp( m_SplitSchemeLambda, 0.0f, 1.0f );

		for (ushort i = 0; i < NumSplits; i++)
		{
			float idm = (float)i / (float)NumSplits;
			float log = m_CameraNear * Math::Pow( (m_CameraFar/m_CameraNear), idm );
			float uniform = m_CameraNear + (m_CameraFar-m_CameraNear) * idm;
			m_SplitDistances[i] = log * m_SplitSchemeLambda + uniform * (1-m_SplitSchemeLambda);
		}

		/// 확실하게 시작과 끝 값을 정한다.
		m_SplitDistances[0] = m_CameraNear;
		m_SplitDistances[NumSplits] = m_CameraFar;
	}

	//----------------------------------------------------------------------------
	/**
		현재 frustum split에 대한 frustum point들을 구한다.
	*/
	void PSSM::CalculateFrustumCorners( ushort SplitIndex,
										const Math::Vector3& camPos,
										const Math::Vector3& camDir,
										const Math::Vector3& camRight,
										const Math::Vector3& camUp,
										float aspect,
										float fov )
	{
		float nearPlane = m_SplitDistances[SplitIndex];
		float farPlane = m_SplitDistances[SplitIndex+1];

		float nearPlaneHeight = Math::Tan(fov * 0.5f) * nearPlane;
		float nearPlaneWidth = nearPlaneHeight * aspect;

		float farPlaneHeight = Math::Tan(fov * 0.5f) * farPlane;
		float farPlaneWidth = farPlaneHeight * aspect;

		Math::Vector3 nearPlaneCenter = camPos + camDir * nearPlane;
		Math::Vector3 farPlaneCenter = camPos + camDir * farPlane;

		m_FrustumCorners[0] = nearPlaneCenter - camRight*nearPlaneWidth - camUp*nearPlaneHeight;
		m_FrustumCorners[1] = nearPlaneCenter - camRight*nearPlaneWidth + camUp*nearPlaneHeight;
		m_FrustumCorners[2] = nearPlaneCenter + camRight*nearPlaneWidth + camUp*nearPlaneHeight;
		m_FrustumCorners[3] = nearPlaneCenter + camRight*nearPlaneWidth - camUp*nearPlaneHeight;

		m_FrustumCorners[4] = farPlaneCenter - camRight*farPlaneWidth - camUp*farPlaneHeight;
		m_FrustumCorners[5] = farPlaneCenter - camRight*farPlaneWidth + camUp*farPlaneHeight;
		m_FrustumCorners[6] = farPlaneCenter + camRight*farPlaneWidth + camUp*farPlaneHeight;
		m_FrustumCorners[7] = farPlaneCenter + camRight*farPlaneWidth - camUp*farPlaneHeight;

		// frustum Center
		for (ushort i = 0; i < 8; i++)
		{
			m_FrustumCenter += m_FrustumCorners[i];
		}
		m_FrustumCenter /= 8.0f;

		// center로 부터 offset 만큼을 더한다.
		for (ushort i = 0; i < 8; i++)
		{
			m_FrustumCorners[i] += (m_FrustumCorners[i] - m_FrustumCenter) * (m_FrustumScale - 1.0f);
		}
	}

	//----------------------------------------------------------------------------
	/**
		light에 대한 view, projection 매트릭스를 계산한다. projection 매트릭스는 
		주어진 분할된 에서 "zoomed in" 이다.
	*/
	void PSSM::CalculateTransforms( ushort SplitIndex )
	{
		Math::Vector3 lightLookAt = m_FrustumCenter;
		Math::Vector3 lightPosition = m_pLight->GetWorldPosition();
		Math::Vector3 lightDir = lightLookAt - lightPosition;
		lightDir.Normalize();

		Math::Matrix44 lightView;
		Math::Matrix44 lightProj;
		lightView.LookAtLH( lightPosition, lightLookAt, Math::Vector3::UNIT_Y );
		lightProj.PerspFovLH( Math::DegreeToRadian(lightFov), 1.0f, lightNear, lightFarMax );

		Math::Matrix44 lightViewProj = lightView * lightProj;

		float lightFar = lightFarMax;

		float maxX = -FLT_MAX;
		float maxY = -FLT_MAX;
		float minX = FLT_MAX;
		float minY = FLT_MAX;
		float maxZ = 0.0f;

		for (ushort i = 0; i < 8; i++)
		{
			Math::Vector4 transformed;
			transformed.Multiply( m_FrustumCorners[i], lightViewProj );

			// projected x, y
			transformed.x /= transformed.w;
			transformed.y /= transformed.w;

			// min, max 값 찾기
			if (transformed.x > maxX)	maxX = transformed.x;
			if (transformed.y > maxY)	maxY = transformed.y;
			if (transformed.x < minX)	minX = transformed.x;
			if (transformed.y < minY)	minY = transformed.y;

			// 가장 큰 z distance 찾기
			if (transformed.z > maxZ)	maxZ = transformed.z;
		}

		// set values to valid range (post-projection)
		maxX = Math::Clamp(maxX, -1.0f, 1.0f);
		maxY = Math::Clamp(maxY, -1.0f, 1.0f);
		minX = Math::Clamp(minX, -1.0f, 1.0f);
		minY = Math::Clamp(minY, -1.0f, 1.0f);

		// frustum split가 가장 먼 point에 있도록 light의 가장 먼 plane을 적용한다.
		// 약간의 bias가 필요하다.
		lightFar = maxZ + lightNear + 1.5f;

		// 새로운 far plane을 가지고 light의 matrix를 다시 생성.
		lightProj.PerspFovLH( Math::DegreeToRadian(lightFov), 1.0f, lightNear, lightFar );

		// 현재 frustum split의 point들만을 포함하기 위해서 light view를 cropping 하기 위한 
		// 특수한 matrix를 만든다.
		Math::Matrix44 cropView;
		CalculateCropMatrix( cropView, maxX, maxY, minX, minY );

		/// 결과!!
		lightProj *= cropView;

		// linearized depth를 위해서 projection matrix를 수정한다.
		lightProj._33 /= lightFar;
		lightProj._43 /= lightFar;

		m_LightViews[SplitIndex] = lightView;
		m_LightViewProjs[SplitIndex] = lightView * lightProj;
	}

	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::CalculateCropMatrix( Math::Matrix44& Output, float maxX, float maxY, float minX, float minY )
	{
		float fScaleX = 2.0f / (maxX-minX);
		float fScaleY = 2.0f / (maxY-minY);

		float fOffsetX = -0.5f * (maxX+minX) * fScaleX;
		float fOffsetY = -0.5f * (maxY+minY) * fScaleY;

		Math::Matrix44 cropView( fScaleX,     0.0f,  0.0f,   0.0f,
									0.0f,  fScaleY,  0.0f,   0.0f,
									0.0f,     0.0f,  1.0f,   0.0f,
								fOffsetX, fOffsetY,  0.0f,   1.0f );

		Output = cropView;
	}

	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::Project( CnCamera* pCamera )
	{
//#pragma TODO("랜더링 하는 부분부터 다시 봐야 합니다요~~~")
		const CnVisibleSet::EntityArray* models = m_VisibleSet->GetEntities( CnVisibleSet::Model );
		if (!models)
			return;

		/// 
		Ptr<CnVisibleSet> tmp = VisibleResolver->GetVisibleSet();

		Math::Frustum frustum;

		Math::AABB aabb;
		Math::AABB viewSpaceAABB;
		CnVisibleSet::EntityConstIter i, iend;

		for (ushort split = 0; split < NumSplits; split++)
		{
			m_VisibleMeshes->Clear();

			iend = models->end();
			for (i=models->begin(); i!=iend; ++i)
			{
				(*i)->GetAABB(aabb);

				if (aabb.ClipStatus(m_LightViewProjs[split]) != Math::ClipStatus::Outside)
				{
					m_VisibleMeshes->Register( (*i) );
				}
			}

			TransformDevice->SetViewProjTransform( m_LightViewProjs[split] );
			VisibleResolver->SetVisibleSet( m_VisibleMeshes );

			m_RenderTarget->SetTexture( m_SplitShadowMaps[split] );

			m_Frame->Run();
		}

		VisibleResolver->SetVisibleSet( tmp );
	}
	
	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::Debug( ushort SplitIndex )
	{
		float size = 128.0f;

		float x = size * SplitIndex + 10.0f;

		// Debug..
		const float vertex2D[4][4+2] = 
		{
			{      x,    0, 0, 1, 0,0 },
			{ x+size,    0, 0, 1, 1,0 },
			{ x+size, size, 0, 1, 1,1 },
			{      x, size, 0, 1, 0,1 },
		};

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		device->SetRenderState( D3DRS_LIGHTING, FALSE );

		device->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
		device->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE);
		device->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
		device->SetRenderState( D3DRS_ZENABLE, D3DZB_TRUE );
		device->SetRenderState( D3DRS_ZFUNC, D3DCMP_ALWAYS );
		device->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );

		device->SetSamplerState( 0, D3DSAMP_MIPFILTER, D3DTEXF_POINT );
		device->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_POINT );
		device->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_POINT );

		D3DXMATRIX mIdentity;
		D3DXMatrixIdentity(&mIdentity);
		device->SetTransform(D3DTS_PROJECTION, &mIdentity);
		device->SetTransform(D3DTS_WORLD, &mIdentity);
		device->SetTransform(D3DTS_VIEW, &mIdentity);

		LPDIRECT3DTEXTURE9 tex = NULL;

		if (m_SplitShadowMaps[SplitIndex].IsValid())
		{
			m_SplitShadowMaps[SplitIndex]->GetTexture( &tex );
		}

		device->SetTexture( 0, tex );
		device->SetTexture( 1, NULL );
		device->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
		device->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_SELECTARG1 );

		device->SetPixelShader(NULL);
		device->SetVertexShader(NULL);
		device->SetFVF( D3DFVF_XYZRHW|D3DFVF_TEX1 );

		device->DrawPrimitiveUP( D3DPT_TRIANGLEFAN, 2, vertex2D, sizeof(float)*(4+2) );
	}

	//----------------------------------------------------------------------------
	/**
	*/
	void PSSM::Debug()
	{
		if (m_ShadowCasterPoints.empty())
			return;
		if (m_ShadowReceiverPoints.empty())
			return;

		for (ushort i = 0; i < NumSplits; i++)
		{
			Debug(i);
		}
	}

} // namespace ShadowMap
} // namespace Cindy