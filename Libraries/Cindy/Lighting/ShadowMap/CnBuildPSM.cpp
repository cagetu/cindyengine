//================================================================
// File:               : CnBuildPSM.cpp
// Related Header File : CnBuildPSM.h
// Original Author     : changhee
// Creation Date       : 2008. 2. 4
//================================================================
#include "Cindy.h"
#include "CnBuildPSM.h"
#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Geometry/CnMesh.h"
#include "Math/CnMathDefines.h"

namespace Cindy
{
namespace ShadowMap
{
	static const bool bSlideBack = true;
	static const bool bUnitCubeClip = true;

	//============================================================================
	__ImplementClass( Cindy, PSM, ShadowMap::SSM );

	PSM::PSM()
		: ShadowMap::SSM()
	{
	}
	PSM::~PSM()
	{
	}

	//================================================================
	/** Build
		@brief	그림자 맵 매트릭스를 생성한다.
	*/
	//================================================================
	void PSM::BuildSMMatrix( CnCamera* pCamera )
	{
		float zNear = pCamera->GetNear();
		float zFar = pCamera->GetFar();
		GetPerspectiveFrustumRange( pCamera, &zNear, &zFar );

		//  compute a slideback
		//	무한 평면과 view-box 사이에 어느정도의 거리를 설정한다(force).
		float fSlideBack = 0.0f;
		if (bSlideBack)
		{
			fSlideBack = GetNearOffset( zNear, zFar );
			zFar += fSlideBack;
			zNear += fSlideBack;
		}

		Math::Matrix44 virtualCameraView;
		Math::Matrix44 virtualCameraProj;
		Math::Matrix44 virtualCameraViewProj;

		// Offset 만큼 이동 시킨 view camera를 뷰 공간으로 변환하는 행렬
		GetVirtualCamera( pCamera, zNear, zFar, fSlideBack, &virtualCameraView, &virtualCameraProj );

		virtualCameraViewProj.Multiply( pCamera->GetView(), virtualCameraView );
		virtualCameraViewProj.Multiply( virtualCameraViewProj, virtualCameraProj );

		// 뷰 공간으로 라이트의 방향을 변환
		Math::Vector4 postProjectiveLight;
		POSTPROJECTIVE_LIGHT postProjectiveLightType;
		GetPostProjectiveLight( pCamera,
								virtualCameraProj, 
								&postProjectiveLight,
								&postProjectiveLightType );

		Math::Matrix44 lightView;
		Math::Matrix44 lightProj;

		// 프로젝션 매트릭스를 계산한다.
		// 만약 라이트가 unit box로 부터의 거리가 1000 unit보다 크다면, ortho matrix를 사용한다. (standard shadow mapping)
		bool bUniformShadowMapping = (fabsf(postProjectiveLight.w) <= Math::EPSILON) ? true : false;
		if (bUniformShadowMapping)
		{
			GetUniformSMMatrix( postProjectiveLight, virtualCameraViewProj );
		}
		else
		{	// otherwise, use perspective shadow mapping
			// virtual Camera 원근 투영 이후 공간으로 변환하는 행렬
			Math::Matrix44 eyeToPostProjectiveVirtualCamera;
			eyeToPostProjectiveVirtualCamera.Multiply( virtualCameraView, virtualCameraProj );

			GetPerspSMMatrix( postProjectiveLight,
							  postProjectiveLightType,
							  eyeToPostProjectiveVirtualCamera,
							  virtualCameraViewProj );
		} // if-else
	}

	//================================================================
	/** Get Near Offset
		@remarsk	시야 절두체의 가까운 평면에 접근한 경우, 좋지 않은 결과.
					1. 가까운 평면을 최대한 밀어낸다.
					2. 시야 절두체를 확장한다. 관찰자를 가상으로 뒤로 옮기고,
					   가까운 평면과 먼 평면을 그만큼 앞으로 옮긴다.
	*/
	//================================================================
	float PSM::GetNearOffset( float zNear, float zFar )
	{
		const float Z_EPSILON=0.0001f;
		const float fMinInfinityZ = 1.5f;

		float slideBack = 0.0f;

		float infinity = zFar / (zFar-zNear);

		float fInfinityZ = fMinInfinityZ;
		if (infinity <= fInfinityZ)
		{
			slideBack = fInfinityZ*(zFar-zNear) - zFar + Z_EPSILON;
		}

		return slideBack;
	}

	//================================================================
	/** view Projection 행렬을 만든다.
		@brief	Near Offset을 조정해주었다면, 조정해 준 결과를 적용하여,
					Frustum을 만들기 위한, 투영행렬을 만든다.
					(시야 절두체를 확장한다. 관찰자를 가상으로 뒤로 옮기고,
					가까운 평면과 먼 평면을 그 만큼 앞으로 옮긴다.)
	*/
	//================================================================
	void PSM::GetVirtualCamera( CnCamera* pCamera,
								float zNear,
								float zFar,
								float fSlideBack,
								Math::Matrix44* outView,
								Math::Matrix44* outProjection )
	{
		Math::Matrix44 virtualCameraView, virtualCameraProj;

		if (bSlideBack)
		{
			//  clamp the view-cube to the slid back objects...
			const Math::Vector3 eyePos(0.0f, 0.0f, 0.0f);
			const Math::Vector3 eyeDir(0.0f, 0.0f, 1.0f);

			virtualCameraView.Translation( 0.0f, 0.0f, fSlideBack );

			if (bUnitCubeClip)
			{
				BoundingCone bc( &m_ShadowReceiverPoints, &virtualCameraView, &eyePos, &eyeDir );
				virtualCameraProj.PerspFovLH( 2.f*tanf(bc.fovX)*zNear, 2.f*tanf(bc.fovY)*zNear, zNear, zFar );
			}
			else
			{
				const float viewHeight = ZFAR_MAX * 0.57735026919f;  // tan(0.5f*VIEW_ANGLE)*ZFAR_MAX
				float viewWidth = viewHeight * pCamera->GetAspect();
				float halfFovy = atanf( viewHeight / (ZFAR_MAX+fSlideBack) );
				float halfFovx = atanf( viewWidth  / (ZFAR_MAX+fSlideBack) );

				virtualCameraProj.PerspFovLH( 2.f*tanf(halfFovx)*zNear, 2.f*tanf(halfFovy)*zNear, zNear, zFar );
			}
		}
		else
		{
			virtualCameraView.Identity();
			//virtualCameraProj.PerspFovLH( pCamera->GetFovY(), pCamera->GetAspect(), pCamera->GetNear(), pCamera->GetFar() );
			virtualCameraProj.PerspFovLH( Math::DegreeToRadian(60.0f), pCamera->GetAspect(), zNear, zFar );
		}

		*outView = virtualCameraView;
		*outProjection = virtualCameraProj;
	}

	//================================================================
	/** 원근 이후 공간에서의 광원
		@brief	1. 시선 방향과 수직인 평행광원은 원근 이후 공간에서 xy 평면과 평행을 유지한다.
					2. 관찰자를 향한 평행광원은 원근 이후 공간에서 무한 평면 상의 점광원이 된다.
					3. 관찰자 뒤에서 장면을 비추는 평행광원의 경우, 원래의 평행광원이 뿜어낸 광선들은
					   원근 이후 공간에서 한 점(소실점)으로 수렴된다. 이는 빛이 무한 평면 상의 한 점에서
					   장면으로 도달한다는 뜻이 아니라 무한 평면에 있는 빛 싱크(sink)에 도달한다는 뜻이다.
					   (빛을 쏘는 점-gpg4 5.3 p536 그림5.3.3 참고)

				   세계 공간에서 원근 이후 공간으로 변환하는 행렬를 적용
				   원근 이후 광원의 위치 P일 때, Pw = 0 이면, 1의 경우,
												 Pw > 0 이면, 2의 경우,
												 Pw < 0 이면, 3의 경우
	*/
	//================================================================
	void PSM::GetPostProjectiveLight( CnCamera* pCamera,
									  const Math::Matrix44& virtualCameraProj,
									  Math::Vector4* outPostProjectLight,
									  PSM::POSTPROJECTIVE_LIGHT* outLightType )
	{
		Math::Vector3 eyeLightDir;
		Math::Vector3 lightDir = m_pLight->GetDirection();
		eyeLightDir.MultiplyNormal( lightDir, pCamera->GetView() );

		// directional light는 투영 이후 공간에서 무한 평면 상의 한 점이 된다.
		Math::Vector4 lightDirW( eyeLightDir.x, eyeLightDir.y, eyeLightDir.z, 0.0f );

		// 원근 이후 공간으로 변환
		outPostProjectLight->Multiply( lightDirW, virtualCameraProj );

		// 빛 싱크인지 판별.
		if (outPostProjectLight->w == 0.0f)
		{
			*outLightType = TO_PALLEL;
		}
		else if (outPostProjectLight->w > 0.0f)
		{
			*outLightType = TO_SPOT;
		}
		else if (outPostProjectLight->w < 0.0f)
		{
			*outLightType = TO_SINK;
		}
	}

	//================================================================
	/** Uniform Shadow Map 행렬을 구한다.
	*/
	//================================================================
	void PSM::GetUniformSMMatrix( const Math::Vector4& postProjectiveLight,
								  const Math::Matrix44& virtualCameraViewProj )
	{
		Math::Vector3 lightDirection( postProjectiveLight.x, postProjectiveLight.y, postProjectiveLight.z );
		lightDirection.Normalize();

		Math::AABB unitBox;
		unitBox.SetExtents( Math::Vector3(-1.0f, -1.0f, 0.0f), Math::Vector3(1.0f, 1.0f, 1.0f) );

		Math::Vector3 cubeCenter = unitBox.GetCenter();

		float hitDist;
		unitBox.Intersect( cubeCenter, lightDirection, &hitDist );
		Math::Vector3 lightPos = cubeCenter + 2.0f*hitDist*lightDirection;

		Math::Vector3 axis = Math::Vector3::UNIT_Y;

		// 만약 view 방향과 Y축과 정렬되어있다면, 이상 가공품들(singularity artifacts)를 피하기 위한,
		// 다른 up vector를 선택한다.
		if (fabsf( lightDirection.Dot(Math::Vector3::UNIT_Y) ) > 0.99f)
			axis = Math::Vector3::UNIT_Z;

		Math::Matrix44 lightView;
		lightView.LookAtLH( lightPos, cubeCenter, axis );
		XFormBoundingBox( &unitBox, unitBox, lightView );

		Math::Matrix44 lightProj;
		lightProj.OrthoOffCenterLH( unitBox.GetMin().x, unitBox.GetMax().x,
									unitBox.GetMin().y, unitBox.GetMax().y,
									unitBox.GetMin().z, unitBox.GetMax().z );

		// 월드 공간에서 투영 이후의 라이트 공간으로 변환하는 매트릭스를 만든다.
		m_LightViewProjection.Multiply( lightView, lightProj );
		m_LightViewProjection.Multiply( virtualCameraViewProj, m_LightViewProjection);
	}

	//================================================================
	/** PSM Matrix를 계산한다.
	*/
	//================================================================
	void PSM::GetPerspSMMatrix( const Math::Vector4& postProjectiveLight,
								PSM::POSTPROJECTIVE_LIGHT postProjectiveLightType,
								const Math::Matrix44& eyeToPostProjectiveVirtualCamera,
								const Math::Matrix44& virtualCameraViewProj )
	{
		Math::Vector3 lightPos;
		float wRecip = 1.0f / postProjectiveLight.w;
		lightPos.x = postProjectiveLight.x * wRecip;
		lightPos.y = postProjectiveLight.y * wRecip;
		lightPos.z = postProjectiveLight.z * wRecip;

		// 투영된 후의 view box가 directX에서는 [-1,-1,0]..[1,1,1] 이다, 그래서, radius는 1.5이다.
		const float cubeRadius = 1.5f;
		const Math::Vector3 cubeCenter(0.f, 0.f, 0.5f);

		Math::Matrix44 lightView;
		Math::Matrix44 lightProj;

		// inverse projection matrix를 사용한다.
		if (TO_SINK == postProjectiveLightType)
		{
			BoundingCone viewCone;
			if (!bUnitCubeClip)
			{
				// shadow map 안에 전체 unit cube를 투영한다.
				AABBArray justOneBox;
				Math::AABB unitCube;
				unitCube.SetExtents( Math::Vector3(-1.0f, -1.0f, 0.0f), Math::Vector3(1.0f, 1.0f, 1.0f) );
				justOneBox.push_back( unitCube );

				Math::Matrix44 tmp;
				tmp.Identity();
				viewCone = BoundingCone( &justOneBox, &tmp, &lightPos );
			}
			else
			{
				// unit box의 부분들을 사용하여 shadow map을 둘러싼다.
				viewCone = BoundingCone( &m_ShadowReceiverPoints, &eyeToPostProjectiveVirtualCamera, &lightPos );
			}

			//  construct the inverse projection matrix -- clamp the fNear value for sanity (clamping at too low
			//  a value causes significant underflow in a 24-bit depth buffer)
			//  the multiplication is necessary since I'm not 
			viewCone.fNear = max( 0.001f, viewCone.fNear*3.0f );

			float viewNear = -viewCone.fNear;
			float viewFar = viewCone.fNear;

			lightView = viewCone.lookAt;
			lightProj.PerspFovLH( 2.f*tanf(viewCone.fovX)*viewNear, 2.f*tanf(viewCone.fovY)*viewNear, viewNear, viewFar );
		}
		else
		{
			Math::Matrix44 eyeToPostProjectiveLightView;

			float fFovy, fAspect, fFar, fNear;
			if (!bUnitCubeClip)
			{
				Math::Vector3 lookAt = cubeCenter - lightPos;

				float distance = lookAt.Length();
				lookAt = lookAt/distance;

				Math::Vector3 axis = Math::Vector3::UNIT_Y;

				// 만약 view 방향과 Y축과 정렬되어있다면, 이상 가공품들(singularity artifacts)를 피하기 위한,
				// 다른 up vector를 선택한다.
				if ( fabsf(axis.Dot( lookAt )) > 0.99f )
					axis = Math::Vector3::UNIT_Z;

				//  this code is super-cheese.  treats the unit-box as a sphere
				//  lots of problems, looks like hell, and requires that MinInfinityZ >= 2
				lightView.LookAtLH( lightPos, cubeCenter, axis );

				fFovy = 2.f*atanf(cubeRadius/distance);
				fAspect = 1.f;
				fNear = max(0.001f, distance - 2.f*cubeRadius);
				fFar = distance + 2.f*cubeRadius;

				eyeToPostProjectiveLightView.Multiply( eyeToPostProjectiveVirtualCamera, lightView );
			}
			else
			{
				//  unit cube clipping
				// 장면에 모든 shadow receiver들(지형 포함)의 bounding geometries에 cone을 맞춘다(fit).
				BoundingCone bc( &m_ShadowReceiverPoints, &eyeToPostProjectiveVirtualCamera, &lightPos );

				lightView = bc.lookAt;

				eyeToPostProjectiveLightView.Multiply( eyeToPostProjectiveVirtualCamera, lightView );

				Math::Vector3 lookAt = lightPos - cubeCenter;
				float distance = lookAt.Length();
				fFovy = 2.f * bc.fovY;
				fAspect = bc.fovX / bc.fovY;
				fFar = bc.fFar;

				//	주의!!! clamping 문제를 피하기 위해, near-plane에 아주 작은 값을 적용한다.
				fNear = bc.fNear * 0.6f;
			} // if-else

			fNear = max( 0.001f, fNear );
			float viewNear = fNear;
			float viewFar = fFar;
			lightProj.PerspFovLH( fFovy, fAspect, viewNear, viewFar );
		} // if-else

		// 월드 공간에서 투영 이후의 라이트 공간으로 변환하는 매트릭스를 만든다.
		m_LightViewProjection.Multiply( lightView, lightProj );
		m_LightViewProjection.Multiply( virtualCameraViewProj, m_LightViewProjection );
	}
}
}
