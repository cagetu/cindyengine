//================================================================
// File:           : CnShadowMapSystem.h
// Original Author : changhee
// Creation Date   : 2009. 12. 7
//================================================================
#pragma once

#include "CnShadowMappable.h"
#include "Util/CnSingleton.h"

namespace Cindy
{
	class CnSceneEntity;
	class CnCamera;

	//================================================================
	/** ShadowMap Manager Class
	    @author    changhee
		@since     2007. 12. 7
		@remarks   그림자 맵에 대한 관리자
	*/
	//================================================================
	class CnShadowMapSystem
	{
		__DeclareSingleton(CnShadowMapSystem);
	public:
		CnShadowMapSystem();
		virtual ~CnShadowMapSystem();

		void				SetGlobalShadowMapper( CnShadowMappable* Mapper );
		CnShadowMappable*	GetGlobalShadowMapper() const;

		//const TexPtr&	GetGlobalShadowMap() const;
		//void			GetGlobalShadowTransform( Math::Matrix44* Transform );

	private:
		CnShadowMappable*	m_GlobalShadowMapper;
	};
} // namespace Cindy

#define ShadowMapSystem		Cindy::CnShadowMapSystem::Instance()