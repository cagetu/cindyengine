//================================================================
// File:               : CnShadowMappable.cpp
// Related Header File : CnShadowMappable.h
// Original Author     : changhee
// Creation Date       : 2009. 12. 7
//================================================================
#include "Cindy.h"
#include "CnShadowMappable.h"
#include "CnShadowMapSystem.h"
#include "Scene/CnCamera.h"
#include "Geometry/CnMesh.h"
#include "Lighting/CnLight.h"

namespace Cindy
{
	//================================================================
	// Class ShadowMappable
	//================================================================
	/// Const/Dest
	CnShadowMappable::CnShadowMappable()
		: m_Builder(0)
		, m_Light(0)
	{
	}
	CnShadowMappable::~CnShadowMappable()
	{
	}

	//----------------------------------------------------------------
	//	Light에 Projector를 설정한다.
	//----------------------------------------------------------------
	void CnShadowMappable::SetShadowBuilder( const Ptr<ShadowMap::Builder>& Builder )
	{
		m_Builder = Builder;

		if (m_Builder.IsValid())
		{
			m_Builder->SetLight( m_Light );
			m_Builder->Initialize();

			if (m_Light->GetType() == CnLight::Global)
			{
				CnShadowMapSystem::Instance()->SetGlobalShadowMapper( this );
			}
		}
	}

	//----------------------------------------------------------------
	//	그림자를 드리우는지 확인
	//----------------------------------------------------------------
	bool CnShadowMappable::IsCastShadow() const
	{
		return m_Builder.IsValid();
	}

	//----------------------------------------------------------------
	//	Light 설정
	//----------------------------------------------------------------
	void CnShadowMappable::SetLight( CnLight* pLight )
	{
		m_Light = pLight;
	}

	//----------------------------------------------------------------
	//	투영한다.
	//----------------------------------------------------------------
	void CnShadowMappable::Project( CnCamera* pCamera )
	{
		if (m_Builder.IsNull())
			return;

		m_Builder->Build( pCamera );
	}

	//----------------------------------------------------------------
	//	Entity를 투영 여부에 대해 검사를 한다.
	//----------------------------------------------------------------
	bool CnShadowMappable::_TestProjection( CnCamera* pCamera, CnSceneEntity* pEntity, bool bVisible )
	{
		if (m_Builder.IsNull())
			return false;

		/** 프로젝터가 있다면, 이 녀석이 Receiver인지, Caster인지를 판정한다.
			Caster라면, 프로젝터에 이 객체를 넘겨주고, 
			Receiver라면, 생성될 텍스쳐의 이름을 알려준다.
		*/
		if (bVisible)
		{
			m_Builder->Add( pEntity, pCamera );
		}
		else
		{
			m_Builder->Test( pEntity, pCamera );
		}

		return true;
	}

	//----------------------------------------------------------------
	//	Debug용
	//----------------------------------------------------------------
	void CnShadowMappable::Debug()
	{
		if (m_Builder.IsValid())
		{
			m_Builder->Debug();
		}
	}

	//----------------------------------------------------------------
	//	Light에 설정된 Projector를 Reset한다.
	//----------------------------------------------------------------
	void CnShadowMappable::Reset()
	{
		if (m_Builder.IsValid())
		{
			m_Builder->Reset();
		}
	}
}