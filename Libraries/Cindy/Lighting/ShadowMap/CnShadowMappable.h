//================================================================
// File:           : CnShadowMappable.h
// Original Author : changhee
// Creation Date   : 2009. 12. 7
//================================================================
#pragma once

#include "CnBuildSM.h"

namespace Cindy
{
	class CnSceneEntity;
	class CnCamera;
	class CnLight;

	//================================================================
	/** Shadow Mapper Class
	    @author    changhee
		@since     2009. 12. 7
		@remarks   그림자 생성자
	*/
	//================================================================
	class CN_DLL CnShadowMappable
	{
	public:
		CnShadowMappable();
		virtual ~CnShadowMappable();

		// SetLight
		void							SetLight( CnLight* pLight );

		// ShadowCaster
		void							SetShadowBuilder( const Ptr<ShadowMap::Builder>& Builder );
		const Ptr<ShadowMap::Builder>&	GetShadowBuilder() const;

		bool							IsCastShadow() const;

		void							Reset();

		void							Project( CnCamera* pCamera );
		void							Debug();

	protected:
		//--------------------------------------------------------------
		// Variables
		//--------------------------------------------------------------
		Ptr<ShadowMap::Builder>	m_Builder;

		CnLight*				m_Light;

		//--------------------------------------------------------------
		// Methods
		//--------------------------------------------------------------
		bool	_TestProjection( CnCamera* pCamera, CnSceneEntity* pEntity, bool bVisible );
	};

	//----------------------------------------------------------------
	//	Light에 Projector를 얻어한다.
	//----------------------------------------------------------------
	inline const Ptr<ShadowMap::Builder>&
	CnShadowMappable::GetShadowBuilder() const
	{
		return m_Builder;
	}
}