//================================================================
// File:               : CnBuildLisPSM.cpp
// Related Header File : CnBuildLisPSM.h
// Original Author     : changhee
// Creation Date       : 2008. 2. 4
//================================================================
#include "Cindy.h"
#include "CnBuildLisPSM.h"

#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Geometry/CnMesh.h"
#include "Geometry/CnSubMesh.h"

namespace Cindy
{
namespace ShadowMap
{
	//============================================================================
	__ImplementClass( Cindy, LisPSM, ShadowMap::SSM );
	// const
	LisPSM::LisPSM()
		: ShadowMap::SSM()
		, m_fCosGamma(0.0f)
	{
	}
	// dest
	LisPSM::~LisPSM()
	{
	}

	//================================================================
	/** Build
		@brief	그림자 맵 매트릭스를 생성한다.
	*/
	//================================================================
	void LisPSM::BuildSMMatrix( CnCamera* pCamera )
	{
		m_fCosGamma = GetAngleBetweenLightAndEyeView( pCamera );

		// lightDirection 과 viewDirection이 수직이라면,
		// uniform shadow map으로..
		if (Math::Abs(m_fCosGamma) >= 0.999f)
		{
			BuildUniformSMProjectionMatrix( pCamera );
		}
		else
		{
			PointArray bodyB;
			bodyB.reserve( m_ShadowCasterPoints.size()*8 + 8 );

			CalcFocusedLightVolumePoints( pCamera, bodyB );
			CalcLispSMMatrix( pCamera, bodyB );
		}
	}

	//================================================================
	/** Build Uniform Shadow Map Matrix
		@brief	직교 그림자 변환 행렬을 만든다.
	*/
	//================================================================
	void LisPSM::BuildUniformSMProjectionMatrix( CnCamera* pCamera )
	{
		Math::Matrix44 view = pCamera->GetView();

		Math::Vector3 eyeLightDir;
		eyeLightDir.MultiplyNormal( m_pLight->GetDirection(), view );

		//float fHeight = RADIAN(60.0f);
		//float fWidth = pCamera->GetAspect() * fHeight;
		//Math::Vector3 minPt(-fWidth*pCamera->GetFar(), -fHeight*pCamera->GetFar(), pCamera->GetNear());
		//Math::Vector3 maxPt(fWidth*pCamera->GetFar(), fHeight*pCamera->GetFar(), pCamera->GetFar());
		//Math::AABB frustumAABB( minPt, maxPt );

		unsigned int numBoxes = 0;
		numBoxes = (unsigned int)m_ShadowReceiverPoints.size();
		Math::AABB frustumAABB( &m_ShadowReceiverPoints[0], numBoxes );

		// 라이트 위치는 view frustum에서 무한히 먼거리에 있다.
		// 모든 shadow caster의 밖에 라이트가 위치에 있어야할 필요가 있다.
		numBoxes = (unsigned int)m_ShadowCasterPoints.size();
		Math::AABB casterAABB( &m_ShadowCasterPoints[0], numBoxes );

		Math::Vector3 frustumCenter = casterAABB.GetCenter();

		// 왜 2.0f을 곱하는가??
		float hitDist;
		casterAABB.Intersect( frustumCenter, eyeLightDir, &hitDist );
		Math::Vector3 lightPos = frustumCenter + 2.0f*hitDist*eyeLightDir;

//		Math::Vector3 lightPos;
//		casterAABB.Intersect( frustumCenter, eyeLightDir, &lightPos );

		Math::Vector3 axis;
		if (Math::Abs( eyeLightDir.Dot( Math::Vector3::UNIT_Y ) ) > 0.99f)
		{
			axis = Math::Vector3::UNIT_Z;
		}
		else
		{
			axis = Math::Vector3::UNIT_Y;
		}

		Math::Matrix44 lightView;
		lightView.LookAtLH( lightPos, frustumCenter, axis );

		XFormBoundingBox( &frustumAABB, frustumAABB, lightView );
		XFormBoundingBox( &casterAABB, casterAABB, lightView );

		//  use a small fudge factor for the near plane, to avoid some minor clipping artifacts
		Math::Matrix44 lightProj;
		lightProj.OrthoOffCenterLH( frustumAABB.GetMin().x, frustumAABB.GetMax().x,
									frustumAABB.GetMin().y, frustumAABB.GetMax().y,
									casterAABB.GetMin().z, frustumAABB.GetMax().z );

		// 결과
		lightView.Multiply( view, lightView );
		m_LightViewProjection.Multiply( lightView, lightProj );
	}

	//================================================================
	/** Get Angle Between Light And EyeView
		@brief	these are the limits specified by the physical camera
					gamma is the "tilt angle" between the light and the view direction.
					(view 방향과 light 사이의 '경사각'(tilt angle) 이 gamma)
	*/
	//================================================================
	float LisPSM::GetAngleBetweenLightAndEyeView( CnCamera* pCamera )
	{
		Math::Matrix44 eyeView = pCamera->GetView();
		Math::Vector3 eyeDir(eyeView._13, eyeView._23, eyeView._33);
		Math::Vector3 lightDir = m_pLight->GetDirection();

		return lightDir.Dot(eyeDir);

		//return lightDir.x * eyeView._13 + 
		//	   lightDir.y * eyeView._23 +
		//	   lightDir.z * eyeView._33;
	}

	//================================================================
	/** Focussing Shadow Map
		@brief	Shadow 계산에 대한 적절한 Convex Body를 만든다.
					시야 절두체와 light Position(Directional light의 경우 무한대)
					의 convex hull 계산과 이 hull과 장면을 감싸는 것(일반적으로 BoundingBox)
					과의 교차에 의해 만들어 진다. 이 결과 Points들의 집합 Body B를 만든다.
	*/
	//================================================================
	void LisPSM::CalcFocusedLightVolumePoints( CnCamera* pCamera, PointArray& outBodyB )
	{
		// shadow caster/receiver 바운등 박스들에 대한 모든 point들과, frustum 극점들을 추가하여 convex body B를 생성한다.
		Math::Matrix44 projection = pCamera->GetProjection();
		Math::Frustum eyeFrustum( &projection );

		Math::Vector3* extrema = eyeFrustum.GetExtrema();
		for (int i=0; i<8; ++i)
			outBodyB.push_back( extrema[i] );

		AABBIter biend = m_ShadowCasterPoints.end();
		for (AABBIter bi=m_ShadowCasterPoints.begin(); bi<biend; ++bi)
		{
			const Math::Vector3* corners = (*bi).GetCorners();
			for (int i=0; i<8; ++i)
				outBodyB.push_back( corners[i] );
		}
	}

	//================================================================
	/** Light Space Perspective Shadow Mapping Matrix 만들기
		@brief	
	*/
	//================================================================
	void LisPSM::CalcLispSMMatrix( CnCamera* pCamera, PointArray& bodyB )
	{
		Math::Matrix44 lightSpaceBasis;
		GetLightSpaceBasis( pCamera, &lightSpaceBasis );

		// 모든 포인트들을 새로운 기저로 변환한다.
		unsigned int numBodyPoints = (unsigned int)bodyB.size();
		Math::Vector3::MultipleArray( &bodyB[0], sizeof(Math::Vector3),
								  &bodyB[0], sizeof(Math::Vector3),
								  &lightSpaceBasis, numBodyPoints );

		// 논문에 의하면, light Space에 대한 x 좌표로 변환된 viewpoint의 x좌표를 이용한다고 하는데, 이상한 듯..
		// 따라서, Y축에 대한 것처럼 bodyB의 x-mid 포인트를 사용한다.
		Math::AABB lightSpaceBox( &bodyB[0], numBodyPoints );

		float zNear, zFar;
		GetPerspectiveFrustumRange( pCamera, &zNear, &zFar );

		// 3. Choosing The free Parameter N
		// 더 정밀한 결과를 얻기 위해, near/far의 실제 거리와 최적화된 near/far 거리의 평균을 사용한다.
		float Nopt = ChooseFreeParemter( pCamera, zNear, zFar );

		Math::Vector3 lightSpaceOrigin = lightSpaceBox.GetCenter();
		lightSpaceOrigin.z = lightSpaceBox.GetMin().z - Nopt;

		// xlate all points in lsBodyB, to match the new lightspace origin, and compute the fov and aspect ratio
		float maxX = 0.0f, maxY = 0.0f, maxZ = 0.0f;
		Math::Vector3 v;

		PointIter pi = bodyB.begin();
		while (pi != bodyB.end())
		{
			v = *pi++ - lightSpaceOrigin;
			maxX = max( maxX, Math::Abs(v.x/v.z) );
			maxY = max( maxY, Math::Abs(v.y/v.z) );
			maxZ = max( maxZ, v.z );
		}

		float fovX = Math::ATan(maxX);
		float fovY = Math::ATan(maxY);

		Math::Matrix44 translate;
		translate.Translation( -lightSpaceOrigin.x, -lightSpaceOrigin.y, -lightSpaceOrigin.z );

		Math::Matrix44 lightSpaceProj;
		lightSpaceProj.PerspLH( 2.0f*maxX*Nopt, 2.0f*maxY*Nopt, Nopt, maxZ );

		lightSpaceBasis.Multiply( lightSpaceBasis, translate );
		lightSpaceBasis.Multiply( lightSpaceBasis, lightSpaceProj );

		// 투영 이후의 큐브 전체를 회전한다. 그래서, 그림자맵이 Y축으로 바라보도록 한다.
		// (x-z 평면이 그림자 맵이 그려질 영역)
		static const Math::Matrix44 permute( 1.f,  0.f,  0.f, 0.f,
											 0.f,  0.f, -1.f, 0.f,
											 0.f,  1.f,  0.f, 0.f,
											 0.f, -0.5f, 1.5f, 1.f );
		lightSpaceBasis.Multiply( lightSpaceBasis, permute );

		Math::Matrix44 ortho;
		ortho.OrthoLH( 2.0f, 1.0f, 0.5f, 2.5f );
		lightSpaceBasis.Multiply( lightSpaceBasis, ortho );

		ApplyUnitCubeClip( &lightSpaceBasis );

		// eyeView * lightSpace 변환 행렬 * 
		// lightSpaceBasis = lightSpaceBasis * translate * lightSpaceproj * permute * clip
		m_LightViewProjection.Multiply( pCamera->GetView(), lightSpaceBasis );
	}

	//================================================================
	/** light Space 기저 행렬을 구한다.
	*/
	//================================================================
	void LisPSM::GetLightSpaceBasis( CnCamera* pCamera, Math::Matrix44* output )
	{
		// eye Vector는 eye 공간에서 항상 -Z
		static const Math::Vector3 eyeVector( 0.0f, 0.0f, -1.0f );	

		// 문서에 설면된 알고리즘을 사용할 "light-space" 기저를 계산한다.
		// note: bodyB가 eye space안에 정의 되어 있기 때문에, 모든 벡터들은 eye Space로 정의되어야 한다.
		Math::Vector3 leftVector, upVector, viewVector;

		// lightDir은 eye Space에 정의되어 있다. 그래서 eye Space로 변환한다.
		upVector.MultiplyNormal( m_pLight->GetDirection(), pCamera->GetView() );

		// note: lightDir point들은 장면에서부터, 그래서, up 방향을 이미 음의 방향("negative")이다.
		// 따라서, 다시 반대 방향으로 할 필요가 없다.
		//  note: lightDir points away from the scene, so it is already the "negative" up direction;
		//  no need to re-negate it.
		leftVector.Cross( upVector, eyeVector );
		leftVector.Normalize();

		// 왜 이렇게 되지???
		viewVector.Cross( upVector, leftVector );
		viewVector.Normalize();

		// view->light space
		output->_11 = leftVector.x; output->_12 = upVector.x; output->_13 = viewVector.x; output->_14 = 0.0f;
		output->_21 = leftVector.y; output->_22 = upVector.y; output->_23 = viewVector.y; output->_24 = 0.0f;
		output->_31 = leftVector.z; output->_32 = upVector.z; output->_33 = viewVector.z; output->_34 = 0.0f;
		output->_41 = 0.0f;			output->_42 = 0.0f;		  output->_43 = 0.0f;		  output->_44 = 1.0f;
	}

	//================================================================
	/** Choosing The free Parameter N
	*/
	//================================================================
	float LisPSM::ChooseFreeParemter( CnCamera* pCamera, float fFrustumNear, float fFrustumFar )
	{
		// 더 정밀한 결과를 얻기 위해, near/far의 실제 거리와 최적화된 near/far 거리의 평균을 사용한다.

		float sinGamma = Math::Sqrt( 1.0f - m_fCosGamma*m_fCosGamma );

		float Nopt0 = fFrustumNear + Math::Sqrt( fFrustumNear*fFrustumFar );
		float Nopt1 = pCamera->GetNear() + Math::Sqrt( pCamera->GetNear()*pCamera->GetFar() );

		float Nopt = (Nopt0+Nopt1) / (2.0f*sinGamma);
		//  add a constant bias, to guarantee some minimum distance between the projection point and the near plane
		Nopt += 0.1f;

		float NoptWeight = 1.0f;

		//  now use the weighting to scale between 0.1 and the computed Nopt
		Nopt = 0.1f + NoptWeight*(Nopt-0.1f);
		return Nopt;
	}

	//================================================================
	/** Unit Cube Clipping을 적용
		@brief	aliasing 문제를 해결하기 위해서, unit cube clip을 적용한다.
	*/
	//================================================================
	void LisPSM::ApplyUnitCubeClip( Math::Matrix44* lightSpaceBasis )
	{
		PointArray receiverPoints;

		unsigned int numPoints = (unsigned int)m_ShadowReceiverPoints.size();
		receiverPoints.reserve( numPoints * 8 );

		AABBIter biend = m_ShadowReceiverPoints.end();
		for (AABBIter bi=m_ShadowReceiverPoints.begin(); bi != biend; ++bi)
		{
			const Math::Vector3* coners = (*bi).GetCorners();
			for (int i=0; i<8; ++i)
				receiverPoints.push_back( coners[i] );
		}

		numPoints = (unsigned int)receiverPoints.size();
		Math::Vector3::MultipleArray( &receiverPoints[0], sizeof(Math::Vector3),
									  &receiverPoints[0], sizeof(Math::Vector3),
									  lightSpaceBasis, numPoints );

		Math::AABB receiverBox( &receiverPoints[0], numPoints );
		Math::Vector3 min = receiverBox.GetMin();
		Math::Vector3 max = receiverBox.GetMax();

		max.x = Math::Min( 1.0f, max.x );
		min.x = Math::Max(-1.0f, min.x );
		max.y = Math::Min( 1.0f, max.y );
		min.y = Math::Max(-1.0f, min.y );

		float boxWidth = max.x - min.x;
		float boxHeight = max.y - min.y;

		if (!ALMOST_ZERO(boxWidth) && !ALMOST_ZERO(boxHeight))
		{
			float boxX = (max.x + min.x) * 0.5f;
			float boxY = (max.y + min.y) * 0.5f;

			Math::Matrix44 clipMatrix(	2.f/boxWidth,		0.f,				0.f, 0.f,
										0.f,				2.f/boxHeight,		0.f, 0.f,
										0.f,				0.f,				1.f, 0.f,
									   -2.f*boxX/boxWidth, -2.f*boxY/boxHeight, 0.f, 1.f );

			lightSpaceBasis->Multiply( *lightSpaceBasis, clipMatrix );
		}
	}
}
}