//================================================================
// File:           : CnBuildLisPSM.h
// Original Author : changhee
// Creation Date   : 2008. 2. 11
//================================================================
#pragma once

#include "CnBuildSSM.h"

namespace Cindy
{
namespace ShadowMap
{
	//================================================================
	/** Light Space Projection Shadow Map Projector
		@author    changhee
		@since     2008. 2. 11
		@remarks   라이트 공간 투영 그림자 맵을 만들어 주는 투영기

		http://www.gpgstudy.com/forum/viewtopic.php?t=4984&sid=0b91746588df91b838f80579075d8c42
		http://www.cg.tuwien.ac.at/research/vr/lispsm/
	*/
	//================================================================
	class CN_DLL LisPSM : public ShadowMap::SSM
	{
		__DeclareClass(LisPSM);
	public:
		LisPSM();
		virtual ~LisPSM();

	protected:
		typedef std::vector<Math::Vector3>	PointArray;
		typedef PointArray::iterator		PointIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		float		m_fCosGamma;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual void	BuildSMMatrix( CnCamera* pCamera ) override;
		void			BuildUniformSMProjectionMatrix( CnCamera* pCamera );

		float			GetAngleBetweenLightAndEyeView( CnCamera* pCamera );

		virtual void	GetLightSpaceBasis( CnCamera* pCamera, Math::Matrix44* output );

		void			ApplyUnitCubeClip( Math::Matrix44* lightSpaceBasis );

	private:
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void	CalcFocusedLightVolumePoints( CnCamera* pCamera, PointArray& outBodyB );
		void	CalcLispSMMatrix( CnCamera* pCamera, PointArray& bodyB );

		float	ChooseFreeParemter( CnCamera* pCamera, float fFrustumNear, float fFrustumFar );
	};
}
}