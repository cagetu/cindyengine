//================================================================
// File:           : CnBuildPSSM.h
// Original Author : changhee
// Creation Date   : 2010. 1. 18
//================================================================
#pragma once

#include "CnBuildSSM.h"
#include "Math/CnMathDefines.h"

namespace Cindy
{
namespace ShadowMap
{
	//================================================================
	/** Parallel Split Shadow Map
		@author    changhee
		@since     2010. 1. 18
		@remarks   PSSM ������
	*/
	//================================================================
	class CN_DLL PSSM : public ShadowMap::SSM
	{
		__DeclareClass(PSSM);
	public:
		PSSM();
		virtual ~PSSM();

		void	Initialize() override;

		void	SetShaderVariables( const Math::Matrix44& WorldTM ) override;

		void	SetFrame( const Ptr<CnFrame>& Frame ) override;

		void	Debug() override;
	protected:
		static const ushort	NumSplits = 4;
		static const ushort NumSplitDistances = NumSplits + 1;
		static const ushort	NumCorners = 8;
		static const float lightFov;
		static const float lightNear;
		static const float lightFarMax;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		void	BuildSMMatrix( CnCamera* pCamera ) override;
		void	Project( CnCamera* pCamera ) override;

		void	AdjustCameraPlanes( CnCamera* pCamera );

		void	CalculateSplitDistances();

		void	CalculateFrustumCorners( ushort SplitIndex,
										 const Math::Vector3& camPos,
										 const Math::Vector3& camDir,
										 const Math::Vector3& camRight,
										 const Math::Vector3& camUp,
										 float aspect,
										 float fov );
		void	CalculateTransforms( ushort SplitIndex );

		void	CalculateCropMatrix( Math::Matrix44& Output, float maxX, float maxY, float minX, float minY );

		void	Debug( ushort SplitIndex );

		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		float				m_CameraNear;
		float				m_CameraFar;

		float				m_SplitDistances[NumSplitDistances];
		float				m_SplitSchemeLambda;

		Math::Vector3		m_FrustumCorners[8];
		Math::Vector3		m_FrustumCenter;
		float				m_FrustumScale;

		Math::Matrix44		m_LightViews[NumSplits];
		Math::Matrix44		m_LightViewProjs[NumSplits];
		TexPtr				m_SplitShadowMaps[NumSplits];

		Ptr<CnVisibleSet>	m_VisibleMeshes;

	};
} // namespace ShadowMap
} // namespace Cindy