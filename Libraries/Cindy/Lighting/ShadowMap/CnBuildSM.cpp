//================================================================
// File:               : CnBuildSM.cpp
// Related Header File : CnBuildSM.h
// Original Author     : changhee
// Creation Date       : 2008. 2. 4
//================================================================
#include "Cindy.h"
#include "CnBuildSM.h"
#include "Lighting/CnLight.h"

namespace Cindy
{
namespace ShadowMap
{
	//============================================================================
	__ImplementRtti( Cindy, Builder, CnObject );

	Builder::Builder()
		: m_pLight(0)
	{
	}
	Builder::~Builder()
	{
	}

	//----------------------------------------------------------------
	/**	@brief	투영의 주체가 되는 라이트
	*/
	void Builder::SetLight( CnLight* pLight )
	{
		m_pLight = pLight;
	}
}
}