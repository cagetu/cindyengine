//================================================================
// File:               : CnSMProjector.cpp
// Related Header File : CnSMProjector.h
// Original Author     : changhee
// Creation Date       : 2008. 2. 4
//================================================================
#include "Cindy.h"
#include "CnBuildSSM.h"
#include "Math/CnSphere.h"
#include "Scene/CnCamera.h"
#include "Scene/CnSceneNode.h"
#include "Scene/CnVisibleResolver.h"
#include "Lighting/CnLight.h"
#include "Material/CnTextureManager.h"
#include "Material/CnShader.h"
#include "Graphics/Dx9/CnDx9Texture.h"
#include "Graphics/Dx9/CnDx9Renderer.h"
#include "Graphics/Base/CnRenderTexture.h"
#include "Graphics/CnTransformDevice.h"

#define TEST_SHADOW_FRAME

namespace Cindy
{
namespace ShadowMap
{
	const float	SSM::ZNEAR_MIN = 1.0f;
	const float	SSM::ZFAR_MAX = 800.0f;
	//================================================================
	__ImplementClass( Cindy, SSM, ShadowMap::Builder );
	//----------------------------------------------------------------
	//	Class SSM
	//----------------------------------------------------------------
	SSM::SSM() 
		: ShadowMap::Builder()
	{
	}
	SSM::~SSM()
	{
		m_VisibleSet = 0;
	}

	//----------------------------------------------------------------
	void SSM::Initialize()
	{
		m_VisibleSet = CnVisibleSet::Create();
	}

	//----------------------------------------------------------------
	void SSM::Reset()
	{
		m_ShadowCasterPoints.clear();
		m_ShadowReceiverPoints.clear();

		m_VisibleSet->Clear();

		m_ReceiverAABB.SetEmpty();
	}

	//----------------------------------------------------------------
	/** Shadow Caster를 추가한다.
		@brief	화면에 보이는 오브젝트 중 caster로 사용할 녀석들을 등록한다.
	*/
	//----------------------------------------------------------------
	void SSM::Add( CnSceneEntity* pEntity, CnCamera* pCamera )
	{
		CnMesh* mesh = DynamicCast<CnMesh>( pEntity );
		if (mesh)
		{
			Math::AABB aabb;
			pEntity->GetAABB( aabb );

			// aabb를 view 공간으로..
			Math::AABB viewSpaceAABB;
			XFormBoundingBox( &viewSpaceAABB, aabb, pCamera->GetView() );

			//mesh->SetShadowMap( this );

			if (mesh->IsCastShadow())
			{
				m_ShadowCasterPoints.push_back( viewSpaceAABB );
				m_VisibleSet->Register( mesh );
			}

			//m_ShadowCasters.push_back( mesh );
			//m_ShadowCasterPoints.push_back( aabb );

			if (mesh->IsEnableReceiveShadow())
			{
				m_ShadowReceiverPoints.push_back( viewSpaceAABB );
				mesh->SetReceiveShadow( true );

				m_ReceiverAABB.Merge( aabb );
			}
		}
	}

	//----------------------------------------------------------------
	/** Shadow Caster를 테스트 후 추가한다.
		@brief	화면에 보이지 않는 객체이지만, Caster로 사용할 것인지를 선정한다.
	*/
	//----------------------------------------------------------------
	void SSM::Test( CnSceneEntity* pEntity, CnCamera* pCamera )
	{
		CnMesh* mesh = DynamicCast<CnMesh>( pEntity );
		if (mesh)
		{
			mesh->SetReceiveShadow( false );

			if (false == mesh->IsCastShadow())
				return;

			Math::AABB aabb;
			pEntity->GetAABB( aabb );

			Math::Vector3 sweepDir = -m_pLight->GetDirection();

			Math::Sphere sphere( &aabb );
			if (pCamera->IsCulled( sphere, sweepDir ))
				return;

			// aabb를 view 공간으로..
			Math::AABB viewSpaceAABB;
			XFormBoundingBox( &viewSpaceAABB, aabb, pCamera->GetView() );

			m_ShadowCasterPoints.push_back( viewSpaceAABB );
			m_VisibleSet->Register( mesh );

			if (mesh->IsEnableReceiveShadow())
			{
				m_ShadowReceiverPoints.push_back( viewSpaceAABB );
				mesh->SetReceiveShadow( true );

				m_ReceiverAABB.Merge( aabb );
			}
		}
	}

	//----------------------------------------------------------------
	/**	그림자맵 매트릭스를 구한다.
	*/
	void SSM::BuildSMMatrix( CnCamera* pCamera )
	{
		Math::Vector3 lightLookAt(0.0f, 0.0f, 0.0f);
		Math::Vector3 lightUp(0.0f, 1.0f, 0.0f);
		Math::Vector3 lightPosition = m_pLight->GetParent()->GetWorldPosition();

		const Math::Vector3 offset(3.0f, 3.0f, 3.0f);
		m_ReceiverAABB.SetExtents( m_ReceiverAABB.GetMin() - offset, m_ReceiverAABB.GetMax() + offset );

		lightLookAt = m_ReceiverAABB.GetCenter();

		Math::Matrix44 lightView;
		lightView.LookAtLH( lightPosition, lightLookAt, lightUp );

		Math::Vector4 out;
		float maxX = 0.0f;
		float maxY = 0.0f;
		const Math::Vector3* corners = m_ReceiverAABB.GetCorners();
		for (int i=0; i < 8; ++i)
		{
			out.Multiply( corners[i], lightView );

			if(Math::Abs(out.x/out.z) > maxX)
				maxX = Math::Abs(out.x/out.z);

			if(Math::Abs(out.y/out.z) > maxY)
				maxY = Math::Abs(out.y/out.z);
		}

		float zNear, zFar;
		GetPerspectiveFrustumRange( pCamera, &zNear, &zFar );

		Math::Matrix44 lightProj;
		lightProj.PerspFovLH( pCamera->GetFovY(), pCamera->GetAspect(), zNear, zFar );

		lightProj.m[0][0] = 0.98f/maxX;
		lightProj.m[1][1] = 0.98f/maxY;

		m_LightViewProjection = lightView * lightProj;
	}

	//----------------------------------------------------------------
	/** 원근 변환된 프러스텀의 near, far를 구한다.
	*/
	//----------------------------------------------------------------
	void SSM::GetPerspectiveFrustumRange( CnCamera* pCamera, float* outNear, float* outFar )
	{
		float min_z = 1e32f;
		float max_z = 0.0f;

		unsigned int numPoints = (unsigned int)m_ShadowReceiverPoints.size();
		for (uint i=0; i< numPoints; ++i)
		{
			min_z = min( min_z, m_ShadowReceiverPoints[i].GetMin().z );
			max_z = max( max_z, m_ShadowReceiverPoints[i].GetMax().z );
		}

		*outNear = max( ZNEAR_MIN, min_z );
		*outFar = min( ZFAR_MAX, max_z );
	}

	//----------------------------------------------------------------
	/** 변환된 AABB
	*/
	//----------------------------------------------------------------
	void SSM::XFormBoundingBox( Math::AABB* result, const Math::AABB& src, const Math::Matrix44& xForm )
	{
		Math::AABB output;
		//result->SetEmpty();

		Math::Vector3 n;
		const Math::Vector3* aCorners = src.GetCorners();
		for (int i=0; i<8; ++i)
		{
			n.Multiply( aCorners[i], xForm );
			output.Merge( n );
		}

		*result = output;
	}

	//----------------------------------------------------------------
	/**
	*/
	//----------------------------------------------------------------
	void SSM::SetFrame( const Ptr<CnFrame>& Frame )
	{
		m_Frame = Frame;

		m_RenderTarget = static_cast<CnRenderTexture*>(RenderDevice->GetRenderTarget(L"FinalSM"));
		if (m_RenderTarget.IsValid())
		{
			m_spShadowMap = m_RenderTarget->GetTexture();

			float offsetX = 0.5f + (0.5f/m_spShadowMap->GetWidth());
			float offsetY = 0.5f + (0.5f/m_spShadowMap->GetHeight());
			float range = 1.0f;
			const Math::Matrix44 texGen( 0.5f,	  0.0f,	   0.0f,  0.0f,
										 0.0f,	 -0.5f,    0.0f,  0.0f,
										 0.0f,	  0.0f,    range, 0.0f,
										 offsetX, offsetY, 0.0f,  1.0f );
			//static const Math::Matrix44 texGen( 0.5f,  0.0f, 0.0f, 0.0f,
			//									0.0f, -0.5f, 0.0f, 0.0f,
			//									0.0f,  0.0f, 1.0f, 0.0f,
			//									0.5f,  0.5f, 0.0f, 1.0f );

			m_TexMatrix = texGen;
		}
	}

	//----------------------------------------------------------------
	/**	그림자 맵 관련 셰이더 변수 설정
	*/
	//----------------------------------------------------------------
	void SSM::SetShaderVariables( const Math::Matrix44& WorldTM )
	{
		if (IsEmpty())
			return;
		if (m_spShadowMap.IsNull())
			return;

		CnShaderEffect* shader = RenderDevice->GetActiveShader();

		ShaderHandle handle = NULL;

		// ModelViewProjection Texture
		if (handle = shader->GetParameter( ShaderState::ModelLightProjTexture ))
		{
			Math::Matrix44 modelViewProj, modelViewProjTex;
			modelViewProj.Multiply( WorldTM, m_LightViewProjection );
			modelViewProjTex.Multiply( modelViewProj, m_TexMatrix );

			shader->SetMatrix( handle, modelViewProjTex );
		}

		if (handle = shader->GetParameter( ShaderState::ShadowMap ))
		{
			shader->SetTexture( handle, m_spShadowMap );
		}
	}

	//----------------------------------------------------------------
	/** Build
		@brief	그림자 맵 만들기
	*/
	//----------------------------------------------------------------
	void SSM::Build( CnCamera* pCamera )
	{
		if (m_ShadowCasterPoints.empty())
			return;
		if (m_ShadowReceiverPoints.empty())
			return;

		BuildSMMatrix( pCamera );
		Project( pCamera );
	}

	//----------------------------------------------------------------
	/** Project
		@brief	그림자맵에 Caster들을 그린다.
	*/
	//----------------------------------------------------------------
	void SSM::Project( CnCamera* pCamera )
	{
		TransformDevice->SetViewProjTransform( m_LightViewProjection );

		if (m_VisibleSet->IsEmpty())
			int a = 1;

		/// 
		Ptr<CnVisibleSet> tmp = VisibleResolver->GetVisibleSet();
		VisibleResolver->SetVisibleSet( m_VisibleSet );

#ifdef TEST_SHADOW_FRAME
		if (m_Frame.IsValid())
		{
			m_Frame->Run();
		}
#else
		RenderDevice->OpenSurface(0);
		{
			LPDIRECT3DSURFACE9 surface = 0;
			m_spShadowMap->GetSurface( &surface );
			RenderDevice->SetSurface( 0, surface );

			LPDIRECT3DSURFACE9 depthStencilSurface = 0;
			m_spShadowMap->GetDepthStencil( &depthStencilSurface );
			if (depthStencilSurface)
			{
				RenderDevice->SetDepthStencilSurface( depthStencilSurface );
			}

			Render( pCamera );
		}
		RenderDevice->CloseSurface(0);

		//D3DXSaveSurfaceToFile( L"TestShadowMap.bmp", D3DXIFF_BMP, d3dTex->GetSurface(), NULL, NULL );
#endif
		CnVisibleResolver::Instance()->SetVisibleSet( tmp );
	}

	//----------------------------------------------------------------
	/** Render
		@brief	Caster들을 랜더링한다.
	*/
	//----------------------------------------------------------------
	void SSM::Render( CnCamera* pCamera )
	{
		CnColor backGround( 1.0f, 1.0f, 1.0f, 0.0f );
		if (RenderDevice->BeginScene( IRenderer::CLEAR_TARGET | IRenderer::CLEAR_ZBUFFER, backGround.GetRGBA(), 1.0f, 0 ))
		{
			LPDIRECT3DDEVICE9 device;
			RenderDevice->GetCurrentDevice( &device );

			//device->SetRenderState( D3DRS_LIGHTING, FALSE );
			//device->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
			//device->SetRenderState( D3DRS_ZENABLE, D3DZB_TRUE );
			////device->SetRenderState( D3DRS_COLORWRITEENABLE, 0 );

			//device->SetRenderState( D3DRS_ZFUNC, D3DCMP_LESSEQUAL );
			//device->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );

			//float depthBias = 4.0f/16777215.f;
			//float fBiasSlope = 1.0f;

			//device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
			//device->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 1);
			//device->SetTextureStageState(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_PROJECTED);

			//device->SetRenderState( D3DRS_DEPTHBIAS, *(DWORD*)&depthBias );
			//device->SetRenderState( D3DRS_SLOPESCALEDEPTHBIAS, *(DWORD*)&fBiasSlope);

			//m_RenderQueue.Draw( pCamera );
		}
		RenderDevice->EndScene();
	}

	//----------------------------------------------------------------
	void SSM::Debug()
	{
		if (m_ShadowCasterPoints.empty())
			return;
		if (m_ShadowReceiverPoints.empty())
			return;

		// Debug..
		static const float vertex2D[4][4+2] = 
		{
			{   0,  0,0,1, 0,0 },
			{ 128,  0,0,1, 1,0 },
			{ 128,128,0,1, 1,1 },
			{   0,128,0,1, 0,1 },
		};

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		device->SetRenderState( D3DRS_LIGHTING, FALSE );

		device->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
		device->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE);
		device->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
		device->SetRenderState( D3DRS_ZENABLE, D3DZB_TRUE );
		device->SetRenderState( D3DRS_ZFUNC, D3DCMP_ALWAYS );
		device->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );

		device->SetSamplerState( 0, D3DSAMP_MIPFILTER, D3DTEXF_POINT );
		device->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_POINT );
		device->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_POINT );

		D3DXMATRIX mIdentity;
		D3DXMatrixIdentity(&mIdentity);
		device->SetTransform(D3DTS_PROJECTION, &mIdentity);
		device->SetTransform(D3DTS_WORLD, &mIdentity);
		device->SetTransform(D3DTS_VIEW, &mIdentity);

		LPDIRECT3DTEXTURE9 tex = NULL;


#ifdef TEST_SHADOW_FRAME
		//m_spShadowMap = TextureMgr->Get(L"FinalSM");
#endif
		if (m_spShadowMap.IsValid())
		{
			m_spShadowMap->GetTexture( &tex );
		}

		device->SetTexture( 0, tex );
		device->SetTexture( 1, NULL );
		device->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
		device->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_SELECTARG1 );

		device->SetPixelShader(NULL);
		device->SetVertexShader(NULL);
		device->SetFVF( D3DFVF_XYZRHW|D3DFVF_TEX1 );

		device->DrawPrimitiveUP( D3DPT_TRIANGLEFAN, 2, vertex2D, sizeof(float)*(4+2) );
	}

	//============================================================================
	//	Struct BoundingCone
	//============================================================================
	SSM::BoundingCone::BoundingCone()
		: direction(0.0f, 0.0f, 1.0f)
		, apex(0.0f, 0.0f, 0.0f )
		, fovX(0.0f)
		, fovY(0.0f)
		, fNear(0.0f)
		, fFar(1.0f)
	{
	}
	//----------------------------------------------------------------
	SSM::BoundingCone::BoundingCone( const AABBArray* boxes,
									 const Math::Matrix44* projection,
									 const Math::Vector3* _apex )
		: apex( *_apex )
	{
		const Math::Vector3 negZAxis(0.0f, 0.0f, -1.0f);

		unsigned int size = (unsigned int)boxes->size();

		if (0 == size)
		{
			direction = negZAxis;
			fovX = fovY = 0.0f;
			lookAt.Identity();
		}
		else
		{
			//  compute a tight bounding sphere for the vertices of the bounding boxes.
			//  the vector from the apex to the center of the sphere is the optimized view direction
			//  start by xforming all points to post-projective space
			std::vector<Math::Vector3> points;
			points.reserve( size*8 );

			unsigned int i, j;
			for (i=0; i<size; ++i)
			{
				const Math::Vector3* corners = (*boxes)[i].GetCorners();
				for (j=0; j<8; ++j)
				{
					Math::Vector3 newPos;
					newPos.Multiply( corners[j], *projection );

					points.push_back( newPos );
				}
			}

			// 최소의 바운딩 구를 구한다.
			unsigned int numPoints = (unsigned int)points.size();
			Math::Sphere sphere( &points[0], numPoints );

			float min_costTheta = 1.0f;

			direction = sphere.center - apex;
			direction.Normalize();

			Math::Vector3 axis = Math::Vector3::UNIT_Y;
			if (fabsf( axis.Dot( direction ) ) > 0.99f)
				axis = Math::Vector3::UNIT_Z;

			lookAt.LookAtLH( apex, (apex+direction), axis );

			fNear = 1e32f;
			fFar = 0.f;

			float maxX = 0.0f;
			float maxY = 0.0f;

			unsigned int pointSize = (unsigned int)points.size();
			for (i=0; i<pointSize; ++i)
			{
				Math::Vector3 tmp;
				tmp.Multiply( points[i], lookAt );

				maxX = max( maxX, fabsf(tmp.x/tmp.z) );
				maxY = max( maxY, fabsf(tmp.y/tmp.z) );

				fNear = min(fNear, tmp.z);
				fFar = max(fFar, tmp.z);
			}

			fovX = atanf(maxX);
			fovY = atanf(maxY);
		} // if-else
	}

	//----------------------------------------------------------------
	SSM::BoundingCone::BoundingCone( const SSM::AABBArray *boxes,
										   const Math::Matrix44 *projection,
										   const Math::Vector3 *_apex, const Math::Vector3 *_direction )
	   : apex( *_apex )
	   , direction( *_direction )
	{
		direction.Normalize();

		Math::Vector3 axis = Math::Vector3::UNIT_Y;
		if ( fabsf( axis.Dot( direction ) )>0.99f )
			axis = Math::Vector3::UNIT_Z;

		lookAt.LookAtLH( apex, (apex+direction), axis );

		fNear = 1e32f;
		fFar = 0.f;

		float maxX = 0.0f;
		float maxY = 0.0f;

		Math::Matrix44 concatMatrix;
		concatMatrix.Multiply( *projection, lookAt );

		Math::Vector3 tmp;

		unsigned int boxSize = (unsigned int)boxes->size();
		for (unsigned int i=0; i<boxSize; ++i)
		{
			const Math::Vector3* corners = (*boxes)[i].GetCorners();
			for (unsigned int j=0; j<8; ++j)
			{
				tmp.Multiply( corners[j], concatMatrix );

				maxX = max( maxX, fabsf(tmp.x/tmp.z) );
				maxY = max( maxY, fabsf(tmp.y/tmp.z) );

				fNear = min(fNear, tmp.z);
				fFar = max(fFar, tmp.z);
			}
		}

		fovX = atanf(maxX);
		fovY = atanf(maxY);
	}

}
}
