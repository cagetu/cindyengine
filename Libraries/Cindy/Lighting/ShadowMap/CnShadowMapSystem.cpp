//================================================================
// File:               : CnShadowMapSystem.cpp
// Related Header File : CnShadowMapSystem.h
// Original Author     : changhee
// Creation Date       : 2009. 12. 7
//================================================================
#include "Cindy.h"
#include "CnShadowMapSystem.h"
#include "Scene/CnCamera.h"
#include "Geometry/CnMesh.h"

namespace Cindy
{
	//================================================================
	// Class ShadowMapSystem
	//================================================================
	__ImplementSingleton(CnShadowMapSystem);
	/// Const/Dest
	CnShadowMapSystem::CnShadowMapSystem()
		: m_GlobalShadowMapper(0)
	{
		__ConstructSingleton;
	}
	CnShadowMapSystem::~CnShadowMapSystem()
	{
		__DestructSingleton;
	}

	//----------------------------------------------------------------
	/**
	*/
	void
	CnShadowMapSystem::SetGlobalShadowMapper( CnShadowMappable* Mapper )
	{
		m_GlobalShadowMapper = Mapper;
	}

	//----------------------------------------------------------------
	/**
	*/
	CnShadowMappable*
	CnShadowMapSystem::GetGlobalShadowMapper() const
	{
		return m_GlobalShadowMapper;
	}

	////----------------------------------------------------------------
	///**
	//*/
	//const TexPtr&
	//CnShadowMapSystem::GetGlobalShadowMap() const
	//{
	//	return m_GlobalShadowMapper->GetShadowBuilder()->GetTexture();
	//}

	////----------------------------------------------------------------
	///**
	//*/
	//void
	//CnShadowMapSystem::GetGlobalShadowTransform( Math::Matrix44* Transform )
	//{
	//	m_GlobalShadowMapper->GetShadowBuilder()->GetProjTM( Transform );
	//}
}