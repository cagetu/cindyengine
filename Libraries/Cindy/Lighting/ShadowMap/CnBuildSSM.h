//================================================================
// File:           : CnBuildSSM.h
// Original Author : changhee
// Creation Date   : 2008. 2. 4
//================================================================
#pragma once

#include "CnBuildSM.h"
#include "Geometry/CnMesh.h"
#include "Scene/CnVisibleSet.h"

namespace Cindy
{
namespace ShadowMap
{
	//================================================================
	/** Standard Shadow Map Projector
		@author    changhee
		@since     2008. 2. 4
		@remarks   그림자 맵을 만들어 주는 투영기
	*/
	//================================================================
	class CN_DLL SSM : public ShadowMap::Builder
	{
		__DeclareClass(SSM);
	public:
		SSM();
		virtual ~SSM();

		virtual void			Initialize() override;
		virtual void			Reset() override;

		// 
		virtual void			Add( CnSceneEntity* pEntity, CnCamera* pCamera ) override;
		virtual void			Test( CnSceneEntity* pEntity, CnCamera* pCamera ) override;

		virtual void			Build( CnCamera* pCamera ) override;
		virtual void			Debug() override;

		// Frame
		virtual void			SetFrame( const Ptr<CnFrame>& Frame ) override;
		// Shader Variables
		virtual void			SetShaderVariables( const Math::Matrix44& WorldTM ) override;

		virtual bool			IsEmpty() const override;

	protected:
		static const float	ZNEAR_MIN;
		static const float	ZFAR_MAX;

		typedef std::vector<Math::AABB>		AABBArray;
		typedef AABBArray::iterator			AABBIter;

		struct BoundingCone
		{
			Math::Vector3	direction;
			Math::Vector3	apex;
			Math::Matrix44	lookAt;
			float			fovY;
			float			fovX;
			float			fNear;
			float			fFar;

			BoundingCone();
			BoundingCone( const AABBArray* boxes, const Math::Matrix44* projection, const Math::Vector3* _apex );
			BoundingCone( const AABBArray* boxes, const Math::Matrix44* projection, const Math::Vector3* _apex, const Math::Vector3* _direction );
			BoundingCone( const vector<Math::Vector3>* points, const Math::Vector3* apex, const Math::Vector3* direction );
		};

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		AABBArray				m_ShadowReceiverPoints;
		AABBArray				m_ShadowCasterPoints;

		TexPtr					m_spShadowMap;
		Ptr<CnRenderTexture>	m_RenderTarget;

		Math::Matrix44			m_LightViewProjection;
		Math::Matrix44			m_TexMatrix;

		Ptr<CnVisibleSet>		m_VisibleSet;
		Ptr<CnFrame>			m_Frame;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void			XFormBoundingBox( Math::AABB* result, const Math::AABB& src, const Math::Matrix44& xForm );
		void			GetPerspectiveFrustumRange( CnCamera* pCamera, float* outNear, float* outFar );

		virtual void	BuildSMMatrix( CnCamera* pCamera );
		virtual void	Project( CnCamera* pCamera );

		void			Render( CnCamera* pCamera );

	private:
		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		Math::AABB		m_ReceiverAABB;
	};

	//----------------------------------------------------------------------------
	/**
	*/
	inline bool
	SSM::IsEmpty() const
	{
		return m_ShadowCasterPoints.empty();
	}

} // namespace ShadowMap
} // namespace Cindy