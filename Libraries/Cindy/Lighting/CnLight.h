//================================================================
// File:           : CnLight.h
// Original Author : changhee
// Creation Date   : 2007. 1. 25
//================================================================
#ifndef __CN_LIGHT_H__
#define __CN_LIGHT_H__

#include "Math/CnVector3.h"
#include "Util/CnColor.h"
#include "Scene/CnSceneEntity.h"
#include "Lighting/ShadowMap/CnShadowMappable.h"

namespace Cindy
{
	class CnCamera;

	//================================================================
	/** Light Class
	    @author    changhee
		@since     2007. 1. 25
		@remarks   Light Ŭ����
	*/
	//================================================================
	class CN_DLL CnLight : public CnSceneEntity,
						   public CnShadowMappable
	{
		__DeclareClass(CnLight);
	public:
		typedef int		Type;

		static const Type	Global = 0;			// Sky(Global) Light
		static const Type	Ambient = 1;		// Global Ambient
		static const Type	Directional = 2;	// Directional Light
		static const Type	Point = 3;			// Point Light
		static const Type	Spot = 4;			// Spot Light

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		float			m_fDistance;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnLight( const CnString& strName = L"" );
		virtual ~CnLight();

		// light type
		void					SetType( Type nType );
		Type					GetType() const;

		// light color
		void					SetAmbient( const CnColor& rColor );
		const CnColor&			GetAmbient() const;

		void					SetDiffuse( const CnColor& rColor );
		const CnColor&			GetDiffuse() const;

		void					SetSpecular( const CnColor& rColor );
		const CnColor&			GetSpecular() const;

		// direction
		void					SetDirection( const Math::Vector3& vDirection );
		const Math::Vector3&	GetDirection() const;

		// attenuation
		void					SetAttenuation( float fAtten0, float fAtten1, float fAtten2 );
		const Math::Vector3&	GetAttenuation() const;

		// range
		void					SetRange( float fRange );
		float					GetRange() const;
		
		//
		void					SetIntensity(float intensity);
		float					GetIntensity() const;

		// falloff
		void					SetFallOff( float fValue );
		float					GetFallOff() const;	

		// cone
		void					SetConeAngle( float fInner, float fOuter );
		float					GetInnerConeAngle() const;
		float					GetOuterConeAngle() const;

		// Relation
		void					RelationTo( CnSceneEntity* pEntity, CnCamera* pCamera ) override;
		void					ClearRelation() override;

		// cull
		bool					Cull( CnCamera* pCamera ) override;
		//
		void					Apply( CnCamera* pCamera ) override;

		/// Update
		bool					Update( CnCamera* pCamera ) override;
		/// Draw
		void					Draw( CnCamera* pCamera ) override;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		Type			m_nType;

		CnColor			m_Ambient;
		CnColor			m_Diffuse;
		CnColor			m_Specular;

		float			m_Intensity;

		Math::Vector3	m_vPosition;
		Math::Vector3	m_vDirection;

		Math::Vector3	m_vAttenuation;
		float			m_fRange;

		float			m_fFallOff;
		float			m_fTheta;
		float			m_fPhi;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		// AABB
		void	GetAABB( Math::AABB& rOutput ) override;
	};

#include "CnLight.inl"
}

#endif	// __CN_LIGHT_H__