//================================================================
// File:               : CnLight.inl
// Related Header File : CnLight.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 25
//================================================================

//----------------------------------------------------------------
/** Type
    @remarks      Type 설정 및 얻어오기
	@param        none
	@return       none
*/
//----------------------------------------------------------------
inline
void CnLight::SetType( Type nType )
{
	m_nType = nType;
}
inline
CnLight::Type CnLight::GetType() const
{
	return m_nType;
}

//----------------------------------------------------------------
/** Set Ambient Color
    @remarks      주변광 색 지정
	@param        rColor : 색상
	@return       none
*/
//----------------------------------------------------------------
inline
void CnLight::SetAmbient( const CnColor& rColor )
{
	m_Ambient = rColor;
}

//----------------------------------------------------------------
/** Set Diffuse Color
    @remarks      Diffuse 색상 설정
	@param        rColor : 색상
	@return       none
*/
//----------------------------------------------------------------
inline
void CnLight::SetDiffuse( const CnColor& rColor )
{
	m_Diffuse = rColor;
}

//----------------------------------------------------------------
/** Set Specular Color
    @remarks      스펙큘러 색을 설정
	@param        rColor : 색상
	@return       none
*/
//----------------------------------------------------------------
inline
void CnLight::SetSpecular( const CnColor& rColor )
{
	m_Specular = rColor;
}

//----------------------------------------------------------------
/** Get Ambient Color
    @remarks      주변광 색상 얻기
	@param        none
	@return       const CnColor& : m_Ambient
*/
//----------------------------------------------------------------
inline
const CnColor& CnLight::GetAmbient() const
{
	return m_Ambient;
}

//----------------------------------------------------------------
/** Get Diffuse Color
    @remarks      디퓨즈 색을 얻기
	@param        none
	@return       const CnColor& : m_Diffuse
*/
//----------------------------------------------------------------
inline
const CnColor& CnLight::GetDiffuse() const
{
	return m_Diffuse;
}

//----------------------------------------------------------------
/** Get Specular Color
    @remarks      스펙큘러 색을 얻기
	@param        none
	@return       const CnColor& : m_Specular
*/
//----------------------------------------------------------------
inline
const CnColor& CnLight::GetSpecular() const
{
	return m_Specular;
}

inline
void CnLight::SetIntensity(float intensity)
{
	m_Intensity = intensity;
}

inline
float CnLight::GetIntensity() const
{
	return m_Intensity;
}

//================================================================
//	DirectionalLight
//================================================================
inline
void CnLight::SetDirection( const Math::Vector3& vDirection )
{
	m_vDirection = vDirection;
	m_vDirection.Normalize();
}

inline
const Math::Vector3& CnLight::GetDirection() const
{
	return m_vDirection;
}

//================================================================
//	PointLight/SpotLight
//================================================================
inline
void CnLight::SetAttenuation( float fAtten0, float fAtten1, float fAtten2 )
{
	m_vAttenuation.x = fAtten0;
	m_vAttenuation.y = fAtten1;
	m_vAttenuation.z = fAtten2;
}

inline
void CnLight::SetRange( float fRange )
{
	m_fRange = fRange;
}

inline
const Math::Vector3& CnLight::GetAttenuation() const
{
	return m_vAttenuation;
}

inline
float CnLight::GetRange() const
{
	return m_fRange;
}

inline
void CnLight::SetFallOff( float fValue )
{
	m_fFallOff = fValue;
}

inline
void CnLight::SetConeAngle( float fInner, float fOuter )
{
	m_fTheta = fInner;
	m_fPhi = fOuter;
}

inline
float CnLight::GetFallOff() const
{
	return m_fFallOff;
}

inline
float CnLight::GetInnerConeAngle() const
{
	return m_fTheta;
}

inline
float CnLight::GetOuterConeAngle() const
{
	return m_fPhi;
}