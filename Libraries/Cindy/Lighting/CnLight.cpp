//================================================================
// File:               : CnLight.cpp
// Related Header File : CnLight.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 25
//================================================================
#include "Cindy.h"
#include "CnLight.h"
#include "Scene/CnSceneNode.h"
#include "Scene/CnCamera.h"
#include "Geometry/CnMesh.h"

namespace Cindy
{
	//================================================================
	// Class Light
	//================================================================
	__ImplementClass(Cindy, CnLight, CnSceneEntity);
	/// Const/Dest
	CnLight::CnLight( const CnString& strName )
		: CnSceneEntity( strName )
		, m_fRange( 0.0f )
		, m_fFallOff( 0.0f )
		, m_fTheta( 0.0f )
		, m_fPhi( 0.0f )
		, m_vAttenuation( 1.0f, 0.0f, 0.0f )
		, m_Ambient(0.0f,0.0f,0.0f,0.0f)
		, m_Intensity(1.0f)
	{
		CnShadowMappable::SetLight( this );
	}
	CnLight::~CnLight()
	{
	}

	//----------------------------------------------------------------
	bool CnLight::Cull( CnCamera* pCamera )
	{
		switch (m_nType)
		{
		case Ambient:
			return true;
		case Directional:
			break;
		case Point:	
		case Spot:
			return pCamera->IsCulled( GetWorldPosition(), GetRange() );

		default:
			return true;
		}

		return false;
	}

	//----------------------------------------------------------------
	/** @brief	관계 형성 
				Light-Mesh
				-라이트와 메쉬의 경우, 라이트가 바라보는 (그림자를 만들 수 있는)
				메쉬를 찾아야 한다.
	*/
	//----------------------------------------------------------------
	void CnLight::RelationTo( CnSceneEntity* pEntity, CnCamera* pCamera )
	{
		bool bVisible = pEntity->Cull( pCamera ) ? false : true;

		CnShadowMappable::_TestProjection( pCamera, pEntity, bVisible );
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	void CnLight::ClearRelation()
	{
		CnShadowMappable::Reset();
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	void CnLight::Apply( CnCamera* pCamera )
	{
	}

	//----------------------------------------------------------------
	/** @brief	Update */
	//----------------------------------------------------------------
	bool CnLight::Update( CnCamera* pCamera )
	{
		//@add by cagetu - 10/01/12 : CnVisibleSet::Register에서 위치 변경
		ClearRelation();

		return true;
	}

	//----------------------------------------------------------------
	/** @brief	*/
	//----------------------------------------------------------------
	void CnLight::GetAABB( Math::AABB& rOutput )
	{	// Blank.. Blank.. Blank..
	}

	//----------------------------------------------------------------
	/** @brief	*/
	//----------------------------------------------------------------
	void CnLight::Draw( CnCamera* pCamera )
	{
	}
}