//================================================================
// File:           : CnLightManager.h
// Original Author : changhee
// Creation Date   : 2007. 1. 25
//================================================================
#ifndef __CN_LIGHT_MANAGER_H__
#define __CN_LIGHT_MANAGER_H__

#include "CnLight.h"
#include "../Util/CnUncopyable.h"

namespace Cindy
{
	//================================================================
	/** Light Queue Class
	    @author    changhee
		@since     2007. 1. 29
		@remarks   Light 관리자 클래스
	*/
	//================================================================
	class CnLightManager : private CnUncopyable
	{
	public:
		typedef std::vector<Pointer<CnLight> > LightList;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		CnLightManager();
		~CnLightManager();

		void	Add( const Pointer<CnLight>& pLight );
		void	Clear();

		const Pointer<CnLight>&	GetGlobalLight() const;
		//const LightList&		GetDirectionalLights() const;
		const LightList&		GetPointLights() const;
		const LightList&		GetSpotLights() const;

		static CnLightManager* Instance();
	private:

		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		Pointer<CnLight>	m_GlobalLight;
		//LightList			m_DirectionalLights;
		LightList			m_PointLights;
		LightList			m_SpotLights;
	};

	// Macro
	#define LightMgr	CnLightManager::Instance()

	//----------------------------------------------------------------
	/**
	*/
	inline
	const Pointer<CnLight>&
	CnLightManager::GetGlobalLight() const
	{
		return m_GlobalLight;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	const CnLightManager::LightList&
	CnLightManager::GetDirectionalLights() const
	{
		return m_DirectionalLights;
	}
	
	//----------------------------------------------------------------
	/**
	*/
	inline
	const CnLightManager::LightList&
	CnLightManager::GetPointLights() const
	{
		return m_PointLights;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	const CnLightManager::LightList&
	CnLightManager::GetSpotLights() const
	{
		return m_SpotLights;
	}
}

#endif	// __CN_LIGHT_MANAGER_H__