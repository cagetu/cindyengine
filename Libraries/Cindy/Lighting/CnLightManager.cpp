//================================================================
// File:               : CnLightManager.cpp
// Related Header File : CnLightManager.h
// Original Author     : changhee
// Creation Date       : 2009. 5. 27
//================================================================
#include "../Cindy.h"
#include "../Util/CnLog.h"
#include "CnLightManager.h"

namespace Cindy
{
#pragma TODO( "라이트 시스템을 어떻게 설계를 할지 결정한다" )
	CnLightManager* CnLightManager::Instance()
	{
		static CnLightManager inst;
		return &inst;
	}
	//================================================================
	/// Const/Dest
	CnLightManager::CnLightManager()
	{
	}
	CnLightManager::~CnLightManager()
	{
		Clear();
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnLightManager::Add( const Pointer<CnLight>& pLight )
	{
		switch (pLight->GetType())
		{
		case CnLight::Global:
			m_GlobalLight = pLight;
			break;

		case CnLight::Ambient:
			break;

		//case CnLight::Directional:
		//	m_DirectionalLights.push_back( pLight );
		//	break;

		case CnLight::Point:
			m_PointLights.push_back( pLight );
			break;

		case CnLight::Spot:
			m_SpotLights.push_back( pLight );
			break;

		default:
			CnError( L"[CnLightManager::Add] Invlaid Light" );
			break;
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnLightManager::Clear()
	{
		m_GlobalLight = 0;
		//m_DirectionalLights.clear();
		m_PointLights.clear();
		m_SpotLights.clear();
	}
}