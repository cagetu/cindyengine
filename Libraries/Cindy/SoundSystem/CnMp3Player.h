// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNMP3PLAYER_H__
#define __CNMP3PLAYER_H__

#include "../Cindy.h"
#include <DShow.h>

namespace Cindy
{
	//==================================================================
	/** Mp3Player
		@author
			cagetu
		@since
			2006년 12월 27일
		@remarks
			Mp3 파일 플레이어..
	*/
	//==================================================================
	class CN_DLL CnMp3Player
	{
	private:
		// mp3 관련 데이터..
		IGraphBuilder*		m_pGB;
		IMediaSeeking*		m_pMS;
		IMediaControl*		m_pMC;
		IMediaEventEx*		m_pME;
		IBasicAudio*		m_pBA;

		HWND				m_hWnd;				//!< 윈도우 핸들
		bool				m_bInitialized;		//!< 초기화 여부

		uint				m_uiNotifyMsg;		//!< 메세지

		bool				m_bPlay;			//!< 현재 상태
		bool				m_bLoop;			//!< 루프인지	
		long				m_lVolume;			//!< -10000 ~ 0

		bool			InitDShow( uint uiNotifyMsg );
		void			ClearDShow();

		void			Reset();

		void			GetGraphBuilderErrorMessage( HRESULT hr, MoString& rResult );
	public:
		CnMp3Player( HWND hWnd );
		~CnMp3Player();

		/** 초기화
			@param uiNotifyMsg
				외부에서 이벤트를 처리하기 위한 윈도우 이벤트를 넣어준다.
			ex) Initialize( WM_APP + 1 );
			...
			어플리케이션 에서 
			switch( nMsg )
			{
			case (WM_APP+1):
				Mp3Player->OnNotify();
				break;
			}

			이렇게 연결해 주어야 한다.
		*/
		bool		Initialize( uint uiNotifyMsg );
		void		Clear();

		bool		Load( const MoString& strFileName );

		bool		Play();
		void		Stop();
		void		Pause();

		void		SetLoop( bool bLoop );
		void		SetVolume( long lVolume );
		
		long		GetVolume() const				{	return m_lVolume;	}

		bool		IsPlaying() const				{	return m_bPlay;		}
		bool		IsLooping() const				{	return m_bLoop;		}

		/** 이벤트 처리
		*/
		void		OnNotify();
	};
}

#endif	// __CNMP3PLAYER_H__