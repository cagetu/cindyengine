// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnDirectSoundManager.h"

namespace Cindy
{
	//----------------------------------------------------------------------------
	CnDirectSoundManager::CnDirectSoundManager()
		: m_bInitialized(false)
	{
	}

	//----------------------------------------------------------------------------
	CnDirectSoundManager::~CnDirectSoundManager()
	{
	}

	//----------------------------------------------------------------------------
	bool CnDirectSoundManager::Initialize( HWND hWnd, DWORD dwCoopLevel )
	{
		if( !m_bInitialized  )
		{
			m_bInitialized = true;
		}
	}

	//----------------------------------------------------------------------------
	void CnDirectSoundManager::Clear()
	{
		m_bInitialized = false;
	}

	//----------------------------------------------------------------------------
	LPDIRECTSOUNDBUFFER	CnDirectSoundManager::CreateSoundBuffer( PDCWAVEFORMAT pcmWaveFormat, ulong ulBufferSize, ulong ulCreationFlags )
	{
		if( false == m_bInitialized )
			return NULL;

		LPDIRECTSOUNDBUFFER pResult = NULL;

		// 2D 웨이브 플래그 셋팅( 3D사운드는 사용하지 않는다.. )
//		ulCreationFlags	= DSBCAPS_CTRLPAN | DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_CTRLFREQUENCY | DSBCAPS_CTRLVOLUME;
		ulong ulCreationFlags = DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLPAN | DSBCAPS_GETCURRENTPOSITION2;
		GUID guid3DAlgorithm = GUID_NULL;

		// Make the DirectSound buffer the same size as the wav file( 실제의 사운드 버퍼를 생성한다. )
		DSBUFFERDESC dsbd;
		ZeroMemory( &dsbd, sizeof(DSBUFFERDESC) );
		dsbd.dwSize            = sizeof(DSBUFFERDESC);
		dsbd.dwFlags           = ulCreationFlags;
		dsbd.dwBufferBytes     = ulBufferSize;
		dsbd.guid3DAlgorithm   = guid3DAlgorithm;
		dsbd.lpwfxFormat       = pcmWaveFormat;

		HRESULT	hr = S_OK;
		if( FAILED( hr = m_pDS->CreateSoundBuffer( &dsbd, &pResult, NULL ) ) )
		{
			// 실패했을경우 
			// DSERR_INVALIDPARAM 값을 리턴시킨다. 이유는 Flags와 guid3DAlgorithm 안맞아서 그렇다. 차후에 이유를 밝힘.
			ulCreationFlags		 = DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLPAN | DSBCAPS_GETCURRENTPOSITION2;
			guid3DAlgorithm		 = GUID_NULL;
			dsbd.dwFlags         = ulCreationFlags;
			dsbd.guid3DAlgorithm = guid3DAlgorithm;

			if( FAILED( pDS->CreateSoundBuffer( &dsbd, &pResult, NULL ) ) )
				return NULL;
		}

		return pResult;
	}

} // end of namespace 