// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnMp3Player.h"

namespace Cindy
{
	//----------------------------------------------------------------------------
	CnMp3Player::CnMp3Player( HWND hWnd )
		: m_hWnd(hWnd)
		, m_pGB(0)
		, m_pMS(0)
		, m_pMC(0)
		, m_pME(0)
		, m_pBA(0)
		, m_bInitialized(false)
		, m_bPlay(false)
		, m_lVolume(0)
	{
	}

	//----------------------------------------------------------------------------
	CnMp3Player::~CnMp3Player()
	{
		Clear();
	}

	//----------------------------------------------------------------------------
	bool CnMp3Player::InitDShow( uint uiNotifyMsg )
	{
		HRESULT hr = CoCreateInstance( CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (void **)&m_pGB );
		if( FAILED( hr ) )
		{
			m_bInitialized = false;

			LOGERR( L"[AvaMP3Mgr::Initialize] Can't create DShow" );

#ifdef _DEBUG
			Except( MoException::ERR_NOT_INITIALIZED, L"Can't create DShow", L"CnMp3Player::Initialize" );
#endif

			return false;
		}

		if( FAILED( hr = m_pGB->QueryInterface( IID_IMediaControl, (void **)&m_pMC ) ) )
			return false;		
		
		if( FAILED( hr = m_pGB->QueryInterface( IID_IMediaSeeking, (void **)&m_pMS ) ) )
			return false;

		if( FAILED( hr = m_pGB->QueryInterface( IID_IMediaEventEx, (void **)&m_pME ) ) )
			return false;

		if( FAILED( hr = m_pGB->QueryInterface( IID_IBasicAudio,   (void **)&m_pBA ) ) )
			return false;

		m_pME->SetNotifyWindow( (OAHWND)m_hWnd, uiNotifyMsg, 0 );
		m_uiNotifyMsg = uiNotifyMsg;

		return true;
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::ClearDShow()
	{
		SAFEREL( m_pME );
		SAFEREL( m_pMC );
		SAFEREL( m_pMS );
		SAFEREL( m_pGB );
		SAFEREL( m_pBA );
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::Reset()
	{
		Clear();
		Initialize( m_uiNotifyMsg );
	}

	//----------------------------------------------------------------------------
	bool CnMp3Player::Initialize( uint uiNotifyMsg )
	{
		if( !m_bInitialized )
		{
			HRESULT hr = CoInitialize(0);
			if( FAILED(hr) )
			{
				LOGERR( L"[CnMp3Player::Initialize] FAILED( hr = CoInitialize( NULL ) )" );
				return false;
			}

			if( !InitDShow( uiNotifyMsg ) )
			{
				LOGERR( L"[CnMp3Player::Initialize] !InitDShow( uiNotifyMsg )" );
				return false;
			}

			m_bInitialized = true;
		}

		return true;
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::Clear()
	{
		if( m_bInitialized )
		{
			Stop();

			ClearDShow();

			CoUninitialize();

			m_bInitialized = false;
		}
	}

	//----------------------------------------------------------------------------
	bool CnMp3Player::Load( const MoString& strFileName )
	{
		// 이걸 꼭 해야하나?
//		Reset();

		if( !m_bInitialized )
			return false;

		HRESULT hr = m_pGB->RenderFile( strFileName.c_str(), NULL );
		if( FAILED( hr ) )
		{
			MoString error;
			GetGraphBuilderErrorMessage( hr, error );

			wchar buffer[256];
			wmemset( buffer, 0x00, 256 );
			swprintf( buffer, L"[CnMp3Player::Load] %s : %s", strFileName.c_str() );
			LOGMSG( buffer );

//#ifdef _DEBUG

//			Except( MoException::ERR_NOT_INITIALIZED, L"Can't render DShow media", L"AvaMP3Mgr::Load" );
			// 계속 들어와서 디버깅이 안됨... 잠시 막음 add bbum
			//Except( MoException::ERR_NOT_INITIALIZED, error.c_str(), L"AvaMP3Mgr::Load" );
//#endif
			return false;
		}

		return true;
	}

	//----------------------------------------------------------------------------
	bool CnMp3Player::Play()
	{
		if( !m_bInitialized || !m_pMC )
			return false;

		SetVolume( m_lVolume );

		if( FAILED( m_pMC->Run() ) )
			return false;

		m_bPlay = true;

		return true;
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::Stop()
	{
		if( !m_bInitialized || m_bPlay || m_pMC )
		{
			m_pMC->Stop();

			LONGLONG pos=0;
			m_pMS->SetPositions( &pos, AM_SEEKING_AbsolutePositioning, NULL, AM_SEEKING_NoPositioning );
			m_pMC->StopWhenReady();

			m_bPlay = false;
		}
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::Pause()
	{
		if( !m_bInitialized || m_bPlay )
		{
			if( m_pMC )
				m_pMC->Pause();
		}
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::SetLoop( bool bLoop )
	{
		m_bLoop = bLoop;
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::SetVolume( long lVolume )
	{
		m_lVolume = lVolume;

		if( m_pBA )
		{
			m_pBA->put_Volume( lVolume );
		}
	}

	//----------------------------------------------------------------------------
	void CnMp3Player::OnNotify()
	{
		if( !m_bInitialized || !m_pME )
			return;

		LONG evCode, evParam1, evParam2;
		HRESULT hr;

		while( SUCCEEDED( m_pME->GetEvent( &evCode, (LONG_PTR*)&evParam1, (LONG_PTR*)&evParam2, 0 ) ) )
		{
			if( EC_COMPLETE == evCode )
			{
				if( m_bLoop )
				{
					if( m_pMS )
					{
						LONGLONG pos = 0;

						hr = m_pMS->SetPositions( &pos, AM_SEEKING_AbsolutePositioning, NULL,
												  AM_SEEKING_NoPositioning );												

						//if( FAILED( hr ) )
						//{
							Stop();
							Play();
						//}
					}
				}
			}

			hr = m_pME->FreeEventParams( evCode, evParam1, evParam2 );
		}

	}

	//----------------------------------------------------------------------------
	void CnMp3Player::GetGraphBuilderErrorMessage( HRESULT hr, MoString& rResult )
	{
		switch( hr )
		{
		case VFW_S_AUDIO_NOT_RENDERED:
			rResult = L"Partial success; the audio was not rendered";
			break;

		case VFW_S_DUPLICATE_NAME:
			rResult = L"Success; the Filter Graph Manager modified the filter name to avoid duplication";
			break;

		case VFW_S_MEDIA_TYPE_IGNORED:
			rResult = L"The method successfully loaded a graph from a .grf file, but one or more filters connected with a different media type than specified.";
			break;

		case VFW_S_PARTIAL_RENDER:
			rResult = L"Some of the streams in this movie are in an unsupported format.";
			break;

		case VFW_S_VIDEO_NOT_RENDERED:
			rResult = L"Partial success; some of the streams in this movie are in an unsupported format.";
			break;

		case E_ABORT:
			rResult = L"Operation aborted.";
			break;

		case E_FAIL:
			rResult = L"Failer";
			break;

		case E_INVALIDARG:
			rResult = L"Argument is invalid";
			break;

		case E_OUTOFMEMORY:
			rResult = L"Insufficient memory";
			break;

		case E_POINTER:
			rResult = L"NULL pointer argument";
			break;

		case VFW_E_CANNOT_CONNECT:
			rResult = L"No combination of intermediate filters could be found to make the connection";
			break;
			
		case VFW_E_CANNOT_LOAD_SOURCE_FILTER:
			rResult = L"The source filter for this file could not be loaded";
			break;

		case VFW_E_CANNOT_RENDER:
			rResult = L"No combination of filters could be found to render the stream";
			break;

		case VFW_E_INVALID_FILE_FORMAT:
			rResult = L"The file format is invalid";
			break;

		case VFW_E_NOT_FOUND:
			rResult = L"An object or name was not found.";
			break;

		case VFW_E_UNKNOWN_FILE_TYPE:
			rResult = L"The media type of this file is not recognized";
			break;

		case VFW_E_UNSUPPORTED_STREAM:
			rResult = L"Cannot play back the file: the format is not supported";
			break;

		default:
			rResult = L"Unknown Reason: Can't render DShow media";
			break;
		}
	}
}