// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __DIRECTSOUNDMANAGER_H__
#define __DIRECTSOUNDMANAGER_H__

#include "../Cindy.h"
#include <dsound.h>

namespace Cindy
{
	//==================================================================
	/** CnDirectSoundManager
		@author
			cagetu
		@since
			2006년 12월 28일
		@remarks
			DirectSound 관리자
	*/
	//==================================================================
	class CN_DLL CnDirectSoundManager : public MoSingleton< CnDirectSoundManager >
	{
	private:
		bool					m_bInitialized;			//!< 초기화 여부

		LPDIRECTSOUND8			m_pDS;
		DSCAPS					m_Dscaps;				// 장치 성능 검사하기

		WAVEFORMATEX			m_wfx;
		WORD					m_wFormatTag;			//! 음원 관련 정보들..
		WORD					m_wChannels;
		DWORD					m_dwSamplesPerSec;
		DWORD					m_dwAvgBytesPerSec;
		WORD					m_wBlockAlign;
		WORD					m_wBitsPerSample;			

		long					m_lVolume;				// -10000 ~ 0
		bool					m_bSoundOff;

		CnVector3				m_vListenerPos;			//! 청취자 위치
		CnVector3				m_vListenerDir;			//! 청취자 방향

	public:
		CnDirectSoundManager();
		virtual ~CnDirectSoundManager();

		/** 초기화/해제
		*/
		bool				Initialize( HWND hWnd, DWORD dwCoopLevel );
		void				Clear();

		/** DirectSound 세팅
		*/

		/** 사운드 버퍼를 생성해준다.
		*/
		LPDIRECTSOUNDBUFFER	CreateSoundBuffer( PCMWAVEFORMAT pcmWaveFormat, ulong ulBufferSize, ulong ulCreationFlags );
	};

#define g_DirectSoundMgr	CnDirectSoundManager::GetSingleton()
#define g_pDirectSoundMgr	CnDirectSoundManager::GetSingletonPtr()
}

#endif	// __DIRECTSOUNDMANAGER_H__