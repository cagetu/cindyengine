//================================================================
// File:               : CnResource.inl
// Related Header File : CnResource.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 2
//================================================================

//----------------------------------------------------------------
inline
const CnString& CnResource::GetName() const
{
	return m_strName;
}

//----------------------------------------------------------------
inline
CnResource::ID CnResource::GetRscID() const
{
	return m_ResourceID;
}

//----------------------------------------------------------------
inline
void CnResource::SetState( CnResource::State eState )
{
	m_State = eState;
}
//----------------------------------------------------------------
inline
CnResource::State CnResource::GetState()
{
	return m_State.Get();
}

//----------------------------------------------------------------
inline
bool CnResource::IsLoaded()
{
	return GetState() != Unloaded;
}

//----------------------------------------------------------------
inline
bool CnResource::IsEmpty()
{
	return GetState() == Empty;
}

//----------------------------------------------------------------
inline
CnResource::RES_PRIORITY CnResource::GetPriority() const
{
	return m_ePriority;
}

//----------------------------------------------------------------
inline
void CnResource::SetPriority( CnResource::RES_PRIORITY ePriority )
{
	m_ePriority = ePriority;
}

//----------------------------------------------------------------
inline
time_t CnResource::GetLastAccessTime() const
{
	return m_tLastAccess;
}
