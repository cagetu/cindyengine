//================================================================
// File:               : CnResource.cpp
// Related Header File : CnResource.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 2
//================================================================
#include "../Cindy.h"
#include "CnResource.h"
#include "CnResourceManager.h"

namespace Cindy
{
	//==============================================================
	__ImplementRtti( Cindy, CnResource, CnObject );
	//--------------------------------------------------------------
	ushort CnResource::ms_NextResourceID = 0;
	//--------------------------------------------------------------
	CnResource::CnResource( CnResourceManager* pParent )
		: m_pParent( pParent )
		, m_ePriority( RP_MID )
		, m_tLastAccess( 0 )
	{
		m_State = Unloaded;

		m_ResourceID = ms_NextResourceID++;
		assert( ms_NextResourceID != 0xffff ); 
	}

	//--------------------------------------------------------------
	CnResource::~CnResource()
	{
	}

	//--------------------------------------------------------------
	void CnResource::Access()
	{
		m_tLastAccess = time( NULL );
	}

	//--------------------------------------------------------------
	bool CnResource::operator < ( const CnResource& rRsc ) const
	{
		if( m_ePriority < rRsc.GetPriority() )
		{
			return true;
		}
		else if( m_ePriority > rRsc.GetPriority() )
		{
			return false;
		}
		// at first, compare access time
		else 
		{
			if( m_tLastAccess < rRsc.GetLastAccessTime() )
				return true;
		}

		return false;
	}
}