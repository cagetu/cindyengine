//================================================================
// File:               : CnExportedDataDefines.cpp
// Related Header File : CnExportedDataDefines.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 16
//================================================================
#include "CnExportedDataDefines.h"
#include "../Graphics/Base/CnVertexBuffer.h"
#include "../Graphics/Base/CnVertexDeclaration.h"
#include "../Geometry/CnMesh.h"

namespace Cindy
{
	namespace Export
	{
		//----------------------------------------------------------------
		//	Node struct
		//----------------------------------------------------------------
		Node::Node()
			: type( MESH )
		{
		}
		Node::~Node()
		{
		}

		//----------------------------------------------------------------
		bool Node::HasParent() const
		{
			return (parent_id != INVALID_NODE_ID);
		}

		//----------------------------------------------------------------
		//	MeshNode struct
		//----------------------------------------------------------------
		MeshNode::MeshNode()
			: enableVertexColor(false)
		{
		}
		MeshNode::~MeshNode()
		{
			std::for_each( submeshdatas.begin(), submeshdatas.end(), DeleteObject() );
		}

		//----------------------------------------------------------------
		bool MeshNode::IsSkinned() const
		{
			return (skindata != 0);
		}

		//----------------------------------------------------------------
		/** @brief	CreateInstance */
		//----------------------------------------------------------------
		CnMesh* MeshNode::CreateInstance()
		{
			CnMesh* mesh = CnNew CnMesh( this->name );

			mesh->SetVertexData( this->vertexData );
			mesh->SetSkinnedData( this->skindata );
			mesh->SetAABB( this->aabb );
			mesh->SetOBB( this->obb );

			Export::SubMesh* data = NULL;
			CnSubMesh* submesh = NULL;

			MeshNode::SubMeshDataArray::iterator i, iend;
			iend = this->submeshdatas.end();
			for(i = this->submeshdatas.begin(); i != iend; ++i)
			{
				data = (*i);

				submesh = CnNew CnSubMesh();
				submesh->SetIndexBuffer( data->indexbuffer );
				submesh->SetMtlID( data->materialID );

				mesh->AddSubMesh( submesh );
			}

			return mesh;
		}

		//----------------------------------------------------------------
		//	SubMesh struct
		//----------------------------------------------------------------
		SubMesh::SubMesh()
		{
			materialID = 0;
			material.opacity = false;
			material.twoside = false;
			material.blending = false;
		}
		SubMesh::~SubMesh()
		{
			indexbuffer.SetNull();
		}

		//----------------------------------------------------------------
		//	Export::SubMesh struct
		//----------------------------------------------------------------
		Skinned::Skinned()
			: num_vertex_blend(0)
			, num_influence_bones(0)
			, influence_bone_indices(0)
		{
		}
		Skinned::~Skinned()
		{
			SAFEDELS( influence_bone_indices );
		}
	}
}
