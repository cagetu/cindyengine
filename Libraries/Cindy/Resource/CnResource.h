// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNRESOURCE_H__
#define __CNRESOURCE_H__

#include "../Foundation/CnObject.h"
#include <MoCommon/Thread/MoThreadVariable.h>
#include <MoCommon/Thread/MoCriticalSection.h>
#include <MoCommon/IO/MoMemStream.h>

namespace Cindy
{
	class CnResourceManager;

	//==================================================================
	/** Resource
			@author			cagetu
			@since			2006년 9월 25일
			@remarks
				리소스 클래스
	*/
	//==================================================================
	class CnResource abstract : public CnObject
	{
		__DeclareRtti;
	public:
		typedef unsigned short ID;

		// Resource Pririty enumeration
		enum RES_PRIORITY
		{
			RP_LOW	= 0,
			RP_MID,
			RP_HIGH,
		};

		// 리소스 상태
		enum State
		{
			Unloaded = 0,		// 아직 읽어지지 않음
			Loaded,				// 읽어짐
			Lost,				// 현재 사용할 수 없음
			Empty,				// 사용가능하지만, 데이터는 비어 있음..
		};

	public:
		virtual ~CnResource();

		// Load/Unload
		virtual bool		Load( const wchar* strFileName ) abstract;
		virtual bool		Unload() abstract;

		// 리소스 이름 얻어오기
		const CnString&		GetName() const;

		// 리소스 ID 얻어오기
		ID					GetRscID() const;

		// 리소스 상태
		void				SetState( State eState );
		State				GetState();

		bool				IsLoaded();
		bool				IsEmpty();

		// Device Lost/Restore
		virtual void		OnLost() abstract;
		virtual void		OnRestore() abstract;

		// Priority
		RES_PRIORITY		GetPriority() const;
		void				SetPriority( RES_PRIORITY ePriority );

		// Access
		virtual void		Access();
		time_t				GetLastAccessTime() const;

		// operator < 
		virtual bool operator < ( const CnResource& rRsc ) const;

	protected:
		CnResourceManager*	m_pParent;			//!< 리소스 매니져

		CnString		m_strName;			//!< 리소스 이름

		time_t			m_tLastAccess;		//!< 마지막 접근 시간
		RES_PRIORITY	m_ePriority;		//!< 우선 순위

		ID				m_ResourceID;		//!< 리소스 ID
		static ID		ms_NextResourceID;

		MoThreadVariable<State>	m_State;			//!< 리소스 상태

	protected:
		/** 생성자
			@remarks
				이 리소스를 관리하는 매니져만 생성 가능하다.
			@param pParent
				부모 관리자
		*/
		CnResource( CnResourceManager* pParent );
	};

	typedef Ptr<CnResource> RscPtr;

#include "CnResource.inl"

}	// end of namespace

#endif	// __CNRESOURCE_H__