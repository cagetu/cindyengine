//================================================================
// File:           : CnExportedDataDefines.h
// Original Author : changhee
// Creation Date   : 2007. 4. 10
//================================================================
#pragma once

#include "../Math/CindyMath.h"
#include "../Foundation/CnObject.h"
#include "../Geometry/CnVertexData.h"
#include "../Graphics/Base/CnIndexBuffer.h"

namespace Cindy
{
	#define INVALID_NODE_ID		0xffff

	class CnVertexDeclaration;
	class CnVertexBuffer;
	class CnIndexBuffer;
	class CnMesh;
	class CnVertexData;

	namespace Export
	{
		struct SubMesh;
		struct Skinned;
		struct LinearKey;
		struct RotKey;

		//================================================================
		/** Node Data
			@author    changhee
			@since     2007. 4. 10
			@remarks   그래픽 툴을 이용해서 작성된 노드 데이터
		*/
		//================================================================
		struct Node : public CnMemLeakObject//CnMemAllocObject
		{
			enum Type
			{
				MESH = 0,
				BONE,
				DUMMY,
			};

			Type			type;

			ushort			id;
			ushort			parent_id;
			CnString		name;
			CnString		parent_name;

			Math::Vector3	local_pos;
			Math::Quaternion local_rot;
			Math::Vector3	local_scl;

			Math::Matrix44	worldTM;

			Node();
			virtual ~Node();

			bool	HasParent() const;
		};


		//================================================================
		/** Mesh Node Data
			@author    changhee
			@since     2007. 4. 10
			@remarks   그래픽 툴을 이용해서 작성된 메쉬 노드 데이터
		*/
		//================================================================
		struct MeshNode : public Node
		{
			typedef std::vector<Export::SubMesh*>	SubMeshDataArray;

			Ptr<CnVertexData>		vertexData;
			Ptr<Export::Skinned>	skindata;

			SubMeshDataArray		submeshdatas;
			bool					enableVertexColor;

			Math::AABB				aabb;
			Math::OBB				obb;

			//------------------------------------------------------
			// Methods
			//------------------------------------------------------
			MeshNode();
			virtual ~MeshNode();

			bool	IsSkinned() const;

			CnMesh*	CreateInstance();
		};

		typedef Node	BoneNode;

		//================================================================
		//	Animation Data
		//================================================================
		struct AnimNode : public Node
		{
			ushort			numPosKeys;
			LinearKey*		posKeys;

			ushort			numRotKeys;
			RotKey*			rotKeys;

			ushort			numScaleKeys;
			LinearKey*		scaleKeys;
		};

		//================================================================
		/** Sub Mesh Data
			@author    changhee
			@since     2007. 4. 10
			@remarks   그래픽 툴을 이용해서 작성된 하위 메쉬 데이터
		*/
		//================================================================
		struct SubMesh
		{
			struct Material
			{
				bool		opacity;
				bool		twoside;
				bool		blending;

				CnString	diffuseMap;
				CnString	specularMap;
				CnString	bumpMap;
			};

			Ptr<CnIndexBuffer>	indexbuffer;

			Material				material;
			ushort					materialID;

			SubMesh();
			~SubMesh();
		};

		struct Skinned : public CnRefCount
		{
			ushort	num_vertex_blend;

			ushort	num_influence_bones;
			ushort*	influence_bone_indices;

			Skinned();
			virtual ~Skinned();
		};

		// BoundingBox 정보
		struct BoundingBox
		{
			ushort		parentID;
			CnString	parentName;
			Math::OBB	obb;

			BoundingBox() : parentID( 0xffff ), parentName( L"" )	{}
		};

		//
		struct LinearKey
		{
			ushort			frame;			//! 프레임
			Math::Vector3	value;			//! 값

			LinearKey() : frame(0) {}
		};

		struct RotKey
		{
			ushort				frame;
			Math::Quaternion	value;

			RotKey() : frame(0) {}
		};
	}

	//@<
	//==================================================================
	// Mesh Data Chunk
	//==================================================================
	#define RD_MESHFILE_MAGICNUM						0x19760601
	#define RD_MESHFILE_VERSION							0x20051225

	#define RD_MESH_MAGIC								0xabcd0123				// magic num 저장					
	#define RD_MESH_VERSION								0xabcd4567				// file version 저장

	#define RD_MESH_NODELIST							0xabcd8901				// 저장하는 거 음따
		#define RD_MESH_NODE							0x50531000				// node type 저장( 0 : mesh 1 : bone 2 : dummy )
			#define RD_MESH_NODE_ID						0x50531010				// node id 저장
			#define RD_MESH_NODE_NAME					0x50531011				// node name 길이 및 문자열 저장
			#define RD_MESH_NODE_PARENTID				0x50531012				// parent id 저장
			#define RD_MESH_NODE_PARENTNAME				0x50531013				// parent node name 길이 및 문자열 저장

			#define RD_MESH_NODE_LOCALTM				0x50532020				// local pos, rot, scl 저장
			#define RD_MESH_NODE_WORLDTM				0x50532021				// world matrix 3 by 4 저장

			#define RD_MESH_NODE_LINKEDBONE				0x50533111				// 이 노드에 걸려있는 총 본의 개수와 노드 인덱스들 저장
			#define RD_MESH_NODE_MAXLINK				0x50533112				// 버텍스 하나당 최대 링크 수
			#define RD_MESH_NODE_VERTEXCOLOR			0x50533113				// 버텍스 칼라 사용 여부
			#define RD_MESH_NODE_PACKEDVERTEX			0x50533114				// 버텍스 압축 사용 여부

			#define RD_MESH_NODE_VERTEX					0x50534100				// vertex 총 개수 저장
				#define RD_MESH_NODE_VERTEX_POS			0x50534121				// position vector 저장
				#define RD_MESH_NODE_VERTEX_NORM		0x50534122				// normal vector 저장
				#define RD_MESH_NODE_VERTEX_UV			0x50534123				// uv 저장
				#define RD_MESH_NODE_VERTEX_BLEND		0x50534124				// link 개수, bone id, weight 저장, 여러번 호출 예정
				#define RD_MESH_NODE_VERTEX_COLOR		0x50534125				// 개수만큼 color vector 저장
				#define RD_MESH_NODE_VERTEX_UV2			0x50534126				// 개수만큼 uv2 저장
				#define RD_MESH_NODE_VERTEX_TANGENT		0x50534127				// 개수만큼 tangent 저장
				#define RD_MESH_NODE_VERTEX_BINORMAL	0x50534128				// 개수만큼 binormal 저장
				// color나 기타 정보 저장해도 됨.

			#define RD_MESH_NODE_FACELIST				0x50535000				// 저장하는 그 음따
				#define RD_MESH_NODE_FACE				0x50535010				// 같은 머테리얼 쓰는 face 개수 저장
					#define RD_MESH_NODE_FACE_INDICES	0x50535011				// index list 저장, face cnt * 3만큼 저장
					#define RD_MESH_NODE_FACE_DIMAP		0x50535012				// diffuse map 이름 길이와 문자열 저장
					#define RD_MESH_NODE_FACE_SPMAP		0x50535013				// specular map 이름 길이와 문자열 저장
					#define RD_MESH_NODE_FACE_TWOSIDE	0x50535014				// two side 여부 저장
					#define RD_MESH_NODE_FACE_OPACITY	0x50535015				// opacity 여부 저장
					#define RD_MESH_NODE_FACE_BLENDING	0x50535016				// alpha blending 여부 저장
					#define RD_MESH_NODE_FACE_NRMAP		0x50535017				// normal map 이름 길이와 문자열 저장
					#define RD_MESH_NODE_FACE_MTLID		0x50535018				// material id

			#define RD_MESH_NODE_AABB					0x50536000				// aabb min, max 저장
			#define RD_MESH_NODE_OBB					0x50537000				// obb center, axis, extent 저장

		#define RD_MESH_NFNODE							0x50541000				// type 저장( 1 : height, 2 : indoor )
			#define RD_MESH_NFNODE_VERTEX				0x50541010				// vertex 총개수와 개수만큼의 pos 저장
			#define RD_MESH_NFNODE_FACE					0x50541020				// face 총개수 저장
				#define RD_MESH_NFNODE_FACE_INDEX		0x50541021				// face index 저장
				#define RD_MESH_NFNODE_FACE_NORMAL		0x50541022				// face normal 저장

	#define RD_MESH_TRACE								0x09876543				// trace top, bottom

	//==================================================================
	// Skeleton Data Chunk
	//==================================================================
	#define RD_SKELFILE_MAGICNUM						0x19790901
	#define RD_SKELFILE_VERSION							0x20051225

	#define RD_SKEL_MAGIC								0x0987edcb			
	#define RD_SKEL_VERSION								0x0987dcba

	#define RD_SKEL_BONELIST							0x0987abcd				// 저장하는 그 음따
		#define RD_SKEL_BONE							0x58361100				// bone type 저장( bone, dummy )
			#define RD_SKEL_BONE_ID						0x58361101				// node id 저장
			#define RD_SKEL_BONE_NAME					0x58361102				// node name 길이 및 문자열 저장
			#define RD_SKEL_BONE_PARENTID				0x58361103				// parent id 저장
			#define RD_SKEL_BONE_PARENTNAME				0x58361104				// parent node name 길이 및 문자열 저장

			#define RD_SKEL_BONE_LOCALTM				0x58361201				// local pos, rot, scl 저장
			#define RD_SKEL_BONE_WORLDTM				0x58361202				// world matrix 3 by 4 저장

			#define RD_SKEL_BONE_AABB					0x58361301				// min, max
			#define RD_SKEL_BONE_OBB					0x58361302				// center, axis(3), extend(3)

			#define RD_SKEL_BONE_DEFINE					0x58361401				// bone 노드의 정의된 성질.

	#define RD_SKEL_BBOXLIST							0x0987fcad				// 저장하는 그 음따
		#define RD_SKEL_BBOX							0x87871500				// 저장하는 그 음따
			#define RD_SKEL_BBOX_PARENTID				0x87871501				// parent id
			#define RD_SKEL_BBOX_PARENTNAME				0x87871502				// parent name
			#define RD_SKEL_BBOX_OBB					0x87871503				// center, axis(3), extend(3)

	#define RD_SKEL_BONE_DEFINE_LIST					0x0975acad				// 본을 지정하는 성질
		#define RD_SKEL_DEFINE							0x20060720				// 설정 정보

	//==================================================================
	// Animation Data Chunk
	//==================================================================
	#define RD_ANIMFILE_MAGICNUM						0x19741031
	#define RD_ANIMFILE_VERSION							0x20051225

	#define RD_ANIM_MAGIC								0x09090909
	#define RD_ANIM_VERSION								0x38383838

	#define RD_ANIM_TOTALFRAME							0x10381038				// 총 Frame
	#define RD_ANIM_FRAMERATE							0x12345678				// Frame Rate

	#define RD_ANIM_NODELIST							0x89498949				// 저장하는 그 음따
		#define RD_ANIM_NODE							0x70003000				// 저장하는 그 음따
		#define RD_ANIM_NODE_ID							0x70003010				// node id 저장
		#define RD_ANIM_NODE_NAME						0x70003020				// node name 길이 및 문자열 저장

		#define RD_ANIM_NODE_POSTRACK					0x70003030				// pos track 개수 저장, 개수만큼 poskey 저장
		#define RD_ANIM_NODE_ROTTRACK					0x70003040				// rot track 개수 저장, 개수만큼 rotkey 저장
		#define RD_ANIM_NODE_SCLTRACK					0x70003050				// scl track 개수 저장, 개수만큼 sclkey 저장
	//@>
}