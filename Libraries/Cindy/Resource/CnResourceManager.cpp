//================================================================
// File:               : CnResourceManager.cpp
// Related Header File : CnResourceManager.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 2
//================================================================
#include "../Cindy.h"
#include "CnResourceManager.h"
#include "../Util/CnLog.h"

namespace Cindy
{
	//============================================================================
	__ImplementRtti( Cindy, CnResourceManager, CnObject );
	///Const/Dest
	CnResourceManager::CnResourceManager()
	{
	}
	CnResourceManager::~CnResourceManager()
	{
		Clear();
	}

	//--------------------------------------------------------------
	/** Add Resource
		@remarks			리소스 매니져에서만 등록이 가능하다. 내부적으로 참조 카운트 1을 증가시킨다.
	*/
	//--------------------------------------------------------------
	bool CnResourceManager::Add( CnResource* pRsc )
	{
		MoCriticalSection::ThreadSafe	cs( m_CS );

		std::pair<RscIter, bool> res = m_Resources.insert( RscMap::value_type( pRsc->GetName(), pRsc ) );
		if (!res.second)
		{
			CnPrint( L"[CnResourceManager::Add] same name exist: %s", pRsc->GetName().c_str() );
			return false;
		}

		pRsc->AddRef();
		return true;
	}

	//--------------------------------------------------------------
	/** 리소스 제거
		@remarks
			리소스를 제거한다. 내부적으로 참조 카운트 1을 감소시킨다.
	*/
	//--------------------------------------------------------------
	bool CnResourceManager::Remove( const RscPtr& rRsc )
	{
		return Remove( rRsc->GetName() );
	}

	//--------------------------------------------------------------
	/** 리소스 제거
		@remarks
			리소스를 제거한다. 내부적으로 참조 카운트 1을 감소시킨다.
	*/
	//--------------------------------------------------------------
	bool CnResourceManager::Remove( const CnString& strName )
	{
		MoCriticalSection::ThreadSafe	cs( m_CS );

		RscIter iter = m_Resources.find( strName );
		if( iter == m_Resources.end() )
			return false;

		CnResource* pRsc = iter->second;
		m_Resources.erase( iter );

		pRsc->Release();

		return true;
	}

	//--------------------------------------------------------------
	/** 리소스 제거
		@remarks
			리소스를 제거한다. 내부적으로 참조 카운트 1을 감소시킨다.
	*/
	//--------------------------------------------------------------
	void CnResourceManager::Clear()
	{
		MoCriticalSection::ThreadSafe	cs( m_CS );

		RscIter iend = m_Resources.end();
		for( RscIter i = m_Resources.begin(); i != iend; ++i )
			i->second->Release();	

		m_Resources.clear();
	}

	//--------------------------------------------------------------
	/** 리소스 찾기
		@return
			스마트 포인터를 반환한다.
	*/
	//--------------------------------------------------------------
	RscPtr CnResourceManager::Get( const CnString& strName )
	{
		MoCriticalSection::ThreadSafe	cs( m_CS );

		RscIter iter = m_Resources.find( strName );
		if( m_Resources.end() == iter )
			return NULL;

		iter->second->Access();

		return RscPtr( iter->second );
	}

	//--------------------------------------------------------------
	//	Data Path List
	//--------------------------------------------------------------
	void CnResourceManager::AddDataPath( const CnString& strPath )
	{
		m_DataPathes.push_back( strPath );
	}

	void CnResourceManager::RemoveAllDataPath()
	{
		m_DataPathes.clear();
	}

	//--------------------------------------------------------------
	bool CnResourceManager::FindData( const CnString& strFileName, MoMemStream& rOutput )
	{
		if (rOutput.Open( strFileName, L"rb" ) )
		{
			return true;
		}
		else
		{
			CnString fullPath;

			PathIter iend = m_DataPathes.end();
			for (PathIter i=m_DataPathes.begin(); i != iend; ++i)
			{
				fullPath = (*i);
				fullPath += strFileName;

				if (rOutput.Open( fullPath, L"rb" ))
					return true;
			}
		}

		return false;
	}

}	// end of CnString