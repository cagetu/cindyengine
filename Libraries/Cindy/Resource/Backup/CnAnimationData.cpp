//==================================================================
// File: CnAnimationData.cpp
// Related Header File: CnAnimationData.h
// Original Author: cagetu
// Creation Date: 2005년 12월 5일 월요일
//==================================================================

#include "CnAnimationData.h"
#include "CnAnimationDataManager.h"

#include "RdMax70ChunkDef.h"
#include "CnResourceChunk.h"

#include "CnResourceBaseData.h"

namespace Cindy
{
	//============================================================================
	IMPLEMENT_RTTI( Cindy, CnAniData, CnResource );

	//------------------------------------------------------------------
	CnAniData::CnAniData( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_usStartFrame(0)
		, m_usEndFrame(0)
		, m_usTotalFrame(0)
		, m_ulFrameRate(0)
		, m_ulPriority(0)
		, m_usAniNodeCount(0)
	{
	}

	//------------------------------------------------------------------
	CnAniData::~CnAniData()
	{
		Unload();
	}

	//------------------------------------------------------------------
	bool CnAniData::Load( const MoString& strFileName )
	{
		if( m_bLoaded )
			return false;

		MoMemStream stream;

		m_bLoaded = m_pParent->FindResData( strFileName, stream );
		if( m_bLoaded )
		{
			LoadAniData( stream );
		}
		stream.Close();

		m_strName = strFileName;

		return true;
	}

	//------------------------------------------------------------------
	bool CnAniData::Unload()
	{
		AniNodeDataIter iend = m_AniNodeDatas.end();
		for( AniNodeDataIter i = m_AniNodeDatas.begin(); i != iend; ++i )
		{
			delete (i->second);
		}
		m_AniNodeDatas.clear();

		m_usAniNodeCount = 0;
		m_bLoaded = false;

		return true;
	}

	//------------------------------------------------------------------
	void CnAniData::LoadAniData( MoMemStream& rStream )
	{
		sChunk chunkblock;

		ulong leftsize = rStream.GetSize();
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_ANIM_MAGIC:
				ReadMagic( rStream );
				break;

			case RD_ANIM_VERSION:
				ReadVersion( rStream );
				break;

			case RD_ANIM_TOTALFRAME:
				{
					rStream.Read( &m_usTotalFrame, sizeof(ushort), 1 );

					m_usStartFrame = 0;
					m_usEndFrame = m_usTotalFrame;
				}
				break;

			case RD_ANIM_FRAMERATE:
				{
					rStream.Read( &m_ulFrameRate, sizeof(ulong), 1 );
				}
				break;

			case RD_ANIM_NODELIST:
				{
					ReadAnim( rStream, chunkblock.ulSize );
				}
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
				break;
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	bool CnAniData::ReadMagic( MoMemStream& rStream )
	{
		ulong value = 0;
		rStream.Read( &value, sizeof(ulong), 1 );
		if( RD_ANIMFILE_MAGICNUM != value )
		{
			EXCEPT( L"파일 고유번호가 달라요!" );
			return false;
		}
		return false;
	}

	//------------------------------------------------------------------
	bool CnAniData::ReadVersion( MoMemStream& rStream )
	{
		ulong value = 0;
		rStream.Read( &value, sizeof(ulong), 1 );
		if( RD_ANIMFILE_VERSION != value )
		{
			EXCEPT( L"파일 버전이 달라요!" );
			return false;
		}

		return false;
	}

	//------------------------------------------------------------------
	void CnAniData::ReadAnim( MoMemStream& rStream, ulong ulReadSize )
	{
		sChunk chunkblock;

		ulong leftsize = ulReadSize - ChunkHeaderSize;
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_ANIM_NODE:
				{
					CnAniNodeData* pNode = new CnAniNodeData();

					ReadAnimNode( rStream, chunkblock.ulSize, pNode );

					AddAniNode( pNode );
				}
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnAniData::ReadAnimNode( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode )
	{
		sChunk chunkblock;
		ulong leftsize = ulReadSize - ChunkHeaderSize;

		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_ANIM_NODE_ID:
				{
					ushort id = 0;
					rStream.Read( &id, sizeof(ushort), 1 );

					pNode->SetID( id );
				}
				break;

			case RD_ANIM_NODE_NAME:
				{
					MoString str;
					ReadName( rStream, str );
					pNode->SetName( str );
				}
				break;

			case RD_ANIM_NODE_POSTRACK:
				{
					ReadPosTrack( rStream, chunkblock.ulSize, pNode );
				}
				break;

			case RD_ANIM_NODE_ROTTRACK:
				{
					ReadRotTrack( rStream, chunkblock.ulSize, pNode );
				}
				break;

			case RD_ANIM_NODE_SCLTRACK:
				{
					ReadSclTrack( rStream, chunkblock.ulSize, pNode );
				}
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnAniData::ReadPosTrack( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode )
	{
		ushort count = 0;
		rStream.Read( &count, sizeof(ushort), 1 );

		pNode->m_usPosKeyCount = count;
		pNode->m_akPosKey = new PosKey[ count ];
		for( ushort i = 0; i < count; ++i )
		{
			rStream.Read( &pNode->m_akPosKey[i].frame, sizeof(ushort), 1 );
			rStream.Read( &pNode->m_akPosKey[i].value, sizeof(float), 3 );
		}
	}

	//------------------------------------------------------------------
	void CnAniData::ReadRotTrack( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode )
	{
		ushort count = 0;
		rStream.Read( &count, sizeof(ushort), 1 );

		pNode->m_usRotKeyCount = count;
		pNode->m_akRotKey = new RotKey[ count ];
		for( ushort i = 0; i < count; ++i )
		{
			rStream.Read( &pNode->m_akRotKey[i].frame, sizeof(ushort), 1 );
			rStream.Read( &pNode->m_akRotKey[i].value, sizeof(float), 4 );
		}
	}

	//------------------------------------------------------------------
	void CnAniData::ReadSclTrack( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode )
	{
		ushort count = 0;
		rStream.Read( &count, sizeof(ushort), 1 );

		pNode->m_usScalekeyCount = count;
		pNode->m_akScaleKey = new ScaleKey[ count ];
		for( ushort i = 0; i < count; ++i )
		{
			rStream.Read( &pNode->m_akScaleKey[i].frame, sizeof(ushort), 1 );
			rStream.Read( &pNode->m_akScaleKey[i].value, sizeof(float), 3 );
		}
	}

	//------------------------------------------------------------------
	CnAniNodeData* CnAniData::GetNodeKey( const MoString& strNodeName )
	{
		AniNodeDataIter iter = m_AniNodeDatas.find( strNodeName );
		if( iter != m_AniNodeDatas.end() )
			return iter->second;

		return NULL;
	}

	//------------------------------------------------------------------
	void CnAniData::AddAniNode( CnAniNodeData* pNode )
	{
		m_AniNodeDatas.insert( AniNodeDataMap::value_type( pNode->GetName(), pNode ) );

		++m_usAniNodeCount;
	}

	//------------------------------------------------------------------
	void CnAniData::Save( const MoString& strFileName )
	{
		char filename[256];

		WideCharToMultiByte( CP_ACP, 0, strFileName.c_str(), -1, filename, 256, NULL, NULL );
		FILE* pFile = fopen( filename, "wb" );
		if( NULL == pFile )
			return;

		ulong ulBegin, ulPtr;

		ulong ulNum = RD_ANIMFILE_MAGICNUM;
		CnResourceChunk::WriteBegin( pFile, RD_ANIM_MAGIC, ulPtr, ulBegin );
		{
			fwrite( &ulNum, sizeof(ulong), 1, pFile );
		}
		CnResourceChunk::WriteEnd( pFile, ulPtr, ulBegin );

		ulNum = RD_ANIMFILE_VERSION;
		CnResourceChunk::WriteBegin( pFile, RD_ANIM_VERSION, ulPtr, ulBegin );
		{
			fwrite( &ulNum, sizeof(ulong), 1, pFile );
		}
		CnResourceChunk::WriteEnd( pFile, ulPtr, ulBegin );

		CnResourceChunk::WriteBegin( pFile, RD_ANIM_TOTALFRAME, ulPtr, ulBegin );
		{
			fwrite( &m_usTotalFrame, sizeof(ushort), 1, pFile );
		}
		CnResourceChunk::WriteEnd( pFile, ulPtr, ulBegin );

		CnResourceChunk::WriteBegin( pFile, RD_ANIM_FRAMERATE, ulPtr, ulBegin );
		{
			fwrite( &m_ulFrameRate, sizeof(ulong), 1, pFile );
		}
		CnResourceChunk::WriteEnd( pFile, ulPtr, ulBegin );

		CnResourceChunk::WriteBegin( pFile, RD_ANIM_NODELIST, ulPtr, ulBegin );
		{
			WriteNodes( pFile );
		}
		CnResourceChunk::WriteEnd( pFile, ulPtr, ulBegin );

		fclose( pFile );
	}

	//------------------------------------------------------------------
	void CnAniData::WriteNodes( FILE* pFile )
	{
		ulong ulPtr, ulBegin, ulSubPtr, ulSubBegin;
		
		CnAniNodeData* pNode = NULL;
		
		AniNodeDataIter iend = m_AniNodeDatas.end();
		for( AniNodeDataIter i = m_AniNodeDatas.begin(); i != iend; ++i )
		{
			pNode = (i->second);
			
			CnResourceChunk::WriteBegin( pFile, RD_ANIM_NODE, ulPtr, ulBegin );
			{
				CnResourceChunk::WriteBegin( pFile, RD_ANIM_NODE_ID, ulSubPtr, ulSubBegin );
				{
					ushort id = pNode->GetID();
					fwrite( &id, sizeof(ushort), 1, pFile );
				}
				CnResourceChunk::WriteEnd( pFile, ulSubPtr, ulSubBegin );

				CnResourceChunk::WriteBegin( pFile, RD_ANIM_NODE_NAME, ulSubPtr, ulSubBegin );
				{
					MoString name = pNode->GetName();
					ushort len = (ushort)name.size();

					fwrite( &len, sizeof(ushort), 1, pFile );
					fwrite( name.c_str(), sizeof(WCHAR), len, pFile );
				}
				CnResourceChunk::WriteEnd( pFile, ulSubPtr, ulSubBegin );

				ushort usKeyCount = 0;
				CnResourceChunk::WriteBegin( pFile, RD_ANIM_NODE_POSTRACK, ulSubPtr, ulSubBegin );
				{
					usKeyCount = pNode->GetPosKeyCount();

					fwrite( &usKeyCount, sizeof(ushort), 1, pFile );

					PosKey* pPos = NULL;
					for( ushort i = 0; i < usKeyCount; ++i )
					{
						pPos = pNode->GetPosKey(i);
						
						fwrite( &pPos->frame, sizeof(ushort), 1, pFile );
						fwrite( &pPos->value, sizeof(float), 3, pFile );
					}
				}
				CnResourceChunk::WriteEnd( pFile, ulSubPtr, ulSubBegin );

				CnResourceChunk::WriteBegin( pFile, RD_ANIM_NODE_ROTTRACK, ulSubPtr, ulSubBegin );
				{
					usKeyCount = pNode->GetRotKeyCount();

					fwrite( &usKeyCount, sizeof(ushort), 1, pFile );

					RotKey* pRot = NULL;
					for( ushort i = 0; i < usKeyCount; ++i )
					{
						pRot = pNode->GetRotKey(i);
						
						fwrite( &pRot->frame, sizeof(ushort), 1, pFile );
						fwrite( &pRot->value, sizeof(float), 4, pFile );
					}
				}
				CnResourceChunk::WriteEnd( pFile, ulSubPtr, ulSubBegin );

				CnResourceChunk::WriteBegin( pFile, RD_ANIM_NODE_SCLTRACK, ulSubPtr, ulSubBegin );
				{
					usKeyCount = pNode->GetScalekeyCount();

					fwrite( &usKeyCount, sizeof(ushort), 1, pFile );

					ScaleKey* pScale = NULL;
					for( ushort i = 0; i < usKeyCount; ++i )
					{
						pScale = pNode->GetScaleKey(i);

						fwrite( &pScale->frame, sizeof(ushort), 1, pFile );
						fwrite( &pScale->value, sizeof(float), 3, pFile );
					}
				}
				CnResourceChunk::WriteEnd( pFile, ulSubPtr, ulSubBegin );

			}
			CnResourceChunk::WriteEnd( pFile, ulPtr, ulBegin );
		}
	}
}