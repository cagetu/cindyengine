// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnMeshTexture.h"
#include "CnMeshTextureManager.h"

namespace Cindy
{
	//============================================================================
	IMPLEMENT_RTTI( Cindy, CnMeshTex, CnBaseTex );

	//----------------------------------------------------------------------------
	CnMeshTex::CnMeshTex( CnResourceManager* pParent )
		: CnBaseTex( pParent )
	{
	}

	//----------------------------------------------------------------------------
	CnMeshTex::~CnMeshTex()
	{
	}

	//----------------------------------------------------------------------------
	bool CnMeshTex::Load( const MoString& strFileName )
	{
		if( m_bLoaded )
			return false;

		MoMemStream stream;

		m_bLoaded = m_pParent->FindResData( strFileName, stream );
		if( m_bLoaded )
		{
			//! 텍스쳐 처리..
		}

		stream.Close();

		m_strName = strFileName;

		return true;
	}
}