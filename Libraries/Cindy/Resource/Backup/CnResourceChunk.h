// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNRESOURCECHUNK_H__
#define __CNRESOURCECHUNK_H__

#include "../Cindy.h"
#include <BaseCore/BcMemStream.h>

namespace Cindy
{
	class CN_DLL CnResourceChunk
	{
	public:
		static const ulong ChunkHeaderSize;

		/** 청크 읽기 */
		static void	Read( sChunk *pChunk, BaseCore::CBcMemStream& rStream );

		/** 청크 건너띄기 */
		static bool	Skip( BaseCore::CBcMemStream& rStream, ulong ulSkipPo );
	};
}

#endif	// __CNRESOURCECHUNK_H__