// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnSkeletonData.h"
#include "CnSkeletonDataManager.h"

#include "RdMax70ChunkDef.h"
#include "CnResourceChunk.h"

namespace Cindy
{
	//============================================================================
	IMPLEMENT_RTTI( Cindy, CnSkeletonData, CnResource );

	//------------------------------------------------------------------
	CnSkeletonData::CnSkeletonData( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_usBoneCounts(0)
	{
	}

	//------------------------------------------------------------------
	CnSkeletonData::~CnSkeletonData()
	{
		Unload();
	}

	//------------------------------------------------------------------
	bool CnSkeletonData::Load( const MoString& strFileName )
	{
		if( m_bLoaded )
			return false;

		MoMemStream rStream;

		m_bLoaded = m_pParent->FindResData( strFileName, rStream );
		if( m_bLoaded )
		{
			LoadSkeleton( rStream );
		}
		rStream.Close();

		m_strName = strFileName;

		return true;
	}

	//------------------------------------------------------------------
	bool CnSkeletonData::Unload()
	{
		BoneDataIter iend = m_BoneDatas.end();
		for( BoneDataIter i = m_BoneDatas.begin(); i != iend; ++i )
		{
			delete (i->second);
		}
		m_BoneDatas.clear();

		m_usBoneCounts = 0;

		BBoxIter biend = m_BBoxes.end();
		for( BBoxIter bi = m_BBoxes.begin(); bi != biend; ++bi )
		{
			delete (*bi);
		}
		m_BBoxes.clear();

		m_BoneDefines.clear();

		m_bLoaded = false;

		return false;
	}

	//------------------------------------------------------------------
	void CnSkeletonData::LoadSkeleton( MoMemStream& rStream )
	{
		sChunk	chunkblock;
		ulong	leftsize = rStream.GetSize();
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_SKEL_MAGIC:
				ReadMagic( rStream );
				break;

			case RD_SKEL_VERSION:
				ReadVersion( rStream );
				break;

			case RD_SKEL_BONELIST:
				ReadSkeleton( rStream, chunkblock.ulSize );
				break;

			case RD_SKEL_BBOXLIST:
				ReadBoundingBoxList( rStream, chunkblock.ulSize );
				break;

			case RD_SKEL_BONE_DEFINE_LIST:
				ReadBoneDefineList( rStream, chunkblock.ulSize );
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
				break;
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	bool CnSkeletonData::ReadMagic( MoMemStream& rStream )
	{
		ulong value = 0;
		rStream.Read( &value, sizeof(ulong), 1 );
		if( RD_SKELFILE_MAGICNUM != value )
		{
			EXCEPT( L"파일 고유번호가 달라요!" );
			return false;
		}

		return true;
	}

	//------------------------------------------------------------------
	bool CnSkeletonData::ReadVersion( MoMemStream& rStream )
	{
		ulong value = 0;
		rStream.Read( &value, sizeof(ulong), 1 );
		if( RD_SKELFILE_VERSION != value )
		{
			EXCEPT( L"파일 버전이 달라요!" );
			return false;
		}

		return true;
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadSkeleton( MoMemStream& rStream, ulong ulSize )
	{
		sChunk chunkblock;

		ulong leftsize = ulSize - ChunkHeaderSize;
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_SKEL_BONE:
				{
					CnBoneNodeData* pNode = new CnBoneNodeData();

					BYTE type = 0;
					rStream.Read( &type, sizeof(BYTE), 1 );
					switch( type )
					{
					case 0:
						break;
					case 1:
						pNode->SetNodeType( CnNodeData::ANIMATE );
						break;
					case 2:
						pNode->SetNodeType( CnNodeData::DUMMY );
						break;
					}

					ulong ulLeftChunkSize = chunkblock.ulSize - sizeof(BYTE);
					ReadBone( rStream, ulLeftChunkSize, pNode );

					AddBoneNode( pNode );
				}
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
				break;
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadBone( MoMemStream& rStream, ulong ulSize, CnBoneNodeData* pBoneData )
	{
		sChunk chunkblock;

		ulong leftsize = ulSize - ChunkHeaderSize;
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_SKEL_BONE_ID:
				//! 부모 노드의 ID
				{
					ushort id = 0;
					rStream.Read( &id, sizeof(ushort), 1 );
					pBoneData->SetID( id );
				}
				break;

			case RD_SKEL_BONE_NAME:
				{
					MoString str;
					ReadName( rStream, str );
					pBoneData->SetName( str );
				}
				break;

			case RD_SKEL_BONE_PARENTID:
				//! 본 노드 ID
				{
					ushort id = 0;
					rStream.Read( &id, sizeof(ushort), 1 );
					pBoneData->SetParentID( id );
				}
				break;

			case RD_SKEL_BONE_PARENTNAME:
				{
					MoString str;
					ReadName( rStream, str );
					pBoneData->SetParentName( str );
				}
				break;

			case RD_SKEL_BONE_LOCALTM:
				//! 노드의 변환 행렬의 위치, 회전, 크기 정보 받아오기
				{
					CnVector3 vPos, vScale;
					CnQuaternion qRot;
					rStream.Read( &vPos, sizeof(float), 3 );
					rStream.Read( &qRot, sizeof(float), 4 );
					rStream.Read( &vScale, sizeof(float), 3 );

					pBoneData->SetLocalPos( vPos );
					pBoneData->SetLocalRot( qRot );
					pBoneData->SetLocalScale( vScale );
				}
				break;

			case RD_SKEL_BONE_WORLDTM:
				{
					//! 3x4
					float matrix[12];
					rStream.Read( matrix, sizeof(float), 12 );

					CnMatrix mat( matrix[0], matrix[1], matrix[2], 0.0f,
								  matrix[3], matrix[4], matrix[5], 0.0f,
								  matrix[6], matrix[7], matrix[8], 0.0f,
								  matrix[9], matrix[10], matrix[11], 1.0f );

					pBoneData->SetWorldTM( mat );
				}
				break;

				// BoneNode에 필요하면 나중에 추가하기 바람
			case RD_SKEL_BONE_AABB:
				{
					CnVector3 vPoint;
					rStream.Read( &vPoint, sizeof(float), 3 );
					rStream.Read( &vPoint, sizeof(float), 3 );
				}
				break;

			case RD_SKEL_BONE_OBB:
				{
					CnVector3 center;
					CnVector3 axis[3];
					float extent[3];

					rStream.Read( &center, sizeof(float), 3 );

					ushort i = 0;
					for( i = 0; i < 3; ++i )
					{
						rStream.Read( &axis[i], sizeof(float), 3 );
					}

					for( i = 0; i < 3; ++i )
					{
						rStream.Read( &extent[i], sizeof(float), 1 );
					}
				}
				break;

			//@< 삭제요망 [2006-07-06] cagetu
			case RD_SKEL_BONE_DEFINE:
				{
					ushort type = 0;

					rStream.Read( &type, sizeof(ushort), 1 );

					pBoneData->SetDefineType( type );
				}
				break;
			//@>

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
				break;
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadBoundingBoxList( MoMemStream& rStream, ulong ulSize )
	{
		sChunk chunkblock;

		ulong leftsize = ulSize - ChunkHeaderSize;
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_SKEL_BBOX:
				{
					BoundingBox* pBox = new BoundingBox();
					ReadBoundingBox( rStream, chunkblock.ulSize, pBox );
					m_BBoxes.push_back( pBox );
				}
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
				break;
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadBoundingBox( MoMemStream& rStream, ulong ulSize, BoundingBox* pBBox )
	{
		sChunk chunkblock;

		ulong leftsize = ulSize - ChunkHeaderSize;
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_SKEL_BBOX_PARENTID:
				{
					ushort id = 0;
					rStream.Read( &id, sizeof(ushort), 1 );
					pBBox->ParentID = id;
				}
				break;

			case RD_SKEL_BBOX_PARENTNAME:
				{
					ReadName( rStream, pBBox->ParentName );
				}
				break;

			case RD_SKEL_BBOX_OBB:
				{
					CnVector3 center;
					CnVector3 axis[3];
					float extent[3];

					rStream.Read( &center, sizeof(float), 3 );

					ushort i = 0;
					for( i = 0; i < 3; ++i )
					{
						rStream.Read( &axis[i], sizeof(float), 3 );
					}

					for( i = 0; i < 3; ++i )
					{
						rStream.Read( &extent[i], sizeof(float), 1 );
					}

					pBBox->OBB.SetCenter( center );
					pBBox->OBB.SetAxis( axis );
					pBBox->OBB.SetExtent( extent );
				}
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
				break;
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnSkeletonData::ReadBoneDefineList( MoMemStream& rStream, ulong ulSize )
	{
		sChunk chunkblock;

		ulong leftsize = ulSize - ChunkHeaderSize;
		while( leftsize )
		{
			CnResourceChunk::Read( &chunkblock, rStream );

			switch( chunkblock.ulTag )
			{
			case RD_SKEL_DEFINE:
				{
					ushort index = 0;
					rStream.Read( &index, sizeof(ushort), 1 );

					ushort boneid = 0;
					rStream.Read( &boneid, sizeof(ushort), 1 );

					BoneDefineIter iter = m_BoneDefines.find( index );
					if( iter == m_BoneDefines.end() )
					{
						m_BoneDefines.insert( BoneDefineMap::value_type( index, boneid ) );
					}
				}
				break;

			default:
				CnResourceChunk::Skip( rStream, chunkblock.ulSize - ChunkHeaderSize );
				break;
			}

			leftsize -= chunkblock.ulSize;
		}
	}

	//------------------------------------------------------------------
	void CnSkeletonData::AddBoneNode( CnBoneNodeData* pBoneData )
	{
		m_BoneDatas.insert( BoneDataMap::value_type( pBoneData->GetID(), pBoneData ) );

		m_usBoneCounts++;
	}

	//------------------------------------------------------------------
	CnBoneNodeData* CnSkeletonData::GetBoneNode( ushort usIndex )
	{
		BoneDataIter iter = m_BoneDatas.find( usIndex );
		if( iter == m_BoneDatas.end() )
			return NULL;

		return iter->second;
	}

	//------------------------------------------------------------------
	const CnObb& CnSkeletonData::GetBBoxOBB( ushort usIndex )
	{
		assert( usIndex < (ushort)m_BBoxes.size() );

		return m_BBoxes[usIndex]->OBB;
	}
}