// Copyright (c) 2006~. cagetu
//
//******************************************************************

//----------------------------------------------------------------------------
inline WaveDataPtr::WaveDataPtr()
: CnSmartPtr< CnWaveData >()
{
}

//----------------------------------------------------------------------------
inline WaveDataPtr::WaveDataPtr( CnWaveData* pObject )
: CnSmartPtr< CnWaveData >( pObject )
{
}

//----------------------------------------------------------------------------
inline WaveDataPtr::WaveDataPtr( const WaveDataPtr& spObject )
: CnSmartPtr< CnWaveData >( spObject )
{
}

//----------------------------------------------------------------------------
inline WaveDataPtr::WaveDataPtr( const RscPtr& spRsc )
: CnSmartPtr< CnWaveData >()
{
	m_pObject = (CnWaveData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->IncreaseReferences();
	}
}

//----------------------------------------------------------------------------
inline WaveDataPtr& WaveDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnWaveData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->DecreaseReferences();

		if( !spRsc.IsNull() )
			spRsc->IncreaseReferences();

		m_pObject = (CnWaveData*)spRsc.GetPtr();
	}

	return *this;
}