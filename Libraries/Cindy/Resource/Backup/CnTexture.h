//================================================================
// File:           : CnTexture.h
// Original Author : changhee
// Creation Date   : 2007. 4. 9
//================================================================
#pragma once

#include "CnResource.h"

namespace Cindy
{
	//================================================================
	/** CnTexture
	    @author    changhee
		@since     2007. 4. 9
		@remarks   
	*/
	//================================================================
	class CnTexture : public CnResource
	{
		DECLARE_RTTI;
	public:
		// texture type
		enum Style
		{
			TEXTURE_UNKNOWN,

			TEXTURE_2D,
			TEXTURE_3D,
			TEXTURE_CUBE,
		};

		// pixel formats
		enum Format
		{
			NOFORMAT,
			X8R8G8B8,
			A8R8G8B8,
			R5G6B5,
			A1R5G5B5,
			A4R4G4B4,
			P8,
			DXT1,
			DXT2,
			DXT3,
			DXT4,
			DXT5,
			R16F,                       // 16 bit float, red only
			G16R16F,                    // 32 bit float, 16 bit red, 16 bit green
			A16B16G16R16F,              // 64 bit float, 16 bit rgba each
			R32F,                       // 32 bit float, red only
			G32R32F,                    // 64 bit float, 32 bit red, 32 bit green
			A32B32G32R32F,              // 128 bit float, 32 bit rgba each
			A8,
		};

		// the sides of a cube map
		enum CubeFace
		{
			PosX = 0,
			NegX,
			PosY,
			NegY,
			PosZ,
			NegZ,
		};

		// usage flags
		enum Usage
		{
			CreateEmpty = (1<<0),               // don't load from disk, instead create empty texture
			CreateFromDDSFile = (1<<1),			// create from dds file inside a compound file
			RenderTargetColor = (1<<2),         // is render target, has color buffer
			RenderTargetDepth = (1<<3),         // is render target, has depth buffer
			RenderTargetStencil = (1<<4),       // is render target, has stencil buffer
			Dynamic = (1<<5),                   // is a dynamic texture (for write access with CPU)
		};

		// lock types
		enum LockType
		{
			ReadOnly,       // cpu will only read from texture
			WriteOnly,      // cpu will only write to texture (an overwrite everything!)
		};

		// lock information
		struct LockInfo
		{
			void* surfPointer;
			int   surfPitch;
		};

	protected:
		Style		m_eStyle;
		Format		m_eFormat;

		ushort		m_usUsage;

		ushort		m_usWidth;				// ���� ������
		ushort		m_usHeight;				// ���� ������
		ushort		m_usDepth;				// ����

		ushort		m_usNumMipMaps;			// �Ӹ� ����

		ulong		m_ulColorKey;			// Į��Ű ��(���� �� ������ ���ٴ�..)

		// Constructor
		CnTexture( CnResourceManager* pParent );
	public:
		virtual ~CnTexture();

		void		SetStyle( Style eStyle );
		Style		GetStyle() const;

		void		SetFormat( Format eFormat );
		Format		GetFormat() const;
		ushort		GetBytesPerPixel() const;

		void		SetWidth( ushort usWidth );
		ushort		GetWidth() const;

		void		SetHeight( ushort usHeight );
		ushort		GetHeight() const;

		void		SetDepth( ushort usDepth );
		ushort		GetDepth() const;

		void		SetUsage( ushort usUsage );
		ushort		GetUsage() const;

		ushort		GetNumMipLevels() const;

		bool		IsRenderTarget() const;
	};

#include "CnTexture.inl"
}