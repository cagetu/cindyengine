// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNSKELETONDATAMANAGER_H__
#define __CNSKELETONDATAMANAGER_H__

#include "CnResourceManager.h"

namespace Cindy
{
	//==================================================================
	/** CnSkeletonDataManager
		@author
			cagetu
		@since
			2005월 12월 12일
		@remarks
			Skeleton 리소스 매니져 
		@see
			CnResourceManager
			Momo::MoSingleton
	*/
	//==================================================================
	class CN_DLL CnSkelDataManager : public CnResourceManager, public MoSingleton<CnSkelDataManager>
	{
	public:
		CnSkelDataManager();
		virtual ~CnSkelDataManager();

		/** Load
			@remarks
				Skeleton 데이터를 읽는다.
		*/
		RscPtr		Load( const MoString& strFileName );
	};

#define g_SkelDataMgr		CnSkelDataManager::GetSingleton()
#define g_pSkelDataMgr		CnSkelDataManager::GetSingletonPtr()
}

#endif	// __CNSKELETONDATAMANAGER_H__