//================================================================
// File:               : CnMeshData.inl
// Related Header File : CnMeshData.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 16
//================================================================
//----------------------------------------------------------------
//	MeshData Class
//----------------------------------------------------------------
inline
int CnMeshData::GetNumMeshNodeDatas() const
{
	return m_nNumMeshNodeDatas;
}

//----------------------------------------------------------------
inline
const CnMeshData::MeshNodeList& CnMeshData::GetMeshNodeDatas() const
{
	return m_MeshNodeDatas;
}

//----------------------------------------------------------------
//	MeshDataPtr Class
//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr()
: CnSmartPtr< CnMeshData >()
{
}

//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr( CnMeshData* pObject )
: CnSmartPtr< CnMeshData >( pObject )
{
}

//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr( const MeshDataPtr& spObject )
: CnSmartPtr< CnMeshData >( spObject )
{
}

//----------------------------------------------------------------
inline
MeshDataPtr::MeshDataPtr( const RscPtr& spRsc )
: CnSmartPtr< CnMeshData >()
{
	m_pObject = (CnMeshData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->IncreaseReferences();
	}
}

//----------------------------------------------------------------
inline
MeshDataPtr& MeshDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnMeshData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->DecreaseReferences();

		if( !spRsc.IsNull() )
			spRsc->IncreaseReferences();

		m_pObject = (CnMeshData*)spRsc.GetPtr();
	}

	return *this;
}