// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNWAVEDATAMANAGER_H__
#define __CNWAVEDATAMANAGER_H__

#include "CnResourceManager.h"

namespace Cindy
{
	//==================================================================
	/** AvaWaveMgr
		@author
			kwan, cagetu
		@since
			2006년 08월 01일 
		@remarks
			사운드 리소스 관리자
		@see
			AvaRscMgr, MoSingleton
	*/
	//==================================================================
	class CN_DLL CnWaveDataManager : public CnResourceManager, MoSingleton< CnWaveDataManager >
	{
	public:
		CnWaveDataManager();
		virtual ~CnWaveDataManager();

		/** 사운드 데이터 로드
		*/
		RscPtr			Load( const MoString& strFileName );
	};

#define g_WaveDataMgr		CnWaveDataManager::GetSingleton()
#define g_pWaveDataMgr		CnWaveDataManager::GetSingletonPtr()
}

#endif	// __CNWAVEDATAMANAGER_H__