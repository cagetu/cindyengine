// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnMeshTextureManager.h"
#include "CnMeshTexture.h"

namespace Cindy
{
	//----------------------------------------------------------------------------
	CnMeshTexManager::CnMeshTexManager()
	{
	}

	//----------------------------------------------------------------------------
	CnMeshTexManager::~CnMeshTexManager()
	{
	}

	//----------------------------------------------------------------------------
	RscPtr CnMeshTexManager::Load( const MoString& strFileName )
	{
		MeshTexPtr spTex = GetRsc( strFileName );
		if( spTex.IsNull() )
		{
			CnMeshTex* pTex = new CnMeshTex( this );

			if( pTex->Load( strFileName ) )
			{
				AddRsc( pTex );
				return pTex;
			}
			else
			{
				SAFEDEL( pTex );
			}
		}
		else
		{
			if( !spTex->IsLoaded() )
			{
				spTex->Load( strFileName );
				return spTex.GetPtr();
			}
		}

		return NULL;
	}
}
