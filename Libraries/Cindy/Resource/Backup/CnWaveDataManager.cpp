// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnWaveDataManager.h"
#include "CnWaveData.h"

namespace Cindy
{
	//------------------------------------------------------------------
	CnWaveDataManager::CnWaveDataManager()
	{
	}

	//------------------------------------------------------------------
	CnWaveDataManager::~CnWaveDataManager()
	{
		RemoveAllRsc();
	}

	//------------------------------------------------------------------
	RscPtr CnWaveDataManager::Load( const MoString& strFileName )
	{
		WaveDataPtr spWave = GetRsc( strFileName );
		if( !spWave.IsNull() )
			return spWave.GetPtr();

		CnWaveData* pWaveData = new CnWaveData( this );

		if( pWaveData->Load( strFileName ) )
		{
			AddRsc( pWaveData );
			return pWaveData;
		}

		SAFEDEL( pWaveData );
		return NULL;
	}
}