// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNMESHTEXTURE_H__
#define __CNMESHTEXTURE_H__

#include "CnBaseTexture.h"

namespace Cindy
{
	//==================================================================
	/** CnMeshTex
		@author
			cagetu
		@since
			2006년 9월 30일
		@remarks
			메쉬 텍스쳐 클래스
	*/
	//==================================================================
	class CN_DLL CnMeshTex : public CnBaseTex
	{
		friend class CnMeshTexManager;

		DECLARE_RTTI;
	private:
		CnMeshTex( CnResourceManager* pParent );

	public:
		virtual ~CnMeshTex();

		/** Unload
			@remarks
				메쉬 용 텍스쳐 객체를 Load 한다.
			@see
				CnResource::Load()
		*/
		bool		Load( const MoString& strFileName );
	};

	//==================================================================
	/** MeshTexPtr
		@author
			cagetu
		@since
			2006년 9월 30일
		@remarks
			메쉬 텍스쳐용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL MeshTexPtr : public CnSmartPtr< CnMeshTex >
	{
	public:
		MeshTexPtr();
		explicit MeshTexPtr( CnMeshTex* pObject );
		MeshTexPtr( const MeshTexPtr& spObject );
		MeshTexPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		MeshTexPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnMeshTexture.inl"
}

#endif	// __CNMESHTEXTURE_H__