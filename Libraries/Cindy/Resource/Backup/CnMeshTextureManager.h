// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNMESHTEXTUREMANAGER_H__
#define __CNMESHTEXTUREMANAGER_H__

#include "CnBaseTextureManager.h"

namespace Cindy
{
	//==================================================================
	/** CnMeshTexManager
		@author
			cagetu
		@since
			2006년 9월 30일
		@remarks
			메쉬 텍스쳐 클래스 관리자
		@see
			CnResourceManager
			Momo::MoSingleton
	*/
	//==================================================================
	class CnMeshTexManager : public CnBaseTexManager, public MoSingleton<CnMeshTexManager>
	{
	public:
		// 생성자 / 소멸자
		CnMeshTexManager();
		virtual ~CnMeshTexManager();

		/** 리소스 읽기
			@remarks
				메쉬 텍스쳐 리소스를 읽는다.
			@see
				CnResourceManager::Load()
		*/
		RscPtr			Load( const MoString& strFileName );
	};

#define g_MeshTexMgr		CnMeshTexManager::GetSingleton()
#define g_pMeshTexMgr		CnMeshTexManager::GetSingletonPtr()
}

#endif	// __CNMESHTEXTUREMANAGER_H__