//================================================================
// File:               : CnMeshData.cpp
// Related Header File : CnMeshData.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 10
//================================================================
#include "../Cindy.h"
#include "CnMeshData.h"

#include "../System/CnString.h"

#include "../RenderDx9/CnDx9VertexDeclaration.h"
#include "../RenderDx9/CnDx9VertexBuffer.h"
#include "../RenderDx9/CnDx9IndexBuffer.h"

#include <BaseCore/BcFileStream.h>

namespace Cindy
{
	//================================================================
	IMPLEMENT_RTTI( Cindy, CnMeshData, CnResource );

	//================================================================
	/** 생성자
		@remarks			이 리소스를 관리하는 매니져만 생성 가능하다.
		@param pParent		부모 관리자
	*/
	//================================================================
	CnMeshData::CnMeshData( CnResourceManager* pParent )
		: CnResource( pParent )
	{
	}
	CnMeshData::~CnMeshData()
	{
	}

	//================================================================
	/** OnLost
		@remarks	디바이스를 잃었을 때..
	*/
	//================================================================
	void CnMeshData::OnLost()
	{
	}

	//================================================================
	/** OnLost
		@remarks	디바이스를 복구 했을 때
	*/
	//================================================================
	void CnMeshData::OnRestore()
	{
	}

	//================================================================
	/** Load
	    @remarks      메쉬 로딩
		@param        strFileName : 파일 이름
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::Load( const CnString& strFileName )
	{
		using namespace BaseCore;

		CBcFileStream file;
		if( !file.Open( strFileName.c_str(), L"rb" ) )
			return false;

		m_strName = strFileName;

		file.Seek( 0, SEEK_END );
		m_ulTotalDataSize = file.Tell();
		file.Seek( 0, SEEK_SET );

		void* buffer = malloc( m_ulTotalDataSize );
		file.Read( buffer, sizeof(char), m_ulTotalDataSize );
		file.Close();
		{
			m_DataMemStream.SetBuffer( (unsigned char*)buffer, m_ulTotalDataSize );

			if( !LoadData() )
			{
				free( buffer );
				buffer = 0;
				return false;
			}
		}
		free( buffer );
		buffer = 0;

		SetState( Loaded );
		return true;
	}

	//================================================================
	/** Unload
	    @remarks      메쉬 해제
		@param        none
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::Unload()
	{
		SetState( Unloaded );
		return true;
	}

	//================================================================
	/** LoadData
	    @remarks      데이터 읽기 시작
		@param        rStream : 메모리 스트림
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::LoadData()
	{
		using namespace BaseCore;

		SBcChunkHdrL chunk;
		ulong leftsize = m_ulTotalDataSize;
		while( leftsize )
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch( chunk.ulTag )
			{
			case RD_MESH_MAGIC:
				{
					if( !ReadMagicNumber() )
						return false;
				}
				break;

			case RD_MESH_VERSION:
				{
					if( !ReadFileVersion() )
						return false;
				}
				break;

			case RD_MESH_NODELIST:
				{
					if( !ReadMeshNodeList( chunk.ulSize ) )
						return false;
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - SBcChunkHdrL::s_ulHeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}

		return true;
	}

	//================================================================
	/**	Read Magic Number
		@remarks
			파일 고유 번호를 읽어온다.
	*/
	//================================================================
	bool CnMeshData::ReadMagicNumber()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if( RD_MESHFILE_MAGICNUM != result )
		{
			Assert( RD_MESHFILE_MAGICNUM == result, L"[CnMeshData::ReadMagicNumber] different file unique number" );
			return false;
		}

		return true;
	}

	//================================================================
	/** Read File Version
		@remarks
			파일 버전을 읽어온다.
	*/
	//================================================================
	bool CnMeshData::ReadFileVersion()
	{
		ulong result = 0;
		m_DataMemStream.Read( &result, sizeof(ulong), 1 );
		if( RD_MESHFILE_VERSION != result )
		{
			Assert( RD_MESHFILE_VERSION == result, L"[CnMeshData::ReadFileVersion] different file version" );
			return false;
		}

		return true;
	}

	//================================================================
	/** Read Mesh Node List
	    @remarks      리소스 메쉬 노드 목록을 읽어온다.
		@param        ulDataSize : 읽어야 할 데이터 사이즈
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeList( ulong ulDataSize )
	{
		BaseCore::SBcChunkHdrL chunk;

		ulong leftsize = ulDataSize - BaseCore::SBcChunkHdrL::s_ulHeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag)
			{
			case RD_MESH_NODE:
				{
					ReadMeshNode( chunk.ulSize );
				}
				break;

			//case RD_MESH_NFNODE:
			//	{
			//	}
			//	break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - BaseCore::SBcChunkHdrL::s_ulHeaderSize );
				break;
			}

			leftsize -= chunk.ulSize;
		}

		return true;
	}

	//================================================================
	/** Read Mesh Node
	    @remarks      리소스 메쉬 노드를 읽어온다.
		@param        ulDataSize : 읽어야 할 데이터 사이즈
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnMeshData::ReadMeshNode( ulong ulDataSize )
	{
		BaseCore::SBcChunkHdrL chunk;

		BYTE nodetype = 0;
		m_DataMemStream.Read( &nodetype, sizeof(BYTE), 1 );
		NodeData* node = CreateNodeData( nodetype );

		ulong leftsize = ulDataSize - sizeof(BYTE) - BaseCore::SBcChunkHdrL::s_ulHeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag)
			{
			// 현재 노드 정보
			case RD_MESH_NODE_ID:
				{
					m_DataMemStream.Read( &node->id, sizeof(ushort), 1 );
				}
				break;
			case RD_MESH_NODE_NAME:
				{
					if( node->id == INVALID_NODE_ID )
						break;

					//! Unique한 이름을 위해, 파일이름 + 노드이름
					CnString filename, name;
					CnStringUtil::SplitPathFileName( m_strName, filename );
					ReadNameData( name );
					node->name = filename + L"@" + name;
				}
				break;

			// 부모 노드 정보
			case RD_MESH_NODE_PARENTID:
				{
					m_DataMemStream.Read( &node->parent_id, sizeof(ushort), 1 );
				}
				break;
			case RD_MESH_NODE_PARENTNAME:
				{
					//! 우리는 1000번 이상 부터 MeshNode 이다..... 기억하라~!!!!
					if( node->parent_id >= 1000 )
					{
						//! Unique한 이름을 위해, 파일이름 + 노드이름
						CnString filename, name;
						CnStringUtil::SplitPathFileName( m_strName, filename );
						ReadNameData( name );

						if( node->parent_id != INVALID_NODE_ID )
						{
							node->parent_name = filename + L"@" + name;
						}
					}
					else
					{
						ReadNameData( node->parent_name );
					}
				}
				break;

			// 노드 변환 행렬
			case RD_MESH_NODE_LOCALTM:
				{
					m_DataMemStream.Read( &node->local_pos, sizeof(float), 3 );
					m_DataMemStream.Read( &node->local_rot, sizeof(float), 4 );
					m_DataMemStream.Read( &node->local_scl, sizeof(float), 3 );
				}
				break;
			case RD_MESH_NODE_WORLDTM:
				{
					float matrix[12];		// 3x4
					m_DataMemStream.Read( matrix, sizeof(float), 12 );

					node->worldTM._11 = matrix[0];
					node->worldTM._12 = matrix[1];
					node->worldTM._13 = matrix[2];
					node->worldTM._14 = 0.0f;

					node->worldTM._21 = matrix[3];
					node->worldTM._22 = matrix[4];
					node->worldTM._23 = matrix[5];
					node->worldTM._24 = 0.0f;

					node->worldTM._31 = matrix[6];
					node->worldTM._32 = matrix[7];
					node->worldTM._33 = matrix[8];
					node->worldTM._34 = 0.0f;

					node->worldTM._41 = matrix[9];
					node->worldTM._42 = matrix[10];
					node->worldTM._43 = matrix[11];
					node->worldTM._44 = 1.0f;
				}
				break;

			// Skin mesh의 경우, 본 정보
			case RD_MESH_NODE_LINKEDBONE:
				{
					Assert( NodeData::MESH == node->type, L"Invalid Node Data" );
					MeshNodeData* meshnode = static_cast<MeshNodeData*>(node);

					if (meshnode->skindata == 0)
						meshnode->skindata = new SkinnedData();

					m_DataMemStream.Read( &meshnode->skindata->num_influence_bones, sizeof(ushort), 1 );

					if (meshnode->skindata->num_influence_bones > 0)
					{
						meshnode->skindata->influence_bone_indices = new ushort [meshnode->skindata->num_influence_bones];
						m_DataMemStream.Read( meshnode->skindata->influence_bone_indices, sizeof(ushort), meshnode->skindata->num_influence_bones );
					}
				}
				break;
			case RD_MESH_NODE_MAXLINK:
				{
					Assert( NodeData::MESH == node->type, L"Invalid Node Data" );
					MeshNodeData* meshnode = static_cast<MeshNodeData*>(node);

					if (meshnode->skindata == 0)
						meshnode->skindata = new SkinnedData();

					m_DataMemStream.Read( &meshnode->skindata->num_vertex_blend, sizeof(BYTE), 1 );
				}
				break;

			// 버텍스 정보
			case RD_MESH_NODE_VERTEX_COLOR:
				{
					BYTE enable = false;

					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
				}
				break;
			case RD_MESH_NODE_VERTEX:
				{
					Assert( NodeData::MESH == node->type, L"Invalid Node Data" );
					MeshNodeData* meshnode = static_cast<MeshNodeData*>(node);

					ReadMeshNodeVertices( chunk.ulSize, meshnode );
				}
				break;

			// 인덱스 정보
			case RD_MESH_NODE_FACELIST:
				{
					Assert( NodeData::MESH == node->type, L"Invalid Node Data" );
					MeshNodeData* meshnode = static_cast<MeshNodeData*>(node);

					ReadMeshNodeFaces( chunk.ulSize, meshnode );
				}
				break;

			// BoundingBox
			case RD_MESH_NODE_AABB:
				{
					Assert( NodeData::MESH == node->type, L"Invalid Node Data" );
					MeshNodeData* meshnode = static_cast<MeshNodeData*>(node);

					ReadAABB( meshnode );
				}
				break;
			case RD_MESH_NODE_OBB:
				{
					Assert( NodeData::MESH == node->type, L"Invalid Node Data" );
					MeshNodeData* meshnode = static_cast<MeshNodeData*>(node);

					ReadOBB( meshnode );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - BaseCore::SBcChunkHdrL::s_ulHeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		Assert( NodeData::MESH == node->type, L"Invalid Node Data" );
		MeshNodeData* meshnode = static_cast<MeshNodeData*>(node);

		m_MeshNodeDatas.push_back( meshnode );
		return true;
	}

	//================================================================
	/** Read Mesh Node Vertices
	    @remarks      메쉬 노드 버텍스 정보
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeVertices( ulong ulDataSize, MeshNodeData* pMeshNodeData )
	{
		CnVertexDeclaration* vertexdeclaration = CnRenderer::Instance()->CreateVertexDeclaration();

		// 버텍스 개수
		ushort num_vertices = 0;
		m_DataMemStream.Read( &num_vertices, sizeof(ushort), 1 );

		CnVector3* positions;
		CnVector3* normals;
		CnVector3* colors;
		CnVector2* texcoords;

		int vertexcomponent = 0;

		BaseCore::SBcChunkHdrL chunk;
		ulong leftsize = ulDataSize - sizeof(ushort) - BaseCore::SBcChunkHdrL::s_ulHeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag)
			{
			case RD_MESH_NODE_VERTEX_POS:
				{
					positions = new CnVector3[num_vertices];
					m_DataMemStream.Read( positions, sizeof(float), 3*num_vertices );

					vertexcomponent |= CnVertexDeclaration::Position;
				}
				break;
			case RD_MESH_NODE_VERTEX_NORM:
				{
					normals = new CnVector3[num_vertices];
					m_DataMemStream.Read( normals, sizeof(float), 3*num_vertices );

					vertexcomponent |= CnVertexDeclaration::Normal;
				}
				break;
			case RD_MESH_NODE_VERTEX_UV:
				{
					texcoords = new CnVector2[num_vertices];
					m_DataMemStream.Read( texcoords, sizeof(float), 2*num_vertices );

					vertexcomponent |= CnVertexDeclaration::Uv0;
				}
				break;
			case RD_MESH_NODE_VERTEX_BLEND:
				{
				}
				break;
			case RD_MESH_NODE_VERTEX_COLOR:
				{
					colors = new CnVector3[num_vertices];
					m_DataMemStream.Read( colors, sizeof(float), 3*num_vertices );

					vertexcomponent |= CnVertexDeclaration::Color;
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - BaseCore::SBcChunkHdrL::s_ulHeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		vertexdeclaration->SetUsageComponent( vertexcomponent );

		//@<
		pMeshNodeData->vertexbuffer = CreateVertexBuffer( num_vertices, vertexdeclaration );
		Assert( pMeshNodeData->vertexbuffer, L"[CnMeshData::ReadMeshNodeVertices] Could not create vertexbuffer" );
		//@>

		float* buffer = pMeshNodeData->vertexbuffer->Lock( 0, 0, 0 );
		if (buffer)
		{
			int nComponent = vertexdeclaration->GetUsageComponent();
			uint vertexsize = vertexdeclaration->GetSizeOfVertex();
			for( ushort i=0; i < num_vertices; ++i )
			{
				if (nComponent & CnVertexDeclaration::Position)
				{
					memcpy( buffer, positions[i], sizeof(float)*3 );
					buffer += 3;
				}
				if (nComponent & CnVertexDeclaration::Normal)
				{
					memcpy( buffer, normals[i], sizeof(float)*3 );
					buffer += 3;
				}
				if (nComponent & CnVertexDeclaration::Uv0)
				{
					memcpy( buffer, texcoords[i], sizeof(float)*2 );
					buffer += 2;
				}
				//if (nComponent & Uv1)      size += 2;
				//if (nComponent & Uv2)      size += 2;
				//if (nComponent & Uv3)      size += 2;
				if (nComponent & CnVertexDeclaration::Color)
				{
					float color[4];
					color[0] = colors[i].x;
					color[1] = colors[i].y;
					color[2] = colors[i].z;
					color[3] = 1.0f;

					memcpy( buffer, color, sizeof(float)*4 );
					buffer += 4;
				}

				//if (nComponent & Tangent)  size += 3;
				//if (nComponent & Binormal) size += 3;
				//if (nComponent & BlendWeights1)	size += 1;
				//if (nComponent & BlendIndices1)	size += 1;
				//if (nComponent & BlendWeights2) size += 2;
				//if (nComponent & BlendIndices2) size += 2;
				//if (nComponent & BlendWeights3) size += 3;
				//if (nComponent & BlendIndices3) size += 3;
				//if (nComponent & BlendWeights4) size += 4;
				//if (nComponent & BlendIndices4) size += 4;
			}
		}
		pMeshNodeData->vertexbuffer->Unlock();

		return true;
	}

	//================================================================
	/** Read Mesh Node Indices
	    @remarks      메쉬 노드 인덱스 정보
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeFaces( ulong ulDataSize, MeshNodeData* pMeshNodeData )
	{
		BaseCore::SBcChunkHdrL chunk;

		ulong leftsize = ulDataSize - BaseCore::SBcChunkHdrL::s_ulHeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag) 
			{
			case RD_MESH_NODE_FACE:
				{
					ReadMeshNodeFaceGroup( chunk.ulSize, pMeshNodeData );
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - BaseCore::SBcChunkHdrL::s_ulHeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		return true;
	}

	//================================================================
	/** Read Mesh Node Indices
	    @remarks      메쉬 노드 인덱스 정보
	*/
	//================================================================
	bool CnMeshData::ReadMeshNodeFaceGroup( ulong ulDataSize, MeshNodeData* pMeshNodeData )
	{
		ushort num_faces = 0;
		m_DataMemStream.Read( &num_faces, sizeof(ushort), 1 );
		if( num_faces <= 0 )
			return false;

		SubMeshData* submeshdata = new SubMeshData();
		submeshdata->indexbuffer = CreateIndexBuffer( num_faces*3 );
		Assert( submeshdata->indexbuffer, L"[CnMeshData::ReadMeshNodeFaceGroup] Could not create indexbuffer" );

		BaseCore::SBcChunkHdrL chunk;
		ulong leftsize = ulDataSize - sizeof(ushort) - BaseCore::SBcChunkHdrL::s_ulHeaderSize;
		while (leftsize)
		{
			if( !m_DataMemStream.ReadChunkHeaderL( &chunk ) )
				break;

			switch (chunk.ulTag) 
			{
			case RD_MESH_NODE_FACE_INDICES:
				{
					ushort* indices = submeshdata->indexbuffer->Lock( 0, 0, 0 );
					if( indices )
					{
						m_DataMemStream.Read( indices, sizeof(ushort), submeshdata->indexbuffer->GetNumIndices() );
					}
					submeshdata->indexbuffer->Unlock();
				}
				break;

			case RD_MESH_NODE_FACE_DIMAP:
				{
					CnString map, filename;
					ReadNameData( map );

					CnStringUtil::SplitPathFileName( map, filename );
					submeshdata->diffuseMap = filename + L".dds";
				}
				break;
			case RD_MESH_NODE_FACE_SPMAP:
				{
					CnString map, filename;
					ReadNameData( map );

					CnStringUtil::SplitPathFileName( map, filename );
					submeshdata->specularMap = filename + L".dds";
				}
				break;

			case RD_MESH_NODE_FACE_TWOSIDE:
				{
					BYTE enable = 0;
					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
					submeshdata->twoside = (enable==1) ? true : false;
				}
				break;
			case RD_MESH_NODE_FACE_OPACITY:
				{
					BYTE enable = 0;
					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
					submeshdata->opacity = (enable==1) ? true : false;
				}
				break;
			case RD_MESH_NODE_FACE_BLENDING:
				{
					BYTE enable = 0;
					m_DataMemStream.Read( &enable, sizeof(BYTE), 1 );
					submeshdata->blending = (enable==1) ? true : false;
				}
				break;

			default:
				m_DataMemStream.SkipChunkL( chunk.ulSize - BaseCore::SBcChunkHdrL::s_ulHeaderSize );
				break;
			} // switch

			leftsize -= chunk.ulSize;
		} // while

		pMeshNodeData->submeshdatas.push_back( submeshdata );
		return true;
	}

	//================================================================
	/** ReadAABB
	    @remarks      AABB 정보 설정
	*/
	//================================================================
	void CnMeshData::ReadAABB( MeshNodeData* pMeshNodeData )
	{
		CnVector3 min, max;
		m_DataMemStream.Read( &min, sizeof(float), 3 );
		m_DataMemStream.Read( &max, sizeof(float), 3 );

		pMeshNodeData->aabb.SetExtents( min, max );
	}

	//================================================================
	/** ReadOBB
	    @remarks      OBB 정보 설정
	*/
	//================================================================
	void CnMeshData::ReadOBB( MeshNodeData* pMeshNodeData )
	{
		CnVector3 center;
		CnVector3 axis[3];
		float extent[3];

		m_DataMemStream.Read( &center, sizeof(float), 3 );

		ushort i = 0;
		for( i = 0; i < 3; ++i )
			m_DataMemStream.Read( &axis[i], sizeof(float), 3 );

		for( i = 0; i < 3; ++i )
			m_DataMemStream.Read( &extent[i], sizeof(float), 1 );

		pMeshNodeData->obb.SetCenter( center );
		pMeshNodeData->obb.SetAxis( axis );
		pMeshNodeData->obb.SetExtent( extent );
	}

	//================================================================
	/** Create Node Data
	    @remarks      리소스 노드 데이터를 생성한다.
	*/
	//================================================================
	NodeData* CnMeshData::CreateNodeData( BYTE Type )
	{
		NodeData* result = NULL;

		switch (Type)
		{
		case NodeData::MESH:
			{
				result = new MeshNodeData();
				result->type = NodeData::MESH;
			}
			break;

		case NodeData::DUMMY:
			{
				result = new NodeData();
				result->type = NodeData::DUMMY;
			}
			break;

		case NodeData::BONE:
			{
				result = new NodeData();
				result->type = NodeData::BONE;
			}
			break;
		}
		return result;
	}

	//================================================================
	/** Create VerteBuffer
	    @remarks      버텍스 버퍼를 생성한다.
	*/
	//================================================================
	CnVertexBuffer* CnMeshData::CreateVertexBuffer( ushort usNumVertices, CnVertexDeclaration* pVertexDeclaration )
	{
		CnVertexBuffer* vertexbuffer = CnRenderer::Instance()->CreateVertexBuffer();

		//! 풀 메쉬일 경우, System 메모리에 버퍼를 잡는다.
		D3DPOOL pool;
		DWORD usage = 0;
		//if( m_bSystemMem )
		//{
		//	pool = D3DPOOL_SYSTEMMEM;
		//	usage = D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC;
		//}
		//else
		{
			pool = D3DPOOL_MANAGED;
			usage = D3DUSAGE_WRITEONLY;
		}

		if( vertexbuffer->Create( usNumVertices, pVertexDeclaration, usage, pool ) )
			return vertexbuffer;

		delete vertexbuffer;
		return NULL;
	}

	//================================================================
	/** Create IndexBuffer
	    @remarks      인덱스 버퍼를 생성한다.
	*/
	//================================================================
	CnIndexBuffer* CnMeshData::CreateIndexBuffer( ushort usNumIndices )
	{
		CnIndexBuffer* buffer = new CnDx9IndexBuffer();

		D3DPOOL pool;
		DWORD usage = 0;
		//if( m_bSystemMem )
		//{
		//	pool = D3DPOOL_SYSTEMMEM;
		//	usage = D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC;
		//}
		//else
		{
			pool = D3DPOOL_MANAGED;
			usage = D3DUSAGE_WRITEONLY;
		}

		if( buffer->Create( usNumIndices, usage, pool ) )
			return buffer;

		delete buffer;
		return NULL;
	}

	//================================================================
	/** Read Name Data
	    @remarks      이름 정보를 읽어온다.
	*/
	//================================================================
	void CnMeshData::ReadNameData( CnString& rResult )
	{
		ushort length = 0;
		m_DataMemStream.Read( &length, sizeof(ushort), 1 );
		if( length > 0 )
		{
			wchar buffer[128];
			memset( buffer, 0, sizeof(wchar) * 128 );
			m_DataMemStream.Read( buffer, sizeof(wchar), (ulong)length );

			rResult = buffer;
		}
	}
}