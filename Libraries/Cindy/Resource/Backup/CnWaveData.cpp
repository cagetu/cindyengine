// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnWaveData.h"
#include "CnWaveDataManager.h"

#include "../SoundSystem/CnDirectSoundManager.h"

namespace Cindy
{
	//============================================================================
	IMPLEMENT_RTTI( Cindy, CnWaveData, CnResource );

	//------------------------------------------------------------------
	CnWaveData::CnWaveData( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_pBuffer(0)
		, m_pWaveFormat(0)
		, m_hmmio(0)
		, m_ulBufferSize(0)
	{
		memset( &m_ckRiff, 0, sizeof(MMCKINFO) );
	}

	//------------------------------------------------------------------
	CnWaveData::~CnWaveData()
	{
		Unload();
	}

	//------------------------------------------------------------------
	bool CnWaveData::Load( const MoString& strFileName )
	{
		if( m_bLoaded )
			return false;

		MoMemStream rStream;

		m_bLoaded = m_pParent->FindResData( strFileName, rStream );
		if( m_bLoaded )
		{
			if( !LoadData( rStream ) )
			{
				m_bLoaded = false;
				rStream.Close();
				return false;
			}
		}
		rStream.Close();

		m_strName = strFileName;

		return true;
	}

	//------------------------------------------------------------------
	bool CnWaveData::Unload()
	{
		if( m_bLoaded )
		{
			SAFEREL( m_pBuffer );
			SAFEDELS( m_pWaveFormat );
			mmioClose( m_hmmio, 0 );

			m_hmmio = NULL;
			m_bLoaded = false;
		}

		return true;
	}

	//------------------------------------------------------------------
	bool CnWaveData::LoadData( MoMemStream& rStream )
	{
		return true;
	}

	//------------------------------------------------------------------
	bool CnWaveData::Read( MoMemStream& rStream )
	{
		// 메모리 버퍼에서 읽기 작업을 지시한다.
		MMIOINFO mmioInfo;
		ZeroMemory( &mmioInfo, sizeof(mmioInfo) );

		mmioInfo.fccIOProc = FOURCC_MEM;
		mmioInfo.cchBuffer = rStream.GetSize();
		mmioInfo.pchBuffer = (char*)rStream.GetBuffer();

		// 메모리 버퍼를 연다.
		m_hmmio = mmioOpen( NULL, &mmioInfo, MMIO_ALLOCBUF | MMIO_READ );
		if( NULL == m_hmmio )
			return E_FAIL;

		// 웨이브 파일인지 점검하고, 웨이브 형식의 헤더 구조체를 위한 메모리를 할당한다.
		// 구조체 각 필드의 값을 채운다. 
		if( !ReadMMIO() )
		{
			LOGERR( L"[CnWaveData::Read]: ReadMMIO() Function E_FAIL" );
			return false;
		}

		return CalcBufferSize();
	}

	//------------------------------------------------------------------
	bool CnWaveData::ReadMMIO()
	{
		if( ( 0 != mmioDescend( m_hmmio, &m_ckRiff, NULL, 0 ) ) )
			return false;

		// 유효한 웨이브 파일인지를 점검한다. 
		if( ( m_ckRiff.ckid != FOURCC_RIFF ) ||
			( m_ckRiff.fccType != mmioFOURCC( 'W', 'A', 'V', 'E' ) ) )
			return false;

		MMCKINFO ckIn;           // 범용 청크 정보 

		// 파일에서 'fmt' 청크를 찾는다.
		ckIn.ckid = mmioFOURCC( 'f', 'm', 't', ' ' );

		// 이 함수를 이용해 포맷 청크가 있는 곳으로 읽을 위치를 옮겨간다.
		// 그 다음 청크를 점검하여 현재 다루고 있는 파일이 PCM 파일인지 확인
		if( 0 != mmioDescend( m_hmmio, &ckIn, &m_ckRiff, MMIO_FINDCHUNK ) )
			return false;

		// 올바른 데이터인지 체크
		if( ckIn.cksize < (long)sizeof(PCMWAVEFORMAT) )
			return false;

		// 로딩시 사용할 임시 PCM 구조체 
		PCMWAVEFORMAT pcmWaveFormat = NULL;

		// 실제 'fmt' 청크 데이터 <pcmWaveFormat>을 읽는다.
		if( mmioRead( m_hmmio, (char*)&pcmWaveFormat, sizeof(pcmWaveFormat) )
			!= sizeof(pcmWaveFormat) )
			return false;

		// pcm 포멧인지 확인후, 정보를 세팅한다.
		if( pcmWaveFormat.wf.wFormatTag == WAVE_FORMAT_PCM )
		{
			m_pWaveFormat = (WAVEFORMATEX*)(new char[ sizeof(WAVEFORMATEX) ]);
			if( NULL == m_pWaveFormat )
				return false;

			// Copy the bytes from the pcm structure to the waveformatex structure
			memcpy( m_pWaveFormat, &pcmWaveFormat, sizeof(pcmWaveFormat) );
			m_pWaveFormat->cbSize = 0;
		}
		else
		{			
			// Read in length of extra bytes.
			ushort cbExtraBytes = 0L;
			if( mmioRead( m_hmmio, (char*)&cbExtraBytes, sizeof(ushort) ) != sizeof(ushort) )
				return false;

			m_pWaveFormat = (WAVEFORMATEX*)(new char[ sizeof(WAVEFORMATEX) + cbExtraBytes ]);
			if( NULL == m_pWaveFormat )
				return false;

			// Copy the bytes from the pcm structure to the waveformatex structure
			memcpy( m_pWaveFormat, &pcmWaveFormat, sizeof(pcmWaveFormat) );
			m_pWaveFormat->cbSize = cbExtraBytes;

			// Now, read those extra bytes into the structure, if cbExtraAlloc != 0.
			if( mmioRead( m_hmmio, (char*)( ( (uchar*)&(m_pWaveFormat->cbSize) ) + sizeof(ushort) ), cbExtraBytes )
				!= cbExtraBytes )
			{
				SAFEDELS( m_pWaveFormat );
				return false;
			}
		
		}

		// 읽기가 완료되면, 'fmt' 청크 외부로 파일포인터를 돌려놓는다.
		if( 0 != mmioAscend( m_hmmio, &ckIn, 0 ) )
		{
			SAFEDELS( m_pWaveFormat );
			return false;
		}

		return true;
	}

	//------------------------------------------------------------------
	bool CnWaveData::ReadBuffer( uchar* pBuffer, ulong ulSizeToRead )
	{
		MMIOINFO mmioinfoIn;						// current status of m_hmmio
		if( 0 != mmioGetInfo( m_hmmio, &mmioinfoIn, 0 ) )
			return false;

		CopyMemory( pBuffer, mmioinfoIn.pchNext, ulSizeToRead );

		return true;
	}

	//------------------------------------------------------------------
	bool CnWaveData::CalcBufferSize()
	{
		MMCKINFO ck;							// Multimedia RIFF chunk

		// Seek to the data
		if( -1 == mmioSeek( m_hmmio, m_ckRiff.dwDataOffset + sizeof(FOURCC), SEEK_SET ) )
			return false;

		// Search the input file for the 'data' chunk.
		ck.ckid = mmioFOURCC( 'd', 'a', 't', 'a' );
		if( 0 != mmioDescend( m_hmmio, &ck, &m_ckRiff, MMIO_FINDCHUNK ) )
			return false;

		m_ulBufferSize = ck.cksize;

		return true;
	}

	//------------------------------------------------------------------
	bool CnWaveData::CreateSoundBuffer()
	{
		m_pBuffer = g_pDirectSoundMgr->CreateSoundBuffer( m_pWaveFormat, m_ulBufferSize );
		if( !m_pBuffer )
			return false;

		VOID* pLockedBuffer = NULL;
		ulong ulLockSize = 0;	

		hr = m_pBuffer->Lock( 0, m_ulBufferSize, &pLockedBuffer, &ulLockSize, NULL, NULL, 0L );
		if( SUCCEEDED( hr ) )
		{
			CalcBufferSize();

			// 웨이브 파일에서 pLockedBuffer로 데이터를 읽어들인다.
			hr = ReadBuffer( (uchar*)pLockedBuffer, ulLockSize );
			if( FAILED( hr ) )
				return false;

			hr = m_pBuffer->Unlock( pLockedBuffer, ulLockSize, NULL, 0 );
			if( FAILED( hr ) )
				return false;
		}

		return true;
	}

}	// end of namespace 
