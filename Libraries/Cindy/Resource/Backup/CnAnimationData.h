// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNANIMATIONDATA_H__
#define __CNANIMATIONDATA_H__

#pragma once

#include "CnResource.h"

namespace Cindy
{
	class CnAniNodeData;

	//==================================================================
	/** CnAniData
		@author
			cagetu
		@since
			2005.12.5
		@remarks
			애니메이션 리소스 데이터
	*/
	//==================================================================
	class CN_DLL CnAniData : public CnResource
	{
		friend class CnAniDataManager;

		DECLARE_RTTI;
	public:
		typedef HashMap<MoString, CnAniNodeData*>			AniNodeDataMap;
		typedef AniNodeDataMap::iterator					AniNodeDataIter;

	private:
		ushort				m_usStartFrame;		//!< 애니메이션 시작 프레임
		ushort				m_usEndFrame;		//!< 애니메이션 끝 프레임
		ushort				m_usTotalFrame;		//!< 전체 프레임

		ulong				m_ulFrameRate;		//!< FrameRate
		ulong				m_ulPriority;		//!< 애니메이션 우선순위

		AniNodeDataMap		m_AniNodeDatas;		//!< 노드당 애니메이션 key 데이터들
		ushort				m_usAniNodeCount;

		/** 애니메이션 정보 읽기
		*/
		void		LoadAniData( MoMemStream& rStream );

		/** 파일 Header를 읽어온다.
		*/
		bool		ReadMagic( MoMemStream& rStream );
		bool		ReadVersion( MoMemStream& rStream );

		/** 노드 애니메이션 정보 읽기
		*/
		void		ReadAnim( MoMemStream& rStream, ulong ulReadSize );
		void		ReadAnimNode( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode );

		/** 노드 Key정보 읽기
		*/
		void		ReadPosTrack( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode );
		void		ReadRotTrack( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode );
		void		ReadSclTrack( MoMemStream& rStream, ulong ulReadSize, CnAniNodeData* pNode );

		/** 노드 정보 추가
		*/
		void		AddAniNode( CnAniNodeData* pNode );

		// Save 관련
		/** 노드 정보 저장
		*/
		void		WriteNodes( FILE* pFile );

	private:
		CnAniData( CnResourceManager* pParent );

	public:
		virtual ~CnAniData();

		/** Override Methods
		*/
		bool				Load( const MoString& strFileName );
		bool				Unload( void );

		/** Save To File..
		*/
		void				Save( const MoString& strFileName );

		/** 노드 애니메이션 정보 얻어오기
		*/
		CnAniNodeData*		GetNodeKey( const MoString& strNodeName );
		ushort				GetNodeCount() const						{	return m_usAniNodeCount;		}

		ushort				GetTotalFrame() const						{	return m_usTotalFrame;			}

		ulong				GetFrameRate() const						{	return m_ulFrameRate;			}
		void				SetFrameRate( ulong ulFrameRate )			{	m_ulFrameRate = ulFrameRate;	}
	};

	//==================================================================
	/** AniDataPtr
		@author
			cagetu
		@since
			2006년 10월 9일
		@remarks
			애니메이션 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL AniDataPtr : public CnSmartPtr< CnAniData >
	{
	public:
		AniDataPtr();
		explicit AniDataPtr( CnAniData* pObject );
		AniDataPtr( const AniDataPtr& spObject );
		AniDataPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		AniDataPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnAnimationData.inl"
}

#endif	// __CNANIMATIONDATA_H__