// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNBASETEXTURE_H__
#define __CNBASETEXTURE_H__

#include "CnResource.h"

namespace Cindy
{
	//==================================================================
	/** CnBaseTex
		@author
			cagetu
		@since
			2006년 9월 30일
		@remarks
			텍스쳐 기본 클래스
	*/
	//==================================================================
	class CnBaseTex : public CnResource
	{
		DECLARE_RTTI;
	protected:
		LPDIRECT3DTEXTURE9			m_pTexture;			//! DirectX Texture 객체

		CnBaseTex( CnResourceManager* pParent );
	public:
		virtual ~CnBaseTex();

		/** Unload
			@remarks
				DirectX 텍스쳐 객체를 Unload 한다.
			@see
				CnResource::Unload()
		*/
		virtual bool			Unload();

		/** 다이렉트 X 텍스쳐 객체 얻어오기
		*/
		LPDIRECT3DTEXTURE9		GetTexture() const		{	return m_pTexture;	}
	};
}

#endif	// __CNBASETEXTURE_H__