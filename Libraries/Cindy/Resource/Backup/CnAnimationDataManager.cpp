//==================================================================
// File: CnAnimationDataManager.cpp
// Related Header File: CnAnimationDataManager.h
// Original Author: cagetu
// Creation Date: 2005년 12월 5일 월요일
//==================================================================

#include "CnAnimationData.h"
#include "CnAnimationDataManager.h"

namespace Cindy
{
	//------------------------------------------------------------------
	CnAniDataManager::CnAniDataManager() : CnResourceManager()
	{
	}

	//------------------------------------------------------------------
	CnAniDataManager::~CnAniDataManager()
	{
		RemoveAllRsc();
	}

	//------------------------------------------------------------------
	RscPtr CnAniDataManager::Load( const MoString& strFileName )
	{
		AniDataPtr spAniData = GetRsc( strFileName );
		if( !spAniData.IsNull() )
			return spAniData.GetPtr();

		CnAniData* pAniData = new CnAniData( this );

		if( pAniData->Load( strFileName ) )
		{
			AddRsc( pAniData );
			return pAniData;
		}

		SAFEDEL( pAniData );
		return NULL;
	}
}