//================================================================
// File:           : CnMeshDataManager.h
// Original Author : changhee
// Creation Date   : 2007. 4. 12
//================================================================
#pragma once

#include "CnResourceManager.h"

namespace Cindy
{
	//================================================================
	/** 
	    @author    changhee
		@since     2007. 4. 12
		@remarks   
	*/
	//================================================================
	class CN_DLL CnMeshDataManager : public CnResourceManager,  public BaseCore::TBcSingleton<CnMeshDataManager>
	{
		friend class BaseCore::TBcSingleton<CnMeshDataManager>;

	public:
		CnMeshDataManager();
		virtual ~CnMeshDataManager();

		RscPtr			Load( const wchar* strFileName ) override;
	};
}
