//================================================================
// File:               : CnTexture.cpp
// Related Header File : CnTexture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 9
//================================================================
#include "../Cindy.h"
#include "CnTexture.h"
#include "CnResourceManager.h"

#include "../System/CnLog.h"

namespace Cindy
{
	//============================================================================
	IMPLEMENT_RTTI( Cindy, CnTexture, CnResource );

	//----------------------------------------------------------------------------
	/** 생성자
		@remarks
			이 리소스를 관리하는 매니져만 생성 가능하다.
		@param pParent
			부모 관리자
	*/
	//----------------------------------------------------------------------------
	CnTexture::CnTexture( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_eStyle( TEXTURE_UNKNOWN )
		, m_eFormat( NOFORMAT )
		, m_usUsage( CreateFromDDSFile )
		, m_usWidth( 0 )
		, m_usHeight( 0 )
		, m_usDepth( 0 )
		, m_usNumMipMaps( 0 )
		, m_ulColorKey( 0 )
	{
	}
	CnTexture::~CnTexture()
	{
	}

	//----------------------------------------------------------------------------
	/** Get Bytes Per Pixel
		@remarks		텍스쳐 의 픽셀 당 바이트 수를 얻어온다.
	*/
	//----------------------------------------------------------------------------
	ushort CnTexture::GetBytesPerPixel() const
	{
	   switch( m_eFormat )
		{
		case X8R8G8B8:  
		case A8R8G8B8:
			return 4;

		case R5G6B5:
		case A1R5G5B5:
		case A4R4G4B4:
			return 2;

		case P8:
		case A8:
			return 1;

		case DXT1:
		case DXT2:
		case DXT3:
		case DXT4:
		case DXT5:
			CNERROR( L"[CnTexture::GetBytesPerPixel] compressed pixel format!" );
			return 1;

		case R16F:
			return 2;

		case G16R16F:
			return 4;

		case A16B16G16R16F:
			return 8;

		case R32F:
			return 4;

		case G32R32F:
			return 8;

		case A32B32G32R32F:
			return 16;
	    
		default:
			CNERROR( L"[CnTexture::GetBytesPerPixel] invalid pixel format!" );
			return 1;
		}
	}
}