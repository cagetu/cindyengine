//================================================================
// File:               : CnTexture.inl
// Related Header File : CnTexture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 9
//================================================================
//----------------------------------------------------------------------------
/** Style
	@remarks		텍스쳐 타입
*/
//----------------------------------------------------------------------------
inline
void CnTexture::SetStyle( CnTexture::Style eStyle )
{
	m_eStyle = eStyle;
}
inline 
CnTexture::Style CnTexture::GetStyle() const
{
	return m_eStyle;
}

//----------------------------------------------------------------------------
/** Format
	@remarks		텍스쳐 포맷 
*/
//----------------------------------------------------------------------------
inline 
void CnTexture::SetFormat( CnTexture::Format eFormat )
{
	m_eFormat = eFormat;
}
inline 
CnTexture::Format CnTexture::GetFormat() const
{
	return m_eFormat;
}

//----------------------------------------------------------------------------
/** Width
	@remarks	텍스쳐 가로 사이즈
*/
//----------------------------------------------------------------------------
inline 
void CnTexture::SetWidth( ushort usWidth )
{
	m_usWidth = usWidth;
}
inline 
ushort CnTexture::GetWidth() const
{
	return m_usWidth;
}

//----------------------------------------------------------------------------
/** Height
	@remarks		텍스쳐 세로 사이즈
*/
//----------------------------------------------------------------------------
inline 
void CnTexture::SetHeight( ushort usHeight )
{
	m_usHeight = usHeight;
}
inline 
ushort CnTexture::GetHeight() const
{
	return m_usHeight;
}

//----------------------------------------------------------------------------
/** Depth
	@remarks		텍스쳐 깊이
*/
//----------------------------------------------------------------------------
inline 
void CnTexture::SetDepth( ushort usDepth )
{
	m_usDepth = usDepth;
}
inline 
ushort CnTexture::GetDepth() const
{
	return m_usDepth;
}

//----------------------------------------------------------------------------
/** Usage
	@remarks	텍스쳐 Usage
*/
//----------------------------------------------------------------------------
inline
void CnTexture::SetUsage( ushort usUsage )
{
	m_usUsage = usUsage;
}
inline 
ushort CnTexture::GetUsage() const
{
	return m_usUsage;
}

//----------------------------------------------------------------------------
/** Is RenderTarget ?!
	@remarks		랜더 타겟 텍스쳐 인지..
*/
//----------------------------------------------------------------------------
inline
bool CnTexture::IsRenderTarget() const
{
	return (0 != (GetUsage() & (RenderTargetColor | RenderTargetDepth | RenderTargetStencil)));
}

//----------------------------------------------------------------------------
/** Get Number Of Mip-map Levels
	@remarks		텍스쳐 밉맵 레벨 수
*/
//----------------------------------------------------------------------------
inline 
ushort CnTexture::GetNumMipLevels() const
{
	return m_usNumMipMaps;
}
