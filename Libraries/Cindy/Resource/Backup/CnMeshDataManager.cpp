//================================================================
// File:               : CnMeshDataManager.cpp
// Related Header File : CnMeshDataManager.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 12
//================================================================
#include "../Cindy.h"
#include "CnMeshDataManager.h"

#include "CnMeshData.h"

namespace Cindy
{
	///Const/Dest
	CnMeshDataManager::CnMeshDataManager()
	{
	}
	CnMeshDataManager::~CnMeshDataManager()
	{
	}

	//----------------------------------------------------------------------------
	/** 리소스 읽기
		@remarks
			리소스를 읽는다.
	*/
	//----------------------------------------------------------------------------
	RscPtr CnMeshDataManager::Load( const wchar* strFileName )
	{
		RscPtr spRsc = GetRsc( strFileName );
		if( !spRsc.IsNull() )
			return spRsc.GetPtr();

		CnMeshData* mesh = new CnMeshData( this );
		if( mesh->Load( strFileName ) )
		{
			AddRsc( mesh );
		}
		else
		{
			SAFEDEL( mesh );
		}

		return mesh;
	}
}