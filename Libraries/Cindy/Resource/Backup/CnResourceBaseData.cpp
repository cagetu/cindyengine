// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnResourceBaseData.h"

namespace Cindy
{
	//==================================================================
	//	CnNodeData
	//==================================================================
	//------------------------------------------------------------------
	CnNodeData::CnNodeData()
		: m_usId(INVALID_NODE)
		, m_usParentId(INVALID_NODE)
		, m_eNodeType(DEFAULT)
		, m_usDefine(0)
	{
	}

	//------------------------------------------------------------------
	CnNodeData::~CnNodeData()
	{
	}

	//------------------------------------------------------------------
	void CnNodeData::SetWorldTM( const CnMatrix& mat)
	{
		m_matWorldTM.MakeIdentity();

		m_matWorldTM = mat;
	}

	//------------------------------------------------------------------
	void CnNodeData::GetInverseTM( CnMatrix& rOut )
	{
		m_matWorldTM.Inverse( rOut );
	}

	//==================================================================
	//	CnAniNodeData
	//==================================================================
	//------------------------------------------------------------------
	CnAniNodeData::CnAniNodeData()
		: m_usPosKeyCount(0)
		, m_akPosKey(0)
		, m_usRotKeyCount(0)
		, m_akRotKey(0)
		, m_usScalekeyCount(0)
		, m_akScaleKey(0)
	{
	}

	//------------------------------------------------------------------
	CnAniNodeData::~CnAniNodeData()
	{
		SAFEDELS( m_akPosKey );
		SAFEDELS( m_akRotKey );
		SAFEDELS( m_akScaleKey );
	}
}