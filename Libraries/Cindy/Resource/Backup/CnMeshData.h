//================================================================
// File:           : CnMeshData.h
// Original Author : changhee
// Creation Date   : 2007. 4. 10
//================================================================
#pragma once

#include "CnResource.h"
#include "CnExportedDataDefines.h"

#include <BaseCore/BcMemStream.h>

namespace Cindy
{
	//================================================================
	/** Mesh Resource Data
	    @author    changhee
		@since     2007. 4. 10
		@remarks   메쉬 리소스 데이터
	*/
	//================================================================
	class CN_DLL CnMeshData : public CnResource
	{
		DECLARE_RTTI;

		friend class CnMeshDataManager;
	public:
		typedef std::vector<MeshNodeData*>		MeshNodeList;
		typedef MeshNodeList::iterator			MeshNodeIter;

	private:
		MeshNodeList			m_MeshNodeDatas;
		int						m_nNumMeshNodeDatas;

		BaseCore::CBcMemStream	m_DataMemStream;
		ulong					m_ulTotalDataSize;

		// Data 읽기 시작
		bool			LoadData();

		// 파일 정보 읽기
		bool			ReadMagicNumber();
		bool			ReadFileVersion();

		// 메쉬 정보 읽기
		bool			ReadMeshNodeList( ulong ulDataSize );
		bool			ReadMeshNode( ulong ulDataSize );
		bool			ReadMeshNodeVertices( ulong ulDataSize, MeshNodeData* pMeshNodeData );
		bool			ReadMeshNodeFaces( ulong ulDataSize, MeshNodeData* pMeshNodeData );
		bool			ReadMeshNodeFaceGroup( ulong ulDataSize, MeshNodeData* pMeshNodeData );

		// 충돌 박스
		void			ReadAABB( MeshNodeData* pMeshNodeData );
		void			ReadOBB( MeshNodeData* pMeshNodeData );

		// 기타
		NodeData*		CreateNodeData( BYTE Type );
		CnVertexBuffer*	CreateVertexBuffer( ushort usNumVertices, CnVertexDeclaration* pVertexDeclaration );
		CnIndexBuffer*	CreateIndexBuffer( ushort usNumIndices );

		void			ReadNameData( CnString& rResult );

	protected:
		CnMeshData( CnResourceManager* pParent );

	public:
		virtual ~CnMeshData();

		// Load/Unload
		bool			Load( const CnString& strFileName ) override;
		bool			Unload() override;

		// Device Lost/Restore
		void			OnLost() override;
		void			OnRestore() override;

		// Datas
		int					GetNumMeshNodeDatas() const;
		const MeshNodeList&	GetMeshNodeDatas() const;
	};

	//==================================================================
	/** MeshDataPtr
		@author			cagetu
		@since			2006년 10월 9일
		@remarks		Mesh 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL MeshDataPtr : public CnSmartPtr< CnMeshData >
	{
	public:
		MeshDataPtr();
		explicit MeshDataPtr( CnMeshData* pObject );
		MeshDataPtr( const MeshDataPtr& spObject );
		MeshDataPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		MeshDataPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnMeshData.inl"
} // end of namespace