// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnResourceChunk.h"

namespace Cindy
{
	static const ulong CnResourceChunk::ChunkHeaderSize = sizeof(ulong) + sizeof(ulong);

	//-------------------------------------------------------------------------
	//	WriteBegin
	//-------------------------------------------------------------------------
	void CnResourceChunk::WriteBegin( FILE* pFile, ulong ulTag, ulong& ulPtr, ulong& ulBegin )
	{
		ulBegin = ftell( pFile );
		// Tag 저장
		fwrite( &ulTag, sizeof(ulong), 1, pFile );

		ulPtr = ftell( pFile );

		// Size 저장 장소 확보
		fwrite( &ulPtr, sizeof(ulong), 1, pFile );
	}

	//-------------------------------------------------------------------------
	//	WriteEnd
	//-------------------------------------------------------------------------
	void CnResourceChunk::WriteEnd( FILE* pFile, ulong ulPtr, ulong ulBegin )
	{
		ulong ulPos, ulSize;
		ulPos = ftell( pFile );
		fseek( pFile, ulPtr, SEEK_SET );
		ulSize = ulPos - ulBegin;

		// 실제 크기 저장 - begin에서 잡아놓았던 그 자리에 덮혀쓴다.
		fwrite( &ulSize, sizeof(ulong), 1, pFile );

		fseek( pFile, ulPos, SEEK_SET );
	}

}