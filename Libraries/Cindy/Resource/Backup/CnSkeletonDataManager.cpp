// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnSkeletonDataManager.h"
#include "CnSkeletonData.h"

namespace Cindy
{
	//------------------------------------------------------------------
	CnSkelDataManager::CnSkelDataManager()
	{
	}

	//------------------------------------------------------------------
	CnSkelDataManager::~CnSkelDataManager()
	{
		RemoveAllRsc();
	}

	//------------------------------------------------------------------
	RscPtr CnSkelDataManager::Load( const MoString& strFileName )
	{
		SkelDataPtr spSkeleton = GetRsc( strFileName );
		if( !spSkeleton.IsNull() )
			return spSkeleton.GetPtr();

		CnSkeletonData* pSkeleton = new CnSkeletonData( this );

		if( pSkeleton->Load( strFileName ) )
		{
			AddRsc( pSkeleton );
			return pSkeleton;
		}

		SAFEDEL( pSkeleton );
		return NULL;
	}
}