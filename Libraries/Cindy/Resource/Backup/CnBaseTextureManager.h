// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNBASETEXTUREMANAGER_H__
#define __CNBASETEXTUREMANAGER_H__

#include "CnResourceManager.h"

namespace Cindy
{
	//==================================================================
	/** CnThread
		@author
			cagetu
		@since
			2006�� 9�� 25��
		@remarks
			Thread Control Class
	*/
	//==================================================================
	class CnBaseTexManager : public CnResourceManager
	{
	protected:
		ushort			m_usMipLevel;			//!< �Ӹ� ����

	public:
		CnBaseTexManager();
		virtual ~CnBaseTexManager();

		/** Mip-map
		*/
		void			SetMipMap( ushort usLevel )			{		m_usMipLevel = usLevel;		}
		ushort			GetMipMap() const					{		return m_usMipLevel;		}
	};
}

#endif	// __CNBASETEXTUREMANAGER_H__