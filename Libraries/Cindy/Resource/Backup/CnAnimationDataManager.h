// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNANIMATIONDATAMANAGER_H__
#define __CNANIMATIONDATAMANAGER_H__

#include "CnResourceManager.h"

namespace Cindy
{
	//==================================================================
	/** CnAniDataManager
		@author
			cagetu
		@since
			2005.12.5
		@remarks
			애니메이션 데이터 리소스 매니져
		@see
			CnResourceManager
			Momo::MoSingleton
	*/
	//==================================================================
	class CN_DLL CnAniDataManager : public CnResourceManager, public MoSingleton<CnAniDataManager>
	{
	public:
		CnAniDataManager();
		virtual ~CnAniDataManager();

		/** Load
			@remarks
				애니메이션 데이터를 읽는다.
		*/
		RscPtr			Load( const MoString& strFileName );
	};

#define g_AniDataMgr	CnAniDataManager::GetSingleton()
#define g_pAniDataMgr	CnAniDataManager::GetSingletonPtr()
}

#endif	//	__CNANIMATIONDATAMANAGER_H__