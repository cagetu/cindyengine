// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNSKELETONDATA_H__
#define __CNSKELETONDATA_H__

#include "CnResource.h"
#include "CnResourceBaseData.h"

namespace Cindy
{
	//==================================================================
	/** CnSkeletonData
		@author
			cagetu
		@since
			2005월 12월 12일
		@remarks
			Skeleton에 대한 정보를 담고 있는 "*.mbf"파일 하나에 대한 리소스 
		@desc
			BoneNode는 인덱스가 작은 것부터 순서대로 들어가야 한다.
			즉, 부모에서 자식으로 순서대로 리스트에 추가되어 있어야 함
	*/
	//==================================================================
	class CN_DLL CnSkeletonData : public CnResource
	{
		DECLARE_RTTI;

		friend class CnSkelDataManager;
	public:
		typedef map< ushort, CnBoneNodeData* >		BoneDataMap;
		typedef BoneDataMap::const_iterator			BoneDataIter;

		typedef vector< BoundingBox* >				BBoxVector;
		typedef BBoxVector::iterator				BBoxIter;

		typedef map< ushort, ushort >				BoneDefineMap;
		typedef BoneDefineMap::iterator				BoneDefineIter;

	private:
		BoneDataMap			m_BoneDatas;			//!< Bone 노드 데이터 리스트
		ushort				m_usBoneCounts;

		BBoxVector			m_BBoxes;
		BoneDefineMap		m_BoneDefines;

		/** 뼈다구 정보를 읽는다.
		*/
		void		LoadSkeleton( MoMemStream& rStream );

		/** 파일 Header를 읽어온다.
		*/
		bool		ReadMagic( MoMemStream& rStream );
		bool		ReadVersion( MoMemStream& rStream );

		/** 본 노드 데이터를 읽어온다.
		*/
		void		ReadSkeleton( MoMemStream& rStream, ulong ulSize );
		void		ReadBone( MoMemStream& rStream, ulong ulSize, CnBoneNodeData* pBoneData );


		// BBox 읽어오기
		void		ReadBoundingBoxList( MoMemStream& rStream, ulong ulSize );
		void		ReadBoundingBox( MoMemStream& rStream, ulong ulSize, BoundingBox* pBBox );

		/** 본 설정 정보 받아오기
		*/
		void		ReadBoneDefineList( MoMemStream& rStream, ulong ulSize );

		/** 리스트에 본노드 데이터를 추가한다.
		*/
		void		AddBoneNode( CnBoneNodeData* pBoneData );

	private:
		CnSkeletonData( CnResourceManager* pParent );

	public:
		virtual ~CnSkeletonData();

		bool					Load( const MoString& strFileName );
		bool					Unload();

		ushort					GetBoneCount()						{	return m_usBoneCounts;				}
		CnBoneNodeData*			GetBoneNode( ushort usIndex );

		const BoneDataMap&		GetBoneDatas() const				{	return m_BoneDatas;					}
		const BBoxVector&		GetBBoxList() const					{	return m_BBoxes;					}	
		const BoneDefineMap&	GetBoneDefines() const				{	return m_BoneDefines;				}

		const CnObb&			GetBBoxOBB( ushort usIndex );
	};

	//==================================================================
	/** SkelDataPtr
		@author
			cagetu
		@since
			2006년 10월 9일
		@remarks
			Skeleton 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL SkelDataPtr : public CnSmartPtr< CnSkeletonData >
	{
	public:
		SkelDataPtr();
		explicit SkelDataPtr( CnSkeletonData* pObject );
		SkelDataPtr( const SkelDataPtr& spObject );
		SkelDataPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		SkelDataPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnSkeletonData.inl"
}

#endif	// __CNSKELETONDATA_H__