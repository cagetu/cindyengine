// Copyright (c) 2006~. cagetu
//
//******************************************************************

//----------------------------------------------------------------------------
inline SkelDataPtr::SkelDataPtr()
: CnSmartPtr< CnSkeletonData >()
{
}

//----------------------------------------------------------------------------
inline SkelDataPtr::SkelDataPtr( CnSkeletonData* pObject )
: CnSmartPtr< CnSkeletonData >( pObject )
{
}

//----------------------------------------------------------------------------
inline SkelDataPtr::SkelDataPtr( const SkelDataPtr& spObject )
: CnSmartPtr< CnSkeletonData >( spObject )
{
}

//----------------------------------------------------------------------------
inline SkelDataPtr::SkelDataPtr( const RscPtr& spRsc )
: CnSmartPtr< CnSkeletonData >()
{
	m_pObject = (CnSkeletonData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->IncreaseReferences();
	}
}

//----------------------------------------------------------------------------
inline SkelDataPtr& SkelDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnSkeletonData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->DecreaseReferences();

		if( !spRsc.IsNull() )
			spRsc->IncreaseReferences();

		m_pObject = (CnSkeletonData*)spRsc.GetPtr();
	}

	return *this;
}