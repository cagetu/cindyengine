// Copyright (c) 2006~. cagetu
//
//******************************************************************

//----------------------------------------------------------------------------
inline AniDataPtr::AniDataPtr()
: CnSmartPtr< CnAniData >()
{
}

//----------------------------------------------------------------------------
inline AniDataPtr::AniDataPtr( CnAniData* pObject )
: CnSmartPtr< CnAniData >( pObject )
{
}

//----------------------------------------------------------------------------
inline AniDataPtr::AniDataPtr( const AniDataPtr& spObject )
: CnSmartPtr< CnAniData >( spObject )
{
}

//----------------------------------------------------------------------------
inline AniDataPtr::AniDataPtr( const RscPtr& spRsc )
: CnSmartPtr< CnAniData >()
{
	m_pObject = (CnAniData*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->IncreaseReferences();
	}
}

//----------------------------------------------------------------------------
inline AniDataPtr& AniDataPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnAniData*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->DecreaseReferences();

		if( !spRsc.IsNull() )
			spRsc->IncreaseReferences();

		m_pObject = (CnAniData*)spRsc.GetPtr();
	}

	return *this;
}