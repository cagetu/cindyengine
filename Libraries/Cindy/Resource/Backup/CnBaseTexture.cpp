// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnBaseTexture.h"

namespace Cindy
{
	//============================================================================
	IMPLEMENT_RTTI( Cindy, CnBaseTex, CnResource );

	//----------------------------------------------------------------------------
	CnBaseTex::CnBaseTex( CnResourceManager* pParent )
		: CnResource( pParent )
		, m_pTexture( 0 )
	{
	}

	//----------------------------------------------------------------------------
	CnBaseTex::~CnBaseTex()
	{
		Unload();
	}

	//----------------------------------------------------------------------------
	bool CnBaseTex::Unload()
	{
		if( m_bLoaded )
		{
			SAFEREL( m_pTexture );

			m_bLoaded = false;
		}

		return true;
	}
}