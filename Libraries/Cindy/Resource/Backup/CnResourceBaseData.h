// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNRESOURCEBASEDATA_H__
#define __CNRESOURCEBASEDATA_H__

#include "../Math/CindyMath.h"

namespace Cindy
{
	#define INVALID_NODE	0xffff

	//==================================================================
	/** CnNodeData
		@author
			cagetu
		@since
			2006.10.9
		@remarks
			Max 상의 Node를 구성하고 있는 Export된 Node를 의미
	*/
	//==================================================================
	class CN_DLL CnNodeData
	{
	public:
		enum NODE_TYPE
		{
			DEFAULT = 0,
			ANIMATE,
			DUMMY
		};

	protected:
		ushort				m_usId;						// My Node ID
		ushort				m_usParentId;				// Parent Node ID
		MoString			m_strName;					// My NodeName
		MoString			m_strParentName;			// My Parent NodeName

		NODE_TYPE			m_eNodeType;				// 노드 타입

		CnVector3			m_vLocalPos;				// 로컬 위치 정보
		CnQuaternion		m_qLocalRot;				// 로컬 회전 정보
		CnVector3			m_vLocalScale;				// 로컬 크기 정보

		CnMatrix			m_matWorldTM;				// 월드 변환 행렬

		ushort				m_usDefine;					// 그래픽 팀과 약속한 노드의 용도 정의
	public:
		CnNodeData();
		virtual ~CnNodeData();

		void				SetID( ushort nID )							{	m_usId = nID;				}
		ushort				GetID() const								{	return m_usId;				}

		void				SetParentID( ushort nID )					{	m_usParentId = nID;			}
		ushort				GetParentID() const							{	return m_usParentId;		}

		bool				HasParent();

		void				SetName( const MoString& strName )			{	m_strName = strName;		}
		const MoString&		GetName() const								{	return m_strName;			}

		void				SetParentName( const MoString& strName )	{	m_strParentName = strName;	}
		const MoString&		GetParentName() const						{	return m_strParentName;		}

		void				SetNodeType( NODE_TYPE eType )				{	m_eNodeType = eType;		}
		NODE_TYPE			GetNodeType() const							{	return m_eNodeType;			}

		void				SetDefineType( ushort usType )				{	m_usDefine = usType;		}
		ushort				GetDefineType() const						{	return m_usDefine;			}

		/** 로컬 변환 행렬
		*/
		void				SetLocalPos( const CnVector3& vPos )		{	m_vLocalPos = vPos;			}
		void				SetLocalRot( const CnQuaternion& qRot )		{	m_qLocalRot = qRot;			}
		void				SetLocalScale( const CnVector3& vScale )	{	m_vLocalScale = vScale;		}
		const CnVector3&	GetLocalPos() const							{	return m_vLocalPos;			}
		const CnQuaternion&	GetLocalRot() const							{	return m_qLocalRot;			}
		const CnVector3&	GetLocalScale() const						{	return m_vLocalScale;		}

		/** 월드 변환 행렬
		*/
		void				SetWorldTM( const CnMatrix& mat );
		const CnMatrix&		GetWorldTM()								{	return m_matWorldTM;		}

		/** 역 변환 행렬
		*/
		void				GetInverseTM( CnMatrix& rOut );
	};

	//==================================================================
	//! BoneNode 재정의
	//==================================================================
	typedef CnNodeData		CnBoneNodeData;

	//
	typedef struct _LinearKey
	{
		ushort		frame;			//! 프레임
		CnVector3	value;			//! 값

		_LinearKey() : frame(0) {}
	} PosKey, ScaleKey;

	//
	struct RotKey
	{
		ushort			frame;
		CnQuaternion	value;

		RotKey() : frame(0) {}
	};

	//==================================================================
	/** CnAniNodeData
		@author
			cagetu
		@since
			2006.1.6
		@remarks
			노드의 애니메이션 정보
	*/
	//==================================================================
	class CN_DLL CnAniNodeData : public CnNodeData
	{
		friend class CnAniData;
		friend class CnAnimController;
	protected:
		ushort			m_usPosKeyCount;
		PosKey*			m_akPosKey;

		ushort			m_usRotKeyCount;
		RotKey*			m_akRotKey;

		ushort			m_usScalekeyCount;
		ScaleKey*		m_akScaleKey;

	public:
		CnAniNodeData();
		virtual ~CnAniNodeData();

		ushort			GetPosKeyCount() const					{	return m_usPosKeyCount;				}
		ushort			GetRotKeyCount() const					{	return m_usRotKeyCount;				}
		ushort			GetScalekeyCount() const				{	return m_usScalekeyCount;			}

		PosKey*			GetPosKey( ushort usIndex ) const		{	return &m_akPosKey[usIndex];		}
		RotKey*			GetRotKey( ushort usIndex )	const		{	return &m_akRotKey[usIndex];		}
		ScaleKey*		GetScaleKey( ushort usIndex ) const		{	return &m_akScaleKey[usIndex];		}
	};

	//
	struct BoundingBox
	{
		ushort		ParentID;
		MoString	ParentName;
		CnObb		OBB;

		BoundingBox() : ParentID( 0xffff ), ParentName( L"" )	{}
	};

#include "CnResourceBaseData.inl"
}	// end of namespace

#endif	// __CNRESOURCEBASEDATA_H__