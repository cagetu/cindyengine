// Copyright (c) 2006~. cagetu
//
//******************************************************************

//----------------------------------------------------------------------------
inline MeshTexPtr::MeshTexPtr()
: CnSmartPtr< CnMeshTex >()
{
}

//----------------------------------------------------------------------------
inline MeshTexPtr::MeshTexPtr( CnMeshTex* pObject )
: CnSmartPtr< CnMeshTex >( pObject )
{
}

//----------------------------------------------------------------------------
inline MeshTexPtr::MeshTexPtr( const MeshTexPtr& spObject )
: CnSmartPtr< CnMeshTex >( spObject )
{
}

//----------------------------------------------------------------------------
inline MeshTexPtr::MeshTexPtr( const RscPtr& spRsc )
: CnSmartPtr< CnMeshTex >()
{
	m_pObject = (CnMeshTex*)spRsc.GetPtr();
	if( m_pObject )
	{
		m_pObject->IncreaseReferences();
	}
}

//----------------------------------------------------------------------------
inline MeshTexPtr& MeshTexPtr::operator = ( const RscPtr& spRsc )
{
	if( m_pObject != (CnMeshTex*)spRsc.GetPtr() )
	{
		if( m_pObject )
			m_pObject->DecreaseReferences();

		if( !spRsc.IsNull() )
			spRsc->IncreaseReferences();

		m_pObject = (CnMeshTex*)spRsc.GetPtr();
	}

	return *this;
}