// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNWAVEDATA_H__
#define __CNWAVEDATA_H__

#include "CnResource.h"

namespace Cindy
{
	//==================================================================
	/** CnWaveData
		@author
			cagetu
		@since
			2006월 12월 26일
		@remarks
			사운드(.Wav) 파일 데이터..
	*/
	//==================================================================
	class CN_DLL CnWaveData : public CnResource
	{
		DECLARE_RTTI;

		friend class CnWaveDataManager;
	private:
		LPDIRECTSOUNDBUFFER		m_pBuffer;						//!< 사운드 버퍼..

	    WAVEFORMATEX*           m_pWaveFormat;					//!< Pointer to WAVEFORMATEX structure
		HMMIO                   m_hmmio;						//!< 웨이브 파일에 대한 MM I/O handle 
	    MMCKINFO				m_ckRiff;						//!< 웨이브 파일 열기 작업시 사용
		ulong					m_ulBufferSize;					//!< 웨이브 버퍼 크기

		bool			LoadData( MoMemStream& rStream );

		bool			Read( MoMemStream& rStream );
		bool			ReadMMIO();
		bool			ReadBuffer( uchar* pBuffer, ulong ulSizeToRead );

		bool			CalcBufferSize();

		bool			CreateSoundBuffer();
	private:
		CnWaveData( CnResourceManager* pParent );

	public:
		virtual ~CnWaveData();

		bool		Load( const MoString& strFileName );
		bool		Unload();

		LPDIRECTSOUNDBUFFER		GetBuffer( void ) const				{	return m_pBuffer;		}
	};

	//==================================================================
	/** WaveDataPtr
		@author
			cagetu
		@since
			2006년 10월 9일
		@remarks
			사운드 리소스(.wav)용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL WaveDataPtr : public CnSmartPtr< CnWaveData >
	{
	public:
		WaveDataPtr();
		explicit WaveDataPtr( CnWaveData* pObject );
		WaveDataPtr( const WaveDataPtr& spObject );
		WaveDataPtr( const RscPtr& spRsc );

		/** operator = 
		*/
		WaveDataPtr& operator = ( const RscPtr& spRsc );
	};

#include "CnWaveData.inl"
}

#endif	// __CNWAVEDATA_H__