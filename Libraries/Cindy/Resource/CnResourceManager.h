// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

#include "CnResource.h"
#include "Util/CnUnCopyable.h"

namespace Cindy
{
	//==================================================================
	/** CnResourceManager
		@author
			cagetu
		@since
			2006년 9월 26일
		@remarks
			Resource Manager class. This class manage all of resource file, 
			Mesh, Texture, Animation, Sound ETC.
		@desc
			약식으로 Resource 를 Rsc로 표기한다.
	*/
	//==================================================================
	class CN_DLL CnResourceManager : public CnObject, private CnUncopyable
	{
		__DeclareRtti;
	public:
		CnResourceManager();
		virtual ~CnResourceManager();

		// Load
		virtual RscPtr			Load( const wchar* strFileName ) abstract;

		// Resource
		RscPtr					Get( const CnString& strName );
		virtual bool			Remove( const RscPtr& rRsc );
		virtual bool			Remove( const CnString& strName );
		virtual void			Clear();

		// Path
		void					AddDataPath( const CnString& strPath );
		void					RemoveAllDataPath();

		bool					FindData( const CnString& strFileName, MoMemStream& rOutput );

	protected:
		typedef HashMap<CnString, CnResource*>	RscMap;
		typedef RscMap::iterator				RscIter;

		typedef std::vector<CnString>			PathArray;
		typedef PathArray::iterator				PathIter;

		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		RscMap				m_Resources;			//!< 리소스 목록
		PathArray			m_DataPathes;

		MoCriticalSection	m_CS;

		//------------------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------------------
		virtual bool		Add( CnResource* pRsc );
	};
}
