//================================================================
// File:           : CnRenderer.h
// Original Author : changhee
// Creation Date   : 2007. 4. 20
//================================================================
#pragma once

#include "Foundation/CnSmartPointer.h"
#include "Util/CnUnCopyable.h"

namespace Cindy
{
	class CnViewport;
	class CnRenderWindow;
	class CnRenderTarget;
	class CnRenderTexture;
	class CnMultiRenderTarget;
	class CnRenderState;
	class CnMaterialState;
	class CnTextureState;
	class CnTexture;
	class CnVertexDeclaration;
	class CnVertexBuffer;
	class CnVertexData;
	class CnIndexBuffer;
	class CnShader;
	class CnShaderEffect;
	class CnQuery;
	class CnShape;
	class CnMaterial;

	//================================================================
	/** Renderer Interface
	    @author    changhee
		@since     2007. 4. 20
		@remarks   Renderer 인터페이스 객체
	*/
	//================================================================
	class IRenderer abstract
	{
	public:
		enum BUFFER_CLEAR
		{
			CLEAR_TARGET = 0x00000001l,
			CLEAR_ZBUFFER = 0x00000002l,
			CLEAR_STENCIL = 0x00000004l,
		};

		enum PRIMITIVE
		{
			POINTLIST = 1,
			LINELIST = 2,
			LINESTRIP = 3,
			TRIANGLELIST = 4,
			TRIANGLESTRIP = 5,
			TRIANGLEFAN = 6,
		};

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		IRenderer();
		virtual ~IRenderer();

		// Device
		virtual void					GetCOM( void* pCOM ) abstract;
		virtual void					GetCurrentDevice( void* pDevice ) abstract;
		virtual void					GetCurrentPresentParams( void* pParams ) abstract;

		// RenderTarget
		virtual void					AddRenderTarget( CnRenderTarget* pRenderTarget ) abstract;
		virtual CnRenderTarget*			GetRenderTarget( const CnString& strName ) abstract;
		virtual CnRenderTarget*			DetachRenderTarget( const CnString& strName ) abstract;
		virtual bool					DeleteRenderTarget( const CnString& strName ) abstract;
		virtual void					DeleteAllRenderTargets() abstract;

		virtual void					UpdateRenderTargets() abstract;
		virtual void					NeedToSortRenderTargets() abstract;

		virtual void					SetCurrentRenderTarget( CnRenderTarget* pRenderTarget ) abstract;
		virtual CnRenderTarget*			GetCurrentRenderTarget() const abstract;

		// Window
		virtual CnRenderWindow*			CreateRenderWindow( HWND hWnd,
															const wchar* strName,
															int nWidth,
															int nHeight,
															ushort usColorDepth,
															ushort usRefreshRate,
															bool bFullScreen,
															bool bThreadSafe,
															bool bCurrentTarget = false ) abstract;

		// RenderTexture
		virtual CnRenderTexture*		CreateRenderTexture( const wchar* strName,
															 ushort usWidth,
															 ushort usHeight,
															 ushort usDepth,
															 int nFormat,
															 int nAntiAliasQuality = 0,
															 bool bEnableDepthStencil = false,
															 bool bGenerateMipMaps = false,
															 bool bManage = true ) abstract;

		// MultiRenderTarget
		virtual CnMultiRenderTarget*	CreateMultiRenderTarget( const wchar* strName,
																 bool bManage = true ) abstract;

		// RenderWindow
		virtual void					SetCurrentRenderWindow( CnRenderWindow* pRenderWindow ) abstract;
		virtual CnRenderWindow*			GetCurrentRenderWindow() const abstract;

		// Surface
		virtual void					SetSurface( int nIndex, void* pTarget ) abstract;
		virtual void					GetSurface( int nIndex, void* pTarget ) abstract;

		virtual void					SetDepthStencilSurface( void* pTarget ) abstract;
		virtual void					GetDepthStencilSurface( void* pTarget ) abstract;

		virtual void					OpenSurface( int nIndex ) abstract;
		virtual void					CloseSurface( int nIndex ) abstract;

		// Viewport
		virtual void					SetViewport( CnViewport* pViewport ) abstract;

		// RenderState
		virtual void					SetRenderState( const CnRenderState& rState ) abstract;
		virtual void					SetRenderState( ushort StateFlags ) abstract;

		virtual void					SetAlphaBlendMode( const CnRenderState& rState ) abstract;

		// MaterialState
		virtual void					SetMaterialState( const CnMaterialState& rState ) abstract;

		// Texture
		virtual void					SetTexture( int nStage, CnTexture* pTexture ) abstract;
		virtual void					SetTextureState( const CnTextureState& rState ) abstract;

		// Shader
		virtual void					SetActiveShader( CnShaderEffect* pEffect ) abstract;
		virtual CnShaderEffect*			GetActiveShader() const abstract;

		virtual CnShaderEffect*			CreateEffect() abstract;
		virtual void					GetEffectPool( void* pPool ) abstract;

		// Material
		virtual void					SetMaterial( CnMaterial* Material ) abstract;

		// Light
		virtual void					EnableLight( bool bEnable ) abstract;
		virtual void					EnableSpecular( bool bEnable ) abstract;
		virtual void					SetAmbient( RGBA ulColor ) abstract;
		virtual void					GetAmbient( RGBA& rOutput ) abstract;

		// Shape
		virtual CnShape*				CreateShape( ushort ShapeType ) abstract;

		// Vertex Buffer
		virtual CnVertexBuffer*			CreateVertexBuffer() abstract;
		virtual CnVertexBuffer*			CreateVertexBuffer( uint nNumVertices, uint nSizeOfVertex, ulong ulUsage, ulong ulPool ) abstract;

		virtual bool					SetStreamSource( uint nStreamID, uint nOffsetInBytes, const Ptr<CnVertexBuffer>& pBuffer ) abstract;
		virtual bool					SetStreamSource( uint BufferID, uint StreamID, uint OffsetInBytes, const Ptr<CnVertexData>& pVertexData ) abstract;
		virtual bool					SetStreamSource( const Ptr<CnVertexData>& pVertexData ) abstract;

		// Index Buffer
		virtual CnIndexBuffer*			CreateIndexBuffer() abstract;
		virtual CnIndexBuffer*			CreateIndexBuffer( uint nNumIndices, ulong ulUsage, ulong ulFormat, ulong ulPool ) abstract;		

		virtual bool					SetIndexBuffer( const Ptr<CnIndexBuffer>& IndexBuffer ) abstract;

		// Vertex Declaration
		virtual CnVertexDeclaration*	CreateVertexDeclaration() abstract;
		virtual void					SetVertexDeclaration( const Ptr<CnVertexDeclaration>& pDeclaration ) abstract;
		virtual void					SetVertexDeclaration( const Ptr<CnVertexData>& pVertexData ) abstract;
		virtual void					SetFVF( ulong ulFVF ) abstract;

		// Query
		virtual CnQuery*				CreateQuery( ushort QueryType ) abstract;

		// Clear
		virtual void					ClearBuffer( ulong ulFlags, ulong ulColor, float fDepth, ushort usStencil ) abstract;

		// Scene Begin/End
		virtual bool					BeginScene( ulong ulFlags = CLEAR_TARGET | CLEAR_ZBUFFER, float fDepth = 1.0f, ushort usStenctil = 0 ) abstract;
		virtual bool					BeginScene( ulong ulFlags, ulong ulBackgroundColor, float fDepth, ushort usStencil ) abstract;
		virtual void					EndScene() abstract;

		// Draw
		virtual bool					DrawPrimitive( PRIMITIVE ePrimitive,
													   uint nIndexOfStartVertex,
													   uint nNumPrimitiveCount ) abstract;
		virtual bool					DrawIndexedPrimitive( PRIMITIVE ePrimitive,
															  uint nNumVertices,
															  uint nNumPrimitiveCount,
															  int nBaseVertexIndex = 0,
															  uint nMinIndex = 0,
															  uint nIndexOfStartVertex = 0 ) abstract;
	};

	//================================================================
	/** Renderer
	    @author    changhee
		@since     2007. 4. 20
		@remarks   외부에서 사용할 수 있도록 만들어진 래퍼 클래스
	*/
	//================================================================
	class CN_DLL CnRenderer
	{
		__DeclareUnCopy(CnRenderer);
	public:
		enum TYPE
		{
			D3D9 = 0,
		};

		enum ShadeType
		{
			Forward = 0,
			Deferred,
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnRenderer();
		~CnRenderer();

		// Set Renderer
		void		Open( TYPE eType = D3D9 );
		void		Close();

		// Renderer Type
		TYPE		GetType() const;

		//
		void		SetShadeType( ShadeType eType = Forward );
		ShadeType	GetShadeType() const;

		// Null
		bool		IsEmpty() const;

		// Pointer
		IRenderer*	GetPtr() const;
		IRenderer*	operator->() const;

		// Singleton
		static CnRenderer& Instance();
	private:
		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		IRenderer*				m_pRenderer;
		TYPE					m_eType;
		ShadeType				m_eShadeType;

		/// Singleton
		static CnRenderer*		ms_pInstance;
	};

#include "CnRenderer.inl"
}

#define RenderDevice	Cindy::CnRenderer::Instance()
