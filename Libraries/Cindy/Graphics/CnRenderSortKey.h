//================================================================
// File:           : CnRenderSortKey.h
// Original Author : changhee
// Creation Date   : 2009. 1. 14
//================================================================
#ifndef __CN_RENDER_SORTKEY_H__
#define __CN_RENDER_SORTKEY_H__

#include "../Util/CnKey.h"

namespace Cindy
{
	namespace RenderGroup
	{
		struct OpaqueKey;
		struct TransparentKey;

		//================================================================
		/** SortKey
			@author		changhee
			@brief		오브젝트를 정렬하기 위한 Key를 만든다.
			@desc		64bit 정렬키 값을 만든다.
						[opaque 객체의 경우]
							material id > distance 로 정렬 한다.

						[transparency 객체의 경우]
							distance > materia id 순서로 정렬한다.

			if number of textures will be more than 65535, this system will be break out.

		*/
		//================================================================
		struct SortKey
		{
			uint64	value;

			SortKey();

			operator uint64() const	{	return value;	}

			SortKey& operator =(uint64 rhs);
			SortKey& operator =(const OpaqueKey& rhs);
			SortKey& operator =(const TransparentKey& rhs);
		};

#pragma pack(push, 1)
		//================================================================
		/**	Opaque Sort Key

			우선적으로, 같은 shader 별로, grouping할 것이다. 따라서, Group안에서
			정렬할 키를 가지면 된다.

			-----------------------------------------------------------
			| textureID(16) | renderState(16) | depth(16) | empty(16) |
			-----------------------------------------------------------
		*/
		//================================================================
		struct OpaqueKey
		{
			union
			{
				// 비트 채우기는 뒤에서 부터 채워 나간다.
				struct
				{
					// second
					uint16 depth;		// 16-bits
					uint16 textureID;	// 16-bits
					uint32 material;	// 32-bits
					// first
						//uint16 empty;			// 16-bits
						//uint16 depth;			// 16-bits
						//uint16 renderState;	// 16-bits
						//uint16 textureID;		// 16-bits
				};
				uint64	key;
			};

			OpaqueKey( const SortKey& rhs );

			OpaqueKey& operator =(uint64 rhs);
			OpaqueKey& operator =(const SortKey& rhs);
		};

		//================================================================
		/**	Transparent Sort Key
			-----------------------------------------------------------------------
			depth(16) | material(16)[shaderid] | textureID(16)  | renderState(16) |
			-----------------------------------------------------------------------
		*/
		//================================================================
		struct TransparentKey
		{
			union
			{
				struct
				{
					uint16 renderState;		// 16-bits
					uint16 textureID;		// 16-bits
					uint16 material;		// 32-bits
					uint16 depth;			// 16-bits
				};
				uint64	key;
			};

			TransparentKey( const SortKey& rhs );

			TransparentKey& operator =(uint64 rhs);
			TransparentKey& operator =(const SortKey& rhs);
		};
#pragma pack(pop)

		//----------------------------------------------------------------
		inline
		SortKey::SortKey()
			: value(0)
		{
		}
		inline
		SortKey& SortKey::operator =(uint64 rhs)
		{
			value = rhs;
			return *this;
		}
		inline
		SortKey& SortKey::operator =(const OpaqueKey& rhs)
		{
			value = rhs.key;
			return *this;
		}
		inline
		SortKey& SortKey::operator =(const TransparentKey& rhs)
		{
			value = rhs.key;
			return *this;
		}

		//----------------------------------------------------------------
		inline
		OpaqueKey::OpaqueKey( const SortKey& rhs )
		{
			key = rhs.value;
		}
		inline
		OpaqueKey& OpaqueKey::operator =(uint64 rhs)
		{
			key = rhs;
			return *this;
		}
		inline
		OpaqueKey& OpaqueKey::operator =(const SortKey& rhs)
		{
			key = rhs.value;
			return *this;
		}

		//----------------------------------------------------------------
		inline
		TransparentKey::TransparentKey( const SortKey& rhs )
		{
			key = rhs.value;
		}
		inline
		TransparentKey& TransparentKey::operator =(uint64 rhs)
		{
			key = rhs;
			return *this;
		}
		inline
		TransparentKey& TransparentKey::operator =(const SortKey& rhs)
		{
			key = rhs.value;
			return *this;
		}
	}
}

#endif	// __CN_RENDER_SORTKEY_H__