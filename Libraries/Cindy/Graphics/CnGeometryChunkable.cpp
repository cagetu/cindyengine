//================================================================
// File:               : CnGeometryChunkable.cpp
// Related Header File : CnGeometryChunkable.h
// Original Author     : changhee
// Creation Date       : 2009. 12. 03
//================================================================
#include "Cindy.h"
#include "CnGeometryChunkable.h"

namespace Cindy
{
	//----------------------------------------------------------------
	/** Const
	*/
	CnGeometryChunkable::CnGeometryChunkable()
	{
	}

	//----------------------------------------------------------------
	/** Dest
	*/
	CnGeometryChunkable::~CnGeometryChunkable()
	{
		RemoveChunks();
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunkable::AddChunk( GeometryChunk::TypeDefines ChunkType, const Ptr<CnGeometryChunk>& Chunk )
	{
		std::pair<ChunkMap::iterator, bool> res = m_Chunks.insert( ChunkMap::value_type(ChunkType, Chunk) );
		Assert(res.second == true, L"Is Alreay Exist!!");
	}

	//----------------------------------------------------------------
	/**
	*/
	const Ptr<CnGeometryChunk>& CnGeometryChunkable::GetChunk( GeometryChunk::TypeDefines ChunkType ) const
	{
		ChunkMap::const_iterator iter = m_Chunks.find( ChunkType );
		return iter->second;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunkable::RemoveChunks()
	{
		m_Chunks.clear();
	}

}