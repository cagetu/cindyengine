//================================================================
// File:               : CnRenderTarget.inl
// Related Header File : CnRenderTarget.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 18
//================================================================

//----------------------------------------------------------------
/** Get Width
	@brief      가로 길이를 구한다.
	@return		int : m_nWidth
*/
inline
int CnRenderTarget::GetWidth() const
{
	return m_nWidth;
}

//----------------------------------------------------------------
/** Get Height
	@brief      세로 길이를 구한다.
	@return       int : m_nHeight
*/
inline
int CnRenderTarget::GetHeight() const
{
	return m_nHeight;
}

//----------------------------------------------------------------
/** Set Name
	@brief      윈도우 이름을 설정.
*/
inline
void CnRenderTarget::SetName( const CnString& strName )
{
	m_strName = strName;
}

//----------------------------------------------------------------
/** Get Name
	@brief      윈도우 이름을 얻는다.
	@return		CnString : m_strName
*/
inline
const CnString& CnRenderTarget::GetName() const
{
	return m_strName;
}

//----------------------------------------------------------------
/**	@brief	타입 결정
*/
inline
void CnRenderTarget::SetType( CnRenderTarget::TYPE eType )
{
	m_eType = eType;
}

//----------------------------------------------------------------
/**	@brief	타입 얻어오기
*/
inline
CnRenderTarget::TYPE CnRenderTarget::GetType() const
{
	return m_eType;
}

//----------------------------------------------------------------
/**	@brief	우선 순위 얻어오기
*/
inline
uint CnRenderTarget::GetPriority() const
{
	return m_nPriority;
}

//----------------------------------------------------------------
/** Active
	@brief	윈도우 활성화 여부 설정
*/
inline
void CnRenderTarget::Active( bool bActive )
{
	m_bIsActive = bActive;
}

//----------------------------------------------------------------
/** Is Active
	@brief	윈도우 활성화 여부
*/
inline
bool CnRenderTarget::IsActive() const
{
	return m_bIsActive;
}

//----------------------------------------------------------------
/**	@brief	안티 알리아싱 레벨
*/
inline
void CnRenderTarget::SetAntiAliasQuality( CnAntiAliasQuality::Type eQuality )
{
	m_eAntiAliasQulity = eQuality;
}

//----------------------------------------------------------------
/**	@brief	안티 알리아싱 레벨
*/
inline
CnAntiAliasQuality::Type CnRenderTarget::GetAntiAliasQuality() const
{
	return m_eAntiAliasQulity;
}


//----------------------------------------------------------------
/** 랜더링시에 렌더타겟의 DepthStencil을 설정한다.
*/
inline
void CnRenderTarget::EnableDepthStencil( bool bEnable )
{
	m_bEnableDepthStencil = bEnable;
}

//----------------------------------------------------------------
/** 렌더링 시에 렌더타겟의 DepthStencil의 활성화 여부를 알아본다.
*/
inline
bool CnRenderTarget::IsEnabledDepthStencil() const
{
	return m_bEnableDepthStencil;
}