//================================================================
// File:           : CnRenderSorting.h
// Original Author : changhee
// Creation Date   : 2009. 11. 31
//================================================================
#ifndef __CN_RENDER_SORTING_H__
#define __CN_RENDER_SORTING_H__

namespace Cindy
{
class CnSubMesh;

namespace Sorting
{
	// type
	typedef bool (*Comparision)(const CnSubMesh*, const CnSubMesh*);

	// 기본
	bool Default( const CnSubMesh* Dest, const CnSubMesh* Src );
	// SortKey가 크도록 정렬
	bool Greater( const CnSubMesh* Dest, const CnSubMesh* Src );
	// SortKey가 작도록 정렬
	bool Less( const CnSubMesh* Dest, const CnSubMesh* Src );
	// 가까운 녀석부터 정렬
	bool FrontToBack( const CnSubMesh* Dest, const CnSubMesh* Src );
	// 먼 녀석부터 정렬
	bool BackToFront( const CnSubMesh* Dest, const CnSubMesh* Src );

	//
	Comparision StringToFunc( const wchar* name );

} // namespace Sorting
} // namespace Cindy

#endif	// __CN_RENDER_SORTING_H__