//================================================================
// File:               : CnTransformDevice.cpp
// Related Header File : CnTransformDevice.h
// Original Author     : changhee
// Creation Date       : 2009. 12. 8
//================================================================
#include "Cindy.h"
#include "CnTransformDevice.h"

namespace Cindy
{
	//-------------------------------------------------------------------------
	/**
	*/
	CnTransformDevice* CnTransformDevice::Instance()
	{
		static CnTransformDevice instance;
		return &instance;
	}

	//-------------------------------------------------------------------------
	/**	Contructor
	*/
	CnTransformDevice::CnTransformDevice()
	{
	}

} // namespace Cindy