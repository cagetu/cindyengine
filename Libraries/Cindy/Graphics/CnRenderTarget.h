//================================================================
// File:           : CnRenderTarget.h
// Original Author : changhee
// Creation Date   : 2007. 4. 18
//================================================================
#pragma once

#include "Scene/CnViewport.h"
#include "CnAntiAliasing.h"

namespace Cindy
{
	//================================================================
	/** Render Target
	    @author    changhee
		@since     2007. 4. 18
		@remarks   랜더 타겟 Base 클래스
	*/
	//================================================================
	class CN_DLL CnRenderTarget : public CnObject
	{
		__DeclareRtti;
	public:
		//enum TYPE
		//{
		//	RT_NONE = 0,
		//	//RT_TEXTURE,
		//	//RT_MRT,
		//	RT_TARGET,
		//	RT_WINDOW,
		//};
		typedef int TYPE;
		const static TYPE RT_NONE = 0;
		const static TYPE RT_TEXTURE = 1;
		const static TYPE RT_MRT = 1;
		const static TYPE RT_WINDOW = 2;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnRenderTarget();
		virtual ~CnRenderTarget();

		// Name
		void				SetName( const CnString& strName );
		const CnString&		GetName() const;

		// Size
		int					GetWidth() const;
		int					GetHeight() const;
		virtual void		Resize( int nWidth, int nHeight );

		// Type
		void				SetType( TYPE eType );
		TYPE				GetType() const;

		// Priority
		void				SetPriority( uint nPriority );
		uint				GetPriority() const;

		// Is Active?
		void				Active( bool bActive );
		bool				IsActive() const;

		// Anti-alias
		void				SetAntiAliasQuality( CnAntiAliasQuality::Type eQuality );
		CnAntiAliasQuality::Type GetAntiAliasQuality() const;

		// 
		void				EnableDepthStencil( bool bEnable );
		bool				IsEnabledDepthStencil() const;

		// Relase/Restore
		virtual void		OnRelease();
		virtual void		OnRestore();
		virtual void		OnReset();

		// Viewport
		virtual CnViewport*	AddViewport( int nZOrder = 0,
										 float fRelTop = 0.0f,
										 float fRelLeft = 0.0f,
										 float fRelWidth = 1.0f,
										 float fRelHeight = 1.0f );
		virtual CnViewport*	GetViewport( int nZOrder );
		virtual bool		RemoveViewport( int nZOrder );
		virtual void		RemoveAllViewports();

		// Update
		virtual bool		Update();
		virtual void		Begin();
		virtual void		Draw();
		virtual void		End();

		// Sort
		static bool Sort( const CnRenderTarget* Dest, const CnRenderTarget* Src );

	protected:
		// List of viewports, map on Z-order
		typedef std::map<int, CnViewport*, std::less<int> >		ViewportMap;
		typedef ViewportMap::iterator							ViewportIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString					m_strName;

		int							m_nWidth;
		int							m_nHeight;

		TYPE						m_eType;
		uint						m_nPriority;

		bool						m_bIsActive;

		ViewportMap					m_Viewports;

		CnAntiAliasQuality::Type	m_eAntiAliasQulity;

		bool						m_bEnableDepthStencil;

		bool						m_bBeginPass;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual bool		_RestoreLostDevice();
	};

#include "CnRenderTarget.inl"
}
