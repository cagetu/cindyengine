//================================================================
// File:           : CnAntiAliasing.h
// Original Author : changhee
// Creation Date   : 2008. 2. 4
//================================================================
#ifndef __CN_ANTIALIASING_H__
#define __CN_ANTIALIASING_H__

namespace Cindy
{
	//================================================================
	/** CnAntiAliasQuality
	    @author    changhee
		@since     2008. 2. 4
		@remarks   ������
	*/
	//================================================================
	class CnAntiAliasQuality
	{
	public:
		enum Type
		{
			None = 0,
			Low,
			Medium,
			High,
		};
	};
}

#endif	// __CN_ANTIALIASING_H__