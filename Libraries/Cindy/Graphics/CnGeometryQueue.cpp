//================================================================
// File:               : CnGeometryQueue.cpp
// Related Header File : CnGeometryQueue.h
// Original Author     : changhee
// Creation Date       : 2009. 12. 03
//================================================================
#include "Cindy.h"
#include "CnGeometryQueue.h"
#include "Scene/CnCamera.h"
#include "Geometry/CnMesh.h"
#include "Geometry/CnSubMesh.h"
#include "Material/CnShaderManager.h"
#include "Graphics/CnRenderer.h"

namespace Cindy
{
	//----------------------------------------------------------------
	/** Const
	*/
	CnGeometryQueue::CnGeometryQueue()
	{
	}

	//----------------------------------------------------------------
	/** Dest
	*/
	CnGeometryQueue::~CnGeometryQueue()
	{
		Clear();
	}

	//----------------------------------------------------------------
	/** Insert
	    @brief      큐에 추가
		@todo		여기에 있는 방법이 좋은 방법이 아니기 때문에, 추후 정리가 필요!!!
	*/
	void CnGeometryQueue::Insert( CnMesh* pMesh )
	{
		ushort num_submeshes = pMesh->GetNumSubMeshes();
		for( ushort i=0; i < num_submeshes; ++i )
		{
			CnSubMesh* subMesh = pMesh->GetSubMesh(i);
			if (!subMesh)
				continue;

			if (false == subMesh->GetMaterial()->IsEnable())
				continue;

			Ptr<CnGeometryChunk> chunk;
			if (subMesh->GetMaterial()->IsTransparent())
			{
				chunk = this->GetChunk( GeometryChunk::AlphaBlend );
			}
			else if (subMesh->GetMaterial()->GetRenderState()->GetAlphaTestEnable())
			{
				chunk = this->GetChunk( GeometryChunk::AlphaTest );
			}
			else
			{
				chunk = this->GetChunk( GeometryChunk::Opaque );
			}

			if (chunk.IsValid())
			{
				chunk->Add( subMesh );
			}
		}
	}

	//----------------------------------------------------------------
	/** Clear
	    @brief	초기화
	*/
	void CnGeometryQueue::Clear()
	{
		ChunkMap::iterator i, iend;
		iend = m_Chunks.end();
		for (i = m_Chunks.begin(); i != iend; i++)
		{
			(i->second)->Clear();
		}
	}

	//----------------------------------------------------------------
	/** Draw
	    @brief      랜더링
		@param        pCamera : 카메라
		@return       none
	*/
	void CnGeometryQueue::Draw( CnCamera* pCamera )
	{
		ChunkMap::iterator i, iend;
		iend = m_Chunks.end();
		for (i = m_Chunks.begin(); i != iend; i++)
		{
			(i->second)->Draw( pCamera );
		}
	}
}