//================================================================
// File:           : CnRenderWindow.h
// Original Author : changhee
// Creation Date   : 2007. 4. 19
//================================================================
#pragma once

#include "Graphics/CnRenderTarget.h"

namespace Cindy
{
	class CnRenderTexture;

	//================================================================
	/** Render Window
	    @author    changhee
		@since     2007. 4. 19
		@remarks   랜더링 될 윈도우
	*/
	//================================================================
	class CN_DLL CnRenderWindow : public CnRenderTarget
	{
		__DeclareRtti;
	public:
		CnRenderWindow( HWND hWnd, const wchar* strName, int nWidth, int nHeight );
		virtual ~CnRenderWindow();

		HWND		GetHWnd() const;

		ushort		GetColorDepth() const;
		ushort		GetRefreshRate() const;

		bool		Update() override;
//		void		Draw() override;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		HWND			m_hWnd;				//!< 윈도우 핸들

		ushort			m_usColorDepth;
		ushort			m_usRefreshRate;
		bool			m_bFullScreen;
	};

#include "CnRenderWindow.inl"
}