//================================================================
// File:           : CnVertexComponent.h
// Original Author : changhee
// Creation Date   : 2009. 4. 10
//================================================================
#ifndef __CN_VERTEXCOMPONENT_H__
#define __CN_VERTEXCOMPONENT_H__

#include "CindyTypes.h"

namespace Cindy
{
	//================================================================
	/** VertexComponent
	    @author		changhee
		@brief		버텍스의 구성요소 하나의 정보
	*/
	//================================================================
	class CnVertexComponent
	{
	public:
		enum Usage
		{
			Position	=0,
			PositionT,
			Normal,
			NormalUB4N,
			Uv0,
			Uv0S2,
			Uv1,
			Uv1S2,
			Uv2,
			Uv2S2,
			Uv3,
			Uv3S2,
			Color,
			ColorUB4N,
			Tangent,
			TangentUB4N,
			Binormal,
			BinormalUB4N,
			BlendWeights,
			BlendWeightsUB4N,
			BlendIndices,
			BlendIndicesUB4,

			Invalid
		};

		enum Format
		{
			Float,      //> one-component float, expanded to (float, 0, 0, 1)
			Float2,     //> two-component float, expanded to (float, float, 0, 1)
			Float3,     //> three-component float, expanded to (float, float, float, 1)
			Float4,     //> four-component float
			UByte4,     //> four-component unsigned byte
			Short2,     //> two-component signed short, expanded to (value, value, 0, 1)
			Short4,     //> four-component signed short
			UByte4N,    //> four-component normalized unsigned byte (value / 255.0f)
			Short2N,    //> two-component normalized signed short (value / 32767.0f)
			Short4N,    //> four-component normalized signed short (value / 32767.0f)
		};

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnVertexComponent();
		CnVertexComponent( Usage eUsage, Format eFormat, ushort usageIndex );

		Usage	GetUsage() const;
		int		GetUsageIndex() const;
		ushort	GetByteSize() const;
		Format	GetFormat() const;

	private:
		Usage	usage_;
		ushort	usageIndex_;
		Format	format_;
	};
}

#include "CnVertexComponent.inl"

#endif	// __CN_VERTEXCOMPONENT_H__