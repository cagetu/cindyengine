//================================================================
// File:           : CnShaderVariableInstance.h
// Original Author : changhee
// Creation Date   : 2008. 11. 24
//================================================================
#ifndef __CN_SHADER_VARIABLE_INSTANCE_H__
#define __CN_SHADER_VARIABLE_INSTANCE_H__

#include "../../Foundation/CnObject.h"
#include "../../Math/CnMatrix44.h"
#include "../../Util/CnColor.h"
#include "../../Util/CnVariable.h"
#include "../../Material/CnTexture.h"

namespace Cindy
{
	class CnShaderVariable;

	//================================================================
	/** Shader VariableInstance
	    @author    changhee
		@remarks   쉐이더 상수 클래스 - Nebula3 쉐이더 시스템 
	*/
	//================================================================
	class CnShaderVariableInstance : public CnObject
	{
		CN_DECLARE_RTTI;
	public:
		CnShaderVariableInstance();
		~CnShaderVariableInstance();

		const Pointer<CnShaderVariable>&	GetShaderVariable() const;

		void	Apply();

		// Set
		void	SetInt( int Value );
		void	SetIntArray( const int* aValues, uint nSize );

		void	SetFloat( float Value );
		void	SetFloatArray( const float* aValues, uint nSize );

		void	SetBool( bool Value );
		void	SetBoolArray( const BOOL* aValues, uint nSize );

		void	SetVector( const Math::Vector4& Value );
		void	SetVectorArray( Math::Vector4* aValues, uint nSize );

		void	SetMatrix( const Math::Matrix44& Value );
		void	SetMatrixArray( Math::Matrix44* aValues, uint nSize );
		
		void	SetTexture( CnTexture* texture );

		void	SetValue( void* pData, uint nBytes );
		void	SetColor( const CnColor& color );

	private:
		friend class CnShaderVariable;

		void	Setup( const Pointer<CnShaderVariable>& Variable );

		Pointer<CnShaderVariable>	m_ShaderVariable;
		CnVariable						m_Value;

		uint							m_NumOfArray;
		void*							m_Array;
	};
}

#include "CnShaderVariableInstance.inl"

#endif	// __CN_SHADER_VARIABLE_INSTANCE_H__