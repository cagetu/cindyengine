//================================================================
// File:               : CnIndexBuffer.cpp
// Related Header File : CnIndexBuffer.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 14
//================================================================
#include "Cindy.h"
#include "CnIndexBuffer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnIndexBuffer, CnObject );

	/// Const/Dest
	CnIndexBuffer::CnIndexBuffer()
		: m_nNumIndices(0)
	{
	}
	CnIndexBuffer::~CnIndexBuffer()
	{
	}
}