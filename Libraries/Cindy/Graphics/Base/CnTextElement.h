//================================================================
// File:           : CnTextElement.h
// Original Author : changhee
// Creation Date   : 2009. 9. 3
//================================================================
#ifndef __CN_TEXT_ELEMENT_H__
#define __CN_TEXT_ELEMENT_H__

#include "Math/CnVector3.h"
#include "Util/CnColor.h"

namespace Cindy
{
	//================================================================
	/** Text Element Class
	    @author    changhee
		@brief	   Text 하나에 대한 정보
				   Position의 경우,
				   2D 상에 표현할 경우, 0~1 사이의 값으로,
				   3D 상에 표현할 경우에는 WorldPosition을 설정한다.
	*/
	//================================================================
	class CnTextElement
	{
	public:
		CnTextElement();
		CnTextElement( const CnString& text, const CnColor& color, const Math::Vector3& pos, bool use3DPos = false );

		const CnString& GetText() const;
		const CnColor& GetColor() const;
		const Math::Vector3& GetPosition() const;

		bool IsUsed3DPos() const;
	private:
		CnString m_Text;
		CnColor m_Color;
		Math::Vector3 m_Pos;
		bool m_Use3DPos;
	};

	//----------------------------------------------------------------
	/**
	*/
	inline const CnString&
	CnTextElement::GetText() const
	{
		return m_Text;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline const CnColor&
	CnTextElement::GetColor() const
	{
		return m_Color;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline const Math::Vector3&
	CnTextElement::GetPosition() const
	{
		return m_Pos;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline bool
	CnTextElement::IsUsed3DPos() const
	{
		return m_Use3DPos;
	}
} // namespace Cindy

#endif // __CN_TEXT_ELEMENT_H__
