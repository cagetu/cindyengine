//================================================================
// File:               : CnRenderWindow.cpp
// Related Header File : CnRenderWindow.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 19
//================================================================
#include "Cindy.h"
#include "CnRenderWindow.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnRenderWindow, CnRenderTarget );
	//----------------------------------------------------------------
	/** Const
	*/
	CnRenderWindow::CnRenderWindow( HWND hWnd,
									const wchar* strName,
									int nWidth,
									int nHeight )
		: CnRenderTarget()
		, m_hWnd( hWnd )
		, m_usColorDepth( 0 )
		, m_usRefreshRate( 0 )
		, m_bFullScreen( false )
	{
		m_strName = strName;
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	}

	//----------------------------------------------------------------
	/**	Destructor
	*/
	CnRenderWindow::~CnRenderWindow()
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	bool CnRenderWindow::Update()
	{
		if (!CnRenderTarget::Update())
			return false;

		return true;
	}

	//----------------------------------------------------------------
	/**
	*/
	//void CnRenderWindow::Draw()
	//{
	//	CnRenderTarget::Draw();
	//}
}
