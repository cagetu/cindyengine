//================================================================
// File:               : CnRenderTexture.cpp
// Related Header File : CnRenderTexture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 19
//================================================================
#include "Cindy.h"
#include "Material/CnTextureManager.h"
#include "CnRenderTexture.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnRenderTexture, CnRenderTarget );
	/// Const/Dest
	CnRenderTexture::CnRenderTexture( const wchar* strName,
									  ushort usWidth,
									  ushort usHeight,
									  ushort usDepth,
									  PixelFormat::Code eFormat,
									  CnAntiAliasQuality::Type eAnitiQuality,
									  bool bEnableDepthStencil,
									  bool bGenerateMipMaps ) : CnRenderTarget()
	{
		m_strName = strName;

		m_Index = 0;

		m_nWidth = usWidth;
		m_nHeight = usHeight;
		m_nDepth = usDepth;

		m_Format = eFormat;
		m_eAntiAliasQulity = eAnitiQuality;
		m_bMipMapsEnabled = bGenerateMipMaps;

		m_bIsActive = false;
		m_bEnableDepthStencil = bEnableDepthStencil;
	}
	CnRenderTexture::~CnRenderTexture()
	{
		//if (CnTextureManager::HasInstance())
		//{
		//	TextureMgr->Remove( m_strName );
		//}

		m_RenderToTexture = NULL;
	}

	//----------------------------------------------------------------
	/** @brief	Setup */
	//----------------------------------------------------------------
	void CnRenderTexture::Setup()
	{
		ushort usage = CnTexture::RenderTargetColor;
		if (this->IsEnabledDepthStencil())
		{
			usage |= CnTexture::RenderTargetDepthStencil;
		}
		if (this->GetMipMapsEnabled())
		{
			usage |= CnTexture::GenerateMipMaps;
		}

		m_RenderToTexture = TextureMgr->Load( m_strName.c_str(),
											  m_nWidth,
											  m_nHeight,
											  m_nDepth,
											  m_Format,
											  usage );
		if (m_RenderToTexture == NULL)
		{
			m_bIsActive = false;
			return;
		}

		m_bIsActive = true;
	}

	//----------------------------------------------------------------
	/** @brief	Texture ���� */
	//----------------------------------------------------------------
	void CnRenderTexture::SetTexture( const TexPtr& spTexture )
	{
		m_RenderToTexture = spTexture;

		m_strName = spTexture->GetName();

		m_nWidth = spTexture->GetWidth();
		m_nHeight = spTexture->GetHeight();
		m_nDepth = spTexture->GetDepth();
		m_Format = spTexture->GetFormat();

		m_bIsActive = true;
	}

	//----------------------------------------------------------------
	/**
	*/
	//----------------------------------------------------------------
	void CnRenderTexture::GenerateMipLevels()
	{
		if (GetMipMapsEnabled())
		{
			m_RenderToTexture->GenerateMipLevels();
		}
	}
}