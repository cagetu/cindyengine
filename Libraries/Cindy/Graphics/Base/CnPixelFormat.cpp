//================================================================
// File:               : CnPixelFormat.cpp
// Related Header File : CnPixelFormat.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 14
//================================================================
#include "Cindy.h"
#include "CnPixelFormat.h"
#include "Util/CnString.h"

namespace Cindy
{
namespace PixelFormat
{
	//----------------------------------------------------------------
	/**	gs_FormatTable
		@brief	Format String ����
	*/
	//----------------------------------------------------------------
	static const wchar* gs_FormatTable[PixelFormat::NumFormats] =
	{
		L"NOFORMAT",
		L"X8R8G8B8",
		L"A8R8G8B8",
		L"R5G6B5",
		L"A1R5G5B5",
		L"A4R4G4B4",
		L"P8",
		// Compress
		L"DXT1",
		L"DXT2",
		L"DXT3",
		L"DXT4",
		L"DXT5",
		// Depth
		L"D32",
		L"D15S1",
		L"D24S8",
		L"D24X8",
		L"D24X4S4",
		L"D16",
		L"D24FS8",
		//
		L"R16F",
		L"G16R16F",
		L"A16B16G16R16F",
		L"R32F",
		L"G32R32F",
		L"A32B32G32R32F",
		//
		L"A8",
		L"L8",
	};

	//----------------------------------------------------------------
	/** 
	*/
	//----------------------------------------------------------------
	PixelFormat::Code PixelFormat::StringToFormat( const wchar* name )
	{
		for( uint i = 0; i < PixelFormat::NumFormats; ++i )
		{
			if (unicode::Compare( gs_FormatTable[i], name ) )
				return (PixelFormat::Code)i;
		}
		return PixelFormat::NOFORMAT;
	}

	//----------------------------------------------------------------
	/** 
	*/
	//----------------------------------------------------------------
	const wchar* PixelFormat::FormatToString( PixelFormat::Code param )
	{
		return gs_FormatTable[param];
	}
} // namespace PixelFormat
} // namespace Cindy