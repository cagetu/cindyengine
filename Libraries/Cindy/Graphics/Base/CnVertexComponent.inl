//================================================================
// File:				: CnVertexComponent.inl
// Related Header File	: CnVertexComponent.h
// Original Author		: changhee
// Creation Date		: 2009. 4. 10
//================================================================
namespace Cindy
{
	//****************************************************************
	//	VertexDeclaration::UsageComponent
	//****************************************************************
	//----------------------------------------------------------------
	inline
	CnVertexComponent::CnVertexComponent()
		: usage_(Invalid)
		, usageIndex_(0)
		, format_(Float)
	{
	}
	//----------------------------------------------------------------
	inline
	CnVertexComponent::CnVertexComponent( Usage eUsage, Format eFormat, ushort usageIndex )
		: usage_(eUsage)
		, usageIndex_(usageIndex)
		, format_(eFormat)
	{
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	inline
	CnVertexComponent::Usage
	CnVertexComponent::GetUsage() const
	{
		return usage_;
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	inline
	int CnVertexComponent::GetUsageIndex() const
	{
		return usageIndex_;
	}

	//----------------------------------------------------------------
	//** @brief	component ������ */
	//----------------------------------------------------------------
	inline
	ushort CnVertexComponent::GetByteSize() const
	{
		switch (format_)
		{
			case Float:     return 4;
			case Float2:    return 8;
			case Float3:    return 12;
			case Float4:    return 16;
			case UByte4:    return 4;
			case Short2:    return 4;
			case Short4:    return 8;
			case UByte4N:   return 4;
			case Short2N:   return 4;
			case Short4N:   return 8;
		}
		return 0;
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	inline
	CnVertexComponent::Format 
	CnVertexComponent::GetFormat() const
	{
		return format_;
	}
}