//================================================================
// File:           : CnShapeRenderer.h
// Original Author : changhee
// Creation Date   : 2009. 6. 29
//================================================================
#ifndef __CN_SHAPE_RENDERER_H__
#define __CN_SHAPE_RENDERER_H__

#include "Foundation/CnObject.h"
#include "Math/CnMatrix44.h"
#include "Util/CnColor.h"

namespace Cindy
{
	class CnCamera;

	//================================================================
	/** ShapeRenderer
	    @author		changhee
		@brief		기본 도형을 그리기 위한 랜더러의 기반 클래스
	*/
	//================================================================
	class CnShapeRenderer : public CnObject
	{
		CN_DECLARE_CLASS(CnShapeRenderer);
	public:
		CnShapeRenderer();
		virtual ~CnShapeRenderer();

		virtual void	Open();
		virtual void	Close();

		virtual void	DrawShape( CnCamera* pCamera,
								   const Math::Matrix44& ModelTransform,
								   CnShapeRenderer::ShapeType eShapeType,
								   const CnColor& Color );

	protected:
		bool	m_IsOpen;
	};
}

#endif // __CN_SHAPE_RENDERER_H__