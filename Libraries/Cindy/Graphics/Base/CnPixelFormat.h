//================================================================
// File:           : CnPixelFormat.h
// Original Author : changhee
// Creation Date   : 2009. 6. 11
//================================================================
#ifndef __CN_PIXEL_FORMAT_H__
#define __CN_PIXEL_FORMAT_H__

namespace Cindy
{
namespace PixelFormat
{
	// pixel formats
	enum Code
	{
		NOFORMAT,
		X8R8G8B8,
		A8R8G8B8,
		R5G6B5,
		A1R5G5B5,
		A4R4G4B4,
		P8,
		// Compress
		DXT1,
		DXT2,
		DXT3,
		DXT4,
		DXT5,
		// Depth
		D32,
		D15S1,
		D24S8,
		D24X8,
		D24X4S4,
		D16,
		D24FS8,
		//
		R16F,                       // 16 bit float, red only
		G16R16F,                    // 32 bit float, 16 bit red, 16 bit green
		A16B16G16R16F,              // 64 bit float, 16 bit rgba each
		R32F,                       // 32 bit float, red only
		G32R32F,                    // 64 bit float, 32 bit red, 32 bit green
		A32B32G32R32F,              // 128 bit float, 32 bit rgba each
		A8,
		L8,

		NumFormats
	};

	Code			StringToFormat( const wchar* format );
	const wchar*	FormatToString( Code format );
} // namespace PixelFormat
} // namespace Cindy

#endif // __CN_PIXEL_FORMAT_H__
