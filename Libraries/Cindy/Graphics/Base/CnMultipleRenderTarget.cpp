//================================================================
// File:               : CnMultipleRenderTarget.cpp
// Related Header File : CnMultipleRenderTarget.h
// Original Author     : changhee
// Creation Date       : 2008. 1. 25
//================================================================
#include "Cindy.h"
#include "Graphics/CnRenderer.h"
#include "CnMultipleRenderTarget.h"

namespace Cindy
{
	//============================================================================
	__ImplementRtti( Cindy, CnMultiRenderTarget, CnRenderTarget );

	// Const/Dest
	CnMultiRenderTarget::CnMultiRenderTarget( const wchar* strName )
		: CnRenderTarget()
	{
		m_strName = strName;

		for (int i=0; i<NUM_MULTIPLE_RENDERTARGETS; ++i)
			m_pSurface[i] = NULL;
	}
	CnMultiRenderTarget::~CnMultiRenderTarget()
	{
		for (int i=0; i<NUM_MULTIPLE_RENDERTARGETS; ++i)
		{
			if (m_pSurface[i].IsNull())
				continue;

			//TextureMgr->Remove( m_pSurface[i]->GetName() );
			m_pSurface[i] = NULL;
		}
	}

	//================================================================
	/** Bind Surface
		@remarks	랜더 타겟 서페이스를 연결한다.
		@param nIndex : 랜더 타겟 index
	*/
	//================================================================
	bool CnMultiRenderTarget::BindSurface( int nIndex, const Ptr<CnRenderTexture>& spTarget )
	{
		for (int i=0; i<NUM_MULTIPLE_RENDERTARGETS; ++i)
		{
			if (m_pSurface[i].IsNull())
				continue;

			if ( (m_pSurface[i]->GetWidth() != spTarget->GetWidth()) ||
				 (m_pSurface[i]->GetHeight() != spTarget->GetHeight()) ||
				 (m_pSurface[i]->GetDepth() != spTarget->GetDepth()) )
				 //(m_pSurface[i]->GetFormat() != spTexture->GetFormat()) )
			{
				Assert(0, "pTexture is invalid Type");
				return false;
			}
		}

		spTarget->SetIndex( nIndex );
		m_pSurface[nIndex] = spTarget;

		if (nIndex == 0)
			Resize( m_pSurface[nIndex]->GetWidth(), m_pSurface[nIndex]->GetHeight() );

		m_bIsActive = true;
		return true;
	}

	//================================================================
	/** Bind Surface
		@remarks	랜더 타겟 서페이스를 연결한다.
		@param nIndex : 랜더 타겟 index
	*/
	//================================================================
	bool CnMultiRenderTarget::BindSurface( int nIndex,
										   const wchar* strName,
										   ushort usWidth,
										   ushort usHeight,
										   ushort usDepth,
										   PixelFormat::Code eFormat,
										   bool enableDepthStencil,
										   bool enableMipMaps)
	{
		//ushort usage = CnTexture::RenderTargetColor;
		//if (enableDepthStencil)
		//{
		//	usage |= CnTexture::RenderTargetDepthStencil;
		//}
		// 새로운 랜더 타겟 추가
		CnRenderTexture* renderTarget =	RenderDevice->CreateRenderTexture( strName,
																		   usWidth,
																		   usHeight,
																		   usDepth,
																		   eFormat,
																		   CnAntiAliasQuality::None,
																		   enableDepthStencil,
																		   enableMipMaps,
																		   true );

		return BindSurface( nIndex, renderTarget );
	}

	////================================================================
	///** Bind Surface
	//	@remarks	랜더 타겟 서페이스를 연결한다.
	//	@param nIndex : 랜더 타겟 index
	//*/
	////================================================================
	//bool CnMultiRenderTarget::BindSurface( int nIndex, const TexPtr& spTexture )
	//{
	//	for (int i=0; i<NUM_MULTIPLE_RENDERTARGETS; ++i)
	//	{
	//		if (m_pSurface[i].IsNull())
	//			continue;

	//		if ( (m_pSurface[i]->GetWidth() != spTexture->GetWidth()) ||
	//			 (m_pSurface[i]->GetHeight() != spTexture->GetHeight()) ||
	//			 (m_pSurface[i]->GetDepth() != spTexture->GetDepth()) )
	//			 //(m_pSurface[i]->GetFormat() != spTexture->GetFormat()) )
	//		{
	//			Assert(0, "pTexture is invalid Type");
	//			return false;
	//		}
	//	}

	//	m_pSurface[nIndex] = spTexture;

	//	if (nIndex == 0)
	//		Resize( m_pSurface[nIndex]->GetWidth(), m_pSurface[nIndex]->GetHeight() );

	//	m_bIsActive = true;
	//	return true;
	//}
	////================================================================
	///** Bind Surface
	//	@remarks	랜더 타겟 서페이스를 연결한다.
	//	@param nIndex : 랜더 타겟 index
	//*/
	////================================================================
	//bool CnMultiRenderTarget::BindSurface( int nIndex,
	//									   const wchar* strName,
	//									   ushort usWidth,
	//									   ushort usHeight,
	//									   ushort usDepth,
	//									   PixelFormat::Code eFormat,
	//									   bool enableDepthStencil)
	//{
	//	ushort usage = CnTexture::RenderTargetColor;
	//	if (enableDepthStencil)
	//	{
	//		usage |= CnTexture::RenderTargetDepthStencil;
	//	}
	//	TexPtr spTexture = TextureMgr->Load( strName, usWidth, usHeight, usDepth, eFormat, usage );

	//	return BindSurface( nIndex, spTexture );
	//}

	//================================================================
	/** Unbind Surface
		@remarks	랜더 타겟 서페이스를 연결을 해제한다.
		@param nIndex : 랜더 타겟 index
	*/
	//================================================================
	void CnMultiRenderTarget::UnbindSurface( int nIndex )
	{
		if (m_pSurface[nIndex].IsValid())
		{
			//TextureMgr->Remove( m_pSurface[nIndex]->GetName() );
			m_pSurface[nIndex] = NULL;
		}
	}
}