//================================================================
// File:           : CnVertexDeclaration.h
// Original Author : changhee
// Creation Date   : 2007. 2. 6
//================================================================
#ifndef __CN_VERTEXDECLARATION_H__
#define __CN_VERTEXDECLARATION_H__

#include "Foundation/CnObject.h"

namespace Cindy
{
//#define __PACK_UV__
//#define __PACK_NORMAL__
//#define __PACK_TANGENT__
#define __PACK_SKIN__
//#define __PACK_COLOR__

#define TANGENT_FLOAT4

	//================================================================
	/** VertexDeclaration
	    @author    changhee
		@since     2007. 2. 12
		@remarks   버텍스 정의 클래스
				   nebula device2, orge3D, 참고..
	*/
	//================================================================
	class CnVertexDeclaration : public CnObject
	{
		__DeclareRtti;
	public:
		enum USAGE
		{
			Position		= (1<<0),
			PositionT		= (1<<1),
			Normal			= (1<<2),
			NormalUB4N		= (1<<3),
			Uv0				= (1<<4),
			Uv0S2			= (1<<5),
			Uv1				= (1<<6),
			Uv1S2			= (1<<7),
			Uv2				= (1<<8),
			Uv2S2			= (1<<9),
			Uv3				= (1<<10),
			Uv3S2			= (1<<11),
			Color			= (1<<12),
			ColorUB4N		= (1<<13),
			Tangent			= (1<<14),
			TangentUB4N		= (1<<15),
			Binormal		= (1<<16),
			BinormalUB4N	= (1<<17),
			BlendWeights	= (1<<18),
			BlendWeightsUB4N = (1<<19),
			BlendIndices	= (1<<20),
			BlendIndicesUB4	= (1<<21),

			NumVertexUsages = 22,
			AllUsageComponents = ((1<<NumVertexUsages) - 1),
		};

		class UsageComponent
		{
		public:
			UsageComponent();
			UsageComponent( int Components );

			void	Add( USAGE eUsage );
			void	Remove( USAGE eUsage );

			void	Set( int Components );
			bool	Has( int nComponent );

			bool	Empty() const;

			int		Get() const;
			int		GetCount() const;
			int		GetOffset( USAGE eUsage ) const;
			ushort	GetSize() const;

		private:
			int		value_;
			int		count_;
		};

		struct VertexElement
		{
			ushort			index;			// Index : Stream ID라고 봐도 무방
			ushort			source;			// 연결된 버텍스 버퍼 인덱스
			UsageComponent	usageComponent;	// usage Component
			ushort			byteSize;

			VertexElement();
			VertexElement( ushort Index, ushort Source, const UsageComponent& Components );
		};

		typedef std::map<ushort, VertexElement>	VertexElementMap;
		typedef VertexElementMap::iterator		VertexElementIter;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		CnVertexDeclaration();
		virtual ~CnVertexDeclaration();

		// VertexElement
		virtual const VertexElement&	AddElement( ushort Index, ushort Source, const UsageComponent& Components );
		virtual void					RemoveElement( ushort Index );
		virtual void					RemoveAllElements();

		const VertexElement*			GetElement( ushort Index );
		const VertexElementMap&			GetElements() const;

		bool							HasComponent( USAGE eComponent );
		int								GetComponents() const;

		// FvF
		virtual ulong					GetFVF() abstract;

		// VertexDeclaration
		virtual void					Build() abstract;

	protected:
		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		VertexElementMap	m_VertexElements;
		int					m_VertexComponents;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		//static ushort	GetVertexSizeFromComponent( int nComponent );
	};
	
	typedef Ptr<CnVertexDeclaration>	VertDeclPtr;
}

#include "CnVertexDeclaration.inl"

#endif	// __CN_VERTEXDECLARATION_H__