//================================================================
// File:               : CnVertexBuffer.cpp
// Related Header File : CnVertexBuffer.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 14
//================================================================
#include "Cindy.h"
#include "CnVertexBuffer.h"
#include "CnVertexDeclaration.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnVertexBuffer, CnObject);

	/// Const/Dest
	CnVertexBuffer::CnVertexBuffer()
		: m_nSizeOfVertex(0)
	{
	}
	CnVertexBuffer::~CnVertexBuffer()
	{
	}

	//================================================================
	//	Class VertexBufferBinder
	//================================================================
	CnVertexBufferBinder::CnVertexBufferBinder()
	{
	}
	CnVertexBufferBinder::~CnVertexBufferBinder()
	{
		Clear();
	}

	//----------------------------------------------------------------
	/** @brief	Bind VertexBuffer
	*/
	void CnVertexBufferBinder::Bind( ushort StreamID, Ptr<CnVertexBuffer>& pBuffer )
	{
		std::pair<BufferIter, bool> result = m_Buffers.insert( BufferMap::value_type( StreamID, pBuffer ) );
		if (!result.second)
		{
#pragma NOTE("여기 확인한번 해보자")
			//delete iWhere->second;
			(*result.first).second = pBuffer;
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnVertexBufferBinder::Unbind( ushort StreamID )
	{
		BufferIter iWhere = m_Buffers.find( StreamID );
		if (m_Buffers.end() == iWhere)
			return;

		//delete iWhere->second;
		m_Buffers.erase( iWhere );
	}
	
	//----------------------------------------------------------------
	/**
	*/
	void CnVertexBufferBinder::Clear()
	{
		//BufferIter iend = m_Buffers.end();
		//for (BufferIter i=m_Buffers.begin(); i!=iend; ++i)
		//	delete i->second;
		m_Buffers.clear();
	}

	//----------------------------------------------------------------
	/**
	*/
	const Ptr<CnVertexBuffer>& CnVertexBufferBinder::GetBuffer( ushort StreamID )
	{
		BufferIter iWhere = m_Buffers.find( StreamID );
		//if (m_Buffers.end() == iWhere)
		//	return NULL;

		return iWhere->second;
	}

	//----------------------------------------------------------------
	/**
	*/
	const CnVertexBufferBinder::BufferMap& CnVertexBufferBinder::GetBuffers() const
	{
		return m_Buffers;
	}
}