//================================================================
// File:               : CnShaderInstance.inl
// Related Header File : CnShaderInstance.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 17
//================================================================
//----------------------------------------------------------------
/** @brief	���̴� ���� ���� */
//----------------------------------------------------------------
inline
unsigned int CnShaderInstance::GetNumVariables() const
{
	return m_nNumVariables;
}

//----------------------------------------------------------------
/** @brief	���̴� Technique ���� */
//----------------------------------------------------------------
inline
unsigned int CnShaderInstance::GetNumTechniques() const
{
	return m_nNumTechniques;
}

//----------------------------------------------------------------
/** @brief	���̴� ���� ���ϱ� ���� */
//----------------------------------------------------------------
inline
CnShader* CnShaderInstance::GetOwner() const
{
	return m_Owner;
}