//================================================================
// File:               : CnRenderWindow.inl
// Related Header File : CnRenderWindow.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 19
//================================================================
//----------------------------------------------------------------
/** Get Window Handle
	@remarks	윈도우 핸들을 얻어온다.
*/
//----------------------------------------------------------------
inline
HWND CnRenderWindow::GetHWnd() const
{
	return m_hWnd;
}

//----------------------------------------------------------------
/** Get Color Depth
	@remarks	Color 깊이 값.
*/
//----------------------------------------------------------------
inline
ushort CnRenderWindow::GetColorDepth() const
{
	return m_usColorDepth;
}

//----------------------------------------------------------------
/** Get Refresh Rate
	@remarks	화면 주사율.
*/
//----------------------------------------------------------------
inline
ushort CnRenderWindow::GetRefreshRate() const
{
	return m_usRefreshRate;
}