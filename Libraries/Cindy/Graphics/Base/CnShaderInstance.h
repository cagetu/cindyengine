//================================================================
// File:           : CnShaderInstance.h
// Original Author : changhee
// Creation Date   : 2008. 11. 17
//================================================================
#ifndef __CN_SHADER_INSTANCE_H__
#define __CN_SHADER_INSTANCE_H__

#include "CnShaderTechnique.h"
#include "CnShaderVariable.h"

namespace Cindy
{
	class CnShader;

	//================================================================
	/** Shader Instance
	    @author    changhee
		@since     2008. 11. 17
		@remarks   ���̴� Ŭ���� - Nebula3 ���̴� �ý��� 
	*/
	//================================================================
	class CN_DLL CnShaderInstance : public CnObject
	{
		CN_DECLARE_RTTI;
	public:
		CnShaderInstance();
		virtual ~CnShaderInstance();

		CnShader*				GetOwner() const;
		void					Discard();

		/// Shader Begin/End
		virtual unsigned int	Begin( bool bSaveState = true );
		virtual void			End();

		/// Pass
		virtual void			BeginPass( uint nPass );
		virtual void			EndPass();
		virtual void			CommitChanges();

		/// Variable
		unsigned int			GetNumVariables() const;
		CnShaderVariable*		GetVariableByIndex( uint Index ) const;
		CnShaderVariable*		GetVariableByName( const CnShaderVariable::Name& Name ) const;
		CnShaderVariable*		GetVariableBySementic( const CnShaderVariable::Sementic& Sementic ) const;

		/// Technique
		unsigned int			GetNumTechniques() const;
		CnShaderTechnique*		GetTechnique( CnShaderFeature::Mask Mask ) const;
		virtual bool			ActiveTechnique( CnShaderFeature::Mask Mask );
	protected:
		typedef std::vector<CnShaderVariable*>									VariableList;
		typedef HashMap<const CnShaderVariable::Name, CnShaderVariable*>		VariableListByName;
		typedef HashMap<const CnShaderVariable::Sementic, CnShaderVariable*>	VariableListBySementic;

		typedef std::map<CnShaderFeature::Mask, CnShaderTechnique*>		TechniqueList;

		friend class CnShader;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		CnShader*	m_Owner;

		/// Flags
		bool		m_Begin;
		bool		m_BeginPass;

		/// Variables
		VariableList			m_Variables;
		VariableListByName		m_VariablesByName;
		VariableListBySementic	m_VariablesBySementic;
		unsigned int			m_nNumVariables;

		/// Technique
		CnShaderTechnique*	m_ActiveTechnique;
		TechniqueList		m_Techniques;
		unsigned int		m_nNumTechniques;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		virtual void	Setup( CnShader* Owner );
		virtual void	Cleanup();

		// Lost/Restore
		virtual void	OnLostDevice() abstract;
		virtual void	OnRestoreDevice() abstract;
	};

#include "CnShaderInstance.inl"
}

#endif	// __CN_SHADER_INSTANCE_H__