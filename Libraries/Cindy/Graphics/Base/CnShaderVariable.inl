//================================================================
// File:               : CnShaderVariable.inl
// Related Header File : CnShaderVariable.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 14
//================================================================
namespace Cindy
{
	//================================================================
	/** @brief		쉐이더 상수 타입 */
	//================================================================
	inline
	void CnShaderVariable::SetType( CnShaderVariable::Type eType )
	{
		m_Type = eType;
	}
	inline
	CnShaderVariable::Type CnShaderVariable::GetType() const
	{
		return m_Type;
	}

	//================================================================
	/** @brief		쉐이더 상수 이름 */
	//================================================================
	inline
	void CnShaderVariable::SetName( const CnShaderVariable::Name& name )
	{
		m_Name = name;
	}
	inline
	const CnShaderVariable::Name& CnShaderVariable::GetName() const
	{
		return m_Name;
	}

	//================================================================
	/** @brief		쉐이더 상수 Sementic */
	//================================================================
	inline
	void CnShaderVariable::SetSementic( const CnShaderVariable::Sementic& sementic )
	{
		m_Sementic = sementic;
	}
	inline
	const CnShaderVariable::Sementic& CnShaderVariable::GetSementic() const
	{
		return m_Sementic;
	}

	//================================================================
	/** @brief	배열 개수 */
	//================================================================
	inline
	void CnShaderVariable::SetNumArrayElements( uint numArrayElements )
	{
		m_NumArrayElements = numArrayElements;
	}
	inline
	uint CnShaderVariable::GetNumArrayElements() const
	{
		return m_NumArrayElements;
	}

	inline
	bool CnShaderVariable::IsArray() const
	{
		return (m_NumArrayElements > 1);
	}
}