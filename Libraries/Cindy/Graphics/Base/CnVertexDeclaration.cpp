//================================================================
// File:               : CnVertexDeclaration.cpp
// Related Header File : CnVertexDeclaration.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 12
//================================================================
#include "Cindy.h"
#include "Util/CnLog.h"
#include "CnVertexDeclaration.h"

namespace Cindy
{
	//****************************************************************
	//	Class VertexDeclaration
	//****************************************************************
	__ImplementRtti(Cindy, CnVertexDeclaration, CnObject );
	/// Const/Dest
	CnVertexDeclaration::CnVertexDeclaration()
		: m_VertexComponents(0)
	{
	}
	CnVertexDeclaration::~CnVertexDeclaration()
	{
		RemoveAllElements();
	}

	//================================================================
	/** Has Component
	*/
	//================================================================
	bool CnVertexDeclaration::HasComponent( CnVertexDeclaration::USAGE eComponent )
	{
		VertexElementIter i = m_VertexElements.begin();
		VertexElementIter iend = m_VertexElements.end();
		for (; i != iend; i++)
		{
			if (i->second.usageComponent.Has( eComponent ))
				return true;
		}
		return false;
	}

	//================================================================
	/** Add VertexElement
	*/
	//================================================================
	const CnVertexDeclaration::VertexElement&
	CnVertexDeclaration::AddElement( ushort Index, ushort Source, const CnVertexDeclaration::UsageComponent& Components )
	{
		pair<VertexElementIter, bool> result = m_VertexElements.insert( VertexElementMap::value_type( Index, VertexElement( Index, Source, Components ) ) );
		return result.first->second;
	}

	//================================================================
	/** Remove VertexElement
	*/
	//================================================================
	void CnVertexDeclaration::RemoveElement( ushort Index )
	{
		VertexElementIter iWhere = m_VertexElements.find( Index );
		if (m_VertexElements.end() == iWhere)
			return;

		m_VertexElements.erase( iWhere );
	}
	void CnVertexDeclaration::RemoveAllElements()
	{
		m_VertexElements.clear();
	}

	//================================================================
	/** Get VertexElement
	*/
	//================================================================
	const CnVertexDeclaration::VertexElement*
	CnVertexDeclaration::GetElement( ushort Index )
	{
		VertexElementIter iWhere = m_VertexElements.find( Index );
		if (m_VertexElements.end() == iWhere)
			return NULL;

		return &iWhere->second;
	}
	const CnVertexDeclaration::VertexElementMap&
	CnVertexDeclaration::GetElements() const
	{
		return m_VertexElements;
	}
	
	//****************************************************************
	//	Class UsageComponent
	//****************************************************************
	CnVertexDeclaration::UsageComponent::UsageComponent()
		: value_(0)
		, count_(0)
	{
	}
	CnVertexDeclaration::UsageComponent::UsageComponent( int Components )
	{
		Set( Components );
	}

	//----------------------------------------------------------------
	int CnVertexDeclaration::UsageComponent::GetOffset( CnVertexDeclaration::USAGE eUsage ) const
	{
		int ret = 0; 

		if (Position == eUsage)		return ret;
		if (value_ & Position)		ret += 3;

#ifdef __PACK_NORMAL__
		if (Normal == eUsage)		return ret;
		if (value_ & Normal)		ret += 1;
#else
		if (Normal == eUsage)		return ret;
		if (value_ & Normal)		ret += 3;
#endif	// __PACK_NORMAL__

#ifdef __PACK_UV__
		if (Uv0 == eUsage)			return ret;
		if (value_ & Uv0)			ret += 1;

		if (Uv1 == eUsage)			return ret;
		if (value_ & Uv1)			ret += 1;

		if (Uv2 == eUsage)			return ret;
		if (value_ & Uv2)			ret += 1;

		if (Uv3 == eUsage)			return ret;
		if (value_ & Uv3)			ret += 1;
#else
		if (Uv0 == eUsage)			return ret;
		if (value_ & Uv0)			ret += 2;

		if (Uv1 == eUsage)			return ret;
		if (value_ & Uv1)			ret += 2;

		if (Uv2 == eUsage)			return ret;
		if (value_ & Uv2)			ret += 2;

		if (Uv3 == eUsage)			return ret;
		if (value_ & Uv3)			ret += 2;
#endif // __PACK_UV__

		if (Color == eUsage)		return ret;
		if (value_ & Color)			ret += 4;

#ifdef __PACK_TANGENT__
		if (Tangent == eUsage)		return ret;
		if (value_ & Tangent)		ret += 1;

		if (Binormal == eUsage)		return ret;
		if (value_ & Binormal)		ret += 1;
#else
		if (Tangent == eUsage)		return ret;
		if (value_ & Tangent)		ret += 3;

		if (Binormal == eUsage)		return ret;
		if (value_ & Binormal)		ret += 3;
#endif // __PACK_TANGENT__

#ifdef __PACK_SKIN__
		if (BlendWeights == eUsage)	return ret;    
		if (value_ & BlendWeights)	ret += 1;

		if (BlendIndices == eUsage)	return ret;
		if (value_ & BlendIndices)	ret += 1;
#else
		if (BlendWeights == eUsage)	return ret;    
		if (value_ & BlendWeights)	ret += 4;

		if (BlendIndices == eUsage)	return ret;
		if (value_ & BlendIndices)	ret += 4;
#endif // __PACK_SKIN__

		// add more components here
		CnError( ToStr(L"Requested eUsage('%i') was not found!\n", (int)eUsage) );
		return -1;
	}

	//****************************************************************
	//	Class VertexElement
	//****************************************************************
	CnVertexDeclaration::VertexElement::VertexElement()
		: index(0xffff)
		, source(0xffff)
		, byteSize(0)
	{
	}
	CnVertexDeclaration::VertexElement::VertexElement( ushort Index, ushort Source, const UsageComponent& Components )
		: index(Index)
		, source(Source)
		, usageComponent(Components)
	{
		byteSize = usageComponent.GetSize()*sizeof(float); 
	}
}