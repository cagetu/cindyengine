//================================================================
// File:           : CnShaderFeature.h
// Original Author : changhee
// Creation Date   : 2008. 11. 14
//================================================================
#ifndef __CN_SHADER_FEATURE_H__
#define __CN_SHADER_FEATURE_H__

#include "../../Cindy.h"

namespace Cindy
{
	//================================================================
	/** Shader Feature
	    @author    changhee
		@remarks   Shader Technique가 어떤 State를 가지고 있는지에 대한
				   정보 클래스

				   Nebula3 ShaderFeature 참고
	*/
	//================================================================
	class CnShaderFeature
	{
		friend class CnShaderManager;
	public:
		typedef unsigned int	Mask;

		CnShaderFeature();
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		uint	m_UniqueId;
		stdext::hash_map<std::string, uint>		m_StringToIndex;
		std::vector<std::string>				m_IndexToString;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		Mask	StringToMask( const std::string& Features );
		void	MaskToString( Mask Features, std::string& Output );
	};
}

#endif	// __CN_SHADER_FEATURE_H__