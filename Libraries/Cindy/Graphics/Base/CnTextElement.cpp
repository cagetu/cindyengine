//================================================================
// File:               : CnTextElement.cpp
// Related Header File : CnTextElement.h
// Original Author     : changhee
// Creation Date       : 2009. 9. 3
//================================================================
#include "Cindy.h"
#include "CnTextElement.h"

namespace Cindy
{
	/// Constructor
	CnTextElement::CnTextElement()
	{
	}
	/// Constructor
	CnTextElement::CnTextElement( const CnString& text, const CnColor& color, const Math::Vector3& pos, bool use3DPos )
		: m_Text(text)
		, m_Color(color)
		, m_Pos(pos)
		, m_Use3DPos(use3DPos)
	{
	}
} // namespace Cindy