//================================================================
// File:               : CnShaderTechnique.cpp
// Related Header File : CnShaderTechnique.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 14
//================================================================
#include "CnShaderTechnique.h"

namespace Cindy
{
	CN_IMPLEMENT_RTTI(Cindy, CnShaderTechnique, CnObject);
	/// Const/Dest
	CnShaderTechnique::CnShaderTechnique()
		: m_NumPasses(0)
		, m_FeatureMask(0)
	{
	}
	CnShaderTechnique::~CnShaderTechnique()
	{
	}
}