//================================================================
// File:           : CnShaderVariable.h
// Original Author : changhee
// Creation Date   : 2008. 11. 14
//================================================================
#ifndef __CN_SHADER_VARIABLE_H__
#define __CN_SHADER_VARIABLE_H__

#include "../../Foundation/CnObject.h"
#include "../../Math/CnMatrix44.h"
#include "../../Util/CnColor.h"
#include "../../Material/CnTexture.h"

namespace Cindy
{
	//================================================================
	/** Shader Variable
	    @author    changhee
		@remarks   쉐이더 상수 클래스 - Nebula3 쉐이더 시스템 
	*/
	//================================================================
	class CnShaderVariable : public CnObject
	{
		CN_DECLARE_RTTI;
	public:
		enum Type
		{
			UnknownType = 0,
			IntType,
			BoolType,
			FloatType,
			VectorType,
			MatrixType,
			TextureType,
		};

		typedef std::string Name;
		typedef std::string	Sementic;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnShaderVariable();
		virtual ~CnShaderVariable();

		// Type
		Type			GetType() const;

		// Name
		const Name&		GetName() const;

		// Sementic
		const Sementic&	GetSementic() const;

		// Array
		uint			GetNumArrayElements() const;
		bool			IsArray() const;

		// 이름 변환
		static void		TypeToString( Type eType, CnString& Output );

		// Set
		virtual void	SetBool( bool Value ) abstract;
		virtual void	SetBoolArray( const BOOL* aValues, uint nSize ) abstract;

		virtual void	SetInt( int Value ) abstract;
		virtual void	SetIntArray( const int* aValues, uint nSize ) abstract;

		virtual void	SetFloat( float Value ) abstract;
		virtual void	SetFloatArray( const float* aValues, uint nSize ) abstract;

		virtual void	SetVector( const Math::Vector4& Value ) abstract;
		virtual void	SetVectorArray( Math::Vector4* aValues, uint nSize ) abstract;

		virtual void	SetMatrix( const Math::Matrix44& Value ) abstract;
		virtual void	SetMatrixArray( Math::Matrix44* aValues, uint nSize ) abstract;
		
		virtual void	SetTexture( CnTexture* texture ) abstract;

		virtual void	SetValue( void* pData, uint nBytes ) abstract;

		virtual void	SetColor( const CnColor& color ) abstract;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		Type		m_Type;
		Name		m_Name;
		Sementic	m_Sementic;
		uint		m_NumArrayElements;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void	SetType( Type eType );
		void	SetName( const Name& name );
		void	SetSementic( const Sementic& sementic );
		void	SetNumArrayElements( uint numArrayElements );
	};
} // namespace Cindy

#include "CnShaderVariable.inl"

#endif	// __CN_SHADER_VARIABLE_H__