//================================================================
// File:               : CnShaderVariableInstance.cpp
// Related Header File : CnShaderVariableInstance.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 24
//================================================================
#include "../../Cindy.h"
#include "../../Util/CnLog.h"
#include "CnShaderVariableInstance.h"
#include "CnShaderVariable.h"

namespace Cindy
{
	CN_IMPLEMENT_RTTI(Cindy, CnShaderVariableInstance, CnObject);
	/// Const/Dest
	CnShaderVariableInstance::CnShaderVariableInstance()
		: m_NumOfArray(0)
		, m_Array(0)
	{
	}
	CnShaderVariableInstance::~CnShaderVariableInstance()
	{
	}

	//================================================================
	/** @brief	Setup */
	//================================================================
	void CnShaderVariableInstance::Setup( const Pointer<CnShaderVariable>& Variable )
	{
		m_ShaderVariable = Variable;

		switch (m_ShaderVariable->GetType())
		{
		case CnShaderVariable::IntType:
			m_Value.SetType(CnVariable::Int);
			if (Variable->IsArray())
			{
				m_NumOfArray = Variable->GetNumArrayElements();
				m_Array = malloc(sizeof(int)*m_NumOfArray);
			}
			break;
		case CnShaderVariable::BoolType:
			m_Value.SetType(CnVariable::Boolean);
			if (Variable->IsArray())
			{
				m_NumOfArray = Variable->GetNumArrayElements();
				m_Array = malloc(sizeof(BOOL)*m_NumOfArray);
			}
			break;
		case CnShaderVariable::FloatType:
			m_Value.SetType(CnVariable::Float);
			if (Variable->IsArray())
			{
				m_NumOfArray = Variable->GetNumArrayElements();
				m_Array = malloc(sizeof(float)*m_NumOfArray);
			}
			break;
		case CnShaderVariable::VectorType:
			m_Value.SetType(CnVariable::Vector4);
			if (Variable->IsArray())
			{
				m_NumOfArray = Variable->GetNumArrayElements();
				m_Array = malloc(sizeof(Math::Vector4)*m_NumOfArray);
			}
			break;
		case CnShaderVariable::MatrixType:
			m_Value.SetType(CnVariable::Matrix44);
			if (Variable->IsArray())
			{
				m_NumOfArray = Variable->GetNumArrayElements();
				m_Array = malloc(sizeof(Math::Matrix44)*m_NumOfArray);
			}
			break;
		case CnShaderVariable::TextureType:
			m_Value.SetType(CnVariable::Object);
			assert(!Variable->IsArray());
			break;
		default:
			break;
		}
	}

	//================================================================
	/** @brief	���� */
	//================================================================
	void CnShaderVariableInstance::Apply()
	{
		if (m_NumOfArray > 0)
		{
			switch (m_Value.GetType())
			{
			case CnVariable::Int:
				m_ShaderVariable->SetIntArray( (int*)m_Array, m_NumOfArray );
				break;
			case CnVariable::Float:
				m_ShaderVariable->SetFloatArray( (float*)m_Array, m_NumOfArray );
				break;
			case CnVariable::Boolean:
				m_ShaderVariable->SetBoolArray( (BOOL*)m_Array, m_NumOfArray );
				break;
			case CnVariable::Vector4:
				m_ShaderVariable->SetVectorArray( (Math::Vector4*)m_Array, m_NumOfArray );
				break;
			case CnVariable::Matrix44:
				m_ShaderVariable->SetMatrixArray( (Math::Matrix44*)m_Array, m_NumOfArray );
				break;
			default:
				CNERROR( L"[CnShaderVariableInstance::Apply] Invalid Type Array" );
				break;
			}
		}
		else
		{
			switch (m_Value.GetType())
			{
			case CnVariable::Int:
				m_ShaderVariable->SetInt( m_Value.GetInt() );
				break;
			case CnVariable::Float:
				m_ShaderVariable->SetFloat( m_Value.GetFloat() );
				break;
			case CnVariable::Boolean:
				m_ShaderVariable->SetBool( (BOOL)m_Value.GetBool() );
				break;
			case CnVariable::Vector4:
				m_ShaderVariable->SetVector( m_Value.GetVector4() );
				break;
			case CnVariable::Matrix44:
				m_ShaderVariable->SetMatrix( m_Value.GetMatrix44() );
				break;
			case CnVariable::Object:
				m_ShaderVariable->SetTexture( (CnTexture*)m_Value.GetObject() );
				break;
			default:
				CNERROR( L"[CnShaderVariableInstance::Apply] Invalid Type" );
				break;
			}
		}
	}
}