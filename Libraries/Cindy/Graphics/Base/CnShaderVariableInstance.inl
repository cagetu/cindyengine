//================================================================
// File:               : CnShaderVariableInstance.inl
// Related Header File : CnShaderVariableInstance.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 24
//================================================================
namespace Cindy
{
	//
	inline const Pointer<CnShaderVariable>&
	CnShaderVariableInstance::GetShaderVariable() const
	{
		return m_ShaderVariable;
	}

	inline
	void CnShaderVariableInstance::SetInt( int Value )
	{
		assert(m_Value.GetType() == CnVariable::Int);
		m_Value = Value;
	}
	inline
	void CnShaderVariableInstance::SetIntArray( const int* aValues, uint nSize )
	{
		assert(m_Value.GetType() == CnVariable::Int);
		assert(m_NumOfArray >= nSize);
		memcpy( m_Array, aValues, sizeof(int)*nSize );
	}

	inline
	void CnShaderVariableInstance::SetFloat( float Value )
	{
		assert(m_Value.GetType() == CnVariable::Float);
		m_Value = Value;
	}
	inline
	void CnShaderVariableInstance::SetFloatArray( const float* aValues, uint nSize )
	{
		assert(m_Value.GetType() == CnVariable::Float);
		assert(m_NumOfArray >= nSize);
		memcpy( m_Array, aValues, sizeof(float)*nSize );
	}

	inline
	void CnShaderVariableInstance::SetBool( bool Value )
	{
		assert(m_Value.GetType() == CnVariable::Boolean);
		m_Value = Value;
	}
	inline
	void CnShaderVariableInstance::SetBoolArray( const BOOL* aValues, uint nSize )
	{
		assert(m_Value.GetType() == CnVariable::Boolean);
		assert(m_NumOfArray >= nSize);
		memcpy( m_Array, aValues, sizeof(BOOL)*nSize );
	}

	inline
	void CnShaderVariableInstance::SetVector( const Math::Vector4& Value )
	{
		assert(m_Value.GetType() == CnVariable::Vector4);
		m_Value = Value;
	}
	inline
	void CnShaderVariableInstance::SetVectorArray( Math::Vector4* aValues, uint nSize )
	{
		assert(m_Value.GetType() == CnVariable::Vector4);
		assert(m_NumOfArray >= nSize);
		memcpy( m_Array, aValues, sizeof(Math::Vector4)*nSize );
	}

	inline
	void CnShaderVariableInstance::SetMatrix( const Math::Matrix44& Value )
	{
		assert(m_Value.GetType() == CnVariable::Matrix44);
		m_Value = Value;
	}
	inline
	void CnShaderVariableInstance::SetMatrixArray( Math::Matrix44* aValues, uint nSize )
	{
		assert(m_Value.GetType() == CnVariable::Matrix44);
		assert(m_NumOfArray >= nSize);
		memcpy( m_Array, aValues, sizeof(Math::Matrix44)*nSize );
	}

	inline
	void CnShaderVariableInstance::SetTexture( CnTexture* texture )
	{
		assert(m_Value.GetType() == CnVariable::Object);
		m_Value.SetObject( texture );
	}

	inline
	void CnShaderVariableInstance::SetValue( void* pData, uint nBytes )
	{
	}

	inline
	void CnShaderVariableInstance::SetColor( const CnColor& color )
	{
	}
}