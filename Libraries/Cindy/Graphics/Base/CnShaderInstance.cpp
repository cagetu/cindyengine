//================================================================
// File:               : CnShaderInstance.cpp
// Related Header File : CnShaderInstance.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 17
//================================================================
#include "../../Cindy.h"
#include "../../Util/CnLog.h"
#include "../../Material/CnShader.h"
#include "CnShaderInstance.h"

namespace Cindy
{
	//================================================================
	CN_IMPLEMENT_RTTI(Cindy, CnShaderInstance, CnObject);
	/// Const/Dest
	CnShaderInstance::CnShaderInstance()
		: m_Owner(0)
		, m_Begin(false)
		, m_BeginPass(false)
		, m_nNumVariables(0)
		, m_nNumTechniques(0)
		, m_ActiveTechnique(0)
	{
	}
	CnShaderInstance::~CnShaderInstance()
	{
	}

	//----------------------------------------------------------------
	/** @brief	���̴� ���� */
	//----------------------------------------------------------------
	void CnShaderInstance::Setup( CnShader* Owner )
	{
		m_Owner = Owner;
	}
	void CnShaderInstance::Cleanup()
	{
		VariableList::iterator vi, viend;
		viend = m_Variables.end();
		for (vi=m_Variables.begin(); vi!=viend; ++vi)
			delete (*vi);
		m_Variables.clear();
		m_VariablesByName.clear();
		m_VariablesBySementic.clear();
		m_nNumVariables = 0;

		TechniqueList::iterator ti, tiend;
		tiend = m_Techniques.end();
		for (ti=m_Techniques.begin(); ti!=tiend; ++ti)
			delete (ti->second);
		m_Techniques.clear();
		m_nNumTechniques = 0;
		m_ActiveTechnique = 0;
	}

	//----------------------------------------------------------------
	/** @brief	���̴� Begin/End */
	//----------------------------------------------------------------
	unsigned int CnShaderInstance::Begin( bool bSaveState )
	{
		assert( false == m_Begin );
		assert( false == m_BeginPass );

		m_Begin = true;

		return 0;
	}
	void CnShaderInstance::End()
	{
		assert( true == m_Begin );
		assert( false == m_BeginPass );

		m_Begin = false;
	}

	//----------------------------------------------------------------
	/** @brief	Discard */
	//----------------------------------------------------------------
	void CnShaderInstance::Discard()
	{
		m_Owner->DiscardInstance( this );
	}

	//----------------------------------------------------------------
	/** @brief	���̴� BeginPass/EndPass */
	//----------------------------------------------------------------
	void CnShaderInstance::BeginPass( uint nPass )
	{
		assert( true == m_Begin );
		assert( false == m_BeginPass );

		m_BeginPass = true;
	}
	void CnShaderInstance::EndPass()
	{
		assert( true == m_Begin );
		assert( true == m_BeginPass );

		m_BeginPass = false;
	}

	//----------------------------------------------------------------
	/** @brief	���̴� CommitChanges ���� */
	//----------------------------------------------------------------
	void CnShaderInstance::CommitChanges()
	{
		assert( true == m_BeginPass );
	}

	//----------------------------------------------------------------
	/** @brief	���̴� Variable ������ */
	//----------------------------------------------------------------
	CnShaderVariable* CnShaderInstance::GetVariableByIndex( uint Index ) const
	{
		return m_Variables[Index];
	}
	CnShaderVariable* CnShaderInstance::GetVariableByName( const CnShaderVariable::Name& Name ) const
	{
		VariableListByName::const_iterator iter = m_VariablesByName.find( Name );
		if (m_VariablesByName.end() == iter)
			return NULL;

		return iter->second;
	}
	CnShaderVariable* CnShaderInstance::GetVariableBySementic( const CnShaderVariable::Sementic& Sementic ) const
	{
		VariableListBySementic::const_iterator iter = m_VariablesBySementic.find( Sementic );
		if (m_VariablesBySementic.end() == iter)
			return NULL;

		return iter->second;
	}

	//----------------------------------------------------------------
	/** @brief	���̴� Technique ������ */
	//----------------------------------------------------------------
	CnShaderTechnique* CnShaderInstance::GetTechnique( CnShaderFeature::Mask Mask ) const
	{
		TechniqueList::const_iterator iter = m_Techniques.find( Mask );
		if (m_Techniques.end() == iter)
			return NULL;

		return iter->second;
	}

	//----------------------------------------------------------------
	/** @brief	���̴� Technique Ȱ��ȭ */
	//----------------------------------------------------------------
	bool CnShaderInstance::ActiveTechnique( CnShaderFeature::Mask Mask )
	{
		TechniqueList::iterator iter = m_Techniques.find( Mask );
		if (m_Techniques.end() == iter)
		{
			CNERROR( CnStringUtil::ToString( L"Unknown shader technique" ).c_str() );
			return false;
		}

		m_ActiveTechnique = iter->second;
		return true;
	}
}