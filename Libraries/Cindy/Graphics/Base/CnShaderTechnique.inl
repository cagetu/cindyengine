//================================================================
// File:               : CnShaderTechnique.inl
// Related Header File : CnShaderTechnique.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 14
//================================================================
//================================================================
/** @brief		�̸� */
//================================================================
inline
void CnShaderTechnique::SetName( const Name& name )
{
	m_Name = name;
}
inline
const CnShaderTechnique::Name& CnShaderTechnique::GetName() const
{
	return m_Name;
}

//================================================================
/** @brief		Pass ���� */
//================================================================
inline
void CnShaderTechnique::SetNumPasses( uint nNumPasses )
{
	m_NumPasses = nNumPasses;
}
inline
uint CnShaderTechnique::GetNumPasses() const
{
	return m_NumPasses;
}

//================================================================
/** @brief		Feature Mask */
//================================================================
inline
void CnShaderTechnique::SetFeatureMask( CnShaderFeature::Mask Mask )
{
	m_FeatureMask = Mask;
}
inline
CnShaderFeature::Mask
CnShaderTechnique::GetFeatureMask() const
{
	return m_FeatureMask;
}