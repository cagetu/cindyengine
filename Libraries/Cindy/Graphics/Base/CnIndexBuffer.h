//================================================================
// File:           : CnIndexBuffer.h
// Original Author : changhee
// Creation Date   : 2007. 1. 31
//================================================================
#ifndef __CN_INDEXBUFFER_H__
#define __CN_INDEXBUFFER_H__

#include "Foundation/CnObject.h"

namespace Cindy
{
	//================================================================
	/** IndexBuffer
	    @author    changhee
		@since     2007. 1. 31
		@remarks   �ε��� ���� Ŭ����
				   16bit Index�� ��~
	*/
	//================================================================
	class CnIndexBuffer : public CnObject
	{
		__DeclareRtti;
	public:
		enum USAGE
		{
			USAGE_WRITEONLY = 0x00000008L,
			USAGE_DYNAMIC = 0x00000200L,
		};

		enum POOL
		{
			POOL_DEFAULT	= 0,
			POOL_MANAGED	= 1,
			POOL_SYSTEMMEM	= 2,
			POOL_SCRATCH	= 3,
		};

		static const DWORD FMT_INDEX16 = 101;
		static const DWORD FMT_INDEX32 = 102;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		CnIndexBuffer();
		virtual ~CnIndexBuffer();

		// Create/Destroy
		virtual bool	Create( uint nNumIndices, ulong ulUsage = D3DUSAGE_WRITEONLY, ulong ulFormat = FMT_INDEX16, ulong ePool = 0 ) abstract;
		virtual void	Destroy() abstract;

		// Lock/Unlock
		virtual ushort*	Lock( uint nOffset, uint nCount, ulong ulFlags ) abstract;
		virtual bool	Unlock() abstract;

		// Get
		uint			GetNumIndices() const;
		uint			GetSizeOfIndex() const;
		uint			GetSizeOfIndices() const;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		unsigned int	m_nNumIndices;

	};

#include "CnIndexBuffer.inl"
}

#endif	// __CN_INDEXBUFFER_H__