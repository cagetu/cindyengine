//================================================================
// File:               : CnQuery.cpp
// Related Header File : CnQuery.h
// Original Author     : changhee
// Creation Date       : 2009. 2. 15
//================================================================
#include "Cindy.h"
#include "CnQuery.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnQuery, CnObject);

	/// Const/Dest
	CnQuery::CnQuery()
	{
	}
	CnQuery::~CnQuery()
	{
	}
}