//================================================================
// File:           : CnShape.h
// Original Author : changhee
// Creation Date   : 2009. 6. 30
//================================================================
#ifndef __CN_SHAPE_H__
#define __CN_SHAPE_H__

namespace Cindy
{
	//================================================================
	/** Shape Class
	    @author    changhee
		@brief	   �⺻ Shape ��ü
	*/
	//================================================================
	class CnShape
	{
	public:
		enum Type
		{
			Box,
			Sphere,
			//Cone,
			Cylinder,
			Torus,

			NumShapeTypes,
			InvalidShapeType,
		};

		CnShape();
		virtual ~CnShape();

		/// ShapeType
		void	SetType( Type eType );
		Type	GetType() const;

	protected:
		Type	m_Type;
	};

	//------------------------------------------------------------------
	/**
	*/
	inline
	void CnShape::SetType( CnShape::Type eType )
	{
		m_Type = eType;
	}

	//------------------------------------------------------------------
	/**
	*/
	inline
	CnShape::Type CnShape::GetType() const
	{
		return m_Type;
	}

}

#endif // __CN_SHAPE_H__