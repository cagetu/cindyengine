//================================================================
// File:               : CnVertexDeclaration.inl
// Related Header File : CnVertexDeclaration.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 12
//================================================================
namespace Cindy
{
	//----------------------------------------------------------------
	/**
	*/
	inline
	int CnVertexDeclaration::GetComponents() const
	{
		return m_VertexComponents;
	}

	//****************************************************************
	//	VertexDeclaration::UsageComponent
	//****************************************************************
	//----------------------------------------------------------------
	/**
	*/
	inline
	void CnVertexDeclaration::UsageComponent::Add( CnVertexDeclaration::USAGE eUsage )
	{
		value_ |= eUsage;
		count_++;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	void CnVertexDeclaration::UsageComponent::Remove( CnVertexDeclaration::USAGE eUsage )
	{
		value_ &= ~eUsage;
		if (--count_<0)
			count_ = 0;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	void CnVertexDeclaration::UsageComponent::Set( int Components )
	{
		value_ = Components & AllUsageComponents;
	}
	
	//----------------------------------------------------------------
	/**
	*/
	inline
	int CnVertexDeclaration::UsageComponent::Get() const
	{
		return value_;
	}
	
	//----------------------------------------------------------------
	/**
	*/
	inline
	bool CnVertexDeclaration::UsageComponent::Has( int nComponent )
	{
		return ( nComponent == (value_ & nComponent) );
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	bool CnVertexDeclaration::UsageComponent::Empty() const
	{
		return value_ == 0;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	int CnVertexDeclaration::UsageComponent::GetCount() const
	{
		return count_;
	}

	//----------------------------------------------------------------
	/**	@brief	component ������
	*/
	inline
	ushort CnVertexDeclaration::UsageComponent::GetSize() const
	{
		assert(0 < value_);

		ushort size = 0;

		if (value_ & Position)		size += 3;
		if (value_ & PositionT)		size += 4;

		if (value_ & Normal)		size += 3;	// float3
		if (value_ & NormalUB4N)	size += 1;	// ubyte4n

		if (value_ & Uv0)			size += 2;	// float2
		if (value_ & Uv0S2)			size += 1;	// short2
		if (value_ & Uv1)			size += 2;	// float2
		if (value_ & Uv1S2)			size += 1;	// short2
		if (value_ & Uv2)			size += 2;	// float2
		if (value_ & Uv2S2)			size += 1;	// short2
		if (value_ & Uv3)			size += 2;	// float2
		if (value_ & Uv3S2)			size += 1;	// short2

		if (value_ & Color)			size += 4;
		if (value_ & ColorUB4N)		size += 1;

#ifdef TANGENT_FLOAT4
		if (value_ & Tangent)		size += 4;	// float4
#else
		if (value_ & Tangent)		size += 3;	// float3
#endif
		if (value_ & TangentUB4N)	size += 1;	// ubyte4n
		if (value_ & Binormal)		size += 3;	// float3
		if (value_ & BinormalUB4N)	size += 1;	// ubyte4n

		if (value_ & BlendWeights)		size += 4;	// float4
		if (value_ & BlendWeightsUB4N)	size += 1;	// ubyte4n
		if (value_ & BlendIndices)		size += 4;	// float4
		if (value_ & BlendIndicesUB4)	size += 1;	// ubyte4

		return size;
	}
}
