//================================================================
// File:           : CnRenderTexture.h
// Original Author : changhee
// Creation Date   : 2007. 4. 19
//================================================================
#pragma once

#include "Graphics/CnRenderTarget.h"
#include "Material/CnTexture.h"

namespace Cindy
{
	class CnRenderWindow;
	class CnScene;
	class CnColor;

	//================================================================
	/** Render Texture
	    @author    changhee
		@since     2007. 4. 19
		@remarks   텍스쳐 랜더링 할 경우, 랜더링 될 텍스쳐 정보에 대한 클래스
	*/
	//================================================================
	class CN_DLL CnRenderTexture : public CnRenderTarget
	{
		__DeclareRtti;
	public:
		CnRenderTexture( const wchar* strName,
						 ushort usWidth,
						 ushort usHeight,
						 ushort usDepth,
						 PixelFormat::Code eFormat = PixelFormat::A8R8G8B8,
						 CnAntiAliasQuality::Type eAnitiQuality = CnAntiAliasQuality::None,
						 bool bEnableDepthStencil = true,
						 bool bGenerateMipMaps = false );
		virtual ~CnRenderTexture();

		// Setup
		virtual void	Setup();

		void			SetIndex( ushort Index );
		ushort			GetIndex() const;

		// texture
		void			SetTexture( const TexPtr& spTexture );
		const TexPtr&	GetTexture() const;

		// Depth
		ushort			GetDepth() const;

		// MipMap
		void			SetMipMapsEnabled( bool bEnable );
		bool			GetMipMapsEnabled() const;

		void			GenerateMipLevels();

		// fill
		virtual void	ColorFill( const CnColor& color ) abstract;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		ushort				m_Index;

		TexPtr				m_RenderToTexture;

		ushort				m_nDepth;
		PixelFormat::Code	m_Format;

		bool				m_bMipMapsEnabled;
	};

	//----------------------------------------------------------------
	/** Get Texture
	    @brief      랜더링할 텍스쳐 얻어오기
	*/
	inline
	const TexPtr& CnRenderTexture::GetTexture() const
	{
		return m_RenderToTexture;
	}
	
	//----------------------------------------------------------------
	/** 
	*/
	inline
	void CnRenderTexture::SetIndex( ushort Index )
	{
		m_Index = Index;
	}

	//----------------------------------------------------------------
	/** 
	*/
	inline
	ushort CnRenderTexture::GetIndex() const
	{
		return m_Index;
	}

	//----------------------------------------------------------------
	/** 
	*/
	inline
	ushort CnRenderTexture::GetDepth() const
	{
		return m_nDepth;
	}

	//----------------------------------------------------------------
	/** 
	*/
	inline
	void CnRenderTexture::SetMipMapsEnabled( bool bEnable )
	{
		m_bMipMapsEnabled = bEnable;
	}

	//----------------------------------------------------------------
	/** 
	*/
	inline
	bool CnRenderTexture::GetMipMapsEnabled() const
	{
		return m_bMipMapsEnabled;
	}
}