//================================================================
// File:           : CnQuery.h
// Original Author : changhee
// Creation Date   : 2009. 2. 15
//================================================================
#ifndef __CN_QUERY_H__
#define __CN_QUERY_H__

#include "Foundation/CnObject.h"

namespace Cindy
{
	//================================================================
	/** Query
	    @author		changhee
		@brief		directX query의 기반 클래스
		@see		ms-help://MS.VSCC.v80/MS.VSIPCC.v80/MS.DirectX9.1033.2007.August/DirectX_SDK/Queries.htm
	*/
	//================================================================
	class CnQuery : public CnObject
	{
		__DeclareRtti;
	public:
		// Async feedback
		enum QUERYTYPE
		{
			QUERYTYPE_VCACHE                 = 4, /* D3DISSUE_END */
			QUERYTYPE_RESOURCEMANAGER        = 5, /* D3DISSUE_END */
			QUERYTYPE_VERTEXSTATS            = 6, /* D3DISSUE_END */
			QUERYTYPE_EVENT                  = 8, /* D3DISSUE_END */
			QUERYTYPE_OCCLUSION              = 9, /* D3DISSUE_BEGIN, D3DISSUE_END */
			QUERYTYPE_TIMESTAMP              = 10, /* D3DISSUE_END */
			QUERYTYPE_TIMESTAMPDISJOINT      = 11, /* D3DISSUE_BEGIN, D3DISSUE_END */
			QUERYTYPE_TIMESTAMPFREQ          = 12, /* D3DISSUE_END */
			QUERYTYPE_PIPELINETIMINGS        = 13, /* D3DISSUE_BEGIN, D3DISSUE_END */
			QUERYTYPE_INTERFACETIMINGS       = 14, /* D3DISSUE_BEGIN, D3DISSUE_END */
			QUERYTYPE_VERTEXTIMINGS          = 15, /* D3DISSUE_BEGIN, D3DISSUE_END */
			QUERYTYPE_PIXELTIMINGS           = 16, /* D3DISSUE_BEGIN, D3DISSUE_END */
			QUERYTYPE_BANDWIDTHTIMINGS       = 17, /* D3DISSUE_BEGIN, D3DISSUE_END */
			QUERYTYPE_CACHEUTILIZATION       = 18, /* D3DISSUE_BEGIN, D3DISSUE_END */
		};

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnQuery();
		virtual ~CnQuery();

		// Create
		virtual bool	Create( QUERYTYPE QueyType ) abstract;

		// Begin/End
		virtual void	Begin() abstract;
		virtual void	End() abstract;

		// Get Data
		virtual void	GetData( void* pData, ulong ulSize ) abstract;

		// Device Lost/Restore
		virtual void	OnLost() abstract;
		virtual void	OnRestore() abstract;
	};
}

#endif	// __CN_QUERY_H__
