//================================================================
// File:               : CnShaderFeature.cpp
// Related Header File : CnShaderFeature.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 14
//================================================================
#include "CnShaderFeature.h"
#include "../../Util/CnString.h"

namespace Cindy
{
	//Const
	CnShaderFeature::CnShaderFeature()
		: m_UniqueId(0)
	{
		m_IndexToString.reserve( 32 );
	}

	//================================================================
	/** @brief	스트링을 Mask으로 변경 */
	//================================================================
	CnShaderFeature::Mask CnShaderFeature::StringToMask( const std::string& Features )
	{
		Mask mask = 0;
		std::string feature;
		stdext::hash_map<std::string, uint>::iterator siIter;

		StringVector tokens = CnStringUtil::Split( Features, "\t |" );
		unsigned int size = (uint)tokens.size();
		for (uint i=0; i<size; ++i)
		{
			feature = tokens[i];

			siIter = m_StringToIndex.find(feature);
			if (siIter != m_StringToIndex.end())
			{
				mask |= (1 << siIter->second);
			}
			else
			{
				uint bit = m_UniqueId++;
				m_StringToIndex.insert( stdext::hash_map<std::string, uint>::value_type( feature, bit ) );
				m_IndexToString.push_back( feature );

				mask |= (1 << bit);
			}
		}
		return mask;
	}

	//================================================================
	/** @brief	Mask를 스트링으로 변경 */
	//================================================================
	void CnShaderFeature::MaskToString( CnShaderFeature::Mask Features, std::string& Output )
	{
		uint size = m_UniqueId;
		for (uint bit=0; bit<size; ++bit)
		{
			if (0 != (Features &  (1<<bit)))
			{
				if (!Output.empty())
				{
					Output.append("|");
				}
				Output.append(m_IndexToString[bit]);
			}
		}
	}
}