//================================================================
// File:           : CnVertexBuffer.h
// Original Author : changhee
// Creation Date   : 2007. 1. 31
//================================================================
#ifndef __CN_VERTEXBUFFER_H__
#define __CN_VERTEXBUFFER_H__

#include "Foundation/CnObject.h"
//#define USE_FVF

namespace Cindy
{
	//================================================================
	/** VertexBuffer
	    @author    changhee
		@since     2007. 1. 31
		@remarks   버텍스 버퍼 클래스
	*/
	//================================================================
	class CnVertexBuffer : public CnObject
	{
		__DeclareRtti;
	public:
		enum USAGE
		{
			USAGE_WRITEONLY = 0x00000008L,
			USAGE_DYNAMIC = 0x00000200L,
		};

		enum POOL
		{
			POOL_DEFAULT	= 0,
			POOL_MANAGED	= 1,
			POOL_SYSTEMMEM	= 2,
			POOL_SCRATCH	= 3,
		};

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		CnVertexBuffer();
		virtual ~CnVertexBuffer();

		// Create/Destroy
		virtual bool			Create( uint nNumVertices, uint nSizeOfVertex, 
										ulong ulUsage = USAGE_WRITEONLY, ulong ulPool = 0,
										bool bUseFVF = false ) abstract;
		virtual void			Destroy() abstract;

		// Get Information
		virtual void			GetBuffer( void* pOutput ) abstract;
		uint					GetSizeOfVertex() const;

		// Lock/Unlock
		//virtual float*			Lock( uint nOffset, uint nCount, ulong ulFlags ) abstract;
		virtual void*			Lock( uint nOffset, uint nCount, ulong ulFlags ) abstract;
		virtual bool			Unlock() abstract;

		// To Renderer
		//virtual bool			SetFVF() abstract;
		//virtual bool			SetStreamSource( uint nStreamID = 0, uint nOffsetInBytes = 0 ) abstract;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		uint		m_nSizeOfVertex;			
	};

	//================================================================
	/** VertexBufferBinder
	    @author    changhee
		@since     2008. 8. 11
		@remarks   여러개의 버텍스 버퍼를 이용한 Stream 최적화를 위한 객체
	*/
	//================================================================
	class CnVertexBufferBinder : public CnObject
	{
	public:
		typedef std::map<ushort, Ptr<CnVertexBuffer> >	BufferMap;
		typedef BufferMap::iterator						BufferIter;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnVertexBufferBinder();
		~CnVertexBufferBinder();

		// Bind
		void	Bind( ushort SteramID, Ptr<CnVertexBuffer>& pBuffer );
		void	Unbind( ushort StreamID );
		void	Clear();

		// Get
		const Ptr<CnVertexBuffer>&	GetBuffer( ushort StreamID );
		const BufferMap&			GetBuffers() const;

	private:
		BufferMap	m_Buffers;
	};

#include "CnVertexBuffer.inl"
}

#endif	// __CN_VERTEXBUFFER_H__