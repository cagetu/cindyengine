//================================================================
// File:           : CnRenderTarget.h
// Original Author : changhee
// Creation Date   : 2008. 1. 25
//================================================================
#pragma once

#include "CnRenderTexture.h"
#include "Material/CnTexture.h"

namespace Cindy
{
	//================================================================
	/** Multiple Render Target
	    @author    changhee
		@since     2008. 1. 25
		@remarks   다중 멀티 타겟에 랜더링을 한다.
				   하나의 장면을 멀티 타겟에 랜더링한다는 것을 가정한다.
				   RenderTarget은 scene과 1:1 대응이지만,
				   MultiRenderTarget(MRT)는 scene과 n:1로 대응된다.
	*/
	//================================================================
	class CN_DLL CnMultiRenderTarget : public CnRenderTarget
	{
		__DeclareRtti;
	public:
		static const int NUM_MULTIPLE_RENDERTARGETS = 4;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnMultiRenderTarget( const wchar* strName );
		virtual ~CnMultiRenderTarget();

		// Surface
		//virtual bool		BindSurface( int nIndex, const TexPtr& spTexture );
		virtual bool		BindSurface( int nIndex, const Ptr<CnRenderTexture>& spTarget );
		virtual bool		BindSurface( int nIndex,
										 const wchar* strName,
										 ushort usWidth,
										 ushort usHeight,
										 ushort usDepth,
										 PixelFormat::Code eFormat = PixelFormat::A8R8G8B8,
										 bool enableDepthStencil = false,
										 bool enableMipMaps = false);

		virtual void		UnbindSurface( int nIndex );

	protected:
		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		//TexPtr		m_pSurface[NUM_MULTIPLE_RENDERTARGETS];
		Ptr<CnRenderTexture>	m_pSurface[NUM_MULTIPLE_RENDERTARGETS];
	};
}
