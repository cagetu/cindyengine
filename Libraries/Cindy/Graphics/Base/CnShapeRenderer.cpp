//================================================================
// File:               : CnShapeRenderer.cpp
// Related Header File : CnShapeRenderer.h
// Original Author     : changhee
// Creation Date       : 2009. 6. 29
//================================================================
#include "Cindy.h"
#include "CnShapeRenderer.h"
#include "Scene/CnCamera.h"

namespace Cindy
{
	//================================================================
	CN_IMPLEMENT_RTTI(Cindy, CnShapeRenderer, CnObject);
	// Constructor
	CnShapeRenderer::CnShapeRenderer()
		: m_IsOpen(false)
	{
	}
	// Destructor
	CnShapeRenderer::~CnShapeRenderer()
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnShapeRenderer::Open()
	{
		assert(!m_IsOpen);
		m_IsOpen = true;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnShapeRenderer::Close()
	{
		assert(m_IsOpen);
		m_IsOpen = false;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnShapeRenderer::DrawShape( CnCamera* pCamera,
									 const Math::Matrix44& ModelTransform,
									 CnShapeRenderer::ShapeType eShapeType,
									 const CnColor& Color )
	{
	}
} // namespace Cindy
