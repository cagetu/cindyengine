//================================================================
// File:           : CnShaderTechnique.h
// Original Author : changhee
// Creation Date   : 2008. 11. 14
//================================================================
#ifndef __CN_SHADER_TECHNIQUE_H__
#define __CN_SHADER_TECHNIQUE_H__

#include "../../Foundation/CnObject.h"
#include "CnShaderFeature.h"

namespace Cindy
{
	//================================================================
	/** Shader Technique
	    @author    changhee
		@remarks   쉐이더 테크닉 정보 - Nebula3 쉐이더 시스템 
	*/
	//================================================================
	class CnShaderTechnique : public CnObject
	{
		CN_DECLARE_RTTI;
	public:
		typedef std::string		Name;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnShaderTechnique();
		virtual ~CnShaderTechnique();

		// Get
		const Name&				GetName() const;
		uint					GetNumPasses() const;
		CnShaderFeature::Mask	GetFeatureMask() const;

		// Active
		virtual void			Active() abstract;
	protected:
		Name					m_Name;
		uint					m_NumPasses;
		CnShaderFeature::Mask	m_FeatureMask;

		void	SetName( const Name& name );
		void	SetNumPasses( uint nNumPasses );
		void	SetFeatureMask( CnShaderFeature::Mask Mask );
	};

	typedef Pointer<CnShaderTechnique>	ShaderTechPtr;

#include "CnShaderTechnique.inl"
} // namespace Cindy

#endif	// __CN_SHADER_TECHNIQUE_H__