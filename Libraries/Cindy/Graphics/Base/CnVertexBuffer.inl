//================================================================
// File:               : CnVertexBuffer.inl
// Related Header File : CnVertexBuffer.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================
//================================================================
/** Get Size Of Vertex
	@remarks      버텍스 사이즈 반환
	@param		  none
	@return		  uint : m_nSizeOfVertex
*/
//================================================================
inline
uint CnVertexBuffer::GetSizeOfVertex() const
{
	return m_nSizeOfVertex;
}
