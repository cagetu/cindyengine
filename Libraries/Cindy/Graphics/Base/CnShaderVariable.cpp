//================================================================
// File:               : CnShaderVariable.cpp
// Related Header File : CnShaderVariable.h
// Original Author     : changhee
// Creation Date       : 2008. 11. 14
//================================================================
#include "../../Cindy.h"
#include "../../Util/CnLog.h"
#include "CnShaderVariable.h"

namespace Cindy
{
	CN_IMPLEMENT_RTTI(Cindy, CnShaderVariable, CnObject);
	/// Const/Dest
	CnShaderVariable::CnShaderVariable()
		: m_Type(UnknownType)
		, m_NumArrayElements(0)
	{
	}
	CnShaderVariable::~CnShaderVariable()
	{
	}

	//================================================================
	/** @brief	Type을 스트링으로 변경 */
	//================================================================
	void CnShaderVariable::TypeToString( CnShaderVariable::Type eType, CnString& Output )
	{
		switch (eType)
		{
		case UnknownType:	Output = L"unknown";	break;
		case IntType:		Output = L"int";		break;
		case BoolType:		Output = L"bool";		break;
		case FloatType:		Output = L"float";		break;
		case VectorType:	Output = L"vector";		break;
		case MatrixType:	Output = L"matrix";		break;
		case TextureType:	Output = L"texture";	break;
		default:
			CNERROR(L"ShaderVariableBase::TypeToString(): invalid type code!");
			return;
		}
	}
}