//================================================================
// File:               : CnIndexBuffer.inl
// Related Header File : CnIndexBuffer.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================

//================================================================
/** Get Number of Indices
    @remarks      인덱스 개수
	@param        none
	@return       uint : m_nNumIndices
*/
//================================================================
inline
uint CnIndexBuffer::GetNumIndices() const
{
	return m_nNumIndices;
}

//================================================================
/** Get Size of Index
    @remarks      인덱스 당 사이즈
	@param        none
	@return       uint : 인덱스 사이즈
*/
//================================================================
inline
uint CnIndexBuffer::GetSizeOfIndex() const
{
	return sizeof(unsigned short);
}

//================================================================
/** Get Size of Indices
    @remarks      인덱스 버퍼 사이즈
	@param        none
	@return       uint : 인덱스 버퍼 사이즈
*/
//================================================================
inline
uint CnIndexBuffer::GetSizeOfIndices() const
{
	return GetSizeOfIndex() * GetNumIndices();
}