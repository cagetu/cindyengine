//================================================================
// File:           : CnGeometryChunk.h
// Original Author : changhee
// Creation Date   : 2009. 11. 31
//================================================================
#ifndef __CN_GEOMETRY_CHUNK_H__
#define __CN_GEOMETRY_CHUNK_H__

#include "Geometry/CnSubMesh.h"
#include "Graphics/CnRenderSorting.h"
#include "Material/CnShader.h"

namespace Cindy
{
	class CnCamera;

	//================================================================
	/** Render Chunk
		@author    changhee
		@since     2009. 11. 31
		@remarks   랜더링 객체 처리
	*/
	//================================================================
	class CnGeometryChunk : public CnObject
	{
		__DeclareClass(CnGeometryChunk);
	public:
		CnGeometryChunk();
		virtual ~CnGeometryChunk();

		void	Add( CnSubMesh* SubMesh );
		void	Clear();

		void	Draw( CnCamera* Camera );

		void	SetSortingMode( Sorting::Comparision Comparision );
		void	SetShaderFeature( ShaderFeature::Feature Feature );
		void	SetRenderState( const wchar* FileName );

	protected:
		typedef std::vector<CnSubMesh*>	MeshArray;
		typedef MeshArray::iterator		MeshIter;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		MeshArray				m_Meshes;

		Sorting::Comparision	m_Comparision;
		ShaderPtr				m_Shader;

		ShaderFeature::Feature	m_ShaderFeature;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		void	Sort();

		void	Begin();
		void	End();
	};

	//----------------------------------------------------------------
	/**
	*/
	inline void
	CnGeometryChunk::SetSortingMode( Sorting::Comparision Comparision )
	{
		m_Comparision = Comparision;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline void
	CnGeometryChunk::SetShaderFeature( ShaderFeature::Feature Feature )
	{
		m_ShaderFeature = Feature;
	}

} // namespace Cindy

#endif	// __CN_GEOMETRY_CHUNK_H__