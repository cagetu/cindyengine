// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
inline 
uint CnDx9DisplayMode::GetWidth() const
{
	return m_DisplayMode.Width;
}

//--------------------------------------------------------------
inline 
uint CnDx9DisplayMode::GetHeight() const
{
	return m_DisplayMode.Height;
}

//--------------------------------------------------------------
inline 
uint CnDx9DisplayMode::GetRefreshRate() const
{
	return m_DisplayMode.RefreshRate;
}

//--------------------------------------------------------------
inline 
D3DFORMAT CnDx9DisplayMode::GetFormat() const
{
	return m_DisplayMode.Format;
}
