//================================================================
// File:               : CnDx9RenderState.cpp
// Related Header File : CnDx9RenderState.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 29
//================================================================
inline
CnDx9RenderState::CnDx9RenderState()
{
	// Blank
}
inline
CnDx9RenderState::CnDx9RenderState( D3DRENDERSTATETYPE eState )
: m_d3dRenderState( eState )
{
}

inline
void CnDx9RenderState::SetState( D3DRENDERSTATETYPE eState )
{
	m_d3dRenderState = eState;
}

inline
D3DRENDERSTATETYPE CnDx9RenderState::GetState() const
{
	return m_d3dRenderState;
}