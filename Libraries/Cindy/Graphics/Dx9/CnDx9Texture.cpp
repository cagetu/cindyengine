//================================================================
// File:               : CnDx9Texture.cpp
// Related Header File : CnDx9Texture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 9
//================================================================
#include "Cindy.h"
#include "Resource/CnResourceManager.h"
#include "Util/CnLog.h"

#include "CnDx9Texture.h"
#include "CnDx9Renderer.h"
#include "CnDx9Types.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnDx9Texture, CnTexture );

	//----------------------------------------------------------------
	CnDx9Texture::CnDx9Texture( CnResourceManager* pParent )
		: CnTexture(pParent)
		, m_pBaseTexture(0)
		, m_pTexture(0)
		, m_pCubeTexture(0)
		, m_pVolumeTexture(0)
		, m_pRenderTargetSurface(0)
		, m_pDepthStencilSurface(0)
	{
	}
	CnDx9Texture::~CnDx9Texture()
	{
		Unload();
	}

	//----------------------------------------------------------------
	/** Load
	    @brief      텍스쳐 로딩
		@param      strFileName : 파일 이름
		@return     true/false : 성공 여부
	*/
	//----------------------------------------------------------------
	bool CnDx9Texture::Load( const wchar* strFileName )
	{
		bool bSuccess = false;

		if( IsRenderTarget() )
		{
			bSuccess = _CreateRenderTarget();
		}
		else if( GetUsage() & CreateFromDDSFile )
		{
			bSuccess = _Load( strFileName, false );
		}
		else if( GetUsage() & CreateEmpty )
		{

		}
		else
		{
			CnString ext = unicode::SplitPathExt( strFileName );
			if( ext == L"dds" )
			{
				bSuccess = _Load( strFileName, false );
			}
			else
			{
				bSuccess = _Load( strFileName, true );
			}
		}

		if( bSuccess )
		{
			m_strName = strFileName;
			SetState( Loaded );
		}

		return bSuccess;
	}

	//----------------------------------------------------------------
	/** Unload
	    @brief      텍스쳐 해제
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	bool CnDx9Texture::Unload()
	{
		if( !IsLoaded() )
			return false;

		SAFEREL( m_pRenderTargetSurface );
		SAFEREL( m_pDepthStencilSurface );

		SAFEREL( m_pBaseTexture );
		SAFEREL( m_pTexture );
		SAFEREL( m_pCubeTexture );
		SAFEREL( m_pVolumeTexture );

		SetState( Unloaded );
		return true;
	}

	//----------------------------------------------------------------
	/** Load
	    @brief      Format으로 변경
		@param        D3DFormat : 텍스쳐의 포맷
		@return       Fortmat
	*/
	//----------------------------------------------------------------
	bool CnDx9Texture::_Load( const CnString& strFileName, bool bGenerateMipMaps )
	{
		MoCommon::MoMemStream memStream;

		if (!m_pParent->FindData( strFileName, memStream ))
		{
			CnError( ToStr(L"file not found: %s", strFileName) );
			return false;
		}

		unsigned char* buffer = memStream.GetBuffer();
		ulong fileSize = memStream.GetSize();

		// check whether this is a 2d texture or a cube texture
		D3DXIMAGE_INFO d3dImageInfo = { 0 };
		HRESULT hr = D3DXGetImageInfoFromFileInMemory( buffer, fileSize, &d3dImageInfo );
		if( FAILED(hr) )
		{
			CnError( ToStr(L"Failed to obtain image info for file '%s'!", GetName().c_str()) );
			return false;
		}

		// Set Image Info
		m_usWidth = d3dImageInfo.Width;
		m_usHeight = d3dImageInfo.Height;
		m_usDepth = d3dImageInfo.Depth;
		m_usNumMipMaps = d3dImageInfo.MipLevels;
		D3DFormatToFormat( d3dImageInfo.Format );

		// check image file format
		if ( D3DXIFF_DDS == d3dImageInfo.ImageFileFormat )
		{
			m_ulColorKey = 0;
		}

		// Generate mipmaps?
		DWORD mipmapFilter = D3DX_FILTER_TRIANGLE | D3DX_FILTER_MIRROR;
		if( bGenerateMipMaps )
		{
			mipmapFilter = D3DX_DEFAULT;
		}

		// D3D usage flags
		DWORD d3dUsage = 0;
		D3DPOOL d3dPool = D3DPOOL_MANAGED;
		if ( m_usUsage & Dynamic )
		{
			d3dUsage = D3DUSAGE_DYNAMIC;
			d3dPool  = D3DPOOL_DEFAULT;
		}

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		// D3D Image Info
		switch( d3dImageInfo.ResourceType )
		{
		case D3DRTYPE_TEXTURE:
			{
				hr = D3DXCreateTextureFromFileInMemoryEx( device,							// Current Render Device
														  buffer,							// Texture Data 
														  fileSize,							// Data Size
														  D3DX_DEFAULT,						// Width
														  D3DX_DEFAULT,						// Height
														  D3DX_DEFAULT,						// Depth
														  d3dUsage,							// Usage
														  D3DFMT_UNKNOWN,					// Format
														  d3dPool,							// Pool
														  D3DX_FILTER_TRIANGLE | D3DX_FILTER_MIRROR,					// Filter
														  mipmapFilter,						// MipFilter
														  m_ulColorKey,						// ColorKey
														  0,								// pImageInfo
														  0,								// pPalette
														  &m_pTexture );					// Result
				if( FAILED( hr ) )
				{
					const TCHAR* tchar	= DXGetErrorString( hr );
					CnError( ToStr(L"[CnDx9Texture::Load] Failed 'D3DXCreateTextureFromFileInMemoryEx', %s ", tchar) );
					return false;
				}

				SetStyle( TEXTURE_2D );

				hr = m_pTexture->QueryInterface(IID_IDirect3DBaseTexture9, (void**) &(m_pBaseTexture));
				Assert( SUCCEEDED(hr), L"[CnDx9Texture::_Load] Failed 'QueryInterface(IID_IDirect3DBaseTexture9...' " );
			}
			break;

		case D3DRTYPE_VOLUMETEXTURE:
			{
				hr = D3DXCreateVolumeTextureFromFileInMemory( device,					// Device,
															  buffer,					// Texture Data,
															  fileSize,					// Data Size,
															  &m_pVolumeTexture );
				if (FAILED(hr))
				{
					const TCHAR* tchar	= DXGetErrorString( hr );
					CnError( ToStr(L"[CnDx9Texture::Load] Failed 'D3DXCreateCubeTextureFromFileInMemoryEx', %s", tchar) );
					return false;
				}

				SetStyle( TEXTURE_3D );

				hr = m_pVolumeTexture->QueryInterface(IID_IDirect3DBaseTexture9, (void**) &(m_pBaseTexture));
				Assert( SUCCEEDED(hr), L"[CnDx9Texture::_Load] Failed 'QueryInterface(IID_IDirect3DBaseTexture9...' " );
			}
			break;

		case D3DRTYPE_CUBETEXTURE:
			{
				hr = D3DXCreateCubeTextureFromFileInMemoryEx( device,						// Current Render Device
															  buffer,						// Texture Data 
															  fileSize,						// Data Size
															  D3DX_DEFAULT,					// Size
															  D3DX_DEFAULT,					// MipLevels
															  d3dUsage,						// Usage
															  D3DFMT_UNKNOWN,				// Format
															  d3dPool,						// Pool
															  D3DX_FILTER_NONE,				// Filter
															  mipmapFilter,					// MipFilter
															  m_ulColorKey,					// ColorKey
															  0,							// pImageInfo
															  0,							// pPalette
															  &m_pCubeTexture );			// Result
				if( FAILED( hr ) )
				{
					const TCHAR* tchar	= DXGetErrorString( hr );
					CnError( ToStr(L"[CnDx9Texture::Load] Failed 'D3DXCreateCubeTextureFromFileInMemoryEx', %s", tchar) );
					return false;
				}

				SetStyle( TEXTURE_CUBE );

				hr = m_pCubeTexture->QueryInterface(IID_IDirect3DBaseTexture9, (void**) &(m_pBaseTexture));
				Assert( SUCCEEDED(hr), L"[CnDx9Texture::_Load] Failed 'QueryInterface(IID_IDirect3DBaseTexture9...' " );
			}
			break;

		default:
			{
				CnPrint( L"[CnDx9Texture::Load()] Unsupport texture style '%s'!", GetName().c_str() );
				CnError( ToStr(L"Failed 'Unsupport texture style' ") );
				return false;
			}
			break;
		}

		return true;
	}

	//----------------------------------------------------------------
	/** Create Render Target
	    @brief      그려질 랜더링 타겟 텍스쳐 생성
		@return       true/false : 성공 여부
	*/
	//----------------------------------------------------------------
	bool CnDx9Texture::_CreateRenderTarget()
	{
		LPDIRECT3D9 d3d;
		RenderDevice->GetCOM( &d3d );

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		if (GetUsage() & RenderTargetColor)
		{
			D3DFORMAT colorFormat = CnDx9Types::ToD3DFormat(GetFormat());
			if (colorFormat == D3DFMT_UNKNOWN)
				return false;

			if (!_CheckRenderTargetFormat( d3d, device, D3DUSAGE_RENDERTARGET, colorFormat ))
			{
				CnError( L"CnDx9Texture::_CreateRenderTarget: Could not create render target surface" );
				return false;
			}

			// 
			DWORD d3dUsage = D3DUSAGE_RENDERTARGET;
			D3DPOOL d3dPool = D3DPOOL_DEFAULT;

			if (this->GetUsage() & GenerateMipMaps)
			{
				d3dUsage |= D3DUSAGE_AUTOGENMIPMAP;
			}

			HRESULT hr = device->CreateTexture( GetWidth(),		// Width
												GetHeight(),	// Height
												1,				// Levels
												d3dUsage,		// Usage
									 			colorFormat,	// Format
												d3dPool,		// Pool
												&m_pTexture,	// ppTexture
												NULL );			// pSharedHandle
			Assert( SUCCEEDED(hr), L"[CnDx9Texture::_CreateRenderTarget] Failed 'CreateTexture' " );

			// get base texture interface pointer
			hr = m_pTexture->QueryInterface(IID_IDirect3DBaseTexture9, (void**) &(m_pBaseTexture));
			Assert( SUCCEEDED(hr), L"[CnDx9Texture::_CreateRenderTarget] Failed 'QueryInterface(IID_IDirect3DBaseTexture9...' " );

			// get pointer to highest mipmap surface
			hr = m_pTexture->GetSurfaceLevel( 0, &(m_pRenderTargetSurface) );
			Assert( SUCCEEDED(hr), L"[CnDx9Texture::_CreateRenderTarget] Failed GetSurfaceLevel()");
		}

		// create optional render target depthStencil surface
		if (GetUsage() & RenderTargetDepthStencil)
		{
			// always create a 24 bit depth buffer
			//D3DFORMAT depthFormat = D3DFMT_D24X8;
			D3DFORMAT depthFormat = D3DFMT_D24S8;

			// make sure the format is compatible on this device
			if (!_CheckRenderTargetFormat( d3d, device, D3DUSAGE_DEPTHSTENCIL, depthFormat))
			{
				CnError( L"CnDx9Texture::_CreateRenderTarget: Could not create render target surface!" );
				return false;
			}

			// create render target surface
			HRESULT hr = device->CreateDepthStencilSurface( GetWidth(),
															GetHeight(),
															depthFormat,				// Format
															D3DMULTISAMPLE_NONE,        // MultiSampleType
															0,                          // MultiSampleQuality
															TRUE,                       // Discard
															&m_pDepthStencilSurface,
															NULL);
			Assert( SUCCEEDED(hr), L"[CnDx9Texture::_CreateRenderTarget] Failed 'CreateDepthStencilSurface' " );
		}
		
		return true;
	}

	//----------------------------------------------------------------
	bool CnDx9Texture::_CheckRenderTargetFormat( LPDIRECT3D9 pD3D,
												 LPDIRECT3DDEVICE9 pDevice, 
												 DWORD dwUsage,
												 D3DFORMAT ePixelFormat )
	{
		D3DDISPLAYMODE displaymode;
		pDevice->GetDisplayMode( 0, &displaymode );
		HRESULT hr = pD3D->CheckDeviceFormat( 0,
											  D3DDEVTYPE_HAL,
											  displaymode.Format,
											  dwUsage,
											  D3DRTYPE_SURFACE,
											  ePixelFormat );
		if (FAILED(hr))
			return false;

		return true;
	}

	//----------------------------------------------------------------
	/** D3D Format To Format
	    @brief      Format으로 변경
		@param        D3DFormat : 텍스쳐의 포맷
		@return       Fortmat
	*/
	//----------------------------------------------------------------
	PixelFormat::Code CnDx9Texture::D3DFormatToFormat( D3DFORMAT eFormat )
	{
        switch(eFormat)
        {
		case D3DFMT_R8G8B8:
		case D3DFMT_X8R8G8B8:       SetFormat(PixelFormat::X8R8G8B8); break;
		case D3DFMT_A8R8G8B8:       SetFormat(PixelFormat::A8R8G8B8); break;
		case D3DFMT_R5G6B5:
		case D3DFMT_X1R5G5B5:       SetFormat(PixelFormat::R5G6B5); break;
		case D3DFMT_A1R5G5B5:       SetFormat(PixelFormat::A1R5G5B5); break;
		case D3DFMT_A4R4G4B4:       SetFormat(PixelFormat::A4R4G4B4); break;
		case D3DFMT_L8:				SetFormat(PixelFormat::L8);	break;
		case D3DFMT_P8:				SetFormat(PixelFormat::P8);	break;
		case D3DFMT_DXT1:           SetFormat(PixelFormat::DXT1); break;
		case D3DFMT_DXT2:           SetFormat(PixelFormat::DXT2); break;
		case D3DFMT_DXT3:           SetFormat(PixelFormat::DXT3); break;
		case D3DFMT_DXT4:           SetFormat(PixelFormat::DXT4); break;
		case D3DFMT_DXT5:           SetFormat(PixelFormat::DXT5); break;
		case D3DFMT_R16F:           SetFormat(PixelFormat::R16F); break;
		case D3DFMT_G16R16F:        SetFormat(PixelFormat::G16R16F); break;
		case D3DFMT_A16B16G16R16F:  SetFormat(PixelFormat::A16B16G16R16F); break;
		case D3DFMT_R32F:           SetFormat(PixelFormat::R32F); break;
		case D3DFMT_G32R32F:        SetFormat(PixelFormat::G32R32F); break;
		case D3DFMT_A32B32G32R32F:  SetFormat(PixelFormat::A32B32G32R32F); break;
		default:
			CnError( L"[CnDx9Texture::D3DFormatToFormat] invalid pixel format!" );
			assert(0);
			break;
        }

		return GetFormat();
	}

	//----------------------------------------------------------------
	/** OnLost
	    @brief      윈도우 크기가 변경되는 등 디바이스가 리셋 되었을 때 처리
		@param		  none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9Texture::OnLost()
	{
		if( GetUsage() & Dynamic || IsRenderTarget() )
		{
			Unload();
			SetState( Lost );
		}
	}

	//----------------------------------------------------------------
	/** OnRestore
	    @brief      디바이스가 복원되었을 경우 처리
		@param		  none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9Texture::OnRestore()
	{
		if( GetUsage() & Dynamic || IsRenderTarget() )
		{
			SetState( Unloaded );
			Load( GetName().c_str() );
		} // if
	}

	//----------------------------------------------------------------
	/**
	*/
	//----------------------------------------------------------------
	void CnDx9Texture::GenerateMipLevels()
	{
		m_pBaseTexture->GenerateMipSubLevels();
	}

} // end of namespace
