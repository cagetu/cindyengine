//================================================================
// File:           : CnDx9RenderState.h
// Original Author : changhee
// Creation Date   : 2007. 1. 29
//================================================================
#ifndef __CN_DX9_RENDERSTATE_H__
#define __CN_DX9_RENDERSTATE_H__

namespace Cindy
{
	//==================================================================
	/** CnDx9RenderState Class
		@author			cagetu
		@since			2007년 1월 29일
		@remarks		DirectX 9 버전 랜더 스테이트
	*/
	//==================================================================
	class CnDx9RenderState
	{
	public:
		CnDx9RenderState();
		CnDx9RenderState( D3DRENDERSTATETYPE eState );

		void				SetState( D3DRENDERSTATETYPE eState );
		D3DRENDERSTATETYPE	GetState() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		D3DRENDERSTATETYPE		m_d3dRenderState;
	};

#include "CnDx9RenderState.inl"
} // end of namespace 

#endif	// __CN_DX9_RENDERSTATE_H__