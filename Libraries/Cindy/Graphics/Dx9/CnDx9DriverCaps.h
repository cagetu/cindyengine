// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CNDX9DRIVERCAPS_H__
#define __CNDX9DRIVERCAPS_H__

#include "Cindy.h"

namespace Cindy
{
	//==================================================================
	/** CnDx9DriverCaps
		@author
			cagetu
		@since
			2006�� 10�� 2��
		@remarks
			Class for save hardware capabilities		
	*/
	//==================================================================
	class CN_DLL CnDx9DriverCaps
	{
	public:
		/** enumerate hardware capabilities
		*/
		enum CAPABILITIES
		{
			CAPS_AUTOMIPMAP					= 0x00000002,		//!< Supporta generating mipmaps in hardware
			CAPS_BLENDING					= 0x00000004,
			CAPS_ANISOTROPY					= 0x00000008,		//!< Supports anisotropic texture filtering
			CAPS_DOT3						= 0x00000010,		//!< Supports fixed-function DOT3 texture blend
			CAPS_CUBEMAPPING				= 0x00000020,		//!< Supports cube mapping
			CAPS_HWSTENCIL					= 0x00000040,		//!< Supports hardware stencil buffer
			CAPS_VBO						= 0x00000080,		//!< Supports hardware vertex and index buffers
			CAPS_VERTEXBLENDING				= 0x00000100,		//!< Supports vertex blending
			CAPS_VERTEX_SHADER				= 0x00000200,		//!< Supports vertex programs (vertex shaders)
			CAPS_PIXEL_SHADER				= 0x00000400,		//!< Supports fragment programs (pixel shaders)
			CAPS_TEXTURE_COMPRESSION		= 0x00000800,		//!< Supports compressed textures
			CAPS_TEXTURE_COMPRESSION_DXT	= 0x00001000,		//!< Supports compressed textures in the DXT/ST3C formats
			CAPS_TEXTURE_COMPRESSION_VTC	= 0x00002000,		//!< Supports compressed textures in the VTC format
			CAPS_SCISSOR_TEST				= 0x00004000,		//!< Supports performing a scissor test to exclude areas of the screen
			CAPS_TWO_SIDED_STENCIL			= 0x00008000,		//!< Supports separate stencil updates for both front and back faces
			CAPS_STENCIL_WRAP				= 0x00010000,		//!< Supports wrapping the stencil value at the range extremeties
			CAPS_HWOCCLUSION				= 0x00020000,		//!< Supports hardware occlusion queries
			CAPS_USER_CLIP_PLANES			= 0x00040000,		//!< Supports user clipping planes
			CAPS_VERTEX_FORMAT_UBYTE4		= 0x00080000,		//!< Supports the VET_UBYTE4 vertex element type
			CAPS_INFINITE_FAR_PLANE			= 0x00100000,		//!< Supports infinite far plane projection
			CAPS_HWRENDER_TO_TEXTURE		= 0x00200000,		//!< Supports hardware render-to-texture (bigger than framebuffer)
			CAPS_TEXTURE_FLOAT				= 0x00400000,		//!< Supports float textures and render targets
			CAPS_NON_POWER_OF_2_TEXTURES	= 0x00800000,		//!< Supports non-power of two textures
			CAPS_TEXTURE_3D					= 0x01000000,		//!< Supports 3d (volume) textures
			CAPS_FULLSCREEN_GAMMA			= 0x02000000,		//!< Supports hardware render-to-texture (bigger than framebuffer)
			CAPS_MRTINDEPENDENTBITDEPTHS	= 0x08000000,		//!< Supports different bit depths for MRT
			CAPS_MRTPOSTPIXELSHADERBLENDING	= 0x10000000,		//!< Supports post-pixel shader operations for MRT
		};

		struct Version
		{
			ulong	major;
			ulong	minor;
		};
	public:
		CnDx9DriverCaps();

		/** capability
		*/
		void		SetCapability( CAPABILITIES eCaps );
		bool		HasCapability( CAPABILITIES eCaps ) const;

		void		SetMaxLight( ushort usCount );
		ushort		GetMaxLight() const;

		void		SetMaxTexture( ushort usValue );
		ushort		GetMaxTexture() const;

		void		SetStencilBufferBitDepth( ushort usDepth );
		ushort		GetStencilBufferBitDepth() const;

		void		SetMaxMRTs( ushort usNum );
		ushort		GetMaxMRTs() const;

		void		SetVertexShaderVersion( DWORD dwVersion );
		void		SetVertexShaderVersion( const CnString& strVersion );
		void		GetVertexShaderVersion( Version& rVersion ) const;
		void		GetVertexShaderVersion( CnString& rVersion ) const;

		void		SetPixelShaderVersion( DWORD dwVersion );
		void		SetPixelShaderVersion( const CnString& strVersion );
		void		GetPixelShaderVersion( Version& rVersion ) const;
		void		GetPixelShaderVersion( CnString& rVersion ) const;

		void		SetVertexShaderFloatRegisterCount( ushort usNum );
		void		SetVertexShaderBoolRegisterCount( ushort usNum );
		void		SetVertexShaderIntRegisterCount( ushort usNum );
		ushort		GetVertexShaderFloatRegisterCount() const;
		ushort		GetVertexShaderBoolRegisterCount() const;
		ushort		GetVertexShaderIntRegisterCount() const;

		void		SetPixelShaderFloatRegisterCount( ushort usNum );
		void		SetPixelShaderBoolRegisterCount( ushort usNum );
		void		SetPixelShaderIntRegisterCount( ushort usNum );
		ushort		GetPixelShaderFloatRegisterCount() const;
		ushort		GetPixelShaderBoolRegisterCount() const;
		ushort		GetPixelShaderIntRegisterCount() const;

	private:
		ulong		m_ulCapabilities;					//!< save Capabilitis
		
		Version		m_VertexShaderVersion;				//!< Verstex Shader 
		Version		m_PixelShaderVersion;				//!< Pixel Shader

		CnString	m_strVertexShaderVersion;			//!< Vertex Shader version
		CnString	m_strPixelShaderVersion;			//!< pixel shader version

		ushort		m_usVertexShaderFloatRegCount;		//!< float type register number for vertex shader
		ushort		m_usVertexShaderBoolRegCount;		//!< bool type register number for vertex shader
		ushort		m_usVertexShaderIntRegCount;		//!< int type register number for vertex shader

		ushort		m_usPixelShaderFloatRegCount;		//!< float type register number for pixel shader
		ushort		m_usPixelShaderBoolRegCount;		//!< bool type register number for pixel shader
		ushort		m_usPixelShaderIntRegCount;			//!< int type register number for pixel shader

		ushort		m_usMaxLights;						//!< usable max light 
		ushort		m_usMaxTextures;					//!< usable texture number
		ushort		m_usMaxWorldMatrices;				//!< Usable world matrices number
		ushort		m_usStencilBufferBits;				//!< usable stencil buffer bit
		ushort		m_usMaxVertexBlendMatrices;			//!< usable Vertex blending number

		ushort		m_usMaxMRTs;						//!< usable number of MRTs

	};

#include "CnDx9DriverCaps.inl"
}

#endif	// __CNDX9DRIVERCAPS_H__