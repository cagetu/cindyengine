//================================================================
// File:               : CnDx9VertexDeclaration.cpp
// Related Header File : CnDx9VertexDeclaration.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 6
//================================================================
#include "Cindy.h"
#include "Util/CnLog.h"
#include "Graphics/CnRenderer.h"
#include "CnDx9VertexDeclaration.h"
#include "CnDx9Renderer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnDx9VertexDeclaration, CnVertexDeclaration );

	/// Const/Dest
	CnDx9VertexDeclaration::CnDx9VertexDeclaration()
		: m_pDeclaration(0)
		, m_bNeedRebuild(true)
	{
	}
	CnDx9VertexDeclaration::~CnDx9VertexDeclaration()
	{
		SAFEREL( m_pDeclaration );
	}
	//================================================================
	/** Add VertexElement
	*/
	//================================================================
	const CnVertexDeclaration::VertexElement&
	CnDx9VertexDeclaration::AddElement( ushort Index, ushort Source, const CnVertexDeclaration::UsageComponent& Components )
	{
		m_bNeedRebuild = true;

		return CnVertexDeclaration::AddElement( Index, Source, Components );
	}

	//================================================================
	/** Remove VertexElement
	*/
	//================================================================
	void CnDx9VertexDeclaration::RemoveElement( ushort Index )
	{
		m_bNeedRebuild = true;

		CnVertexDeclaration::RemoveElement( Index );
	}
	void CnDx9VertexDeclaration::RemoveAllElements()
	{
		m_bNeedRebuild = true;

		CnVertexDeclaration::RemoveAllElements();
	}

	//================================================================
	/** Create Vertex Declaration
	    @remarks      버텍스 Declaration을 생성한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnDx9VertexDeclaration::Build()
	{
#ifdef _DEBUG
		Assert( !m_pDeclaration, L"[CnDx9VertexDeclaration::CreateVertexDeclaration] Is exist Created Declaration" );
#else
		if( m_pDeclaration )
		{
			CnPrint( L"[CnDx9VertexDeclaration::CreateVertexDeclaration] Is exist Created Declaration" );
			return;
		}
#endif
		if (m_bNeedRebuild)
		{
			SAFEREL( m_pDeclaration );

			m_VertexComponents = 0;

			int numElements = 0;
			VertexElement* vertElement = 0;
			VertexElementIter iEnd = this->m_VertexElements.end();
			for (VertexElementIter iWhere = m_VertexElements.begin(); iWhere != iEnd; ++iWhere)
			{
				vertElement = &(iWhere->second);
				numElements += vertElement->usageComponent.GetCount();
				m_VertexComponents |= vertElement->usageComponent.Get();
			}

			D3DVERTEXELEMENT9* decl = new D3DVERTEXELEMENT9[numElements + 1];

			int currentOffset = 0;
			int currentElement = 0;
			int postionUsageIndex = 0;
			int normalUsageIndex = 0;
			int tangentUsageIndex = 0;
			int binormalUsageIndex = 0;
			int colorUsageIndex = 0;
			int weightsUsageIndex = 0;
			int jindciesUsageIndex = 0;

			CnPrint( L"[CnDx9VertexDeclaration::CreateVertexDeclaration] Vertex Declaration" );

			for (VertexElementIter iWhere = m_VertexElements.begin(); iWhere != iEnd; ++iWhere)
			{
				vertElement = &iWhere->second;

				currentOffset = 0;

				for( int i = 0; i < NumVertexUsages; ++i )
				{
					if (currentElement == numElements)
						break;

					int mask = (1<<i);

					if (vertElement->usageComponent.Has( mask ))
					{
						decl[currentElement].Stream = vertElement->index;
						decl[currentElement].Offset = currentOffset;
						decl[currentElement].Method = D3DDECLMETHOD_DEFAULT;

						switch (mask)
						{
						case Position:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT3;
							decl[currentElement].Usage = D3DDECLUSAGE_POSITION;
							decl[currentElement].UsageIndex = postionUsageIndex++;
							currentOffset += sizeof(float) * 3;
							CnPrint( L"[Position: float3]" );
							break;

						case PositionT:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT4;
							decl[currentElement].Usage = D3DDECLUSAGE_POSITIONT;
							decl[currentElement].UsageIndex = 0;
							currentOffset += sizeof(float) * 4;
							CnPrint( L"[PositionT: float4]" );
							break;

						// Normal 정보
						case Normal:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT3;
							decl[currentElement].Usage = D3DDECLUSAGE_NORMAL;
							decl[currentElement].UsageIndex = normalUsageIndex++;
							currentOffset += sizeof(float) * 3;
							CnPrint( L"[Normal: float3]" );
							break;
						case NormalUB4N:	// float3->ubyte4로 압축
							decl[currentElement].Type = D3DDECLTYPE_UBYTE4N;
							decl[currentElement].Usage = D3DDECLUSAGE_NORMAL;
							decl[currentElement].UsageIndex = normalUsageIndex++;
							currentOffset += sizeof(unsigned char) * 4; // ubyte4
							CnPrint( L"[Normal: ubyte4n]" );
							break;

						// uv 정보
						case Uv0:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 0;
							currentOffset += sizeof(float) * 2;
							CnPrint( L"[Uv0: float2]" );
							break;
						case Uv0S2:
							decl[currentElement].Type = D3DDECLTYPE_SHORT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 0;
							currentOffset += sizeof(short)*2;
							CnPrint( L"[Uv0: short2]" );
							break;

						case Uv1:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 1;
							currentOffset += sizeof(float) * 2;
							CnPrint( L"[Uv1: float2]" );
							break;
						case Uv1S2:
							decl[currentElement].Type = D3DDECLTYPE_SHORT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 1;
							currentOffset += sizeof(short)*2;
							CnPrint( L"[Uv1: short2]" );
							break;

						case Uv2:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 2;
							currentOffset += sizeof(float) * 2;
							CnPrint( L"[Uv2: float2]" );
							break;
						case Uv2S2:
							decl[currentElement].Type = D3DDECLTYPE_SHORT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 2;
							currentOffset += sizeof(short) * 2;
							CnPrint( L"[Uv2: short2]" );
							break;

						case Uv3:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 3;
							currentOffset += sizeof(float) * 2;
							CnPrint( L"[Uv3: float2]" );
							break;
						case Uv3S2:
							decl[currentElement].Type = D3DDECLTYPE_SHORT2;
							decl[currentElement].Usage = D3DDECLUSAGE_TEXCOORD;
							decl[currentElement].UsageIndex = 3;
							currentOffset += sizeof(short) * 2;
							CnPrint( L"[Uv3: short2]" );
							break;

						// 버텍스 칼라 정보
						case Color:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT4;		// D3DDECLTYPE_D3DCOLOR
							decl[currentElement].Usage = D3DDECLUSAGE_COLOR;
							decl[currentElement].UsageIndex = colorUsageIndex++;
							currentOffset += sizeof(float) * 4;
							CnPrint( L"[Color: float4]" );
							break;
						case ColorUB4N:
							assert(0);
							break;

						// Tangent/Binormal 정보
						case Tangent:
#ifdef TANGENT_FLOAT4
							decl[currentElement].Type = D3DDECLTYPE_FLOAT4;
							decl[currentElement].Usage = D3DDECLUSAGE_TANGENT;
							decl[currentElement].UsageIndex = tangentUsageIndex++;
							currentOffset += sizeof(float) * 4;
							CnPrint( L"[Tangent: float4]" );
#else
							decl[currentElement].Type = D3DDECLTYPE_FLOAT3;
							decl[currentElement].Usage = D3DDECLUSAGE_TANGENT;
							decl[currentElement].UsageIndex = tangentUsageIndex++;
							currentOffset += sizeof(float) * 3;
							CnPrint( L"[Tangent: float3]" );
#endif
							break;
						case TangentUB4N:
							decl[currentElement].Type = D3DDECLTYPE_UBYTE4N;
							decl[currentElement].Usage = D3DDECLUSAGE_TANGENT;
							decl[currentElement].UsageIndex = tangentUsageIndex++;
							currentOffset += sizeof(unsigned char) * 4; // ubyte4
							CnPrint( L"[Tangent: ubyte4n]" );
							break;

						case Binormal:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT3;
							decl[currentElement].Usage = D3DDECLUSAGE_BINORMAL;
							decl[currentElement].UsageIndex = binormalUsageIndex++;
							currentOffset += sizeof(float) * 3;
							CnPrint( L"[Binormal: float3]" );
							break;
						case BinormalUB4N:
							decl[currentElement].Type = D3DDECLTYPE_UBYTE4N;
							decl[currentElement].Usage = D3DDECLUSAGE_BINORMAL;
							decl[currentElement].UsageIndex = binormalUsageIndex++;
							currentOffset += sizeof(unsigned char) * 4; // ubyte4
							CnPrint( L"[Binormal: ubyte4n]" );
							break;

						// Skin 정보
						case BlendWeights:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT4;
							decl[currentElement].Usage = D3DDECLUSAGE_BLENDWEIGHT;
							decl[currentElement].UsageIndex = weightsUsageIndex++;
							currentOffset += sizeof(float) * 4;
							CnPrint( L"[BlendWeights: float4]" );
							break;
						case BlendWeightsUB4N:
							decl[currentElement].Type = D3DDECLTYPE_UBYTE4N;
							decl[currentElement].Usage = D3DDECLUSAGE_BLENDWEIGHT;
							decl[currentElement].UsageIndex = weightsUsageIndex++;
							currentOffset += sizeof(unsigned char) * 4;
							CnPrint( L"[BlendWeights: ubyte4n]" );
							break;

						case BlendIndices:
							decl[currentElement].Type = D3DDECLTYPE_FLOAT4;
							decl[currentElement].Usage = D3DDECLUSAGE_BLENDINDICES;
							decl[currentElement].UsageIndex = jindciesUsageIndex++;
							currentOffset += sizeof(float) * 4;
							CnPrint( L"[BlendIndices: float4]" );
							break;
						case BlendIndicesUB4:
							decl[currentElement].Type = D3DDECLTYPE_UBYTE4;
							decl[currentElement].Usage = D3DDECLUSAGE_BLENDINDICES;
							decl[currentElement].UsageIndex = jindciesUsageIndex++;
							currentOffset += sizeof(unsigned char) * 4;
							CnPrint( L"[BlendIndices: ubyte4]" );
							break;

						default:
							CnPrint( L"[CnDx9VertexDeclaration::CreateVertexDeclaration] Invalid Component" );
							break;
						} // switch

						++currentElement;
					} // if( mask )
				} // if
			}

			decl[currentElement].Stream = 0xff;
			decl[currentElement].Offset = 0;
			decl[currentElement].Type = D3DDECLTYPE_UNUSED;
			decl[currentElement].Method = 0;
			decl[currentElement].Usage = 0;
			decl[currentElement].UsageIndex = 0;

			LPDIRECT3DDEVICE9 device;
			RenderDevice->GetCurrentDevice( &device );
			HRESULT hr = device->CreateVertexDeclaration( decl, &m_pDeclaration );
			Assert( SUCCEEDED( hr ), L"[CnDx9VertexDeclaration::CreateVertexDeclaration] Failed 'CreateVertexDeclaration' " );

			delete [] decl;

			m_bNeedRebuild = false;
		}
	}

	//================================================================
	/** Get FVF
		@remarks      버텍스 포멧 구하기
		@param		  none
		@return		  none
	*/
	//================================================================
	ulong CnDx9VertexDeclaration::GetFVF()
	{
		ulong result = 0;

		VertexElement* vertElement = 0;
		VertexElementIter iEnd = this->m_VertexElements.end();
		for (VertexElementIter iWhere = m_VertexElements.begin(); iWhere != iEnd; ++iWhere)
		{
			vertElement = &iWhere->second;

			for( uint i = 0; i < NumVertexUsages; ++i )
			{
				int mask = (1<<i);
				if (vertElement->usageComponent.Has( mask ))
				{
					switch( mask )
					{
					case CnVertexDeclaration::Position:
						result |= D3DFVF_XYZ;
						break;
					case CnVertexDeclaration::PositionT:
						result |= D3DFVF_XYZRHW;
						break;

					case CnVertexDeclaration::Normal:
						result |= D3DFVF_NORMAL;
						break;

					case CnVertexDeclaration::Uv0:
						result |= D3DFVF_TEX0;
						break;

					case CnVertexDeclaration::Uv1:
						result |= D3DFVF_TEX1;
						break;

					case CnVertexDeclaration::Uv2:
						result |= D3DFVF_TEX2;
						break;

					case CnVertexDeclaration::Uv3:
						result |= D3DFVF_TEX3;
						break;

					case CnVertexDeclaration::Color:
						result |= D3DFVF_DIFFUSE;
						break;

					case CnVertexDeclaration::Tangent:
						break;
					case CnVertexDeclaration::Binormal:
						break;

					case CnVertexDeclaration::BlendWeights:
						break;
					case CnVertexDeclaration::BlendIndices:
						break;
					} // switch
				} // if
			} // for
		}

		return result;
	}
}
