// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnDx9DisplayMode.h"

#include <MoCommon/MoStringUtil.h>

namespace Cindy
{
	//--------------------------------------------------------------
	CnDx9DisplayMode::CnDx9DisplayMode( D3DDISPLAYMODE d3ddm )
	{
		m_DisplayMode = d3ddm;
	}

	//--------------------------------------------------------------
	CnDx9DisplayMode::~CnDx9DisplayMode()
	{
	}

	//--------------------------------------------------------------
	uint CnDx9DisplayMode::GetColorDepth() const
	{
		uint colorDepth = 16;
		if( m_DisplayMode.Format == D3DFMT_X8R8G8B8 ||
			m_DisplayMode.Format == D3DFMT_A8R8G8B8 ||
			m_DisplayMode.Format == D3DFMT_R8G8B8 )
			colorDepth = 32;

		return colorDepth;
	}

	//--------------------------------------------------------------
	void CnDx9DisplayMode::GetDescription( CnString& rDesc ) const
	{
		static wchar buffer[256];

		MoStringUtil::ToString( buffer, L"%d x %d x %d(bit)", GetWidth(), GetHeight(), GetColorDepth() );
		rDesc = buffer;
	}
}