//================================================================
// File:           : CnDx9VertexBuffer.h
// Original Author : changhee
// Creation Date   : 2007. 1. 31
//================================================================
#ifndef __CN_DX9VERTEXBUFFER_H__
#define __CN_DX9VERTEXBUFFER_H__

#include "CnDx9Renderer.h"
#include "Graphics/Base/CnVertexBuffer.h"

namespace Cindy
{
	//================================================================
	/** Dx9 VertexBuffer
	    @author    changhee
		@since     2007. 1. 31
		@remarks   Direct9 버전 버텍스 버퍼 객체
	*/
	//================================================================
	class CnDx9VertexBuffer : public CnVertexBuffer
	{
		__DeclareRtti;
	private:
		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		LPDIRECT3DVERTEXBUFFER9		m_pBuffer;

		ulong						m_ulFVF;

	public:
		CnDx9VertexBuffer();
		virtual ~CnDx9VertexBuffer();

		// Create/Destroy
		bool		Create( uint nNumVertices, uint nSizeOfVertex, 
							ulong ulUsage = USAGE_WRITEONLY, ulong ulPool = 0,
							bool bUseFVF = false ) override;

		void		Destroy() override;

		// Buffer
		void		GetBuffer( void* pOutput ) override;

		// Lock/Unlock
		//float*		Lock( uint nOffset, uint nCount, ulong ulFlags ) override;
		void*		Lock( uint nOffset, uint nCount, ulong ulFlags ) override;
		bool		Unlock() override;

		// To Renderer
		//bool		SetFVF() override;
		//bool		SetStreamSource( uint nStreamID = 0, uint nOffsetInBytes = 0 ) override;
	};
#include "CnDx9VertexBuffer.inl"
}

#endif	// __CN_DX9VERTEXBUFFER_H__