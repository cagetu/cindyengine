//================================================================
// File:           : CnDx9RenderWindow.h
// Original Author : changhee
// Creation Date   : 2007. 4. 19
//================================================================
#pragma once

#include "../Base/CnRenderWindow.h"

namespace Cindy
{
	class CnDx9Driver;
	class CnViewport;
	class CnDx9Renderer;

	//================================================================
	/** Directx9 Render Window
	    @author    changhee
		@since     2007. 4. 19
		@remarks   Dx9 디바이스를 이용해서, 랜더링 될 윈도우
	*/
	//================================================================
	class CnDx9RenderWindow : public CnRenderWindow
	{
		__DeclareRtti;
	public:
		virtual ~CnDx9RenderWindow();

		// D3D Informations...
		//const D3DPRESENT_PARAMETERS&		GetD3DPresent() const;
		D3DPRESENT_PARAMETERS*		GetD3DPresent();
		LPDIRECT3DDEVICE9			GetD3DDevice() const;

		// Restore/Release
		void						OnRelease() override;
		void						OnRestore() override;
		void						OnReset() override;

		// Resize
		void						Resize( int nWidth, int nHeight ) override;

		// Update
		bool						Update() override;

		//
		void						Begin() override;
		void						Draw() override;
		void						End() override;

	private:
		friend class CnDx9Renderer;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnDx9Renderer*			m_pRenderer;
		
		LPDIRECT3DDEVICE9		m_pDevice;
		LPDIRECT3DSURFACE9		m_pRenderSurface;
		LPDIRECT3DSURFACE9		m_pRenderZBuffer;
		D3DPRESENT_PARAMETERS	m_d3dPP;

		bool					m_bLostDevice;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		bool		_CreateDevice( CnDx9Driver* pDriver, bool bThreadSafe );
		bool		_RestoreLostDevice() override;

		void		_InitPresentParameter( CnDx9Driver* pDriver );

		bool		_Create( CnDx9Driver* pDriver, ushort usColorDepth, ushort usRefreshRate, bool bFullScreen, bool bThreadSafe );
		void		_Destroy();

		long		_SwapBuffer();
		void		_ClearBuffer( ulong ulFlags, ulong ulColor, float fDepth, ushort usStencil );

		CnDx9RenderWindow( HWND hWnd, const wchar* strName, int nWidth, int nHeight, CnDx9Renderer* pRenderer );

		//------------------------------------------------------
		// Prevent Copy..
		//------------------------------------------------------
//		CnDx9RenderWindow( const CnDx9RenderWindow& )				{}
//		CnDx9RenderWindow& operator =( const CnDx9RenderWindow& )	{}
	};

#include "CnDx9RenderWindow.inl"
}