//================================================================
// File:               : CnDx9TextRenderer.cpp
// Related Header File : CnDx9TextRenderer.h
// Original Author     : changhee
// Creation Date       : 2009. 9. 3
//================================================================
#include "Cindy.h"
#include "CnDx9TextRenderer.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/Base/CnRenderWindow.h"
#include "Scene/CnCamera.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnDx9TextRenderer, CnTextRenderer);
	/// Constructor
	CnDx9TextRenderer::CnDx9TextRenderer()
		: m_D3DFont(0)
		, m_D3DSprite(0)
	{
	}
	/// Destructor
	CnDx9TextRenderer::~CnDx9TextRenderer()
	{
		if (m_Opened)
		{
			Close();
		}
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnDx9TextRenderer::Open()
	{
		assert(!m_Opened);
		assert(0 == m_D3DFont);
		assert(0 == m_D3DSprite);

		CnTextRenderer::Open();

		// setup D3DX font object
		HRESULT hr;
		IDirect3DDevice9* d3d9Dev;
		RenderDevice->GetCurrentDevice(&d3d9Dev);
		assert(0 != d3d9Dev);
		hr = D3DXCreateFont(d3d9Dev,                    // pDevice
							18,                         // Height
							0,                          // Width
							FW_BOLD,                    // Weight
							0,                          // MipLevels
							FALSE,                      // Italic
							DEFAULT_CHARSET,            // CharSet
							OUT_DEFAULT_PRECIS,         // OutputPrecision
							NONANTIALIASED_QUALITY,     // Quality
							DEFAULT_PITCH|FF_DONTCARE,  // PitchAndFamily
							_T("Arial"),                // pFaceName
							&m_D3DFont);
		assert(SUCCEEDED(hr));
		m_D3DFont->PreloadGlyphs(32, 255);

		// create sprite object for batched rendering
		hr = D3DXCreateSprite(d3d9Dev, &m_D3DSprite);
		assert(SUCCEEDED(hr));
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnDx9TextRenderer::Close()
	{
		assert(m_Opened);
		assert(0 != m_D3DFont);
		assert(0 != m_D3DSprite);

		// release d3d resources
		m_D3DFont->Release();
		m_D3DFont = 0;
		m_D3DSprite->Release();
		m_D3DSprite = 0;

		CnTextRenderer::Close();
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnDx9TextRenderer::DrawText( CnCamera* camera )
	{
		if (m_TextElements.empty())
			return;

		int width = RenderDevice->GetCurrentRenderWindow()->GetWidth();
		int height = RenderDevice->GetCurrentRenderWindow()->GetHeight();

		m_D3DSprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE);

		Math::Vector3 pos;

		TextElementArray::iterator iend = m_TextElements.end();
		for (TextElementArray::iterator i = m_TextElements.begin(); i != iend; i++)
		{
			pos = (*i).GetPosition();

			// 3D 상의 위치 처리하기...
			if ((*i).IsUsed3DPos())
			{
				// calculate screen space position
				const Math::Matrix44 viewProj = camera->GetView() * camera->GetProjection();
				Math::Vector3 screenPos;
				screenPos.Multiply( pos, viewProj );

				pos.x = (screenPos.x + 1.0f) * 0.5f;
				pos.y = 1.0f - ((screenPos.y + 1.0f) * 0.5f);
			}

			RECT rect;
			rect.left = LONG( pos.x * float(width) );
			rect.top = LONG( pos.y * float(height) );
			rect.bottom = rect.top;
			rect.right = rect.left;

			m_D3DFont->DrawText( m_D3DSprite,						// Sprite
								 (*i).GetText().c_str(),			// Text
								 -1,								// Count(number of characters)
								 &rect,								// Rect
								 DT_LEFT|DT_TOP|DT_NOCLIP,          // Format
								 (*i).GetColor().GetRGBA() );		// Color
		}

		m_D3DSprite->End();

		m_TextElements.clear();
	}

} // namespace Cindy