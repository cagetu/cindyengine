//================================================================
// File:               : CnDx9RenderTarget.cpp
// Related Header File : CnDx9RenderTarget.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 23
//================================================================
#include "../../Cindy.h"
#include "CnDx9RenderTarget.h"
#include "CnDx9Renderer.h"
#include "CnDx9Driver.h"

#include "../../Scene/CnWindow.h"
#include "../../System/CnLog.h"

namespace Cindy
{
//#define _PROFILE_NVPERFHUD
//#define _DEBUG_SHADER

	//----------------------------------------------------------------------------
	CnDx9RenderTarget::CnDx9RenderTarget( CnWindow* pWindow, CnDx9Renderer* pParent )
		: m_pWindow( pWindow )
		, m_pRenderer( pParent )
		, m_usColorDepth(0)
		, m_usRefreshRate(0)
		, m_bFullScreen(false)
		, m_bIsActive(false)
	{
	}

	//----------------------------------------------------------------------------
	CnDx9RenderTarget::~CnDx9RenderTarget()
	{
		SAFEDEL( m_pWindow );

		_Destroy();
	}

	//----------------------------------------------------------------------------
	void CnDx9RenderTarget::_Destroy()
	{
		Release();

		SAFEREL( m_pDevice );

//		m_pRenderSurface = 0;
//		m_pRenderZBuffer = 0;
	}

	//----------------------------------------------------------------------------
	bool CnDx9RenderTarget::_Create( CnDx9Driver* pDriver, ushort usColorDepth, ushort usRefreshRate,
									bool bFullScreen, bool bThreadSafe )
	{
		m_usRefreshRate = usRefreshRate;
		m_bFullScreen = bFullScreen;

		if( m_bFullScreen )
		{
			m_usColorDepth = usColorDepth;
		}
		else
		{
			HDC hDC = GetDC( m_pWindow->GetHWnd() );
			m_usColorDepth = GetDeviceCaps( hDC, BITSPIXEL );
			ReleaseDC( m_pWindow->GetHWnd(), hDC );
		}

		if( !_CreateDevice( pDriver, bThreadSafe ) )
		{
			return false;
		}

		m_bIsActive = true;

		return true;
	}

	//----------------------------------------------------------------------------
	bool CnDx9RenderTarget::_CreateDevice( CnDx9Driver* pDriver, bool bThreadSafe )
	{
		if( !pDriver )
		{
			CNERROR( L"[CnDx9RenderTarget::_CreateDevice] Can't find Dx9Driver" );
			return false;
		}

		_InitPresentParameter( pDriver );

		//******************************************************************
		//	NVDIA HVPerfHUD를 사용하기 위한 세팅
		//	NVDIA HVPerfHUD가 없다면, REF 모드로 실행 하도록 만들어 준다.
		//	NVDIA HVPerfHUD가 있다면, REF 모드라고 HAL로 실행된다.
		//******************************************************************

		D3DDEVTYPE devType = D3DDEVTYPE_HAL;			//!< Support only HAL mode
		ushort activeDriver = pDriver->GetIndex();		//!< 현재 장착한 드라이버 아뒤

#ifdef _PROFILE_NVPERFHUD
		for( ushort adapter = 0; adapter < pDriver->GetD3D()->GetAdapterCount(); ++adapter )
		{
			D3DADAPTER_IDENTIFIER9 Identifier;
			HRESULT Res = pD3D->GetAdapterIdentifier( adapter, 0, &Identifier );
			if( strcmp( Identifier.Description, "NVIDIA NVPerfHUD" ) == 0 )
			{
				activeDriver = adapter;
				devType = D3DDEVTYPE_REF;
				CNLOG( L"** [NVIDIA NVPerfHUD] Profiling **" );
				break;
			}
		}
#endif

		//******************************************************************
		// Thread-Safe를 위해 D3DCREATE_MULTITHREADED 옵션을 주고 디바이스를 생성.
		// 옵션을 주었는데도 Thread 관련 문제가 발생한다면, Device에 접근할 때를 
		// CriticalSection으로 동기화 처리해 버리는 방법도 생각해 봐야 할 듯.
		//******************************************************************
		HRESULT hr = 0;
		if( bThreadSafe )
		{
			CNLOG( L"D3D MULTI_THREAD SUPPORT : [D3DCREATE_MULTITHREADED]" );
			CNLOG( L"Creating Direct3DDevice : HARDWARE_VERTEX_PROCESSING" );

#ifdef _DEBUG_SHADER
			ulong mode = D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED;
#else
			ulong mode = D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED;
#endif

			hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, m_pWindow->GetHWnd(), mode, &m_d3dPP, &m_pDevice );

			if( FAILED( hr ) )
			{
				CNLOG( L"Failed to create Direct3DDevice. MODE : HARDWARE_VERTEX_PROCESSING" );
				CNLOG( L"Creating Direct3DDevice : MIXED_VERTEX_PROCESSING" );

				hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, m_pWindow->GetHWnd(), D3DCREATE_MIXED_VERTEXPROCESSING | D3DCREATE_MULTITHREADED, &m_d3dPP, &m_pDevice );
				if( FAILED( hr ) )
				{
					CNLOG( L"Failed to create Direct3DDevice. MODE : MIXED_VERTEX_PROCESSING" );
					CNLOG( L"Creating Direct3DDevice : SOFRWARE_VERTEX_PROCESSING" );

					hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, m_pWindow->GetHWnd(), D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED, &m_d3dPP, &m_pDevice );
				}
			}
		}
		else
		{
			CNLOG( L"D3D MULTI_THREAD SUPPORT : NONE'" );
			CNLOG( L"Creating Direct3DDevice : HARDWARE_VERTEX_PROCESSING" );

			hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, m_pWindow->GetHWnd(), 
									 D3DCREATE_HARDWARE_VERTEXPROCESSING, 
									 &m_d3dPP, &m_pDevice );

			if( FAILED( hr ) )
			{
				CNLOG( L"Failed to create Direct3DDevice. MODE : HARDWARE_VERTEX_PROCESSING" );
				CNLOG( L"Creating Direct3DDevice : MIXED_VERTEX_PROCESSING" );

				hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, m_pWindow->GetHWnd(), D3DCREATE_MIXED_VERTEXPROCESSING, &m_d3dPP, &m_pDevice );
				if( FAILED( hr ) )
				{
					CNLOG( L"Failed to create Direct3DDevice. MODE : MIXED_VERTEX_PROCESSING" );
					CNLOG( L"Creating Direct3DDevice : SOFRWARE_VERTEX_PROCESSING" );

					hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, m_pWindow->GetHWnd(), D3DCREATE_SOFTWARE_VERTEXPROCESSING, &m_d3dPP, &m_pDevice );
				}
			}
		}

		if( SUCCEEDED( hr ) )
		{
			// Store references to buffers for convenience
			m_pDevice->GetRenderTarget( 0, &m_pRenderSurface );
			m_pDevice->GetDepthStencilSurface( &m_pRenderZBuffer );

			// release immediately so we don't hog them
			m_pRenderSurface->Release();
			m_pRenderZBuffer->Release();
		}
		else
		{
			CNERROR( L"[CnDx9RenderTarget::_CreateDevice] Failed to create D3D Device" );
		}

		return true;
	}

	//----------------------------------------------------------------------------
	void CnDx9RenderTarget::_InitPresentParameter( CnDx9Driver* pDriver )
	{
		ZeroMemory( &m_d3dPP, sizeof( D3DPRESENT_PARAMETERS ) );

		m_d3dPP.hDeviceWindow			= m_pWindow->GetHWnd();
		m_d3dPP.Windowed				= !m_bFullScreen;
		m_d3dPP.SwapEffect				= D3DSWAPEFFECT_DISCARD;
		m_d3dPP.BackBufferCount			= 1;
		m_d3dPP.BackBufferWidth			= m_pWindow->GetWidth();
		m_d3dPP.BackBufferHeight		= m_pWindow->GetHeight();

		// Request depth and stencil buffers.  The parameters are not independent
		// for DirectX.  TO DO.  For now, just grab a 24-bit depth buffer and an
		// 8-bit stencil buffer.
		m_d3dPP.EnableAutoDepthStencil	= TRUE;
		m_d3dPP.AutoDepthStencilFormat	= D3DFMT_D24S8;

		if( m_bFullScreen )
		{
			m_d3dPP.FullScreen_RefreshRateInHz	= m_usRefreshRate;
		}
		else
		{
			m_d3dPP.FullScreen_RefreshRateInHz	= 0;
		}

		m_d3dPP.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

		//! Driver Informations.....
		D3DDEVTYPE devType = D3DDEVTYPE_HAL;			//!< Support only HAL mode
		ushort activeDriver = pDriver->GetIndex();		//!< 현재 장착한 드라이버 아뒤

		//! Back buffer format...
		HDC hDC = GetDC( m_pWindow->GetHWnd() );
		ushort usDesktopBit = GetDeviceCaps( hDC, BITSPIXEL );
		if( !m_bFullScreen || ( m_bFullScreen && ( m_usColorDepth > usDesktopBit ) ) )
		{
			m_d3dPP.BackBufferFormat = pDriver->GetCurDisplayMode().Format;
		}
		else
		{
			if( m_usColorDepth > 16 )
			{
				m_d3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
				CNLOG( L"Windowed BACK_BUFFER_FORMAT : D3DFMT_X8R8G8B8" );
			}
			else
			{
				m_d3dPP.BackBufferFormat = D3DFMT_R5G6B5;
				CNLOG( L"Windowed BACK_BUFFER_FORMAT : D3DFMT_R5G6B5" );
			}
		}

		//! Find Depth Stencil format
		if( m_usColorDepth > 16 )
		{
			if( FAILED( pDriver->GetD3D()->CheckDeviceFormat( activeDriver, devType, m_d3dPP.BackBufferFormat,
															  D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, D3DFMT_D24S8 ) ) )
			{
				if( SUCCEEDED( pDriver->GetD3D()->CheckDeviceFormat( activeDriver, devType, m_d3dPP.BackBufferFormat,
																	 D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, D3DFMT_D32 ) ) )
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D32;
					CNLOG( L"DEPTH_STENCIL Format : D3DFMT_D32" );
				}
				else
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D16;
					CNLOG( L"DEPTH_STENCIL Format : D3DFMT_D16" );
				}
			}
			else
			{
				if( SUCCEEDED( pDriver->GetD3D()->CheckDepthStencilMatch( activeDriver, devType, m_d3dPP.BackBufferFormat, 
																		  m_d3dPP.BackBufferFormat, D3DFMT_D24X8 ) ) )
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D24X8;
					CNLOG( L"DEPTH_STENCIL Format : D3DFMT_D24X8" );
				}
				else
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D24S8;
					CNLOG( L"DEPTH_STENCIL Format : D3DFMT_D24S8" );
				}
			}
		}
		else
		{
			m_d3dPP.AutoDepthStencilFormat = D3DFMT_D16;
			CNLOG( L"DEPTH_STENCIL Format : D3DFMT_D16" );
		}
	}

	//================================================================
	/** SwapBuffer
	    @remarks	  백버퍼와 주버퍼를 교체    
		@param        none
		@return       long : HRESULT
	*/
	//================================================================
	long CnDx9RenderTarget::_SwapBuffer()
	{
		if( m_pDevice )
		{
			return m_pDevice->Present( 0, 0, 0, 0 );
		}
		return 0;
	}

	//================================================================
	/** Update
	    @remarks	  랜더 타겟 갱신
		@param        none
		@return       long : HRESULT
	*/
	//================================================================
	long CnDx9RenderTarget::Update()
	{
		if( !m_bIsActive )
			return 0;

		//m_pRenderer->SetCurrentRenderTarget( this );
		m_pWindow->Update();

		return _SwapBuffer();
	}

	//================================================================
	/** Resize
	    @remarks	  사이즈 조정
		@param        usWidth : 가로 길이 설정
		@return       usHeight : 세로 길이 설정
	*/
	//================================================================
	void CnDx9RenderTarget::Resize( ushort usWidth, ushort usHeight )
	{
		m_pWindow->Resize( usWidth, usHeight );

		m_d3dPP.BackBufferWidth = usWidth;
		m_d3dPP.BackBufferHeight = usHeight;
	}

	//================================================================
	/** Release
	    @remarks	  D3D Device 해제
		@param        none
		@return       none
	*/
	//================================================================
	void CnDx9RenderTarget::Release()
	{
		SAFEREL( m_pRenderSurface );
		SAFEDEL( m_pRenderZBuffer );

		m_bIsActive = false;
	}

	//================================================================
	/** Restore
	    @remarks	  D3D Device 복구
		@param        none
		@return       none
	*/
	//================================================================
	void CnDx9RenderTarget::Restore()
	{
		m_pDevice->GetRenderTarget( 0, &m_pRenderSurface );

		if( m_pRenderSurface )
		{
			D3DSURFACE_DESC d3dSurfDesc;
			HRESULT hr = m_pRenderSurface->GetDesc( &d3dSurfDesc );
			Assert( SUCCEEDED( hr ), L"[CnDx9RenderTarget::Restore] Failed 'Restore' " );
		}

		m_pDevice->GetDepthStencilSurface( &m_pRenderZBuffer );

		m_bIsActive = true;
	}
}