//================================================================
// File:               : CnDx9Texture.inl
// Related Header File : CnDx9Texture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 9
//================================================================
//----------------------------------------------------------------
//	텍스쳐 내부 객체 반환
//----------------------------------------------------------------
inline
void CnDx9Texture::GetBaseTexture( void* pBaseTexture )
{
	LPDIRECT3DBASETEXTURE9* baseTex = (LPDIRECT3DBASETEXTURE9*)pBaseTexture;
	*baseTex = m_pBaseTexture;
}

inline
void CnDx9Texture::GetTexture( void* pTexture )
{
	LPDIRECT3DTEXTURE9* tex = (LPDIRECT3DTEXTURE9*)pTexture;
	*tex = m_pTexture;
}

//----------------------------------------------------------------
//	Surface 객체 반환
//----------------------------------------------------------------
inline
void CnDx9Texture::GetSurface( void* pSurface )
{
	LPDIRECT3DSURFACE9* surface = (LPDIRECT3DSURFACE9*)pSurface;
	*surface = m_pRenderTargetSurface;
}

inline
void CnDx9Texture::GetDepthStencil( void* pSurface )
{
	LPDIRECT3DSURFACE9* surface = (LPDIRECT3DSURFACE9*)pSurface;
	*surface = m_pDepthStencilSurface;
}