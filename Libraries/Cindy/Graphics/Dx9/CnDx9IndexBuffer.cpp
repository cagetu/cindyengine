//================================================================
// File:               : CnDx9IndexBuffer.inl
// Related Header File : CnDx9IndexBuffer.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================
#include "Cindy.h"
#include "CnDx9IndexBuffer.h"
#include "CnDx9Renderer.h"

namespace Cindy
{
	/// Const/Dest
	CnDx9IndexBuffer::CnDx9IndexBuffer()
	: m_pBuffer(0)
	{
	}
	CnDx9IndexBuffer::~CnDx9IndexBuffer()
	{
		Destroy();
	}

	//================================================================
	/** Create
	    @remarks      인덱스 버퍼 생성
		@param        nNumIndices : 인덱스 개수
		@param		  ulUsage : 모드
		@param		  ulPool : 풀링 모드
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnDx9IndexBuffer::Create( uint nNumIndices, ulong ulUsage, ulong ulFormat, ulong ulPool )
	{
		if( m_pBuffer )
			return false;

		if( 0 == nNumIndices || 0xffffffff == nNumIndices )
			return false;

		m_nNumIndices = nNumIndices;

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		HRESULT hr = device->CreateIndexBuffer( GetSizeOfIndices(), ulUsage, (D3DFORMAT)ulFormat, (D3DPOOL)ulPool, &m_pBuffer, NULL );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9IndexBuffer::Create] Failed 'Create IndexBuffer'" );
#else
		if( FAILED( hr ) )
			return NULL;
#endif
		return true;
	}

	//================================================================
	/** Destroy
	    @remarks      인덱스 버퍼 삭제
		@param        none
		@return       none
	*/
	//================================================================
	void CnDx9IndexBuffer::Destroy()
	{
		if( !m_pBuffer )
			return;

		SAFEREL( m_pBuffer );
	}

	//================================================================
	/** Lock
	    @remarks      인덱스 버퍼 세팅   
		@param        none
		@return       none
	*/
	//================================================================
	ushort* CnDx9IndexBuffer::Lock( uint nOffset, uint nCount, ulong ulFlags )
	{
		if( !m_pBuffer )
			return NULL;

		ushort* indices = NULL;

		HRESULT hr = m_pBuffer->Lock( nOffset*GetSizeOfIndex(), nCount*GetSizeOfIndex(), (void**)&indices, ulFlags );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9IndexBuffer::Lock] Failed 'Lock'" );
#else
		if( FAILED( hr ) )
			return NULL;
#endif
		return indices;
	}

	//================================================================
	/** Set Indices
	    @remarks      인덱스 버퍼 세팅   
		@param        none
		@return       none
	*/
	//================================================================
	bool CnDx9IndexBuffer::Unlock()
	{
		if( !m_pBuffer )
			return false;

		HRESULT hr = m_pBuffer->Unlock();
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9IndexBuffer::Unlock] Failed 'Unlock'" );
#else
		if( FAILED( hr ) )
			return false;
#endif
		return true;
	}
}
