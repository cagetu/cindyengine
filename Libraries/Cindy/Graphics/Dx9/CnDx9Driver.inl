// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
inline ushort CnDx9Driver::GetIndex() const
{
	return m_usIndex;
}

//--------------------------------------------------------------
inline LPDIRECT3D9 CnDx9Driver::GetD3D() const
{
	return m_pD3D;
}

//--------------------------------------------------------------
inline D3DADAPTER_IDENTIFIER9 CnDx9Driver::GetAdapterIdentifier() const
{
	return m_AdapterIdentifier;
}

//--------------------------------------------------------------
inline D3DDISPLAYMODE CnDx9Driver::GetCurDisplayMode() const
{
	return m_CurDisplayMode;
}