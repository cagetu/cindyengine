//================================================================
// File:               : CnDx9RenderWindow.inl
// Related Header File : CnDx9RenderWindow.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 19
//================================================================

//----------------------------------------------------------------
/** Get D3D Present
	@remarks	현재 설정 정보를 얻어온다.
*/
//----------------------------------------------------------------
inline
//const D3DPRESENT_PARAMETERS& CnDx9RenderWindow::GetD3DPresent() const
D3DPRESENT_PARAMETERS* CnDx9RenderWindow::GetD3DPresent()
{
	return &m_d3dPP;
}

//----------------------------------------------------------------
/** Get D3D device
	@remarks	Dx Device를 얻어온다.
*/
//----------------------------------------------------------------
inline
LPDIRECT3DDEVICE9 CnDx9RenderWindow::GetD3DDevice() const
{
	return m_pDevice;
}