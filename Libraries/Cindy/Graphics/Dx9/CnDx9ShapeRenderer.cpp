//================================================================
// File:               : CnDx9ShapeRenderer.cpp
// Related Header File : CnDx9ShapeRenderer.h
// Original Author     : changhee
// Creation Date       : 2009. 6. 29
//================================================================
#include "Cindy.h"
#include "CnDx9ShapeRenderer.h"
#include "Util/CnLog.h"
#include "Scene/CnCamera.h"
#include "Graphics/CnRenderer.h"

namespace Cindy
{
	//================================================================
	CN_IMPLEMENT_RTTI(Cindy, CnDx9ShapeRenderer, CnShapeRenderer);
	// Constructor
	CnDx9ShapeRenderer::CnDx9ShapeRenderer()
	{
		ZeroMemory( m_ShapeMeshes, sizeof(m_ShapeMeshes) );
	}
	// Destructor
	CnDx9ShapeRenderer::~CnDx9ShapeRenderer()
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9ShapeRenderer::Open()
	{
		CnShapeRenderer::Open();

		HRESULT hr;
		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice(&d3dDevice);
		hr = D3DXCreateBox(d3dDevice, 1.0f ,1.0f, 1.0f, &m_ShapeMeshes[CnShapeRenderer::Box], NULL);
		assert(SUCCEEDED(hr));
		hr = D3DXCreateSphere(d3dDevice, 1.0f, 10, 5, &m_ShapeMeshes[CnShapeRenderer::Sphere], NULL);
		assert(SUCCEEDED(hr));
		hr = D3DXCreateCylinder(d3dDevice, 1.0f, 1.0f, 1.0f, 10, 1, &m_ShapeMeshes[CnShapeRenderer::Cylinder], NULL);
		assert(SUCCEEDED(hr));
		hr = D3DXCreateTorus(d3dDevice, 1.0f, 1.0f, 10, 10, &m_ShapeMeshes[CnShapeRenderer::Torus], NULL);
		assert(SUCCEEDED(hr));
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9ShapeRenderer::Close()
	{
		CnShapeRenderer::Close();

		// release D3DX shapes
		for (int i = 0; i < CnShapeRenderer::NumShapeTypes; i++)
		{
			if (0 != m_ShapeMeshes[i])
			{
				m_ShapeMeshes[i]->Release();
				m_ShapeMeshes[i] = 0;
			}
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9ShapeRenderer::DrawShape( CnCamera* pCamera,
										const Math::Matrix44& ModelTransform,
										CnShapeRenderer::ShapeType eShapeType,
										const CnColor& Color )
	{
		assert(0 != m_ShapeMeshes[eShapeType]);
		assert(eShapeType < CnShapeRenderer::NumShapeTypes);

		HRESULT hr = m_ShapeMeshes[eShapeType]->DrawSubset(0);
		assert(SUCCEEDED(hr));
	}

} // namespace Cindy
