//================================================================
// File:               : CnDx9Query.cpp
// Related Header File : CnDx9Query.h
// Original Author     : changhee
// Creation Date       : 2009. 2. 15
//================================================================
#include "Cindy.h"
#include "CnDx9Query.h"
#include "CnDx9Renderer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnDx9Query, CnQuery);
	/// Const/Dest
	CnDx9Query::CnDx9Query()
		: m_pD3DQuery(0)
	{
	}
	CnDx9Query::~CnDx9Query()
	{
		SAFEREL( m_pD3DQuery );
	}

	//--------------------------------------------------------------
	/**	@brief	Query 생성
	*/
	bool CnDx9Query::Create( CnQuery::QUERYTYPE QueryType )
	{
		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice(&d3dDevice);

		HRESULT hr = d3dDevice->CreateQuery( (D3DQUERYTYPE)QueryType, &m_pD3DQuery );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9Query::Create] Failed 'Create Query'" );
#endif
		return SUCCEEDED( hr );
	}

	//--------------------------------------------------------------
	/**	@brief	Begin Issue
	*/
	void CnDx9Query::Begin()
	{
		HRESULT hr = m_pD3DQuery->Issue( D3DISSUE_BEGIN );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9Query::Begin] Failed 'm_pD3DQuery->Issue( D3DISSUE_BEGIN )'" );
#endif
	}

	//--------------------------------------------------------------
	/**	@brief	End Issue
	*/
	void CnDx9Query::End()
	{
		HRESULT hr = m_pD3DQuery->Issue( D3DISSUE_END );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9Query::End] Failed 'm_pD3DQuery->Issue( D3DISSUE_END )'" );
#endif
	}

	//--------------------------------------------------------------
	/**	@brief	결과 얻기
	*/
	void CnDx9Query::GetData( void* pData, ulong ulSize )
	{
		while (m_pD3DQuery->GetData(pData, ulSize, D3DGETDATA_FLUSH) == S_FALSE);
	}

	//--------------------------------------------------------------
	/**	@brief	Device 손실 처리
	*/
	void CnDx9Query::OnLost()
	{
#pragma TODO("Device Lost/Restore 처리를 전반적으로 정리해야 한다.")
	}

	//--------------------------------------------------------------
	/**	@brief	Device 복구 처리
	*/
	void CnDx9Query::OnRestore()
	{
	}
}