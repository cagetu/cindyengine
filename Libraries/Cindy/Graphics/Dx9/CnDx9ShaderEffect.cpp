//================================================================
// File:               : CnDx9ShaderEffect.cpp
// Related Header File : CnDx9ShaderEffect.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 2
//================================================================
#include "Cindy.h"
#include "Material/CnShaderManager.h"
#include "Util/CnLog.h"

#include "CnDx9ShaderEffect.h"
#include "CnDx9ShaderInclude.h"
#include "CnDx9Renderer.h"
#include "CnDx9Texture.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnDx9ShaderEffect, CnShaderEffect);
	/// Const/Dest
	CnDx9ShaderEffect::CnDx9ShaderEffect()
		: m_bHasVaildTechnique(false)
		, m_bInvalidTechnique(false)
		, m_bHasValidParameters(false)
		, m_nNumPass(0)
		, m_pEffect(0)
	{
	}
	CnDx9ShaderEffect::~CnDx9ShaderEffect()
	{
		Clear();
	}

	//================================================================
	/** CreateEffect
	    @remarks      쉐이더 Effect
		@param		  none
		@return       none
	*/
	//================================================================
	bool CnDx9ShaderEffect::Create( unsigned char* shaderCode, ulong fileSize )
	{
		ID3DXBuffer* errorBuffer = 0;
#if _D3D9_DEBUG
		DWORD compileFlags = D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION;
#else
		DWORD compileFlags = 0;
#endif

		//CnDx9ShaderInclude include( L"E:\\[개인]작업\\CrazyXIII\\sdk\\Shader\\" );
		//CnDx9ShaderInclude include( L"D:\\#Private\\#MyProject\\CrazyXIII\\sdk\\Shader\\" );
		//CnDx9ShaderInclude include( L"..\\..\\sdk\\Shader\\" );
		CnDx9ShaderInclude include( ShaderMgr->GetIncludePath().c_str() );

		LPDIRECT3DDEVICE9 device = NULL;
		RenderDevice->GetCurrentDevice( &device );

		ID3DXEffectPool* pEffectPool = NULL;
		RenderDevice->GetEffectPool( &pEffectPool );

		HRESULT hr = D3DXCreateEffect(	device,				// device
										shaderCode,			// FileData
										fileSize,			// filesize
										NULL,				// pDefines
										NULL, //&include,			// LPD3DXINCLUDE
										compileFlags,		// flags
										pEffectPool,		// effect pool
										&m_pEffect,			// effect
										&errorBuffer );		// compilationErrors
		if (FAILED(hr))
		{
			CnString error;
			if (errorBuffer)
				unicode::CharToWChar( (char*)errorBuffer->GetBufferPointer(), error );
			else
				error = L"No D3DX error message";

			CnError( error.c_str() );

//#ifdef _DEBUG
			MessageBox( NULL, error.c_str(), L"[CnDx9ShaderEffect::Load]", NULL );
//#endif
			SAFEREL(errorBuffer);

			return false;
		}

		_UpdateTechniques();
		_UpdateParameterHandles();

		m_bHasVaildTechnique = false;
		m_bInvalidTechnique = false;

		return true;
	}

	//================================================================
	/** _UpdateTechniques
	    @remarks      쉐이더 테크닉을 얻어온다.
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::_UpdateTechniques()
	{
		_ClearTechniques();

		D3DXTECHNIQUE_DESC techDesc = {0};
		ShaderHandle hTechnique = NULL;

		D3DXPASS_DESC passDesc = {0};
		ShaderHandle hPass = NULL;

		LPD3DXCONSTANTTABLE constTable = NULL;
		D3DXCONSTANTTABLE_DESC constTableDesc = {0};
		D3DXCONSTANT_DESC constDesc = {0};
		D3DXPARAMETER_DESC paramDesc = { 0 };
		ShaderHandle hConst = NULL;

		CnString name;

		HRESULT hr = NULL;
		while (1)
		{
			hr = m_pEffect->FindNextValidTechnique( hTechnique, &hTechnique );
			if (S_OK != hr)
				break;

			m_pEffect->GetTechniqueDesc( hTechnique, &techDesc );

			unicode::CharToWChar( techDesc.Name, name );
			CnTechnique* technique = _AddTechnique( name );
			technique->SetHandle( hTechnique );

			// generate the feature mask from the "Mask" annotation
			D3DXHANDLE hFeatureAnnotation = m_pEffect->GetAnnotationByName(hTechnique, "Mask");
			if (NULL != hFeatureAnnotation)
			{
				LPCSTR strPtr = 0;
				hr = m_pEffect->GetString(hFeatureAnnotation, &strPtr);
				assert(SUCCEEDED(hr));

				CnString annotation;
				unicode::CharToWChar( strPtr, annotation );
				CnShaderFeature::Mask mask = ShaderMgr->FeatureStringToMask( annotation );
				technique->SetFeatureMask(mask);
			}
			else
			{
				// issue a warning
				//CnPrint( L"WARNING: No Features annotation in technique '%s'!\n", name.c_str() );
			}

			for (unsigned int i=0; i < techDesc.Passes; ++i)
			{
				hPass = m_pEffect->GetPass( hTechnique, i );
				m_pEffect->GetPassDesc( hPass, &passDesc );

				unicode::CharToWChar( passDesc.Name, name );
				CnPass* pass = technique->AddPass( name );
				pass->SetIndex( i );
				pass->SetHandle( hPass );

#ifdef _ENABLE_PASS_PARAM_
				//unsigned int numSementics = 0;
				//D3DXSEMANTIC aa[30];
				//D3DXGetShaderInputSemantics( passDesc.pVertexShaderFunction, aa, &numSementics );
				//D3DXGetShaderOutputSemantics( passDesc.pVertexShaderFunction, aa, &numSementics );

				// vertexShader 함수에서 사용하는 Parameter들 등록
				if (SUCCEEDED( D3DXGetShaderConstantTable( passDesc.pVertexShaderFunction, &constTable ) ))
				{
					constTable->GetDesc( &constTableDesc );
					for (unsigned int j=0; j<constTableDesc.Constants; ++j)
					{
						hConst = constTable->GetConstant( NULL, j );
						constTable->GetConstantDesc( hConst, &constDesc, NULL );

						hConst = m_pEffect->GetParameterByName( NULL, constDesc.Name );
						m_pEffect->GetParameterDesc( hConst, &paramDesc );

						if (paramDesc.Semantic)
						{
							ShaderState::Param param = ShaderState::StringToParam( paramDesc.Semantic );
							pass->SetParemter( param, hConst );
						}
						else
						{
							CnString string;
							unicode::CharToWChar( paramDesc.Name, string );
							CnError( L"%s has not sementic", string.c_str() );
						}
					}
				}

#pragma NOTE( "Sampler의 경우, texture + '_' + Sampler라고 이름을 규약한다." )
#pragma NOTE( "(ex) diffuseMap => diffuseMap_Sampler" )

				// PixelShader 함수에서 사용하는 Parameter들 등록
				if (SUCCEEDED( D3DXGetShaderConstantTable( passDesc.pPixelShaderFunction, &constTable ) ))
				{
					constTable->GetDesc( &constTableDesc );
					for (unsigned int j=0; j<constTableDesc.Constants; ++j)
					{
						hConst = constTable->GetConstant( NULL, j );
						constTable->GetConstantDesc( hConst, &constDesc, NULL );

						// Sampler의 경우, texture + '_' + "Sampler"라고 이름을 규약한다.
						// ex) diffuseMap => diffuseMap_Sampler
						std::string constName( constDesc.Name );
						if (constDesc.RegisterSet==D3DXRS_SAMPLER)
						{
							size_t pos = constName.find_first_of( L'_' );
							constName = constName.substr( 0, pos );
						}

						hConst = m_pEffect->GetParameterByName( NULL, constName.c_str() );
						m_pEffect->GetParameterDesc( hConst, &paramDesc );

						if (paramDesc.Semantic)
						{
							ShaderState::Param param = ShaderState::StringToParam( paramDesc.Semantic );
							pass->SetParemter( param, hConst );
						}
						else
						{
							CnString string;
							unicode::CharToWChar( paramDesc.Name, string );
							CnError( L"%s has not sementic", string.c_str() );
						}
					} // for
				} // if
#endif	// _ENABLE_PASS_PARAM_
			} // for
		}; // while
	}

	//================================================================
	/** _UpdateParameterHandles
	    @remarks      쉐이더 변수의 파라미터 핸들을 업데이트 한다.
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::_UpdateParameterHandles()
	{
		if( m_bHasValidParameters )
			return;

		Assert( m_pEffect, L"[CnDx9ShaderEffect::_UpdateTechniques] Isn't initialized" );

		memset( (void*)m_hParameters, 0, sizeof(m_hParameters) );
		//m_ParameterNames.clear();

		// for each parameter in the effect...
		D3DXEFFECT_DESC effectDesc = { 0 };
		D3DXPARAMETER_DESC paramDesc = { 0 };

		HRESULT hr = m_pEffect->GetDesc( &effectDesc );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::_UpdateParameterHandles] Failed 'GetDesc'");

		ShaderHandle currentHandle = NULL;
		for( uint i = 0; i < effectDesc.Parameters; ++i )
		{
			currentHandle = m_pEffect->GetParameter( NULL, i );
			Assert( currentHandle, L"[CnDx9ShaderEffect::_UpdateParameterHandles] Failed 'NULL == currentHandle'");

			// 파라미터 정보를 구한다.
			hr = m_pEffect->GetParameterDesc( currentHandle, &paramDesc );
			Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::_UpdateParameterHandles] Failed 'GetParameterDesc'");

			// 파라미터 정보를 저장한다.
			//ShaderState::Param param = ShaderState::StringToParam( paramDesc.Name );
			if (!paramDesc.Semantic)
				continue;

			ShaderState::Param param = ShaderState::StringToParam( paramDesc.Semantic );
			m_hParameters[param] = currentHandle;
		}

		m_bHasValidParameters = true;
	}

	//================================================================
	/** _SetVaildTechnique
	    @remarks      쉐이더 변수의 파라미터 핸들을 업데이트 한다.
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::_UpdateVaildTechnique()
	{
		// 첫 번째 인자가 NULL이라면, 유효한 테크닉의 검색은 이펙트의 첫 번째 테크닉으로부터 시작된다.

		ShaderHandle technique = NULL;
		HRESULT hr = m_pEffect->FindNextValidTechnique( NULL, &technique );
		if( SUCCEEDED( hr ) )
		{
			SetTechnique( technique );
			m_bHasVaildTechnique = true;
			m_bInvalidTechnique = false;
		}
		else
		{
			m_bInvalidTechnique = true;

			CnPrint( L"[CnDx9ShaderEffect::_UpdateVaildTechnique] Invalid Technique" );

			// 예외 처리 요망..
		}
	}

	//================================================================
	/** SetTechnique
	    @remarks      현재 사용할 Technique을 설정한다.
		@param		  hTechnique : 테크닉 이름
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::SetTechnique( ShaderHandle hTechnique )
	{
		if ( hTechnique == m_pEffect->GetCurrentTechnique() )
			return;

		HRESULT hr = m_pEffect->SetTechnique( hTechnique );
		if ( SUCCEEDED( hr ) )
		{
			m_bHasVaildTechnique = true;
		}
		else
		{
			m_bInvalidTechnique = true;

			CnPrint( L"[CnDx9ShaderEffect::SetTechnique] Invalid Technique" );

			// 예외 처리 요망..
		}
	}

	//================================================================
	/** Begin
	    @remarks	  쉐이더 사용 시작
		@param		  bSaveState : 스테이트 저장 여부
		@return       int : m_nNumPass
	*/
	//================================================================
	int CnDx9ShaderEffect::Begin( bool bSaveState )
	{
		if( !m_bHasVaildTechnique )
			_UpdateVaildTechnique();

		if( m_bInvalidTechnique )
			return 0;

		DWORD flags = 0;
		if( !bSaveState )
			flags = D3DXFX_DONOTSAVESTATE;

		HRESULT hr = m_pEffect->Begin( &m_nNumPass, flags );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::Begin] Failed 'm_pEffect->Begin'");

		return m_nNumPass;
	}

	//================================================================
	/** End
	    @remarks      쉐이더 사용 끝
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::End()
	{
		if( m_bInvalidTechnique )
			return;

		HRESULT hr = m_pEffect->End();
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::End] Failed 'm_pEffect->End'");
	}

	//================================================================
	/** Lost
	    @remarks      윈도우 크기가 변경되는 등 디바이스가 리셋 되었을 때 처리
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::OnLost()
	{
		if(!m_pEffect)
			return;

		HRESULT hr = m_pEffect->OnLostDevice();
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::Lost] Failed 'm_pEffect->OnLostDevice'");
	}

	//================================================================
	/** Restore
	    @remarks      디바이스가 복원되었을 경우 처리
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::OnRestore()
	{
		if(!m_pEffect)
			return;

		HRESULT hr = m_pEffect->OnResetDevice();
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::Restore] Failed 'm_pEffect->OnResetDevice'");
	}

	//================================================================
	/** Clear
	    @remarks      쉐이더 객체 해제
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::Clear()
	{
		SAFEREL( m_pEffect );
	}

	//================================================================
	/** Begin Pass
	    @remarks      Pass 시작
		@param		  nPass : 패스 인덱스
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::BeginPass( uint nPass )
	{
#if (D3D_SDK_VERSION >= 32) //summer 2004 update sdk
		HRESULT hr = m_pEffect->BeginPass( nPass );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::BeginPass] Failed 'm_pEffect->BeginPass'");
#else
		HRESULT hr = m_pEffect->Pass( nPass );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::Pass] Failed 'm_pEffect->BeginPass'");
#endif
	}

	//================================================================
	/** EndPass
	    @remarks      Pass 끝
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::EndPass()
	{
#if (D3D_SDK_VERSION >= 32) //summer 2004 update sdk
		HRESULT hr = m_pEffect->EndPass();
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::EndPass] Failed 'm_pEffect->EndPass'");
#endif
	}

	//================================================================
	/** CommitChanges
	    @remarks      랜더링 되기 전에 디바이스에 Pass를 활성화 상태로 전환시킨다.
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::CommitChanges()
	{
#if (D3D_SDK_VERSION >= 32) //summer 2004 update sdk
		HRESULT hr = m_pEffect->CommitChanges();
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::CommitChanges] Failed 'm_pEffect->CommitChanges'");
#endif
	}

	//================================================================
	/** Set Boolean
	    @remarks      Boolean 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        bValue : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetBool( ShaderHandle hHandle, bool val )
	{
		HRESULT hr = m_pEffect->SetBool( hHandle, val );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetBool] Failed 'm_pEffect->SetBool'");
	}

	//================================================================
	/** Set Int
	    @remarks      Int 형 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        val : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetInt( ShaderHandle hHandle, int val )
	{
		HRESULT hr = m_pEffect->SetInt( hHandle, val );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetInt] Failed 'm_pEffect->SetInt'");
	}

	//================================================================
	/** Set Float
	    @remarks      float형 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        val : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetFloat( ShaderHandle hHandle, float val )
	{
		HRESULT hr = m_pEffect->SetFloat( hHandle, val );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetFloat] Failed 'm_pEffect->SetFloat'");
	}

	//================================================================
	/** Set Float4
	    @remarks      float4형 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        val : 값
	*/
	//================================================================
	//void CnDx9ShaderEffect::SetFloat4( ShaderHandle hHandle, float* val )
	//{
	//	HRESULT hr = m_pEffect->SetFloat4( hHandle, val );
	//	Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetFloat] Failed 'm_pEffect->SetFloat4'");
	//}


	//================================================================
	/** Set Float Array
	    @remarks      쉐이더 Effect
		@param		  none
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::SetFloatArray( ShaderHandle hHandle, float* aFloats, int nSize )
	{
		HRESULT hr = m_pEffect->SetFloatArray( hHandle, aFloats, nSize );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetFloatArray] Failed 'm_pEffect->SetFloatArray'");
	}

	//================================================================
	/** Set Vector3 
	    @remarks      vector3 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        value : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetVector3( ShaderHandle hHandle, const Math::Vector3& val )
	{
		if ( hHandle == NULL )
			return;

		HRESULT hr = m_pEffect->SetFloatArray( hHandle, (float*)&val, 3 );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetVector3] Failed 'm_pEffect->SetFloatArray'");
	}

	//================================================================
	/** Set Vector4 
	    @remarks      Vector4 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        value : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetVector4( ShaderHandle hHandle, const Math::Vector4& val )
	{
		if ( hHandle == NULL )
			return;

		HRESULT hr = m_pEffect->SetVector( hHandle, &val );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetVector4] Failed 'm_pEffect->SetVector4'");
	}

	//================================================================
	/** Set Matrix
	    @remarks      Matrix 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        value : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetMatrix( ShaderHandle hHandle, const Math::Matrix44& val )
	{
		if (hHandle == NULL)
			return;

		HRESULT hr = m_pEffect->SetMatrix( hHandle, &val );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetMatrix] Failed 'm_pEffect->SetMatrix'");
	}
	
	//================================================================
	/** Set Vector3 Array
	    @remarks      Vector3 배열 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        aVectors : 벡터 배열
		@param		  nSize : 배열 개수
	*/
	//================================================================
	void CnDx9ShaderEffect::SetVector3Array( ShaderHandle hHandle, const Math::Vector3* aVectors, int nSize )
	{
		if (hHandle == NULL)
			return;

		HRESULT hr = m_pEffect->SetValue(hHandle, aVectors, nSize * sizeof(D3DXVECTOR3)); 
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetVector3Array] Failed 'm_pEffect->SetValue'");
	}
	
	//================================================================
	/** Set Vector4 Array
	    @remarks      Vector4 배열 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        aVectors : 벡터 배열
		@param		  nSize : 배열 개수
	*/
	//================================================================
	void CnDx9ShaderEffect::SetVector4Array( ShaderHandle hHandle, const Math::Vector4* aVectors, int nSize )
	{
		if (hHandle == NULL)
			return;

		HRESULT hr = m_pEffect->SetVectorArray( hHandle, (const D3DXVECTOR4*)aVectors, nSize );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetVector4Array] Failed 'm_pEffect->SetVectorArray'");
	}

	//================================================================
	/** Set Matrix Array
	    @remarks      Matrix 배열 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        aMatrices : 매트릭스 배열
		@param		  nSize : 배열 개수
	*/
	//================================================================
	void CnDx9ShaderEffect::SetMatrixArray( ShaderHandle hHandle, const Math::Matrix44* aMatrices, int nSize )
	{
		if (hHandle == NULL)
			return;

		HRESULT hr = m_pEffect->SetMatrixArray( hHandle, aMatrices, nSize );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetMatrixArray] Failed 'm_pEffect->SetMatrixArray'");
	}

	//================================================================
	/** Set Texture
	    @remarks      텍스쳐 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        texture : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetTexture( ShaderHandle hHandle, CnTexture* pTexture )
	{
		if (hHandle == NULL || pTexture == NULL)
			return;

		//LPDIRECT3DTEXTURE9 tex = NULL;
		//pTexture->GetTexture( &tex );
		LPDIRECT3DBASETEXTURE9 tex = NULL;
		pTexture->GetBaseTexture( &tex );

		HRESULT hr = m_pEffect->SetTexture( hHandle, tex );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetTexture] Failed 'm_pEffect->SetTexture'");
	}

	//================================================================
	/** Set Textures
	    @remarks      텍스쳐 파라미터 설정
		@param		  nParam : 파라미터 번호
		@param        texture : 값
	*/
	//================================================================
	void CnDx9ShaderEffect::SetTextures( ShaderHandle hHandle, CnTexture** pTextures, int nSize )
	{
		if (hHandle == NULL)
			return;
	}

	//================================================================
	/** Set Value
	    @remarks      임의의 파라미터를 설정한다.
		@param		  hHandle : 파라미터 지정
		@param		  pData : 데이터
		@param		  bytes : 사이즈
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::SetValue( ShaderHandle hHandle, void* pData, unsigned int nBytes )
	{
		HRESULT hr = m_pEffect->SetValue( hHandle, pData, nBytes );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetValue] Failed 'm_pEffect->SetValue'");
	}
	
	//================================================================
	/** Set Color
	    @remarks      색상 파라미터 설정
		@param		  hHandle : 파라미터 지정
		@param		  color : 색상
		@return       none
	*/
	//================================================================
	void CnDx9ShaderEffect::SetColor( ShaderHandle hHandle, const CnColor& color )
	{
		HRESULT hr = m_pEffect->SetValue( hHandle, &color, sizeof(CnColor) );
		Assert( SUCCEEDED(hr), L"[CnDx9ShaderEffect::SetColor] Failed 'm_pEffect->SetValue'");
	}
}