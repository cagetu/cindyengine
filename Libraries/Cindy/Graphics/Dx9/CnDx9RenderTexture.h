//================================================================
// File:           : CnDx9RenderTexture.h
// Original Author : changhee
// Creation Date   : 2007. 4. 19
//================================================================
#pragma once

#include "../Base/CnRenderTexture.h"

namespace Cindy
{
	class CnDx9RenderWindow;

	//================================================================
	/** Direct9 Render Texture
	    @author    changhee
		@since     2007. 4. 19
		@remarks   랜더링 될 텍스쳐 타겟에 대한 클래스
	*/
	//================================================================
	class CnDx9RenderTexture : public CnRenderTexture
	{
		__DeclareRtti;
	public:
		CnDx9RenderTexture( const wchar* strName,
							ushort usWidth,
							ushort usHeight,
							ushort usDepth,
							PixelFormat::Code eFormat = PixelFormat::A8R8G8B8,
							CnAntiAliasQuality::Type eAnitiQuality = CnAntiAliasQuality::None,
							bool bEnableDepthStencil = true,
							bool bGenerateMipMaps = false );
		virtual ~CnDx9RenderTexture();

		// Setup
		void	Setup();

		//// Restore/Release
		//void	OnRelease() override;
		//void	OnRestore() override;

		//
		void	ColorFill( const CnColor& Color ) override;

		// Update
		bool	Update() override;	
//		void	Draw() override;

		//
		void	Begin() override;
		void	Draw() override;
		void	End() override;

		// Debug
		void	Debug();

	protected:
		D3DMULTISAMPLE_TYPE	m_d3d9MultiSampleType;
		DWORD				m_d3d9MultiSampleQuality;

		LPDIRECT3DSURFACE9	m_d3dColorBuffer;
		LPDIRECT3DSURFACE9	m_d3dDepthStencilBuffer;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void	SetupMultiSampleType();
	};
}