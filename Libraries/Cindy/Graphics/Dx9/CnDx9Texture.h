//================================================================
// File:           : CnDx9Texture.h
// Original Author : changhee
// Creation Date   : 2007. 4. 9
//================================================================
#pragma once

#include "Material/CnTexture.h"

namespace Cindy
{
	//================================================================
	/** Dx9 Texture
	    @author    changhee
		@since     2007. 4. 9
		@remarks   
	*/
	//================================================================
	class CnDx9Texture : public CnTexture
	{
		__DeclareRtti;
	public:
		virtual ~CnDx9Texture();

		// Load/Unload
		bool	Load( const wchar* strFileName ) override;
		bool	Unload() override;

		// Lost/Restore
		void	OnLost() override;
		void	OnRestore() override;

		// Get Texture
		void	GetBaseTexture( void* pBaseTexture );
		void	GetTexture( void* pTexture );
		void	GetSurface( void* pSurface );
		void	GetDepthStencil( void* pStencil );

		void	GenerateMipLevels() override;
	protected:
		CnDx9Texture( CnResourceManager* pParent );

		PixelFormat::Code	D3DFormatToFormat( D3DFORMAT eFormat );

		bool	_Load( const CnString& strFileName, bool bGenerateMipMaps );
		bool	_CreateRenderTarget();

		bool	_CheckRenderTargetFormat( LPDIRECT3D9 pD3D, LPDIRECT3DDEVICE9 pDevice, DWORD dwUsage, D3DFORMAT ePixelFormat );

	private:
		friend class CnTextureManager;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		LPDIRECT3DBASETEXTURE9		m_pBaseTexture;
		LPDIRECT3DTEXTURE9			m_pTexture;
		LPDIRECT3DCUBETEXTURE9		m_pCubeTexture;
		LPDIRECT3DVOLUMETEXTURE9	m_pVolumeTexture;

		LPDIRECT3DSURFACE9			m_pRenderTargetSurface;
		LPDIRECT3DSURFACE9			m_pDepthStencilSurface;
	};

#include "CnDx9Texture.inl"
} // end of namespace 