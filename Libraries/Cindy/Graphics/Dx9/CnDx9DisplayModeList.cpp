// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnDx9DisplayModeList.h"
#include "CnDx9Driver.h"

namespace Cindy
{
	//=====================================================================
	//	Display Mode List
	//=====================================================================
	CnDx9DisplayModeList::CnDx9DisplayModeList( CnDx9Driver* pAdapter )
	{
        assert( pAdapter );

		m_pAdapter = pAdapter;

		_Enumerate();
	}

	//--------------------------------------------------------------
	CnDx9DisplayModeList::~CnDx9DisplayModeList()
	{
		m_DisplayModes.clear();
	}

	//--------------------------------------------------------------
	ushort CnDx9DisplayModeList::GetCount()
	{
		return (ushort)m_DisplayModes.size();
	}

	//--------------------------------------------------------------
	void CnDx9DisplayModeList::_Enumerate()
	{
		LPDIRECT3D9 pD3D = m_pAdapter->GetD3D();
		uint uiAdapter = m_pAdapter->GetIndex();

		D3DDISPLAYMODE d3dDM;

		// For 16bit Pixel Format. Support R5G6B5
		ulong ulModeNum = pD3D->GetAdapterModeCount( uiAdapter, D3DFMT_R5G6B5 );
		for( ulong ulMode = 0; ulMode < ulModeNum; ++ulMode )
		{
			pD3D->EnumAdapterModes( uiAdapter, D3DFMT_R5G6B5, ulMode, &d3dDM );

			// Unnessare data will ignored
			if( d3dDM.Width < 640 || d3dDM.Height < 480 )
				continue;

			m_DisplayModes.push_back( CnDx9DisplayMode( d3dDM ) );
		}


		// For 32 bit pixel format. Support X8R8G8B8
		ulModeNum = pD3D->GetAdapterModeCount( uiAdapter, D3DFMT_X8R8G8B8 );
		for( ulong ulMode = 0; ulMode < ulModeNum; ++ulMode )
		{
			pD3D->EnumAdapterModes( uiAdapter, D3DFMT_X8R8G8B8, ulMode, &d3dDM );

			// filtering
			if( d3dDM.Width < 640 || d3dDM.Height < 480 )
				continue;

			m_DisplayModes.push_back( CnDx9DisplayMode( d3dDM ) );
		}
	}

	//--------------------------------------------------------------
    CnDx9DisplayMode* CnDx9DisplayModeList::GetDisplayMode( const CnString& strDesc )
	{
		CnString desc;

		DispIter itEnd = m_DisplayModes.end();
		for( DispIter it = m_DisplayModes.begin(); it != itEnd; ++it )
		{
			it->GetDescription( desc );
			if(	desc == strDesc )
			{
				return &(*it);
			}
		}

		return NULL;
	}

	//--------------------------------------------------------------
	CnDx9DisplayMode* CnDx9DisplayModeList::GetDisplayMode( ushort usIndex )
	{
		if( m_DisplayModes.empty() )
			return NULL;

		return &m_DisplayModes.at( usIndex );
	}

	//--------------------------------------------------------------
	CnDx9DisplayMode* CnDx9DisplayModeList::GetDisplayMode( uint uiWidth, uint uiHeight, uint uiColorDepth, uint uiRefreshRate )
	{
		DispIter itend = m_DisplayModes.end();
		for( DispIter it = m_DisplayModes.begin(); it != itend; ++it )
		{
			if( ( it->GetWidth() == uiWidth ) &&
				( it->GetHeight() == uiHeight ) &&
				( it->GetColorDepth() == uiColorDepth ) &&
				( it->GetRefreshRate() == uiRefreshRate ) )
			{
                return &(*it);
			}
		}
        
		return NULL;
	}
}	// end of namespace 