// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
inline
CnDx9DriverCaps::CnDx9DriverCaps()
{
	m_ulCapabilities			= 0;
	m_usMaxWorldMatrices		= 0;
	m_usMaxMRTs					= 0;
	m_usMaxTextures				= 0;
	m_usStencilBufferBits		= 0;
	m_usMaxVertexBlendMatrices	= 0;
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetCapability( CnDx9DriverCaps::CAPABILITIES eCaps )
{
	m_ulCapabilities |= eCaps;
}

//--------------------------------------------------------------
inline
bool CnDx9DriverCaps::HasCapability( CnDx9DriverCaps::CAPABILITIES eCaps ) const
{
	if( m_ulCapabilities & eCaps )
		return true;

	return false;
}

//--------------------------------------------------------------
//	Max Light Count
//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetMaxLight( ushort usCount )
{
	m_usMaxLights = usCount;
}

//--------------------------------------------------------------
inline
ushort CnDx9DriverCaps::GetMaxLight() const
{
	return m_usMaxLights;
}

//--------------------------------------------------------------
//	Stencil Buffer Depth Bit..
//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetStencilBufferBitDepth( ushort usDepth )
{
	m_usStencilBufferBits = usDepth;
}

//--------------------------------------------------------------
inline
ushort CnDx9DriverCaps::GetStencilBufferBitDepth() const
{
	return m_usStencilBufferBits;
}

//--------------------------------------------------------------
//	Max Texture Count
//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetMaxTexture( ushort usValue )
{
	m_usMaxTextures = usValue;
}

//--------------------------------------------------------------
inline
ushort CnDx9DriverCaps::GetMaxTexture() const
{
	return m_usMaxTextures;
}

//--------------------------------------------------------------
//	Max Number Of Multiple Render Targets
//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetMaxMRTs( ushort usNum )
{
	m_usMaxMRTs = usNum;
}
inline
ushort CnDx9DriverCaps::GetMaxMRTs() const
{
	return m_usMaxMRTs;
}

//--------------------------------------------------------------
//	Set Vertex/Pixel Shader Resiger Count
//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetVertexShaderFloatRegisterCount( ushort usNum )
{
	m_usVertexShaderFloatRegCount = usNum;
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetVertexShaderBoolRegisterCount( ushort usNum )
{
	m_usVertexShaderBoolRegCount = usNum;
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetVertexShaderIntRegisterCount( ushort usNum )
{
	m_usVertexShaderIntRegCount	= usNum;
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetPixelShaderFloatRegisterCount( ushort usNum )
{
	m_usPixelShaderFloatRegCount = usNum;
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetPixelShaderBoolRegisterCount( ushort usNum )
{
	m_usPixelShaderBoolRegCount = usNum;
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetPixelShaderIntRegisterCount( ushort usNum )
{
	m_usPixelShaderIntRegCount = usNum;
}

//--------------------------------------------------------------
//	Set Vertex/Pixel Shader Version
//--------------------------------------------------------------
inline
void	CnDx9DriverCaps::SetVertexShaderVersion( DWORD dwVersion)
{
	m_VertexShaderVersion.major = static_cast<ushort>( (dwVersion & 0x0000FF00) >> 8 );
	m_VertexShaderVersion.minor = static_cast<ushort>( dwVersion & 0x000000FF );		
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetPixelShaderVersion( DWORD dwVersion )
{
	m_PixelShaderVersion.major = static_cast<ushort>( (dwVersion & 0x0000FF00) >> 8 );
	m_PixelShaderVersion.minor = static_cast<ushort>( dwVersion & 0x000000FF );		
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetVertexShaderVersion( const CnString& strVersion )
{
	m_strVertexShaderVersion = strVersion;
}

//--------------------------------------------------------------
inline
void CnDx9DriverCaps::SetPixelShaderVersion( const CnString& strVersion )
{
	m_strPixelShaderVersion = strVersion;
}

//--------------------------------------------------------------
//	Get Vertex/Pixel Shader Version
//--------------------------------------------------------------
inline
void CnDx9DriverCaps::GetVertexShaderVersion( CnString& rVersion ) const
{
	rVersion = m_strVertexShaderVersion;
}

//--------------------------------------------------------------
inline 
void CnDx9DriverCaps::GetPixelShaderVersion( CnString& rVersion ) const
{
	rVersion = m_strPixelShaderVersion;
}

//--------------------------------------------------------------
inline 
void CnDx9DriverCaps::GetVertexShaderVersion( CnDx9DriverCaps::Version& rVersion ) const
{
	rVersion = m_VertexShaderVersion;
}

//--------------------------------------------------------------
inline 
void CnDx9DriverCaps::GetPixelShaderVersion( CnDx9DriverCaps::Version& rVersion ) const
{
	rVersion = m_PixelShaderVersion;
}

//--------------------------------------------------------------
//	Get Vertex/Pixel Shader Resiger Count
//--------------------------------------------------------------
inline 
ushort CnDx9DriverCaps::GetVertexShaderFloatRegisterCount( void ) const
{
	return m_usVertexShaderFloatRegCount;
}

//--------------------------------------------------------------
inline 
ushort CnDx9DriverCaps::GetVertexShaderBoolRegisterCount( void ) const
{
	return m_usVertexShaderBoolRegCount;
}

//--------------------------------------------------------------
inline 
ushort CnDx9DriverCaps::GetVertexShaderIntRegisterCount( void ) const
{
	return m_usVertexShaderIntRegCount;
}

//--------------------------------------------------------------
inline 
ushort CnDx9DriverCaps::GetPixelShaderFloatRegisterCount( void ) const
{
	return m_usPixelShaderFloatRegCount;
}

//--------------------------------------------------------------
inline 
ushort CnDx9DriverCaps::GetPixelShaderBoolRegisterCount( void ) const
{
	return m_usPixelShaderBoolRegCount;
}

//--------------------------------------------------------------
inline 
ushort CnDx9DriverCaps::GetPixelShaderIntRegisterCount( void ) const
{
	return m_usPixelShaderIntRegCount;
}