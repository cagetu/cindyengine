//================================================================
// File:               : CnDx9VertexDeclaration.inl
// Related Header File : CnDx9VertexDeclaration.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 6
//================================================================
//----------------------------------------------------------------
inline
IDirect3DVertexDeclaration9* CnDx9VertexDeclaration::GetDeclaration() const
{
	return m_pDeclaration;
}