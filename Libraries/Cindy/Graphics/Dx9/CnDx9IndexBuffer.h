//================================================================
// File:           : CnDx9IndexBuffer.h
// Original Author : changhee
// Creation Date   : 2007. 1. 31
//================================================================
#ifndef __CN_DX9_INDEXBUFFER_H__
#define __CN_DX9_INDEXBUFFER_H__

#include "../Base/CnIndexBuffer.h"

namespace Cindy
{
	//================================================================
	/** Dx9IndexBuffer
	    @author    changhee
		@since     2007. 1. 31
		@remarks   Direct9 ���� �ε��� ���� Ŭ����
				   16bit Index�� ��~
	*/
	//================================================================
	class CnDx9IndexBuffer : public CnIndexBuffer
	{
	public:
		CnDx9IndexBuffer();
		virtual ~CnDx9IndexBuffer();

		// Create/Destroy
		bool		Create( uint nNumIndices, ulong ulUsage = D3DUSAGE_WRITEONLY, ulong ulFormat = FMT_INDEX16, ulong ulPool = 0 ) override;
		void		Destroy() override;

		// Lock/Unlock
		ushort*		Lock( uint nOffset, uint nCount, ulong ulFlags ) override;
		bool		Unlock() override;

		///
		LPDIRECT3DINDEXBUFFER9	GetDxBuffer() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		LPDIRECT3DINDEXBUFFER9		m_pBuffer;
	};

	//----------------------------------------------------------------
	/**
	*/
	inline LPDIRECT3DINDEXBUFFER9
	CnDx9IndexBuffer::GetDxBuffer() const
	{
		return m_pBuffer;
	}
}

#endif	// __CN_DX9_INDEXBUFFER_H__