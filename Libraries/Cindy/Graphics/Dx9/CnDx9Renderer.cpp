// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnDx9Renderer.h"
#include "CnDx9RenderWindow.h"
#include "CnDx9RenderTexture.h"
#include "CnDx9MultipleRenderTarget.h"
#include "CnDx9Light.h"
#include "CnDx9Texture.h"
#include "CnDx9VertexDeclaration.h"
#include "CnDx9VertexBuffer.h"
#include "CnDx9IndexBuffer.h"
#include "CnDx9ShaderEffect.h"
#include "CnDx9Query.h"
#include "CnDx9Types.h"
#include "CnDx9Shape.h"

#include "Graphics/CindyRenderConfig.h"
#include "Geometry/CnVertexData.h"
#include "Scene/CnViewport.h"
#include "Util/CnLog.h"
#include "Material/CnShader.h"
#include "Material/CnRenderState.h"
#include "Material/CnMaterialState.h"
#include "Material/CnTextureState.h"
#include "Material/CnMaterial.h"

namespace Cindy
{
	// Const/Dest
	CnDx9Renderer::CnDx9Renderer()
		: m_pDriverList(0)
		, m_pCurrentDriver(0)
		, m_pDriverCaps(0)
		, m_pCurrentRenderTarget(0)
		, m_pCurrentRenderWindow(0)
		, m_pCurrentViewport(0)
		, m_pEffectPool(0)
		, m_usRenderStateFlags(0)
		, m_bNeedToSortRenderTargets(false)
		, m_LastVertexComponent(-1)
	{
		m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
		Assert( m_pD3D, L"[CnDx9Renderer::CnDx9Renderer] Failed 'Direct3DCreate9' " );

		_InitDrivers();				//!< Driver 정보 설정
		_CreateEffectPool();		//!< Shader Effect Pool 생성
	}
	CnDx9Renderer::~CnDx9Renderer()
	{
		SAFEREL( m_pEffectPool );

		m_RenderTargets.clear();

		m_pCurrentRenderTarget = NULL;
		m_pCurrentRenderWindow = NULL;
		m_pCurrentDriver = NULL;

		SAFEDEL( m_pDriverCaps );
		SAFEDEL( m_pDriverList );
	}

	//----------------------------------------------------------------
	/** Create Render Window
	    @brief      랜더링 타겟(Window) 생성한다. 엄밀히 말하면, Window에 D3D Device를 세팅한 녀석
		@param        hWnd					: 윈도우 핸들
		@param		  nWidth				: 윈도우 가로 크기
		@param		  nHeight				: 윈도우 세로 크기
		@param		  usColorDepth			: 색상 비트
		@param		  usRefreshRate			: 화면 주사율
		@param		  bFullScreen			: 전체 화면 모드 여부
		@param		  bThreadSafe			: 멀티쓰레드 사용여부
		@param		  bCurrentTarget		: 현재 타겟 설정
		@return		  CnDx9RenderWindow		: 생성된 랜더 타겟 (NULL이면, 생성되어 있는 녀석이 있음)
	*/
	//----------------------------------------------------------------
	CnRenderWindow* CnDx9Renderer::CreateRenderWindow( HWND hWnd,
													   const wchar* strName,
													   int nWidth,
													   int nHeight,
													   ushort usColorDepth,
													   ushort usRefreshRate,
													   bool bFullScreen,
													   bool bThreadSafe,
													   bool bCurrentTarget )
	{
		CnRenderTarget* target = GetRenderTarget( strName );
		if (target)
			return NULL;

		CnDx9RenderWindow* pWindow = CnNew CnDx9RenderWindow( hWnd, strName, nWidth, nHeight, this );
		if (!pWindow->_Create( m_pCurrentDriver, usColorDepth, usRefreshRate, bFullScreen, bThreadSafe ))
		{
			delete pWindow;
			return NULL;
		}

		_InitCapabilities( pWindow );

		if (bCurrentTarget)
			m_pCurrentRenderWindow = pWindow;

		// add renderTarget
		pWindow->SetType( CnRenderTarget::RT_WINDOW );
		AddRenderTarget( pWindow );

		return pWindow;
	}

	//----------------------------------------------------------------
	/** Create Render Texture
	    @brief		랜더링 타겟 텍스쳐를 생성한다.
		@return		CnRenderTexture	: 생성된 랜더 타겟 (NULL이면, 생성되어 있는 녀석이 있음)
	*/
	//----------------------------------------------------------------
	CnRenderTexture* CnDx9Renderer::CreateRenderTexture( const wchar* strName,
														 ushort usWidth,
														 ushort usHeight,
														 ushort usDepth,
														 int nFormat,
														 int nAntiAliasQuality,
														 bool bEnableDepthStencil,
														 bool bGenerateMipMaps,
														 bool bManage )
	{
		CnRenderTarget* target = GetRenderTarget( strName );
		if (target)
			return NULL;

		CnDx9RenderTexture* pRenderTexture = CnNew CnDx9RenderTexture(	strName,
																		usWidth,
																		usHeight,
																		usDepth,
																		(PixelFormat::Code)nFormat,
																		(CnAntiAliasQuality::Type)nAntiAliasQuality,
																		bEnableDepthStencil,
																		bGenerateMipMaps );
		{
			pRenderTexture->Setup();
			pRenderTexture->SetType( CnRenderTarget::RT_TEXTURE );
		}

		if (bManage)
		{
			AddRenderTarget( pRenderTexture );
		}

		return pRenderTexture;
	}
	
	//----------------------------------------------------------------
	/** Create Multi Render Target
	    @brief      멀터 랜더링 타겟을 생성한다.
		@return		  CnMultiRenderTarget	: 생성된 랜더 타겟 (NULL이면, 생성되어 있는 녀석이 있음)
	*/
	//----------------------------------------------------------------
	CnMultiRenderTarget* CnDx9Renderer::CreateMultiRenderTarget( const wchar* strName,
																 bool bManage  )
	{
		CnRenderTarget* target = GetRenderTarget( strName );
		if (target)
			return NULL;

		CnDx9MultiRenderTarget* pRenderTarget = CnNew CnDx9MultiRenderTarget( strName );

		// add renderTarget
		pRenderTarget->SetType( CnRenderTarget::RT_MRT );

		if (bManage)
		{
			AddRenderTarget( pRenderTarget );
		}

		return pRenderTarget;
	}

	//----------------------------------------------------------------
	/** Get RenderTarget
	    @brief		랜더링 타겟를 구한다
		@param	pRenderTarget	추가할 랜더 타겟
		@return	CnRenderTarget	타겟
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::AddRenderTarget( CnRenderTarget* pRenderTarget )
	{
		m_RenderTargets.push_back( pRenderTarget );
		m_bNeedToSortRenderTargets = true;
	}

	//----------------------------------------------------------------
	/** Get RenderTarget
	    @brief		랜더링 타겟를 구한다
		@param		strName			: 타겟 이름
		@return		CnRenderTarget	: 타겟
	*/
	//----------------------------------------------------------------
	CnRenderTarget* CnDx9Renderer::GetRenderTarget( const CnString& strName )
	{
		RenderTargetIter iend = m_RenderTargets.end();
		for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		{
			if ((*i)->GetName() == strName)
				return (*i);
		}
		return NULL;
	}

	//----------------------------------------------------------------
	/** Detach RenderTarget
	    @brief      랜더 타겟을 외부로 뽑는다.
		@param		strName			: 타겟 이름
		@return		CnRenderTarget	: 타겟
	*/
	//----------------------------------------------------------------
	CnRenderTarget* CnDx9Renderer::DetachRenderTarget( const CnString& strName )
	{
		CnRenderTarget* renderTarget = NULL;
		RenderTargetIter iend = m_RenderTargets.end();
		for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		{
			if ((*i)->GetName() == strName)
			{
				renderTarget = (*i);
				m_RenderTargets.erase( i );
				return renderTarget;
			}
		}
		return NULL;
	}

	//----------------------------------------------------------------
	/** Delete RenderWindow
	    @brief      랜더링 윈도우를 삭제 한다.
		@param        strName		: 윈도우 이름
		@return       true/false	: 성공여부
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::DeleteRenderTarget( const CnString& strName )
	{
		RenderTargetIter iend = m_RenderTargets.end();
		for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		{
			if ((*i)->GetName() == strName)
			{
				m_RenderTargets.erase(i);
				m_bNeedToSortRenderTargets = true;
				return true;
			}
		}
		return false;
	}

	//----------------------------------------------------------------
	/** DeleteAllRenderTargets
	    @brief      랜더링 타겟들을 모두 제거 한다.
		@param        none
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::DeleteAllRenderTargets()
	{
		m_RenderTargets.clear();
	}

	//----------------------------------------------------------------
	/**	SetCurrentRenderTarget
		@brief		현재 랜더링 되고 있는 랜더 타겟을 설정한다.
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetCurrentRenderTarget( CnRenderTarget* pRenderTarget )
	{
		m_pCurrentRenderTarget = pRenderTarget;
	}

	//----------------------------------------------------------------
	/**	GetCurrentRenderTarget
		@brief		현재 랜더링 되고 있는 랜더 타겟을 얻어온다.
	*/
	//----------------------------------------------------------------
	CnRenderTarget* CnDx9Renderer::GetCurrentRenderTarget() const
	{
		return m_pCurrentRenderTarget;
	}

	//----------------------------------------------------------------
	/** UpdateRenderTargets
	    @brief      모든 랜더링 타겟을 업데이트 한다.
		@param        none
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::UpdateRenderTargets()
	{
		if (m_bNeedToSortRenderTargets)
		{
			std::sort( m_RenderTargets.begin(), m_RenderTargets.end(), CnRenderTarget::Sort );
			m_bNeedToSortRenderTargets = false;
		}

		RenderTargetIter iend = m_RenderTargets.end();
		for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		{
			if ((*i)->IsActive())
			{
				m_bLostDevice = ( FAILED( (*i)->Update() ) ) ? true : false;
			} // if
		} // for
	}

	//----------------------------------------------------------------
	/** @brief	RenderTarget 재정렬 */
	//----------------------------------------------------------------
	void CnDx9Renderer::NeedToSortRenderTargets()
	{
		this->m_bNeedToSortRenderTargets = true;
	}

	//----------------------------------------------------------------
	/** Surface 설정/얻기
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetSurface( int nIndex, void* pTarget )
	{
		LPDIRECT3DSURFACE9 surface = (LPDIRECT3DSURFACE9)pTarget;
		GetCurrentDevice()->SetRenderTarget( nIndex, surface );
	}
	//----------------------------------------------------------------
	void CnDx9Renderer::GetSurface( int nIndex, void* pTarget )
	{
		LPDIRECT3DSURFACE9* surface = (LPDIRECT3DSURFACE9*)pTarget;
		GetCurrentDevice()->GetRenderTarget( nIndex, surface );
	}

	//----------------------------------------------------------------
	/** DepthStencilSurface 설정/얻기
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetDepthStencilSurface( void* pTarget )
	{
		LPDIRECT3DSURFACE9 surface = (LPDIRECT3DSURFACE9)pTarget;
		GetCurrentDevice()->SetDepthStencilSurface( surface );
	}
	//----------------------------------------------------------------
	void CnDx9Renderer::GetDepthStencilSurface( void* pTarget )
	{
		LPDIRECT3DSURFACE9* surface = (LPDIRECT3DSURFACE9*)pTarget;
		GetCurrentDevice()->GetDepthStencilSurface( surface );
	}

	//----------------------------------------------------------------
	/** @brief	개념 상 새로운 Surface를 설정하기 위해서...
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::OpenSurface( int nIndex )
	{
		std::set<int>::iterator iter = m_OpenedSurfaces.find(nIndex);
		assert( iter == m_OpenedSurfaces.end() );

		GetCurrentDevice()->GetRenderTarget( nIndex, &m_pLastSurface );
		GetCurrentDevice()->GetDepthStencilSurface( &m_pLastDepthStencil );

		m_OpenedSurfaces.insert( nIndex );
	}

	//----------------------------------------------------------------
	/**	@brief	개념상 닫기
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::CloseSurface( int nIndex )
	{
		std::set<int>::iterator iter = m_OpenedSurfaces.find(nIndex);
		if (iter == m_OpenedSurfaces.end())
			return;

		GetCurrentDevice()->SetRenderTarget( nIndex, m_pLastSurface );
		GetCurrentDevice()->SetDepthStencilSurface( m_pLastDepthStencil );

		m_OpenedSurfaces.erase( iter );
	}

	//----------------------------------------------------------------
	/** Set Current RenderTarget
	    @brief      현재 랜더링 될 랜더 타겟 설정
		@param        none
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetCurrentRenderWindow( CnRenderWindow* pRenderWindow )
	{
		CnDx9RenderWindow* pWindow = DynamicCast<CnDx9RenderWindow>( pRenderWindow );
		if (pWindow)
		{
			m_pCurrentRenderWindow = pWindow;
		}
		else
		{
			m_pCurrentRenderWindow = NULL;
		}
	}
	
	//----------------------------------------------------------------
	/** Get Current RenderTarget
	    @brief      현재 랜더링 될 랜더 타겟 얻어오기
	*/
	//----------------------------------------------------------------
	CnRenderWindow* CnDx9Renderer::GetCurrentRenderWindow() const
	{
		return m_pCurrentRenderWindow;
	}

	//----------------------------------------------------------------
	/** SetCurrentViewport
	    @brief      뷰포트 세팅.
		@param        pViewport : 뷰포트 객체
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetViewport( CnViewport* pViewport )
	{
		if (m_pCurrentViewport != pViewport)
		{
			m_pCurrentViewport = pViewport;

			D3DVIEWPORT9 d3dVP;
			d3dVP.X = m_pCurrentViewport->GetActualLeft();
			d3dVP.Y = m_pCurrentViewport->GetActualTop();
			d3dVP.Width = m_pCurrentViewport->GetActualWidth();
			d3dVP.Height = m_pCurrentViewport->GetActualHeight();
			d3dVP.MinZ = 0.0f;
			d3dVP.MaxZ = 1.0f;

			m_pCurrentRenderWindow->GetD3DDevice()->SetViewport( &d3dVP );
		} // if(m_pCurrentViewport...)
	}

	//----------------------------------------------------------------
	/** _RestoreLostDevice
	    @brief      Device 복구
		@param        none
		@return		  true/false : 성공 여부
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::_RestoreLostDevice()
	{
		//if (m_bLostDevice)
		//{
		//	RenderTargetIter iend = m_RenderTargets.end();
		//	for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		//	{
		//		if (!(i->second)->_RestoreLostDevice())
		//		{
		//			m_pCurrentRenderWindow = NULL;
		//			return false;
		//		}
		//	}

		//	m_bLostDevice = true;
		//}
		return true;
	}

	//----------------------------------------------------------------
	/** Get Current Device
	    @brief      현재 설정된 Device 반환
		@param        none
		@return		  LPDIRECT3DDEVICE9 : 현재 디바이스
	*/
	//----------------------------------------------------------------
	LPDIRECT3DDEVICE9 CnDx9Renderer::GetCurrentDevice() const
	{
		if (m_pCurrentRenderWindow)
			return m_pCurrentRenderWindow->GetD3DDevice();

		return NULL;
	}
	void CnDx9Renderer::GetCurrentDevice( void* pDevice )
	{
		LPDIRECT3DDEVICE9* device = (LPDIRECT3DDEVICE9*)pDevice;

		if (m_pCurrentRenderWindow)
			*device = m_pCurrentRenderWindow->GetD3DDevice();
	}

	//----------------------------------------------------------------
	/** ResetCurrentDevice
	    @brief      현재 설정된 Device 갱신
		@param        none
		@return		  true/false : 성공 여부
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::ResetCurrentDevice()
	{
		_Release();

		RenderTargetIter iend = m_RenderTargets.end();
		for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
			(*i)->OnReset();
		//RenderTargetIter iend = m_RenderTargets.end();
		//for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		//	(i->second)->OnReset();

		_Restore();
	}

	//----------------------------------------------------------------
	/** Get Current Present Parameters
	    @brief      현재 설정된 화면의 정보 얻어오기
		@param        none
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::GetCurrentPresentParams( void* pParams )
	{
		if (m_pCurrentRenderWindow)
		{
			memcpy( pParams, m_pCurrentRenderWindow->GetD3DPresent(), sizeof(D3DPRESENT_PARAMETERS) );
		}
	}

	//----------------------------------------------------------------
	/** _Release
	    @brief      장치를 사용하는 D3D 객체들을 해제한다.
		@param        none
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::_Release()
	{
		RenderTargetIter iend = m_RenderTargets.end();
		for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		{
			(*i)->Release();
		}
		//RenderTargetIter iend = m_RenderTargets.end();
		//for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		//{
		//	(i->second)->Release();
		//}
	}

	//----------------------------------------------------------------
	/** _Restore
	    @brief      장치를 사용하는 D3D 객체들을 복구한다.
		@param        none
		@return		  true/false : 성공 여부
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::_Restore()
	{
		RenderTargetIter iend = m_RenderTargets.end();
		for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		{
			(*i)->OnRestore();
		}
		//RenderTargetIter iend = m_RenderTargets.end();
		//for (RenderTargetIter i = m_RenderTargets.begin(); i != iend; ++i)
		//{
		//	(i->second)->OnRestore();
		//}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9Renderer::ClearBuffer( ulong ulFlags, ulong ulColor, float fDepth, ushort usStencil )
	{
		if (NULL == m_pCurrentRenderWindow)
			return;

		m_pCurrentRenderWindow->_ClearBuffer( ulFlags, ulColor, fDepth, usStencil );
	}

	//----------------------------------------------------------------
	/** Begin Scene
	    @brief      장면 그리기 시작
		@param        ulFlags		: 버퍼 클리어 옵션 ( CLEAR_TARGET, CLEAR_ZBUFFER, CLEAR_STENCIL )
		@param		  fDepth		: 깊이값
		@param		  usStencil		: 스텐실 값
		@return		  true/false	: 성공 여부
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::BeginScene( ulong ulFlags, float fDepth, ushort usStencil )
	{
		if (NULL == m_pCurrentRenderWindow || NULL == m_pCurrentViewport)
			return false;

		if (!m_pCurrentRenderWindow->_RestoreLostDevice())
		{
			CnPrint( L"[CnDx9Renderer::BeginScene] LostDevice" );
			return false;
		}
		m_pCurrentRenderWindow->_ClearBuffer( ulFlags, m_pCurrentViewport->GetBackgroundColor(), fDepth, usStencil );

		HRESULT hr = m_pCurrentRenderWindow->GetD3DDevice()->BeginScene();
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::BeginScene] Failed 'Begin Scene' " );

		return true;
	}
	//----------------------------------------------------------------
	bool CnDx9Renderer::BeginScene( ulong ulFlags, ulong ulBackgroundColor, float fDepth, ushort usStencil )
	{
		if (NULL == m_pCurrentRenderWindow)
			return false;

		if (!m_pCurrentRenderWindow->_RestoreLostDevice())
		{
			CnPrint( L"[CnDx9Renderer::BeginScene] LostDevice" );
			return false;
		}

		m_pCurrentRenderWindow->_ClearBuffer( ulFlags, ulBackgroundColor, fDepth, usStencil );

		HRESULT hr = m_pCurrentRenderWindow->GetD3DDevice()->BeginScene();
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::BeginScene] Failed 'Begin Scene' " );

		return true;
	}

	//----------------------------------------------------------------
	/** End Scene
	    @brief      장면 그리기 끝
		@param        none
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::EndScene()
	{
		HRESULT hr = m_pCurrentRenderWindow->GetD3DDevice()->EndScene();
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::EndScene] Failed 'End Scene' " );

		m_LastVertexComponent = -1;
	}

	//----------------------------------------------------------------
	/** Set AlphaBlend Mode
	    @brief      AlphaBlend Mode 설정
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetAlphaBlendMode( const CnRenderState& rState )
	{
		// AlphaBlend..
		if (rState.IsFlags(CnRenderState::_ALPHABLEND))
		{
			_SetRenderState( D3DRS_BLENDOP, (ulong)rState.GetAlphaBlendOp() );
			_SetRenderState( D3DRS_SRCBLEND, (ulong)rState.GetAlphaSrcBlend() );
			_SetRenderState( D3DRS_DESTBLEND, (ulong)rState.GetAlphaDstBlend() );
		}
	}

	//----------------------------------------------------------------
	/** Set RenderState
	    @brief      RenderState 설정
		@param        eState	: 설정할 랜더스테이트
		@param		  ulValue	: 값
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::_SetRenderState( D3DRENDERSTATETYPE eState, ulong ulValue )
	{
		ulong old;
		m_pCurrentRenderWindow->GetD3DDevice()->GetRenderState( eState, &old );
		if (old == ulValue)
			return;

		HRESULT hr = m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( eState, ulValue );
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::SetRenderState] Failed 'Set RenderState' " );
	}

	//----------------------------------------------------------------
	/** Set RenderState
	    @brief      RenderState 설정
		@param		rRenderState 설정
		@desc		value changes들은 상대적으로 싸다.
					따라서, 값의 변경은 유연하게 처리한다.
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetRenderState( const CnRenderState& rState )
	{
		//return;

		SetRenderState( rState.GetFlags() );

		// AlphaTest
		if (rState.IsFlags(CnRenderState::_ALPHATEST))
		{
			_SetRenderState( D3DRS_ALPHAFUNC, (ulong)rState.GetAlphaFunc() );
			_SetRenderState( D3DRS_ALPHAREF, (ulong)rState.GetAlphaRef() );
		}

		// AlphaBlend..
		if (rState.IsFlags(CnRenderState::_ALPHABLEND))
		{
			_SetRenderState( D3DRS_BLENDOP, (ulong)rState.GetAlphaBlendOp() );
			_SetRenderState( D3DRS_SRCBLEND, (ulong)rState.GetAlphaSrcBlend() );
			_SetRenderState( D3DRS_DESTBLEND, (ulong)rState.GetAlphaDstBlend() );
		}

		// Scissor
		if (rState.IsFlags(CnRenderState::_SCISSORTEST))
		{
			RECT rect = rState.GetScissorTestRect();
			GetCurrentDevice()->SetScissorRect( &rect );
		}

		// TextureFactor
		_SetRenderState( D3DRS_TEXTUREFACTOR, rState.GetTFactor() );

		// AlphaTest
		if (rState.GetAlphaTestEnable())
		{
			_SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
			_SetRenderState( D3DRS_ALPHAFUNC, (ulong)rState.GetAlphaFunc() );
			_SetRenderState( D3DRS_ALPHAREF, (ulong)rState.GetAlphaRef() );
		}
		else
		{
			_SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
		}

		//// AlphaBlend..
		//if (rState.GetAlphaBlendEnable())
		//{
		//	_SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
		//	_SetRenderState( D3DRS_BLENDOP, (ulong)rState.GetAlphaBlendOp() );
		//	_SetRenderState( D3DRS_SRCBLEND, (ulong)rState.GetAlphaSrcBlend() );
		//	_SetRenderState( D3DRS_DESTBLEND, (ulong)rState.GetAlphaDstBlend() );
		//}
		//else
		//{
		//	_SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
		//}

		//_SetRenderState( D3DRS_CULLMODE, (ulong)rState.GetCullMode() );
		//_SetRenderState( D3DRS_FILLMODE, (ulong)rState.GetFillMode() );
		//_SetRenderState( D3DRS_ZWRITEENABLE, (ulong)rState.GetZBufferMode() );
		//_SetRenderState( D3DRS_LIGHTING, (ulong)rState.GetLightEnable() );
		//_SetRenderState( D3DRS_ZENABLE, (ulong)rState.GetZBufferEnable() );
		//_SetRenderState( D3DRS_TEXTUREFACTOR, rState.GetTFactor() );
		//_SetRenderState( D3DRS_SCISSORTESTENABLE, rState.GetScissorTestEnable() );
		//if (rState.GetScissorTestEnable())
		//{
		//	RECT rect = rState.GetScissorTestRect();
		//	GetCurrentDevice()->SetScissorRect( &rect );
		//}
	}

	//----------------------------------------------------------------
	/** Set RenderState
	    @brief      RenderState 설정
		@param		BitFlags
		@desc		Functional changes들은 상대적으로 비싸다.
					따라서, 기능적인 변경은 최소화 한다.
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetRenderState( ushort StateFlags )
	{
		ushort changeBits = StateFlags ^ m_usRenderStateFlags;
		if (changeBits == 0)
			return;

		m_usRenderStateFlags = StateFlags;

		// CULLMODE
		if (changeBits&(CnRenderState::_CULL_CCW|CnRenderState::_CULL_NONE))
		{
			if ( (StateFlags&(CnRenderState::_CULL_CCW|CnRenderState::_CULL_NONE)) == CnRenderState::_CULL_CCW )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
			}
		}

		// FILLMODE
		if (changeBits&(CnRenderState::_FILL_SOLID|CnRenderState::_FILL_WIRE))
		{
			if ( (StateFlags&(CnRenderState::_FILL_SOLID|CnRenderState::_FILL_WIRE)) == CnRenderState::_FILL_WIRE )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );				
			}
		}

		// ALPHATEST
		if (changeBits&(CnRenderState::_ALPHATEST|CnRenderState::_NO_ALPHATEST))
		{
			if ( (StateFlags&(CnRenderState::_ALPHATEST|CnRenderState::_NO_ALPHATEST)) == CnRenderState::_ALPHATEST )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
			}
		}

		// ALPHABLEND
		if (changeBits&(CnRenderState::_NO_ALPHABLEND|CnRenderState::_ALPHABLEND))
		{
			if ( (StateFlags&(CnRenderState::_NO_ALPHABLEND|CnRenderState::_ALPHABLEND)) == CnRenderState::_ALPHABLEND )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
			}
		}

		// ZENABLE
		if (changeBits&(CnRenderState::_Z_ENABLE|CnRenderState::_Z_DISABLE))
		{
			if ( (StateFlags&(CnRenderState::_Z_ENABLE|CnRenderState::_Z_DISABLE)) == CnRenderState::_Z_ENABLE )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ZENABLE, TRUE );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ZENABLE, FALSE );
			}
		}

		// ZWRITE
		if (changeBits&(CnRenderState::_ZWRITE_ENABLE|CnRenderState::_ZWRITE_DISABLE))
		{
			if ( (StateFlags&(CnRenderState::_ZWRITE_ENABLE|CnRenderState::_ZWRITE_DISABLE)) == CnRenderState::_ZWRITE_ENABLE )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
			}
		}

		// FOGENABLE
		if (changeBits&(CnRenderState::_FOG_DISABLE|CnRenderState::_FOG_ENABLE))
		{
			if ( (StateFlags&(CnRenderState::_FOG_DISABLE|CnRenderState::_FOG_ENABLE)) == CnRenderState::_FOG_ENABLE )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_FOGENABLE, TRUE );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_FOGENABLE, FALSE );
			}
		}

		// SCISSORTESTENABLE
		if (changeBits&(CnRenderState::_SCISSORTEST|CnRenderState::_NO_SCISSORTEST))
		{
			if ( (StateFlags&(CnRenderState::_SCISSORTEST|CnRenderState::_NO_SCISSORTEST)) == CnRenderState::_SCISSORTEST )
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_SCISSORTESTENABLE, TRUE );
			}
			else
			{
				m_pCurrentRenderWindow->GetD3DDevice()->SetRenderState( D3DRS_SCISSORTESTENABLE, FALSE );
			}
		}
	}

	//----------------------------------------------------------------
	/** Set Material State
	    @brief      Material State 설정
		@param        eState	: 설정할 매터리얼
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetMaterialState( const CnMaterialState& rState )
	{
		static D3DMATERIAL9 material;
		ZeroMemory( &material, sizeof(material) );

		CnColor color = rState.GetAmbient();
		material.Ambient.r = color.r;
		material.Ambient.g = color.g;
		material.Ambient.b = color.b;
		material.Ambient.a = 1.0f;

		color = rState.GetDiffuse();
		material.Diffuse.r = color.r;
		material.Diffuse.g = color.g;
		material.Diffuse.b = color.b;
		material.Diffuse.a = color.a;

		color = rState.GetSpecular();
		material.Specular.r = color.r;
		material.Specular.g = color.g;
		material.Specular.b = color.b;
		material.Specular.a = 1.0f;

		color = rState.GetEmissive();
		material.Emissive.r = color.r;
		material.Emissive.g = color.g;
		material.Emissive.b = color.b;
		material.Emissive.a = 1.0f;

		material.Power = rState.GetShininess();

		this->GetCurrentDevice()->SetMaterial( &material );
	}

	//----------------------------------------------------------------
	/** Set Texture State
	    @brief	  텍스쳐 상태 설정   
		@param        nStage	: 텍스쳐 스테이지
		@param		  rState	: 텍스쳐 클래스
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetTextureState( const CnTextureState& rState )
	{
		const CnTextureState::StageStateList stages = rState.GetStages();
		CnTextureState::StageStateList::const_iterator i, iend;
		iend = stages.end();

		CnTextureState::Stage stage;
		CnTextureState::StageState state;

		for (i = stages.begin(); i != iend; ++i)
		{
			stage = i->first;
			state = (i->second);

			// color blend op
			GetCurrentDevice()->SetTextureStageState( stage, D3DTSS_COLOROP, (ulong)state.color.top );
			GetCurrentDevice()->SetTextureStageState( stage, D3DTSS_COLORARG1, (ulong)state.color.arg1 );
			GetCurrentDevice()->SetTextureStageState( stage, D3DTSS_COLORARG2, (ulong)state.color.arg2 );

			// alphablend op
			GetCurrentDevice()->SetTextureStageState( stage, D3DTSS_ALPHAOP, (ulong)state.alpha.top );
			GetCurrentDevice()->SetTextureStageState( stage, D3DTSS_ALPHAARG1, (ulong)state.alpha.arg1 );
			GetCurrentDevice()->SetTextureStageState( stage, D3DTSS_ALPHAARG2, (ulong)state.alpha.arg2 );

			// address
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_ADDRESSU, (ulong)state.address.u );
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_ADDRESSV, (ulong)state.address.v );
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_ADDRESSW, (ulong)state.address.w );

			// filter
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_MAGFILTER, (ulong)state.filter.mag );
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_MINFILTER, (ulong)state.filter.min );
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_MIPFILTER, (ulong)state.filter.mip );

			// mipmap
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_MAXMIPLEVEL, state.maxmipmaplevel );
			GetCurrentDevice()->SetSamplerState( stage, D3DSAMP_MIPMAPLODBIAS, *((LPDWORD)(&state.mipmaplodbias)));
		}
	}

	//----------------------------------------------------------------
	/** Set Texture
	    @brief	  텍스쳐 설정   
		@param        nStage	: 텍스쳐 스테이지
		@param		  pTexture	: 텍스쳐 클래스
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetTexture( int nStage, CnTexture* pTexture )
	{
		LPDIRECT3DBASETEXTURE9 old = NULL;
		GetCurrentDevice()->GetTexture( (DWORD)nStage, &old );

		LPDIRECT3DBASETEXTURE9 baseTex = NULL;
		pTexture->GetBaseTexture( &baseTex );

		if (baseTex == old)
			return;

		LPDIRECT3DTEXTURE9 tex = NULL;
		pTexture->GetTexture( &tex );

		HRESULT hr = GetCurrentDevice()->SetTexture( (DWORD)nStage, tex );
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::SetTexture] Failed 'Set SetTexture' " );
	}

	//----------------------------------------------------------------
	/** Set Material
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetMaterial( CnMaterial* Material )
	{
		m_Material = Material;

		this->SetActiveShader( Material->GetShader()->GetEffect() );
		//this->SetAlphaBlendMode( *Material->GetRenderState() );
	}

	//----------------------------------------------------------------
	/** Enable Light
	    @brief      라이트 사용 여부
		@param        bEnable : 라이트 사용 여부
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::EnableLight( bool bEnable )
	{
		_SetRenderState( D3DRS_LIGHTING, bEnable );
	}

	//----------------------------------------------------------------
	/** Enable Light
	    @brief      라이트 사용 여부
		@param        bEnable : 라이트 사용 여부
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::EnableSpecular( bool bEnable )
	{
		_SetRenderState( D3DRS_SPECULARENABLE, bEnable );
	}

	//----------------------------------------------------------------
	/** Set Ambient 
	    @brief      주변광 설정
		@param        ulColor : 주변광 색상
		@return		  none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetAmbient( RGBA ulColor )
	{
		_SetRenderState( D3DRS_AMBIENT, ulColor );
	}
	void CnDx9Renderer::GetAmbient( RGBA& rOutput )
	{
		HRESULT hr = m_pCurrentRenderWindow->GetD3DDevice()->GetRenderState( D3DRS_AMBIENT, &rOutput );
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::GetAmbient] Failed 'Get RenderState' " );
	}

	//----------------------------------------------------------------
	/**	@brief	기본 Shape을 생성한다.
	*/
	//----------------------------------------------------------------
	CnShape* CnDx9Renderer::CreateShape( ushort ShapeType )
	{
		return NULL;
	}

	//----------------------------------------------------------------
	/** Create VertexDeclaration
	    @brief      버텍스 정의 클래스 생성
		@param		  none
		@return		  CnVertexDeclaration : 결과.
	*/
	//----------------------------------------------------------------
	CnVertexDeclaration* CnDx9Renderer::CreateVertexDeclaration()
	{
		return CnNew CnDx9VertexDeclaration();
	}

	//----------------------------------------------------------------
	/** Create VertexBuffer
	    @brief      버텍스 버퍼 생성
		@param		  none
		@return		  CnVertexBuffer : 결과
	*/
	//----------------------------------------------------------------
	CnVertexBuffer* CnDx9Renderer::CreateVertexBuffer()
	{
		CnVertexBuffer* newBuffer = CnNew CnDx9VertexBuffer();
		return newBuffer;
	}
	CnVertexBuffer* CnDx9Renderer::CreateVertexBuffer( uint nNumVertices, uint nSizeOfVertex, ulong ulUsage, ulong ulPool )
	{
		CnVertexBuffer* newBuffer = CreateVertexBuffer();
		if (newBuffer->Create( nNumVertices, nSizeOfVertex, ulUsage, ulPool ))
			return newBuffer;

		delete newBuffer;
		return NULL;
	}

	//----------------------------------------------------------------
	/** Create IndexBuffer
	    @brief      인덱스 버퍼 생성
		@param		  none
		@return		  CnIndexBuffer : 결과
	*/
	//----------------------------------------------------------------
	CnIndexBuffer* CnDx9Renderer::CreateIndexBuffer()
	{
		return CnNew CnDx9IndexBuffer();
	}

	//----------------------------------------------------------------
	/** Create IndexBuffer
	    @brief      인덱스 버퍼 생성
		@param		  none
		@return		  CnIndexBuffer : 결과
	*/
	//----------------------------------------------------------------
	CnIndexBuffer* CnDx9Renderer::CreateIndexBuffer( uint nNumIndices, ulong ulUsage, ulong ulFormat, ulong ulPool )
	{
		CnIndexBuffer* newBuffer = this->CreateIndexBuffer();
		if (newBuffer->Create( nNumIndices, ulUsage, ulFormat, ulPool ))
			return newBuffer;

		delete newBuffer;
		return NULL;
	}

	//----------------------------------------------------------------
	/** Set Source Stream
		@brief	버텍스 버퍼를 전송
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::SetStreamSource( const Ptr<CnVertexData>& pVertexData )
	{
		Ptr<CnVertexBuffer> vertexBuffer;
		ushort index = 0;

		const CnVertexBufferBinder::BufferMap bufferList = pVertexData->GetBufferBinder()->GetBuffers();
		CnVertexBufferBinder::BufferMap::const_iterator i, iend;
		iend = bufferList.end();
		for (i=bufferList.begin(); i!=iend; i++, index++)
		{
			vertexBuffer = (i->second);
			if (!this->SetStreamSource( index, 0, vertexBuffer ))
				return false;
		}

		return true;
	}

	//----------------------------------------------------------------
	/** Set Source Stream
		@brief	버텍스 버퍼를 전송
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::SetStreamSource( uint BufferID, uint StreamID, uint OffsetInBytes, const Ptr<CnVertexData>& pVertexData )
	{
		Ptr<CnVertexBuffer> vertexBuffer = pVertexData->GetBuffer( BufferID );
		return this->SetStreamSource( StreamID, OffsetInBytes, vertexBuffer );
	}

	//----------------------------------------------------------------
	/** Set Source Stream
		@brief	버텍스 버퍼를 전송
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::SetStreamSource( uint nStreamID, uint nOffsetInBytes, const Ptr<CnVertexBuffer>& pBuffer )
	{
		LPDIRECT3DDEVICE9 device = GetCurrentDevice();

		LPDIRECT3DVERTEXBUFFER9 vb = 0;
		pBuffer->GetBuffer( &vb );

		HRESULT hr = device->SetStreamSource( nStreamID, vb, nOffsetInBytes, pBuffer->GetSizeOfVertex() );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::SetStreamSource] Failed 'SetStreamSource'" );
#else
		if( FAILED( hr ) )
			return false;
#endif
		return true;
	}

	//----------------------------------------------------------------
	/**
	*/
	bool CnDx9Renderer::SetIndexBuffer( const Ptr<CnIndexBuffer>& IndexBuffer )
	{
		Ptr<CnDx9IndexBuffer> dx9IndexBuffer = IndexBuffer.Cast<CnDx9IndexBuffer>();
		LPDIRECT3DDEVICE9 device = GetCurrentDevice();

		HRESULT hr = device->SetIndices( dx9IndexBuffer->GetDxBuffer() );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::SetIndexBuffer] Failed 'SetIndices'" );
#else
		if( FAILED( hr ) )
			return false;
#endif
		return true;
	}

	//----------------------------------------------------------------
	/** Set FVF
		@brief	버텍스 포멧 설정
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetFVF( ulong ulFVF )
	{
		HRESULT hr = GetCurrentDevice()->SetFVF( ulFVF );
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::SetFVF] Failed 'Set FVF'" );
	}

	//----------------------------------------------------------------
	/** Set Vertex Declaration
	    @brief      버텍스 Declaration을 랜더러에 설정한다.
		@param        pDeclaration : pDeclaration
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetVertexDeclaration( const Cindy::Ptr<CnVertexData>& pVertexData )
	{
		this->SetVertexDeclaration( pVertexData->GetDeclaration() );
	}

	//----------------------------------------------------------------
	/** Set Vertex Declaration
	    @brief      버텍스 Declaration을 랜더러에 설정한다.
		@param        pDeclaration : pDeclaration
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetVertexDeclaration( const Ptr<CnVertexDeclaration>& pDeclaration )
	{
		if (m_LastVertexComponent == pDeclaration->GetComponents())
			return;

		Ptr<CnDx9VertexDeclaration> dx9Decl = pDeclaration.Cast<CnDx9VertexDeclaration>();
		if (dx9Decl.IsNull())
			return;

		IDirect3DVertexDeclaration9* current_decl = dx9Decl->GetDeclaration();
#ifdef _DEBUG
		Assert( current_decl, L"[CnDx9Renderer::SetVertexDeclaration] '!current_decl' " );
#else
		if( !current_decl )
		{
			CnPrint( L"[CnDx9Renderer::SetVertexDeclaration] '!declaration' " );
			return;
		}
#endif
		HRESULT hr = 0;

		//IDirect3DVertexDeclaration9* lastdecl = NULL;
		//hr = GetCurrentDevice()->GetVertexDeclaration( &lastdecl );
		//if (hr && lastdecl == current_decl)
		//{
		//	CnPrint( L"[CnDx9Renderer::SetVertexDeclaration] lastdecl == current_decl" );
		//	return;
		//}

		hr = GetCurrentDevice()->SetVertexDeclaration( current_decl );
		Assert( SUCCEEDED( hr ), L"[CnDx9VertexDeclaration::SetVertexDeclaration] Failed 'SetVertexDeclaration' " );

		m_LastVertexComponent = pDeclaration->GetComponents();
	}

	//----------------------------------------------------------------
	/** @brief	Query 생성
		@return 실패 시 NULL을 반환
	*/
	//----------------------------------------------------------------
	CnQuery* CnDx9Renderer::CreateQuery( ushort QueryType )
	{
		CnDx9Query* query = CnNew CnDx9Query();
		if (query->Create( (CnQuery::QUERYTYPE)QueryType ))
			return query;

		delete query;
		return NULL;
	}

	//----------------------------------------------------------------
	/** _CreateEffectPool
	    @brief	  Shader Effect Pool 생성
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::_CreateEffectPool()
	{
		if (m_pEffectPool)
			return;

		// create effect pool
		HRESULT hr = D3DXCreateEffectPool( &m_pEffectPool );
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::_CreateEffectPool] Failed 'D3DXCreateEffectPool' " );
	}

	//----------------------------------------------------------------
	/** Set Shader
	*/
	//----------------------------------------------------------------
	void CnDx9Renderer::SetActiveShader( CnShaderEffect* pEffect )
	{
		m_pShaderEffect = pEffect;
	}
	CnShaderEffect* CnDx9Renderer::GetActiveShader() const
	{
		return m_pShaderEffect;
	}

	//----------------------------------------------------------------
	/**	@brief	Shader Effect 생성
	*/
	//----------------------------------------------------------------
	CnShaderEffect* CnDx9Renderer::CreateEffect()
	{
		return CnNew CnDx9ShaderEffect();
	}

	//----------------------------------------------------------------
	/** DrawPrimitive
	    @brief      Primitive(도형)을 랜더링한다.
		@param		  ePrimitive			: 그릴 도형 타입
		@param        nIndexOfStartVertex	: 그릴 버텍스 버퍼의 시작할 버텍스 번호
		@param		  nNumPrimitiveCount	: 그릴 도형의 개수
		@return		  true / false
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::DrawPrimitive( IRenderer::PRIMITIVE ePrimitive,
									   uint nIndexOfStartVertex,
									   uint nNumPrimitiveCount )
	{
		LPDIRECT3DDEVICE9 device = GetCurrentDevice();
		Assert( device, L"[CnDx9Renderer::DrawPrimitive] Failed 'CurrentDevice Is Null' " );

		if (m_pShaderEffect)
			m_pShaderEffect->CommitChanges();

		HRESULT hr = device->DrawPrimitive( (D3DPRIMITIVETYPE)ePrimitive, nIndexOfStartVertex, nNumPrimitiveCount );
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::DrawPrimitive] Failed 'DrawPrimitive' " );

		return true;
	}

	//----------------------------------------------------------------
	/** DrawIndexedPrimitive
	    @brief      인덱스를 이용해서 Primitive(도형)을 랜더링한다.
		@param		  ePrimitive			: 그릴 도형 타입
		@param		  nNumVertices			: 버텍스 개수
		@param		  nNumPrimitiveCount	: 그릴 도형의 개수
		@param        nBaseVertexIndex		: 버텍스 버퍼의 시작에서 첫번째 버텍스까지의 Offset
		@param		  nMinIndex				: 최소 버텍스 인덱스
		@param		  nIndexOfStartVertex	: 그릴 버텍스 버퍼의 시작할 버텍스 인덱스
		@return		  true / false
	*/
	//----------------------------------------------------------------
	bool CnDx9Renderer::DrawIndexedPrimitive( PRIMITIVE ePrimitive,
											  uint nNumVertices,
											  uint nNumPrimitiveCount,
											  int nBaseVertexIndex,
											  uint nMinIndex,
											  uint nIndexOfStartVertex )
	{
		LPDIRECT3DDEVICE9 device = GetCurrentDevice();
		Assert( device, L"[CnDx9Renderer::DrawIndexedPrimitive] Failed 'CurrentDevice Is Null' " );

		if (m_pShaderEffect)
			m_pShaderEffect->CommitChanges();

		HRESULT hr = device->DrawIndexedPrimitive( (D3DPRIMITIVETYPE)ePrimitive, nBaseVertexIndex,
												   nMinIndex, nNumVertices,
												   nIndexOfStartVertex, nNumPrimitiveCount );

#ifdef _DEBUG
		if (FAILED(hr))
		{
			const TCHAR* tchar	= DXGetErrorString( hr );
			CnError( tchar );
		}
#endif
		Assert( SUCCEEDED( hr ), L"[CnDx9Renderer::DrawIndexedPrimitive] Failed 'DrawIndexedPrimitive' " );
		return true;
	}

	//****************************************************************************
	//	Driver 초기화
	//****************************************************************************
	void CnDx9Renderer::_InitDrivers()
	{
		if (!m_pDriverList)
		{
			m_pDriverList = new CnDx9DriverList( m_pD3D );

			m_pCurrentDriver = m_pDriverList->GetDriver( D3DADAPTER_DEFAULT );
		}
	}

	//--------------------------------------------------------------
	void CnDx9Renderer::_InitCapabilities( CnDx9RenderWindow* pRenderTarget )
	{
		using namespace MoCommon;

		SAFEDEL( m_pDriverCaps );
		m_pDriverCaps = new CnDx9DriverCaps();

		CnPrint( L"" );
		CnPrint( L"===================================================================" );
		CnPrint( L"Begin check hardware capacity" );
		CnPrint( L"===================================================================" );		

		D3DADAPTER_IDENTIFIER9 identifier = m_pCurrentDriver->GetAdapterIdentifier();

		CnPrint( L"Graphic Card Count: %d", m_pDriverList->GetCount() );
		CnPrint( L"" );
		CnPrint( L"----------------------Current Driver Info--------------------------" );
		CnPrint( L"Description : %S", identifier.Description );
		CnPrint( L"Product : %d", HIWORD(identifier.DriverVersion.HighPart) );
		CnPrint( L"Version : %d", LOWORD(identifier.DriverVersion.HighPart) );
		CnPrint( L"SubVersion : %d", HIWORD(identifier.DriverVersion.LowPart) );
		CnPrint( L"Build : %d", LOWORD(identifier.DriverVersion.LowPart) );
		CnPrint( L"-------------------------------------------------------------------" );
		CnPrint( L"" );

		m_pD3D->GetDeviceCaps( m_pCurrentDriver->GetIndex(), CINDY_DIRECT3D_DEVICETYPE, &m_d3dCaps );

		LPDIRECT3DSURFACE9 pSurf;
		D3DSURFACE_DESC desc;
		pRenderTarget->GetD3DDevice()->GetDepthStencilSurface( &pSurf );
		pSurf->GetDesc( &desc );
		pSurf->Release();

		if( desc.Format == D3DFMT_D24S8 || desc.Format == D3DFMT_D24X8 )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_HWSTENCIL );
			m_pDriverCaps->SetStencilBufferBitDepth( 8 );		// Actually, it's always 8-bit
			CnPrint( L"HARDWARE_STENCIL enabled" );
		}

		// max lights
		m_pDriverCaps->SetMaxLight( (ushort)m_d3dCaps.MaxActiveLights );
		CnPrint( L"MAX_ACTIVE_LIGHTS: %d enabled", m_d3dCaps.MaxActiveLights );

		// max textures
		m_pDriverCaps->SetMaxTexture( (ushort)m_d3dCaps.MaxSimultaneousTextures );
		CnPrint( L"MAX_SIMULTANEOUS_TEXTURES: %d enabled", m_d3dCaps.MaxSimultaneousTextures );

		if( m_d3dCaps.MaxAnisotropy > 1 )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_ANISOTROPY );
			CnPrint( L"ANISOTROPY_TEXTURE enabled" );
		}

		if( m_d3dCaps.Caps2 & D3DCAPS2_CANAUTOGENMIPMAP )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_AUTOMIPMAP );
			CnPrint( L"AUTO_MIPMAPPING enabled" );
		}

		m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_BLENDING );
		CnPrint( L"HARDWARE_BLENDING enabled" );

		if( m_d3dCaps.TextureOpCaps & D3DTEXOPCAPS_DOTPRODUCT3 )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_DOT3 );
			CnPrint( L"TEXTURE_DOT_PRODUCT3 enabled" );
		}

		if( m_d3dCaps.TextureCaps & D3DPTEXTURECAPS_CUBEMAP )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_CUBEMAPPING );
			CnPrint( L"CUBE_MAPPING enabled" );
		}

		m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_TEXTURE_COMPRESSION );
		m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_TEXTURE_COMPRESSION_DXT );
		CnPrint( L"DXT_TEXTURE_COMPRESSION enabled" );

		m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_VBO );
		CnPrint( L"VERTEX_BUFFER_OBJECT enabled" );

		if( m_d3dCaps.RasterCaps & D3DPRASTERCAPS_SCISSORTEST )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_SCISSOR_TEST );
			CnPrint( L"SCISSOR_TEST enabled" );
		}

		if( m_d3dCaps.StencilCaps & D3DSTENCILCAPS_TWOSIDED )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_TWO_SIDED_STENCIL );
			CnPrint( L"TWO_SIDE_STENCIL enabled" );
		}

		if( ( m_d3dCaps.StencilCaps & D3DSTENCILCAPS_INCR ) && ( m_d3dCaps.StencilCaps & D3DSTENCILCAPS_DECR ) )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_STENCIL_WRAP );
			CnPrint( L"STENCIL_WRAP enabled" );
		}

		// Check for hardware occlusion support
		if( SUCCEEDED( pRenderTarget->GetD3DDevice()->CreateQuery( D3DQUERYTYPE_OCCLUSION, NULL ) ) )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_HWOCCLUSION );
			CnPrint( L"HARDWARE_OCCLUSION enabled" );
		}

		if( m_d3dCaps.MaxUserClipPlanes > 0 )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_USER_CLIP_PLANES );
			CnPrint( L"USER_CLIP_PLANES enabled" );
		}

		if( m_d3dCaps.DeclTypes & D3DDTCAPS_UBYTE4 )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_VERTEX_FORMAT_UBYTE4 );
			CnPrint( L"VERTEX_UBYTE4 enabled" );
		}

		//if( m_d3dCaps.Caps2 & D3DCAPS2_CANCALIBRATEGAMMA )
		//	CnPrint( L"Calibrate Gamma" );

		if( m_d3dCaps.Caps2 & D3DCAPS2_FULLSCREENGAMMA )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_FULLSCREEN_GAMMA );
			CnPrint( L"FullScreen Gamma" );
		}

		// 3D textures?
		if( m_d3dCaps.TextureCaps & D3DPTEXTURECAPS_VOLUMEMAP )
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_TEXTURE_3D );
			CnPrint( L"Support 3D Texture" );
		}
		
        // non-power-of-two texturs always supported
        m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_NON_POWER_OF_2_TEXTURES );

		// We always support rendertextures bigger than the frame buffer
        m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_HWRENDER_TO_TEXTURE );

		// Multiple Render Targets
		CnPrint( L"\n\t[Multiple Render Targets]" );

		ushort numMRTs = (ushort)m_d3dCaps.NumSimultaneousRTs;
		m_pDriverCaps->SetMaxMRTs( numMRTs );
		CnPrint( L"MAX_MRTs: %d enabled", numMRTs );

		if (m_d3dCaps.PrimitiveMiscCaps & D3DPMISCCAPS_MRTINDEPENDENTBITDEPTHS)
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_MRTINDEPENDENTBITDEPTHS );
			CnPrint( L"Support MRT Independent Bit Depth" );
		}
		if (m_d3dCaps.PrimitiveMiscCaps & D3DPMISCCAPS_MRTPOSTPIXELSHADERBLENDING)
		{
			m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_MRTPOSTPIXELSHADERBLENDING );
			CnPrint( L"Support MRT Post PixelShader Blending" );
		}

		//--------------------------------------------------------------
		// support shader 
		//--------------------------------------------------------------
		_InitVertexShaderCaps();
		_InitPixelShaderCaps();
		
		// Infinite projection?
		// We have no capability for this, so we have to base this on our
		// experience and reports from users
		// Non-vertex program capable hardware does not appear to support it
		if ( m_pDriverCaps->HasCapability( CnDx9DriverCaps::CAPS_VERTEX_SHADER ) )
		{
			// GeForce4 Ti (and presumably GeForce3) does not
			// render infinite projection properly, even though it does in GL
            // So exclude all cards prior to the FX range from doing infinite
            D3DADAPTER_IDENTIFIER9 adapterID = m_pCurrentDriver->GetAdapterIdentifier();
			if (adapterID.VendorId != 0x10DE || // not nVidia
                adapterID.DeviceId >= 0x0301) // or GeForce FX or above
			{
				m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_INFINITE_FAR_PLANE );
			}
		}

		CnString vertexshaderversion, pixelshaderversion;
		m_pDriverCaps->GetVertexShaderVersion( vertexshaderversion );
		m_pDriverCaps->GetPixelShaderVersion( pixelshaderversion );

		CnPrint( L"" );
		CnPrint( L"===================================================================" );
		CnPrint( L"Ending Hardware capacity check" );
		CnPrint( L"===================================================================" );
	}

	//--------------------------------------------------------------
	void CnDx9Renderer::_InitVertexShaderCaps()
	{
		ushort usMajor, usMinor;
		usMajor = static_cast<ushort>( ( m_d3dCaps.VertexShaderVersion & 0x0000FF00 ) >> 8 );
		usMinor = static_cast<ushort>( m_d3dCaps.VertexShaderVersion & 0x000000FF );		

		m_pDriverCaps->SetVertexShaderVersion( m_d3dCaps.VertexShaderVersion );

		CnPrint( L"" );
		CnPrint( L"================================================" );
		CnPrint( L"Checking Vertex Shader" );
		CnPrint( L"================================================" );		

		switch( usMajor )
		{
		case 1:
			m_pDriverCaps->SetVertexShaderVersion( L"vs_1_1" );
			m_pDriverCaps->SetVertexShaderBoolRegisterCount( 0 );
			m_pDriverCaps->SetVertexShaderIntRegisterCount( 0 );
			m_pDriverCaps->SetVertexShaderFloatRegisterCount( (ushort)m_d3dCaps.MaxVertexShaderConst );
			break;
		case 2:
			if( usMinor > 0 )
			{
				m_pDriverCaps->SetVertexShaderVersion( L"vs_2_x" );
			}
			else
			{
				m_pDriverCaps->SetVertexShaderVersion( L"vs_2_0" );
			}

			m_pDriverCaps->SetVertexShaderBoolRegisterCount( 16 );
			m_pDriverCaps->SetVertexShaderIntRegisterCount( 16 );
			m_pDriverCaps->SetVertexShaderFloatRegisterCount( (ushort)m_d3dCaps.MaxVertexShaderConst );
			break;
		case 3:
			m_pDriverCaps->SetVertexShaderVersion( L"vs_3_0" );
			m_pDriverCaps->SetVertexShaderBoolRegisterCount( 16 );
			m_pDriverCaps->SetVertexShaderIntRegisterCount( 16 );
			m_pDriverCaps->SetVertexShaderFloatRegisterCount( (ushort)m_d3dCaps.MaxVertexShaderConst );
			break;
		default:
			m_pDriverCaps->SetVertexShaderVersion( L"" );
			CnPrint( L"Can't use Vertex Shader" );
			break;
		}

		// TODO : VS Manager code

		switch( usMajor )
		{
		case 3:			
		case 2:
		case 1:
			{
				m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_VERTEX_SHADER );

				CnString version;
				m_pDriverCaps->GetVertexShaderVersion( version );
				CnPrint( L"Vertex Shader Version : %s", version.c_str() );
				CnPrint( L"Usable Constant Boolean Count : %d", m_pDriverCaps->GetVertexShaderBoolRegisterCount() );
				CnPrint( L"Usable Constant Int Count : %d", m_pDriverCaps->GetVertexShaderIntRegisterCount() );
				CnPrint( L"Usable Constant Float Count : %d", (ushort)m_d3dCaps.MaxVertexShaderConst );
			}
			break;
		}
	}

	//--------------------------------------------------------------
	void CnDx9Renderer::_InitPixelShaderCaps()
	{
		ushort usMajor, usMinor;
		usMajor = static_cast<ushort> ( ( m_d3dCaps.PixelShaderVersion & 0x0000FF00 ) >> 8 );
		usMinor = static_cast<ushort> ( m_d3dCaps.PixelShaderVersion & 0x000000FF );

		m_pDriverCaps->SetPixelShaderVersion( m_d3dCaps.PixelShaderVersion );

		CnPrint( L"================================================" );
		CnPrint( L"Checking Pixel Shader" );
		CnPrint( L"================================================" );		
		switch( usMajor )
		{
		case 1:
			switch( usMinor )
			{
			case 1:
				m_pDriverCaps->SetPixelShaderVersion( L"ps_1_1" );
				break;
			case 2:
				m_pDriverCaps->SetPixelShaderVersion( L"ps_1_2" );
				break;
			case 3:
				m_pDriverCaps->SetPixelShaderVersion( L"ps_1_3" );
				break;
			case 4:
				m_pDriverCaps->SetPixelShaderVersion( L"ps_1_4" );
				break;
			}
			m_pDriverCaps->SetPixelShaderBoolRegisterCount( 0 );
			m_pDriverCaps->SetPixelShaderIntRegisterCount( 0 );
			m_pDriverCaps->SetPixelShaderFloatRegisterCount( 8 );
			break;
		case 2:
			if( usMinor > 0 )
			{
				m_pDriverCaps->SetPixelShaderVersion( L"ps_2_X" );
				m_pDriverCaps->SetPixelShaderBoolRegisterCount( 16 );
				m_pDriverCaps->SetPixelShaderIntRegisterCount( 16 );
				m_pDriverCaps->SetPixelShaderFloatRegisterCount( 224 );
			}
			else
			{
				m_pDriverCaps->SetPixelShaderVersion( L"ps_2_0" );
				m_pDriverCaps->SetPixelShaderBoolRegisterCount( 0 );
				m_pDriverCaps->SetPixelShaderIntRegisterCount( 0 );
				m_pDriverCaps->SetPixelShaderFloatRegisterCount( 32 );
			}
			break;
		case 3:
			if( usMinor > 0 )
			{
				m_pDriverCaps->SetPixelShaderVersion( L"ps_3_X" );				
			}
			else
			{
				m_pDriverCaps->SetPixelShaderVersion( L"ps_3_0" );
			}
			m_pDriverCaps->SetPixelShaderBoolRegisterCount( 16 );
			m_pDriverCaps->SetPixelShaderIntRegisterCount( 16 );
			m_pDriverCaps->SetPixelShaderFloatRegisterCount( 224 );
			break;
		default:
			m_pDriverCaps->SetPixelShaderVersion( L"" );
			CnPrint( L"Can't use Pixel Shader" );
			break;
		}

		// TODO : Pixle Shader Mgr
		switch( usMajor )
		{
		case 3:
		case 2:
		case 1:
			{
				m_pDriverCaps->SetCapability( CnDx9DriverCaps::CAPS_PIXEL_SHADER );

				CnString version;
				m_pDriverCaps->GetPixelShaderVersion( version );
				CnPrint( L"Pixel Shader Version : %s", version.c_str() );
				CnPrint( L"Usable Constant Boolean Count : %d", m_pDriverCaps->GetPixelShaderBoolRegisterCount() );
				CnPrint( L"Usable Constant Int Count : %d", m_pDriverCaps->GetPixelShaderIntRegisterCount() );
				CnPrint( L"Usable Constant Float Count : %d", m_pDriverCaps->GetPixelShaderFloatRegisterCount() );
			}
			break;
		}
	}
}