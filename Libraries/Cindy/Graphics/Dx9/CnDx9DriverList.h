// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNDX9DRIVERLIST_H__
#define __CNDX9DRIVERLIST_H__

#include "CnDx9Driver.h"

namespace Cindy
{
	//==================================================================
	/** CnDx9DriverList
		@author
			cagetu
		@since
			2006년 10월 1일
		@remarks
			그래픽 카드의 정보 목록
		@see
			Orge3D - D3DDriverList class
	*/
	//==================================================================
	class CnDx9DriverList
	{
		typedef std::vector<CnDx9Driver>	DriverVector;
		typedef DriverVector::iterator		DriverIter;
	public:
		// 생성자 / 소멸자
		CnDx9DriverList( LPDIRECT3D9 pD3D );
		~CnDx9DriverList();

		/** 드라이버 개수
		*/
		ushort				GetCount();

		/** 드라이버 정보를 얻어온다.
		*/
		CnDx9Driver*		GetDriver( ushort usIndex );
		CnDx9Driver*		GetDriver( const CnString& strDesc );

	private:
		LPDIRECT3D9		m_pD3D;
		DriverVector	m_Drivers;

		/** 그래픽 카드 목록을 갱신한다.
		*/
		void		_Enumerate();
	};
}

#endif	// __CNDX9DRIVERLIST_H__