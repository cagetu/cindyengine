//================================================================
// File:           : CnTextRenderer.h
// Original Author : changhee
// Creation Date   : 2009. 9. 3
//================================================================
#ifndef __CN_DX9_TEXT_RENDERER_H__
#define __CN_DX9_TEXT_RENDERER_H__

#include "Graphics/CnTextRenderer.h"

namespace Cindy
{
	//================================================================
	/** DirectX9 Text Renderer Class
	    @author    changhee
		@brief	   Dx9 Text ������
	*/
	//================================================================
	class CnDx9TextRenderer : public CnTextRenderer
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------------------
		// Methods
		//------------------------------------------------------------------
		CnDx9TextRenderer();
		virtual ~CnDx9TextRenderer();

		void Open() override;
		void Close() override;

		void DrawText( CnCamera* camera ) override;
	private:
		ID3DXFont*	m_D3DFont;
		ID3DXSprite* m_D3DSprite;
	};
} // namespace Cindy

#endif // __CN_DX9_TEXT_RENDERER_H__
