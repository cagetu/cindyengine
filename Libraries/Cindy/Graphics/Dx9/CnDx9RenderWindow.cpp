//================================================================
// File:               : CnDx9RenderWindow.cpp
// Related Header File : CnDx9RenderWindow.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 19
//================================================================
#include "Cindy.h"
#include "Material/CnTextureManager.h"
#include "Util/CnLog.h"

#include "../CindyRenderConfig.h"

#include "CnDx9RenderWindow.h"
#include "CnDx9Renderer.h"
#include "CnDx9Driver.h"
#include "CnDx9RenderTexture.h"
#include "CnDx9Texture.h"

namespace Cindy
{
	//============================================================================
	__ImplementRtti( Cindy, CnDx9RenderWindow, CnRenderWindow );
	/// Const/Dest
	CnDx9RenderWindow::CnDx9RenderWindow( HWND hWnd, const wchar* strName, int nWidth, int nHeight, CnDx9Renderer* pRenderer )
		: CnRenderWindow( hWnd, strName, nWidth, nHeight )
		, m_pRenderer( pRenderer )
		, m_pDevice( NULL )
		, m_bLostDevice( false )
		, m_pRenderZBuffer( 0 )
	{
	}
	CnDx9RenderWindow::~CnDx9RenderWindow()
	{
		_Destroy();
	}

	//----------------------------------------------------------------
	/** _Destroy
	    @brief	  모든 상태를 해제 한다.      
		@param        none
		@return       none
	*/
	void CnDx9RenderWindow::_Destroy()
	{
		OnRelease();

		SAFEREL( m_pDevice );
	}

	//----------------------------------------------------------------
	/** Release
	    @brief	  D3D Device 해제
		@param        none
		@return       none
	*/
	void CnDx9RenderWindow::OnRelease()
	{
		SAFEREL( m_pRenderSurface );
		SAFEREL( m_pRenderZBuffer );

		m_bIsActive = false;
	}

	//----------------------------------------------------------------
	/** Restore
	    @brief	  D3D Device 복구
		@param        none
		@return       none
	*/
	void CnDx9RenderWindow::OnRestore()
	{
		m_pDevice->GetRenderTarget( 0, &m_pRenderSurface );

		if( m_pRenderSurface )
		{
			D3DSURFACE_DESC d3dSurfDesc;
			HRESULT hr = m_pRenderSurface->GetDesc( &d3dSurfDesc );
			Assert( SUCCEEDED( hr ), L"[CnDx9RenderWindow::Restore] Failed 'Restore' " );
		}

		m_pDevice->GetDepthStencilSurface( &m_pRenderZBuffer );

		m_bIsActive = true;
	}

	//----------------------------------------------------------------
	/** Reset
	    @brief	  리셋
		@param        none
		@return       none
	*/
	void CnDx9RenderWindow::OnReset()
	{
		HRESULT hr = GetD3DDevice()->Reset( &m_d3dPP );
		Assert( SUCCEEDED( hr ), L"[CnDx9RenderWindow::Reset] Failed 'Reset Device' " );
	}

	//----------------------------------------------------------------
	/** _Create
	    @brief	  D3D Device 윈도우에 세팅
	*/
	bool CnDx9RenderWindow::_Create( CnDx9Driver* pDriver, ushort usColorDepth, ushort usRefreshRate,
									bool bFullScreen, bool bThreadSafe )
	{
		m_usRefreshRate = usRefreshRate;
		m_bFullScreen = bFullScreen;

		if( m_bFullScreen )
		{
			m_usColorDepth = usColorDepth;
		}
		else
		{
			HDC hDC = GetDC( GetHWnd() );
			m_usColorDepth = GetDeviceCaps( hDC, BITSPIXEL );
			ReleaseDC( GetHWnd(), hDC );
		}

		if( !_CreateDevice( pDriver, bThreadSafe ) )
			return false;

		m_bIsActive = true;

		return true;
	}

	//----------------------------------------------------------------
	/** _Create
	    @brief	  실제 Device 생성..
	*/
	bool CnDx9RenderWindow::_CreateDevice( CnDx9Driver* pDriver, bool bThreadSafe )
	{
		if( !pDriver )
		{
			CnError( L"[CnDx9RenderWindow::_CreateDevice] Can't find Dx9Driver" );
			return false;
		}

		_InitPresentParameter( pDriver );

		//******************************************************************
		//	NVDIA HVPerfHUD를 사용하기 위한 세팅
		//	NVDIA HVPerfHUD가 없다면, REF 모드로 실행 하도록 만들어 준다.
		//	NVDIA HVPerfHUD가 있다면, REF 모드라고 HAL로 실행된다.
		//******************************************************************

		D3DDEVTYPE devType = CINDY_DIRECT3D_DEVICETYPE;	//!< Support only HAL mode
		ushort activeDriver = pDriver->GetIndex();		//!< 현재 장착한 드라이버 아뒤

//#define _PROFILE_NVPERFHUD
#ifdef _PROFILE_NVPERFHUD
		for( ushort adapter = 0; adapter < pDriver->GetD3D()->GetAdapterCount(); ++adapter )
		{
			D3DADAPTER_IDENTIFIER9 Identifier;
			HRESULT Res = pDriver->GetD3D()->GetAdapterIdentifier( adapter, 0, &Identifier );
			if( strcmp( Identifier.Description, "NVIDIA NVPerfHUD" ) == 0 )
			{
				activeDriver = adapter;
				devType = D3DDEVTYPE_REF;
				CnPrint( L"** [NVIDIA NVPerfHUD] Profiling **" );
				break;
			}
		}
#endif

		//******************************************************************
		// Thread-Safe를 위해 D3DCREATE_MULTITHREADED 옵션을 주고 디바이스를 생성.
		// 옵션을 주었는데도 Thread 관련 문제가 발생한다면, Device에 접근할 때를 
		// CriticalSection으로 동기화 처리해 버리는 방법도 생각해 봐야 할 듯.
		//******************************************************************
		HRESULT hr = 0;
		if( bThreadSafe )
		{
			CnPrint( L"D3D MULTI_THREAD SUPPORT : [D3DCREATE_MULTITHREADED]" );
			CnPrint( L"Creating Direct3DDevice : HARDWARE_VERTEX_PROCESSING" );

#ifdef _DEBUG_SHADER
			ulong mode = D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED;
#else
			ulong mode = D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED;
#endif

			hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, GetHWnd(), mode, &m_d3dPP, &m_pDevice );

			if( FAILED( hr ) )
			{
				CnPrint( L"Failed to create Direct3DDevice. MODE : HARDWARE_VERTEX_PROCESSING" );
				CnPrint( L"Creating Direct3DDevice : MIXED_VERTEX_PROCESSING" );

				hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, GetHWnd(), D3DCREATE_MIXED_VERTEXPROCESSING | D3DCREATE_MULTITHREADED, &m_d3dPP, &m_pDevice );
				if( FAILED( hr ) )
				{
					CnPrint( L"Failed to create Direct3DDevice. MODE : MIXED_VERTEX_PROCESSING" );
					CnPrint( L"Creating Direct3DDevice : SOFRWARE_VERTEX_PROCESSING" );

					hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, GetHWnd(), D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED, &m_d3dPP, &m_pDevice );
				}
			}
		}
		else
		{
			CnPrint( L"D3D MULTI_THREAD SUPPORT : NONE'" );
			CnPrint( L"Creating Direct3DDevice : HARDWARE_VERTEX_PROCESSING" );

			hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, GetHWnd(), 
												  D3DCREATE_HARDWARE_VERTEXPROCESSING, 
												  &m_d3dPP, &m_pDevice );

			if( FAILED( hr ) )
			{
				CnPrint( L"Failed to create Direct3DDevice. MODE : HARDWARE_VERTEX_PROCESSING" );
				CnPrint( L"Creating Direct3DDevice : MIXED_VERTEX_PROCESSING" );

				hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, GetHWnd(), D3DCREATE_MIXED_VERTEXPROCESSING, &m_d3dPP, &m_pDevice );
				if( FAILED( hr ) )
				{
					CnPrint( L"Failed to create Direct3DDevice. MODE : MIXED_VERTEX_PROCESSING" );
					CnPrint( L"Creating Direct3DDevice : SOFRWARE_VERTEX_PROCESSING" );

					hr = pDriver->GetD3D()->CreateDevice( activeDriver, devType, GetHWnd(), D3DCREATE_SOFTWARE_VERTEXPROCESSING, &m_d3dPP, &m_pDevice );
				}
			}
		}

		if( SUCCEEDED( hr ) )
		{
			// Store references to buffers for convenience
			m_pDevice->GetRenderTarget( 0, &m_pRenderSurface );
			m_pDevice->GetDepthStencilSurface( &m_pRenderZBuffer );

			// release immediately so we don't hog them
			SAFEREL( m_pRenderSurface );
			SAFEREL( m_pRenderZBuffer );
		}
		else
		{
			CnError( L"[CnDx9RenderWindow::_CreateDevice] Failed to create D3D Device" );
		}

		return true;
	}

	//----------------------------------------------------------------
	/** Initialize Parameters
	    @brief	  기본 정보들 세팅
	*/
	void CnDx9RenderWindow::_InitPresentParameter( CnDx9Driver* pDriver )
	{
		ZeroMemory( &m_d3dPP, sizeof( D3DPRESENT_PARAMETERS ) );

		m_d3dPP.hDeviceWindow			= GetHWnd();
		m_d3dPP.Windowed				= !m_bFullScreen;
		m_d3dPP.SwapEffect				= D3DSWAPEFFECT_DISCARD;
		m_d3dPP.BackBufferCount			= 1;
		m_d3dPP.BackBufferWidth			= GetWidth();
		m_d3dPP.BackBufferHeight		= GetHeight();
		m_d3dPP.MultiSampleType         = D3DMULTISAMPLE_NONE;
		m_d3dPP.MultiSampleQuality      = 0;

		// Request depth and stencil buffers.  The parameters are not independent
		// for DirectX.  TO DO.  For now, just grab a 24-bit depth buffer and an
		// 8-bit stencil buffer.
		m_d3dPP.EnableAutoDepthStencil	= TRUE;
		m_d3dPP.AutoDepthStencilFormat	= D3DFMT_D24S8;

		m_d3dPP.Flags = 0;

		if( m_bFullScreen )
		{
			m_d3dPP.FullScreen_RefreshRateInHz	= m_usRefreshRate;
		}
		else
		{
			m_d3dPP.FullScreen_RefreshRateInHz	= 0;
		}

		bool verticalSync = false;
		if (verticalSync)
		{
			m_d3dPP.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
		}
		else
		{
			m_d3dPP.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		}

		//! Driver Informations.....
		D3DDEVTYPE devType = D3DDEVTYPE_HAL;			//!< Support only HAL mode
		ushort activeDriver = pDriver->GetIndex();		//!< 현재 장착한 드라이버 아뒤

		//! Back buffer format...
		HDC hDC = GetDC( GetHWnd() );
		ushort usDesktopBit = GetDeviceCaps( hDC, BITSPIXEL );
		if( !m_bFullScreen || ( m_bFullScreen && ( m_usColorDepth > usDesktopBit ) ) )
		{
			m_d3dPP.BackBufferFormat = pDriver->GetCurDisplayMode().Format;
		}
		else
		{
			if( m_usColorDepth > 16 )
			{
				m_d3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
				CnPrint( L"Windowed BACK_BUFFER_FORMAT : D3DFMT_X8R8G8B8" );
			}
			else
			{
				m_d3dPP.BackBufferFormat = D3DFMT_R5G6B5;
				CnPrint( L"Windowed BACK_BUFFER_FORMAT : D3DFMT_R5G6B5" );
			}
		}

		//! Find Depth Stencil format
		if( m_usColorDepth > 16 )
		{
			if( FAILED( pDriver->GetD3D()->CheckDeviceFormat( activeDriver,
															  devType,
															  m_d3dPP.BackBufferFormat,
															  D3DUSAGE_DEPTHSTENCIL,
															  D3DRTYPE_SURFACE,
															  D3DFMT_D24S8 ) ) )
			{
				if( SUCCEEDED( pDriver->GetD3D()->CheckDeviceFormat( activeDriver,
																	 devType,
																	 m_d3dPP.BackBufferFormat,
																	 D3DUSAGE_DEPTHSTENCIL,
																	 D3DRTYPE_SURFACE,
																	 D3DFMT_D32 ) ) )
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D32;
					CnPrint( L"DEPTH_STENCIL Format : D3DFMT_D32" );
				}
				else
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D16;
					CnPrint( L"DEPTH_STENCIL Format : D3DFMT_D16" );
				}
			}
			else
			{
				if( SUCCEEDED( pDriver->GetD3D()->CheckDepthStencilMatch( activeDriver,
																		  devType,
																		  m_d3dPP.BackBufferFormat, 
																		  m_d3dPP.BackBufferFormat,
																		  D3DFMT_D24S8 ) ) )
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D24S8;
					CnPrint( L"DEPTH_STENCIL Format : D3DFMT_D24S8" );
				}
				else
				{
					m_d3dPP.AutoDepthStencilFormat = D3DFMT_D24X8;
					CnPrint( L"DEPTH_STENCIL Format : D3DFMT_D24X8" );
				}
			}
		}
		else
		{
			m_d3dPP.AutoDepthStencilFormat = D3DFMT_D16;
			CnPrint( L"DEPTH_STENCIL Format : D3DFMT_D16" );
		}

		//m_d3dPP.EnableAutoDepthStencil = false;
		//m_d3dPP.AutoDepthStencilFormat = D3DFMT_D24S8;
	}

	//----------------------------------------------------------------
	/** Initialize Parameters
	    @brief	  기본 정보들 세팅
	*/
	bool CnDx9RenderWindow::_RestoreLostDevice()
	{
		if( !m_bLostDevice )
			return true;

		HRESULT hr = GetD3DDevice()->TestCooperativeLevel();
		if( D3DERR_DEVICELOST == hr )
			return true;

		if( D3DERR_DEVICENOTRESET == hr )
		{
			// Release

			OnRestore();

			// Restore

			hr = GetD3DDevice()->TestCooperativeLevel();
			if( SUCCEEDED(hr) )
			{
				m_bLostDevice = true;
				return true;
			}
		}

		return false;
	}

	//----------------------------------------------------------------
	/** SwapBuffer
	    @brief	  백버퍼와 주버퍼를 교체    
		@param        none
		@return       long : HRESULT
	*/
	long CnDx9RenderWindow::_SwapBuffer()
	{
		if( m_pDevice )
		{
			HRESULT hr = m_pDevice->Present( 0, 0, 0, 0 );
			if (FAILED(hr))
			{
				const TCHAR* tchar = DXGetErrorString( hr );
				CnError( ToStr(L"[D3D9RenderDevice::Present] Failed Present = %s\n", tchar) );
			}

			IDirect3DQuery9* query = 0;
			if (SUCCEEDED( m_pDevice->CreateQuery( D3DQUERYTYPE_EVENT, &query ) ))
			{
				query->Issue( D3DISSUE_END );
				BOOL data[1] = {0};
				do
				{
					hr = query->GetData( data, sizeof(BOOL), D3DGETDATA_FLUSH );
				} while (hr == S_FALSE);

				query->Release();
			}

			return hr;
		}

		return 0;
	}

	//----------------------------------------------------------------
	/** Clear Buffer
	    @brief      버퍼 클리어
		@param        ulFlags : 
		@param        ulColor : 
		@param        fDepth : 
		@param        usStencil : 
		@return		  none
	*/
	void CnDx9RenderWindow::_ClearBuffer( ulong ulFlags, ulong ulColor, float fDepth, ushort usStencil )
	{
		if (ulFlags == 0)
			return;

		if( (ulFlags & IRenderer::CLEAR_STENCIL ) && !m_pRenderer->GetDriverCaps()->HasCapability( CnDx9DriverCaps::CAPS_HWSTENCIL ) )
		{
#ifdef _DEBUG
			CnPrint( L"Stencil is not support" );
#endif
			ulFlags &= ~IRenderer::CLEAR_STENCIL;
		}

		HRESULT hr = GetD3DDevice()->Clear( 0, NULL, ulFlags, ulColor, fDepth, usStencil );
		Assert( SUCCEEDED( hr ), L"[CnDx9RenderWindow::_ClearBuffer] Failed 'Clear Device'");
	}

	//----------------------------------------------------------------
	/** Resize
	    @brief	  사이즈 조정
		@param        usWidth : 가로 길이 설정
		@param        usHeight : 세로 길이 설정
		@see		  CnRenderTarget::Resize..
	*/
	void CnDx9RenderWindow::Resize( int nWidth, int nHeight )
	{
		CnRenderTarget::Resize( nWidth, nHeight );

		m_d3dPP.BackBufferWidth = nWidth;
		m_d3dPP.BackBufferHeight = nHeight;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9RenderWindow::Begin()
	{
		CnRenderWindow::Begin();

		//RenderDevice->SetDepthStencilSurface( NULL );

		//IDirect3DSurface9* backBuffer = 0;
		//HRESULT hr = 0;
		//hr = m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backBuffer);
		//assert(SUCCEEDED(hr));
		//hr = m_pDevice->SetRenderTarget(0, backBuffer);
		//assert(SUCCEEDED(hr));
		//backBuffer->Release();
	}
	
	//----------------------------------------------------------------
	/** Draw
	    @brief	  랜더 타겟 그리기
	*/
	void CnDx9RenderWindow::Draw()
	{
		CnRenderWindow::Draw();
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9RenderWindow::End()
	{
		//m_pDevice->SetRenderTarget(0, NULL);

		m_bLostDevice = SUCCEEDED(_SwapBuffer()) ? false : true;

		CnRenderWindow::End();
	}

	//----------------------------------------------------------------
	/** Update
	    @brief	  랜더 타겟 갱신
		@param        none
		@return       long : HRESULT
	*/
	bool CnDx9RenderWindow::Update()
	{
//		if (!m_bIsActive)
//			return false;

//		m_pRenderer->SetCurrentRenderWindow( this );

        //IDirect3DSurface9* backBuffer = 0;
        //HRESULT hr = GetD3DDevice()->GetBackBuffer(0,0, D3DBACKBUFFER_TYPE_MONO, &backBuffer);
        //assert(SUCCEEDED(hr));
        //hr = GetD3DDevice()->SetRenderTarget(0, backBuffer);
        //assert(SUCCEEDED(hr));
        //backBuffer->Release();

		//RenderDevice->SetDepthStencilSurface( NULL );

		CnRenderWindow::Update();
//		CnRenderWindow::Draw();

		//// test
		//TexPtr spTexture = TextureMgr->Load( L"SceneRenderTarget" );
		//CnDx9Texture* d3dTex = static_cast<CnDx9Texture*>( spTexture.GetPtr() );
		//if (NULL == d3dTex)
		//	return true;

		//_ClearBuffer( IRenderer::CLEAR_TARGET | IRenderer::CLEAR_ZBUFFER, 0xffffffff, 1.0f, 0 );

		//CnDx9Renderer* renderer = static_cast<CnDx9Renderer*>( RenderDevice.GetPtr() );
		//LPDIRECT3DDEVICE9 device = renderer->GetCurrentDevice();
		//device->BeginScene();

		//
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		////
		////  그림자 맵이 제대로 만들어졌는지 테스트 출력.
		////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		//float vertex2D[4][4+2] = 
		//{
		//	{   0,  0,0,1, 0,0 },
		//	{ (float)m_d3dPP.BackBufferWidth,  0,0,1, 1,0 },	
		//	{ (float)m_d3dPP.BackBufferWidth,(float)m_d3dPP.BackBufferHeight,0,1, 1,1 },
		//	{   0, (float)m_d3dPP.BackBufferHeight,0,1, 0,1 },
		//};
		//device->SetVertexShader( NULL );
		//device->SetTexture( 0, d3dTex->GetD3DTexture() );
		//device->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		//device->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		//device->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_POINT );
		//device->SetSamplerState( 0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP );
		//device->SetSamplerState( 0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP );
		//device->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
		//device->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
		//device->SetFVF( D3DFVF_XYZRHW | D3DFVF_TEX1 );
		//device->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,2,vertex2D,sizeof(float)*(4+2));

		//device->EndScene();

		m_bLostDevice = SUCCEEDED(_SwapBuffer()) ? false : true;
		return m_bLostDevice;
	}
}