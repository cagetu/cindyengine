// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNDX9DISPLAYMODE_H__
#define __CNDX9DISPLAYMODE_H__

#include "Cindy.h"

namespace Cindy
{
	//==================================================================
	/** CnDx9DisplayMode
		@author
			cagetu
		@since
			2006년 10월 1일
		@remarks
			화면 해상도, 비율 등을 표현 하는 클래스
			ex) 1024 * 768 * 32
		@see
			Orge3D - D3D9VideoMode Class
	*/
	//==================================================================
	class CnDx9DisplayMode
	{
	public:
		CnDx9DisplayMode( D3DDISPLAYMODE d3ddm );
		~CnDx9DisplayMode();

		/** 정보 얻어오기
		*/
		uint				GetWidth() const;
		uint				GetHeight() const;
		D3DFORMAT			GetFormat() const;
		uint				GetRefreshRate() const;
		uint				GetColorDepth() const;

		/** 화면 해상도를 출력한다.
		*/
		void				GetDescription( CnString& rDesc ) const;
		
	private:
		D3DDISPLAYMODE		m_DisplayMode;
	};

#include "CnDx9DisplayMode.inl"
}

#endif	//	__CNDX9DISPLAYMODE_H__