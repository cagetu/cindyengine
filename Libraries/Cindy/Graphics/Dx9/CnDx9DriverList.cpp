// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnDx9DriverList.h"

namespace Cindy
{
	/// Constructor
	CnDx9DriverList::CnDx9DriverList( LPDIRECT3D9 pD3D )
	{
		assert( m_pD3D );

		m_pD3D = pD3D;

		_Enumerate();
	}
	/// Destructor
	CnDx9DriverList::~CnDx9DriverList()
	{
		m_Drivers.clear();
	}

	//--------------------------------------------------------------
	void CnDx9DriverList::_Enumerate()
	{
		D3DADAPTER_IDENTIFIER9 adapterIdentifier;
		D3DDISPLAYMODE d3ddm;

		uint count = m_pD3D->GetAdapterCount();
		for( ushort i = 0; i < count; ++i )
		{
			m_pD3D->GetAdapterIdentifier( i, 0, &adapterIdentifier );
			m_pD3D->GetAdapterDisplayMode( i, &d3ddm );

			m_Drivers.push_back( CnDx9Driver( m_pD3D, i, adapterIdentifier, d3ddm ) );
		}
	}

	//--------------------------------------------------------------
	ushort CnDx9DriverList::GetCount()
	{
		return (ushort)m_Drivers.size();
	}

	//--------------------------------------------------------------
	CnDx9Driver* CnDx9DriverList::GetDriver( ushort usIndex )
	{
		if( m_Drivers.empty() )
			return NULL;

		return &m_Drivers.at( usIndex );
	}

	//--------------------------------------------------------------
	CnDx9Driver* CnDx9DriverList::GetDriver( const CnString& strDesc )
	{
		CnString desc;

		DriverIter itEnd = m_Drivers.end();
		for( DriverIter it = m_Drivers.begin(); it != itEnd; ++it )
		{
			it->GetDescription( desc );
			if(	desc == strDesc )
			{
				return &(*it);
			}
		}

		return NULL;
	}
}