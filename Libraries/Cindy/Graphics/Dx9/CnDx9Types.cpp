//================================================================
// File:               : CnDx9Types.cpp
// Related Header File : CnDx9Types.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 9
//================================================================
#include "../../Cindy.h"
#include "../../Util/CnLog.h"
#include "CnDx9Types.h"

namespace Cindy
{
	//---------------------------------------------------------------------
	/**
	*/
	D3DDECLUSAGE 
	CnDx9Types::ToVertexDeclarationUsage( CnVertexComponent::Usage usage )
	{
		switch (usage)
		{
		case CnVertexComponent::Position:
			return D3DDECLUSAGE_POSITION;
		case CnVertexComponent::PositionT:
			return D3DDECLUSAGE_POSITIONT;
		case CnVertexComponent::Normal:
			return D3DDECLUSAGE_NORMAL;
		case CnVertexComponent::Uv0:
		case CnVertexComponent::Uv1:
		case CnVertexComponent::Uv2:
		case CnVertexComponent::Uv3:
			return D3DDECLUSAGE_TEXCOORD;
		case CnVertexComponent::Color:
			return D3DDECLUSAGE_COLOR;
		case CnVertexComponent::Tangent:
			return D3DDECLUSAGE_TANGENT;
		case CnVertexComponent::Binormal:
			return D3DDECLUSAGE_BINORMAL;
		case CnVertexComponent::BlendWeights:
			return D3DDECLUSAGE_BLENDWEIGHT;
		case CnVertexComponent::BlendIndices:
			return D3DDECLUSAGE_BLENDINDICES;
		}

		CnError( L"[CnDx9Types::ToVertexDeclarationUsage] Invalid Input parameter!" );
		assert( 0 );
		return D3DDECLUSAGE_POSITION;
	}

	//---------------------------------------------------------------------
	/**
	*/
	D3DDECLTYPE
	CnDx9Types::ToVertexDeclarationType( CnVertexComponent::Format usage )
	{
		assert( 0 );
		return D3DDECLTYPE_UNUSED;
	}

	//---------------------------------------------------------------------
	/**
	*/
	D3DMULTISAMPLE_TYPE
	CnDx9Types::ToMultiSampleType( CnAntiAliasQuality::Type type )
	{
		switch (type)
		{
		case CnAntiAliasQuality::None:    
			return D3DMULTISAMPLE_NONE;
		case CnAntiAliasQuality::Low:     
			return D3DMULTISAMPLE_2_SAMPLES;
		case CnAntiAliasQuality::Medium:  
		case CnAntiAliasQuality::High:    
			return D3DMULTISAMPLE_4_SAMPLES;
		}

		CnError( L"D3D9Types::AsD3D9MultiSampleType(): unsupported AntiAliasQuality!" );
		assert(0);
		return D3DMULTISAMPLE_NONE;
	}

	//---------------------------------------------------------------------
	/**
	*/
	D3DFORMAT
	CnDx9Types::ToD3DFormat( PixelFormat::Code format )
	{
		D3DFORMAT colorFormat;
		switch (format)
		{
		case PixelFormat::X8R8G8B8:			colorFormat = D3DFMT_X8R8G8B8;			break;
		case PixelFormat::A8R8G8B8:			colorFormat = D3DFMT_A8R8G8B8;			break;
		case PixelFormat::R16F:				colorFormat = D3DFMT_R16F;				break;
		case PixelFormat::G16R16F:			colorFormat = D3DFMT_G16R16F;			break;
		case PixelFormat::A16B16G16R16F:	colorFormat = D3DFMT_A16B16G16R16F;		break;
		case PixelFormat::R32F:				colorFormat = D3DFMT_R32F;				break;
		case PixelFormat::G32R32F:			colorFormat = D3DFMT_G32R32F;			break;
		case PixelFormat::A32B32G32R32F:	colorFormat = D3DFMT_A32B32G32R32F;		break;
		case PixelFormat::DXT1:				colorFormat = D3DFMT_DXT1;				break;
		case PixelFormat::DXT2:				colorFormat = D3DFMT_DXT2;				break;
		case PixelFormat::DXT3:				colorFormat = D3DFMT_DXT3;				break;
		case PixelFormat::DXT4:				colorFormat = D3DFMT_DXT4;				break;
		case PixelFormat::DXT5:				colorFormat = D3DFMT_DXT5;				break;
		case PixelFormat::D32:				colorFormat = D3DFMT_D32;				break;
		case PixelFormat::D15S1:			colorFormat = D3DFMT_D15S1;				break;
		case PixelFormat::D24S8:			colorFormat = D3DFMT_D24S8;				break;
		case PixelFormat::D24X8:			colorFormat = D3DFMT_D24X8;				break;
		case PixelFormat::D24X4S4:			colorFormat = D3DFMT_D24X4S4;			break;
		case PixelFormat::D16:				colorFormat = D3DFMT_D16;				break;
		case PixelFormat::D24FS8:			colorFormat = D3DFMT_D24FS8;			break;
		default:
			CnError( L"CnDx9Types::ToFormat: invalid render target pixel format!" );
			return D3DFMT_UNKNOWN;
		}
		return colorFormat;
	}
	
	//---------------------------------------------------------------------
	/**
	*/
	//PixelFormat::Code
	//CnDx9Types::ToPixelFormat( D3DFORMAT format )
	//{
	//}
}
