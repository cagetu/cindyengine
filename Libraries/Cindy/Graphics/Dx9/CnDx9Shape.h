//================================================================
// File:           : CnDx9Shape.h
// Original Author : changhee
// Creation Date   : 2009. 6. 30
//================================================================
#ifndef __CN_DX9_SHAPE_H__
#define __CN_DX9_SHAPE_H__

#include "Graphics/Base/CnShape.h"

namespace Cindy
{
	//================================================================
	/** DirectX9 Shape Class
	    @author    changhee
		@brief	   �⺻ Shape ��ü
	*/
	//================================================================
	class CnDx9Shape : public CnShape
	{
	public:
		CnDx9Shape();
		virtual ~CnDx9Shape();

	private:
		//ID3DXMesh*	m_Shape;
	};

}

#endif // __CN_DX9_SHAPE_H__