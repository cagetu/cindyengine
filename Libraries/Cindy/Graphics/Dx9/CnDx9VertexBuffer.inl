//================================================================
// File:               : CnDx9VertexBuffer.cpp
// Related Header File : CnDx9VertexBuffer.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 31
//================================================================

//----------------------------------------------------------------
/** @brief	버텍스 버퍼 객체 반환
			"vb->GetBuffer( &buf );" 로 사용
*/
//----------------------------------------------------------------
inline
void CnDx9VertexBuffer::GetBuffer( void* pOutput )
{
	LPDIRECT3DVERTEXBUFFER9* buffer = (LPDIRECT3DVERTEXBUFFER9*)pOutput;

	*buffer = m_pBuffer;
}
