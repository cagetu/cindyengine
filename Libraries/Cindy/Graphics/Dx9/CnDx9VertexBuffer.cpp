//================================================================
// File:               : CnDx9VertexBuffer.cpp
// Related Header File : CnDx9VertexBuffer.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 12
//================================================================
#include "Cindy.h"
#include "CnDx9VertexBuffer.h"
#include "CnDx9VertexDeclaration.h"

#include "CnDx9Renderer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnDx9VertexBuffer, CnVertexBuffer );

	/// Const/Dest
	//----------------------------------------------------------------
	CnDx9VertexBuffer::CnDx9VertexBuffer()
		: m_pBuffer(0)
		, m_ulFVF(0)
	{
	}
	CnDx9VertexBuffer::~CnDx9VertexBuffer()
	{
		Destroy();
	}

	//================================================================
	/** Create 
		@remarks      버텍스 버퍼 생성
		@param        nNumVertices : 버텍스 개수
		@param		  nSizeOfVertex : 버텍스 사이즈
		@param		  ulUsage : 버퍼 권한 설정
		@param		  ePool : 버퍼 풀링 모드
		@return		  none
	*/
	//================================================================
	bool CnDx9VertexBuffer::Create( uint nNumVertices, uint nSizeOfVertex,
									ulong ulUsage, ulong ulPool, bool bUseFVF )
	{
		if( m_pBuffer )
			return false;

		if( 0 == nNumVertices || 0xffff == nNumVertices )
			return false;

		m_ulFVF = 0;

		this->m_nSizeOfVertex = nSizeOfVertex;

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );
		HRESULT hr = device->CreateVertexBuffer( nNumVertices*nSizeOfVertex, ulUsage, m_ulFVF, (D3DPOOL)ulPool, &m_pBuffer, NULL );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9VertexBuffer::Create] Failed 'Create VertexBuffer'" );
#else
		if( FAILED( hr ) )
			return NULL;
#endif
		return true;
	}

	//================================================================
	/** Destroy
		@remarks      버텍스 버퍼 삭제
		@param		  none
		@return		  none
	*/
	//================================================================
	void CnDx9VertexBuffer::Destroy()
	{
		SAFEREL( m_pBuffer );
	}

	//================================================================
	/** Lock
		@remarks      버텍스 버퍼 Lock
		@param		  nOffset : 락 걸 인덱스
		@param		  nCount : 락을 걸 버텍스 개수
		@param		  ulFlags : 모드
		@return		  float* : 버퍼 반환
	*/
	//================================================================
	void* CnDx9VertexBuffer::Lock( uint nOffset, uint nCount, ulong ulFlags )
	{
		if (!m_pBuffer)
			return NULL;

		void* ptr = NULL;
		HRESULT hr = m_pBuffer->Lock( nOffset*GetSizeOfVertex(), nCount*GetSizeOfVertex(), (void**)&ptr, ulFlags );
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9VertexBuffer::Lock] Failed 'Lock'" );
#else
		if( FAILED( hr ) )
			return NULL;
#endif

		return ptr;
	}
//	float* CnDx9VertexBuffer::Lock( uint nOffset, uint nCount, ulong ulFlags )
//	{
//		if( !m_pBuffer )
//			return NULL;
//
//		float* ptr = NULL;
//		HRESULT hr = m_pBuffer->Lock( nOffset*GetSizeOfVertex(), nCount*GetSizeOfVertex(), (void**)&ptr, ulFlags );
//#ifdef _DEBUG
//		Assert( SUCCEEDED( hr ), L"[CnDx9VertexBuffer::Lock] Failed 'Lock'" );
//#else
//		if( FAILED( hr ) )
//			return NULL;
//#endif
//
//		return ptr;
//	}

	//================================================================
	/** Unlock
		@remarks      버텍스 버퍼 Unlock
		@param		  none
		@return		  none
	*/
	//================================================================
	bool CnDx9VertexBuffer::Unlock()
	{
		if( !m_pBuffer )
			return false;

		HRESULT hr = m_pBuffer->Unlock();
#ifdef _DEBUG
		Assert( SUCCEEDED( hr ), L"[CnDx9VertexBuffer::Unlock] Failed 'Unlock'" );
#else
		if( FAILED( hr ) )
			return false;
#endif
		return true;
	}

	//================================================================
	/** Set FVF
		@remarks      FVF 설정 
		@param		  none
		@return		  true/false : 성공 여부
	*/
	//================================================================
	//bool CnDx9VertexBuffer::SetFVF()
	//{
	//	if( 0 == m_ulFVF )
	//		return false;

	//	LPDIRECT3DDEVICE9 device;
	//	RenderDevice->GetCurrentDevice( &device );

	//	HRESULT hr = device->SetFVF( m_ulFVF );
	//#ifdef _DEBUG
	//	Assert( SUCCEEDED( hr ), L"[CnDx9VertexBuffer::SetFVF] Failed 'Set FVF'" );
	//#else
	//	if( FAILED( hr ) )
	//		return false;
	//#endif
	//	return true;
	//}

	//================================================================
	/** Set Source Stream
		@remarks      디바이스에 버퍼 세팅
		@param		  nStreamID : 데이터 스트림 인덱스
		@param		  nOffsetInBytes : 버텍스 데이터 시작 Offset
		@return		  true/false : 성공 여부
	*/
	//================================================================
	//bool CnDx9VertexBuffer::SetStreamSource( uint nStreamID, uint nOffsetInBytes )
	//{
	//	LPDIRECT3DDEVICE9 device;
	//	RenderDevice->GetCurrentDevice( &device );
	//	RenderDevice->SetVertexDeclaration( GetVertexDeclaration() );

	//	HRESULT hr = device->SetStreamSource( nStreamID, m_pBuffer, nOffsetInBytes, GetSizeOfVertex() );
	//#ifdef _DEBUG
	//	Assert( SUCCEEDED( hr ), L"[CnDx9VertexBuffer::SetStreamSource] Failed 'SetStreamSource'" );
	//#else
	//	if( FAILED( hr ) )
	//		return false;
	//#endif
	//	return true;
	//}
}