//================================================================
// File:               : CnDx9ShaderInclude.cpp
// Related Header File : CnDx9ShaderInclude.h
// Original Author     : changhee
// Creation Date       : 2007. 3. 5
//================================================================
#include "Cindy.h"
#include "Util/CnLog.h"

#include "CnDx9ShaderInclude.h"

#include <MoCommon/IO/MoFileStream.h>

namespace Cindy
{
	CnDx9ShaderInclude::CnDx9ShaderInclude( const CnString& strDir )
		: m_strDir( strDir )
	{
		//m_strDir += L"/";
	}

	//================================================================
	/** Open
	    @brief  쉐이더 파일 읽기
		@param InlcludeType : #include " " / #include < > 어떤식으로 선언되었는지 알려준다
		@param pFileName	: 파일 이름. 즉, "", <> 안의 텍스트
		@param pParentData	: 모르니까 넘어가자!!!
		@param ppData		: 데이터를 저장할 메모리 주소
		@param pBytes		: 데이터의 메모리 크기를 저장할 변수의 주소
	*/
	//================================================================
	HRESULT CnDx9ShaderInclude::Open( D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID* ppData, UINT* pBytes )
	{
		using namespace MoCommon;

		MoFileStream file;

		CnString fileName;
		unicode::CharToWChar( pFileName, fileName );

		// 절대 경로로 한번 시도해본다.
		if( !file.Open( fileName.c_str(), L"r" ) )
		{
			// 셰이더 경로 안에서 시도
			CnString filepath = m_strDir + fileName;
			if( !file.Open( filepath.c_str(), L"r" ) )
			{
				CnError( ToStr(L"could not open include file '%s' nor '%s'! \n", fileName.c_str(), filepath.c_str()) );
				return E_FAIL;
			}
		}

		long filesize = 0;
		file.Seek( 0, SEEK_END );
		filesize = file.Tell();
		file.Seek( 0, SEEK_SET );

		void* buffer = malloc( filesize );
		file.Read( buffer, sizeof(unsigned char), filesize );

		CnString tmp;
		unicode::CharToWChar( (char*)buffer, tmp );

		//CnError( tmp.c_str() );
		//MessageBox( NULL, tmp.c_str(), L"[CnDx9ShaderInclude::Open]", NULL );

		*ppData = buffer;
		*pBytes = filesize;

		file.Close();
		return S_OK;
	}

	//================================================================
	/** Close
	    @brief	닫기
		@param pData	: Open에서 저장된 메모리 주소
	*/
	//================================================================
	HRESULT CnDx9ShaderInclude::Close( LPCVOID pData )
	{
		free((void*)pData);
		return S_OK;
	}
}