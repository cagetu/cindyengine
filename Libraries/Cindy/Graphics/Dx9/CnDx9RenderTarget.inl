//================================================================
// File:               : CnDx9RenderTarget.inl
// Related Header File : CnDx9RenderTarget.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 24
//================================================================

//-------------------------------------------------------------------------
inline const D3DPRESENT_PARAMETERS&	CnDx9RenderTarget::GetD3DPresent() const
{
	return m_d3dPP;
}

//-------------------------------------------------------------------------
inline LPDIRECT3DDEVICE9 CnDx9RenderTarget::GetD3DDevice() const
{
	return m_pDevice;
}

//-------------------------------------------------------------------------
inline const CnWindow* CnDx9RenderTarget::GetWindow() const
{
	return m_pWindow;
}

//-------------------------------------------------------------------------
inline bool	CnDx9RenderTarget::IsActive() const
{
	return m_bIsActive;
}
