//================================================================
// File:           : CnDx9ShaderInclude.h
// Original Author : changhee
// Creation Date   : 2007. 3. 5
//================================================================
#ifndef __CN_DX9_SHADER_INCLUDE_H__
#define __CN_DX9_SHADER_INCLUDE_H__

#include <d3dx9.h>

namespace Cindy
{
	//================================================================
	/** Dx9ShaderInclude
	    @author    changhee
		@since     2007. 3. 5
		@remarks   
	*/
	//================================================================
	class CnDx9ShaderInclude : public ID3DXInclude
	{
	public:
		CnDx9ShaderInclude( const CnString& strDir );

		/// open an include file and read its contents
		STDMETHOD(Open)(D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes);
		
		/// close an include file
		STDMETHOD(Close)(LPCVOID pData);

	private:
		CnString	m_strDir;	//!< ���
	};
}

#endif	// __CN_DX9_SHADER_INCLUDE_H__