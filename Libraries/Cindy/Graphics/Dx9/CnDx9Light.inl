//================================================================
// File:               : CnDx9Light.cpp
// Related Header File : CnDx9Light.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 29
//================================================================
/// Const
inline
CnDx9Light::CnDx9Light( const D3DLIGHT9& light )
: m_d3dLight( light )
{
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetAmbient( float r, float g, float b, float a )
{
	m_d3dLight.Ambient.r = r;
	m_d3dLight.Ambient.g = g;
	m_d3dLight.Ambient.b = b;
	m_d3dLight.Ambient.a = a;
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetDiffuse( float r, float g, float b, float a )
{
	m_d3dLight.Diffuse.r = r;
	m_d3dLight.Diffuse.g = g;
	m_d3dLight.Diffuse.b = b;
	m_d3dLight.Diffuse.a = a;
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetSpecular( float r, float g, float b, float a )
{
	m_d3dLight.Specular.r = r;
	m_d3dLight.Specular.g = g;
	m_d3dLight.Specular.b = b;
	m_d3dLight.Specular.a = a;
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetPosition( float x, float y, float z )
{
	m_d3dLight.Position.x = x;
	m_d3dLight.Position.y = y;
	m_d3dLight.Position.z = z;
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetDirection( float x, float y, float z )
{
	m_d3dLight.Direction.x = x;
	m_d3dLight.Direction.y = y;
	m_d3dLight.Direction.z = z;
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetAttenuation( float atten0, float atten1, float atten2 )
{
    m_d3dLight.Attenuation0 = atten0;     
    m_d3dLight.Attenuation1 = atten1;     
    m_d3dLight.Attenuation2 = atten2;     
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetRange( float range )
{
	m_d3dLight.Range = range;
}

//----------------------------------------------------------------
inline
void CnDx9Light::SetConeAngle( float theta, float phi, float falloff )
{
	m_d3dLight.Theta = theta;
	m_d3dLight.Phi = phi;

	m_d3dLight.Falloff = falloff;
}

//----------------------------------------------------------------
inline
const D3DLIGHT9*	CnDx9Light::GetLight() const
{
	return &m_d3dLight;
}