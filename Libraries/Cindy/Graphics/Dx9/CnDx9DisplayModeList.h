// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNDX9DISPLAYMODELIST_H__
#define __CNDX9DISPLAYMODELIST_H__

#include "CnDx9DisplayMode.h"

namespace Cindy
{
	class CnDx9Driver;

	//==================================================================
	/** CnDx9DisplayModeList
		@author
			cagetu
		@since
			2006년 10월 1일
		@remarks
			표현 가능한 모든 화면 해상도를 얻어온다.
		@see
			Orge3D - D3D9VideoModeList Class
	*/
	//==================================================================
	class CnDx9DisplayModeList
	{
	public:
		// 생성자 / 소멸자
		CnDx9DisplayModeList( CnDx9Driver* pAdapter );
		virtual ~CnDx9DisplayModeList();

		/** 화면 모드 개수
		*/
		ushort				GetCount();

		/** 화면 모드 얻어오기
		*/
		CnDx9DisplayMode*	GetDisplayMode( ushort usIndex );
		CnDx9DisplayMode*	GetDisplayMode( const CnString& strDesc );
		CnDx9DisplayMode*	GetDisplayMode( uint uiWidth, uint uiHeight, uint uiColorDepth, uint uiRefreshRate );
		
	private:
		typedef std::vector< CnDx9DisplayMode >	DispModeVector;
		typedef DispModeVector::iterator		DispIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnDx9Driver*		m_pAdapter;
		DispModeVector		m_DisplayModes;

		/** 화면 모드 목록 갱신
		*/
		void				_Enumerate();
	};
}

#endif	// __CNDX9DISPLAYMODELIST_H__