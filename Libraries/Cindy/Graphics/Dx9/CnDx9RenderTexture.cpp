//================================================================
// File:               : CnDx9RenderTexture.cpp
// Related Header File : CnDx9RenderTexture.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 20
//================================================================
#include "Cindy.h"
#include "Util/CnColor.h"

#include "../CindyRenderConfig.h"

#include "CnDx9RenderTexture.h"
#include "CnDx9RenderWindow.h"
#include "CnDx9Texture.h"
#include "CnDx9Renderer.h"
#include "CnDx9Types.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti( Cindy, CnDx9RenderTexture, CnRenderTexture );
	//----------------------------------------------------------------
	/// Const/Dest
	CnDx9RenderTexture::CnDx9RenderTexture( const wchar* strName,
											ushort usWidth,
											ushort usHeight,
											ushort usDepth,
											PixelFormat::Code eFormat,
											CnAntiAliasQuality::Type eAnitiQuality,
											bool bEnableDepthStencil,
											bool bGenerateMipMaps )
		: CnRenderTexture( strName, usWidth, usHeight, usDepth, eFormat, eAnitiQuality, bEnableDepthStencil, bGenerateMipMaps )
		, m_d3d9MultiSampleType( D3DMULTISAMPLE_NONE )
		, m_d3d9MultiSampleQuality(0)
		, m_d3dColorBuffer(0)
		, m_d3dDepthStencilBuffer(0)
	{
	}
	CnDx9RenderTexture::~CnDx9RenderTexture()
	{
		SAFEREL(m_d3dColorBuffer);
		SAFEREL(m_d3dDepthStencilBuffer);
	}

	//----------------------------------------------------------------
	/**	Texture Setup
	*/
	//----------------------------------------------------------------
	void CnDx9RenderTexture::Setup()
	{
		CnRenderTexture::Setup();

		if (m_bIsActive)
		{
			SetupMultiSampleType();
		}
	}

	//----------------------------------------------------------------
	/** Set Color Fill
	    @brief	  랜더 타겟의 전체 색상을 설정한다.
		@param        color : 색상
		@return       none
	*/
	//----------------------------------------------------------------
	void CnDx9RenderTexture::ColorFill( const CnColor& Color )
	{
		if (m_RenderToTexture.IsNull())
			return;

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		LPDIRECT3DSURFACE9 surface = NULL;
		m_RenderToTexture->GetSurface( &surface );

		HRESULT hr = device->ColorFill( surface, NULL, Color.GetRGBA() );
	}

	//----------------------------------------------------------------
	/**	antialias 파라미터들을 선택한다.
	*/
	//----------------------------------------------------------------
	void CnDx9RenderTexture::SetupMultiSampleType()
	{
		LPDIRECT3D9 d3d9;
		RenderDevice->GetCOM( &d3d9 );

#if CINDY_DIRECT3D_DEBUG
		m_d3d9MultiSampleType = D3DMULTISAMPLE_NONE;
		m_d3d9MultiSampleQuality = 0;
#else
		m_d3d9MultiSampleType = CnDx9Types::ToMultiSampleType( this->GetAntiAliasQuality() );

        // check if the multisample type is compatible with the selected display mode
        DWORD availableQualityLevels = 0;
        HRESULT renderTargetResult = d3d9->CheckDeviceMultiSampleType( 0, 
																	   CINDY_DIRECT3D_DEVICETYPE,
																	   (D3DFORMAT)m_RenderToTexture->GetFormat(),
																	   FALSE,
																	   m_d3d9MultiSampleType,
																	   &availableQualityLevels );
        HRESULT depthBufferResult = d3d9->CheckDeviceMultiSampleType( 0,
																	  CINDY_DIRECT3D_DEVICETYPE,
																	  D3DFMT_D24S8,
																	  FALSE,
																	  m_d3d9MultiSampleType,
																	  NULL );
        if ((D3DERR_NOTAVAILABLE == renderTargetResult) || (D3DERR_NOTAVAILABLE == depthBufferResult))
        {
            // reset to no multisampling
            m_d3d9MultiSampleType = D3DMULTISAMPLE_NONE;
            m_d3d9MultiSampleQuality = 0;
        }
        else
        {
            assert(SUCCEEDED(renderTargetResult) && SUCCEEDED(depthBufferResult));
        }
#endif
	}

	//----------------------------------------------------------------
	/** Update
	    @brief	  Update
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	bool CnDx9RenderTexture::Update()
	{
//		CnRenderTexture::Update();
//		Draw();
		//if (false == IsActive())
		//	return false;
		if (m_RenderToTexture.IsNull())
			return false;

		// setrendertarget
		// clearbuffer
		// render
		// restore_rendertarget

		//RenderDevice->OpenSurface(0);

		LPDIRECT3DSURFACE9 lastSurface = 0;
		LPDIRECT3DSURFACE9 lastDepthStencil = 0;

		RenderDevice->GetSurface( 0, &lastSurface );
		RenderDevice->GetDepthStencilSurface( &lastDepthStencil );
		{
			LPDIRECT3DSURFACE9 surface = 0;
			m_RenderToTexture->GetSurface( &surface );
			RenderDevice->SetSurface( 0, surface );

			if (IsEnabledDepthStencil())
			{
				LPDIRECT3DSURFACE9 depthStencilSurface = 0;
				m_RenderToTexture->GetDepthStencil( &depthStencilSurface );
				if (depthStencilSurface)
				{
					RenderDevice->SetDepthStencilSurface( depthStencilSurface );
				}
			}

			//CnRenderTexture::Draw();
			CnRenderTexture::Update();
		}
		RenderDevice->SetSurface( 0, lastSurface );
		//RenderDevice->CloseSurface(0);

		return true;
	}

	//----------------------------------------------------------------
	/** Begin
	    @brief		랜더링 시작!!
	*/
	//----------------------------------------------------------------
	void CnDx9RenderTexture::Begin()
	{
		CnRenderTarget::Begin();

		// setrendertarget
		// clearbuffer
		// render
		// restore_rendertarget

		//RenderDevice->OpenSurface(0);

		RenderDevice->GetSurface( m_Index, &m_d3dColorBuffer );

		LPDIRECT3DSURFACE9 surface = 0;
		m_RenderToTexture->GetSurface( &surface );
		RenderDevice->SetSurface( m_Index, surface );

		if (IsEnabledDepthStencil())
		{
			RenderDevice->GetDepthStencilSurface( &m_d3dDepthStencilBuffer );

			LPDIRECT3DSURFACE9 depthStencilSurface = 0;
			m_RenderToTexture->GetDepthStencil( &depthStencilSurface );
			if (depthStencilSurface)
			{
				RenderDevice->SetDepthStencilSurface( depthStencilSurface );
			}
		}
	}

	//----------------------------------------------------------------
	/**	Draw
		@brief		랜더링!!!
	*/
	//----------------------------------------------------------------
	void CnDx9RenderTexture::Draw()
	{
		//CnRenderTexture::Draw();
		CnRenderTexture::Update();
	}

	//----------------------------------------------------------------
	/** End
	    @brief		랜더링 끝
	*/
	//----------------------------------------------------------------
	void CnDx9RenderTexture::End()
	{
		this->GenerateMipLevels();

		RenderDevice->SetSurface( m_Index, m_d3dColorBuffer );
		//RenderDevice->CloseSurface(0);

		CnRenderTarget::End();
	}

	//----------------------------------------------------------------
	/** Draw
	    @brief	  그리기
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	//void CnDx9RenderTexture::Draw()
	//{
	//	if (m_spTexture.IsNull())
	//		return;

	//	// setrendertarget
	//	// clearbuffer
	//	// render
	//	// restore_rendertarget

	//	LPDIRECT3DDEVICE9 device;
	//	RenderDevice->GetCurrentDevice( &device );

	//	LPDIRECT3DSURFACE9 rendertarget, zbuffer;
	//	device->GetRenderTarget( 0, &rendertarget );
	//	device->GetDepthStencilSurface( &zbuffer );
	//	{
	//		LPDIRECT3DSURFACE9 surface = NULL, depthStencil = NULL;
	//		m_RenderToTexture->GetSurface( &surface );
	//		m_RenderToTexture->GetDepthStencil( &depthStencil );

	//		device->SetRenderTarget( 0, surface );
	//		device->SetDepthStencilSurface( depthStencil );

	//		CnRenderTexture::Draw();
	//	}
	//	device->SetRenderTarget( 0, rendertarget );
	//	device->SetDepthStencilSurface( zbuffer );
	//}

	//----------------------------------------------------------------
	/**	@brief	Texture 디버그
	*/
	//----------------------------------------------------------------
	void CnDx9RenderTexture::Debug()
	{
		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		///////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  그림자 맵이 제대로 만들어졌는지 테스트 출력.
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////
		float vertex2D[4][4+2] = 
		{
			{   0,  0,0,1, 0,0 },
			{ 128,  0,0,1, 1,0 },	
			{ 128,128,0,1, 1,1 },
			{   0,128,0,1, 0,1 },
		};
		LPDIRECT3DTEXTURE9 texture = NULL;
		m_RenderToTexture->GetTexture( &texture );

		device->SetTexture( 0, texture );
		device->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		device->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		device->SetFVF( D3DFVF_XYZRHW | D3DFVF_TEX1 );
		device->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,2,vertex2D,sizeof(float)*(4+2));
	}

	//----------------------------------------------------------------
	/** Release
	    @brief      해제
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
//	void CnDx9RenderTexture::OnRelease()
//	{
//		m_bIsActive = false;
//
//		m_spTexture->OnLost();
//	}
//
//	//----------------------------------------------------------------
//	/** Restore
//	    @brief	  디바이스 복구시 복구
//		@param        none
//		@return       none
//	*/
//	//----------------------------------------------------------------
//	void CnDx9RenderTexture::OnRestore()
//	{
//		m_spTexture->OnRestore();
//
//		m_bIsActive = true;
//	}
}