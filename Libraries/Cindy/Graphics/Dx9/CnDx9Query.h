//================================================================
// File:           : CnDx9Query.h
// Original Author : changhee
// Creation Date   : 2009. 2. 15
//================================================================
#ifndef __CN_DX9_QUERY_H__
#define __CN_DX9_QUERY_H__

#include "../Base/CnQuery.h"

namespace Cindy
{
	//================================================================
	/** Query -DX9
	    @author		changhee
		@brief		directX query의 기반 클래스
		@see		ms-help://MS.VSCC.v80/MS.VSIPCC.v80/MS.DirectX9.1033.2007.August/DirectX_SDK/Queries.htm
	*/
	//================================================================
	class CnDx9Query : public CnQuery
	{
		__DeclareRtti;
	public:
		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		CnDx9Query();
		virtual ~CnDx9Query();

		// Create
		bool	Create( QUERYTYPE QueryType ) override;

		// Issue
		void	Begin() override;
		void	End() override;

		// Get Data
		void	GetData( void* pData, ulong ulSize ) override;

		// Device Lost/Restore
		void	OnLost() override;
		void	OnRestore() override;
	private:
		LPDIRECT3DQUERY9	m_pD3DQuery;
	};
}

#endif	// __CN_DX9_QUERY_H__
