// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNDX9DRIVER_H__
#define __CNDX9DRIVER_H__

#include "CnDx9DisplayModeList.h"

namespace Cindy
{
	//==================================================================
	/** CnDx9Driver
		@author
			cagetu
		@since
			2006년 10월 1일
		@remarks
			그래픽 카드의 정보에 대한 클래스
		@see
			Orge3D - D3D9Driver class
	*/
	//==================================================================
	class CnDx9Driver
	{
	public:
		// 생성자 / 소멸자
		CnDx9Driver( LPDIRECT3D9 pD3D, ushort usIndex, D3DADAPTER_IDENTIFIER9 adapterIdentifer, D3DDISPLAYMODE d3dDispMode );
		~CnDx9Driver();

		// 드라이버 인덱스
		ushort							GetIndex() const;

		// 드라이버 설명
		void							GetName( CnString& rResult ) const;
		void							GetDescription( CnString& rResult ) const;

		// 드라이버 정보 얻어오기
		LPDIRECT3D9						GetD3D() const;
		D3DADAPTER_IDENTIFIER9			GetAdapterIdentifier() const;
		D3DDISPLAYMODE					GetCurDisplayMode() const;

		// 드라이버에서 표시할 수 있는 화면 해상도 리스트
		const CnDx9DisplayModeList*		GetDisplayModeList();
		
	private:
		ushort					m_usIndex;				//!< Driver 인덱스
		LPDIRECT3D9				m_pD3D;					//!< DirectX 객체
		D3DADAPTER_IDENTIFIER9	m_AdapterIdentifier;	//!< Driver 정보
		D3DDISPLAYMODE			m_CurDisplayMode;		//!< Current Display Mode
		CnDx9DisplayModeList*	m_pDisplayModeList;		//!< Save all Display Mode
	};

#include "CnDx9Driver.inl"
}

#endif	//	__CNDX9DRIVER_H__