//================================================================
// File:               : CnDx9MultipleRenderTarget.cpp
// Related Header File : CnDx9MultipleRenderTarget.h
// Original Author     : changhee
// Creation Date       : 2008. 1. 25
//================================================================
#include "Cindy.h"
#include "CnDx9MultipleRenderTarget.h"
#include "CnDx9Texture.h"
#include "CnDx9Renderer.h"

namespace Cindy
{
	//============================================================================
	__ImplementRtti( Cindy, CnDx9MultiRenderTarget, CnMultiRenderTarget );
	// Const/Dest
	CnDx9MultiRenderTarget::CnDx9MultiRenderTarget( const wchar* strName )
		: CnMultiRenderTarget( strName )
	{
	}
	CnDx9MultiRenderTarget::~CnDx9MultiRenderTarget()
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9MultiRenderTarget::Begin()
	{
		CnRenderTarget::Begin();

		// setrendertarget
		// clearbuffer
		// render
		// restore_rendertarget

		//RenderDevice->OpenSurface(0);

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		HRESULT hr = NULL;

		// Set MRTs..
		device->GetRenderTarget( 0, &m_d3dColorBuffer );
		device->GetDepthStencilSurface( &m_d3dDepthStencilBuffer );

		LPDIRECT3DSURFACE9 surface = NULL;
		LPDIRECT3DSURFACE9 depthStencil = NULL;

		for (int i=0; i<CnMultiRenderTarget::NUM_MULTIPLE_RENDERTARGETS; ++i)
		{
			if (m_pSurface[i].IsNull())
				continue;

			//m_pSurface[i]->GetSurface( &surface );

			//hr = device->SetRenderTarget( i, surface );
			//Assert( SUCCEEDED(hr), L"[CnDx9MultiRenderTarget::Draw] Failed 'device->SetRenderTarget'");

			//if (m_pSurface[i]->GetUsage() & CnTexture::RenderTargetDepthStencil)
			//{
			//	m_pSurface[i]->GetDepthStencil( &depthStencil );
			//	RenderDevice->SetDepthStencilSurface( depthStencil );
			//}
			m_pSurface[i]->Begin();
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9MultiRenderTarget::Draw()
	{
		// Draw Scene..
		CnMultiRenderTarget::Update();
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnDx9MultiRenderTarget::End()
	{
		// ClearMRTs...
		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		for (int i=0; i<CnMultiRenderTarget::NUM_MULTIPLE_RENDERTARGETS; ++i)
		{
			if (m_pSurface[i].IsNull())
				continue;

			m_pSurface[i]->End();
		}

		//device->SetRenderTarget( 0, m_d3dColorBuffer );
		//for (int i=1; i<CnMultiRenderTarget::NUM_MULTIPLE_RENDERTARGETS; ++i)
		//	device->SetRenderTarget( i, 0 );

		//device->SetDepthStencilSurface( m_d3dDepthStencilBuffer );

		CnRenderTarget::End();
	}

	//----------------------------------------------------------------
	/**
	*/
	bool CnDx9MultiRenderTarget::Update()
	{
		assert(0);

		//CnMultiRenderTarget::Update();
		//Draw();

		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		HRESULT hr = NULL;

		// Set MRTs..
		LPDIRECT3DSURFACE9 rendertarget; //, zBuffer;
		{
			device->GetRenderTarget( 0, &rendertarget );
			//device->GetDepthStencilSurface( &zBuffer );

			LPDIRECT3DSURFACE9 surface = NULL, depthStencil = NULL;

			for (int i=0; i<CnMultiRenderTarget::NUM_MULTIPLE_RENDERTARGETS; ++i)
			{
				if (m_pSurface[i].IsNull())
					continue;

				//m_pSurface[i]->GetSurface( &surface );
				//m_pSurface[i]->GetDepthStencil( &depthStencil );

				hr = device->SetRenderTarget( i, surface );
				Assert( SUCCEEDED(hr), L"[CnDx9MultiRenderTarget::Draw] Failed 'device->SetRenderTarget'");

				//if (depthStencil)
				//{
				//	hr = device->SetDepthStencilSurface( depthStencil );
				//	Assert( SUCCEEDED(hr), L"[CnDx9MultiRenderTarget::Draw] Failed 'device->SetDepthStencilSurface'");
				//}
			}
		}

		// Draw Scene..
		CnMultiRenderTarget::Update();

		// ClearMRTs...
		{
			device->SetRenderTarget( 0, rendertarget );
			for (int i=1; i<CnMultiRenderTarget::NUM_MULTIPLE_RENDERTARGETS; ++i)
				device->SetRenderTarget( i, 0 );

			//device->SetDepthStencilSurface( zbuffer );
		}
		return true;
	}

	//----------------------------------------------------------------
	/**
	*/
	//void CnDx9MultiRenderTarget::Draw()
	//{
	//	LPDIRECT3DDEVICE9 device;
	//	RenderDevice->GetCurrentDevice( &device );

	//	HRESULT hr = NULL;

	//	// Set MRTs..
	//	LPDIRECT3DSURFACE9 rendertarget, zbuffer;
	//	{
	//		device->GetRenderTarget( 0, &rendertarget );
	//		device->GetDepthStencilSurface( &zbuffer );

	//		LPDIRECT3DSURFACE9 surface = NULL, depthStencil = NULL;

	//		for (int i=0; i<CnMultiRenderTarget::NUM_MULTIPLE_RENDERTARGETS; ++i)
	//		{
	//			if (m_pSurface[i].IsNull())
	//				continue;

	//			m_pSurface[i]->GetSurface( &surface );
	//			m_pSurface[i]->GetDepthStencil( &depthStencil );

	//			hr = device->SetRenderTarget( i, surface );
	//			Assert( SUCCEEDED(hr), L"[CnDx9MultiRenderTarget::Draw] Failed 'device->SetRenderTarget'");
	//		}

	//		hr = device->SetDepthStencilSurface( depthStencil );
	//		Assert( SUCCEEDED(hr), L"[CnDx9MultiRenderTarget::Draw] Failed 'device->SetDepthStencilSurface'");
	//	}

	//	// Draw Scene..
	//	CnMultiRenderTarget::Draw();

	//	// ClearMRTs...
	//	{
	//		device->SetRenderTarget( 0, rendertarget );
	//		for (int i=1; i<CnMultiRenderTarget::NUM_MULTIPLE_RENDERTARGETS; ++i)
	//			device->SetRenderTarget( i, 0 );

	//		device->SetDepthStencilSurface( zbuffer );
	//	}
	//}
}