// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CNDX9RENDERER_H__
#define __CNDX9RENDERER_H__

#include "CnDx9DriverList.h"
#include "CnDx9DriverCaps.h"

#include "Graphics/CnRenderer.h"
#include "Graphics/CnRenderTarget.h"

namespace Cindy
{
	class CnViewport;
	class CnRenderWindow;
	class CnRenderTarget;
	class CnDx9RenderWindow;
	class CnDx9Light;
	class CnShader;

	//==================================================================
	/** CnDx9Renderer
		@author			cagetu
		@since			2006년 10월 1일
		@remarks		랜더러 클래스(DirectX 9 버전을 기본으로..)
						1. 랜더러는 1개만 존재
						2. 랜더러는 n개의 Window에서 랜더링 되는 n개의 디바이스를 생성하고 관리한다.
	*/
	//==================================================================
	class CnDx9Renderer : public IRenderer
	{
	public:
		CnDx9Renderer();
		virtual ~CnDx9Renderer();

		// Device
		void						GetCOM( void* pCOM ) override;
		void						GetCurrentDevice( void* pDevice ) override;
		void						ResetCurrentDevice();

		void						GetCurrentPresentParams( void* pParams ) override;

		// Driver..
		const CnDx9DriverList*		GetDriverList() const;
		const CnDx9Driver*			GetCurrentDriver() const;
		const CnDx9DriverCaps*		GetDriverCaps() const;

		// RenderTarget
		void						AddRenderTarget( CnRenderTarget* pRenderTarget ) override;
		CnRenderTarget*				GetRenderTarget( const CnString& strName ) override;
		CnRenderTarget*				DetachRenderTarget( const CnString& strName ) override;
		bool						DeleteRenderTarget( const CnString& strName ) override;
		void						DeleteAllRenderTargets() override;

		void						UpdateRenderTargets() override;
		void						NeedToSortRenderTargets() override;

		void						SetCurrentRenderTarget( CnRenderTarget* pRenderTarget ) override;
		CnRenderTarget*				GetCurrentRenderTarget() const override;

		// RenderWindow
		CnRenderWindow*				CreateRenderWindow( HWND hWnd,
														const wchar* strName,
														int nWidth,
														int nHeight,
														ushort usColorDepth,
														ushort usRefreshRate,
														bool bFullScreen,
														bool bThreadSafe,
														bool bCurrentTarget = false ) override;

		void						SetCurrentRenderWindow( CnRenderWindow* pRenderWindow ) override;
		CnRenderWindow*				GetCurrentRenderWindow() const;

		// RenderTexture
		CnRenderTexture*			CreateRenderTexture( const wchar* strName,
														 ushort usWidth,
														 ushort usHeight,
														 ushort usDepth,
														 int nFormat,
														 int nAntiAliasQuality,
														 bool bEnableDepthStencil = false,
														 bool bGenerateMipMaps = false,
														 bool bManage = true ) override;

		// MultiRenderTarget
		CnMultiRenderTarget*		CreateMultiRenderTarget( const wchar* starName,
															 bool bManage = true ) override;

		// Surface
		void						SetSurface( int nIndex, void* pTarget ) override;
		void						GetSurface( int nIndex, void* pTarget ) override;

		void						SetDepthStencilSurface( void* pTarget ) override;
		void						GetDepthStencilSurface( void* pTarget ) override;

		void						OpenSurface( int nIndex ) override;
		void						CloseSurface( int nIndex ) override;

		// Viewport
		void						SetViewport( CnViewport* pViewport ) override;

		// RenderState
		void						SetRenderState( const CnRenderState& rState ) override;
		void						SetRenderState( ushort StateFlags ) override;

		void						SetAlphaBlendMode( const CnRenderState& rState ) override;

		// MaterialState
		void						SetMaterialState( const CnMaterialState& rState ) override;
		
		// Texture
		void						SetTexture( int nStage, CnTexture* pTexture ) override;
		void						SetTextureState( const CnTextureState& rState ) override;

		// Shader
		CnShaderEffect*				CreateEffect() override;
		void						GetEffectPool( void* pPool );

		void						SetActiveShader( CnShaderEffect* pEffect );
		CnShaderEffect*				GetActiveShader() const;

		// Material
		void						SetMaterial( CnMaterial* Material ) override;

		// Light
		void						EnableLight( bool bEnable ) override;
		void						EnableSpecular( bool bEnable ) override;
		void						SetAmbient( RGBA ulColor ) override;
		void						GetAmbient( RGBA& rOutput ) override;

		// Shape
		CnShape*					CreateShape( ushort ShapeType ) override;

		// Vertex Buffer
		CnVertexBuffer*				CreateVertexBuffer() override;
		CnVertexBuffer*				CreateVertexBuffer( uint nNumVertices, uint nSizeOfVertex, ulong ulUsage, ulong ulPool ) override;

		bool						SetStreamSource( uint nStreamID, uint nOffsetInBytes, const Ptr<CnVertexBuffer>& pBuffer ) override;
		bool						SetStreamSource( uint BufferID, uint StreamID, uint OffsetInBytes, const Ptr<CnVertexData>& pVertexData ) override;
		bool						SetStreamSource( const Ptr<CnVertexData>& pVertexData ) override;

		// Index Buffer
		CnIndexBuffer*				CreateIndexBuffer() override;
		CnIndexBuffer*				CreateIndexBuffer( uint nNumIndices, ulong ulUsage, ulong ulFormat, ulong ulPool ) override;

		bool						SetIndexBuffer( const Ptr<CnIndexBuffer>& IndexBuffer ) override;

		// Vertex Declaration
		CnVertexDeclaration*		CreateVertexDeclaration() override;
		void						SetVertexDeclaration( const Ptr<CnVertexDeclaration>& pDeclaration ) override;
		void						SetVertexDeclaration( const Ptr<CnVertexData>& pVertexData ) override;
		void						SetFVF( ulong ulFVF ) override;

		// Query
		CnQuery*					CreateQuery( ushort QueryType ) override;

		///
		void						ClearBuffer( ulong ulFlags, ulong ulColor, float fDepth, ushort usStencil ) override;

		// Begin / End Scene
		bool						BeginScene( ulong ulFlags, float fDepth = 1.0f, ushort usStencil = 0 ) override;
		bool						BeginScene( ulong ulFlags, ulong ulBackgroundColor, float fDepth, ushort usStencil ) override;
		void						EndScene() override;

		// Draw
		bool						DrawPrimitive( PRIMITIVE ePrimitive,
												   uint nIndexOfStartVertex,
												   uint nNumPrimitiveCount ) override;
		bool						DrawIndexedPrimitive( PRIMITIVE ePrimitive,
														  uint nNumVertices,
														  uint nNumPrimitiveCount,
														  int nBaseVertexIndex = 0,
														  uint nMinIndex = 0,
														  uint nIndexOfStartVertex = 0 ) override;
	private:
		friend class CnCindy;

		typedef std::vector<Ptr<CnRenderTarget>>	RenderTargetArray;
		typedef RenderTargetArray::iterator			RenderTargetIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		LPDIRECT3D9				m_pD3D;
		D3DCAPS9				m_d3dCaps;

		// Driver 정보..
		CnDx9DriverList*		m_pDriverList;
		CnDx9Driver*			m_pCurrentDriver;
		CnDx9DriverCaps*		m_pDriverCaps;

		CnViewport*				m_pCurrentViewport;

		bool					m_bNeedToSortRenderTargets;
		RenderTargetArray		m_RenderTargets;
		CnRenderTarget*			m_pCurrentRenderTarget;
		CnDx9RenderWindow*		m_pCurrentRenderWindow;

		bool					m_bLostDevice;

		// Surface
		std::set<int>			m_OpenedSurfaces;
		LPDIRECT3DSURFACE9		m_pLastSurface;
		LPDIRECT3DSURFACE9		m_pLastDepthStencil;

		// Shader 정보
		ID3DXEffectPool*		m_pEffectPool;
		CnShaderEffect*			m_pShaderEffect;

		// RenderState
		ushort					m_usRenderStateFlags;
		CnMaterial*				m_Material;

		// Vertex
		int						m_LastVertexComponent;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		/** Driver 정보 설정
		*/
		void		_InitDrivers();

		void		_InitCapabilities( CnDx9RenderWindow* pRenderTarget );
		void		_InitVertexShaderCaps();
		void		_InitPixelShaderCaps();

		bool		_RestoreLostDevice();

		void		_Release();
		void		_Restore();

		void		_CreateEffectPool();

		void		_SetRenderState( D3DRENDERSTATETYPE eState, ulong ulValue );

		LPDIRECT3D9					GetD3D() const;
		LPDIRECT3DDEVICE9			GetCurrentDevice() const;

		//------------------------------------------------------
		// Prevent Copy..
		//------------------------------------------------------
		CnDx9Renderer( const CnDx9Renderer& ) {}
		CnDx9Renderer& operator = ( const CnDx9Renderer& ) {}
	};

#include "CnDx9Renderer.inl"
}

#endif	// __CNDX9RENDERER_H__