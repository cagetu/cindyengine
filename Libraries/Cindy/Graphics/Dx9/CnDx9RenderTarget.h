//================================================================
// File:           : CnDx9RenderTarget.h
// Original Author : changhee
// Creation Date   : 2007. 1. 23
//================================================================
#ifndef __CN_DX9_RENDER_TARGET_H__
#define __CN_DX9_RENDER_TARGET_H__

namespace Cindy
{
	class CnDx9Driver;
	class CnWindow;
	class CnViewport;
	class CnDx9Renderer;

	//==================================================================
	/** CnDx9RenderWindow
		@author
			cagetu
		@since
			2006년 10월 10일
		@remarks
			Window 정보를 받아서, 받은 Window에 랜더링 D3d9 Device를 세팅한다.
			랜더링은 이 녀석을 통해서 업데이트 된다.
	*/
	//==================================================================
	class CnDx9RenderTarget
	{
		friend class CnDx9Renderer;
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnDx9Renderer*			m_pRenderer;

		LPDIRECT3DSURFACE9		m_pRenderSurface;
		LPDIRECT3DSURFACE9		m_pRenderZBuffer;

		bool					m_bIsActive;

		D3DPRESENT_PARAMETERS	m_d3dPP;

		//------------------------------------------------------
		// Constructor
		//------------------------------------------------------
		CnDx9RenderTarget( CnWindow* pWindow, CnDx9Renderer* pParent );

		//------------------------------------------------------
		// Prevent Copy..
		//------------------------------------------------------
		CnDx9RenderTarget( const CnDx9RenderTarget& )				{}
		CnDx9RenderTarget& operator =( const CnDx9RenderTarget& )	{}
	public:
		~CnDx9RenderTarget();

		// D3D Informations...
		const D3DPRESENT_PARAMETERS&		GetD3DPresent() const;
		LPDIRECT3DDEVICE9					GetD3DDevice() const;

		// Window
		const CnWindow*						GetWindow() const;

		// Is Active?
		bool								IsActive() const;

		// Restore/Release
		void								Release();
		void								Restore();

		// Resize
		void								Resize( ushort usWidth, ushort usHeight );

		// Viewport
		void								SetViewport( CnViewport* pViewport );

		// Update
		long								Update();
	};

#include "CnDx9RenderTarget.inl"
}

#endif	// __CN_DX9_RENDER_TARGET_H__