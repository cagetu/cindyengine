//================================================================
// File:           : CnDx9ShapeRenderer.h
// Original Author : changhee
// Creation Date   : 2009. 6. 29
//================================================================
#ifndef __CN_DX9_SHAPE_RENDERER_H__
#define __CN_DX9_SHAPE_RENDERER_H__

#include "Graphics/Base/CnShapeRenderer.h"

namespace Cindy
{
	//================================================================
	/** Dx9ShapeRenderer
	    @author		changhee
		@brief		기본 도형을 그리기 위한 랜더러의 기반 클래스
	*/
	//================================================================
	class CnDx9ShapeRenderer : public CnShapeRenderer
	{
		CN_DECLARE_CLASS(CnDx9ShapeRenderer);
	public:
		CnDx9ShapeRenderer();
		virtual ~CnDx9ShapeRenderer();

		void	Open() override;
		void	Close() override;

		void	DrawShape( CnCamera* pCamera,
						   const Math::Matrix44& ModelTransform,
						   CnShapeRenderer::ShapeType eShapeType,
						   const CnColor& Color ) override;

	private:
		ID3DXMesh*	m_ShapeMeshes[CnShapeRenderer::NumShapeTypes];
	};
}

#endif // __CN_DX9_SHAPE_RENDERER_H__