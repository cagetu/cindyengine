//================================================================
// File:           : CnDx9VertexDeclaration.h
// Original Author : changhee
// Creation Date   : 2007. 2. 6
//================================================================
#ifndef __CN_DX9_VERTEXDECLARATION_H__
#define __CN_DX9_VERTEXDECLARATION_H__

#include "Graphics/Base/CnVertexDeclaration.h"

namespace Cindy
{
	//================================================================
	/** Dx9 Vertex Declaration
	    @author    changhee
		@since     2007. 2. 6
		@remarks   Dx9 버전 버텍스 정의 클래스
	*/
	//================================================================
	class CnDx9VertexDeclaration : public CnVertexDeclaration
	{
		__DeclareRtti;
	public:
		CnDx9VertexDeclaration();
		virtual ~CnDx9VertexDeclaration();

		// VertexElement
		const VertexElement&			AddElement( ushort Index, ushort Source, const UsageComponent& Components ) override;
		void							RemoveElement( ushort Index ) override;
		void							RemoveAllElements() override;

		// FVF
		ulong							GetFVF() override;

		// VertexDeclaration
		void							Build() override;
		LPDIRECT3DVERTEXDECLARATION9	GetDeclaration() const;

	private:
		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		IDirect3DVertexDeclaration9*	m_pDeclaration;

		bool							m_bNeedRebuild;
	};

#include "CnDx9VertexDeclaration.inl"
}

#endif	// __CN_DX9_VERTEXDECLARATION_H__