// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "CnDx9Driver.h"

#include <MoCommon/MoStringUtil.h>

namespace Cindy
{
	//--------------------------------------------------------------
	CnDx9Driver::CnDx9Driver( LPDIRECT3D9 pD3D, ushort usIndex, D3DADAPTER_IDENTIFIER9 adapterIdentifer, D3DDISPLAYMODE d3dDispMode )
		: m_pD3D( pD3D )
		, m_usIndex( usIndex )
		, m_AdapterIdentifier( adapterIdentifer )
		, m_CurDisplayMode( d3dDispMode )
		, m_pDisplayModeList( NULL )
	{
	}

	//--------------------------------------------------------------
	CnDx9Driver::~CnDx9Driver()
	{
		SAFEDEL( m_pDisplayModeList );
	}

	//--------------------------------------------------------------
	const CnDx9DisplayModeList* CnDx9Driver::GetDisplayModeList()
	{
		if( !m_pDisplayModeList )
			m_pDisplayModeList = new CnDx9DisplayModeList( this );

		return m_pDisplayModeList;
	}

	//--------------------------------------------------------------
	void CnDx9Driver::GetName( CnString& rResult ) const
	{
		static wchar buffer[256];
		swprintf_s( buffer, L"%S", m_AdapterIdentifier.DeviceName );

		rResult = buffer;
	}

	//--------------------------------------------------------------
	void CnDx9Driver::GetDescription( CnString& rResult ) const
	{
		static wchar buffer[256];
		swprintf_s( buffer, L"%S", m_AdapterIdentifier.Description );

		rResult = buffer;
	}
}