//================================================================
// File:           : CnDx9Types.h
// Original Author : changhee
// Creation Date   : 2007. 4. 9
//================================================================
#ifndef __CN_DX9_TYPES_H__
#define __CN_DX9_TYPES_H__

#include "../CnAntiAliasing.h"
#include "../Base/CnVertexComponent.h"
#include "../Base/CnPixelFormat.h"

namespace Cindy
{
	//================================================================
	/** CnDx9Types
	    @author    changhee
		@since     2008. 2. 4
		@remarks   D3DType���� ����
	*/
	//================================================================
	class CnDx9Types
	{
	public:
		static D3DDECLUSAGE	ToVertexDeclarationUsage( CnVertexComponent::Usage usage );
		static D3DDECLTYPE ToVertexDeclarationType( CnVertexComponent::Format type );

		/// convert antialias quality to D3D multisample type
		static D3DMULTISAMPLE_TYPE ToMultiSampleType( CnAntiAliasQuality::Type type );

		/// 
		static D3DFORMAT ToD3DFormat( PixelFormat::Code format );
		//static PixelFormat::Code ToPixelFormat( D3DFORMAT format );
	};
}

#endif	// __CN_DX9_TYPES_H__
