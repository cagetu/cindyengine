// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
//	About Driver...
//--------------------------------------------------------------
inline
const CnDx9DriverList* CnDx9Renderer::GetDriverList() const
{
	return m_pDriverList;
}

//--------------------------------------------------------------
inline
void CnDx9Renderer::GetCOM( void* pCOM )
{
	LPDIRECT3D9* d3d = (LPDIRECT3D9*)pCOM;
	*d3d = m_pD3D;
}

//--------------------------------------------------------------
inline
const CnDx9Driver* CnDx9Renderer::GetCurrentDriver() const
{
	return m_pCurrentDriver;
}

//--------------------------------------------------------------
inline
const CnDx9DriverCaps* CnDx9Renderer::GetDriverCaps() const
{
	return m_pDriverCaps;
}

//--------------------------------------------------------------
inline
void CnDx9Renderer::GetEffectPool( void* pPool )
{
	ID3DXEffectPool** pool = (ID3DXEffectPool**)pPool;
	*pool = m_pEffectPool;
}
