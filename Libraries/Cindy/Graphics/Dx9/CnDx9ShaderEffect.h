//================================================================
// File:           : CnDx9ShaderEffect.h
// Original Author : changhee
// Creation Date   : 2009. 5. 21
//================================================================
#ifndef __CN_DX9_SHADER_EFFECT_H__
#define __CN_DX9_SHADER_EFFECT_H__

#include <d3dx9shader.h>
#include "../../Material/CnShaderEffect.h"

namespace Cindy
{
	//================================================================
	/** Dx9 ShaderEffect
	    @author    changhee
		@since     2009. 5. 21
		@remarks   쉐이더 Effect 파일 하나에 대한 클래스
	*/
	//================================================================
	class CnDx9ShaderEffect : public CnShaderEffect
	{
		__DeclareRtti;
	public:
		CnDx9ShaderEffect();
		virtual ~CnDx9ShaderEffect();

		// Create
		bool			Create( unsigned char* shaderCode, ulong fileSize ) override;

		/// get pointer to d3d effect
		ID3DXEffect*	GetD3D9Effect() const;

		// Technique
		void			SetTechnique( ShaderHandle hTechnique ) override;

		// Shader 사용 시작/끝
		int				Begin( bool bSaveState = true ) override;
		void			End() override;

		// Device Lost/Restore
		void			OnLost() override;
		void			OnRestore() override;
		void			Clear() override;

		// Pass
		void			BeginPass( uint nPass ) override;
		void			EndPass() override;
		void			CommitChanges() override;

		// Set
		void			SetBool( ShaderHandle hHandle, bool bValue ) override;
		void			SetInt( ShaderHandle hHandle, int val ) override;
		void			SetFloat( ShaderHandle hHandle, float val ) override;
		//void			SetFloat4( ShaderHandle hHandle, float* val ) override;
		void			SetFloatArray( ShaderHandle hHandle, float* aFloats, int nSize ) override;

		void			SetVector3( ShaderHandle hHandle, const Math::Vector3& val ) override;
		void			SetVector4( ShaderHandle hHandle, const Math::Vector4& val ) override;
		void			SetMatrix( ShaderHandle hHandle, const Math::Matrix44& val ) override;

		void			SetVector3Array( ShaderHandle hHandle, const Math::Vector3* aVectors, int nSize ) override;
		void			SetVector4Array( ShaderHandle hHandle, const Math::Vector4* aVectors, int nSize ) override;
		void			SetMatrixArray( ShaderHandle hHandle, const Math::Matrix44* aMatrices, int nSize ) override;

		void			SetTexture( ShaderHandle hHandle, CnTexture* pTexture ) override;
		void			SetTextures( ShaderHandle hHandle, CnTexture** pTextures, int nSize ) override;

		void			SetValue( ShaderHandle hHandle, void* pData, unsigned int nBytes ) override;
		void			SetColor( ShaderHandle hHandle, const CnColor& color ) override;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		ID3DXEffect*	m_pEffect;											//!< Effect Shader

		bool			m_bHasVaildTechnique;								//!< Technique 존재 여부
		bool			m_bInvalidTechnique;								//!< Technique 설정 실패

		uint			m_nNumPass;											//!< Pass의 개수

		bool			m_bHasValidParameters;								//!< 파라미터 목록을 설정했는가?

		//------------------------------------------------------
		// Internal Methods
		//------------------------------------------------------
		void		_UpdateTechniques() override;
		void		_UpdateVaildTechnique();
		void		_UpdateParameterHandles();
	};

	//------------------------------------------------------------------------------
	/**
	*/
	inline ID3DXEffect*
	CnDx9ShaderEffect::GetD3D9Effect() const
	{
		assert(0 != m_pEffect);
		return m_pEffect;
	}
}

#endif	// __CN_DX9_SHADER_H__