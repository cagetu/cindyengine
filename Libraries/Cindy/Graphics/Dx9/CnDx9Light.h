//================================================================
// File:           : CnCnDx9Light.h
// Original Author : changhee
// Creation Date   : 2007. 1. 29
//================================================================
#ifndef __CN_DX9_LIGHT_H__
#define __CN_DX9_LIGHT_H__

namespace Cindy
{
	//==================================================================
	/** CnDx9Light Class
		@author			cagetu
		@since			2007년 1월 29일
		@remarks		DirectX 9 버전 라이트
	*/
	//==================================================================
	class CnDx9Light
	{
	public:
		//------------------------------------------------------
		// Construct
		//------------------------------------------------------
		CnDx9Light() {}
		CnDx9Light( const D3DLIGHT9& light );

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void				SetAmbient( float r, float g, float b, float a );
		void				SetDiffuse( float r, float g, float b, float a );
		void				SetSpecular( float r, float g, float b, float a );

		void				SetPosition( float x, float y, float z );
		void				SetDirection( float x, float y, float z );

		void				SetAttenuation( float atten0, float atten1, float atten2 );
		void				SetRange( float range );

		void				SetConeAngle( float theta, float phi, float falloff );

		const D3DLIGHT9*	GetLight() const;

	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		D3DLIGHT9			m_d3dLight;
	};

#include "CnDx9Light.inl"
} // end of namespace 

#endif	// __CN_DX9_LIGHT_H__