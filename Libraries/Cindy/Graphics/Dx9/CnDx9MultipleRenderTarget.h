//================================================================
// File:           : CnDx9MultipleRenderTarget.h
// Original Author : changhee
// Creation Date   : 2008. 1. 25
//================================================================
#pragma once

#include "Graphics/Base/CnMultipleRenderTarget.h"

namespace Cindy
{
	//================================================================
	/** Dx9 Multiple Render Target
	    @author    changhee
		@since     2008. 1. 25
		@remarks   Dx9용 MRT를 구현한다.
	*/
	//================================================================
	class CN_DLL CnDx9MultiRenderTarget : public CnMultiRenderTarget
	{
		__DeclareRtti;
	public:
		CnDx9MultiRenderTarget( const wchar* strName );
		virtual ~CnDx9MultiRenderTarget();

		bool		Update() override;
//		void		Draw() override;

		//
		void		Begin() override;
		void		Draw() override;
		void		End() override;

	protected:
		LPDIRECT3DSURFACE9	m_d3dColorBuffer;
		LPDIRECT3DSURFACE9	m_d3dDepthStencilBuffer;
	};
}
