//================================================================
// File:               : CnRenderer.cpp
// Related Header File : CnRenderer.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 20
//================================================================
#include "Cindy.h"
#include "CnRenderer.h"

#include "Graphics/Dx9/CnDx9Renderer.h"

namespace Cindy
{
	//================================================================
	//	IRenderer
	//================================================================
	/// Constructor
	IRenderer::IRenderer()
	{
	}
	/// Destructor
	IRenderer::~IRenderer()
	{
	}

	//================================================================
	//	CnRenderer
	//================================================================
	CnRenderer* CnRenderer::ms_pInstance = NULL;
	/// Constructor
	CnRenderer::CnRenderer()
		: m_pRenderer(NULL)
	{
		Assert( !ms_pInstance, L"[CnRenderer::CnRenderer]" );
		ms_pInstance = this;

		m_eShadeType = Forward;
	}
	/// Destructor
	CnRenderer::~CnRenderer()
	{
		//Close();
		ms_pInstance = NULL;
	}

	//----------------------------------------------------------------
	/** Create
	    @remarks      랜더러 생성
		@param        eType : 랜더러 타입
		@return       none
	*/
	//----------------------------------------------------------------
	void CnRenderer::Open( CnRenderer::TYPE eType )
	{
		if (m_pRenderer)
			return;

		switch (eType)
		{
		case D3D9:
			m_pRenderer = new CnDx9Renderer();
			break;

		default:
			m_pRenderer = NULL;
			break;
		}

		m_eType = eType;
	}

	//----------------------------------------------------------------
	/** Delete
	    @remarks      랜더러 삭제
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnRenderer::Close()
	{
		SAFEDEL( m_pRenderer );
	}
}