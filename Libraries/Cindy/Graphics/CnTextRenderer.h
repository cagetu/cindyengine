//================================================================
// File:           : CnTextRenderer.h
// Original Author : changhee
// Creation Date   : 2009. 9. 3
//================================================================
#ifndef __CN_TEXT_RENDERER_H__
#define __CN_TEXT_RENDERER_H__

#include "Foundation/CnObject.h"
#include "Base/CnTextElement.h"

namespace Cindy
{
	class CnCamera;

	//================================================================
	/** Text Renderer Class
	    @author    changhee
		@brief	   기본 Text 랜더러
	*/
	//================================================================
	class CnTextRenderer : public CnObject
	{
		__DeclareRtti;
	public:
		typedef std::vector<CnTextElement>	TextElementArray;

		//------------------------------------------------------------------
		// Methods
		//------------------------------------------------------------------
		CnTextRenderer();
		virtual ~CnTextRenderer();

		virtual void Open();
		virtual void Close();

		virtual void DrawText( CnCamera* camera );

		void AddTextElement( const CnTextElement& textElement );
		void AddTextElements( const TextElementArray& textElements );

	protected:
		TextElementArray	m_TextElements;
		bool				m_Opened;
	};

	//================================================================
	/** Text Renderer Proxy
	    @author    changhee
		@brief	   Text 랜더러 대행
	*/
	//================================================================
	class CnTextRendererProxy
	{
	public:
		CnTextRendererProxy();
		~CnTextRendererProxy();

		void Open();
		void Close();

		bool IsNull() const;
		CnTextRenderer* operator ->() const;

		static CnTextRendererProxy&	Instance();
	private:
		CnTextRenderer* m_TextRenderer;

		static CnTextRendererProxy*	ms_pSingleton;
	};
} // namespace Cindy

#define TextRenderer	Cindy::CnTextRendererProxy::Instance()

#define _debug_text(txt, pos, color) \
	CnTextElement elem(txt, color, pos); \
	TextRenderer->AddTextElement(elem);

#define _debug_text3D(txt, pos, color) \
	CnTextElement elem(txt, color, pos, true); \
	TextRenderer->AddTextElement(elem);

#endif // __CN_TEXT_RENDERER_H__
