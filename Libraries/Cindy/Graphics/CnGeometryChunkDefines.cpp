//================================================================
// File:               : CnGeometryChunkDefines.cpp
// Related Header File : CnGeometryChunkDefines.h
// Original Author     : changhee
// Creation Date       : 2009. 12. 03
//================================================================
#include "../Cindy.h"
#include "CnGeometryChunkDefines.h"
#include "Util/CnString.h"

namespace Cindy
{
namespace GeometryChunk
{
	//----------------------------------------------------------------
	/**	gs_TypeTable
	*/
	//----------------------------------------------------------------
	static const wchar* gs_TypeTable[GeometryChunk::NumDefines] =
	{
		// Transform
		L"Opaque",
		L"AlphaTest",
		L"AlphaBlend",
	};

	//----------------------------------------------------------------
	/** StringToType
	*/
	//----------------------------------------------------------------
	GeometryChunk::TypeDefines
	GeometryChunk::StringToType( const wchar* name )
	{
		for( uint i = 0; i < GeometryChunk::NumDefines; ++i )
		{
			if (unicode::Compare( gs_TypeTable[i], name ))
				return (GeometryChunk::TypeDefines)i;
		}
		return GeometryChunk::InvalidDefine;
	}

	//----------------------------------------------------------------
	/** TypeToString
	*/
	//----------------------------------------------------------------
	const wchar*
	GeometryChunk::TypeToString( GeometryChunk::TypeDefines type )
	{
		return gs_TypeTable[type];
	}
}

}