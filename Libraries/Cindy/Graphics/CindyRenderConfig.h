// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __CINDYRENDERCONFIG_H__
#define __CINDYRENDERCONFIG_H__

//------------------------------------------------------------------------------
#define CINDY_USEDIRECT3D9 (1)
#define CINDY_USEDIRECT3D10 (0)

#define CINDY_DIRECT3D_USENVPERFHUD (0)
#define CINDY_DIRECT3D_DEBUG (0)

#if CINDY_DIRECT3D_USENVPERFHUD
#define CINDY_DIRECT3D_DEVICETYPE D3DDEVTYPE_REF
#else
#define CINDY_DIRECT3D_DEVICETYPE D3DDEVTYPE_HAL
#endif

#endif	// __CINDYRENDERCONFIG_H__