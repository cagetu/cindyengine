//================================================================
// File:           : CnGeometryChunkDefines.h
// Original Author : changhee
// Creation Date   : 2009. 12. 03
//================================================================
#ifndef __CN_GEOMETRY_CHUNK_DEFINES_H__
#define __CN_GEOMETRY_CHUNK_DEFINES_H__

namespace Cindy
{
namespace GeometryChunk
{
	enum TypeDefines
	{
		Opaque,
		AlphaTest,
		AlphaBlend,
		NumDefines,
		InvalidDefine,
	};

	/// Type
	TypeDefines		StringToType( const wchar* name );
	const wchar*	TypeToString( TypeDefines type );
} // namespace GeometryChunk
} // namespace Cindy

#endif // __CN_GEOMETRY_CHUNK_DEFINES_H__