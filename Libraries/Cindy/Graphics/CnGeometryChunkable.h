//================================================================
// File:           : CnGeometryChunkable.h
// Original Author : changhee
// Creation Date   : 2009. 12. 03
//================================================================
#ifndef __CN_GEOMETRY_CHUNKABLE_H__
#define __CN_GEOMETRY_CHUNKABLE_H__

#include "CnGeometryChunk.h"
#include "CnGeometryChunkDefines.h"

namespace Cindy
{
	//================================================================
	/** GeometryChunkable
	    @author		changhee
		@since		2007. 1. 27
		@remarks	GeometryChunk를 사용하는 클래스
	*/
	//================================================================
	class CnGeometryChunkable
	{
	public:
		CnGeometryChunkable();
		virtual ~CnGeometryChunkable();

		/// Chunks
		void						AddChunk( GeometryChunk::TypeDefines ChunkType, const Ptr<CnGeometryChunk>& Chunk );
		void						RemoveChunks();
		const Ptr<CnGeometryChunk>&	GetChunk( GeometryChunk::TypeDefines ChunkType ) const;

	protected:
		typedef std::map<GeometryChunk::TypeDefines, Ptr<CnGeometryChunk> >	ChunkMap;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		ChunkMap		m_Chunks;
	};
}

#endif	// __CN_GEOMETRY_CHUNKABLE_H__