//================================================================
// File:               : CnRenderTarget.cpp
// Related Header File : CnRenderTarget.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 18
//================================================================
#include "../Cindy.h"
#include "CnRenderTarget.h"
#include "CnRenderer.h"

namespace Cindy
{
	//----------------------------------------------------------------
	__ImplementRtti( Cindy, CnRenderTarget, CnObject );

	/// Const/Dest
	CnRenderTarget::CnRenderTarget()
		: m_nWidth( 0 )
		, m_nHeight( 0 )
		, m_eType( RT_NONE )
		, m_nPriority( 0 )
		, m_bIsActive( true )
		, m_eAntiAliasQulity( CnAntiAliasQuality::None )
		, m_bEnableDepthStencil( false )
		, m_bBeginPass( false )
	{
	}
	CnRenderTarget::~CnRenderTarget()
	{
		RemoveAllViewports();
	}

	//----------------------------------------------------------------
	/**	@brief 정렬
	*/
	bool CnRenderTarget::Sort( const CnRenderTarget* Dest, const CnRenderTarget* Src )
	{
		if (Dest->GetType() < Src->GetType())
			return true;
		
		if (Dest->GetType() == Src->GetType() && Dest->GetPriority() < Src->GetPriority() )
			return true;

		return false;
	};

	//----------------------------------------------------------------
	/**	@brief	우선 순위 설정
	*/
	void CnRenderTarget::SetPriority( uint nPriority )
	{
		m_nPriority = nPriority;

		RenderDevice->NeedToSortRenderTargets();
	}

	//----------------------------------------------------------------
	/** Add Viewport
	    @brief      뷰포트를 생성한다.
		@param        nZOrder : 뷰포트의 z 값( 뷰포트가 여러개 있을 때 정렬값으로 이용)
		@param        fRelTop : 상대적인 뷰포트 Top 위치( 0.0f ~ 1.0f )
		@param        fRelLeft : 상대적인 뷰포트 Left 위치( 0.0f ~ 1.0f )
		@param        fRelWidth : 상대적인 뷰포트 Width 위치( 0.0f ~ 1.0f )
		@param        fRelHeight : 상대적인 뷰포트 Height 위치( 0.0f ~ 1.0f )
		@return       CnViewport : 생성된 뷰포트
	*/
	//----------------------------------------------------------------
	CnViewport* CnRenderTarget::AddViewport( int nZOrder, float fRelTop, float fRelLeft, float fRelWidth, float fRelHeight )
	{
		ViewportIter iter = m_Viewports.find( nZOrder );
		if( iter == m_Viewports.end() )
		{
			CnViewport* viewport = CnNew CnViewport( this, nZOrder, fRelTop, fRelLeft, fRelWidth, fRelHeight );
			m_Viewports.insert( ViewportMap::value_type( nZOrder, viewport ) );

			return viewport;
		}

		return iter->second;
	}

	//----------------------------------------------------------------
	/** Get Viewport
	    @brief      뷰포트를 얻어온다.
		@param        nZOrder : 뷰포트의 z 값( 뷰포트가 여러개 있을 때 정렬값으로 이용)
		@return       CnViewport : 뷰포트
	*/
	//----------------------------------------------------------------
	CnViewport* CnRenderTarget::GetViewport( int nZOrder )
	{
		ViewportIter iter = m_Viewports.find( nZOrder );
		if( iter == m_Viewports.end() )
			return NULL;

		return iter->second;
	}

	//----------------------------------------------------------------
	/** Remove Viewport
	    @brief      뷰포트를 제거한다.
		@param        nZOrder : 뷰포트의 z 값( 뷰포트가 여러개 있을 때 정렬값으로 이용)
		@return       true/false
	*/
	//----------------------------------------------------------------
	bool CnRenderTarget::RemoveViewport( int nZOrder )
	{
		ViewportIter iter = m_Viewports.find( nZOrder );
		if( iter == m_Viewports.end() )
			return false;

		delete( iter->second );
		m_Viewports.erase( iter );
		return true;
	}

	//----------------------------------------------------------------
	/** Remove All Viewports
	    @brief      모든 뷰포트를 제거한다.
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::RemoveAllViewports()
	{
		ViewportIter iend = m_Viewports.end();
		for( ViewportIter i = m_Viewports.begin(); i != iend; ++i )
			delete (i->second);
		m_Viewports.clear();
	}

	//----------------------------------------------------------------
	/** Resize
		@brief      윈도우의 사이즈를 갱신한다.
		@param        nWidth : 가로 길이
		@param		  nHeight : 세로 길이
		@return       none
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::Resize( int nWidth, int nHeight )
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;

		ViewportIter iend = m_Viewports.end(); 
		for( ViewportIter i = m_Viewports.begin(); i != iend; ++i )
		{
			i->second->UpdateDimension();
		}
	}

	//----------------------------------------------------------------
	/** Release
		@brief      디바이스 해제시 설정
		@param		  none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::OnRelease()
	{// Blank
	}

	//----------------------------------------------------------------
	/** Restore
		@brief      디바이스 복구시 설정
		@param		  none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::OnRestore()
	{// Blank
	}

	//----------------------------------------------------------------
	/** Reset
		@brief      디바이스 리셋
		@param		  none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::OnReset()
	{// Blank
	}

	//----------------------------------------------------------------
	/** Restore
		@brief      디바이스 복구시 설정
		@param		  none
		@return       none
	*/
	//----------------------------------------------------------------
	bool CnRenderTarget::_RestoreLostDevice()
	{
		return true;
	}

	//----------------------------------------------------------------
	/** Update
	    @brief      윈도우를 업데이트 한다
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	bool CnRenderTarget::Update()
	{
		RenderDevice->SetCurrentRenderTarget( this );

		ViewportIter iend = m_Viewports.end(); 
		for( ViewportIter i = m_Viewports.begin(); i != iend; ++i )
		{
			i->second->Update();
			i->second->Draw();
		}

		return true;
	}

	//----------------------------------------------------------------
	/**	Begin
		@brief		RenderTarget 랜더링 시작
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::Begin()
	{
		assert(!m_bBeginPass);
		m_bBeginPass = true;

		RenderDevice->SetCurrentRenderTarget( this );
	}

	//----------------------------------------------------------------
	/**	Draw
		@brief		RenderTarget 랜더링!!!
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::Draw()
	{
		assert(!m_bBeginPass);
	//	ViewportIter iend = m_Viewports.end(); 
	//	for( ViewportIter i = m_Viewports.begin(); i != iend; ++i )
	//	{
	//		i->second->Draw();
	//	}
	}

	//----------------------------------------------------------------
	/**	End
		@brief		RenderTarget 랜더링 끝
	*/
	//----------------------------------------------------------------
	void CnRenderTarget::End()
	{
		assert(m_bBeginPass);
		m_bBeginPass = false;
	}
}