//================================================================
// File:           : CnGeometryQueue.h
// Original Author : changhee
// Creation Date   : 2009. 12. 03
//================================================================
#ifndef __CN_GEOMETRY_QUEUE_H__
#define __CN_GEOMETRY_QUEUE_H__

#include "CnGeometryChunkable.h"
#include "Util/CnUnCopyable.h"

namespace Cindy
{
	class CnMesh;
	class CnCamera;

	//================================================================
	/** RenderQueue
	    @author    changhee
		@since     2007. 1. 27
		@remarks   화면에 그릴 녀석들을 모아 놓은 큐
	*/
	//================================================================
	class CnGeometryQueue : public CnGeometryChunkable
	{
		__DeclareUnCopy(CnGeometryQueue);
	public:
		CnGeometryQueue();
		virtual ~CnGeometryQueue();

		void		Insert( CnMesh* pMesh );
		void		Clear();

		void		Draw( CnCamera* pCamera );
	};
}

#endif	// __CN_GEOMETRY_QUEUE_H__