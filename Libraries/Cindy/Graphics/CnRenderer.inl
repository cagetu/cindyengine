//================================================================
// File:               : CnRenderer.inl
// Related Header File : CnRenderer.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 20
//================================================================

//----------------------------------------------------------------
/**
*/
inline
CnRenderer& CnRenderer::Instance()
{
	return *ms_pInstance;
}

//----------------------------------------------------------------
/**
*/
inline
bool CnRenderer::IsEmpty() const
{
	return (0==m_pRenderer) ? true : false;
}

//----------------------------------------------------------------
/**
*/
inline
CnRenderer::TYPE CnRenderer::GetType() const
{
	return m_eType;
}

//----------------------------------------------------------------
/**
*/
inline
void CnRenderer::SetShadeType( CnRenderer::ShadeType eType )
{
	m_eShadeType = eType;
}

//----------------------------------------------------------------
/**
*/
inline
CnRenderer::ShadeType CnRenderer::GetShadeType() const
{
	return m_eShadeType;
}

//----------------------------------------------------------------
/**
*/
inline
IRenderer* CnRenderer::operator->() const
{
	assert( m_pRenderer );
	return m_pRenderer;
}

//----------------------------------------------------------------
/**
*/
inline
IRenderer* CnRenderer::GetPtr() const
{
	return m_pRenderer;
}

