//================================================================
// File:               : CnTextRenderer.cpp
// Related Header File : CnTextRenderer.h
// Original Author     : changhee
// Creation Date       : 2009. 9. 3
//================================================================
#include "Cindy.h"
#include "CnTextRenderer.h"
#include "CnRenderer.h"
#include "Dx9/CnDx9TextRenderer.h"

namespace Cindy
{
	//==================================================================
	__ImplementRtti(Cindy, CnTextRenderer, CnObject);

	/// Constructor
	CnTextRenderer::CnTextRenderer()
		: m_Opened(false)
	{
	}
	/// Destructor
	CnTextRenderer::~CnTextRenderer()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	void
	CnTextRenderer::Open()
	{
		m_Opened = true;
	}

	//------------------------------------------------------------------
	/**
	*/
	void
	CnTextRenderer::Close()
	{
		m_TextElements.clear();
		m_Opened = false;
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnTextRenderer::DrawText( CnCamera* camera )
	{
		m_TextElements.clear();
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnTextRenderer::AddTextElement( const CnTextElement& textElement )
	{
		m_TextElements.push_back(textElement);
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnTextRenderer::AddTextElements( const CnTextRenderer::TextElementArray& textElements )
	{
		TextElementArray::const_iterator iend = textElements.end();
		for (TextElementArray::const_iterator i = textElements.begin(); i < iend; i++)
		{
			m_TextElements.push_back( (*i) );
		}
	}


	//==================================================================
	//	Text Renderer Proxy
	//==================================================================
	CnTextRendererProxy* CnTextRendererProxy::ms_pSingleton = NULL;
	//------------------------------------------------------------------
	CnTextRendererProxy& CnTextRendererProxy::Instance()
	{		
		assert( ms_pSingleton );
		return (*ms_pSingleton );
	}

	//------------------------------------------------------------------
	CnTextRendererProxy::CnTextRendererProxy()
		: m_TextRenderer(0)
	{
		assert( !ms_pSingleton );
		ms_pSingleton = this;
	}
	CnTextRendererProxy::~CnTextRendererProxy()
	{
		Close();

		assert( ms_pSingleton );
		ms_pSingleton = 0;
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnTextRendererProxy::Open()
	{
		assert(m_TextRenderer == NULL);

		if (RenderDevice.GetType() == CnRenderer::D3D9)
		{
			m_TextRenderer = CnNew CnDx9TextRenderer();
			m_TextRenderer->Open();
		}
	}

	//------------------------------------------------------------------
	/**
	*/
	void CnTextRendererProxy::Close()
	{
		SAFEDEL( m_TextRenderer );
	}

	//------------------------------------------------------------------
	/**
	*/
	bool CnTextRendererProxy::IsNull() const
	{
		return (m_TextRenderer == NULL) ? true : false;
	}

	//------------------------------------------------------------------
	/**
	*/
	CnTextRenderer* CnTextRendererProxy::operator ->() const
	{
		return m_TextRenderer;
	}
} // namespace Cindy