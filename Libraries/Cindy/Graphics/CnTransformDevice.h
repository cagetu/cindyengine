//================================================================
// File:           : CnTransformDevice.h
// Original Author : changhee
// Creation Date   : 2009. 12. 8
//================================================================
#ifndef __CN_TRANSFORM_DEVICE_H__
#define __CN_TRANSFORM_DEVICE_H__

#include "Math/CnMatrix44.h"
#include "Util/CnUnCopyable.h"

namespace Cindy
{
	//==================================================================
	/** Transform Device
		@author		cagetu
		@brief		변환 행렬 관리
	*/
	//==================================================================
	class CnTransformDevice
	{
		__DeclareUnCopy(CnTransformDevice);
	public:
		CnTransformDevice();

		void	SetViewTransform( const Math::Matrix44& Transform );
		void	SetProjTransform( const Math::Matrix44& Transform );
		void	SetViewProjTransform( const Math::Matrix44& Transform );

		const Math::Matrix44&	GetViewTransform() const;
		const Math::Matrix44&	GetProjTransform() const;
		const Math::Matrix44&	GetViewProjTransform() const;

		static CnTransformDevice* Instance();
	private:
		Math::Matrix44	m_ViewTransform;
		Math::Matrix44	m_ProjTransform;
		Math::Matrix44	m_ViewProjTransform;
	};

	//----------------------------------------------------------------
	/**
	*/
	inline
	void CnTransformDevice::SetViewTransform( const Math::Matrix44& Transform )
	{
		m_ViewTransform = Transform;
		m_ViewProjTransform = m_ViewTransform * m_ProjTransform;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	void CnTransformDevice::SetProjTransform( const Math::Matrix44& Transform )
	{
		m_ProjTransform = Transform;
		m_ViewProjTransform = m_ViewTransform * m_ProjTransform;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	void CnTransformDevice::SetViewProjTransform( const Math::Matrix44& Transform )
	{
		m_ViewProjTransform = Transform;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline const Math::Matrix44&
	CnTransformDevice::GetViewTransform() const
	{
		return m_ViewTransform;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline const Math::Matrix44&
	CnTransformDevice::GetProjTransform() const
	{
		return m_ProjTransform;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline const Math::Matrix44&
	CnTransformDevice::GetViewProjTransform() const
	{
		return m_ViewProjTransform;
	}

} // namespace Cindy

#define	TransformDevice	Cindy::CnTransformDevice::Instance()

#endif // __CN_TRANSFORM_DEVICE_H__