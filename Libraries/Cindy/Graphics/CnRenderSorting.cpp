//================================================================
// File:               : CnRenderSorting.cpp
// Related Header File : CnRenderSorting.h
// Original Author     : changhee
// Creation Date       : 2009. 11. 31
//================================================================
#include "../Cindy.h"
#include "Graphics/CnRenderSorting.h"
#include "Geometry/CnMesh.h"
#include "Util/CnString.h"

namespace Cindy
{
namespace Sorting
{
	//----------------------------------------------------------------
	/**
	*/
	Comparision StringToFunc( const wchar* name )
	{
		CnString string(name);
		unicode::LowerCase( string );
		
		if (unicode::Compare(string.c_str(), L"default"))
		{
			return Sorting::Default;
		}
		else if (unicode::Compare(string.c_str(), L"greater"))
		{
			return Sorting::Greater;
		}
		else if (unicode::Compare(string.c_str(), L"less"))
		{
			return Sorting::Less;
		}
		else if (unicode::Compare(string.c_str(), L"fronttoback"))
		{
			return Sorting::FrontToBack;
		}
		else if (unicode::Compare(string.c_str(), L"backtofront"))
		{
			return Sorting::BackToFront;
		}
		else
		{
			assert(0);
		}

		return 0;
	}

	//----------------------------------------------------------------
	/**	정렬을 하지 않는다.
	*/
	bool Default( const CnSubMesh* Dest, const CnSubMesh* Src )
	{
		return false;
	}

	//----------------------------------------------------------------
	/**	SortKey가 크도록 정렬 - Opaque 객체들을 정렬할 때 사용
	*/
	bool Greater( const CnSubMesh* Dest, const CnSubMesh* Src )
	{
		return Dest->GetSortKey() < Src->GetSortKey();
	}

	//----------------------------------------------------------------
	/**	SortKey가 작도록 정렬 - 알파 블랜딩 객체들을 정렬할 때 사용
	*/
	bool Less( const CnSubMesh* Dest, const CnSubMesh* Src )
	{
		return Dest->GetSortKey() > Src->GetSortKey();
	};

	//----------------------------------------------------------------
	/**	가까운 녀석부터 정렬
	*/
	bool FrontToBack( const CnSubMesh* Dest, const CnSubMesh* Src )
	{
		return Dest->GetParent()->GetDistanceToCamera() < Dest->GetParent()->GetDistanceToCamera();
	}

	//----------------------------------------------------------------
	/**	먼 녀석부터 정렬
	*/
	bool BackToFront( const CnSubMesh* Dest, const CnSubMesh* Src )
	{
		return Dest->GetParent()->GetDistanceToCamera() > Dest->GetParent()->GetDistanceToCamera();
	}

} // namespace Sorting
} // namespace Cindy