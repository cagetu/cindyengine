//================================================================
// File:               : CnGeometryChunk.cpp
// Related Header File : CnGeometryChunk.h
// Original Author     : changhee
// Creation Date       : 2009. 11. 31
//================================================================
#include "../Cindy.h"
#include "CnGeometryChunk.h"
#include "CnGeometryQueue.h"
#include "Graphics/CnRenderer.h"
#include "Scene/CnCamera.h"
#include "Geometry/CnMesh.h"
#include "Material/CnShaderManager.h"

namespace Cindy
{
	//================================================================
	__ImplementClass(Cindy, CnGeometryChunk, CnObject);
	//----------------------------------------------------------------
	/**
	*/
	CnGeometryChunk::CnGeometryChunk()
		: m_ShaderFeature(ShaderFeature::InvalidFeature)
	{
		m_Comparision = Sorting::Default;

		m_Meshes.reserve(128);
	}

	//----------------------------------------------------------------
	/**
	*/
	CnGeometryChunk::~CnGeometryChunk()
	{
		Clear();
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunk::Add( CnSubMesh* SubMesh )
	{
		m_Meshes.push_back( SubMesh );
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunk::Clear()
	{
		m_Meshes.clear();
	}

	//--------------------------------------------------------------
	void CnGeometryChunk::SetRenderState( const wchar* FileName )
	{
		m_Shader = ShaderMgr->Load( FileName, 0 );
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunk::Sort()
	{
		std::sort( m_Meshes.begin(), m_Meshes.end(), m_Comparision );
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunk::Begin()
	{
		if (m_Shader.IsValid())
		{
			int numPasses = m_Shader->Begin();
			assert(1 == numPasses);
			m_Shader->BeginPass(0);
			m_Shader->CommitChanges();
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunk::End()
	{
		if (m_Shader.IsValid())
		{
			m_Shader->EndPass();
			m_Shader->End();
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnGeometryChunk::Draw( CnCamera* Camera )
	{
		Begin();
		{
			Sort();

			MeshIter iend = m_Meshes.end();
			for (MeshIter i = m_Meshes.begin(); i != iend; i++)
			{
				CnSubMesh* subMesh = (*i);
				CnMesh* parentMesh = subMesh->GetParent();

				const Ptr<CnMaterial>& material = subMesh->GetMaterial();
				material->AddFeature( m_ShaderFeature );
				subMesh->UpdateMaterial();

				/// Vertex Information
				RenderDevice->SetVertexDeclaration( parentMesh->GetVertexData()->GetDeclaration() );
				RenderDevice->SetStreamSource( parentMesh->GetVertexData() );
				RenderDevice->SetIndexBuffer( subMesh->GetIndexBuffer() );

				/// Apply Material(Shader)
				RenderDevice->SetMaterial( material );

				/// Draw
				//CnTechnique* technique = material->GetCurrentTechnique();
				CnTechnique* technique = material->GetShader()->GetDefaultTechnique();
				if (technique)
				{
					uint numPasses = technique->Begin();
					Assert(numPasses > 0, L"Invalid EffectPass Size");

					subMesh->ApplyState( Camera );

					for (uint curPass = 0; curPass < numPasses; curPass++)
					{
						CnPass* pass = technique->GetPass(curPass);
						Assert(pass, L"Pass == NULL");

						pass->Begin();
						{
							subMesh->Draw( Camera );
						}
						pass->End();
					} // for
				} // if

				material->RemoveFeature( m_ShaderFeature );
			} // for
		}
		End();
	}
}