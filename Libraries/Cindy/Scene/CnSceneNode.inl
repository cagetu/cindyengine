//================================================================
// File:               : CnSceneNode.inl
// Related Header File : CnSceneNode.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 26
//================================================================

inline
void CnSceneNode::Visible( bool bVisible )
{
	m_bVisible = bVisible;
}
inline
bool CnSceneNode::IsVisible() const
{
	return m_bVisible;
}

inline
void CnSceneNode::EnableCulling( bool bEnalbe )
{
	m_bUseCulling = bEnalbe;
}
inline
bool CnSceneNode::UseCulling() const
{
	return m_bUseCulling;
}

inline
void CnSceneNode::GetBound( Math::AABB* pBox )
{
	*pBox = m_Bound;
}

inline
const CnSceneNode::EntityMap& CnSceneNode::GetAllEntities() const
{
	return m_SceneEntities;
}
