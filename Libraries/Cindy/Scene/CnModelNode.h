//================================================================
// File:           : CnModelNode.h
// Original Author : changhee
// Creation Date   : 2007. 4. 16
//================================================================
#pragma once

#include "CnSceneNode.h"
#include "../Geometry/CnMeshData.h"
#include "../Material/CnMaterialData.h"
#include "../Animation/CnSkeletonInstance.h"
#include "../Animation/CnAnimationSequencer.h"
#include "../Animation/CnIKController.h"

#include <MoCommon/Thread/MoCriticalSection.h>

namespace Cindy
{
	class CnSkeletonInstance;

	//================================================================
	/** CnModelNode
	    @author    changhee
		@since     2007. 4. 16
		@remarks   메쉬, 뼈대, 애니메이션들을 붙이는 장면 노드
	*/
	//================================================================
	class CN_DLL CnModelNode : public CnSceneNode
	{
		__DeclareClass(CnModelNode);
	public:
		// 메쉬들..
		typedef HashMap<CnString, MeshDataPtr>	MeshDataList;
		typedef MeshDataList::iterator			MeshDataIter;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnModelNode();
		CnModelNode( const wchar* strName, ushort usIndex = 0xffff );
		virtual ~CnModelNode();

		// Mesh
		bool							AddMesh( const CnString& MeshName, const CnString& MtlName, bool CastShadow = false );
		bool							RemoveMesh( const CnString& MeshName );
		void							RemveAllMeshes();
		bool							HasMesh( const CnString& MeshName ) const;

		// Skeleton
		bool							AddSkeleton( const CnString& strName );
		bool							AddSkeleton( const SkelDataPtr& spSkeleton );
		bool							RemoveSkeleton();
		const Ptr<CnSkeletonInstance>&	GetSkeleton() const;

		// Animation
		CnAnimChannel*					AddAnimation( PRIORITY Priority, const AnimDataPtr& spData, bool bRepeat = true, float fBlendWeight = 0.0f, float fBlendRate = 1.0f );
		CnAnimChannel*					AddAnimation( PRIORITY Priority, const wchar* fname, bool bRepeat = true, float fBlendWeight = 0.0f, float fBlendRate = 1.0f );
		void							RemoveAnimation( PRIORITY Priority );
		const Ptr<CnAnimController>&	GetAnimController() const;

		// IK
		void							AddIKController( CnIKController* pController );

		// Shadow
		void							SetCastShadow( bool bCast );
		void							SetEnableReceiveShadow( bool bEnable );
	private:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		MeshDataList			m_MeshDatas;	// 메쉬 리스트

		Ptr<CnSkeletonInstance>	m_Skeleton;			// 뼈다구
		Ptr<CnAnimController>	m_AnimController;	// 애니메이션
		Ptr<CnIKController>		m_IKController;

		GlobalStatePtr			m_States[CnGlobalState::MAX_STATE];

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		// Mesh Node
		bool	_BuildMeshNodes( const MeshDataPtr& spMesh, const MtlDataPtr& spMaterial, bool bCastShadow );
		bool	_ClearMeshNodes( const MeshDataPtr& spMesh );

		// Mesh Entity
		bool	_BuildMeshEntities( CnSceneNode* pMeshNode, Export::MeshNode* pMeshNodeData,
									const MtlDataPtr& spMaterial, bool bCastShadow );

		// Skeleton
		bool	_BuildSkeleton( const SkelDataPtr& spSkeleton );

		// Controller
		void	_UpdateAfterCulling() override;

		// 상태
		void	UpdateState( std::vector<CnGlobalState*>* paGSStack ) override;
	};

#include "CnModelNode.inl"
}