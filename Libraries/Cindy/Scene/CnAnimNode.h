//================================================================
// File:           : CnAnimNode.h
// Original Author : changhee
//================================================================
#ifndef __CN_ANIMATENODE_H__
#define __CN_ANIMATENODE_H__

#include "CnNode.h"

namespace Cindy
{
	class CnAnimChannel;

	//================================================================
	/** AnimateNode
	    @author		changhee
		@brief		Animate 기능이 있는 노드
	*/
	//================================================================
	class CN_DLL CnAnimNode : public CnNode
	{
		__DeclareRtti;
	public:
		CnAnimNode();
		CnAnimNode( const wchar* strName, ushort nIndex );
		virtual ~CnAnimNode();

	protected:
		friend class CnAnimChannel;
		friend class CnSkeletonInstance;

		virtual void	_Animate( CnAnimChannel* pChannel, bool bNeedUpdate = true );
	};
}

#endif	// __CN_ANIMATEOBJECT_H__
