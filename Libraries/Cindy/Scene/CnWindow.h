//================================================================
// File					: CnWindow.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 23.
//================================================================
#ifndef __CN_WINDOW_H__
#define __CN_WINDOW_H__

namespace Cindy
{
	class CnViewport;

	//==================================================================
	/** CnWindow
		@author			cagetu
		@since			2007년 1월 23일
		@remarks		윈도우
						랜더링 할 윈도우
	*/
	//==================================================================
	class CN_DLL CnWindow
	{
		// List of viewports, map on Z-order
		typedef std::map<int, CnViewport*, std::less<int> >		ViewportList;
		typedef ViewportList::iterator							ViewportIter;
	private:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		HWND			m_hWnd;				//!< 윈도우 핸들
		int				m_nWidth;
		int				m_nHeight;

		CnString		m_strName;

		ViewportList	m_Viewports;
	public:
		CnWindow( HWND hWnd, const CnString& strName, int nWidth, int nHeight );
		~CnWindow();

		// Get Window Informations
		HWND			GetHWnd() const;
		int				GetWidth() const;
		int				GetHeight() const;
		const CnString&	GetName() const;

		// Resize
		void			Resize( int nWidth, int nHeight );

		// Viewport
		CnViewport*		CreateViewport( int nZOrder = 0, float fRelTop = 0.0f, float fRelLeft = 0.0f,
										float fRelWidth = 1.0f, float fRelHeight = 1.0f );

		// Update
		void			Update();
	};

#include "CnWindow.inl"
}

#endif	// __CN_WINDOW_H__