// Copyright (c) 2006~. cagetu
//
//******************************************************************
#include "../Cindy.h"
#include "CnViewport.h"
#include "CnCamera.h"
#include "CnScene.h"

#include "Graphics/CnRenderTarget.h"
#include "Graphics/CnRenderer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnViewport, CnObject);
	//----------------------------------------------------------------
	CnViewport::CnViewport( CnRenderTarget* pParent,
							int nZOrder,
							float fRelTop,
							float fRelLeft,
							float fRelWidth,
							float fRelHeight )
		: m_pTarget( pParent )
		, m_pCamera( 0 )
		, m_pScene( 0 )
		, m_nZOrder( nZOrder )
		, m_fRelativeTop( fRelTop )
		, m_fRelativeLeft( fRelLeft )
		, m_fRelativeWidth( fRelWidth )
		, m_fRelativeHeight( fRelHeight )
		, m_ulBackgroundColor( 0xffffffff )
	{
		UpdateDimension();
	}
	//----------------------------------------------------------------
	CnViewport::~CnViewport()
	{
		m_pScene = 0;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnViewport::SetCamera( const Ptr<CnCamera>& pCamera )
	{
		m_pCamera = pCamera;

		if (m_pCamera != NULL)
		{ // 화면의 종횡비가 변하지 않았다면, 업데이트 할 필요가 없지 않는가?
			float aspect = (float)m_pTarget->GetWidth() / (float)m_pTarget->GetHeight();
			m_pCamera->SetAspect( aspect );
		}
	}

	//----------------------------------------------------------------
	/**	UpdateDimension
		@brief : 뷰포트의 실제 크기를 설정한다.
		@param : none
		@return : none
	*/
	void CnViewport::UpdateDimension()
	{
		m_nActualTop = (int)( m_fRelativeTop * (float)m_pTarget->GetHeight() );
		m_nActualLeft = (int)( m_fRelativeLeft * (float)m_pTarget->GetWidth() );
		m_nActualWidth = (int)( m_fRelativeWidth * (float)m_pTarget->GetWidth() );
		m_nActualHeight = (int)( m_fRelativeHeight * (float)m_pTarget->GetHeight() );

		//! 이 녀석은 여기에 있으면 안될 듯 하다..
		if (m_pCamera != NULL)
		{ // 화면의 종횡비가 변하지 않았다면, 업데이트 할 필요가 없지 않는가?
			m_pCamera->SetAspect( (float)m_pTarget->GetWidth() / (float)m_pTarget->GetHeight() );
		}
	}

	//----------------------------------------------------------------
	/**	Update
		@brief : viewport 상에 그려지는 모든 장면을 업데이트 한다.
		@param : none
		@return : none
	*/
	void CnViewport::Update()
	{
		RenderDevice->SetViewport( this );
	
		if (m_pScene.IsValid())
		{
			m_pScene->Update( m_pCamera );
			//m_pScene->Draw( m_pCamera );
		}
	}

	//----------------------------------------------------------------
	/**	Draw
		@brief : viewport 에 연결된 장면을 그린다.
		@param : none
		@return : none
	*/
	void CnViewport::Draw()
	{
		RenderDevice->SetViewport( this );

		if (m_pScene.IsValid())
		{
			m_pScene->Draw( m_pCamera );
		}
	}
}