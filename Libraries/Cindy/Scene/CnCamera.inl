// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
//inline
//void CnCamera::SetScene( CnScene* pScene )
//{
//	m_pScene = pScene;
//}
//--------------------------------------------------------------
inline
void CnCamera::SetName( const CnString& Name )
{
	m_strName = Name;
}
inline
const CnString& CnCamera::GetName() const
{
	return m_strName;
}

//--------------------------------------------------------------
inline
void CnCamera::SetFrustum( float fFovY, float fAspect, float fNear, float fFar, float fFarForFrustum )
{
	m_fFovY = fFovY;
	m_fAspect = fAspect;
	m_fNear = fNear;
	m_fFar = fFar;

	m_fFarForFrustum = fFarForFrustum;

	m_bNeedUpdateProjection = true;
}

//--------------------------------------------------------------
inline 
void CnCamera::SetAspect( float fAspect )
{
	if (m_fAspect != fAspect)
		m_bNeedUpdateProjection = true;

	m_fAspect = fAspect;
}

//--------------------------------------------------------------
inline
float CnCamera::GetAspect() const
{
	return m_fAspect;
}

//--------------------------------------------------------------
inline
void CnCamera::SetFar( float fFar )
{
	m_fFar = fFar;
	if (m_fFar > m_fFarForFrustum)
		m_fFarForFrustum = fFar;

	m_bNeedUpdateProjection = true;
}

//--------------------------------------------------------------
inline
void CnCamera::SetFarForFrustum( float fFar )
{
	m_fFarForFrustum = fFar;
	m_bNeedUpdateProjection = true;
}

//--------------------------------------------------------------
inline
float CnCamera::GetFar() const
{
	return m_fFar;
}

//--------------------------------------------------------------
inline
float CnCamera::GetFarForFrustum() const
{
	return m_fFarForFrustum;
}

//--------------------------------------------------------------
inline
void CnCamera::SetNear( float fNear )
{
	m_fNear = fNear;
	m_bNeedUpdateProjection = true;
}

//--------------------------------------------------------------
inline
float CnCamera::GetNear() const
{
	return m_fNear;
}

//--------------------------------------------------------------
inline
void CnCamera::SetFovY( float fFovY )
{
	m_fFovY = fFovY;
	m_bNeedUpdateProjection = true;
}

//--------------------------------------------------------------
inline
float CnCamera::GetFovY() const
{
	return m_fFovY;
}

//--------------------------------------------------------------
inline
float CnCamera::GetNearWidth() const
{
	return m_fNearWidth;
}

//--------------------------------------------------------------
inline
float CnCamera::GetNearHeight() const
{
	return m_fNearHeight;
}

//--------------------------------------------------------------
inline
float CnCamera::GetFarWidth() const
{
	return m_fFarWidth;
}

//--------------------------------------------------------------
inline
float CnCamera::GetFarHeight() const
{
	return m_fFarHeight;
}

//--------------------------------------------------------------
inline
void CnCamera::SetPerspective( bool bPersp )
{
	m_bPerspective = bPersp;
}
inline
bool CnCamera::IsPerspective() const
{
	return m_bPerspective;
}

//--------------------------------------------------------------
inline
void CnCamera::SetDefault( bool bDefault )
{
	m_bDefault = bDefault;
}
inline
bool CnCamera::IsDefault() const
{
	return m_bDefault;
}

//--------------------------------------------------------------
inline
void CnCamera::SetPosition( const Math::Vector3& vPosition )
{
	m_vPosition = vPosition;

	m_bNeedUpdateView = true;
}
inline
const Math::Vector3& CnCamera::GetPosition() const
{
	return m_vPosition;
}

//--------------------------------------------------------------
inline
void CnCamera::SetUp( const Math::Vector3& vUp )
{
	m_vUp = vUp;
}
inline
const Math::Vector3& CnCamera::GetUp() const
{
	return m_vUp;
}

//--------------------------------------------------------------
inline
void CnCamera::SetLookAt( const Math::Vector3& vLookAt )
{
	m_vLookAt = vLookAt;

	m_bNeedUpdateView = true;
}
inline
const Math::Vector3& CnCamera::GetLookAt() const
{
	return m_vLookAt;
}

//--------------------------------------------------------------
inline
const Math::Vector3& CnCamera::GetDirection() const
{
	return m_vDirection;
}

//--------------------------------------------------------------
inline
const Math::Matrix44& CnCamera::GetView() const
{
	UpdateView();

	return m_mView;
}

//--------------------------------------------------------------
inline
const Math::Matrix44& CnCamera::GetProjection() const
{
	UpdateProjection();

	return m_mProjection;
}

//--------------------------------------------------------------
inline
const Math::Matrix44& CnCamera::GetViewProjection() const
{
	UpdateView();
	UpdateProjection();
	UpdateFrustum();
	
	return m_mViewProjection;
}