//================================================================
// File:           : CnIKObject.h
// Original Author : changhee
// Creation Date   : 2007. 4. 16
//================================================================
#ifndef __CN_IKOBJECT_H__
#define __CN_IKOBJECT_H__

#include "CnTransformable.h"
#include "../Animation/CnIKController.h"

namespace Cindy
{
	//================================================================
	/** IKObject
	    @author		changhee
		@brief		IK를 적용시키는 객체
	*/
	//================================================================
	class CnIKObject : public CnTransformable
	{
		__DeclareRtti;
	public:
		CnIKObject();
		CnIKObject( const wchar* strName, ushort nIndex );
		virtual ~CnIKObject();

	private:
		friend class CnIKController::IKSolver;

		bool	_ComputeIK( CnIKController::IKSolver* pIKCtrlr, uint& TryCount );
	};
}

#endif	// __CN_IKOBJECT_H__