//================================================================
// File:               : CnAnimNode.cpp
// Related Header File : CnAnimNode.h
// Original Author     : changhee
//================================================================
#include "../Cindy.h"
#include "CnAnimNode.h"
#include "Animation/CnAnimationSequencer.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnAnimNode, CnNode);
	//----------------------------------------------------------------
	CnAnimNode::CnAnimNode()
	{
	}
	CnAnimNode::CnAnimNode( const wchar* strName, ushort nIndex )
		: CnNode( strName, nIndex )
	{
	}
	// dest	
	CnAnimNode::~CnAnimNode()
	{
	}

	//================================================================
	/** Animate
		@brief	애니메이션 데이터를 이용한 노드 업데이트
	*/
	//================================================================
	void CnAnimNode::_Animate( CnAnimChannel* pChannel, bool bNeedUpdate )
	{
		CnAnimInstance::Locker::Result lockType = pChannel->GetMotion()->IsLocked(GetName());
		if (lockType == CnAnimInstance::Locker::Lock)
		{
			bNeedUpdate = false;
		}
		else if (lockType == CnAnimInstance::Locker::Free)
		{
			bNeedUpdate = true;
		}

		float mixtureRatio = pChannel->GetMixtureRatio();

		AnimKey newKey;
		if (bNeedUpdate && pChannel->GetKey( this, &newKey ))
		{
			if (mixtureRatio < 1.0f)
			{
				// 보간
				newKey.pos.Lerp( m_vLocalPosition, newKey.pos, mixtureRatio );
				newKey.rot.Slerp( m_qLocalRotation, newKey.rot, mixtureRatio );
				newKey.scl.Lerp( m_vLocalScale, newKey.scl, mixtureRatio );
			}

			this->SetPosition( newKey.pos );
			this->SetRotation( newKey.rot );
			this->SetScale( newKey.scl );
		}

		CnAnimNode* node = NULL;
		ChildIter iend = m_Children.end();
		for (ChildIter i = m_Children.begin(); i != iend; ++i)
		{
			node = DynamicCast<CnAnimNode>(i->second.GetPtr());
			if (node)
			{
				node->_Animate( pChannel, bNeedUpdate );
			}
		}
	}
}