//================================================================
// File					: CnUpdateSceneComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"

#include "Scene/CnCamera.h"
#include "Scene/CnSceneGraph.h"
#include "Scene/CnVisibleSet.h"
#include "Frame/CnFrameListener.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** Update Scene Component
		@author		cagetu
		@brief		장면 업데이트 Component
	*/
	//==================================================================
	class CN_DLL UpdateScene : public Scene::Component,
							   public IFrameListener
	{
		__DeclareClass(UpdateScene);
	public:
		UpdateScene();
		virtual ~UpdateScene();

		// 장면 그래프 등록
		void			AddSceneGraph( const SceneGraphPtr& SceneGraph );
		void			RemoveSceneGraph( const SceneGraphPtr& SceneGraph );

		CnVisibleSet*	GetVisibleSet() const;

		virtual void	OnUpdate() override;

		virtual void	OnBeginFrame( CnCamera* pCamera ) override;
		virtual void	OnFrame( CnCamera* pCamera ) override;
		virtual void	OnEndFrame( CnCamera* pCamera ) override;

	private:
		typedef std::vector<SceneGraphPtr>	SceneGraphArray;
		typedef SceneGraphArray::iterator	SceneGraphIter;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		SceneGraphArray		m_SceneGraphes;
		Ptr<CnVisibleSet>	m_pVisibleSet;

		bool				m_bNeedUpdate;

		Ptr<CnCamera>		m_pCamera;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		// FrameListener 재정의
		void	BeginFrame() override;
		void	EndFrame() override;

		// Update
		void	FindVisible( CnCamera* pCamera );
		void	UpdateRelation( CnCamera* pCamera );
	};

	//----------------------------------------------------------------
	/**
	*/
	inline
	CnVisibleSet* UpdateScene::GetVisibleSet() const
	{
		return m_pVisibleSet;
	}
} // namespace Scene
} // namespace Cindy