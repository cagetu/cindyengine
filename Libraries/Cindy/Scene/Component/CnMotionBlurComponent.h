//================================================================
// File					: CnMotionBlurComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnPostProcessComponent.h"
#include "Math/CnMatrix44.h"

namespace Cindy
{
namespace Scene
{
namespace MotionBlur
{
	//==================================================================
	/** MotionBlur - Velocity
		@brief	Object의 변화는 신경쓰지 않고, 카메라만 이동했다는 가정으로,
				이전 ViewTM과 현재 ViewTM을 이용한 변화량을 가지고 VelocityMap을 만든다.

		@see	GPU Gems3 Chapter 27. Motion Blur as Post-Processing Effect
				http://http.developer.nvidia.com/GPUGems3/gpugems3_ch27.html
	*/
	//==================================================================
	class CN_DLL Velocity : public Scene::PostProcessComponent
	{
		__DeclareClass(Velocity);
	public:
		Velocity();
		virtual ~Velocity();

	private:
		//------------------------------------------------------------------
		// Variables
		//------------------------------------------------------------------
		//Math::Matrix44 m_PrevViewProj;

		//------------------------------------------------------------------
		// Methods
		//------------------------------------------------------------------
		virtual void ApplyShaderVariables( CnCamera* pCamera ) override;
	};
} // namespace MotionBlur
} // namespace Scene
} // namespace Cindy