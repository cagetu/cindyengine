//================================================================
// File					: CnLightPrePassComponent.cpp
// Related Header File	: CnLightPrePassComponent.h
// Original Author		: Changhee
//================================================================
#include "CnLightPrePassComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnVisibleSet.h"
#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/Base/CnRenderWindow.h"

namespace Cindy
{
namespace Scene
{
namespace LightPrePass
{
	//==================================================================
	//	Base Lighting
	//==================================================================
	__ImplementRtti(Cindy, BaseLighting, PostProcessComponent);
	//------------------------------------------------------------------
	BaseLighting::BaseLighting()
	{
		m_SceneID = L"LightPrePass::BaseLighting";
	}
	BaseLighting::~BaseLighting()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	void BaseLighting::ApplyRenderState()
	{
		LPDIRECT3DDEVICE9 d3d9Device = 0;
		RenderDevice->GetCurrentDevice(&d3d9Device);

		/// zenble 
		d3d9Device->SetRenderState(D3DRS_ZENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

		/// enable writing
		d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);

		/// enable alpha
		d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		///
		d3d9Device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_FOGENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		
		//RenderDevice->SetRenderState( *m_Material.GetRenderState() );
	}

	//------------------------------------------------------------------
	/**	ShaderVariable 적용
	*/
	void BaseLighting::ApplyShaderState( CnShader* pShader, CnCamera* pCamera )
	{
		ShaderHandle handle = NULL;

		RenderDevice->SetActiveShader( pShader->GetEffect() );

		//if (handle = pShader->GetParameter( ShaderState::ModelView ) )
		//{
		//	pShader->SetMatrix( handle, pCamera->GetView() );
		//}

		//
		if (handle = pShader->GetParameter(ShaderState::InvView))
		{
			Math::Matrix44 invView;
			pCamera->GetView().Inverse(invView);

			pShader->SetMatrix( handle, invView );
		}
		
		// Texture
		TexPtr texture;
		for(int i=0; i<MapType::NumMapTypes; i++)
		{
			int shaderparam = MapType::ToShaderState( (MapType::Define)i );
			if (handle = pShader->GetParameter( (ShaderState::Param)shaderparam ))
			{
				texture = m_Material.GetTexture( (MapType::Define)i );
				pShader->SetTexture(handle, texture);
			}
		}

		/// 해상도
		float windowWidth = (float)RenderDevice->GetCurrentRenderWindow()->GetWidth();
		float windowHeight = (float)RenderDevice->GetCurrentRenderWindow()->GetHeight();

		// 화면 해상도
		if (handle = pShader->GetParameter(ShaderState::DisplayResolution))
		{
			Math::Vector4 res( windowWidth, windowHeight, 1.0f/windowWidth, 1.0f/windowHeight );
			pShader->SetValue( handle, res, sizeof(Math::Vector4) );
		}
		if (handle = pShader->GetParameter(ShaderState::ScreenUVAdjust))
		{
			Math::Vector2 uvAdjust( 0.5f/windowWidth, 0.5f/windowHeight );
			pShader->SetValue( handle, uvAdjust, sizeof(Math::Vector2) );
		}

		// ViewInfo
		if (handle = pShader->GetParameter( ShaderState::ViewInfo ))
		{
			// tanFOV
			float fov = pCamera->GetFovY();
			float tanFOV = 1.0f / Math::Tan(fov*0.5f);

			Math::Vector4 viewInfo;
			viewInfo.x = pCamera->GetAspect();
			viewInfo.y = tanFOV;
			viewInfo.z = pCamera->GetNear();
			viewInfo.w = pCamera->GetFar();
			pShader->SetValue( handle, viewInfo, sizeof(Math::Vector4) );
		}
	}

	//------------------------------------------------------------------
	/**
	*/
	void BaseLighting::ApplyShaderStatePerLight( CnShader* pShader, CnLight* pLight, CnCamera* pCamera )
	{
		// light
		ShaderHandle handle = NULL;

		//if (handle = pShader->GetParameter( ShaderState::LightDirection ))
		//{
		//	Math::Vector3 viewSpaceLightDir;
		//	viewSpaceLightDir.MultiplyNormal( pLight->GetDirection(), pCamera->GetView() );
		//	//viewSpaceLightDir.Normalize();
		//	shader->SetVector3( handle, viewSpaceLightDir );
		//}
		if (handle = pShader->GetParameter( ShaderState::LightPosition ))
		{
			/// viewspace light position을 연결한다.
			Math::Vector3 lightViewPos;
			lightViewPos.Multiply( pLight->GetWorldPosition(), pCamera->GetView() );
			pShader->SetVector3( handle, lightViewPos );
			//pShader->SetVector3( handle, pLight->GetWorldPosition() );
		}
		if (handle = pShader->GetParameter( ShaderState::LightDiffuse ))
		{
			pShader->SetColor( handle, pLight->GetDiffuse() );
		}
		if (handle = pShader->GetParameter( ShaderState::LightAttenuation ))
		{
			pShader->SetVector3( handle, pLight->GetAttenuation() );
		}

		//! Light Pre Pass 내에서 그림자 처리할때...
		if (pLight->GetShadowBuilder().IsValid())
		{
			Math::Matrix44 matrix;

			pLight->GetShadowBuilder()->SetShaderVariables( matrix );
		}
	}

	//------------------------------------------------------------------
	/**
	*/
	void BaseLighting::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* lights = visibleSet->GetEntities( CnVisibleSet::Light );
		if (NULL == lights)
			return;

		ShaderPtr shader = m_Material.GetShader();
		if (shader.IsNull())
			return;

		ApplyRenderState();
		ApplyShaderState( shader, pCamera ); 

		CnTechnique* technique = m_Material.GetCurrentTechnique();

		CnVisibleSet::EntityConstIter i, iend;
		iend = lights->end();
		for (i=lights->begin(); i!=iend; ++i)
		{
			CnLight* light = (CnLight*)(*i);

			if (!CheckLightType(light))
				continue;

			ApplyShaderStatePerLight( shader, light, pCamera );

			int numPasses = technique->Begin();
			for (int i=0; i<numPasses; ++i)
			{
				CnPass* pass = technique->GetPass(i);
				if (NULL == pass)
					continue;

				pass->Begin();
				{
					RenderDevice->DrawPrimitive( IRenderer::TRIANGLEFAN, 0, 2 );
				}
				pass->End();
			}
			technique->End();
		} // for
	}

	//==================================================================
	//	DirectionalLight
	//==================================================================
	__ImplementClass(Cindy, DirectionLighting, BaseLighting);
	//------------------------------------------------------------------
	DirectionLighting::DirectionLighting()
	{
		m_SceneID = L"LightPrePass::DirectionLighting";
	}
	DirectionLighting::~DirectionLighting()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	bool DirectionLighting::CheckLightType( CnLight* pLight )
	{
		return (pLight->GetType() == CnLight::Directional || pLight->GetType() == CnLight::Global);
	}

	//==================================================================
	//	PointLighting
	//==================================================================
	__ImplementClass(Cindy, PointLighting, BaseLighting);
	//------------------------------------------------------------------
	PointLighting::PointLighting()
	{
		m_SceneID = L"LightPrePass::PointLighting";
	}
	PointLighting::~PointLighting()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLighting::Open()
	{
		assert(m_VertexData == 0);
		m_VertexData = new CnVertexData();

		CnVertexDeclaration::VertexElement vertElement;
		VertDeclPtr vertexDeclaration = m_VertexData->GetDeclaration();
		{
			CnVertexDeclaration::UsageComponent vertexComponent;
			vertexComponent.Add( CnVertexDeclaration::Position );
			vertexComponent.Add( CnVertexDeclaration::Normal );
			vertElement = vertexDeclaration->AddElement( 0, 0, vertexComponent );
		}
		vertexDeclaration->Build();

		///
		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice(&d3dDevice);

		HRESULT hr = D3DXCreateSphere(d3dDevice, 1.0f, 16, 8, &m_d3dSphere, NULL);
		assert(SUCCEEDED(hr));
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLighting::Close()
	{
		m_VertexData = 0;
		SAFEREL( m_d3dSphere );
	}

	//------------------------------------------------------------------
	/**
	*/
	bool PointLighting::CheckLightType( CnLight* pLight )
	{
		return (pLight->GetType() == CnLight::Point);
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLighting::ApplyShaderState( CnShader* pShader, CnCamera* pCamera )
	{
		RenderDevice->SetActiveShader( pShader->GetEffect() );

		/// 해상도
		float windowWidth = (float)RenderDevice->GetCurrentRenderWindow()->GetWidth();
		float windowHeight = (float)RenderDevice->GetCurrentRenderWindow()->GetHeight();

		ShaderHandle handle = NULL;

		// 화면 해상도
		if (handle = pShader->GetParameter(ShaderState::DisplayResolution))
		{
			Math::Vector4 res( windowWidth, windowHeight, 1.0f/windowWidth, 1.0f/windowHeight );
			pShader->SetValue( handle, res, sizeof(Math::Vector4) );
		}
		if (handle = pShader->GetParameter(ShaderState::ScreenUVAdjust))
		{
			Math::Vector2 uvAdjust( 0.5f/windowWidth, 0.5f/windowHeight );
			pShader->SetValue( handle, uvAdjust, sizeof(Math::Vector2) );
		}
		// ViewInfo
		if (handle = pShader->GetParameter(ShaderState::ViewInfo))
		{
			// tanFOV
			float fov = pCamera->GetFovY();
			float tanFOV = 1.0f / Math::Tan(fov*0.5f);

			Math::Vector4 viewInfo;
			viewInfo.x = pCamera->GetAspect();
			viewInfo.y = tanFOV;
			viewInfo.z = pCamera->GetNear();
			viewInfo.w = pCamera->GetFar();

			pShader->SetValue( handle, viewInfo, sizeof(Math::Vector4) );
		}
		
		// Texture
		TexPtr texture;
		for(int i=0; i<MapType::NumMapTypes; i++)
		{
			int shaderparam = MapType::ToShaderState( (MapType::Define)i );
			if (handle = pShader->GetParameter( (ShaderState::Param)shaderparam ))
			{
				texture = m_Material.GetTexture( (MapType::Define)i );
				pShader->SetTexture(handle, texture);
			}
		}
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLighting::ApplyShaderStatePerLight( CnShader* pShader, CnLight* pLight, CnCamera* pCamera )
	{
		ShaderHandle handle = NULL;

		Math::Vector3 lightPosition = pLight->GetWorldPosition();

		// mvp
		if (handle = pShader->GetParameter( ShaderState::ModelViewProjection ) )
		{
			// sphere의 mvp를 설정한다.
			Math::Matrix44 mTranslate;
			mTranslate.Translation(lightPosition.x, lightPosition.y, lightPosition.z);

			Math::Matrix44 mScale;
			float lightScale = pLight->GetRange();// * 1.1f;
			mScale.Scaling( lightScale, lightScale, lightScale );

			Math::Matrix44 mvp = mScale * mTranslate * pCamera->GetViewProjection();
			pShader->SetMatrix( handle, mvp );
		}

		// light
		if (handle = pShader->GetParameter(ShaderState::LightPosition))
		{
			/// viewspace light position을 연결한다.
			Math::Vector3 lightViewPos;
			lightViewPos.Multiply( lightPosition, pCamera->GetView() );
			pShader->SetVector3( handle, lightViewPos );
		}
		if (handle = pShader->GetParameter(ShaderState::LightDiffuse))
		{
			pShader->SetColor( handle, pLight->GetDiffuse() );
		}
		if (handle = pShader->GetParameter(ShaderState::LightRange))
		{
			float lightRange = pLight->GetRange();
			//float sqrLightRange = lightRange * lightRange;
			//float lightInvRange = 1.0f / sqrLightRange;
			//float lightInvRange = 1.0f / lightRange;
			pShader->SetFloat( handle, lightRange );
		}
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLighting::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* lights = visibleSet->GetEntities( CnVisibleSet::Light );
		if (NULL == lights)
			return;

		ShaderPtr shader = m_Material.GetShader();
		if (shader.IsNull())
			return;

		//ApplyRenderState();
		ApplyShaderState( shader, pCamera );

		CnTechnique* technique = m_Material.GetCurrentTechnique();

		CnVisibleSet::EntityConstIter i, iend;
		iend = lights->end();
		for (i=lights->begin(); i!=iend; ++i)
		{
			CnLight* light = (CnLight*)(*i);

			if (!CheckLightType( light ))
				continue;

			RenderDevice->ClearBuffer( IRenderer::CLEAR_STENCIL, 0xff000000, 1.0f, 1 );

			ApplyShaderStatePerLight( shader, light, pCamera );

			shader->CommitChanges();

			int numPasses = technique->Begin();
			for (int i=0; i<numPasses; ++i)
			{
				CnPass* pass = technique->GetPass(i);
				if (NULL == pass)
					continue;

				pass->Begin();
				{
					HRESULT hr = m_d3dSphere->DrawSubset(0);
					assert( SUCCEEDED(hr) );
				}
				pass->End();
			}
			technique->End();
		} // for
	}
} // namespace LightPrePass
} // namespace Scene
} // namespace Cindy