//================================================================
// File					: CnRenderGeometryComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"
#include "Graphics/CnGeometryQueue.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** Render Geometry
		@author		cagetu
		@brief		Geometry ������
	*/
	//==================================================================
	class CN_DLL RenderGeometry : public Scene::Component
	{
		__DeclareClass(RenderGeometry);
	public:
		RenderGeometry();
		virtual ~RenderGeometry();

		void	Open() override;

		void	OnBeginFrame( CnCamera* pCamera ) override;
		void	OnFrame( CnCamera* pCamera ) override;
		void	OnEndFrame( CnCamera* pCamera ) override;

		void	AddChunk( const wchar* type, const wchar* feature, const wchar* shader, const wchar* sort );
	protected:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		CnGeometryQueue		m_RenderQueue;
	};
} // namespace Scene
} // namespace Cindy