//================================================================
// File					: CnOcclusionComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"
#include "../../Material/CnTexture.h"

namespace Cindy
{
class CnQuery;

namespace Scene
{
	//==================================================================
	/** Occlusion
		@author		cagetu
		@brief		Occlusion ó�� Component
		@see		http://www.gamedev.net/reference/programming/features/occlusionculling/
		@desc		
			1. Render every object's bounding mesh 
			2. For every object: 
				a. Begin query 
				b. Re-render the bounding mesh 
				c. End query 
				d. Retrieve occlusion query data.
				   If the pixels visible are greater than zero, the object should be rendered.
				   Otherwise, the object should be occluded from rendering. 

	*/
	//==================================================================
	class CN_DLL Occlusion : public Scene::Component
	{
		__DeclareClass(Occlusion);
	public:
		Occlusion();
		virtual ~Occlusion();

		void	Open() override;

		void	OnFrame( CnCamera* pCamera ) override;

	private:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		CnQuery*	m_Query;
		TexPtr		m_OcclusionToTexture;
	};
} // namespace Scene
} // namespace Cindy

