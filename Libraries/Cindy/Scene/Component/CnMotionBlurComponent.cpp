//================================================================
// File					: CnMotionBlurComponent.cpp
// Related Header File	: CnMotionBlurComponent.h
// Original Author		: Changhee
//================================================================
#include "CnMotionBlurComponent.h"
#include "Scene/CnCamera.h"
#include "Util/CnLog.h"

namespace Cindy
{
namespace Scene
{
namespace MotionBlur
{
	//==================================================================
	//	Velocity
	//==================================================================
	__ImplementClass(Cindy, Velocity, Scene::PostProcessComponent);
	//------------------------------------------------------------------
	Velocity::Velocity()
	{
		m_SceneID = L"MotionBlurVelocity";
	}
	Velocity::~Velocity()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	void Velocity::ApplyShaderVariables( CnCamera* pCamera )
	{
		PostProcessComponent::ApplyShaderVariables( pCamera );

		ShaderHandle handle = NULL;
		ShaderPtr shader = m_Material.GetShader();

		//if (handle = shader->GetParameter( ShaderState::ViewProjectionInverse ))
		//{
		//	Math::Matrix44 viewProjection = pCamera->GetView() * pCamera->GetProjection();

		//	Math::Matrix44 invViewProj;
		//	viewProjection.Inverse(invViewProj);

		//	shader->SetMatrix( handle, invViewProj );
		//}
	}

} // namespace MotionBlur
} // namespace Scene
} // namespace Cindy