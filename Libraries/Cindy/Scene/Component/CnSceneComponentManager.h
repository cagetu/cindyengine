//================================================================
// File					: CnSceneComponentManager.h			
// Original Author		: Changhee
//================================================================
#ifndef __CN_SCENECOMPONENT_MANAGER_H__
#define __CN_SCENECOMPONENT_MANAGER_H__

#include "CnBaseSceneComponent.h"
#include "Util/CnUnCopyable.h"
#include "Util/CnSingleton.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** Scene Component Manager
		@author			cagetu
		@since			2009년 8월 12일
		@remarks		생성된 장면 Component를 관리한다.
	*/
	//==================================================================
	class CN_DLL ComponentManager
	{
		__DeclareUnCopy(ComponentManager);
		__DeclareSingleton(ComponentManager);
	public:
		ComponentManager();
		~ComponentManager();

		void Open();
		void Close();

		void AddComponent( const CnString& strName, const Ptr<Scene::Component>& pComponent );
		void RemoveComponent( const CnString& strName );
		Ptr<Scene::Component> GetComponent( const CnString& strName );

	private:
		typedef HashMap<CnString, Ptr<Scene::Component> > ComponentMap;

		//--------------------------------------------------------------
		// Variables
		//--------------------------------------------------------------
		bool			m_bOpened;
		ComponentMap	m_Components;
	};
} // namespace Scene
#define SceneComponentMgr	Scene::ComponentManager::Instance()
} // namespace Cindy

#endif	// __CN_SCENECOMPONENT_MANAGER_H__