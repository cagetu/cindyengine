//================================================================
// File					: CnBaseSceneComponent.cpp
// Related Header File	: CnBaseSceneComponent.h
// Original Author		: Changhee
//================================================================
#include "CnBaseSceneComponent.h"

namespace Cindy
{
namespace Scene
{
	__ImplementRtti(Cindy, Component, CnObject);

	//Const/Dest
	Component::Component()
	{
		m_SceneID = L"BaseComponent";
	}
	Component::~Component()
	{
		m_Links.clear();
	}

	//----------------------------------------------------------------
	/** @brief	초기화
	*/
	void Component::Open()
	{	// Blank.. Blank.. Blank..
	}

	//----------------------------------------------------------------
	/** @brief	끝
	*/
	void Component::Close()
	{	// Blank.. Blank.. Blank..
	}

	//----------------------------------------------------------------
	/**
	*/
	void Component::OnUpdate()
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	void Component::OnBeginFrame( CnCamera* pCamera )
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	void Component::OnFrame( CnCamera* pCamera )
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	void Component::OnEndFrame( CnCamera* pCamera )
	{
	}

	//----------------------------------------------------------------
	/**	@brief	연관성 설정
	*/
	void Component::Link( const Ptr<Component>& pComponent )
	{
		std::pair<LinkMap::iterator, bool> res = m_Links.insert( LinkMap::value_type( pComponent->GetID(), pComponent ) );
	}

	//----------------------------------------------------------------
	/**	@brief	연관성 끊기
	*/
	void Component::Unlink( const Ptr<Component>& pComponent )
	{
		LinkMap::iterator iter = m_Links.find( pComponent->GetID() );
		if (iter==m_Links.end())
			return;

		m_Links.erase(iter);
	}

	//----------------------------------------------------------------
	/**	@brief	연결된 Component 찾기
	*/
	Ptr<Component> Component::GetLink( const SceneID& ID ) const
	{
		LinkMap::const_iterator iter = m_Links.find( ID );
		if (iter==m_Links.end())
			return NULL;

		return iter->second;
	}

} // namespace Scene
} // namespace Cindy