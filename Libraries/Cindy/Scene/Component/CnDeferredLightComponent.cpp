//================================================================
// File					: CnDeferredLightComponent.cpp
// Related Header File	: CnDeferredLightComponent.h
// Original Author		: Changhee
//================================================================
#include "CnDeferredLightComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnVisibleSet.h"
#include "Scene/CnCamera.h"
#include "Lighting/CnLight.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/Base/CnRenderWindow.h"

namespace Cindy
{
namespace Scene
{
namespace Deferred
{
	//==================================================================
	//	GlobalLight
	//==================================================================
	__ImplementClass(Cindy, GlobalLight, PostProcessComponent);
	//------------------------------------------------------------------
	GlobalLight::GlobalLight()
	{
		m_SceneID = L"Deferred::GlobalLight";
	}
	GlobalLight::~GlobalLight()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	void GlobalLight::ApplyRenderState()
	{
		LPDIRECT3DDEVICE9 d3d9Device = 0;
		RenderDevice->GetCurrentDevice(&d3d9Device);

		/// zenble 
		d3d9Device->SetRenderState(D3DRS_ZENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

		/// enable writing
		d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);

		/// enable alpha
		d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		///
		d3d9Device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_FOGENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	}

	//------------------------------------------------------------------
	/**	ShaderVariable 적용
	*/
	void GlobalLight::ApplyShaderState( CnCamera* pCamera, CnShader* pShader, CnTechnique* pTechnique )
	{
		ShaderHandle handle = NULL;

		// mvp
		if (handle = pShader->GetParameter( ShaderState::ModelView ) )
		{
			pShader->SetMatrix( handle, pCamera->GetView() );
		}

		// Texture
		TexPtr texture;
		for(int i=0; i<MapType::NumMapTypes; i++)
		{
			int shaderparam = MapType::ToShaderState( (MapType::Define)i );
			if (handle = pShader->GetParameter( (ShaderState::Param)shaderparam ))
			{
				texture = m_Material.GetTexture( (MapType::Define)i );
				pShader->SetTexture(handle, texture);
			}
		}

		// ViewInfo
		if (handle = pShader->GetParameter( ShaderState::ViewInfo ))
		{
			// tanFOV
			float fov = pCamera->GetFovY();
			float tanFOV = 1.0f / Math::Tan(fov*0.5f);

			Math::Vector4 viewInfo;
			viewInfo.x = pCamera->GetAspect();
			viewInfo.y = tanFOV;
			viewInfo.z = pCamera->GetNear();
			viewInfo.w = pCamera->GetFar();
			pShader->SetValue( handle, viewInfo, sizeof(Math::Vector4) );
		}
	}

	//------------------------------------------------------------------
	/**
	*/
	void GlobalLight::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* lights = visibleSet->GetEntities( CnVisibleSet::Light );
		if (NULL == lights)
			return;

		ShaderPtr shader = m_Material.GetShader();
		if (shader.IsNull())
			return;

		CnTechnique* technique = m_Material.GetCurrentTechnique();

		RenderDevice->SetActiveShader( shader->GetEffect() );

		SetVertexStream();
		SetIndexStream();

		ApplyRenderState();
		ApplyShaderState( pCamera, shader.GetPtr(), technique ); 

		CnVisibleSet::EntityConstIter i, iend;
		iend = lights->end();
		for (i=lights->begin(); i!=iend; ++i)
		{
			CnLight* light = (CnLight*)(*i);

			if (!(light->GetType() == CnLight::Ambient || light->GetType() == CnLight::Directional || light->GetType() == CnLight::Global))
				continue;

			if (light->GetType() == CnLight::Ambient)
			{
				technique = shader->GetTechnique(L"AmbientLighting");
				if (!technique)
				{
					technique = m_Material.GetCurrentTechnique();
				}

				// light
				ShaderHandle handle = NULL;
				if (handle = shader->GetParameter( ShaderState::LightDiffuse ))
				{
					shader->SetColor( handle, light->GetDiffuse() );
				}
			}
			else if (light->GetType() == CnLight::Directional ||
					 light->GetType() == CnLight::Global)
			{
				technique = shader->GetTechnique(L"DirectionalLighting");
				if (!technique)
				{
					technique = m_Material.GetCurrentTechnique();
				}

				// light
				ShaderHandle handle = NULL;
				if (handle = shader->GetParameter( ShaderState::LightDirection ))
				{
					Math::Vector3 viewSpaceLightDir;
					viewSpaceLightDir.MultiplyNormal( light->GetDirection(), pCamera->GetView() );
					//viewSpaceLightDir.Normalize();
					shader->SetVector3( handle, viewSpaceLightDir );
				}
				if (handle = shader->GetParameter( ShaderState::LightPosition ))
				{
					shader->SetVector3( handle, light->GetWorldPosition() );
				}
				if (handle = shader->GetParameter( ShaderState::LightDiffuse ))
				{
					shader->SetColor( handle, light->GetDiffuse() );
				}
				if (handle = shader->GetParameter( ShaderState::LightAttenuation ))
				{
					shader->SetVector3( handle, light->GetAttenuation() );
				}
			} // if 

			int numPasses = technique->Begin();
			for (int i=0; i<numPasses; ++i)
			{
				CnPass* pass = technique->GetPass(i);
				if (NULL == pass)
					continue;

				pass->Begin();
				{
					RenderDevice->DrawPrimitive( IRenderer::TRIANGLEFAN, 0, 2 );
				}
				pass->End();
			}
			technique->End();
		} // for
	}

	//==================================================================
	//	PointLight
	//==================================================================
	__ImplementClass(Cindy, PointLight, Scene::Component);
	//------------------------------------------------------------------
	PointLight::PointLight()
	{
		m_SceneID = L"Deferred::PointLight";
	}
	PointLight::~PointLight()
	{
		Close();
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLight::Open()
	{
		m_VertexData = new CnVertexData();

		CnVertexDeclaration::VertexElement vertElement;
		VertDeclPtr vertexDeclaration = m_VertexData->GetDeclaration();
		{
			CnVertexDeclaration::UsageComponent vertexComponent;
			vertexComponent.Add( CnVertexDeclaration::Position );
			vertexComponent.Add( CnVertexDeclaration::Normal );
			vertElement = vertexDeclaration->AddElement( 0, 0, vertexComponent );
		}
		vertexDeclaration->Build();

		///
		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice(&d3dDevice);

		HRESULT hr = D3DXCreateSphere(d3dDevice, 1.0f, 16, 8, &m_d3dSphere, NULL);
		assert(SUCCEEDED(hr));
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLight::Close()
	{
		m_VertexData = 0;
		SAFEREL( m_d3dSphere );
	}

	//------------------------------------------------------------------
	/**
	*/
	void PointLight::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* lights = visibleSet->GetEntities( CnVisibleSet::Light );
		if (NULL == lights)
			return;

		ShaderPtr shader = m_Material.GetShader();
		if (shader.IsNull())
			return;

		RenderDevice->SetActiveShader( shader->GetEffect() );

		RenderDevice->SetVertexDeclaration( m_VertexData );
		//RenderDevice->SetRenderState( *m_Material.GetRenderState() );

		CnTechnique* technique = NULL;
		technique = shader->GetTechnique(L"PointLighting");

		/// 해상도
		float windowWidth = (float)RenderDevice->GetCurrentRenderWindow()->GetWidth();
		float windowHeight = (float)RenderDevice->GetCurrentRenderWindow()->GetHeight();

		HRESULT hr;
		ShaderHandle handle = NULL;
		// 화면 해상도
		if (handle = shader->GetParameter(ShaderState::DisplayResolution))
		{
			Math::Vector4 res( windowWidth, windowHeight, 1.0f/windowWidth, 1.0f/windowHeight );
			shader->SetValue( handle, res, sizeof(Math::Vector4) );
		}
		if (handle = shader->GetParameter(ShaderState::ScreenUVAdjust))
		{
			Math::Vector2 uvAdjust( 0.5f/windowWidth, 0.5f/windowHeight );
			shader->SetValue( handle, uvAdjust, sizeof(Math::Vector2) );
		}
		// ViewInfo
		if (handle = shader->GetParameter(ShaderState::ViewInfo))
		{
			// tanFOV
			float fov = pCamera->GetFovY();
			float tanFOV = 1.0f / Math::Tan(fov*0.5f);

			Math::Vector4 viewInfo;
			viewInfo.x = pCamera->GetAspect();
			viewInfo.y = tanFOV;
			viewInfo.z = pCamera->GetNear();
			viewInfo.w = pCamera->GetFar();

			shader->SetValue( handle, viewInfo, sizeof(Math::Vector4) );
		}

		CnVisibleSet::EntityConstIter i, iend;
		iend = lights->end();
		for (i=lights->begin(); i!=iend; ++i)
		{
			CnLight* light = (CnLight*)(*i);

			if (light->GetType() != CnLight::Point)
				continue;

			RenderDevice->ClearBuffer( IRenderer::CLEAR_STENCIL, 0xff000000, 1.0f, 1 );

			Math::Vector3 lightPosition = light->GetWorldPosition();

			// mvp
			if (handle = shader->GetParameter( ShaderState::ModelViewProjection ) )
			{
				// sphere의 mvp를 설정한다.
				Math::Matrix44 mTranslate;
				mTranslate.Translation(lightPosition.x, lightPosition.y, lightPosition.z);

				Math::Matrix44 mScale;
				float lightScale = light->GetRange() * 1.1f;
				mScale.Scaling( lightScale, lightScale, lightScale );

				Math::Matrix44 mvp = mScale * mTranslate * pCamera->GetViewProjection();
				shader->SetMatrix( handle, mvp );
			}
			// light
			if (handle = shader->GetParameter(ShaderState::LightPosition))
			{
				/// viewspace light position을 연결한다.
				Math::Vector3 lightViewPos;
				lightViewPos.Multiply( lightPosition, pCamera->GetView() );
				shader->SetVector3( handle, lightViewPos );
			}
			if (handle = shader->GetParameter(ShaderState::LightDiffuse))
			{
				shader->SetColor( handle, light->GetDiffuse() );
			}
			if (handle = shader->GetParameter(ShaderState::LightRange))
			{
				float lightInvRange = 1.0f / light->GetRange() * light->GetRange();
				shader->SetFloat( handle, lightInvRange );
			}

			shader->CommitChanges();

			int numPasses = technique->Begin();
			for (int i=0; i<numPasses; ++i)
			{
				CnPass* pass = technique->GetPass(i);
				if (NULL == pass)
					continue;

				pass->Begin();
				{
					hr = m_d3dSphere->DrawSubset(0);
					assert( SUCCEEDED(hr) );
				}
				pass->End();
			}
			technique->End();
		} // for
	}

	//==================================================================
	//	SpotLight
	//==================================================================
	__ImplementClass(Cindy, SpotLight, Scene::Component);
	//------------------------------------------------------------------
	SpotLight::SpotLight()
	{
		m_SceneID = L"Deferred::SpotLight";
	}
	SpotLight::~SpotLight()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	void SpotLight::Open()
	{
		m_VertexData = new CnVertexData();

		CnVertexDeclaration::VertexElement vertElement;
		VertDeclPtr vertexDeclaration = m_VertexData->GetDeclaration();
		{
			CnVertexDeclaration::UsageComponent vertexComponent;
			vertexComponent.Add( CnVertexDeclaration::Position );
			vertexComponent.Add( CnVertexDeclaration::Normal );
			vertElement = vertexDeclaration->AddElement( 0, 0, vertexComponent );
		}
		vertexDeclaration->Build();

		///
		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice(&d3dDevice);

		HRESULT hr = D3DXCreateSphere(d3dDevice, 1.0f, 16, 8, &m_d3dCone, NULL);
		assert(SUCCEEDED(hr));
	}

	//------------------------------------------------------------------
	/**
	*/
	void SpotLight::Close()
	{
		m_VertexData = 0;
		SAFEREL( m_d3dCone );
	}

	//------------------------------------------------------------------
	/**
	*/
	void SpotLight::OnFrame( CnCamera* pCamera )
	{
	}

} // namespace Deferred
} // namespace Scene
} // namespace Cindy