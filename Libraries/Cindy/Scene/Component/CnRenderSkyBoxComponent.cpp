//================================================================
// File					: CnRenderSkyBoxComponent.cpp
// Related Header File	: CnRenderSkyBoxComponent.h
// Original Author		: Changhee
//================================================================
#include "CnRenderSkyBoxComponent.h"
#include "Scene/CnCamera.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/Base/CnRenderWindow.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	RenderSkyBox
	//==================================================================
	__ImplementClass( Cindy, RenderSkyBox, Scene::Component );

	//----------------------------------------------------------------
	RenderSkyBox::RenderSkyBox()
		: m_Size(1.0f)
	{
		m_SceneID = L"RenderSkyBox";
	}
	RenderSkyBox::~RenderSkyBox()
	{
	}

	//----------------------------------------------------------------
	/**
	*/
	void RenderSkyBox::Open()
	{
		m_VertexData = new CnVertexData();

		/// VertexDeclaration
		CnVertexDeclaration::VertexElement vertElement;
		VertDeclPtr vertexDeclaration = m_VertexData->GetDeclaration();
		{
			CnVertexDeclaration::UsageComponent vertexComponent;
			vertexComponent.Add( CnVertexDeclaration::Position );
			vertElement = vertexDeclaration->AddElement( 0, 0, vertexComponent );
		}
		vertexDeclaration->Build();

		/// VertexBuffer
		Ptr<CnVertexBuffer> vertexBuffer = RenderDevice->CreateVertexBuffer();
		if (vertexBuffer->Create( 4,
								  vertElement.byteSize,
								  CnVertexBuffer::USAGE_WRITEONLY,
								  CnVertexBuffer::POOL_DEFAULT ))
		{
			VertexFormat* buffer = (VertexFormat*)vertexBuffer->Lock( 0, 0, 0 );
			if (buffer)
			{
				float width = (float)RenderDevice->GetCurrentRenderWindow()->GetWidth();
				float height = (float)RenderDevice->GetCurrentRenderWindow()->GetHeight();

				float highW = -1.0f - (1.0f/width);
				float highH = -1.0f - (1.0f/height);
				float lowW = 1.0f + (1.0f/width);
				float lowH = 1.0f + (1.0f/height);

				buffer[0].pos = Math::Vector4(lowW, lowH, 1.0f, 1.0f);
				buffer[1].pos = Math::Vector4(lowW, highH, 1.0f, 1.0f);
				buffer[2].pos = Math::Vector4(highW, lowH, 1.0f, 1.0f);
				buffer[3].pos = Math::Vector4(highW, highH, 1.0f, 1.0f);

				vertexBuffer->Unlock();
			}
		}

		m_VertexData->AddBuffer( 0, vertexBuffer );
	}
	
	//----------------------------------------------------------------
	/**
	*/
	void RenderSkyBox::Close()
	{
		m_VertexData = 0;
	}

	//----------------------------------------------------------------
	/**
	*/
	void RenderSkyBox::OnFrame( CnCamera* pCamera )
	{
		if (m_Material.IsNull())
			return;

		ShaderPtr shader = m_Material->GetShader();
		if (shader.IsNull())
			return;

		RenderDevice->SetActiveShader( shader->GetEffect() );

#ifdef _MULTIPLE_STREAM_
		// SetVertex/Index Buffer
		RenderDevice->SetVertexDeclaration( m_VertexData );
		RenderDevice->SetStreamSource( m_VertexData );
#else
		// SetVertex/Index Buffer
		RenderDevice->SetVertexDeclaration( m_VertexData );
		RenderDevice->SetStreamSource( 0, 0, 0, m_VertexData );
#endif // _MULTIPLE_STREAM_

		//RenderDevice->SetRenderState( *m_Material->GetRenderState() );
		//RenderDevice->SetTextureState( *m_Material.GetTextureState() );

		LPDIRECT3DDEVICE9 d3d9Device = 0;
		RenderDevice->GetCurrentDevice(&d3d9Device);

		/// zenble 
		d3d9Device->SetRenderState(D3DRS_ZENABLE, TRUE);
		d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

		/// enable writing
		d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);
		d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

		/// enable alpha
		d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		///
		d3d9Device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_FOGENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

		// technique
		CnTechnique* technique = m_Material->GetCurrentTechnique();
		if (technique)
		{
			int numPasses = technique->Begin();
			{
				CnPass* pass = NULL;

				ApplyShaderVariables( pCamera );

				for (int i=0; i<numPasses; ++i)
				{
					pass = technique->GetPass(i);
					if (NULL == pass)
						continue;

					pass->Begin();
					{
						RenderDevice->DrawPrimitive( IRenderer::TRIANGLESTRIP, 0, 2 );
					}
					pass->End();
				}
			}
			technique->End();
		}
	}

	//--------------------------------------------------------------
	/**
	*/
	void RenderSkyBox::ApplyShaderVariables( CnCamera* pCamera )
	{
		ShaderHandle handle = NULL;
		ShaderPtr shader = m_Material->GetShader();

		// mvp
		if (handle = shader->GetParameter( ShaderState::InvModelViewProjection ))
		{
			Math::Matrix44 viewProjection = pCamera->GetViewProjection();

			Math::Matrix44 invViewProj;
			viewProjection.Inverse(invViewProj);

			shader->SetMatrix( handle, invViewProj );
		}

		// texture 
		if (handle = shader->GetParameter( ShaderState::CubeMap0 ))
		{
			TexPtr cubeMap = m_Material->GetTexture( MapType::CubeMap0 );
			shader->SetTexture( handle, cubeMap );
		}
	}

} // namespace Scene
} // namespace Cindy
