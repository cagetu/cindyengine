//================================================================
// File					: CnShadowComponent.cpp
// Related Header File	: CnShadowComponent.h
// Original Author		: Changhee
//================================================================
#include "CnShadowComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnCamera.h"
#include "Scene/CnVisibleSet.h"
#include "Scene/CnVisibleResolver.h"
#include "Lighting/CnLight.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	CastShadow
	//==================================================================
	__ImplementClass( Cindy, CastShadow, Scene::Component );

	//----------------------------------------------------------------
	CastShadow::CastShadow()
	{
		m_SceneID = L"CastShadow";
	}
	CastShadow::~CastShadow()
	{
	}

	//----------------------------------------------------------------
	/** @brief	라이트 업데이트 */
	//----------------------------------------------------------------
	void CastShadow::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* lights = visibleSet->GetEntities( CnVisibleSet::Light );
		if (NULL == lights)
			return;

		CnLight* light = 0;

		CnVisibleSet::EntityConstIter i, iend;
		iend = lights->end();
		for (i=lights->begin(); i!=iend; ++i)
		{
			light = static_cast<CnLight*>((*i));
			light->Project( pCamera );
		}
	}

	//==================================================================
	//	CastShadow
	//==================================================================
	__ImplementClass( Cindy, BuildShadowMap, Scene::Component );

	//----------------------------------------------------------------
	BuildShadowMap::BuildShadowMap()
	{
		m_SceneID = L"BuildShadowMap";
	}
	BuildShadowMap::~BuildShadowMap()
	{
	}

	//----------------------------------------------------------------
	/** @brief	초기화 */
	//----------------------------------------------------------------
	void BuildShadowMap::AddChunk( const wchar* type, const wchar* feature, const wchar* shader, const wchar* sort )
	{
		Ptr<CnGeometryChunk> chunk = CnGeometryChunk::Create();

		chunk->SetSortingMode( Sorting::StringToFunc( sort ) );
		chunk->SetRenderState( shader );
		chunk->SetShaderFeature( ShaderFeature::StringToFeature( feature ) );

		m_RenderQueue.AddChunk( GeometryChunk::StringToType( type ), chunk );
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void BuildShadowMap::OnBeginFrame( CnCamera* pCamera )
	{
		Ptr<CnVisibleSet> visibleSet = CnVisibleResolver::Instance()->GetVisibleSet();
		CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (NULL == models || models->empty())
			return;

		CnMesh* mesh = 0;

		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			mesh = (CnMesh*)(*i);
			m_RenderQueue.Insert( mesh );
		}
	}

	//----------------------------------------------------------------
	/** @brief	그림자 맵 만들기 실행 */
	//----------------------------------------------------------------
	void BuildShadowMap::OnFrame( CnCamera* pCamera )
	{
		m_RenderQueue.Draw( pCamera );
	}

	//----------------------------------------------------------------
	/** @brief	 */
	//----------------------------------------------------------------
	void BuildShadowMap::OnEndFrame( CnCamera* pCamera )
	{
		m_RenderQueue.Clear();
	}

} // namespace Scene
} // namespace Cindy