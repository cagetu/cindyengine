//================================================================
// File					: CindySceneComponents.h			
// Original Author		: Changhee
//================================================================
#ifndef __CINDY_SCENE_COMPONENTS_H__
#define __CINDY_SCENE_COMPONENTS_H__

// Component list
#include "CnCommonSceneComponent.h"
#include "CnUpdateSceneComponent.h"
//#include "CnRenderModelComponent.h"
//#include "CnRenderDepthComponent.h"
#include "CnRenderGeometryComponent.h"
#include "CnRenderTextComponent.h"
#include "CnRenderSkyBoxComponent.h"
#include "CnPostProcessComponent.h"
#include "CnDebugSceneComponent.h"
#include "CnShadowComponent.h"
#include "CnOcclusionComponent.h"
//#include "CnMotionBlurComponent.h"
#include "CnDeferredLightComponent.h"
#include "CnLightPrePassComponent.h"
#include "CnSceneComponentManager.h"

#endif	// __CINDY_SCENE_COMPONENTS_H__