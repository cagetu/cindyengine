//================================================================
// File					: CnSceneComponentManager.cpp
// Related Header File	: CnSceneComponentManager.h
// Original Author		: Changhee
//================================================================
#include "CnSceneComponentManager.h"

namespace Cindy
{
namespace Scene
{
	__ImplementSingleton(ComponentManager);
	//----------------------------------------------------------------
	//Const/Dest
	ComponentManager::ComponentManager()
		: m_bOpened(false)
	{
		__ConstructSingleton;
	}
	ComponentManager::~ComponentManager()
	{
		__DestructSingleton;
	}

	//----------------------------------------------------------------
	/**
	*/
	void ComponentManager::Open()
	{
		assert(!m_bOpened);

		m_bOpened = true;
	}

	//----------------------------------------------------------------
	/**
	*/
	void ComponentManager::Close()
	{
		assert(m_bOpened);

		m_Components.clear();
		m_bOpened = false;
	}

	//----------------------------------------------------------------
	/**
	*/
	void ComponentManager::AddComponent( const CnString& strName, const Ptr<Scene::Component>& pComponent )
	{
		std::pair<ComponentMap::iterator, bool> res = m_Components.insert( ComponentMap::value_type(strName, pComponent) );
		assert(res.second == true);
	}

	//----------------------------------------------------------------
	/**
	*/
	void ComponentManager::RemoveComponent( const CnString& strName )
	{
		ComponentMap::iterator iter = m_Components.find(strName);
		if (m_Components.end() == iter)
			return;

		m_Components.erase(iter);
	}

	//----------------------------------------------------------------
	/**
	*/
	Ptr<Scene::Component> ComponentManager::GetComponent( const CnString& strName )
	{
		ComponentMap::iterator iter = m_Components.find(strName);
		if (m_Components.end() == iter)
			return 0;

		return iter->second;
	}

} // namespace Scene
} // namespace Cindy
