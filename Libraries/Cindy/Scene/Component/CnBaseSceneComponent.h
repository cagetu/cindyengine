//================================================================
// File					: CnBaseSceneComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "Foundation/CnObject.h"

namespace Cindy
{
class CnCamera;
class CnRenderQueue;
class CnShader;

typedef CnString	SceneID;

namespace Scene
{
	//==================================================================
	/** Scene Component
		@author			cagetu
		@since			2008년 5월 11일
		@remarks		장면 Component

						Scene(장면)을 구성한다.
	*/
	//==================================================================
	class CN_DLL Component : public CnObject
	{
		__DeclareRtti;
	public:
		Component();
		virtual ~Component();

		const SceneID&	GetID() const;

		void			SetName( const CnString& strName );
		const CnString&	GetName() const;

		virtual void	Open();
		virtual void	Close();

		virtual void	OnUpdate();

		virtual void	OnBeginFrame( CnCamera* pCamera );
		virtual void	OnFrame( CnCamera* pCamera );
		virtual void	OnEndFrame( CnCamera* pCamera );

		// 
		void			Link( const Ptr<Component>& pComponent );
		void			Unlink( const Ptr<Component>& pComponent );
		Ptr<Component>	GetLink( const SceneID& ID ) const;

	protected:
		typedef HashMap<SceneID, Ptr<Component> >	LinkMap;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		SceneID		m_SceneID;
		CnString	m_Name;

		LinkMap		m_Links;
	};

	//----------------------------------------------------------------
	/**
	*/
	inline
	const SceneID& Component::GetID() const
	{
		return m_SceneID;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	void Component::SetName( const CnString& strName )
	{
		m_Name = strName;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline
	const CnString& Component::GetName() const
	{
		return m_Name;
	}
} // namespace Scene
typedef Ptr<Scene::Component> SceneComPtr;
} // namespace Cindy