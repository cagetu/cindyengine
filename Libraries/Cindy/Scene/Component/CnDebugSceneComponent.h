//================================================================
// File					: CnDebugSceneComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"
#include "Geometry/CnVertexData.h"

namespace Cindy
{
class CnSkeletonInstance;

namespace Scene
{
	//==================================================================
	/** RenderAABB
		@author		cagetu
		@brief		AABB 그리기 Component
	*/
	//==================================================================
	class CN_DLL RenderAABB : public Scene::Component
	{
		__DeclareClass(RenderAABB);
	public:
		RenderAABB();
		virtual ~RenderAABB();

		virtual void	OnFrame( CnCamera* pCamera ) override;
	};

	//==================================================================
	/** RenderSkeleton
		@author		cagetu
		@brief		Skeleton 그리기 Component
	*/
	//==================================================================
	class CN_DLL RenderSkeleton : public Scene::Component
	{
		__DeclareClass(RenderSkeleton);
	public:
		RenderSkeleton();
		virtual ~RenderSkeleton();

		virtual void	OnFrame( CnCamera* pCamera ) override;

		void	AddSkeleton( CnSkeletonInstance* Skeleton );
	private:
		typedef std::vector<CnSkeletonInstance*>	SkeletonArray;

		SkeletonArray	m_Skeletons;
	};

	//==================================================================
	/** RenderLight
		@author		cagetu
		@brief		Light 그리기 Component
	*/
	//==================================================================
	class CN_DLL DebugLight : public Scene::Component
	{
		__DeclareClass(DebugLight);
	public:
		DebugLight();
		virtual ~DebugLight();

		virtual void	Open() override;
		virtual void	Close() override;

		virtual void	OnFrame( CnCamera* pCamera ) override;
	private:
		ID3DXMesh*			m_d3dSphere;
		Ptr<CnVertexData>	m_VertexData;
	};

} // namespace Scene
} // namespace Cindy