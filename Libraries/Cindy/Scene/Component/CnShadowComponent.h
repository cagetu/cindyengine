//================================================================
// File					: CnShadowComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"
#include "Graphics/CnGeometryQueue.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** CastShadow
		@author		cagetu
		@brief		그림자 드리우기 Component
	*/
	//==================================================================
	class CN_DLL CastShadow : public Scene::Component
	{
		__DeclareClass(CastShadow);
	public:
		CastShadow();
		virtual ~CastShadow();

		virtual void	OnFrame( CnCamera* pCamera ) override;
	};

	//==================================================================
	/** BuildShadowMap
		@author		cagetu
		@brief		라이트 단위의 그림자 맵을 그리는데 사용하고 싶은 녀석
	*/
	//==================================================================
	class CN_DLL BuildShadowMap : public Scene::Component
	{
		__DeclareClass(BuildShadowMap);
	public:
		BuildShadowMap();
		virtual ~BuildShadowMap();

		/// Add Chunk
		void	AddChunk( const wchar* type, const wchar* feature, const wchar* shader, const wchar* sort );

		/// 실행 전 처리
		void	OnBeginFrame( CnCamera* pCamera ) override;
		/// 실행
		void	OnFrame( CnCamera* pCamera ) override;
		/// 실행 후 처리
		void	OnEndFrame( CnCamera* pCamera ) override;
	private:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		CnGeometryQueue		m_RenderQueue;
	};

} // namespace Scene
} // namespace Cindy