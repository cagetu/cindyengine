//================================================================
// File					: CnOcclusionComponent.cpp
// Related Header File	: CnOcclusionComponent.h
// Original Author		: Changhee
//================================================================
#include "CnOcclusionComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnCamera.h"
#include "Scene/CnVisibleSet.h"
#include "Geometry/CnMesh.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/Base/CnQuery.h"
#include "Material/CnTextureManager.h"
#include "Util/CnLog.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	Occlusion
	//==================================================================
	__ImplementClass( Cindy, Occlusion, Scene::Component );
	//----------------------------------------------------------------
	Occlusion::Occlusion()
	{
		m_SceneID = L"Occlusion";
		m_Query = 0;
	}
	Occlusion::~Occlusion()
	{
		SAFEDEL( m_Query );
	}

	//--------------------------------------------------------------
	/**	@brief	초기화
		@desc	
	*/
	void Occlusion::Open()
	{
		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice( &d3dDevice );

		// Get the display mode to obtain the format
		D3DDISPLAYMODE mode;
		d3dDevice->GetDisplayMode( 0, &mode );

		m_Query = RenderDevice->CreateQuery( CnQuery::QUERYTYPE_OCCLUSION );

		ushort usage = CnTexture::RenderTargetColor | CnTexture::RenderTargetDepthStencil;
		m_OcclusionToTexture = TextureMgr->Load( L"OcclusionQuery", 320, 240, 32, (PixelFormat::Code)mode.Format, usage );
	}

	//--------------------------------------------------------------
	/**	@brief	실행
	*/
	void Occlusion::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (NULL == models || models->empty())
			return;

		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice( &d3dDevice );

		RenderDevice->OpenSurface( 0 );
		{
			if (RenderDevice->BeginScene( IRenderer::CLEAR_TARGET | IRenderer::CLEAR_ZBUFFER, CN_RGBA(255, 255, 255, 255), 1.0f, 0 ))
			{
				d3dDevice->SetVertexShader( 0 );
				d3dDevice->SetFVF( D3DFVF_XYZ );
				d3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
				d3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

				
				Math::Matrix44 mat;
				mat.Identity();

				d3dDevice->SetTransform( D3DTS_WORLD, &mat );
				d3dDevice->SetTransform( D3DTS_PROJECTION, &pCamera->GetProjection() );
				d3dDevice->SetTransform( D3DTS_VIEW, &pCamera->GetView() );

				// variables
				Math::AABB aabb;
				Math::Vector3 vMin, vMax;
				Math::Vector3 v[3*12];

				CnMesh* mesh = NULL;
				CnVisibleSet::EntityConstIter i, iend;

				// 먼저, 모든 오브젝트의 bounding box를 그린다.
				iend = models->end();
				for (i=models->begin(); i!=iend; ++i)
				{
					mesh = (CnMesh*)(*i);

					mesh->GetAABB( aabb );

					vMin = aabb.GetMin();
					vMax = aabb.GetMax();

					// front
					v[0].x = vMin.x; v[0].y = vMax.y; v[0].z = vMin.z;
					v[1].x = vMin.x; v[1].y = vMin.y; v[1].z = vMin.z;
					v[2].x = vMax.x; v[2].y = vMin.y; v[2].z = vMin.z;
					v[3].x = vMin.x; v[3].y = vMax.y; v[3].z = vMin.z;
					v[4].x = vMax.x; v[4].y = vMin.y; v[4].z = vMin.z;
					v[5].x = vMax.x; v[5].y = vMax.y; v[5].z = vMin.z;

					// left
					v[6].x = vMin.x; v[6].y = vMax.y; v[6].z = vMax.z;
					v[7].x = vMin.x; v[7].y = vMin.y; v[7].z = vMax.z;
					v[8].x = vMin.x; v[8].y = vMin.y; v[8].z = vMin.z;
					v[9].x = vMin.x; v[9].y = vMax.y; v[9].z = vMax.z;
					v[10].x = vMin.x; v[10].y = vMin.y; v[10].z = vMin.z;
					v[11].x = vMin.x; v[11].y = vMax.y; v[11].z = vMin.z;

					// right
					v[12].x = vMax.x; v[12].y = vMax.y; v[12].z = vMin.z;
					v[13].x = vMax.x; v[13].y = vMin.y; v[13].z = vMin.z;
					v[14].x = vMax.x; v[14].y = vMin.y; v[14].z = vMax.z;
					v[15].x = vMax.x; v[15].y = vMax.y; v[15].z = vMin.z;
					v[16].x = vMax.x; v[16].y = vMin.y; v[16].z = vMax.z;
					v[17].x = vMax.x; v[17].y = vMax.y; v[17].z = vMax.z;

					// back
					v[18].x = vMax.x; v[18].y = vMax.y; v[18].z = vMax.z;
					v[19].x = vMax.x; v[19].y = vMin.y; v[19].z = vMax.z;
					v[20].x = vMin.x; v[20].y = vMin.y; v[20].z = vMax.z;
					v[21].x = vMax.x; v[21].y = vMax.y; v[21].z = vMax.z;
					v[22].x = vMin.x; v[22].y = vMin.y; v[22].z = vMax.z;
					v[23].x = vMin.x; v[23].y = vMax.y; v[23].z = vMax.z;

					// top
					v[24].x = vMin.x; v[24].y = vMax.y; v[24].z = vMin.z;
					v[25].x = vMin.x; v[25].y = vMax.y; v[25].z = vMax.z;
					v[26].x = vMax.x; v[26].y = vMax.y; v[26].z = vMax.z;
					v[27].x = vMin.x; v[27].y = vMax.y; v[27].z = vMin.z;
					v[28].x = vMax.x; v[28].y = vMax.y; v[28].z = vMax.z;
					v[29].x = vMax.x; v[29].y = vMax.y; v[29].z = vMin.z;

					// bottom
					v[30].x = vMin.x; v[30].y = vMin.y; v[30].z = vMin.z;
					v[31].x = vMin.x; v[31].y = vMin.y; v[31].z = vMax.z;
					v[32].x = vMax.x; v[32].y = vMin.y; v[32].z = vMax.z;
					v[33].x = vMin.x; v[33].y = vMin.y; v[33].z = vMin.z;
					v[34].x = vMax.x; v[34].y = vMin.y; v[34].z = vMax.z;
					v[35].x = vMax.x; v[35].y = vMin.y; v[35].z = vMin.z;

					d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLELIST, 12, v, sizeof(Math::Vector3) );
				}

				// 현재, 각 box를 다시 한번 그린다. 이번에는 occlusion query를 사용하여, 얼마나 많은
				// pixel들을 보이는지를 센다. 모든 bounding box들은 이미 랜더링 되었기 때문에, 
				// right amount 를 얻기 위함이다.
				iend = models->end();
				for (i=models->begin(); i!=iend; ++i)
				{
					mesh = (CnMesh*)(*i);

					mesh->GetAABB( aabb );

					vMin = aabb.GetMin();
					vMax = aabb.GetMax();

					m_Query->Begin();
					{
						// front
						v[0].x = vMin.x; v[0].y = vMax.y; v[0].z = vMin.z;
						v[1].x = vMin.x; v[1].y = vMin.y; v[1].z = vMin.z;
						v[2].x = vMax.x; v[2].y = vMin.y; v[2].z = vMin.z;
						v[3].x = vMin.x; v[3].y = vMax.y; v[3].z = vMin.z;
						v[4].x = vMax.x; v[4].y = vMin.y; v[4].z = vMin.z;
						v[5].x = vMax.x; v[5].y = vMax.y; v[5].z = vMin.z;

						// left
						v[6].x = vMin.x; v[6].y = vMax.y; v[6].z = vMax.z;
						v[7].x = vMin.x; v[7].y = vMin.y; v[7].z = vMax.z;
						v[8].x = vMin.x; v[8].y = vMin.y; v[8].z = vMin.z;
						v[9].x = vMin.x; v[9].y = vMax.y; v[9].z = vMax.z;
						v[10].x = vMin.x; v[10].y = vMin.y; v[10].z = vMin.z;
						v[11].x = vMin.x; v[11].y = vMax.y; v[11].z = vMin.z;

						// right
						v[12].x = vMax.x; v[12].y = vMax.y; v[12].z = vMin.z;
						v[13].x = vMax.x; v[13].y = vMin.y; v[13].z = vMin.z;
						v[14].x = vMax.x; v[14].y = vMin.y; v[14].z = vMax.z;
						v[15].x = vMax.x; v[15].y = vMax.y; v[15].z = vMin.z;
						v[16].x = vMax.x; v[16].y = vMin.y; v[16].z = vMax.z;
						v[17].x = vMax.x; v[17].y = vMax.y; v[17].z = vMax.z;

						// back
						v[18].x = vMax.x; v[18].y = vMax.y; v[18].z = vMax.z;
						v[19].x = vMax.x; v[19].y = vMin.y; v[19].z = vMax.z;
						v[20].x = vMin.x; v[20].y = vMin.y; v[20].z = vMax.z;
						v[21].x = vMax.x; v[21].y = vMax.y; v[21].z = vMax.z;
						v[22].x = vMin.x; v[22].y = vMin.y; v[22].z = vMax.z;
						v[23].x = vMin.x; v[23].y = vMax.y; v[23].z = vMax.z;

						// top
						v[24].x = vMin.x; v[24].y = vMax.y; v[24].z = vMin.z;
						v[25].x = vMin.x; v[25].y = vMax.y; v[25].z = vMax.z;
						v[26].x = vMax.x; v[26].y = vMax.y; v[26].z = vMax.z;
						v[27].x = vMin.x; v[27].y = vMax.y; v[27].z = vMin.z;
						v[28].x = vMax.x; v[28].y = vMax.y; v[28].z = vMax.z;
						v[29].x = vMax.x; v[29].y = vMax.y; v[29].z = vMin.z;

						// bottom
						v[30].x = vMin.x; v[30].y = vMin.y; v[30].z = vMin.z;
						v[31].x = vMin.x; v[31].y = vMin.y; v[31].z = vMax.z;
						v[32].x = vMax.x; v[32].y = vMin.y; v[32].z = vMax.z;
						v[33].x = vMin.x; v[33].y = vMin.y; v[33].z = vMin.z;
						v[34].x = vMax.x; v[34].y = vMin.y; v[34].z = vMax.z;
						v[35].x = vMax.x; v[35].y = vMin.y; v[35].z = vMin.z;

						d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLELIST, 12, v, sizeof(Math::Vector3) );

						//// front
						//v[0].x = vMin.x; v[0].y = vMax.y; v[0].z = vMin.z;
						//v[1].x = vMin.x; v[1].y = vMin.y; v[1].z = vMin.z;
						//v[2].x = vMax.x; v[2].y = vMax.y; v[2].z = vMin.z;
						//v[3].x = vMax.x; v[3].y = vMin.y; v[3].z = vMin.z;
						//d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, v, sizeof(Math::Vector3) );

						//// right
						//v[0].x = vMax.x; v[0].y = vMax.y; v[0].z = vMin.z;
						//v[1].x = vMax.x; v[1].y = vMin.y; v[1].z = vMin.z;
						//v[2].x = vMax.x; v[2].y = vMax.y; v[2].z = vMax.z;
						//v[3].x = vMax.x; v[3].y = vMin.y; v[3].z = vMax.z;
						//d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, v, sizeof(Math::Vector3) );

						//// back
						//v[0].x = vMax.x; v[0].y = vMax.y; v[0].z = vMax.z;
						//v[1].x = vMax.x; v[1].y = vMin.y; v[1].z = vMax.z;
						//v[2].x = vMin.x; v[2].y = vMax.y; v[2].z = vMax.z;
						//v[3].x = vMin.x; v[3].y = vMin.y; v[3].z = vMax.z;
						//d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, v, sizeof(Math::Vector3) );

						//// left
						//v[0].x = vMin.x; v[0].y = vMax.y; v[0].z = vMin.z;
						//v[1].x = vMin.x; v[1].y = vMin.y; v[1].z = vMin.z;
						//v[2].x = vMin.x; v[2].y = vMax.y; v[2].z = vMax.z;
						//v[3].x = vMin.x; v[3].y = vMin.y; v[3].z = vMax.z;
						//d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, v, sizeof(Math::Vector3) );

						//// top
						//v[0].x = vMin.x; v[0].y = vMax.y; v[0].z = vMax.z;
						//v[1].x = vMin.x; v[1].y = vMax.y; v[1].z = vMin.z;
						//v[2].x = vMax.x; v[2].y = vMax.y; v[2].z = vMax.z;
						//v[3].x = vMax.x; v[3].y = vMax.y; v[3].z = vMin.z;
						//d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, v, sizeof(Math::Vector3) );

						//// bottom
						//v[0].x = vMin.x; v[0].y = vMin.y; v[0].z = vMin.z;
						//v[1].x = vMin.x; v[1].y = vMin.y; v[1].z = vMax.z;
						//v[2].x = vMax.x; v[2].y = vMin.y; v[2].z = vMin.z;
						//v[3].x = vMax.x; v[3].y = vMin.y; v[3].z = vMax.z;
						//d3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, v, sizeof(Math::Vector3) );
					}
					m_Query->End();

					// Loop until the data becomes available
					DWORD pixelsVisible = 0;
					m_Query->GetData( (void*)&pixelsVisible, sizeof(DWORD) );

					if (pixelsVisible == 0)
					{
						mesh->SetCulled( true );	// No pixels visible, do not render

						CnPrint( L"[Occlusion::Run]: %s is culled", mesh->GetName().c_str() );
					}
					else
					{
						mesh->SetCulled( false );	// Pixels visible, render
					}
				} // for

				RenderDevice->EndScene();
			} // beginScene

		} // Surface
		RenderDevice->CloseSurface(0);
	}

} // namespace Scene
} // namespace Cindy
