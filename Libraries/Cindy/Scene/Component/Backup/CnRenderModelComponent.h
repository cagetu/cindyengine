//================================================================
// File					: CnRenderModelComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"
#include "CnUpdateSceneComponent.h"

#include "Render/CnRenderQueue.h"
#include "Render/CnGeometryQueue.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** RenderModel
		@author		cagetu
		@brief		�� �׸��� Component
	*/
	//==================================================================
	class CN_DLL RenderModel : public Scene::Component
	{
		__DeclareClass(RenderModel);
	public:
		RenderModel();
		virtual ~RenderModel();

		virtual void	Open() override;

		// Render
		virtual void	OnBeginFrame( CnCamera* pCamera ) override;
		virtual void	OnFrame( CnCamera* pCamera ) override;
		virtual void	OnEndFrame( CnCamera* pCamera ) override;

	private:
		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		CnRenderQueue		m_RenderQueue;
		CnGeometryQueue		m_GeometryQueue;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		void	UpdateQueue( CnCamera* pCamera );
		void	RenderQueue( CnCamera* pCamera );
	};

} // namespace Scene
} // namespace Cindy