//================================================================
// File					: CnRenderDepthComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"

#include "Geometry/CnVertexData.h"
#include "Material/CnMaterial.h"
#include "Geometry/CnMesh.h"

#include "Render/CnGeometryQueue.h"

namespace Cindy
{
namespace Scene
{
//#define SORT_FRONT_TO_BACK

	//==================================================================
	/** RenderDepth
		@author		cagetu
		@brief		깊이 값만 랜더링


		-09/10/26 : Double-Speed Z (EarlyZ)를 이용하기 위해서는, 오직 Z값만 기록!!!!
					 (Stencil도 안되나?!?!?!)
					 작업 요망!!!!

	    // DepthBuffer를 기록할 때의 RenderState!!!
	    technique t0
		{
			pass p0
			{
				ColorWriteEnable  = RED|GREEN|BLUE|ALPHA; // write depth as color for later use in other shaders
				ZEnable           = True;
				ZWriteEnable      = True;
				ZFunc             = LessEqual;
				StencilEnable     = False;
				FogEnable         = False;
				AlphaBlendEnable  = False;
				AlphaTestEnable   = False;
				AlphaFunc         = GreaterEqual;
				ScissorTestEnable = False;
				CullMode          = CW;
			}
		}

		// ColorBuffer를 기록할 때의 RenderState!!!
		technique t0
		{
			pass p0
			{
				ColorWriteEnable  = RED|GREEN|BLUE|ALPHA;
				ZEnable           = True;
				ZWriteEnable      = False;
				ZFunc             = Equal;
				StencilEnable     = False;
				FogEnable         = False;
				AlphaBlendEnable  = False;
				AlphaTestEnable   = False;
				AlphaFunc         = GreaterEqual;
				ScissorTestEnable = False;
				CullMode          = CW;
			}
		}

		-09/11/17 : EarlyZ를 사용하기 위해서는, Depth를 기록할 때, 그리는 tm 정보와 Color를 그릴 때의 tm정보가 완전히 동일해야한다.
					또, DepthBias와 ScopeScaleDepthBias의 설정도 주의해야 한다.
	*/
	//==================================================================
	class CN_DLL RenderDepth : public Scene::Component
	{
		__DeclareClass(RenderDepth);
	public:
		RenderDepth();
		virtual ~RenderDepth();

		virtual void	Open() override;

		virtual void	OnBeginFrame( CnCamera* pCamera ) override;
		virtual void	OnFrame( CnCamera* pCamera ) override;
		virtual void	OnEndFrame( CnCamera* pCamera ) override;

		// Shader 설정
		virtual void	SetShader( const CnString& Shader );
	protected:
		// 정렬
		struct Sort : public binary_function< const CnSceneEntity*, const CnSceneEntity*, bool >
		{
			bool operator () ( const CnSceneEntity* Dest, const CnSceneEntity* Src ) const
			{
				return ((CnMesh*)Dest)->IsSkinned() < ((CnMesh*)Src)->IsSkinned();
			}
		};

#ifdef SORT_FRONT_TO_BACK
		// 가까운 녀석부터 정렬
		struct FrontToBack : public binary_function< const CnSceneEntity*, const CnSceneEntity*, bool >
		{
			bool operator () ( const CnSceneEntity* Dest, const CnSceneEntity* Src ) const
			{
				return ((CnMesh*)Dest)->GetDistanceToCamera() < ((CnMesh*)Src)->GetDistanceToCamera();
			}
		};
#endif

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		CnMaterial		m_Material;

		CnGeometryQueue	m_RenderQueue;
	};
} // namespace Scene
} // namespace Cindy