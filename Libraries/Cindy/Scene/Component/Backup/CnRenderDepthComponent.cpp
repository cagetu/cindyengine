//================================================================
// File					: CnRenderDepthComponent.cpp
// Related Header File	: CnRenderDepthComponent.h
// Original Author		: Changhee
//================================================================
#include "CnRenderDepthComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnCamera.h"
#include "Scene/CnVisibleSet.h"
#include "Render/CnRenderer.h"
#include "Material/CnShaderManager.h"
#include "Util/CnLog.h"

namespace Cindy
{
namespace Scene
{
#define TEST_CHUNK

	//==================================================================
	//	RenderDepth
	//==================================================================
	__ImplementClass(Cindy, RenderDepth, Scene::Component);

	//----------------------------------------------------------------
	RenderDepth::RenderDepth()
	{
		m_SceneID = L"RenderDepth";
	}
	RenderDepth::~RenderDepth()
	{
	}

	//----------------------------------------------------------------
	/** @brief	초기화 */
	//----------------------------------------------------------------
	void RenderDepth::Open()
	{
#ifdef TEST_CHUNK
		Ptr<CnGeometryChunk> chunk;

		chunk = CnGeometryChunk::Create();
		chunk->SetSortingMode( Sorting::Greater );
		chunk->SetRenderState( L"b_solid.fx" );
		chunk->SetShaderFeature( ShaderFeature::Depth );
		m_RenderQueue.AddChunk( GeometryChunk::Opaque, chunk );

		chunk = CnGeometryChunk::Create();
		chunk->SetSortingMode( Sorting::Greater );
		chunk->SetRenderState( L"b_alphatest.fx" );
		chunk->SetShaderFeature( ShaderFeature::Depth );
		m_RenderQueue.AddChunk( GeometryChunk::AlphaTest, chunk );

		chunk = CnGeometryChunk::Create();
		chunk->SetSortingMode( Sorting::Greater );
		chunk->SetRenderState( L"b_solid.fx" );
		chunk->SetShaderFeature( ShaderFeature::Depth );
		m_RenderQueue.AddChunk( GeometryChunk::AlphaBlend, chunk );
#endif
	}

	//----------------------------------------------------------------
	/** @brief	쉐이더 설정 */
	//----------------------------------------------------------------
	void RenderDepth::SetShader( const CnString& Shader )
	{
		m_Material.SetShader(Shader.c_str(), 1);
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void RenderDepth::OnBeginFrame( CnCamera* pCamera )
	{
#ifdef TEST_CHUNK
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (NULL == models || models->empty())
			return;

		CnMesh* mesh = 0;

		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			mesh = (CnMesh*)(*i);

			m_RenderQueue.Insert( mesh );
		}
#endif
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void RenderDepth::OnFrame( CnCamera* pCamera )
	{
		m_RenderQueue.Draw( pCamera );
#ifndef TEST_CHUNK
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (NULL == models || models->empty())
			return;

		CnMesh* mesh = 0;
		CnTechnique* technique = 0;
		int numPasses = 0;
		CnPass* pass = NULL;
		ShaderHandle handle = NULL;

		// RenderState
		//RenderDevice->SetRenderState( *m_Material.GetRenderState() );

		LPDIRECT3DDEVICE9 d3d9Device = 0;
		RenderDevice->GetCurrentDevice(&d3d9Device);

		/// zenble 
		d3d9Device->SetRenderState(D3DRS_ZENABLE, TRUE);
		d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
		d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

		/// enable writing
		//d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, 0);
		d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);

		// enable alpha
		d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
		d3d9Device->SetRenderState(D3DRS_ALPHAREF, 120);

		d3d9Device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_FOGENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

#ifdef SORT_FRONT_TO_BACK
		std::sort( models->begin(), models->end(), FrontToBack() );
#else
		// 정적 객체부터 정렬.
		std::sort( models->begin(), models->end(), Sort() );
#endif

		//CnVisibleSet::EntityConstIter i, iend;
		//iend = models->end();
		//for (i=models->begin(); i!=iend; ++i)
		//{
		//	(*i)->Apply( pCamera );
		//}

		/// 
		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			mesh = (CnMesh*)(*i);

			/// Shader 설정
			m_Material.ResetFeatures();
				if (mesh->IsSkinned())
					m_Material.AddFeature( ShaderFeature::Skinned );
			m_Material.Apply();

			/// Apply Shader
			RenderDevice->SetActiveShader( m_Material.GetShader()->GetEffect() );

			technique = m_Material.GetCurrentTechnique();
			numPasses = technique->Begin();
			Assert(numPasses > 0, L"Invalid EffectPass Size");

			/// Vertex Information
			RenderDevice->SetVertexDeclaration( mesh->GetVertexData()->GetDeclaration() );
			RenderDevice->SetStreamSource( mesh->GetVertexData() );

			/// Draw 
			ushort subMeshes = mesh->GetNumSubMeshes();
			for (ushort cur = 0; cur < subMeshes; ++cur)
			{
				CnSubMesh* subMesh = mesh->GetSubMesh(cur);
				RenderDevice->SetIndexBuffer( subMesh->GetIndexBuffer() );

				//@add by cagetu -09/11/13 : Nebula3에서 사용하는 형식을 그대로 적용해봄..
				//							 원칙적으로는 AlphaTest도 허용하지 않는다!!!
				bool alphaTest = subMesh->GetMaterial()->GetRenderState()->GetAlphaTestEnable();
				d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, (BOOL)alphaTest);

				subMesh->ApplyState( pCamera );

				for (int curPass=0; curPass<numPasses; ++curPass)
				{
					pass = technique->GetPass(curPass);
					if (NULL == pass)
						continue;

					pass->Begin();
					{
						subMesh->Draw( pCamera );
					}
					pass->End();
				}
			}

			technique->End();
		}
#endif
		//CnPrint( L":::: Run Depth Component ::::\n" );
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void RenderDepth::OnEndFrame( CnCamera* pCamera )
	{
		m_RenderQueue.Clear();
	}

} // namespace Scene
} // namespace Cindy