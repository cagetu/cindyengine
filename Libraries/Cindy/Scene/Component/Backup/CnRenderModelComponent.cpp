//================================================================
// File					: CnRenderModelComponent.cpp
// Related Header File	: CnRenderModelComponent.h
// Original Author		: Changhee
//================================================================
#include "CnRenderModelComponent.h"
#include "Scene/CnVisibleSet.h"
#include "Scene/CnCamera.h"
#include "Scene/CnSceneEntity.h"
#include "Render/CnRenderer.h"

namespace Cindy
{
namespace Scene
{
#define TEST_CHUNK
	//==================================================================
	//	RenderModel
	//==================================================================
	__ImplementClass( Cindy, RenderModel, Scene::Component );

	//----------------------------------------------------------------
	RenderModel::RenderModel()
	{
		m_SceneID = L"RenderModel";
	}
	RenderModel::~RenderModel()
	{
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	void RenderModel::Open()
	{
#ifdef TEST_CHUNK
		Ptr<CnGeometryChunk> chunk;

		chunk = CnGeometryChunk::Create();
		chunk->SetSortingMode( Sorting::Greater );
		chunk->SetRenderState( L"b_solid.fx" );
		m_GeometryQueue.AddChunk( GeometryChunk::Opaque, chunk );

		chunk = CnGeometryChunk::Create();
		chunk->SetSortingMode( Sorting::Greater );
		chunk->SetRenderState( L"b_alphatest.fx" );
		m_GeometryQueue.AddChunk( GeometryChunk::AlphaTest, chunk );

		chunk = CnGeometryChunk::Create();
		chunk->SetSortingMode( Sorting::Less );
		chunk->SetRenderState( L"b_alphablend.fx" );
		m_GeometryQueue.AddChunk( GeometryChunk::AlphaBlend, chunk );
#endif
	}

	//----------------------------------------------------------------
	/** @brief	랜더큐에 넣기 */
	//----------------------------------------------------------------
	void RenderModel::UpdateQueue( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		m_RenderQueue.Clear();

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (0 == models)
			return;

		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			if (!(*i)->IsVisible() ||
				 (*i)->IsCulled() )
				continue;

			(*i)->Apply( pCamera );

			m_RenderQueue.Register( (*i) );
		}
	}

	//----------------------------------------------------------------
	/** @brief	랜더큐 그리기 */
	//----------------------------------------------------------------
	void RenderModel::RenderQueue( CnCamera* pCamera )
	{
		LPDIRECT3DDEVICE9 d3d9Device = 0;
		RenderDevice->GetCurrentDevice(&d3d9Device);

		/// zenble 
		d3d9Device->SetRenderState(D3DRS_ZENABLE, TRUE);		// early-Z 일 경우에는 FALSE
		d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
		//d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
		/// early-Z 일 경우에는 Equal
		d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_EQUAL);
		//d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

		/// enable writing
		d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);

		/// enable alpha
		d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		///
		d3d9Device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_FOGENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

		m_RenderQueue.Draw( pCamera );
	}

	//----------------------------------------------------------------
	/** @brief	Run */
	//----------------------------------------------------------------
	void RenderModel::OnBeginFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (0 == models)
			return;

		CnMesh* mesh = 0;

		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			if (!(*i)->IsVisible() ||
				 (*i)->IsCulled() )
				continue;

			mesh = (CnMesh*)(*i);

			m_GeometryQueue.Insert( mesh );
		}
	}

	//----------------------------------------------------------------
	/** @brief	Run */
	//----------------------------------------------------------------
	void RenderModel::OnFrame( CnCamera* pCamera )
	{
#ifdef TEST_CHUNK
		m_GeometryQueue.Draw( pCamera );
#else
		UpdateQueue( pCamera );
		RenderQueue( pCamera );
#endif // TEST_CHUNK
	}

	//----------------------------------------------------------------
	/** @brief	Run */
	//----------------------------------------------------------------
	void RenderModel::OnEndFrame( CnCamera* pCamera )
	{
		m_GeometryQueue.Clear();
	}

} // namespace Scene
} // namespace Cindy