//================================================================
// File					: CnCommonSceneComponent.cpp
// Related Header File	: CnCommonSceneComponent.h
// Original Author		: Changhee
//================================================================
#include "CnCommonSceneComponent.h"
#include "Scene/CnCamera.h"
#include "Util/CnLog.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/CnRenderTarget.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	BeginScene
	//==================================================================
	__ImplementClass( Cindy, BeginScene, Scene::Component );
	//----------------------------------------------------------------
	BeginScene::BeginScene()
	{
		m_SceneID = L"BeginScene";
		m_ClearFlag = IRenderer::CLEAR_TARGET | IRenderer::CLEAR_ZBUFFER | IRenderer::CLEAR_STENCIL;
	}

	//--------------------------------------------------------------
	void BeginScene::SetClearFlag( ulong ulClearFlag )
	{
		m_ClearFlag = ulClearFlag;
	}

	//--------------------------------------------------------------
	void BeginScene::OnFrame( CnCamera* pCamera )
	{
		Ptr<CnRenderTarget> currentRenderTarget = RenderDevice->GetCurrentRenderTarget();
		assert(false == currentRenderTarget.IsNull());

		m_ClearFlag = IRenderer::CLEAR_TARGET;
		if (currentRenderTarget->IsEnabledDepthStencil() ||
			currentRenderTarget->GetType() == CnRenderTarget::RT_WINDOW)
		{
			m_ClearFlag |= IRenderer::CLEAR_ZBUFFER | IRenderer::CLEAR_STENCIL;
		}
		//if (currentRenderTarget->IsEnabledDepthStencil())
		//{
		//	m_ClearFlag |= IRenderer::CLEAR_ZBUFFER | IRenderer::CLEAR_STENCIL;
		//}

		RenderDevice->BeginScene( m_ClearFlag );

		//CnPrint( L":::: Run Begin Component ::::\n" );
	}

	//==================================================================
	//	EndScene
	//==================================================================
	__ImplementClass( Cindy, EndScene, Scene::Component );
	//----------------------------------------------------------------
	EndScene::EndScene()
	{
		m_SceneID = L"EndScene";
	}

	//--------------------------------------------------------------
	void EndScene::OnFrame( CnCamera* pCamera )
	{
		RenderDevice->EndScene();

		//CnPrint( L":::: Run End Component ::::\n" );
	}

} // namespace Scene
} // namespace Cindy