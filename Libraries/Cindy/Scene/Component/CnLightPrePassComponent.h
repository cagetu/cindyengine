//================================================================
// File					: CnLightPrePassLightComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnPostProcessComponent.h"

namespace Cindy
{
class CnShader;
class CnLight;

namespace Scene
{
namespace LightPrePass
{
	//==================================================================
	/** Base Lighting
	*/
	//==================================================================
	class BaseLighting : public PostProcessComponent
	{
		__DeclareRtti;
	public:
		BaseLighting();
		virtual ~BaseLighting();

		// ����
		void	OnFrame( CnCamera* pCamera ) override;

	protected:
		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		virtual bool	CheckLightType( CnLight* pLight ) abstract;

		void			ApplyRenderState();

		virtual void	ApplyShaderState( CnShader* pShader, CnCamera* pCamera );
		virtual void	ApplyShaderStatePerLight( CnShader* pShader, CnLight* pLight, CnCamera* pCamera );
	};

	//==================================================================
	/** Direction Lighting
	*/
	//==================================================================
	class CN_DLL DirectionLighting : public BaseLighting
	{
		__DeclareClass(DirectionLighting);
	public:
		DirectionLighting();
		virtual ~DirectionLighting();

	private:
		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		bool	CheckLightType( CnLight* pLight ) override;
	};

	//==================================================================
	/** Point Lighting
	*/
	//==================================================================
	class CN_DLL PointLighting : public BaseLighting
	{
		__DeclareClass(PointLighting);
	public:
		PointLighting();
		virtual ~PointLighting();

		void	Open() override;
		void	Close() override;

		void	OnFrame( CnCamera* pCamera ) override;
	private:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		ID3DXMesh* m_d3dSphere;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		bool	CheckLightType( CnLight* pLight ) override;

		void	ApplyShaderState( CnShader* pShader, CnCamera* pCamera ) override;
		void	ApplyShaderStatePerLight( CnShader* pShader, CnLight* pLight, CnCamera* pCamera ) override;
	};

} // namespace LightPrePass
} // namespace Scene
} // namespace Cindy