//================================================================
// File					: CnPostProcessComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"

#include "Geometry/CnVertexData.h"
#include "Graphics/Base/CnIndexBuffer.h"

#include "Material/CnMaterial.h"
#include "Math/CindyMath.h"
#include "Util/CnVariable.h"

#include <MoCommon/External/minixml.h>

namespace Cindy
{

namespace PostEffect
{
class Effect;
}

namespace Scene
{
	//==================================================================
	/** PostProcessComponent
		@desc	PostProcessing용 장면 사각형 메쉬
	*/
	//==================================================================
	class CN_DLL PostProcessComponent : public Scene::Component
	{
		__DeclareClass(PostProcessComponent);
	public:
		PostProcessComponent();
		virtual ~PostProcessComponent();

		static PostProcessComponent* Create(int nWidth, int nHeight, float tu0 = 0.0f, float tv0 = 0.0f, float tu1 = 1.0f, float tv1 = 1.0f);

		// Initialize
		bool		Init(int nWidth, int nHeight, float tu0 = 0.0f, float tv0 = 0.0f, float tu1 = 1.0f, float tv1 = 1.0f);
		void		Init(CMiniXML::const_iterator node, PostEffect::Effect* effect);
		void		Discard();

		// Material
		void		 SetMaterial( const CnMaterial& Material );
		virtual void SetMaterial( const CMiniXML::iterator& node );
		//CnMaterial*	GetMaterial();

		void		SetParameter(const wchar* param, float var);
		void		SetParameter(const wchar* param, const Math::Vector3& var);
		void		SetParameter(const wchar* param, const Math::Vector4& var);

		void		SetSourceTexSize(const wchar* maptype);

		// Offset, Weights 구하기
		void		SetSampleOffsets( Math::Vector2* paOffsets, int nSize );
		void		SetSampleWeights( Math::Vector4* paWeights, int nSize );

		void		CalcDown2x2SampleOffsets(int width, int height);
		void		CalcDown4x4SampleOffsets(int width, int height);
		void		CalcGaussBlur5x5SampleOffsets(int width, int height);
		void		CalcBloomSampleOffsets( int width, int height, float fDeviation, float fMultiplier );
		void		CalcToneMappingSampleOffsets(int width, int height);

		// Run
		void		OnFrame( CnCamera* pCamera ) override;
	protected:
		void	SetVertexStream();
		void	SetIndexStream();

		int					m_Width;
		int					m_Height;

		Ptr<CnVertexData>	m_VertexData;
		Ptr<CnIndexBuffer>	m_IndexBuffer;

		CnMaterial			m_Material;

		CnString			m_SourceTexture;

		// Sampling
		Math::Vector2		m_aSampleOffsets[16];
		Math::Vector4		m_aSampleWeights[16];

//#pragma TODO("정리해주세용~~~!!")
		// 시간
		ulong				m_PrevTick;
		float				m_CurrentTime;
		float				m_MaxTime;

		struct _PARAM
		{
			int sementic;
			CnVariable v;
			
			bool operator == (int h) const { return this->sementic == h ? true : false; }
		};
		std::vector<_PARAM> m_ShaderParams;

		//----------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------
		virtual void ApplyShaderVariables( CnCamera* pCamera );
	};

	//----------------------------------------------------------------
	/**	@brief	매터리얼 설정
	*/
	inline
	void PostProcessComponent::SetMaterial( const CnMaterial& Material )
	{
		m_Material = Material;
	}

	//----------------------------------------------------------------
	/**	@brief	매터리얼의 포인터를 얻어온다.
	*/
	//inline
	//CnMaterial* PostProcessComponent::GetMaterial()
	//{
	//	return &m_Material;
	//}

} // namespace Scene
} // namespace Cindy