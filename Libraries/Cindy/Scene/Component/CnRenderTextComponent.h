//================================================================
// File					: CnRenderTextComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** Render Text Component
		@desc	Text Render Component 
	*/
	//==================================================================
	class CN_DLL RenderText : public Scene::Component
	{
		__DeclareClass(RenderText);
	public:
		RenderText();
		virtual ~RenderText();

		// Run
		void		OnFrame( CnCamera* pCamera ) override;

	protected:
		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
	};

} // namespace Scene
} // namespace Cindy