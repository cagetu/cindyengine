//================================================================
// File					: CnDeferredLightComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnPostProcessComponent.h"

namespace Cindy
{
class CnShader;
class CnTechnique;

namespace Scene
{
namespace Deferred
{
	//==================================================================
	/** Deferred Lighting - Global Light
	*/
	//==================================================================
	class CN_DLL GlobalLight : public PostProcessComponent
	{
		__DeclareClass(GlobalLight);
	public:
		GlobalLight();
		virtual ~GlobalLight();

		// 실행
		void OnFrame( CnCamera* pCamera ) override;
	private:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		void ApplyRenderState();
		void ApplyShaderState( CnCamera* pCamera, CnShader* pShader, CnTechnique* pTechnique );
	};

	//==================================================================
	/** Deferred Lighting - Point Light
	*/
	//==================================================================
	class CN_DLL PointLight : public Scene::Component
	{
		__DeclareClass(PointLight);
	public:
		PointLight();
		virtual ~PointLight();

		// 시작
		void Open() override;
		void Close() override;

		// Material
		void SetMaterial( const CnMaterial& Material );
		CnMaterial*	GetMaterial();

		// 실행
		void OnFrame( CnCamera* pCamera ) override;

	private:
		ID3DXMesh*			m_d3dSphere;
		Ptr<CnVertexData>	m_VertexData;

		CnMaterial			m_Material;
	};

	//==================================================================
	/** Deferred Lighting - Spot Light
	*/
	//==================================================================
	class CN_DLL SpotLight : public Scene::Component
	{
		__DeclareClass(SpotLight);
	public:
		SpotLight();
		virtual ~SpotLight();

		// 시작
		void		Open() override;
		void		Close() override;

		// Material
		void		SetMaterial( const CnMaterial& Material );
		CnMaterial*	GetMaterial();

		// 실행
		void OnFrame( CnCamera* pCamera ) override;

	private:
		ID3DXMesh*			m_d3dCone;
		Ptr<CnVertexData>	m_VertexData;

		CnMaterial			m_Material;
	};

	//// Point Light
	//----------------------------------------------------------------
	/**	@brief	매터리얼 설정
	*/
	inline
	void PointLight::SetMaterial( const CnMaterial& Material )
	{
		m_Material = Material;
	}

	//----------------------------------------------------------------
	/**	@brief	매터리얼의 포인터를 얻어온다.
	*/
	inline
	CnMaterial* PointLight::GetMaterial()
	{
		return &m_Material;
	}

	//// Spot Light
	//----------------------------------------------------------------
	/**	@brief	매터리얼 설정
	*/
	inline
	void SpotLight::SetMaterial( const CnMaterial& Material )
	{
		m_Material = Material;
	}

	//----------------------------------------------------------------
	/**	@brief	매터리얼의 포인터를 얻어온다.
	*/
	inline
	CnMaterial* SpotLight::GetMaterial()
	{
		return &m_Material;
	}
} // namespace Deferred
} // namespace Scene
} // namespace Cindy