//================================================================
// File					: CnPostProcessComponent.cpp
// Related Header File	: CnPostProcessComponent.h
// Original Author		: Changhee
//================================================================
#include "CnPostProcessComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnVisibleSet.h"
#include "Scene/CnCamera.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/CnTransformDevice.h"
#include "Graphics/Base/CnRenderWindow.h"
#include "Util/CnString.h"
#include "Util/CnLog.h"
#include "Frame/CnPostEffect.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	PostProcessComponent
	//==================================================================
	__ImplementClass( Cindy, PostProcessComponent, Scene::Component );

	PostProcessComponent* PostProcessComponent::Create(int nWidth, int nHeight, float tu0, float tv0, float tu1, float tv1)
	{
		PostProcessComponent* component = Scene::PostProcessComponent::Create();
		component->Init(nWidth, nHeight, tu0, tv0, tu1, tv1);

		return component;
	}

	//----------------------------------------------------------------
	PostProcessComponent::PostProcessComponent()
	{
		m_SceneID = L"PostProcessComponent";
	}
	PostProcessComponent::~PostProcessComponent()
	{
	}

	//----------------------------------------------------------------
	/**	@brief	초기화 한다.
	*/
	bool PostProcessComponent::Init( int nWidth, int nHeight, float tu0, float tv0, float tu1, float tv1 )
	{
		m_VertexData = new CnVertexData();

		VertDeclPtr vertexDeclaration = m_VertexData->GetDeclaration();
			CnVertexDeclaration::UsageComponent vertexComponent;
			vertexComponent.Add( CnVertexDeclaration::Position );
			vertexComponent.Add( CnVertexDeclaration::Uv0 );
			CnVertexDeclaration::VertexElement vertElement = vertexDeclaration->AddElement( 0, 0, vertexComponent );
		vertexDeclaration->Build();

		Ptr<CnVertexBuffer> vertexBuffer = RenderDevice->CreateVertexBuffer();
		if (!vertexBuffer->Create(4, vertElement.byteSize, CnVertexBuffer::USAGE_WRITEONLY, CnVertexBuffer::POOL_MANAGED))
			return false;

		struct VertexFormat
		{
			Math::Vector3 pos;
			Math::Vector2 uv;
		};
		VertexFormat* buffer = (VertexFormat*)vertexBuffer->Lock( 0, 0, 0 );
		if (buffer)
		{
			/*	postprocess overlay는 이런 순서입니다... 
				3 ----- 2
				|		|
				|		|
				1-------0
			*/
			for(int i=0; i<4; i++)
			{
				//float idx = (float)((i&1) + ((i&2)^2));
				float idx = (float)i;
				buffer[i].pos = Math::Vector3(!(i&1) ? 0.0f : nWidth, !(i&2) ? 0.0f : nHeight, idx);
				buffer[i].uv.x = (!(i&1) ? tu0 : tu1);
				buffer[i].uv.y = (!(i&2) ? tv0 : tv1);
			}
		}
		vertexBuffer->Unlock();

		m_VertexData->AddBuffer( 0, vertexBuffer );

		m_IndexBuffer = RenderDevice->CreateIndexBuffer();
		if(false == m_IndexBuffer->Create( 6, CnIndexBuffer::USAGE_WRITEONLY, CnIndexBuffer::FMT_INDEX16, CnIndexBuffer::POOL_MANAGED ))
		{
			m_IndexBuffer->Release();
			m_IndexBuffer = NULL;
			return false;
		}
		else
		{
			ushort* indices = m_IndexBuffer->Lock( 0, 0, 0 );
			if (indices)
			{
				const unsigned short idx[] = {
					0, 1, 3, 0, 3, 2,
				} ;
				memcpy(indices, idx, sizeof(idx));
			}
			m_IndexBuffer->Unlock();
		}

		// 
		m_Width = nWidth;
		m_Height = nHeight;

		//
		//--------------------------------------------------------------//
		// Set up FullScreenQuad vertex buffers & formats etc.
		//--------------------------------------------------------------//
		m_PrevTick = timeGetTime();
		m_CurrentTime = 0;
		m_MaxTime = 200.0f;
		return true;
	}

	void PostProcessComponent::Init(CMiniXML::const_iterator node, PostEffect::Effect* effect)
	{
		// shader name
		const CnString shaderName = node.GetAttribute(L"shader");
		const CnString shaderTechniqueName = node.GetAttribute(L"shaderTechnique");

		m_Material.SetShader( shaderName.c_str() );
		m_Material.SetCurrentTechnique( shaderTechniqueName );

		/// Param
		CMiniXML::const_iterator xmlParam = node.GetChild();
		xmlParam = xmlParam.Find(_T("Param"));
		if (xmlParam != NULL)
		{
			// <Param name="" type="vector4">0 0 0 0</Param>
			const wchar* sementic = xmlParam.GetAttribute(L"name");
			const wchar* type = xmlParam.GetAttribute(L"type");
			const wchar* value = xmlParam.GetValue();

			if (wcscmp(type, L"float"))
			{
				float v = unicode::ToFloat(value);
				SetParameter(sementic, v);
			}
			else if (wcscmp(type, L"vector3"))
			{
				Math::Vector3 v;
				swscanf_s(value, L"%f %f %f", &v.x, &v.y, &v.z);
				SetParameter(sementic, v);
			}
			else if (wcscmp(type, L"vector4"))
			{
				Math::Vector4 v;
				swscanf_s(value, L"%f %f %f %f", &v.x, &v.y, &v.z, &v.w);
				SetParameter(sementic, v);
			}
			else
			{
				_ASSERT(0);
			}
		}
		
		/// Sample
		bool enableSample = node.FindChild(_T("Sample")) != NULL ? true : false;
		int sourcewidth = 0;
		int sourceheight = 0;

		/// Textures
		CnString texName;
		CnString sementic;

		CMiniXML::const_iterator xmlTex = node.GetChild();
		for (xmlTex=xmlTex.Find(_T("Texture")); xmlTex != NULL; xmlTex=xmlTex.Find(_T("Texture")))
		{
			texName = xmlTex.GetAttribute(L"name");

			// texture의 타입이 설정이 안되어 있다면, RenderTarget의 결과를 이용한다.
			// Type: File 일 경우도 있음
			const wchar* type = xmlTex.GetAttribute(L"type");
			if (type == NULL || (type!=NULL && unicode::Compare(type, L"rendertarget")))
			{
				const Ptr<CnRenderTarget>& rt = effect->FindRenderTarget(texName);
				texName = rt->GetName();

				if (enableSample)
				{
					sourcewidth = rt->GetWidth();
					sourceheight = rt->GetHeight();
				}
			}
			//else if (0 == wcscmp(type, L"File"))
			//{
			//}

			sementic = xmlTex.GetAttribute(L"sementic");
			
			MapType::Define mapType = MapType::StringToDefine( sementic.c_str() );
			if (mapType != MapType::InvalidType)
				m_Material.SetTexture( mapType, texName );
		}

		if (enableSample)
		{
			CMiniXML::const_iterator item = node.FindChild(L"Sample");
			CnString sampleType = item.GetAttribute(L"type");

			if (sampleType == L"DownSample4x4")
				CalcDown4x4SampleOffsets(sourcewidth, sourceheight);
			else if (sampleType == L"DownSample2x2")
				CalcDown2x2SampleOffsets(sourcewidth, sourceheight);
			else if (sampleType == L"GaussBlur5x5")
				CalcGaussBlur5x5SampleOffsets(sourcewidth, sourceheight);
			else if (sampleType == L"BloomSample")
			{
				float fDeviation = 3.0f;
				const wchar* deviation = item.GetAttribute(L"deviation");
				if (deviation)
					fDeviation = (float)_wtof(deviation);

				float fMultiplier = 2.0f;
				const wchar* multiplier = item.GetAttribute(L"multiplier");
				if (multiplier)
					fMultiplier = (float)_wtof(multiplier);

				CalcBloomSampleOffsets( sourcewidth, sourceheight, fDeviation, fMultiplier );
			}
			else if (sampleType == L"ToneMapSample")
				CalcToneMappingSampleOffsets(sourcewidth, sourceheight);
			else
				CnError( ToStr(L"[Step::Parse] Invalid Sample Type : %s", sampleType.c_str()) );
		}

		/// Sample
		CMiniXML::const_iterator item = node.GetChild();
		item = item.Find(_T("TexelSize"));
		if (item != NULL)
		{
			const wchar* texname = item.GetAttribute(L"name");
			SetSourceTexSize(texname);
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void PostProcessComponent::Discard()
	{
	}
	
	//----------------------------------------------------------------
	/**
	*/
	void PostProcessComponent::SetMaterial(const CMiniXML::iterator& node)
	{
		CMiniXML::iterator item = node.GetChild();
		for (;item != NULL; item=item.Next())
		{
			const wchar* name = item.GetName();
			if (unicode::Compare(name, L"ApplyShader"))
			{
				const wchar* shader = item.GetAttribute(L"shader");
				m_Material.SetShader(shader);
			}
			else if (unicode::Compare(name, L"Texture"))
			{
				const wchar* texName = item.GetAttribute(L"name");
				const wchar* sementic = item.GetAttribute(L"sementic");

				MapType::Define bindType = MapType::StringToDefine(sementic);
				if (bindType != MapType::InvalidType)
					m_Material.SetTexture( bindType, texName );
			}
		} // for
	}

	void PostProcessComponent::SetVertexStream()
	{
#ifdef _MULTIPLE_STREAM_
		// SetVertex/Index Buffer
		RenderDevice->SetVertexDeclaration( m_VertexData );
		RenderDevice->SetStreamSource( m_VertexData );
#else
		// SetVertex/Index Buffer
		RenderDevice->SetVertexDeclaration( m_VertexData );
		RenderDevice->SetStreamSource( 0, 0, 0, m_VertexData );
#endif // _MULTIPLE_STREAM_
	}

	void PostProcessComponent::SetIndexStream()
	{
		RenderDevice->SetIndexBuffer( m_IndexBuffer );
	}

	//----------------------------------------------------------------
	/**	@brief	Post-Processing Sample Offset 설정
	*/		
	void PostProcessComponent::SetSampleOffsets( Math::Vector2* paOffsets, int nSize )
	{
		memcpy( m_aSampleOffsets, paOffsets, sizeof(Math::Vector2)*nSize );
	}

	//----------------------------------------------------------------
	/**	@brief	Post-Processing Sample Weight 설정
	*/
	void PostProcessComponent::SetSampleWeights( Math::Vector4* paWeights, int nSize )
	{
		memcpy( m_aSampleWeights, paWeights, sizeof(Math::Vector4)*nSize );
	}

	//--------------------------------------------------------------
	/**
	*/
	void PostProcessComponent::SetParameter(const wchar* param, float var)
	{
		CnVariable::Type type = CnVariable::StringToType(param);
		std::vector<_PARAM>::iterator it = std::find(m_ShaderParams.begin(), m_ShaderParams.end(), (int)type);
		if (m_ShaderParams.end() == it)
		{
			_PARAM p;
			p.sementic = (int)type;
			p.v = var;
			m_ShaderParams.push_back(p);
		}
		else
		{
			(*it).v = var;
		}
	}

	void PostProcessComponent::SetParameter(const wchar* param, const Math::Vector3& var)
	{
		CnVariable::Type type = CnVariable::StringToType(param);
		std::vector<_PARAM>::iterator it = std::find(m_ShaderParams.begin(), m_ShaderParams.end(), (int)type);
		if (m_ShaderParams.end() == it)
		{
			_PARAM p;
			p.sementic = (int)type;
			p.v = var;
			m_ShaderParams.push_back(p);
		}
		else
		{
			(*it).v = var;
		}
	}
	
	void PostProcessComponent::SetParameter(const wchar* param, const Math::Vector4& var)
	{
		CnVariable::Type type = CnVariable::StringToType(param);
		std::vector<_PARAM>::iterator it = std::find(m_ShaderParams.begin(), m_ShaderParams.end(), (int)type);
		if (m_ShaderParams.end() == it)
		{
			_PARAM p;
			p.sementic = (int)type;
			p.v = var;
			m_ShaderParams.push_back(p);
		}
		else
		{
			(*it).v = var;
		}
	}
	
	//--------------------------------------------------------------
	/**
	*/
	void PostProcessComponent::SetSourceTexSize(const wchar* texname)
	{
		m_SourceTexture = texname;
	}

	Math::Vector3 mul(const Math::Vector3& p, const Math::Matrix44& m)
	{
		return Math::Vector3(p.x*m._11 + p.y*m._21 + p.z*m._31 + m._41, p.x*m._12 + p.y*m._22 + p.z*m._32 + m._42, p.x*m._13 + p.y*m._23 + p.z*m._33 + m._43);
	}

	//--------------------------------------------------------------
	/**
	*/
	void PostProcessComponent::ApplyShaderVariables( CnCamera* pCamera )
	{
		ShaderHandle handle = NULL;
		ShaderPtr shader = m_Material.GetShader();

		for(unsigned int i=0; i<m_ShaderParams.size(); i++)
		{
			if (handle = shader->GetParameter( (ShaderState::Param)m_ShaderParams[i].sementic ))
			{
				switch (m_ShaderParams[i].v.GetType())
				{
				case CnVariable::Float:
					shader->SetFloat(handle, m_ShaderParams[i].v.GetFloat());
					break;
				case CnVariable::Vector3:
					shader->SetVector3(handle, m_ShaderParams[i].v.GetVector3());
					break;
				case CnVariable::Vector4:
					shader->SetVector4(handle, m_ShaderParams[i].v.GetVector4());
					break;
				default:
					Assert(0, L"[PostProcessComponent::ApplyShaderVariables] Invaild Param");
					break;
				} ;
			}
		}

		// 화면 해상도
		if (handle = shader->GetParameter( ShaderState::DisplayResolution ))
		{
			/// 해상도
			float w = (float)m_Width;
			float h = (float)m_Height;

			shader->SetVector4(handle, Math::Vector4(w, h, 1.0f/w, 1.0f/h));
		}
		if (handle = shader->GetParameter(ShaderState::ScreenProj))
		{
			float invscreensize[2] = {1.0f/(float)m_Width, 1.0f/(float)m_Height};

			Math::Vector4 screenproj[2];
			screenproj[0] = Math::Vector4(2.0f * invscreensize[0], -2.0f * invscreensize[1], 1, 1);
			screenproj[1] = Math::Vector4(-1.0f - invscreensize[0], 1.0f + invscreensize[1], 0, 0);
			shader->SetVector4Array( handle, screenproj, 2 );
		}

		// ViewInfo
		if (handle = shader->GetParameter( ShaderState::ViewInfo ))
		{
			// tanFOV
			float fov = pCamera->GetFovY();
			float tanFOV = 1.0f / Math::Tan(fov*0.5f);

			Math::Vector4 viewInfo;
			viewInfo.x = pCamera->GetAspect();
			viewInfo.y = tanFOV;
			viewInfo.z = pCamera->GetNear();
			viewInfo.w = pCamera->GetFar();
			shader->SetValue( handle, viewInfo, sizeof(Math::Vector4) );
		}
		if (handle = shader->GetParameter( ShaderState::FrustumCorners ))
		{			
			/*	frustumExtrema					outFrustumPoints
				6 ----------------- 2			3 ----------------- 1
				|Far				|			|Far				|
				|	7 --------- 3	|			|	7 --------- 5	|
				|	|Near		|	|	-->		|	|Near		|	|
				|	|			|	|			|	|			|	|
				|	5 ---------	1	|			|	6 ---------	4	|
				|					|			|					|
				4 ----------------- 0			2 ----------------- 0
			*/
			/*
				frustum points: 0, PLANE_FAR | PLANE_BOTTOM | PLANE_RIGHT
				frustum points: 1, PLANE_NEAR | PLANE_BOTTOM | PLANE_RIGHT
				frustum points: 2, PLANE_FAR | PLANE_TOP | PLANE_RIGHT
				frustum points: 3, PLANE_NEAR | PLANE_TOP | PLANE_RIGHT
				frustum points: 4, PLANE_FAR | PLANE_BOTTOM | PLANE_LEFT
				frustum points: 5, PLANE_NEAR | PLANE_BOTTOM | PLANE_LEFT
				frustum points: 6, PLANE_FAR | PLANE_TOP | PLANE_LEFT
				frustum points: 7, PLANE_NEAR | PLANE_TOP | PLANE_LEFT
			*/
			/*	postprocess overlay는 이런 순서입니다... 
				0 ----- 1
				|		|
				|		|
				2-------3
			*/
			const Math::Matrix44& viewTM = pCamera->GetView();
			const Math::Vector3* frustumpoints = pCamera->GetFrustumPoints();

			//for(int i=0; i<8; i++)
			//{
			//	OutputDebugString(unicode::ToString(L"frustum points: %d, %f %f %f\n", i, frustumpoints[i].x, frustumpoints[i].y, frustumpoints[i].z).c_str());
			//}
			
			/*
				이런 결과가 나오네... 뭔가 view frustum 만드는 과정에서 문제가 있는 듯 하다...
				값이 이상하게 나온다... ㅡㅡ;;

				frustum points: 0, PLANE_FAR | PLANE_BOTTOM | PLANE_RIGHT
				frustum points: 1, PLANE_NEAR | PLANE_BOTTOM | PLANE_RIGHT
				frustum points: 2, PLANE_FAR | PLANE_TOP | PLANE_RIGHT
				frustum points: 3, PLANE_NEAR | PLANE_TOP | PLANE_RIGHT
				frustum points: 4, PLANE_FAR | PLANE_BOTTOM | PLANE_LEFT
				frustum points: 5, PLANE_NEAR | PLANE_BOTTOM | PLANE_LEFT
				frustum points: 6, PLANE_FAR | PLANE_TOP | PLANE_LEFT
				frustum points: 7, PLANE_NEAR | PLANE_TOP | PLANE_LEFT

				frustum points: 0, -950.000244 -414.208893 552.278564
				frustum points: 1, 49.499752 -0.207208 0.276277
				frustum points: 2, -950.000244 414.208893 552.278564
				frustum points: 3, 49.499752 0.207208 0.276277
				frustum points: 4, -950.000244 -414.208893 -552.278564
				frustum points: 5, 49.499752 -0.207208 -0.276277
				frustum points: 6, -950.000244 414.208893 -552.278564
				frustum points: 7, 49.499752 0.207208 -0.276277
			*/

			// far plane cornerpoint 4개를 view 공간으로 변환해서 보내준다.
			static Math::Vector3 p[4];
			p[0] = mul(frustumpoints[7], viewTM);
			p[1] = mul(frustumpoints[3], viewTM);
			p[2] = mul(frustumpoints[5], viewTM);
			p[3] = mul(frustumpoints[1], viewTM);
			
			shader->SetValue(handle, p, sizeof(Math::Vector3)*4);
		}

		//
		if (handle = shader->GetParameter( ShaderState::Projection ))
		{
			//shader->SetMatrix( handle, CnTransformDevice::Instance()->GetProjTransform() );
			shader->SetMatrix( handle, pCamera->GetProjection() );
		}
		if (handle = shader->GetParameter( ShaderState::ViewProjection ))
		{
			shader->SetMatrix( handle, CnTransformDevice::Instance()->GetViewProjTransform() );
		}

		// offset
		if (handle = shader->GetParameter( ShaderState::FilterSampleOffsets ))
		{
			shader->SetValue( handle, m_aSampleOffsets, sizeof(m_aSampleOffsets) );
		}
		if (handle = shader->GetParameter( ShaderState::FilterSampleWeights ))
		{
			shader->SetValue( handle, m_aSampleWeights, sizeof(m_aSampleWeights) );
		}

		/// 
		ulong current = timeGetTime();
		ulong elapse = current - m_PrevTick;
		m_PrevTick = current;

		// GrainFilter
		if (handle = shader->GetParameter( ShaderState::TimeValue ))
		{
			m_PrevTick = current;
			m_CurrentTime += ((float)elapse*0.001f);
			if (m_CurrentTime > m_MaxTime)
			{
				m_CurrentTime = 0.0f;
			}
			shader->SetFloat( handle, m_CurrentTime );
		}
		if (handle = shader->GetParameter( ShaderState::ElapsedTime ))
		{
			shader->SetFloat( handle, ((float)elapse*0.001f) );
		}

		// LightRays
		if (handle = shader->GetParameter( ShaderState::SunLightPosition ))
		{
			const Math::Vector4 sunLightPosition(0.0f, 10.0f, 0.0f, 1.0f);

			shader->SetVector4( handle, sunLightPosition );
		}
		
		if (handle = shader->GetParameter( ShaderState::RenderTargetSize ))
		{
			float w = (float)m_Width;
			float h = (float)m_Height;
			shader->SetVector4(handle, Math::Vector4(w, h, 1.0f/w, 1.0f/h));
		}
		if (handle = shader->GetParameter( ShaderState::SourceSize ))
		{
			MapType::Define type = MapType::StringToDefine(m_SourceTexture.c_str());
			TexPtr texture = m_Material.GetTexture(type);
			if (texture.IsValid())
			{
				float w = (float)texture->GetWidth();
				float h = (float)texture->GetHeight();
				shader->SetVector4(handle, Math::Vector4(w, h, 1.0f/w, 1.0f/h));
			}
		}

		// Texture
		TexPtr texture;
		for(int i=0; i<MapType::NumMapTypes; i++)
		{
			int shaderparam = MapType::ToShaderState( (MapType::Define)i );
			if (handle = shader->GetParameter( (ShaderState::Param)shaderparam ))
			{
				texture = m_Material.GetTexture( (MapType::Define)i );
				shader->SetTexture(handle, texture);
			}
		}
	}

	//--------------------------------------------------------------
	/**
	*/
	void PostProcessComponent::OnFrame( CnCamera* pCamera )
	{
		ShaderPtr shader = m_Material.GetShader();
		if (shader.IsNull())
			return;

		RenderDevice->SetActiveShader( shader->GetEffect() );

		SetVertexStream();
		SetIndexStream();

		//RenderDevice->SetRenderState( *m_Material.GetRenderState() );
		//RenderDevice->SetTextureState( *m_Material.GetTextureState() );

		LPDIRECT3DDEVICE9 d3d9Device = 0;
		RenderDevice->GetCurrentDevice(&d3d9Device);

		/// zenble 
		d3d9Device->SetRenderState(D3DRS_ZENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

		/// enable writing
		d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);

		/// enable alpha
		d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		///
		d3d9Device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_FOGENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

		// technique
		CnTechnique* technique = m_Material.GetCurrentTechnique();
		if (technique)
		{
			int numPasses = technique->Begin();
			{
				CnPass* pass = NULL;

				ApplyShaderVariables( pCamera );

				for (int i=0; i<numPasses; i++)
				{
					pass = technique->GetPass(i);
					if (NULL == pass)
						continue;

					pass->Begin();
					{
						RenderDevice->DrawIndexedPrimitive( IRenderer::TRIANGLELIST, 4, 2 );
					}
					pass->End();
				}
			}
			technique->End();
		}
	}

	//----------------------------------------------------------------
	void PostProcessComponent::CalcDown2x2SampleOffsets(int width, int height)
	{
		float tu = 1.0f / width;
		float tv = 1.0f / height;

		int index=0;
		for (int y=0; y<2; ++y)
		{
			for (int x=0; x<2; ++x)
			{
				m_aSampleOffsets[index].x = (x-0.5f) * tu;
				m_aSampleOffsets[index].y = (y-0.5f) * tv;
				index++;
			}
		}
	}

	//----------------------------------------------------------------
	void PostProcessComponent::CalcDown4x4SampleOffsets(int width, int height)
	{
		float tu = 1.0f / width;
		float tv = 1.0f / height;

		int index=0;
		for (int y=0; y<4; ++y)
		{
			for (int x=0; x<4; ++x)
			{
				m_aSampleOffsets[index].x = (x-1.5f) * tu;
				m_aSampleOffsets[index].y = (y-1.5f) * tv;
				index++;
			}
		}
	}

	//----------------------------------------------------------------
	void PostProcessComponent::CalcBloomSampleOffsets( int width, int height, float fDeviation, float fMultiplier )
	{
		float tu = 1.0f / width;
		float tv = 1.0f / height;

		// 가운데 텍셀을 채운다.
		float weight = fMultiplier * Math::GaussianDistribution( 0, 0, fDeviation );
		m_aSampleWeights[0] = Math::Vector4( weight, weight, weight, 1.0f );
		m_aSampleOffsets[0].y = 0.0f;

		int i = 0;
		for (i=1; i<8; ++i)
		{
			// 이 Offset에 대한 Gaussian intensity를 얻는다.
			weight = fMultiplier * Math::GaussianDistribution( (float)i, 0, fDeviation );
			m_aSampleOffsets[i].y = i * tu;

			m_aSampleWeights[i] = Math::Vector4( weight, weight, weight, 1.0f );
		}

		// Mirror to the second half
		for (i=8; i<15; ++i)
		{
			m_aSampleWeights[i] = m_aSampleWeights[i-7];
			m_aSampleOffsets[i].y = -m_aSampleOffsets[i-7].y;
		}
	}

	//----------------------------------------------------------------
	void PostProcessComponent::CalcGaussBlur5x5SampleOffsets(int width, int height)
	{
		float tu = 1.0f / width;
		float tv = 1.0f / height;

		Math::Vector4 white( 1.0f, 1.0f, 1.0f, 1.0f );
		float totalWeight = 0.0f;
		int index = 0;
		for (int x=-2; x<=2; ++x)
		{
			for (int y=-2; y<=2; ++y)
			{
				// 계수가 작아질 부분은 소거
				if (abs(x) + abs(y) > 2)
					continue;

				m_aSampleOffsets[index].x = x * tu;
				m_aSampleOffsets[index].y = y * tv;

				m_aSampleWeights[index] = white * Math::GaussianDistribution( (float)x, (float)y, 1.0f );

				totalWeight += m_aSampleWeights[index].x;
				index++;
			}
		}

		for (int i=0; i<index; ++i)
			m_aSampleWeights[i] *= 1.0f/totalWeight;
	}

	//----------------------------------------------------------------
	void PostProcessComponent::CalcToneMappingSampleOffsets(int width, int height)
	{
		float tu = 1.0f / 3.0f * (float)width;
		float tv = 1.0f / 3.0f * (float)height;

		int index = 0;
		for (int y=-1; y<=1; ++y)
		{
			for (int x=-1; x<=1; ++x)
			{
				m_aSampleOffsets[index].x = x * tu;
				m_aSampleOffsets[index].y = y * tv;
				index++;
			}
		}
	}

} // namespace Scene
} // namespace Cindy