//================================================================
// File					: CnRenderSkyBoxComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"

#include "Geometry/CnVertexData.h"
#include "Graphics/Base/CnIndexBuffer.h"

#include "Material/CnMaterial.h"
#include "Math/CindyMath.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** SkyBox Component
		@author			cagetu
		@since			2009년 11월 01일
		@brief			SkyBox 랜더링
	*/
	//==================================================================
	class CN_DLL RenderSkyBox : public Scene::Component
	{
		__DeclareClass(RenderSkyBox);
	public:
		RenderSkyBox();
		virtual ~RenderSkyBox();

		// Open/Close
		void	Open() override;
		void	Close() override;

		// Material
		void	SetMaterial( const Ptr<CnMaterial>& Material );
		const Ptr<CnMaterial>&	GetMaterial() const;

		// Run
		void	OnFrame( CnCamera* pCamera ) override;

	private:
		struct VertexFormat
		{
			Math::Vector4 pos;
		};

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		Ptr<CnMaterial>		m_Material;
		Ptr<CnVertexData>	m_VertexData;

		float				m_Size;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		void ApplyShaderVariables( CnCamera* pCamera );
	};

	//----------------------------------------------------------------
	/**	@brief	매터리얼 설정
	*/
	inline
	void RenderSkyBox::SetMaterial( const Ptr<CnMaterial>& Material )
	{
		m_Material = Material;
	}

	//----------------------------------------------------------------
	/**	@brief	매터리얼의 포인터를 얻어온다.
	*/
	inline
	const Ptr<CnMaterial>& RenderSkyBox::GetMaterial() const
	{
		return m_Material;
	}

} // namespace Scene
} // namespace Cindy
