//================================================================
// File					: CnUpdateSceneComponent.cpp
// Related Header File	: CnUpdateSceneComponent.h
// Original Author		: Changhee
//================================================================
#include "CnUpdateSceneComponent.h"
#include "Scene/CnVisibleResolver.h"
#include "Scene/CnSceneEntity.h"
#include "Graphics/CnRenderer.h"
#include "Frame/CnFrameManager.h"
#include "Util/CnLog.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	Default Scene
	//==================================================================
	__ImplementClass( Cindy, UpdateScene, Scene::Component );
	//--------------------------------------------------------------
	UpdateScene::UpdateScene()
		: m_bNeedUpdate(false)
	{
		m_pVisibleSet = CnVisibleSet::Create();

		CnFrameManager::Instance()->AddListener( this );

		m_SceneID = L"SceneUpdator";
	}
	UpdateScene::~UpdateScene()
	{
		m_SceneGraphes.clear();

		m_pVisibleSet = 0;

		CnFrameManager::Instance()->RemoveListener( this );
	}

	//----------------------------------------------------------------
	/** @brief	프레임 시작시점에서 처리할 내용 */
	//----------------------------------------------------------------
	void UpdateScene::BeginFrame()
	{
		m_bNeedUpdate = true;
	}
	//----------------------------------------------------------------
	/** @brief	프레임 끝 시점에서 처리할 내용 */
	//----------------------------------------------------------------
	void UpdateScene::EndFrame()
	{
		m_pCamera = NULL;
	}

	//----------------------------------------------------------------
	/** @brief	등록 */
	//----------------------------------------------------------------
	void UpdateScene::AddSceneGraph( const SceneGraphPtr& SceneGraph )
	{
		m_SceneGraphes.push_back( SceneGraph );
	}

	//----------------------------------------------------------------
	/** @brief	해제 */
	//----------------------------------------------------------------
	void UpdateScene::RemoveSceneGraph( const SceneGraphPtr& SceneGraph )
	{
		SceneGraphIter iter = std::find( m_SceneGraphes.begin(), m_SceneGraphes.end(), SceneGraph );
		if (iter == m_SceneGraphes.end())
			return;

		m_SceneGraphes.erase( iter );
	}

	//----------------------------------------------------------------
	/** @brief	SceneGraph 업데이트 */
	//----------------------------------------------------------------
	void UpdateScene::OnUpdate()
	{
		if (false == m_bNeedUpdate)
			return;

		SceneGraphIter i, iend;
		iend = m_SceneGraphes.end();
		for (i=m_SceneGraphes.begin(); i!=iend; ++i)
		{
			(*i)->UpdateTransform();
		}

		m_bNeedUpdate = false;
	}

	//----------------------------------------------------------------
	/** @brief	보이는 객체들 찾기 */
	//----------------------------------------------------------------
	void UpdateScene::FindVisible( CnCamera* pCamera )
	{
		m_pVisibleSet->Clear();

		SceneGraphIter i, iend;
		iend = m_SceneGraphes.end();
		for (i=m_SceneGraphes.begin(); i!=iend; ++i)
		{
			(*i)->FindVisible( pCamera, m_pVisibleSet );
		}
	}

	//----------------------------------------------------------------
	/** @brief	보이는 객체들의 관계를 만든다. */
	//----------------------------------------------------------------
	void UpdateScene::UpdateRelation( CnCamera* pCamera )
	{
		if (m_pVisibleSet->IsEmpty())
			return;

		const CnVisibleSet::EntityArray* lights = m_pVisibleSet->GetEntities( CnVisibleSet::Light );
		const CnVisibleSet::EntityArray* models = m_pVisibleSet->GetEntities( CnVisibleSet::Model );

		if (lights && models)
		{
			CnVisibleSet::EntityConstIter li, liend;
			liend = lights->end();
			for (li = lights->begin(); li != liend; ++li)
			{
				CnVisibleSet::EntityConstIter mi, miend;
				miend = models->end();
				for (mi = models->begin(); mi != miend; ++mi)
				{
					(*mi)->RelationTo( (*li), pCamera );
					(*li)->RelationTo( (*mi), pCamera );
				} // for
			} // for
		} // if
	}

	//----------------------------------------------------------------
	/**
	*/
	void UpdateScene::OnBeginFrame( CnCamera* pCamera )
	{
		if (m_pCamera == pCamera)
			return;

		FindVisible( pCamera );
		UpdateRelation( pCamera );

		m_pCamera = pCamera;
	}

	//----------------------------------------------------------------
	/** @brief	그리기
				[순서]
				1. 화면에 보이는 객체들을 모은다.
				2. 보이는 객체들간의 관계를 형성한다.
				3. 각 관계별로 처리를 한다.
				4. 화면에 그린다.
	*/
	//----------------------------------------------------------------
	void UpdateScene::OnFrame( CnCamera* pCamera )
	{
		VisibleResolver->SetVisibleSet( m_pVisibleSet );
		//if (m_pCamera == pCamera)
		//	return;

		//FindVisible( pCamera );
		//UpdateRelation( pCamera );

		//m_pCamera = pCamera;

		//CnPrint( L":::: Run Update Component ::::\n" );
	}

	//----------------------------------------------------------------
	/**
	*/
	void UpdateScene::OnEndFrame( CnCamera* pCamera )
	{
	}

} // namespace Scene
} // namespace Cindy