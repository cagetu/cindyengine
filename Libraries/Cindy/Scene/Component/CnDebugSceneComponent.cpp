//================================================================
// File					: CnDebugSceneComponent.cpp
// Related Header File	: CnDebugSceneComponent.h
// Original Author		: Changhee
//================================================================
#include "CnDebugSceneComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnCamera.h"
#include "Scene/CnVisibleSet.h"
#include "Lighting/CnLight.h"
#include "Geometry/CnMesh.h"
#include "Graphics/CnRenderer.h"
#include "Animation/CnSkeletonInstance.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	RenderLight
	//==================================================================
	__ImplementClass( Cindy, DebugLight, Scene::Component );

	//----------------------------------------------------------------
	DebugLight::DebugLight()
	{
		m_SceneID = L"DebugLight";
	}
	DebugLight::~DebugLight()
	{
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	void DebugLight::Open()
	{
		m_VertexData = new CnVertexData();

		CnVertexDeclaration::VertexElement vertElement;
		VertDeclPtr vertexDeclaration = m_VertexData->GetDeclaration();
		{
			CnVertexDeclaration::UsageComponent vertexComponent;
			vertexComponent.Add( CnVertexDeclaration::Position );
			vertexComponent.Add( CnVertexDeclaration::Normal );
			vertElement = vertexDeclaration->AddElement( 0, 0, vertexComponent );
		}
		vertexDeclaration->Build();

		///
		LPDIRECT3DDEVICE9 d3dDevice;
		RenderDevice->GetCurrentDevice(&d3dDevice);

		HRESULT hr = D3DXCreateSphere(d3dDevice, 1.0f, 16, 8, &m_d3dSphere, NULL);
		assert(SUCCEEDED(hr));
	}

	//----------------------------------------------------------------
	/** */
	//----------------------------------------------------------------
	void DebugLight::Close()
	{
		m_VertexData = 0;
		SAFEREL( m_d3dSphere );
	}

	//----------------------------------------------------------------
	/** @brief	라이트 업데이트 */
	//----------------------------------------------------------------
	void DebugLight::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* lights = visibleSet->GetEntities( CnVisibleSet::Light );
		if (NULL == lights)
			return;

		CnVisibleSet::EntityConstIter i, iend;
		iend = lights->end();
		for (i=lights->begin(); i!=iend; ++i)
		{
			CnLight* light = (CnLight*)(*i);
			light->Debug();
		}


		//LPDIRECT3DDEVICE9 d3d9Device;
		//RenderDevice->GetCurrentDevice( &d3d9Device );

		//d3d9Device->SetVertexShader( 0 );
		//d3d9Device->SetFVF( D3DFVF_XYZ );
		//d3d9Device->SetRenderState( D3DRS_LIGHTING, FALSE );
		//d3d9Device->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

		///// zenble 
		//d3d9Device->SetRenderState(D3DRS_ZENABLE, TRUE);
		//d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

		///// enable writing
		//d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);
		//d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

		///// enable alpha
		//d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		//d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		//d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		//Math::Matrix44 mat;

		//d3d9Device->SetTransform( D3DTS_PROJECTION, &pCamera->GetProjection() );
		//d3d9Device->SetTransform( D3DTS_VIEW, &pCamera->GetView() );

		//d3d9Device->SetTexture(0, NULL);
		//d3d9Device->SetTexture(1, NULL);

		//for (i=lights->begin(); i!=iend; ++i)
		//{
		//	CnLight* light = (CnLight*)(*i);

		//	Math::Vector3 lightPosition = light->GetWorldPosition();

		//	mat.Identity();
		//	mat.SetScale( Math::Vector3(5.0f, 5.0f, 5.0f) );
		//	mat.SetPosition( lightPosition );

		//	d3d9Device->SetTransform( D3DTS_WORLD, &mat );
		//	HRESULT hr = m_d3dSphere->DrawSubset(0);
		//}
	}

	//==================================================================
	//	RenderAABB
	//==================================================================
	__ImplementClass( Cindy, RenderAABB, Scene::Component );

	//----------------------------------------------------------------
	RenderAABB::RenderAABB()
	{
		m_SceneID = L"RenderAABB";
	}
	RenderAABB::~RenderAABB()
	{
	}

	//--------------------------------------------------------------
	void RenderAABB::OnFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		const CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (NULL == models)
			return;

		CnMesh* mesh = NULL;

		LPDIRECT3DDEVICE9 d3d9Device;
		RenderDevice->GetCurrentDevice( &d3d9Device );

		d3d9Device->SetVertexShader( 0 );
		d3d9Device->SetFVF( D3DFVF_XYZ );
		d3d9Device->SetRenderState( D3DRS_LIGHTING, FALSE );
		d3d9Device->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );

		/// zenble 
		d3d9Device->SetRenderState(D3DRS_ZENABLE, TRUE);
		d3d9Device->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

		/// enable writing
		d3d9Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);
		d3d9Device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

		/// enable alpha
		d3d9Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		d3d9Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		Math::Matrix44 mat;
		mat.Identity();

		d3d9Device->SetTransform( D3DTS_WORLD, &mat );
		d3d9Device->SetTransform( D3DTS_PROJECTION, &pCamera->GetProjection() );
		d3d9Device->SetTransform( D3DTS_VIEW, &pCamera->GetView() );

		d3d9Device->SetTexture(0, NULL);
		d3d9Device->SetTexture(1, NULL);

		D3DXVECTOR3 l[10], l2[2];

		Math::AABB aabb;
		Math::Vector3 vMin;
		Math::Vector3 vMax;
		Math::Vector3 v[3*12];

		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			mesh = static_cast<CnMesh*>(*i);

			mesh->GetAABB( aabb );

			vMin = aabb.GetMin();
			vMax = aabb.GetMax();

			l[0] = D3DXVECTOR3( vMin.x, vMax.y, vMin.z );
			l[1] = D3DXVECTOR3( vMin.x, vMax.y, vMax.z );
			l[2] = D3DXVECTOR3( vMax.x, vMax.y, vMax.z );
			l[3] = D3DXVECTOR3( vMax.x, vMax.y, vMin.z );
			l[4] = D3DXVECTOR3( vMin.x, vMax.y, vMin.z );

			l[5] = D3DXVECTOR3( vMin.x, vMin.y, vMin.z );
			l[6] = D3DXVECTOR3( vMin.x, vMin.y, vMax.z );
			l[7] = D3DXVECTOR3( vMax.x, vMin.y, vMax.z );
			l[8] = D3DXVECTOR3( vMax.x, vMin.y, vMin.z );
			l[9] = D3DXVECTOR3( vMin.x, vMin.y, vMin.z );

			d3d9Device->DrawPrimitiveUP( D3DPT_LINESTRIP, 4, &l[0], sizeof( D3DXVECTOR3 ) );
			d3d9Device->DrawPrimitiveUP( D3DPT_LINESTRIP, 4, &l[5], sizeof( D3DXVECTOR3 ) );

			l2[0] = l[0];
			l2[1] = l[5];
			d3d9Device->DrawPrimitiveUP( D3DPT_LINELIST, 1, l2, sizeof( D3DXVECTOR3 ) );

			l2[0] = l[1];
			l2[1] = l[6];
			d3d9Device->DrawPrimitiveUP( D3DPT_LINELIST, 1, l2, sizeof( D3DXVECTOR3 ) );

			l2[0] = l[2];
			l2[1] = l[7];
			d3d9Device->DrawPrimitiveUP( D3DPT_LINELIST, 1, l2, sizeof( D3DXVECTOR3 ) );

			l2[0] = l[3];
			l2[1] = l[8];
			d3d9Device->DrawPrimitiveUP( D3DPT_LINELIST, 1, l2, sizeof( D3DXVECTOR3 ) );
		}
	}

	//==================================================================
	//	RenderSkeleton
	//==================================================================
	__ImplementClass( Cindy, RenderSkeleton, Scene::Component );

	//----------------------------------------------------------------
	RenderSkeleton::RenderSkeleton()
	{
		m_SceneID = L"RenderSkeleton";
	}
	RenderSkeleton::~RenderSkeleton()
	{
		m_Skeletons.clear();
	}

	//--------------------------------------------------------------
	void RenderSkeleton::AddSkeleton( CnSkeletonInstance* Skeleton )
	{
		m_Skeletons.push_back( Skeleton );
	}

	//--------------------------------------------------------------
	void RenderSkeleton::OnFrame( CnCamera* pCamera )
	{
		LPDIRECT3DDEVICE9 device;
		RenderDevice->GetCurrentDevice( &device );

		device->SetVertexShader( NULL );
		device->SetTexture( 0, NULL ); 
		device->SetTexture( 1, NULL );
		device->SetRenderState( D3DRS_LIGHTING, FALSE );
		device->SetRenderState( D3DRS_FOGENABLE, FALSE );
		device->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

		float pointsize = 5.0f;
		device->SetRenderState( D3DRS_POINTSIZE, *((DWORD*)&pointsize) );
		device->SetTransform( D3DTS_PROJECTION, &pCamera->GetProjection() );
		device->SetTransform( D3DTS_VIEW, &pCamera->GetView() );
		{
			// Skeleton
			SkeletonArray::iterator i = m_Skeletons.begin();
			SkeletonArray::iterator iend = m_Skeletons.end();
			for (; i!=iend; ++i)
			{
				(*i)->DrawForDebug();
			}
		}

		device->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
		device->SetRenderState( D3DRS_FOGENABLE, TRUE );
		device->SetRenderState( D3DRS_LIGHTING, TRUE );
		device->SetFVF( NULL );

		pointsize = 1.0f;
		device->SetRenderState( D3DRS_POINTSIZE, *((DWORD*)&pointsize) );
	}

} // namespace Scene
} // namespace Cindy