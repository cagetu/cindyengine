//================================================================
// File					: CnCommonSceneComponent.h			
// Original Author		: Changhee
//================================================================
#pragma once

#include "CnBaseSceneComponent.h"
#include "Graphics/CnRenderer.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	/** BeginScene
		@author		cagetu
		@brief		장면 시작 Component
	*/
	//==================================================================
	class CN_DLL BeginScene : public Scene::Component
	{
		__DeclareClass(BeginScene);
	public:
		BeginScene();

		void	SetClearFlag( ulong ClearFlag );

		virtual void	OnFrame( CnCamera* pCamera ) override;

	private:
		ulong	m_ClearFlag;
	};

	//==================================================================
	/** End Scene
		@author		cagetu
		@brief		장면 끝 Component
	*/
	//==================================================================
	class CN_DLL EndScene : public Scene::Component
	{
		__DeclareClass(EndScene);
	public:
		EndScene();

		virtual void	OnFrame( CnCamera* pCamera ) override;
	};

} // namespace Scene
} // namespace Cindy