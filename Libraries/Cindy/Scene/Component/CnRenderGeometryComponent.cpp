//================================================================
// File					: CnRenderGeometryComponent.cpp
// Related Header File	: CnRenderGeometryComponent.h
// Original Author		: Changhee
//================================================================
#include "CnRenderGeometryComponent.h"
#include "CnUpdateSceneComponent.h"
#include "Scene/CnCamera.h"
#include "Scene/CnVisibleSet.h"
#include "Scene/CnVisibleResolver.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/CnTransformDevice.h"
#include "Util/CnLog.h"

namespace Cindy
{
namespace Scene
{
#define TEST_CHUNK

	//==================================================================
	//	RenderGeometry
	//==================================================================
	__ImplementClass(Cindy, RenderGeometry, Scene::Component);

	//----------------------------------------------------------------
	RenderGeometry::RenderGeometry()
	{
		m_SceneID = L"RenderGeometry";
	}
	RenderGeometry::~RenderGeometry()
	{
	}

	//----------------------------------------------------------------
	/** @brief	초기화 */
	//----------------------------------------------------------------
	void RenderGeometry::Open()
	{
		//Ptr<CnGeometryChunk> chunk;

		//chunk = CnGeometryChunk::Create();
		//chunk->SetSortingMode( Sorting::Greater );
		//chunk->SetRenderState( L"b_solid.fx" );
		//chunk->SetShaderFeature( ShaderFeature::Depth );
		//m_RenderQueue.AddChunk( GeometryChunk::Solid, chunk );

		//chunk = CnGeometryChunk::Create();
		//chunk->SetSortingMode( Sorting::Greater );
		//chunk->SetRenderState( L"b_alphatest.fx" );
		//chunk->SetShaderFeature( ShaderFeature::Depth );
		//m_RenderQueue.AddChunk( GeometryChunk::AlphaTest, chunk );
	}

	//----------------------------------------------------------------
	/** @brief	초기화 */
	//----------------------------------------------------------------
	void RenderGeometry::AddChunk( const wchar* type, const wchar* feature, const wchar* shader, const wchar* sort )
	{
		Ptr<CnGeometryChunk> chunk = CnGeometryChunk::Create();

		chunk->SetSortingMode( Sorting::StringToFunc( sort ) );
		chunk->SetRenderState( shader );
		chunk->SetShaderFeature( ShaderFeature::StringToFeature( feature ) );

		m_RenderQueue.AddChunk( GeometryChunk::StringToType( type ), chunk );
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void RenderGeometry::OnBeginFrame( CnCamera* pCamera )
	{
		Ptr<UpdateScene> sceneComp = GetLink(L"SceneUpdator").Cast<UpdateScene>();
		if (sceneComp.IsNull())
			return;

		CnVisibleSet* visibleSet = sceneComp->GetVisibleSet();
		CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		if (NULL == models || models->empty())
			return;

		//Ptr<CnVisibleSet> visibleSet = CnVisibleResolver::Instance()->GetVisibleSet();
		//CnVisibleSet::EntityArray* models = visibleSet->GetEntities( CnVisibleSet::Model );
		//if (NULL == models || models->empty())
		//	return;

		CnMesh* mesh = 0;

		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			mesh = (CnMesh*)(*i);

			m_RenderQueue.Insert( mesh );
		}
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void RenderGeometry::OnFrame( CnCamera* pCamera )
	{
		TransformDevice->SetViewTransform( pCamera->GetView() );
		TransformDevice->SetProjTransform( pCamera->GetProjection() );

		m_RenderQueue.Draw( pCamera );

		//CnPrint( L":::: Run Depth Component ::::\n" );
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void RenderGeometry::OnEndFrame( CnCamera* pCamera )
	{
		m_RenderQueue.Clear();
	}

} // namespace Scene
} // namespace Cindy