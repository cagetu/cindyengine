//================================================================
// File					: CnRenderTextComponent.cpp
// Related Header File	: CnRenderTextComponent.h
// Original Author		: Changhee
//================================================================
#include "CnRenderTextComponent.h"
#include "Graphics/CnTextRenderer.h"

namespace Cindy
{
namespace Scene
{
	//==================================================================
	//	RenderText
	//==================================================================
	__ImplementClass( Cindy, RenderText, Scene::Component );

	//------------------------------------------------------------------
	/**
	*/
	RenderText::RenderText()
	{
		m_SceneID = L"RenderText";
	}
	RenderText::~RenderText()
	{
	}

	//------------------------------------------------------------------
	/**
	*/
	void RenderText::OnFrame( CnCamera* pCamera )
	{
		TextRenderer->DrawText( pCamera );
	}

} // namespace Scene
} // namespace Cindy