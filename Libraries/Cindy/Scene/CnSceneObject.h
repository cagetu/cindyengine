//================================================================
// File:           : CnSceneObject.h
// Original Author : changhee
// Creation Date   : 2007. 1. 27
//================================================================
#ifndef __CN_SCENEOBJECT_H__
#define __CN_SCENEOBJECT_H__

#include "CnVisibleSet.h"
#include "../Material/CnGlobalState.h"

namespace Cindy
{
	class CnCamera;

	//================================================================
	/** Scene Object
	    @author    changhee
		@since     2007. 1. 27
		@remarks   장면을 그릴 수 있는 객체들을 가질 수 있는 특성이 있다.

					- WildMagic에서 RenderState 계층을 훔치다.
	*/
	//================================================================
	class CN_DLL ISceneObject
	{
	public:
		virtual ~ISceneObject();

		// GlobalState
		void			AttachGlobalState( CnGlobalState* pState );
		void			DetachGlobalState( CnGlobalState::TYPE eType );
		void			DetachAllGlobalStates();
		CnGlobalState*	GetGlobalState( CnGlobalState::TYPE eType ) const;
		CnGlobalState*	GetGlobalState( uint Index ) const;
		unsigned int	GetNumOfGlobalStates() const;

		// Update
		virtual void	UpdateScene( CnCamera* pCamera, CnVisibleSet* pVisibleSet ) abstract;
		virtual void	UpdateRS( std::vector<CnGlobalState*>* paGSStack = 0 );
	protected:
		//----------------------------------------------------------------
		//	Variable
		//----------------------------------------------------------------
		std::vector<GlobalStatePtr>	m_GlobalStates;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		// Render State
		//void			PropagateStateFromRoot( std::vector<CnGlboalState*>* paGSStack );
		void			PushState( std::vector<CnGlobalState*>* paGSStack );
		void			PopState( std::vector<CnGlobalState*>* paGSStack );
		virtual void	UpdateState( std::vector<CnGlobalState*>* paGSStack ) abstract;
	};
}

#include "CnSceneObject.inl"

#endif	// __CN_SCENEOBJECT_H__