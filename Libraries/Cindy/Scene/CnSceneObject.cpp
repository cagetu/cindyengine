//================================================================
// File:               : CnSceneObject.cpp
// Related Header File : CnSceneObject.h
// Original Author     : changhee
// Creation Date       : 2008. 12. 1
//================================================================
#include "../Cindy.h"
#include "CnSceneObject.h"

namespace Cindy
{
	ISceneObject::~ISceneObject()
	{
		DetachAllGlobalStates();
	}
	
	//================================================================
	/** Attach Global State
	    @remarks      상태 추가
	*/
	//================================================================
	void ISceneObject::AttachGlobalState( CnGlobalState* pState )
	{
		uint size = (uint)m_GlobalStates.size();
		for (uint i=0; i<size; ++i)
		{
			if (m_GlobalStates[i]->GetStateType() == pState->GetStateType())
			{
				m_GlobalStates[i] = pState;
				return;
			}
		}
		m_GlobalStates.push_back( pState );
	}
	//----------------------------------------------------------------
	void ISceneObject::DetachGlobalState( CnGlobalState::TYPE eType )
	{
		std::vector<GlobalStatePtr>::iterator i, iend;
		iend = m_GlobalStates.end();
		for (i=m_GlobalStates.begin(); i!=iend; ++i)
		{
			if ((*i)->GetStateType() == eType)
			{
				m_GlobalStates.erase( i );
				return;
			}
		}
	}

	//================================================================
	/** @brief	RenderState 업데이트 */
	//================================================================
	void ISceneObject::UpdateRS( std::vector<CnGlobalState*>* paGSStack )
	{
		bool isInit = (paGSStack == 0);
		if (isInit)
		{
			paGSStack = new std::vector<CnGlobalState*>[CnGlobalState::MAX_STATE];
			for (int i=0; i<CnGlobalState::MAX_STATE; ++i)
			{
				paGSStack[i].push_back(0);
			}

			PushState( paGSStack );
		}
		else
		{
			PushState( paGSStack );
		}

		UpdateState( paGSStack );

		if (isInit)
		{
			delete[] paGSStack;
		}
		else
		{
			PopState( paGSStack );
		}
	}
	//----------------------------------------------------------------------------
	//void ISceneObject::PropagateStateFromRoot( std::vector<CnGlobalState*>* paGSStack )
	//{
	//}
	//----------------------------------------------------------------------------
	void ISceneObject::PushState( std::vector<CnGlobalState*>* paGSStack )
	{
		std::vector<GlobalStatePtr>::iterator i, iend;
		iend = m_GlobalStates.end();
		for (i=m_GlobalStates.begin(); i!=iend; ++i)
		{
			int eType = (*i)->GetStateType();
			paGSStack[eType].push_back( (*i) );
		}
	}
	//----------------------------------------------------------------------------
	void ISceneObject::PopState( std::vector<CnGlobalState*>* paGSStack )
	{
		std::vector<GlobalStatePtr>::iterator i, iend;
		iend = m_GlobalStates.end();
		for (i=m_GlobalStates.begin(); i!=iend; ++i)
		{
			int eType = (*i)->GetStateType();
			paGSStack[eType].pop_back();
		}
	}
}