//================================================================
// File:               : CnCameraImpl.cpp
// Related Header File : CnCameraImpl.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 25
//================================================================
#include "../Cindy.h"
#include "CnCameraImpl.h"

namespace Cindy
{
	//==================================================================
	//	Class FirstPersonCamera
	//================================================================
	__ImplementClass( Cindy, CnFirstPersonCamera, CnCamera );
	//--------------------------------------------------------------
	CnFirstPersonCamera::CnFirstPersonCamera()
	{
	}
	CnFirstPersonCamera::CnFirstPersonCamera( const CnString& strName )
		: CnCamera( strName )
	{
	}
	CnFirstPersonCamera::~CnFirstPersonCamera()
	{
	}

	//==================================================================
	//	Class ModelViewCamera
	//================================================================
	__ImplementClass( Cindy, CnModelViewCamera, CnCamera );
	//--------------------------------------------------------------
	CnModelViewCamera::CnModelViewCamera()
		: m_fRadius(10.0f)
		, m_fMinRadius(1.0f)
		, m_fMaxRadius(100.0f)
		, m_vOffset(0.0f, 0.0f, 0.0f)
		, m_fRestrictAngle(0.822f)
	{
		m_Angles[Axis::X] = m_Angles[Axis::Y] = m_Angles[Axis::Z] = 0.0f;
	}
	CnModelViewCamera::CnModelViewCamera( const CnString& strName )
		: CnCamera( strName )
		, m_fRadius(10.0f)
		, m_fMinRadius(1.0f)
		, m_fMaxRadius(100.0f)
		, m_vOffset(0.0f, 0.0f, 0.0f)
		, m_fRestrictAngle(0.822f)
	{
		m_Angles[Axis::X] = m_Angles[Axis::Y] = m_Angles[Axis::Z] = 0.0f;
	}
	CnModelViewCamera::~CnModelViewCamera()
	{
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::UpdateView() const
	{
		if( m_bNeedUpdateView )
		{
			float dy = Math::Sin( m_Angles[Axis::X] ) * m_fRadius;
			float dz = Math::Cos( m_Angles[Axis::X] ) * m_fRadius;

			m_vPosition.x = m_vLookAt.x + ( dz * Math::Cos( m_Angles[Axis::Y] ) );
			m_vPosition.y = m_vLookAt.y + dy;
			m_vPosition.z = m_vLookAt.z + ( dz * Math::Sin( m_Angles[Axis::Y] ) );

			_UpdateEye();
			m_mView.LookAtLH( m_vEye, m_vLookAt, m_vUp );

			m_bNeedUpdateView = false;
			m_bNeedUpdateFrustum = true;
		}
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::SetPosition( const Math::Vector3& vPosition )
	{
		CnCamera::SetPosition( vPosition );
		//m_vPosition = vPosition;
		//_UpdateEye();

		//m_mView.LookAtLH( m_vEye, m_vLookAt, m_vUp );
	
		//m_bNeedUpdateView = false;
		//m_bNeedUpdateFrustum = true;
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::SetLookAt( const Math::Vector3& vLookAt )
	{
		CnCamera::SetLookAt( vLookAt );
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::SetAngle( Radian fAngle, Axis::Enum eAxis )
	{
		m_Angles[eAxis] = fAngle;

		m_bNeedUpdateView = true;
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::Rotate( Radian fAngle, Axis::Enum eAxis )
	{
		switch (eAxis)
		{
		case Axis::X:
			{
				m_Angles[Axis::X] += fAngle;

				if (m_Angles[Axis::X] < m_fRestrictAngle - Math::HALF_PI)
				{
					m_Angles[Axis::X] = m_fRestrictAngle - Math::HALF_PI;
				}
				else if (m_Angles[Axis::X] > Math::HALF_PI - m_fRestrictAngle)
				{
					m_Angles[Axis::X] = Math::HALF_PI - m_fRestrictAngle;
				}
			}
			break;
		case Axis::Y:
			{
				m_Angles[Axis::Y] -= fAngle;
			}
			break;
		case Axis::Z:
			break;
		}

		m_bNeedUpdateView = true;
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::SetRadius( float fMin, float fMax )
	{
		m_fMinRadius = fMin;
		m_fMaxRadius = fMax;

		if (m_fMaxRadius < m_fRadius)
		{
			m_fRadius = m_fMaxRadius;
			m_bNeedUpdateView = true;
		}
		else if (m_fMinRadius > m_fRadius)
		{
			m_fRadius = m_fMinRadius;
			m_bNeedUpdateView = true;
		}
	}
	void CnModelViewCamera::SetRadius( float fCurrent )
	{
		if (m_fMinRadius > fCurrent)
		{
			m_fRadius = m_fMinRadius;
		}
		else if (m_fMaxRadius < fCurrent)
		{
			m_fRadius = m_fMaxRadius;
		}
		else
		{
			m_fRadius = fCurrent;
		}

		m_bNeedUpdateView = true;
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::GetRadius( float& Min, float& Max, float& Current )
	{
		Min = m_fMinRadius;
		Max = m_fMaxRadius;
		Current = m_fRadius;
	}

	//----------------------------------------------------------------
	float CnModelViewCamera::GetAngle( Axis::Enum eAxis )
	{
		return m_Angles[eAxis];
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::GetDirection( Math::Vector3& Output )
	{
		Output = m_vLookAt - GetPosition();
		Output.Normalize();
	}

	//----------------------------------------------------------------
	void CnModelViewCamera::GetRight( Math::Vector3& Output )
	{
		Math::Vector3 dir;
		GetDirection( dir );
		Output.Cross( dir, GetUp() );
		Output.Normalize();
	}
}