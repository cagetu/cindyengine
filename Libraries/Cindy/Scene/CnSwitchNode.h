#ifndef __CN_SWITCH_NODE_H__
#define __CN_SWITCH_NODE_H__

#include "CnSceneNode.h"

namespace Cindy
{
	//================================================================
	/** Switch Node
	    @author    changhee
		@since     2008. 5. 27
		@remarks   하위 장면 노드를 선택할 수 있는 노드
	*/
	//================================================================
	class CN_DLL CnSwitchNode : public CnSceneNode
	{
		__DeclareClass(CnSwitchNode);
	public:
		CnSwitchNode();
		CnSwitchNode( const wchar* strName, ushort nIndex );
		virtual ~CnSwitchNode();

		void			SelectChild( const CnString& strName );
		const CnString& SelectedChild() const;

		// LOD
		void			SetDistance( const CnString& strChild, float fMinDist, float fMaxDist );
		float			GetMinDistance( const CnString& strChild );
		float			GetMaxDistance( const CnString& strChild );

		virtual void	UpdateScene( CnCamera* pCamera, CnVisibleSet* pVisibleSet ) override;

	private:
		typedef HashMap<CnString, float>	ChildDistMap;
		typedef ChildDistMap::iterator		ChildDistIter;

		//------------------------------------------------------
		//	Variables
		//------------------------------------------------------
		CnString		m_strSelectedChild;

		ChildDistMap	m_ChildMinDist;
		ChildDistMap	m_ChildMaxDist;

		//------------------------------------------------------
		//	Methods
		//------------------------------------------------------
		void	_SelectChild( CnCamera* pCamera );
	};

#include "CnSwitchNode.inl"
}

#endif	// __CN_SWITCH_NODE_H__