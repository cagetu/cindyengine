//================================================================
// File:               : CnSceneEntity.inl
// Related Header File : CnSceneEntity.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 26
//================================================================

//----------------------------------------------------------------
inline
void CnSceneEntity::SetName( const CnString& strName )
{
	m_strName = strName;
}
inline
const CnString& CnSceneEntity::GetName() const
{
	return m_strName;
}

//----------------------------------------------------------------
inline
void CnSceneEntity::Visible( bool bVisible )
{
	m_bVisible = bVisible;
}
inline
bool CnSceneEntity::IsVisible() const
{
	return m_bVisible;
}

//----------------------------------------------------------------
inline
void CnSceneEntity::SetCulled( bool bCulled )
{
	m_bCulled = bCulled;
}
inline
bool CnSceneEntity::IsCulled() const
{
	return m_bCulled;
}

//================================================================
/** @brief	그림자를 드리우는 녀석인지 평가 */
//================================================================
inline
void CnSceneEntity::SetCastShadow( bool bEnable )
{
	m_bCastShadow = bEnable;
}

//----------------------------------------------------------------
inline
bool CnSceneEntity::IsCastShadow() const
{
	return m_bCastShadow;
}

//----------------------------------------------------------------
inline
void CnSceneEntity::EnableReceiveShadow( bool bEnable )
{
	m_bEnableReceiveShadow = bEnable;
}

//----------------------------------------------------------------
inline
bool CnSceneEntity::IsEnableReceiveShadow() const
{
	return m_bEnableReceiveShadow;
}

//----------------------------------------------------------------
inline
void CnSceneEntity::SetReceiveShadow( bool bEnable )
{
	m_bReceiveShadow = bEnable;
}

//----------------------------------------------------------------
inline
bool CnSceneEntity::IsReceiveShadow() const
{
	return m_bReceiveShadow;
}