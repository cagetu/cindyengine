//================================================================
// File:               : CnSwitchNode.cpp
// Related Header File : CnSwitchNode.h
// Original Author     : changhee
// Creation Date       : 2008. 5. 27
//================================================================
#include "../Cindy.h"
#include "CnSwitchNode.h"
#include "CnCamera.h"

namespace Cindy
{
	//============================================================================
	__ImplementClass( Cindy, CnSwitchNode, CnSceneNode );

	/// Const/Dest
	CnSwitchNode::CnSwitchNode()
	{
	}
	CnSwitchNode::CnSwitchNode( const wchar* strName, ushort nIndex )
		: CnSceneNode( strName, nIndex )
	{
	}
	CnSwitchNode::~CnSwitchNode()
	{
		DeleteAllEntities();
	}

	//================================================================
	/** Set Distance
	*/
	//================================================================
	void CnSwitchNode::SetDistance( const CnString& strChild, float fMinDist, float fMaxDist )
	{
		ChildDistIter iter = m_ChildMinDist.find( strChild );
		if (iter == m_ChildMinDist.end())
		{
			m_ChildMinDist.insert( ChildDistMap::value_type( strChild, fMinDist ) );
			m_ChildMaxDist.insert( ChildDistMap::value_type( strChild, fMaxDist ) );
		}
		else
		{
			iter->second = fMinDist;

			ChildDistIter im = m_ChildMaxDist.find( strChild );
			im->second = fMaxDist;
		}
	}

	//================================================================
	/** @brief	카메라와의 거리에 따라 자식을 선택한다.
	*/
	//================================================================
	void CnSwitchNode::_SelectChild( CnCamera* pCamera )
	{
		Math::Vector3 length = GetWorldPosition() - pCamera->GetPosition();
		float distance = length.Length();

		int size = (int)m_ChildMinDist.size();
		ChildDistIter imin = m_ChildMinDist.begin();
		ChildDistIter imax = m_ChildMaxDist.begin();
		
		for (int i=0; i<size; ++i)
		{
			if (imin->second <= distance && distance < imax->second)
			{
				SelectChild( imin->first );
				break;
			}

			++imin;
			++imax;
		}
	}

	//================================================================
	/** Update Scene
	    @remarks      실제 랜더링 될 개체들을 업데이트 한다.
		@param        pCamera : 카메라
		@param		  pVisibleSet : 보이는 Entity 담는 그릇..
		@return       none
	*/
	//================================================================
	void CnSwitchNode::UpdateScene( CnCamera* pCamera, CnVisibleSet* pVisibleSet )
	{
		if (false == _FindVisibleEntities( pCamera, pVisibleSet ))
			return;

		_SelectChild( pCamera );

		CnNode* child = GetChild( m_strSelectedChild );
		if (child)
		{
			child->UpdateScene( pCamera, pVisibleSet );
		}
	}
}