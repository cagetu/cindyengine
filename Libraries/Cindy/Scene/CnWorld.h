#ifndef __CN_WORLD_H__
#define __CN_WORLD_H__

#include "CnSceneGraph.h"
#include "../Render/CnRenderQueue.h"

namespace Cindy
{
	class CnVisibleSet;
	class CnCamera;

	//================================================================
	/** World
	    @author		changhee
		@since		2008. 5. 21
		@remarks	가상 세계에 대한 클래스
					
	*/
	//================================================================
	class CnWorld
	{
		typedef std::vector<SceneGraphPtr>	SceneGraphList;
		typedef SceneGraphList::iterator	SceneGraphIter;
	private:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		SceneGraphList		m_SceneGraphes;

		CnVisibleSet*		m_pVisibleSet;
		CnRenderQueue		m_RenderQueue;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		void	FindVisible( CnCamera* pCamera );
		void	UpdateLinks( CnCamera* pCamera );

		void	UpdateQueue();
		void	RenderQueue( CnCamera* pCamera );
	public:
		CnWorld();
		~CnWorld();

		void	Register( const SceneGraphPtr& SceneGraph );
		void	Unregister( const SceneGraphPtr& SceneGraph );

		void	Draw( CnCamera* pCamera );
	};
}

#endif	// __CN_WORLD_H__