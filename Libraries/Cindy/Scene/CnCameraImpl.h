//================================================================
// File:           : CnCameraImpl.h
// Original Author : changhee
// Creation Date   : 2007. 1. 25
//================================================================
#pragma once

#include "CnCamera.h"

#include "Math/CnQuaternion.h"

namespace Cindy
{
	//==================================================================
	/** First Person Camera
		@author		ChangeHee
		@remarks
			1인칭 카메라
	*/
	//==================================================================
	class CN_DLL CnFirstPersonCamera : public CnCamera
	{
		__DeclareClass(CnFirstPersonCamera);
	public:
		CnFirstPersonCamera();
		CnFirstPersonCamera( const CnString& strName );
		virtual ~CnFirstPersonCamera();

		//void		SetPosition( const Math::Vector3& vPosition ) override;
	};

	//==================================================================
	/** Model View Camera
		@author		ChangHee
		@remarks
			3인칭 카메라
	*/
	//==================================================================
	class CN_DLL CnModelViewCamera : public CnCamera
	{
		__DeclareClass(CnModelViewCamera);
	public:
		struct Axis
		{
			enum Enum
			{
				X = 0,
				Y,
				Z,
				Axises,
			};
		};

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnModelViewCamera();
		CnModelViewCamera( const CnString& strName );
		virtual ~CnModelViewCamera();

		void	SetPosition( const Math::Vector3& vPosition ) override;
		void	SetLookAt( const Math::Vector3& vLookAt ) override;

		void	SetAngle( Radian fAngle, Axis::Enum eAxis );
		void	Rotate( Radian fAngle, Axis::Enum eAxis );

		//void	SetDirection( const CnVector2& Direction );
		void	SetOffset( const Math::Vector3& vOffset );
		void	SetRestrictAngle( float fAngle );

		void	SetRadius( float fMin, float fMax );
		void	SetRadius( float fCurrent );

		void	GetRadius( float& Min, float& Max, float& Current );
		Radian	GetAngle( Axis::Enum eAxis );

		void	GetDirection( Math::Vector3& Output );
		void	GetRight( Math::Vector3& Output );

	protected:
		mutable Math::Vector3	m_vEye;
		Math::Vector3			m_vOffset;

		float				m_Angles[Axis::Axises];

		float				m_fRadius;
		float				m_fMinRadius;
		float				m_fMaxRadius;

		float				m_fRestrictAngle;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		void	UpdateView() const override;
		void	_UpdateEye() const;
	};

	//==================================================================
	/** MeshDataPtr
		@author			cagetu
		@since			2006년 10월 9일
		@remarks		Mesh 리소스용 스마트 포인터
	*/
	//==================================================================
	class CN_DLL MdvCamPtr : public Ptr<CnModelViewCamera>
	{
	public:
		MdvCamPtr();
		explicit MdvCamPtr( CnModelViewCamera* pObject );
		MdvCamPtr( const MdvCamPtr& spObject );
		MdvCamPtr( const CameraPtr& spCam );

		/** operator = */
		MdvCamPtr& operator = ( const CameraPtr& spCam );
	};

#include "CnCameraImpl.inl"
}