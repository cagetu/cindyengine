//================================================================
// File:           : CnCamera.h
// Original Author : changhee
// Creation Date   : 2007. 1. 27
//================================================================
#ifndef __CNCAMERA_H__
#define __CNCAMERA_H__

#include "Cindy.h"
#include "Math/CindyMath.h"
#include "Math/CnFrustum.h"
#include "Foundation/CnObject.h"

namespace Cindy
{
	class CnScene;

	//==================================================================
	/** Camera Class
		@author			cagetu
		@since			2007년 1월 27일
		@remarks		카메라 클래스
	*/
	//==================================================================
	class CN_DLL CnCamera : public CnObject
	{
		__DeclareRtti;
	public:
		CnCamera();
		CnCamera( const CnString& strName );
		virtual ~CnCamera();

		// Set Informations
		void					SetName( const CnString& Name );
		const CnString&			GetName() const;

		void					SetFrustum( float fFovY, float fAspect, float fNear, float fFar, float fFarForFrustum = 0.0f );

		void					SetAspect( float fAspect );
		float					GetAspect() const;

		void					SetFar( float fFar );
		float					GetFar() const;

		void					SetFarForFrustum( float fFar );
		float					GetFarForFrustum() const;

		void					SetFovY( float fFovY );
		float					GetFovY() const;

		void					SetNear( float fNear );
		float					GetNear() const;

		float					GetNearWidth() const;
		float					GetNearHeight() const;
		float					GetFarWidth() const;
		float					GetFarHeight() const;

		void					SetPerspective( bool bPersp );
		bool					IsPerspective() const;

		void					SetDefault( bool bDefault );
		bool					IsDefault() const;

		Math::Vector3*			GetFrustumPoints() const;

		// Position
		virtual void					SetPosition( const Math::Vector3& vPosition );
		virtual const Math::Vector3&	GetPosition() const;

		// Rotate

		// Up
		void					SetUp( const Math::Vector3& vUp );
		const Math::Vector3&	GetUp() const;

		// Look
		virtual void			SetLookAt( const Math::Vector3& vLookAt );
		const Math::Vector3&	GetLookAt() const;

		// Direction
		const Math::Vector3&	GetDirection() const;

		// Matrix
		virtual void			UpdateView() const;
		virtual void			UpdateProjection() const;
		virtual void			UpdateFrustum() const;

		const Math::Matrix44&	GetView() const;
		const Math::Matrix44&	GetProjection() const;
		const Math::Matrix44&	GetViewProjection() const;

		// Culling
		bool					IsCulled( const Math::AABB& rBox ) const;
		bool					IsCulled( const Math::OBB& rBox ) const;
		bool					IsCulled( const Math::Vector3& rCenter, float fRadius ) const;
		bool					IsCulled( const Math::Sphere& sphere, const Math::Vector3& sweepDir ) const;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString				m_strName;

		float					m_fNear;
		float					m_fFar;
		float					m_fAspect;
		float					m_fFovY;

		mutable float			m_fNearWidth;
		mutable float			m_fNearHeight;
		mutable float			m_fFarWidth;
		mutable float			m_fFarHeight;

		bool					m_bPerspective;

		bool					m_bDefault;

		float					m_fFarForFrustum;
		mutable	Math::Frustum	m_Frustum;

		mutable Math::Vector3	m_vPosition;
		mutable Math::Vector3	m_vLookAt;
		mutable Math::Vector3	m_vUp;
		mutable Math::Vector3	m_vDirection;

		mutable Math::Matrix44	m_mView;							//!< 뷰 매트릭스
		mutable Math::Matrix44	m_mProjection;						//!< 프로젝션 매트릭스
		mutable Math::Matrix44	m_mViewProjection;					//!< 뷰-프로젝션 매트릭스

		mutable bool			m_bNeedUpdateProjection;
		mutable bool			m_bNeedUpdateView;
		mutable bool			m_bNeedUpdateFrustum;
	};

	typedef Ptr<CnCamera>	CameraPtr;
#include "CnCamera.inl"
}

#endif	// __CNCAMERA_H__