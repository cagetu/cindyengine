//================================================================
// File:           : CnSceneEntity.h
// Original Author : changhee
// Creation Date   : 2007. 1. 26
//================================================================
#ifndef __CN_SCENE_ENTITY_H__
#define __CN_SCENE_ENTITY_H__

#include "Foundation/CnObject.h"

namespace Cindy
{
	class CnCamera;
	class CnSceneNode;

	namespace Math
	{
		class AABB;
		class Vector3;
		class Quaternion;
	}

	//================================================================
	/** SceneEntity
	    @author    changhee
		@since     2007. 1. 26
		@remarks   화면에 그려질 개체들의 최상위 클래스
	*/
	//================================================================
	class CN_DLL CnSceneEntity : public CnObject
	{
		__DeclareRtti;
	public:
		CnSceneEntity( const CnString& strName = L"" );
		virtual ~CnSceneEntity();

		// Name
		void					SetName( const CnString& strName );
		const CnString&			GetName() const;

		// Parent
		void					SetParent( CnSceneNode* pParent );
		CnSceneNode*			GetParent() const;

		// Visible
		void					Visible( bool bVisible );
		bool					IsVisible() const;

		// Cast Shadow
		virtual void			SetCastShadow( bool bEnable );
		bool					IsCastShadow() const;

		void					EnableReceiveShadow( bool bEnable );
		bool					IsEnableReceiveShadow() const;

		void					SetReceiveShadow( bool bEnable );
		bool					IsReceiveShadow() const;

		//
		void					SetCulled( bool bCulled );
		bool					IsCulled() const;

		// Coordination
		const Math::Vector3&	GetWorldPosition() const;
		const Math::Quaternion&	GetWorldRotation() const;
		const Math::Vector3&	GetWorldScale() const;

		// Links
		virtual void			RelationTo( CnSceneEntity* pEntity, CnCamera* pCamera ) abstract;
		virtual void			ClearRelation() abstract;

		// Bounding Volume
		virtual void			GetAABB( Math::AABB& rOut ) abstract;

		// Culling
		virtual bool			Cull( CnCamera* pCamera ) abstract;

		//
		virtual void			Apply( CnCamera* pCamera ) abstract;

		// Update
		virtual bool			Update( CnCamera* pCamera ) abstract;
		virtual void			Draw( CnCamera* pCamera ) abstract;

	protected:
		//typedef std::vector<CnSceneEntity*>	LinkList;
		//typedef LinkList::iterator		LinkIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString		m_strName;
		CnSceneNode*	m_pParent;

		bool			m_bVisible;
		bool			m_bCulled;

		bool			m_bCastShadow;
		bool			m_bReceiveShadow;
		bool			m_bEnableReceiveShadow;

		//LinkList		m_Links;
	};

#include "CnSceneEntity.inl"
}

#endif	// __CN_SCENE_ENTITY_H__