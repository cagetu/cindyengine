//================================================================
// File:               : CnWindow.cpp
// Related Header File : CnWindow.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 23
//================================================================
#include "../Cindy.h"
#include "CnWindow.h"
#include "CnViewport.h"

namespace Cindy
{
	//-------------------------------------------------------------------------
	CnWindow::CnWindow( HWND hWnd, const CnString& strName, int nWidth, int nHeight )
		: m_hWnd( hWnd )
		, m_strName( strName )
		, m_nWidth( nWidth )
		, m_nHeight( nHeight )
	{
	}

	//-------------------------------------------------------------------------
	CnWindow::~CnWindow()
	{
	}

	//================================================================
	/** Create Viewport
	    @remarks      뷰포트를 생성한다.
		@param        nZOrder : 뷰포트의 z 값( 뷰포트가 여러개 있을 때 정렬값으로 이용)
		@param        fRelTop : 상대적인 뷰포트 Top 위치( 0.0f ~ 1.0f )
		@param        fRelLeft : 상대적인 뷰포트 Left 위치( 0.0f ~ 1.0f )
		@param        fRelWidth : 상대적인 뷰포트 Width 위치( 0.0f ~ 1.0f )
		@param        fRelHeight : 상대적인 뷰포트 Height 위치( 0.0f ~ 1.0f )
		@return       CnViewport : 생성된 뷰포트
	*/
	//================================================================
	CnViewport* CnWindow::CreateViewport( int nZOrder, float fRelTop, float fRelLeft, float fRelWidth, float fRelHeight )
	{
		ViewportIter iter = m_Viewports.find( nZOrder );
		if( iter == m_Viewports.end() )
		{
			CnViewport* viewport = new CnViewport( this, nZOrder, fRelTop, fRelLeft, fRelWidth, fRelHeight );
			m_Viewports.insert( ViewportList::value_type( nZOrder, viewport ) );

			return viewport;
		}

		return iter->second;
	}

	//================================================================
	/** Update
	    @remarks      윈도우를 업데이트 한다
		@param        none
		@return       none
	*/
	//================================================================
	void CnWindow::Update()
	{
		ViewportIter iend = m_Viewports.end(); 
		for( ViewportIter i = m_Viewports.begin(); i != iend; ++i )
		{
			i->second->Update();
		}
	}

	//================================================================
	/** Resize
		@remarks      윈도우의 사이즈를 갱신한다.
		@param        nWidth : 가로 길이
		@param		  nHeight : 세로 길이
		@return       none
	*/
	//================================================================
	void CnWindow::Resize( int nWidth, int nHeight )
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;

		ViewportIter iend = m_Viewports.end(); 
		for( ViewportIter i = m_Viewports.begin(); i != iend; ++i )
		{
			i->second->UpdateDimension();
		}
	}
}
