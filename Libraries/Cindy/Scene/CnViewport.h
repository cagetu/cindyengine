// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNVIEWPORT_H__
#define __CNVIEWPORT_H__

#include "Foundation/CnObject.h"
#include "CnCamera.h"
#include "CnScene.h"

namespace Cindy
{
	class CnRenderTarget;

	//==================================================================
	/** CnViewport
		@author			cagetu
		@since			2006년 10월 1일
		@brief			뷰포트
						: Rendering 과정에서 실제 투영 변환한 도형을 표시하는 직사각형의 영역.
	*/
	//==================================================================
	class CN_DLL CnViewport : public CnObject
	{
		__DeclareRtti;
	public:
		CnViewport( CnRenderTarget* pParent,
					int nZOrder, 
					float fRelTop,
					float fRelLeft,
					float fRelWidth,
					float fRelHeight );
		~CnViewport();

		// Set Positions
		void	SetDimension( float fRelLeft,
							  float fRelTop,
							  float fRelWidth,
							  float fRelHeight );

		// Get Informations
		int		GetZOrder() const;
		int		GetActualTop() const;
		int		GetActualLeft() const;
		int		GetActualWidth() const;
		int		GetActualHeight() const;

		void	SetCamera( const Ptr<CnCamera>& pCamera );
		void	SetScene( const Ptr<CnScene>& pScene );

		// Background Color
		void	SetBackgroundColor( ulong ulColor );
		ulong	GetBackgroundColor() const;

		// Update
		void	Update();
		// Draw
		void	Draw();

	private:
		friend class CnRenderTarget;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnRenderTarget*	m_pTarget;					//!< 부모 랜더 타겟

		float			m_fRelativeTop;				//!< related top position ( 0.0f ~ 1.0f )
		float			m_fRelativeLeft;			//!< related left position ( 0.0f ~ 1.0f )
		float			m_fRelativeWidth;			//!< related width ( 0.0f ~ 1.0f )
		float			m_fRelativeHeight;			//!< related height ( 0.0f ~ 1.0f )

		int				m_nActualTop;				//!< actual top position. Pixel
		int				m_nActualLeft;				//!< actual left position. Pixel
		int				m_nActualWidth;				//!< actual width. Pixel
		int				m_nActualHeight;			//!< actual height. Pixel

		int				m_nZOrder;					//!< Z Order

		ulong			m_ulBackgroundColor;		//!< 배경색

		Ptr<CnCamera>	m_pCamera;					//!< 뷰포트를 그리는 카메라
		Ptr<CnScene>	m_pScene;					//!< Viewport에 그릴 장면

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void		UpdateDimension();
	};

#include "CnViewport.inl"
}

#endif	// __CNVIEWPORT_H__