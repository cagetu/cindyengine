//================================================================
// File:           : CnVisibleSet.h
// Original Author : changhee
// Creation Date   : 2008. 5. 17
//================================================================
#ifndef __CN_VISIBLESET_H__
#define __CN_VISIBLESET_H__

#include "Foundation/CnMemObject.h"
#include "Foundation/CnObject.h"

namespace Cindy
{
	class CnSceneEntity;
	class CnCamera;

	//================================================================
	/** VisibleSet
	    @author    changhee
		@remarks   보이는 것을 모은다..
	*/
	//================================================================
	class CnVisibleSet : public CnObject
	{
		__DeclareClass(CnVisibleSet);
	public:
		typedef std::vector<CnSceneEntity*>	EntityArray;
		typedef EntityArray::iterator		EntityIter;
		typedef EntityArray::const_iterator	EntityConstIter;

		enum EntityType
		{
			Light = 0,
			Model,

			None,
			//ShadowCaster,
		};

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnVisibleSet();
		virtual ~CnVisibleSet();

		virtual void		Register( CnSceneEntity* pEntity );
		virtual void		Clear();

		bool				IsEmpty() const;

		//const EntityArray*	GetEntities( EntityType eType );
		EntityArray*		GetEntities( EntityType eType );

	protected:
		typedef std::map<EntityType, EntityArray*>	TypeEntityMap;
		typedef TypeEntityMap::iterator				TypeEntityIter;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		TypeEntityMap		m_TypeEntities;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		EntityType		GetType( CnSceneEntity* pEntity );
	};
}

#endif	// __CN_VISIBLESET_H__
