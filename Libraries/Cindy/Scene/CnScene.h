//================================================================
// File					: CnScene.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 22.
//================================================================
#ifndef __CN_SCENE_H__
#define __CN_SCENE_H__

#include "Foundation/CnMemObject.h"
#include "Util/CnRefCount.h"
#include "Util/CnUnCopyable.h"
#include "Material/CnShader.h"
#include "Scene/Component/CnBaseSceneComponent.h"

namespace Cindy
{
	class CnCamera;

	//==================================================================
	/** Scene
		@author			cagetu
		@since			2007년 1월 22일
		@remarks		장면
						하나의 장면을 여러개의 Viewport가 랜더링 할 수 있다.

						장면을 구성하는 Component들을 조합하여, 장면을 구성한다.
						Register 한 순서대로 처리한다.

						ex) scene->Register( new BeginScene() );
							scene->Regisger( new DefaultScene() );
							scene->Register( new GFxScene() );
							scene->Register( new EndScene() );
	*/
	//==================================================================
	class CN_DLL CnScene : public CnRefCount,
						   public CnMemLeakObject
	{
		__DeclareRtti;
		__DeclareUnCopy(CnScene);
	public:
		CnScene( const CnString& strName );
		virtual ~CnScene();

		// Name
		void		SetName( const CnString& Name );

		//
		void		SetShader( const wchar* FileName, int ShaderType = 0 );

		// Component
		void		AddComponent( const Ptr<Scene::Component>& spComponent );
		void		RemoveComponent( const SceneID& ID );
		SceneComPtr	GetComponent( const SceneID& ID );
		void		ClearComponents();

		// Update & Draw
		void		Update( CnCamera* pCamera );
		void		Draw( CnCamera* pCamera );

	protected:
		typedef std::vector<Ptr<Scene::Component> >	ComponentArray;
		typedef ComponentArray::iterator			ComponentIter;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString		m_strName;
		ShaderPtr		m_Shader;

		ComponentArray	m_Components;
	};

#include "CnScene.inl"
}

#endif	// __CN_SCENE_H__
