// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNNODE_H__
#define __CNNODE_H__

#include "CnIKObject.h"
#include "CnSceneObject.h"

namespace Cindy
{
	#define INVALID_NODE_ID	0xffff

	class CnSceneObject;
	class CnController;

	class CnNode;
	typedef Ptr<CnNode>	NodePtr;

	//==================================================================
	/** CnNode
		@author			cagetu
		@since			2006년 11월 29일
		@remarks		노드의 기본 클래스
						부모 자식 관계를 형성하고, 
	*/
	//==================================================================
	class CN_DLL CnNode : public CnIKObject,
						  public ISceneObject
	{
		__DeclareRtti;
	public:
		typedef HashMap<CnString, NodePtr>		ChildMap;
		typedef ChildMap::iterator				ChildIter;

		typedef std::vector<CnTransformable*>	UpdateArray;
		typedef UpdateArray::iterator			UpdateIter;

		//typedef std::map<int, CnController*>	ControllerMap;
		//typedef ControllerMap::iterator		ControllerIter;
	
		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnNode();
		CnNode( const wchar* strName, ushort nIndex = 0xffff );
		virtual ~CnNode();

		// Child
		virtual CnNode*		CreateChild( const CnString& strName, ushort nIndex = 0xffff,
										 const Math::Vector3& vPosition = Math::Vector3(0.0f, 0.0f, 0.0f),
										 const Math::Quaternion& qRotation = Math::Quaternion(0.0f, 0.0f, 0.0f, 1.0f),
										 const Math::Vector3& vScale = Math::Vector3(1.0f, 1.0f, 1.0f) );

		virtual bool		AttachChild( CnNode* pChild );

		virtual CnNode*		DetachChild( ushort nIndex );
		virtual CnNode*		DetachChild( const CnString& strName );
		virtual void		DetachChildren( std::vector<CnNode*>& rChildren );

		virtual bool		DeleteChild( ushort nIndex );
		virtual bool		DeleteChild( const CnString& strName );
		virtual void		DeleteChildren();

		CnNode*				GetChild( ushort nIndex, bool bRecursive = false );
		CnNode*				GetChild( const CnString& strName, bool bRecursive = false );
		const ChildMap&		GetChildren() const;

		ushort				GetNumOfChildren() const;

		// Controller
		//bool				AttachController( CnController* pController );
		//CnController*		GetController( int nID );
		//
		//CnController*		DetachController( int nID );
		//void				DetachAllController( std::vector<CnController*>& rControllers );

		//bool				DeleteController( int nID );
		//void				DeleteAllControllers();

		// Update
		void				NeedUpdateParent() override;

		void				RequestUpdate( CnTransformable* pNode );
		void				CancelUpdate( CnTransformable* pNode );

		void				UpdateTransform( bool bUpdateChildren, bool bChangedParent ) override;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		ChildMap			m_Children;			//!< 자식 노드들
		ushort				m_NumOfChildren;
		//ControllerMap		m_Controllers;		//!< 컨트롤러 리스트

		mutable	UpdateArray	m_ChildrenToUpdate;	//!< 업데이트 리스트
		mutable bool		m_bUpdateChildren;	//!< 자식들 업데이트 여부

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual CnNode*	_CreateChildImpl( const MoString& strName, ushort nIndex ) abstract;
		virtual void	UpdateState( std::vector<CnGlobalState*>* paGSStack ) override;
	};

#include "CnNode.inl"
}

#endif	// __CNNODE_H__