//================================================================
// File:               : CnSwitchNode.inl
// Related Header File : CnSwitchNode.h
// Original Author     : changhee
//================================================================

inline
void CnSwitchNode::SelectChild( const CnString& strName )
{
	m_strSelectedChild = strName;
}
inline
const CnString& CnSwitchNode::SelectedChild() const
{
	return m_strSelectedChild;
}

inline
float CnSwitchNode::GetMaxDistance( const CnString& strChild )
{
	ChildDistIter iter = m_ChildMaxDist.find( strChild );
	return iter->second;
}

inline
float CnSwitchNode::GetMinDistance( const CnString& strChild )
{
	ChildDistIter iter = m_ChildMinDist.find( strChild );
	return iter->second;
}
