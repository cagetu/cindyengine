// Copyright (c) 2006~. cagetu
//
//******************************************************************
//----------------------------------------------------------------
//	Class ModelViewCamera
//----------------------------------------------------------------
inline
void CnModelViewCamera::SetOffset( const Math::Vector3& vOffset )
{
	m_vOffset = vOffset;
}

//----------------------------------------------------------------
inline
void CnModelViewCamera::SetRestrictAngle( float fAngle )
{
	m_fRestrictAngle = fAngle;
}

//----------------------------------------------------------------
inline
void CnModelViewCamera::_UpdateEye() const
{
	m_vEye = m_vPosition + m_vOffset;
}

//----------------------------------------------------------------
//	MdvCamPtr Class
//----------------------------------------------------------------
inline
MdvCamPtr::MdvCamPtr()
: Ptr< CnModelViewCamera >()
{
}

//----------------------------------------------------------------
inline
MdvCamPtr::MdvCamPtr( CnModelViewCamera* pObject )
: Ptr< CnModelViewCamera >( pObject )
{
}

//----------------------------------------------------------------
inline
MdvCamPtr::MdvCamPtr( const MdvCamPtr& spObject )
: Ptr< CnModelViewCamera >( spObject )
{
}

//----------------------------------------------------------------
inline
MdvCamPtr::MdvCamPtr( const CameraPtr& spCam )
: Ptr< CnModelViewCamera >()
{
	m_pObject = (CnModelViewCamera*)spCam.GetPtr();
	if( m_pObject )
	{
		m_pObject->AddRef();
	}
}

//----------------------------------------------------------------
inline
MdvCamPtr& MdvCamPtr::operator = ( const CameraPtr& spCam )
{
	if( m_pObject != (CnModelViewCamera*)spCam.GetPtr() )
	{
		if( m_pObject )
			m_pObject->Release();

		if( !spCam.IsNull() )
			spCam->AddRef();

		m_pObject = (CnModelViewCamera*)spCam.GetPtr();
	}

	return *this;
}