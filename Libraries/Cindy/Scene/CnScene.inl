//================================================================
// File					: CnScene.cpp
// Related Header File	: CnScene.inl
// Original Author		: Changhee
// Creation Date		: 2007. 1. 22.
//================================================================
inline
void CnScene::SetName( const CnString& Name )
{
	m_strName = Name;
}