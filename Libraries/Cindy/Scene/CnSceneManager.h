//================================================================
// File					: CnSceneManager.h			
// Original Author		: Changhee
// Creation Date		: 2009. 9. 14.
//================================================================
#ifndef __CN_SCENE_MANAGER_H__
#define __CN_SCENE_MANAGER_H__

#include "Util/CnSingleton.h"
#include "Scene/CnScene.h"

namespace Cindy
{
	//==================================================================
	/** SceneManager
		@author			cagetu
		@since			2009년 9월 14일
		@remarks		현재 처리되고 있는 장면 관리자.
	*/
	//==================================================================	
	class CN_DLL CnSceneManager
	{
		__DeclareSingleton(CnSceneManager);
		__DeclareUnCopy(CnSceneManager);
	public:
		CnSceneManager();
		virtual ~CnSceneManager();

		void SetCurrentScene( const Pointer<CnScene>& Scene );
		const Pointer<CnScene>& GetCurrentScene() const;

		void SetCurrentCamera( const Pointer<CnCamera>& Camera );
		const Pointer<CnCamera>& GetCurrentCamera() const;
	private:
		Pointer<CnScene>	m_CurrentScene;
		Pointer<CnCamera>	m_CurrentCamera;
	};

	//----------------------------------------------------------------
	/** @brief
	*/
	inline void
	CnSceneManager::SetCurrentScene( const Pointer<CnScene>& Scene )
	{
		m_CurrentScene = Scene;
	}

	//----------------------------------------------------------------
	/** @brief
	*/
	inline const Pointer<CnScene>&
	CnSceneManager::GetCurrentScene() const
	{
		return m_CurrentScene;
	}

	//----------------------------------------------------------------
	/** @brief
	*/
	inline void
	CnSceneManager::SetCurrentCamera( const Pointer<CnCamera>& Camera )
	{
		m_CurrentCamera = Camera;
	}

	//----------------------------------------------------------------
	/** @brief
	*/
	inline const Pointer<CnCamera>&
	CnSceneManager::GetCurrentCamera() const
	{
		return m_CurrentCamera;
	}
} // namespace Cindy

#endif	// __CN_SCENE_MANAGER_H__