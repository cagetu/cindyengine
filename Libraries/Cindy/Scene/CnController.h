//================================================================
// File:           : CnController.h
// Original Author : changhee
// Creation Date   : 2007. 1. 27
//================================================================
#ifndef __CN_CONTROLLER_H__
#define __CN_CONTROLLER_H__

#include "../Foundation/CnObject.h"

namespace Cindy
{
	class CnNode;

	typedef int ControllerID;

	//================================================================
	/** Controller 
	    @author    changhee
		@since     2007. 1. 27
		@remarks   Controller Ŭ����
					animation updator
					material updator
					scale updator
					skeleton updator ...
	*/
	//================================================================
	class CnController : public CnObject
	{
		__DeclareRtti;
	public:
		CnController();
		virtual ~CnController();

		// Set
		void	SetParent( CnNode* pParent );

		// Get
		virtual ControllerID	GetID() const abstract;

		virtual void	Update() abstract;
		virtual void	Clear() abstract;

	protected:
		//----------------------------------------------------------------
		// Variables
		//----------------------------------------------------------------
		CnNode*		m_pParent;
	};
}

#endif	// __CN_CONTROLLER_H__