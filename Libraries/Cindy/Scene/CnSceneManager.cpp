//================================================================
// File					: CnSceneManager.cpp
// Related Header File	: CnSceneManager.h
// Original Author		: Changhee
// Creation Date		: 2009. 9. 14.
//================================================================
#include "../Cindy.h"
#include "CnSceneManager.h"
#include "Util/CnLog.h"

namespace Cindy
{
	__ImplementSingleton(CnSceneManager);
	//--------------------------------------------------------------
	// const
	CnSceneManager::CnSceneManager()
	{
		__ConstructSingleton;
	}
	// dest
	CnSceneManager::~CnSceneManager()
	{
		__DestructSingleton;
	}
}