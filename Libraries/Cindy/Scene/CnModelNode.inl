//================================================================
// File:               : CnModelNode.inl
// Related Header File : CnModelNode.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 25
//================================================================

//----------------------------------------------------------------
inline
const Ptr<CnSkeletonInstance>&
CnModelNode::GetSkeleton() const
{
	return m_Skeleton;
}

//----------------------------------------------------------------
inline
const Ptr<CnAnimController>&
CnModelNode::GetAnimController() const
{
	return m_AnimController;
}