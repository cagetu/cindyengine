//================================================================
// File:           : CnSceneNode.h
// Original Author : changhee
// Creation Date   : 2007. 1. 26
//================================================================
#ifndef __CN_SCENENODE_H__
#define __CN_SCENENODE_H__

#include "CnAnimNode.h"
#include "CnSceneEntity.h"

namespace Cindy
{
	//================================================================
	/** Scene Node
	    @author    changhee
		@since     2007. 1. 26
		@remarks   장면을 그릴 수 있는 객체들을 가지는 노드
	*/
	//================================================================
	class CN_DLL CnSceneNode : public CnAnimNode
	{
		__DeclareClass(CnSceneNode);
	public:
		//------------------------------------------------------
		// Types
		//------------------------------------------------------
		typedef std::vector<CnSceneNode*>			SceneNodeArray;
		typedef SceneNodeArray::iterator			SceneNodeIter;

		typedef HashMap<CnString, CnSceneEntity*>	EntityMap;
		typedef EntityMap::iterator					EntityIter;

		//static MoMemoryPool<CnSceneNode>		ms_MemoryPool;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		CnSceneNode();
		CnSceneNode( const wchar* strName, ushort nIndex );
		virtual ~CnSceneNode();

		// Child
		bool				AttachChild( CnNode* pChild ) override;

		CnNode*				DetachChild( ushort nIndex ) override;
		CnNode*				DetachChild( const CnString& strName ) override;
		void				DetachChildren( std::vector<CnNode*>& rChildren ) override;

		bool				DeleteChild( ushort nIndex ) override;
		bool				DeleteChild( const CnString& strName ) override;
		void				DeleteChildren() override;

		// Entity
		bool				AttachEntity( CnSceneEntity* pEntity );
		CnSceneEntity*		GetEntity( const CnString& strName );
		const EntityMap&	GetAllEntities() const;

		CnSceneEntity*		DetachEntity( const CnString& strName );
		void				DetachAllEntities( std::vector<CnSceneEntity*>& rEntities );

		bool				DeleteEntity( const CnString& strName );
		void				DeleteAllEntities();

		// Update
		void				UpdateTransform( bool bUpdateChildren, bool bChangedParent ) override;
		void				UpdateScene( CnCamera* pCamera, CnVisibleSet* pVisibleSet ) override;

		// Visible
		void				Visible( bool bVisible );
		bool				IsVisible() const;

		// Etc
		void				GetBound( Math::AABB* pBox );

		void				EnableCulling( bool bUse );
		bool				UseCulling() const;

	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		SceneNodeArray	m_ChildSceneNodes;		//!< 장면 노드만 모아둔 리스트
		EntityMap		m_SceneEntities;		//!< 장면 Entity 리스트

		bool			m_bVisible;				
		bool			m_bUseCulling;

		Math::AABB		m_Bound;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		virtual CnNode*	_CreateChildImpl( const CnString& strName, ushort nIndex ) override;

		void			_AddSceneNode( CnNode* pChild );
		void			_RemoveSceneNode( CnNode* pChild );
		void			_RemoveAllSceneNodes();

		virtual void	_UpdateBound();

		virtual void	_UpdateAfterCulling();
		virtual bool	_FindVisibleEntities( CnCamera* pCamera, CnVisibleSet* pVisibleSet );
	};

#include "CnSceneNode.inl"
}

#endif	// __CN_SCENENODE_H__