//================================================================
// File					: CnSceneGraph.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 27.
//================================================================
#ifndef __CN_SCENEGRAPH_H__
#define __CN_SCENEGRAPH_H__

#include "Foundation/CnObject.h"
#include "Util/CnUnCopyable.h"

namespace Cindy
{
	class CnCamera;
	class CnNode;
	class CnSceneNode;
	class CnLight;
	class CnRenderQueue;
	class CnVisibleSet;

	//==================================================================
	/** SceneGraph
		@author			cagetu
		@since			2007년 1월 27일
		@remarks		장면 그래프 관리자.
	*/
	//==================================================================	
	class CN_DLL CnSceneGraph : public CnObject//, private CnUncopyable
	{
		__DeclareClass(CnSceneGraph);
		__DeclareUnCopy(CnSceneGraph);
	public:
		CnSceneGraph( const CnString& strName = L"" );
		virtual ~CnSceneGraph();

		// Name
		void				SetName( const CnString& Name );
		const CnString&		GetName() const;

		// Node
		CnSceneNode*		GetRoot() const;
		void				AttachChild( CnNode* pNode );

		// Update 
		virtual void		UpdateTransform();
		virtual void		FindVisible( CnCamera* pCamera, CnVisibleSet* pVisibleSet );

	private:
		friend class CnScene;
		friend class CnCindy;

		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString		m_strName;

		CnSceneNode*	m_pRootNode;
		//CnRenderQueue*	m_pRenderQueue;
	};

typedef Ptr<CnSceneGraph>	SceneGraphPtr;
}

#endif	// __CN_SCENEGRAPH_H__