#include "../Cindy.h"
#include "CnWorld.h"
#include "CnVisibleSet.h"

#include "CnLight.h"
#include "../Geometry/CnMesh.h"

namespace Cindy
{
	CnWorld::CnWorld()
	{
	}
	CnWorld::~CnWorld()
	{
		m_SceneGraphes.clear();
	}

	//----------------------------------------------------------------
	/** @brief	등록 */
	//----------------------------------------------------------------
	void CnWorld::Register( const SceneGraphPtr& SceneGraph )
	{
		m_SceneGraphes.push_back( SceneGraph );
	}

	//----------------------------------------------------------------
	/** @brief	해제 */
	//----------------------------------------------------------------
	void CnWorld::Unregister( const SceneGraphPtr& SceneGraph )
	{
		SceneGraphIter iter = std::find( m_SceneGraphes.begin(), m_SceneGraphes.end(), SceneGraph );
		if (iter == m_SceneGraphes.end())
			return;

		m_SceneGraphes.erase( iter );
	}

	//----------------------------------------------------------------
	/** @brief	보이는 객체들 찾기 */
	//----------------------------------------------------------------
	void CnWorld::FindVisible( CnCamera* pCamera )
	{
		m_pVisibleSet->Clear();

		SceneGraphIter i, iend;
		iend = m_SceneGraphes.end();
		for (i=m_SceneGraphes.begin(); i!=iend; ++i)
		{
			(*i)->FindVisible( pCamera, m_pVisibleSet );
		}
	}

	//----------------------------------------------------------------
	/** @brief	보이는 객체들의 관계를 만든다. */
	//----------------------------------------------------------------
	void CnWorld::UpdateLinks( CnCamera* pCamera )
	{
		if (m_pVisibleSet->IsEmpty())
			return;

		const CnVisibleSet::EntityList* lights = m_pVisibleSet->GetEntities( CnVisibleSet::Light );
		const CnVisibleSet::EntityList* models = m_pVisibleSet->GetEntities( CnVisibleSet::Model );

		if (lights)
		{
			if (models)
			{
				CnVisibleSet::EntityConstIter li, liend;
				liend = lights->end();
				for (li = lights->begin(); li != liend; ++li)
				{
					CnVisibleSet::EntityConstIter mi, miend;
					miend = models->end();
					for (mi = models->begin(); mi != miend; ++mi)
					{
						(*mi)->RelationTo( (*li), pCamera );
						(*li)->RelationTo( (*mi), pCamera );
					}
				}
			}
		}
	}

	//----------------------------------------------------------------
	/** @brief	랜더큐에 넣기 */
	//----------------------------------------------------------------
	void CnWorld::UpdateQueue()
	{
		m_RenderQueue.Clear();

		const CnVisibleSet::EntityList* models = m_pVisibleSet->GetEntities( CnVisibleSet::Model );
		CnVisibleSet::EntityConstIter i, iend;
		iend = models->end();
		for (i=models->begin(); i!=iend; ++i)
		{
			m_RenderQueue.Register( (*i) );
		}
	}

	//----------------------------------------------------------------
	/** @brief	랜더큐 그리기 */
	//----------------------------------------------------------------
	void CnWorld::RenderQueue( CnCamera* pCamera )
	{
		m_RenderQueue.Draw( pCamera );
	}

	//----------------------------------------------------------------
	/** @brief	그리기
				[순서]
				1. 화면에 보이는 객체들을 모은다.
				2. 보이는 객체들간의 관계를 형성한다.
				3. 각 관계별로 처리를 한다.
				4. 화면에 그린다.
	*/
	//----------------------------------------------------------------
	void CnWorld::Draw( CnCamera* pCamera )
	{
		FindVisible( pCamera );
		UpdateLinks( pCamera );
		UpdateQueue();
		RenderQueue( pCamera );
	}
} // end of namespace