//================================================================
// File:               : CnTransformable.cpp
// Related Header File : CnTransformable.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 25
//================================================================
#include "../Cindy.h"
#include "CnTransformable.h"
#include "CnNode.h"

#include "../Util/CnLog.h"

namespace Cindy
{
	//============================================================================
	__ImplementRtti( Cindy, CnTransformable, CnObject );

	/// Const/Dest
	CnTransformable::CnTransformable()
		: m_nIndex( 0xffff )
		, m_pParent( 0 )
		, m_bMatrixOutOfDate( false )
		, m_bUpdateParent( false )
		, m_bParentNotified( false )
		, m_bUpdateRotationFromParent( true )
		, m_bUpdateScaleFromParent( true )
		, m_vLocalPosition( Math::Vector3::ZERO )
		, m_qLocalRotation( Math::Quaternion::IDENTITY )
		, m_vLocalScale( Math::Vector3::UNIT )
		, m_vWorldPosition( Math::Vector3::ZERO )
		, m_qWorldRotation( Math::Quaternion::IDENTITY )
		, m_vWorldScale( Math::Vector3::UNIT )
		, m_mWorldXForm( Math::Matrix44::IDENTITY )
	{
		_NeedUpdateParent();
	}
	//--------------------------------------------------------------
	CnTransformable::CnTransformable( const wchar* strName, ushort nIndex )
		: m_strName( strName )
		, m_nIndex( nIndex )
		, m_pParent( 0 )
		, m_bMatrixOutOfDate( false )
		, m_bUpdateParent( false )
		, m_bParentNotified( false )
		, m_bUpdateRotationFromParent( true )
		, m_bUpdateScaleFromParent( true )
		, m_vLocalPosition( Math::Vector3::ZERO )
		, m_qLocalRotation( Math::Quaternion::IDENTITY )
		, m_vLocalScale( Math::Vector3::UNIT )
		, m_vWorldPosition( Math::Vector3::ZERO )
		, m_qWorldRotation( Math::Quaternion::IDENTITY )
		, m_vWorldScale( Math::Vector3::UNIT )
		, m_mWorldXForm( Math::Matrix44::IDENTITY )
	{
		_NeedUpdateParent();
	}

	//--------------------------------------------------------------
	CnTransformable::~CnTransformable()
	{
	}

	//--------------------------------------------------------------
	void CnTransformable::_SetParent( CnNode* pParent )
	{
		m_pParent = pParent;
		m_bParentNotified = false;

		NeedUpdateParent();
	}

	//================================================================
	/** Update From Parent Node
	    @remarks      부모 노드의 결과로 노드의 변환값을 구한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnTransformable::_UpdateFromParent() const
	{
		//if( m_pParent )
		//{
		//	Math::Vector3 parent_position = m_pParent->GetWorldPosition();
		//	Math::Quaternion parent_rotation = m_pParent->GetWorldRotation();
		//	Math::Vector3 parent_scale = m_pParent->GetWorldScale();

		//	if( m_bUpdateRotationFromParent )
		//	{
		//		m_qWorldRotation.Multiply( m_qLocalRotation, parent_rotation );
		//		m_vWorldPosition.Rotate( m_vLocalPosition, parent_rotation );
		//	}
		//	else
		//	{
		//		m_qWorldRotation = m_qLocalRotation;
		//		m_vWorldPosition.Rotate( m_vLocalPosition, m_qLocalRotation );
		//	}

		//	m_vWorldPosition.Multiply( parent_scale );
		//	m_vWorldPosition += parent_position;

		//	m_vWorldScale = m_vLocalScale;
		//	if( m_bUpdateScaleFromParent )
		//	{
		//		m_vWorldScale.Multiply( parent_scale );
		//	}
		//}
		//else
		//{
		//	m_vWorldPosition = m_vLocalPosition;
		//	m_qWorldRotation = m_qLocalRotation;
		//	m_vWorldScale = m_vLocalScale;
		//}
		if( m_pParent )
		{
			Math::Vector3 parent_position = m_pParent->GetWorldPosition();
			Math::Quaternion parent_rotation = m_pParent->GetWorldRotation();
			Math::Vector3 parent_scale = m_pParent->GetWorldScale();

			if( m_bUpdateRotationFromParent )
			{
				//m_qWorldRotation.Multiply( parent_rotation, m_qLocalRotation );
				m_qWorldRotation.Multiply( m_qLocalRotation, parent_rotation );
			}
			else
			{
				m_qWorldRotation = m_qLocalRotation;
			}

			m_vWorldScale = m_vLocalScale;
			if( m_bUpdateScaleFromParent )
			{
				m_vWorldScale.Multiply( parent_scale );
			}

			Math::Vector3 tmpPos = parent_scale;
			tmpPos.Multiply( m_vLocalPosition );
			m_vWorldPosition.Rotate( tmpPos, parent_rotation );
			m_vWorldPosition += parent_position;
		}
		else
		{
			m_vWorldPosition = m_vLocalPosition;
			m_qWorldRotation = m_qLocalRotation;
			m_vWorldScale = m_vLocalScale;
		}

		m_bUpdateParent = false;
		m_bMatrixOutOfDate = true;
	}

	//================================================================
	/** Get World Position
	    @remarks      현재 노드의 월드 상의 위치를 구한다.
		@param        none
		@return       Math::Vector3 : m_vWorldPosition
	*/
	//================================================================
	void CnTransformable::_NeedUpdateParent()
	{
		m_bUpdateParent = true;
		m_bMatrixOutOfDate = true;

		if( m_pParent && !m_bParentNotified )
		{
			m_pParent->RequestUpdate( this );
			m_bParentNotified = true;
		}
	}

	//================================================================
	/** Get World Position
	    @remarks      현재 노드의 월드 상의 위치를 구한다.
		@param        none
		@return       Math::Vector3 : m_vWorldPosition
	*/
	//================================================================
	const Math::Vector3& CnTransformable::GetWorldPosition() const
	{
		if( m_bUpdateParent )
		{
			_UpdateFromParent();
		}
		return m_vWorldPosition;
	}

	//================================================================
	/** Get World Rotation
	    @remarks      현재 노드의 월드 상의 회전 정보를 구한다.
		@param        none
		@return       Math::Quaternion : m_qWorldRotation
	*/
	//================================================================
	const Math::Quaternion& CnTransformable::GetWorldRotation() const
	{
		if( m_bUpdateParent )
		{
			_UpdateFromParent();
		}
		return m_qWorldRotation;
	}

	//================================================================
	/** Get World Scale
	    @remarks      현재 노드의 월드 상의 스케일을 구한다.
		@param        none
		@return       Math::Vector3 : m_vWorldScale
	*/
	//================================================================
	const Math::Vector3& CnTransformable::GetWorldScale() const
	{
		if( m_bUpdateParent )
		{
			_UpdateFromParent();
		}
		return m_vWorldScale;
	}

	//================================================================
	/** Set Local Position
		@remarks      위치를 지정한다.(로컬)
		@param        vPosition : 위치
		@return       none
	*/
	//================================================================
	void CnTransformable::SetPosition( const Math::Vector3& vPosition )
	{
		m_vLocalPosition = vPosition;

		NeedUpdateParent();
	}
	
	//================================================================
	/** Set Local Rotation
		@remarks      회전값을 지정한다.(로컬)
		@param        qRotation : 회전값
		@return       none
	*/
	//================================================================
	void CnTransformable::SetRotation( const Math::Quaternion& qRotation )
	{
		m_qLocalRotation = qRotation;

		NeedUpdateParent();
	}
	
	//================================================================
	/** Set Local Scale
		@remarks      스케일을 지정한다.(로컬)
		@param        vScale : 크기값
		@return       none
	*/
	//================================================================
	void CnTransformable::SetScale( const Math::Vector3& vScale )
	{
		m_vLocalScale = vScale;

		NeedUpdateParent();
	}

	//================================================================
	/** GetWorldXForm
		@remarks      이동
		@param        vDelta : 이동거리
		@param		  eCoord : 기준 좌표계
		@return       none
	*/
	//================================================================
	const Math::Matrix44& CnTransformable::GetWorldXForm() const
	{
		if (m_bMatrixOutOfDate)
		{
			m_mWorldXForm.Build( GetWorldPosition(), GetWorldRotation(), GetWorldScale() );
			m_bMatrixOutOfDate = false;
		}

		return m_mWorldXForm;
	}

	//================================================================
	/** Translate
		@remarks      이동
		@param        vDelta : 이동거리
		@param		  eCoord : 기준 좌표계
		@return       none
	*/
	//================================================================
	void CnTransformable::Translate( const Math::Vector3& vDelta, CnTransformable::COORD_SPACE eCoord )
	{
		switch( eCoord )
		{
		case CS_LOCAL:
			{
				Math::Vector3 vec;
				vec.Rotate( vDelta, m_qLocalRotation );
				m_vLocalPosition += vec;
			}
			break;

		case CS_PARENT:
			m_vLocalPosition += vDelta;
			break;

		case CS_WORLD:
            if( m_pParent )
			{
				Math::Quaternion inv;
				m_pParent->GetWorldRotation().Inverse( inv );

				Math::Vector3 vec;
				vec.Rotate( vDelta, inv );
				m_vLocalPosition += vec;
			}
			else
			{
				m_vLocalPosition += vDelta;
			}
			break;

		default:
			CnPrint( L"[CnTransformable::Translate] Invalid 'COORD_SPCAE'" );
			break;
		}

		NeedUpdateParent();
	}

	//================================================================
	/** Translate
		@remarks      이동
		@param		  mAxis : 기준 축
		@param        vDelta : 이동거리
		@param		  eCoord : 기준 좌표계
		@return       none
	*/
	//================================================================
	void CnTransformable::Translate( const Math::Matrix44& mAxis, const Math::Vector3& vDelta, CnTransformable::COORD_SPACE eCoord )
	{
		Math::Vector3 result;
		result.Multiply( vDelta, mAxis );

		Translate( result, eCoord );
	}

	//================================================================
	/** Rotate
		@remarks      이동
		@param		  mAxis : 기준 축
		@param        vDelta : 이동거리
		@param		  eCoord : 기준 좌표계
		@return       none
	*/
	//================================================================
	void CnTransformable::Rotate( const Math::Quaternion& qRotate, CnTransformable::COORD_SPACE eCoord )
	{
		switch( eCoord )
		{
		case CS_LOCAL:
			m_qLocalRotation.Multiply( m_qLocalRotation, qRotate );
			//D3DXQuaternionMultiply( &m_qLocalRotation, &m_qLocalRotation, &qRotate );
			break;

		case CS_PARENT:
			m_qLocalRotation.Multiply( qRotate, m_qLocalRotation );
			//D3DXQuaternionMultiply( &m_qLocalRotation, &qRotate, &m_qLocalRotation );
			break;

		case CS_WORLD:
			{
				Math::Quaternion inv, tmp;
				GetWorldRotation().Inverse( inv );
				tmp.Multiply( qRotate, GetWorldRotation() );
				tmp.Multiply( inv, tmp );
				m_qLocalRotation.Multiply( m_qLocalRotation, tmp );
				//D3DXQuaternionInverse( &inv, &(GetWorldRotation()) );
				//D3DXQuaternionMultiply( &tmp, &qRotate, &(GetWorldRotation()) );
				//D3DXQuaternionMultiply( &tmp, &inv, &tmp );
				//D3DXQuaternionMultiply( &m_qLocalRotation, &m_qLocalRotation, &tmp );
			}
			break;

		default:
			CnPrint( L"[CnTransformable::Rotate] Invalid 'COORD_SPCAE'" );
			break;
		}

		NeedUpdateParent();
	}

	//================================================================
	/** Yaw
		@remarks      Y 축 기준 회전
		@param		  fRadian : 회전 각도(라디안)
		@param		  eCoord : 기준 좌표계
		@return       none
	*/
	//================================================================
	void CnTransformable::Yaw( float fRadian, CnTransformable::COORD_SPACE eCoord )
	{
		Math::Quaternion rot( Math::Vector3::UNIT_Y, fRadian );
		Rotate( rot, eCoord );
	}

	//================================================================
	/** Roll
		@remarks      Z 축 기준 회전
		@param		  fRadian : 회전 각도(라디안)
		@param		  eCoord : 기준 좌표계
		@return       none
	*/
	//================================================================
	void CnTransformable::Roll( float fRadian, CnTransformable::COORD_SPACE eCoord )
	{
		Math::Quaternion rot( Math::Vector3::UNIT_Z, fRadian );
		Rotate( rot, eCoord );
	}

	//================================================================
	/** Pitch
		@remarks      X 축 기준 회전
		@param		  fRadian : 회전 각도(라디안)
		@param		  eCoord : 기준 좌표계
		@return       none
	*/
	//================================================================
	void CnTransformable::Pitch( float fRadian, CnTransformable::COORD_SPACE eCoord )
	{
		Math::Quaternion rot( Math::Vector3::UNIT_X, fRadian );
		Rotate( rot, eCoord );
	}

	//================================================================
	/** Scale
		@remarks      크기 변환
		@param		  vScale : 스케일 값
		@return       none
	*/
	//================================================================
	void CnTransformable::Scale( const Math::Vector3& vScale )
	{
		m_vLocalScale.Multiply( vScale );

		NeedUpdateParent();
	}

	//================================================================
	/** Need Update
		@remarks      업데이트 요청
		@param        none
		@return       none
	*/
	//================================================================
	void CnTransformable::NeedUpdateParent()
	{
		_NeedUpdateParent();
	}

	//================================================================
	/** @brief	부모로 부터 회전값 적용 여부 */
	//================================================================
	void CnTransformable::SetInheritRotation( bool Enable )
	{
		m_bUpdateRotationFromParent = Enable;
		NeedUpdateParent();
	}
	bool CnTransformable::GetInheritRotation() const
	{
		return m_bUpdateRotationFromParent;
	}

	//================================================================
	/** @brief	부모로 부터 스케일값 적용 여부 */
	//================================================================
	void CnTransformable::SetInheritScale( bool Enable )
	{
		m_bUpdateScaleFromParent = Enable;
		NeedUpdateParent();
	}
	bool CnTransformable::GetInheritScale() const
	{
		return m_bUpdateScaleFromParent;
	}

	//================================================================
	/** Update Transform
		@remarks      월드 변환 업데이트
		@param        bUpdateChildren : 자식들 업데이트 여부
		@param        bChangedParent : 부모 노드 업데이트 여부
		@return       none
	*/
	//================================================================
	void CnTransformable::UpdateTransform( bool bUpdateChildren, bool bChangedParent )
	{
		m_bParentNotified = false;

		if( !bChangedParent && !m_bUpdateParent )
			return;

		if( m_bUpdateParent || bChangedParent )
			_UpdateFromParent();
	}
}