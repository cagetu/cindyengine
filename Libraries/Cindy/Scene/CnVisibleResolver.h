//================================================================
// File:           : CnVisibleResolver.h
// Original Author : changhee
// Creation Date   : 2010. 1. 9
//================================================================
#ifndef __CN_VISIBLE_RESOLVER_H__
#define __CN_VISIBLE_RESOLVER_H__

#include "CnVisibleSet.h"
#include "Util/CnUnCopyable.h"
#include "Util/CnSingleton.h"

namespace Cindy
{
	//==================================================================
	/** Visible Resolver
		@author		cagetu
		@since		2010년 1월 8일
		@brief		VisibleSet에 대한 관리
	*/
	//==================================================================
	class CnVisibleResolver
	{
		__DeclareUnCopy(CnVisibleResolver);
		__DeclareSingleton(CnVisibleResolver);
	public:
		CnVisibleResolver();
		~CnVisibleResolver();

		/// Visible Set
		void						SetVisibleSet( const Ptr<CnVisibleSet>& visibleSet );
		const Ptr<CnVisibleSet>&	GetVisibleSet() const;

		/// 
		void						Clear();

	private:
		//-------------------------------------------------------------------------
		//	Methods
		//-------------------------------------------------------------------------
		Ptr<CnVisibleSet>		m_VisibleSet;
	};

	//-------------------------------------------------------------------------
	/**
	*/
	inline
	void CnVisibleResolver::SetVisibleSet( const Ptr<CnVisibleSet>& visibleSet )
	{
		m_VisibleSet = visibleSet;
	}

	//-------------------------------------------------------------------------
	/**
	*/
	inline
	const Ptr<CnVisibleSet>& CnVisibleResolver::GetVisibleSet() const
	{
		return m_VisibleSet;
	}

} // namespace Cindy

#define VisibleResolver		Cindy::CnVisibleResolver::Instance()

#endif	// __CN_VISIBLE_RESOLVER_H__