//================================================================
// File:               : CnVisibleSet.cpp
// Related Header File : CnVisibleSet.h
// Original Author     : changhee
// Creation Date       : 2008. 5. 20
//================================================================
#include "../Cindy.h"
#include "CnVisibleSet.h"

#include "Lighting/CnLight.h"
#include "Geometry/CnMesh.h"

namespace Cindy
{
	__ImplementClass(Cindy, CnVisibleSet, CnObject);
	//----------------------------------------------------------------
	CnVisibleSet::CnVisibleSet()
	{
	}
	CnVisibleSet::~CnVisibleSet()
	{
		TypeEntityIter i, iend;
		iend = m_TypeEntities.end();
		for (i=m_TypeEntities.begin(); i!=iend; ++i)
		{
			i->second->clear();
			delete (i->second);
		}
		m_TypeEntities.clear();
	}

	//----------------------------------------------------------------
	/** @brief	타입 찾기 */
	//----------------------------------------------------------------
	CnVisibleSet::EntityType CnVisibleSet::GetType( CnSceneEntity* pEntity )
	{
		if (CnLight::RTTI == pEntity->GetRTTI())
		{
			return Light;
		}
		else //if (CnMesh::RTTI == pEntity->GetRTTI())
		{
			//CnMesh* mesh = DynamicCast<CnMesh>( pEntity );
			//if (mesh->IsShadowCaster())
			//	return ShadowCaster;

			return Model;
		}

		Assert( false, "Invalid EntityType" );
		return None;
	}

	//----------------------------------------------------------------
	/** @brief	등록 */
	//----------------------------------------------------------------
	void CnVisibleSet::Register( CnSceneEntity* pEntity )
	{
		EntityType type = GetType( pEntity );
		if (None == type)
			return;

		//@del by cagetu - 10/01/12 : CnMesh::Update로 이동. VisibleSet은 처리와 독립적이어야 한다.
		//pEntity->ClearRelation();

		TypeEntityIter iter = m_TypeEntities.find( type );
		if (iter == m_TypeEntities.end())
		{
			EntityArray* entityList = new EntityArray;
			entityList->reserve( 64 );
			entityList->push_back( pEntity );

			m_TypeEntities.insert( TypeEntityMap::value_type( type, entityList ) );
		}
		else
		{
			EntityArray* entityList = iter->second;
			entityList->push_back( pEntity );
		}
	}

	//----------------------------------------------------------------
	/** @brief	모든 Entity들 제거
				매 프레임 반복될것이므로, 삭제를 하지는 않는다.
	*/
	//----------------------------------------------------------------
	void CnVisibleSet::Clear()
	{
		TypeEntityIter i, iend;
		iend = m_TypeEntities.end();
		for (i=m_TypeEntities.begin(); i!=iend; ++i)
			i->second->clear();
	}

	//----------------------------------------------------------------
	/** @brief	비어 있는가? */
	//----------------------------------------------------------------
	bool CnVisibleSet::IsEmpty() const
	{
		return m_TypeEntities.empty();
	}

	//----------------------------------------------------------------
	/** @brief	한 단위 타입의 Entity들을 얻어온다 */
	//----------------------------------------------------------------
	//const CnVisibleSet::EntityArray* CnVisibleSet::GetEntities( CnVisibleSet::EntityType eType )
	CnVisibleSet::EntityArray* CnVisibleSet::GetEntities( CnVisibleSet::EntityType eType )
	{
		TypeEntityIter iter = m_TypeEntities.find( eType );
		if (iter==m_TypeEntities.end())
			return NULL;

		return iter->second;
		//return m_TypeEntities[eType];
	}
}	// end of namespace