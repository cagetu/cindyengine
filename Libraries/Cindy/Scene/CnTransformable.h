//================================================================
// File:           : CnTransformable.h
// Original Author : changhee
// Creation Date   : 2007. 1. 26
//================================================================
#ifndef __CN_TRANSFORMABLE_H__
#define __CN_TRANSFORMABLE_H__

#include "../Foundation/CnObject.h"
#include "../Math/CindyMath.h"

namespace Cindy
{
	class CnNode;

	//class CnTransformable;
	//typedef Ptr<CnTransformable>	XFormPtr;

	//==================================================================
	/** CnTransformable
		@author
			cagetu
		@since
			2006년 11월 29일
		@remarks
			변환을 사용할 수 있다.
	*/
	//==================================================================
	class CN_DLL CnTransformable : public CnObject
	{
		__DeclareRtti;
	public:
		enum COORD_SPACE
		{
			CS_LOCAL = 0,
			CS_PARENT,
			CS_WORLD,
		};
	
		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnTransformable();
		CnTransformable( const wchar* strName, ushort nIndex );
		virtual ~CnTransformable();

		// Node ID / Name
		void					SetID( ushort ID );
		void					SetName( const CnString& Name );

		ushort					GetID() const;
		const CnString&			GetName() const;

		// Parent
		CnNode*					GetParent() const;

		// Get Coordination
		const Math::Vector3&	GetLocalPosition() const;
		const Math::Quaternion&	GetLocalRotation() const;
		const Math::Vector3&	GetLocalScale() const;

		const Math::Vector3&	GetWorldPosition() const;
		const Math::Quaternion&	GetWorldRotation() const;
		const Math::Vector3&	GetWorldScale() const;

		// Set Coordinatiion
		void					SetPosition( const Math::Vector3& vPosition );
		void					SetRotation( const Math::Quaternion& qRotation );
		void					SetScale( const Math::Vector3& vScale );

		// Transform
		const Math::Matrix44&	GetWorldXForm() const;

		// Translate
		void					Translate( const Math::Vector3& vDelta, COORD_SPACE eCoord = CS_PARENT );
		void					Translate( const Math::Matrix44& mAxis, const Math::Vector3& vDelta, COORD_SPACE eCoord = CS_PARENT );

		// Rotate
		void					Rotate( const Math::Quaternion& qRotate, COORD_SPACE eCoord = CS_LOCAL );
		void					Yaw( float fRadian, COORD_SPACE eCoord = CS_LOCAL );
		void					Roll( float fRadian, COORD_SPACE eCoord = CS_LOCAL );
		void					Pitch( float fRadian, COORD_SPACE eCoord = CS_LOCAL );

		// Scale
		void					Scale( const Math::Vector3& vScale );

		// Update
		virtual void			NeedUpdateParent();
		virtual void			UpdateTransform( bool bUpdateChildren, bool bChangedParent );

		//
		virtual void			SetInheritRotation( bool Enable );
		virtual bool			GetInheritRotation() const;

		virtual void			SetInheritScale( bool Enable );
		virtual bool			GetInheritScale() const;
	protected:
		//------------------------------------------------------
		// Variables
		//------------------------------------------------------
		CnString				m_strName;			//!< 노드 이름
		ushort					m_nIndex;			//!< 노드 인덱스

		CnNode*					m_pParent;			//!< 부모 노드

		// Local coordination
		Math::Vector3			m_vLocalPosition;	//!< 자신의 축 기준 위치값
		Math::Quaternion		m_qLocalRotation;	//!< 자신의 축 기준 회전값
		Math::Vector3			m_vLocalScale;		//!< 자신의 축 기준 크기값

		// World coordination
		mutable Math::Vector3	m_vWorldPosition;	//!< 월드 축 기준 위치값
		mutable Math::Quaternion m_qWorldRotation;	//!< 월드 축 기준 회전값
		mutable Math::Vector3	m_vWorldScale;		//!< 월드 축 기준 크기값

		mutable Math::Matrix44	m_mWorldXForm;		//!< 월드 변환 행렬
		mutable bool			m_bMatrixOutOfDate;	//!< 변환 행렬 갱신 여부

		mutable bool			m_bUpdateParent;	//!< 부모 노드 업데이트 여부
		mutable bool			m_bParentNotified;	//!< 부모에게 업데이트 요청

		bool					m_bUpdateRotationFromParent;
		bool					m_bUpdateScaleFromParent;

		//------------------------------------------------------
		// Methods
		//------------------------------------------------------
		void				_SetParent( CnNode* pParent );

		void				_NeedUpdateParent();
		virtual void		_UpdateFromParent() const;
	};

#include "CnTransformable.inl"
}

#endif	// __CN_TRANSFORMABLE_H__