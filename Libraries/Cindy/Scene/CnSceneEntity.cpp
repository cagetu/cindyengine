//================================================================
// File:               : CnSceneEntity.cpp
// Related Header File : CnSceneEntity.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 26
//================================================================
#include "../Cindy.h"
#include "CnSceneEntity.h"
#include "CnSceneNode.h"
#include "CnCamera.h"

namespace Cindy
{
	//============================================================================
	__ImplementRtti( Cindy, CnSceneEntity, CnObject );

	/// Const/Dest
	CnSceneEntity::CnSceneEntity( const CnString& strName )
		: m_strName( strName )
		, m_bVisible( true )
		, m_bCulled( false )
		, m_bCastShadow( false )
		, m_bReceiveShadow( false )
		, m_bEnableReceiveShadow( true )
		, m_pParent(0)
	{
	}
	CnSceneEntity::~CnSceneEntity()
	{
	}

	//================================================================
	/** Set Parent Node
	    @remarks      
		@param        none
		@return       none
	*/
	//================================================================
	void CnSceneEntity::SetParent( CnSceneNode* pParent )
	{
		m_pParent = pParent;

		if (IsCastShadow() && m_pParent)
		{
#pragma TODO( "그림자 관련해서는 추후에 다시 체크" )
			m_pParent->EnableCulling( false );
		}
	}
	CnSceneNode* CnSceneEntity::GetParent() const
	{
		return m_pParent;
	}

	//================================================================
	/** Get World Position
	    @remarks      현재 Entity의 월드 상의 위치를 구한다.
		@param        none
		@return       Math::Vector3 : m_vWorldPosition
	*/
	//================================================================
	const Math::Vector3& CnSceneEntity::GetWorldPosition() const
	{
		return GetParent()->GetWorldPosition();
	}

	//================================================================
	/** Get World Rotation
	    @remarks      현재 Entity의 월드 상의 회전 정보를 구한다.
		@param        none
		@return       Math::Quaternion : m_qWorldRotation
	*/
	//================================================================
	const Math::Quaternion& CnSceneEntity::GetWorldRotation() const
	{
		return GetParent()->GetWorldRotation();
	}

	//================================================================
	/** Get World Scale
	    @remarks      현재 Entity의 월드 상의 스케일을 구한다.
		@param        none
		@return       Math::Vector3 : m_vWorldScale
	*/
	//================================================================
	const Math::Vector3& CnSceneEntity::GetWorldScale() const
	{
		return GetParent()->GetWorldScale();
	}

	//================================================================
	/** @brief	관련 있는 있는 객체와 연결 */
	//================================================================
	//void CnSceneEntity::Link( CnSceneEntity* pEntity )
	//{
	//	m_Links.push_back( pEntity );
	//}

	//================================================================
	/** @brief	관련성 해제 */
	//================================================================
	//void CnSceneEntity::UnlinkAll()
	//{
	//	m_Links.clear();
	//}
}