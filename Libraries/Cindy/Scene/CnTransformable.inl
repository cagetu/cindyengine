//================================================================
// File:               : CnTransformable.inl
// Related Header File : CnTransformable.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 25
//================================================================

//--------------------------------------------------------------
inline
void CnTransformable::SetName( const CnString& Name )
{
	m_strName = Name;
}
inline
const CnString& CnTransformable::GetName() const
{
	return m_strName;
}

//--------------------------------------------------------------
inline
void CnTransformable::SetID( ushort ID )
{
	m_nIndex = ID;
}
inline
ushort CnTransformable::GetID() const
{
	return m_nIndex;
}

//--------------------------------------------------------------
inline
CnNode* CnTransformable::GetParent() const
{
	return m_pParent;
}

//--------------------------------------------------------------
/** Get Local Position
    @brief      로컬 상의 위치를 구한다.
	@param      none
	@return     CnVector : m_vLocalPosition
*/
//--------------------------------------------------------------
inline
const Math::Vector3& CnTransformable::GetLocalPosition() const
{
	return m_vLocalPosition;
}

//--------------------------------------------------------------
/** Get Local Rotation
    @brief      로컬 상의 회전값를 구한다.
	@param		none
	@return     Math::Quaternion : m_qLocalRotation
*/
//--------------------------------------------------------------
inline
const Math::Quaternion& CnTransformable::GetLocalRotation() const
{
	return m_qLocalRotation;
}

//--------------------------------------------------------------
/** Get Local Scale
    @brief		로컬 상의 스케일를 구한다.
	@param      none
	@return     CnVector : m_vLocalScale
*/
//--------------------------------------------------------------
inline
const Math::Vector3& CnTransformable::GetLocalScale() const
{
	return m_vLocalScale;
}
