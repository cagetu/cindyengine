//================================================================
// File:               : CnVisibleResolver.cpp
// Related Header File : CnVisibleResolver.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 8
//================================================================
#include "Cindy.h"
#include "CnVisibleResolver.h"

namespace Cindy
{
	__ImplementSingleton(CnVisibleResolver);
	//-------------------------------------------------------------------------
	/** Constructor
	*/
	CnVisibleResolver::CnVisibleResolver()
	{
		__ConstructSingleton;
	}

	//-------------------------------------------------------------------------
	/**	Destructor
	*/
	CnVisibleResolver::~CnVisibleResolver()
	{
		__DestructSingleton;
	}

	//-------------------------------------------------------------------------
	/**
	*/
	void CnVisibleResolver::Clear()
	{
		m_VisibleSet = 0;
	}

} // namespace Cindy