//================================================================
// File:               : CnModelNode.cpp
// Related Header File : CnModelNode.h
// Original Author     : changhee
// Creation Date       : 2007. 4. 16
//================================================================
#include "../Cindy.h"
#include "CnModelNode.h"

#include "Geometry/CnMesh.h"
#include "Geometry/CnSubMesh.h"
#include "Geometry/CnMeshDataManager.h"

#include "Material/CnRenderState.h"
#include "Material/CnTextureState.h"
#include "Material/CnMaterialDataManager.h"

#include "Animation/CnBoneNode.h"
#include "Animation/CnSkeletonInstance.h"
#include "Animation/CnSkeletonDataManager.h"
#include "Animation/CnAnimationDataManager.h"

#include "Util/CnLog.h"

namespace Cindy
{
	//================================================================
	__ImplementClass( Cindy, CnModelNode, CnSceneNode );

	/// Const/Dest
	CnModelNode::CnModelNode()
	{
	}
	//----------------------------------------------------------------
	CnModelNode::CnModelNode( const wchar* strName, ushort nIndex )
		: CnSceneNode( strName, nIndex )
	{
	}
	//----------------------------------------------------------------
	CnModelNode::~CnModelNode()
	{
		RemveAllMeshes();
		RemoveSkeleton();
	}

	//----------------------------------------------------------------
	/** @brief	메쉬 추가
	*/
	//----------------------------------------------------------------
	bool CnModelNode::AddMesh( const CnString& MeshName, const CnString& MtlName, bool CastShadow )
	{
		MeshDataIter iter = m_MeshDatas.find( MeshName );
		if (iter != m_MeshDatas.end())
			return false;

		MeshDataPtr spMesh = MeshDataMgr->Load( MeshName.c_str() );
		if (spMesh.IsNull())
		{
			CnError( L"[CnModelNode::AddMesh] Failed" );
			return false;
		}

		MtlDataPtr spMaterial = MaterialDataMgr->Load( MtlName.c_str() );
		if (spMaterial.IsNull())
		{
			CnError( L"[CnModelNode::AddMesh] Failed" );
			return false;
		}

		if (false == _BuildMeshNodes( spMesh, spMaterial, CastShadow ))
		{
			CnError( L"[CnModelNode::AddMesh] '_BuildMeshNodes' Failed" );
			return false;
		}

		m_MeshDatas.insert( MeshDataList::value_type( MeshName, spMesh ) );
		return true;
	}

	//----------------------------------------------------------------
	/** @brief	메쉬 제거하기 */
	//----------------------------------------------------------------
	bool CnModelNode::RemoveMesh( const CnString& MeshName )
	{
		MeshDataIter iter = m_MeshDatas.find( MeshName );
		if (iter == m_MeshDatas.end())
			return false;

		_ClearMeshNodes( iter->second );
		m_MeshDatas.erase( iter );
		return true;
	}

	
	//----------------------------------------------------------------
	/** @brief	모든 메쉬 해제
	*/
	//----------------------------------------------------------------
	void CnModelNode::RemveAllMeshes()
	{
		MeshDataIter iend = m_MeshDatas.end();
		for (MeshDataIter i = m_MeshDatas.begin(); i != iend; ++i)
			_ClearMeshNodes( i->second );

		m_MeshDatas.clear();
	}

	//----------------------------------------------------------------
	/** Find Mesh
	    @brief      메쉬 찾기
	*/
	//----------------------------------------------------------------
	bool CnModelNode::HasMesh( const CnString& MeshName ) const
	{
		MeshDataList::const_iterator iter = m_MeshDatas.find( MeshName );
		if (iter == m_MeshDatas.end())
			return false;

		return true;
	}

	//----------------------------------------------------------------
	/** Build MeshNodes
	    @brief      메쉬 노드들 생성
		@param        spMesh : 메쉬 데이터
		@return       true/false 
	*/
	//----------------------------------------------------------------
	bool CnModelNode::_BuildMeshNodes( const MeshDataPtr& spMesh, const MtlDataPtr& spMaterial, bool bCastShadow )
	{
		const CnMeshData::MeshNodeArray nodedatas = spMesh->GetMeshNodeDatas();

		CnSceneNode* meshnode = NULL;
		Export::MeshNode* nodedata = NULL;
		CnNode* parent = NULL;

		CnMeshData::MeshNodeArray::const_iterator i, iend;
		iend = nodedatas.end();
		for( i = nodedatas.begin(); i != iend; ++i )
		{
			nodedata = (*i);

			//CnPrint( L"[CnModelNode::_BuildMeshNodes]: %s", nodedata->name.c_str() );

			// set nodes
			if (nodedata->HasParent())
			{
				parent = this->GetChild( nodedata->parent_name, true );
				if (!parent)
					continue;

				meshnode = CnNew CnSceneNode( nodedata->name.c_str(), nodedata->id );
				if (false == parent->AttachChild( meshnode ))
				{
					delete meshnode;
					continue;
				}
			}
			else
			{
				meshnode = CnNew CnSceneNode( nodedata->name.c_str(), nodedata->id );
				if (false == AttachChild( meshnode ))
				{
					delete meshnode;
					continue;
				}
			}

			bool isSkinned = nodedata->IsSkinned();

			// position, scale, rotation.
			meshnode->SetPosition( nodedata->local_pos );
			meshnode->SetRotation( nodedata->local_rot );
			meshnode->SetScale( nodedata->local_scl );

			// culling enable.
			meshnode->EnableCulling( !isSkinned );

			if (!_BuildMeshEntities( meshnode, nodedata, spMaterial, bCastShadow ))
				continue; 
		}

		return true;
	}

	//----------------------------------------------------------------
	/** Clear MeshNodes
	    @brief      메쉬 노드들 해제
		@param        spMesh : 메쉬 데이터
		@return       true/false 
	*/
	//----------------------------------------------------------------
	bool CnModelNode::_ClearMeshNodes( const MeshDataPtr& spMesh )
	{
		if (spMesh.IsNull())
			return false;

		const CnMeshData::MeshNodeArray nodedatas = spMesh->GetMeshNodeDatas();

		Export::MeshNode* nodedata = NULL;
		CnNode* parent = NULL;

		CnMeshData::MeshNodeArray::const_iterator i, iend;
		iend = nodedatas.end();
		for (i = nodedatas.begin(); i != iend; ++i)
		{
			nodedata = (*i);

			// set nodes
			if (nodedata->HasParent())
			{
				parent = this->GetChild( nodedata->parent_name );
				if (!parent)
					continue;

				parent->DeleteChild( nodedata->name );
			}
			else
			{
				DeleteChild( nodedata->name );
			}
		}

		return true;
	}

	//----------------------------------------------------------------
	/** Clear MeshNodes
	    @brief      메쉬 노드들 해제
		@param        spMesh : 메쉬 데이터
		@return       true/false 
	*/
	//----------------------------------------------------------------
	bool CnModelNode::_BuildMeshEntities( CnSceneNode* pMeshNode, 
										  Export::MeshNode* pMeshNodeData,
										  const MtlDataPtr& spMaterial,
										  bool bCastShadow )
	{
		CnMesh* mesh = pMeshNodeData->CreateInstance();
		mesh->SetMaterial( spMaterial );
		mesh->SetSkeleton( m_Skeleton );
		mesh->SetCastShadow( bCastShadow );

		return pMeshNode->AttachEntity( mesh );
	}

	//----------------------------------------------------------------
	/** Build Skeleton
	    @brief      Skeleton 형성하기
		@param        spMesh : 뼈대 데이터
		@return       true/false 
	*/
	//----------------------------------------------------------------
	bool CnModelNode::_BuildSkeleton( const SkelDataPtr& spSkeleton )
	{
		m_Skeleton->Setup( spSkeleton );
		return true;
	}

	//----------------------------------------------------------------
	/** Attach Skeleton
	    @brief      Skeleton 붙이기
		@param        spMesh : 뼈대 데이터
		@return       true/false 
	*/
	//----------------------------------------------------------------
	bool CnModelNode::AddSkeleton( const SkelDataPtr& spSkeleton )
	{
		if (spSkeleton.IsNull())
			return false;

		if (m_Skeleton.IsNull())
		{
			m_Skeleton = CnSkeletonInstance::Create();
			m_Skeleton->SetOwner( this );
		}

		return _BuildSkeleton( spSkeleton );
	}
	//----------------------------------------------------------------
	bool CnModelNode::AddSkeleton( const CnString& strName )
	{
		SkelDataPtr skeleton = SkeletonDataMgr->Load( strName.c_str() );
		return AddSkeleton( skeleton );
	}

	//----------------------------------------------------------------
	/** Detach Skeleton
	    @brief      Skeleton 붙이기
		@return       true/false 
	*/
	//----------------------------------------------------------------
	bool CnModelNode::RemoveSkeleton()
	{
		if (m_Skeleton.IsNull())
			return true;

		//m_Skeleton->RemoveAllNodes( true );
		m_Skeleton->Clear();
		m_Skeleton.SetNull();

		return true;
	}

	//----------------------------------------------------------------
	/** Update Controller
	    @brief      컨트롤러를 업데이트 한다.
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnModelNode::_UpdateAfterCulling()
	{
		if (m_AnimController != NULL)
			m_AnimController->Update();

		if (m_Skeleton != NULL)
			m_Skeleton->Update();

		if (m_IKController != NULL)
			m_IKController->Update();
	}

	//----------------------------------------------------------------
	/** @brief	애니메이션 추가
		@param		Priority : 애니메이션 우선순위
		@param		spData : 데이터
		@return		true/false
	*/
	//----------------------------------------------------------------
	CnAnimChannel* CnModelNode::AddAnimation( PRIORITY Priority, const AnimDataPtr& spData, bool bRepeat, float fBlendWeight, float fBlendRate )
	{
		if (spData.IsNull())
			return NULL;

		if (m_AnimController == NULL)
		{
			m_AnimController = CnAnimController::Create();
			m_AnimController->SetContainer( m_Skeleton );
		}

		Ptr<CnAnimChannel> channel = m_AnimController->GetChannel( Priority );
		if (channel == NULL)
		{
			CnAnimChannel* newChannel = CnAnimChannel::Create();
			m_AnimController->AddChannel( Priority, newChannel );
			channel = newChannel;
		}

		Ptr<CnAnimInstance> instance = spData->CreateInstance();
		channel->SetMotion( instance, bRepeat, fBlendWeight, fBlendRate );

		return channel;
	}

	CnAnimChannel* CnModelNode::AddAnimation(PRIORITY Priority, const wchar* fname, bool bRepeat, float fBlendWeight, float fBlendRate)
	{
		AnimDataPtr spData = AnimDataMgr->Load(fname);
		if (spData.IsNull())
			return NULL;

		if (m_AnimController == NULL)
		{
			m_AnimController = CnAnimController::Create();
			m_AnimController->SetContainer( m_Skeleton );
		}

		Ptr<CnAnimChannel> channel = m_AnimController->GetChannel( Priority );
		if (channel == NULL)
		{
			CnAnimChannel* newChannel = CnAnimChannel::Create();
			m_AnimController->AddChannel( Priority, newChannel );
			channel = newChannel;
		}

		Ptr<CnAnimInstance> instance = spData->CreateInstance();
		channel->SetMotion( instance, bRepeat, fBlendWeight, fBlendRate );

		return channel;
	}

	//----------------------------------------------------------------
	/*@ @brief	애니메이션 제거 */
	//----------------------------------------------------------------
	void CnModelNode::RemoveAnimation( PRIORITY Priority )
	{
		m_AnimController->RemoveChannel( Priority );
	}

	//----------------------------------------------------------------
	/** @brief	IK Controller 설치 */
	//----------------------------------------------------------------
	void CnModelNode::AddIKController( CnIKController* pController )
	{
		m_IKController = pController;
		m_IKController->SetParent( this );
	}

	//----------------------------------------------------------------
	/** Update State
	    @brief      상태 업데이트
		@param        paGSStack : 
		@return       none
	*/
	//----------------------------------------------------------------
	void CnModelNode::UpdateState( std::vector<CnGlobalState*>* paGSStack )
	{
		// update global state
		for (int i=0; i<CnGlobalState::MAX_STATE; ++i)
		{
			CnGlobalState* state = paGSStack[i].back();
			m_States[i] = state;
		}
	}

	//----------------------------------------------------------------
	/** Set CastShadow
	*/
	//----------------------------------------------------------------
	void CnModelNode::SetCastShadow( bool bCast )
	{
		//const EntityMap& entities = this->GetAllEntities();
		//EntityMap::const_iterator im, imend;
		//imend = entities.end();
		//for (im = entities.begin(); im != imend; im++)
		//{
		//}
	}

	//----------------------------------------------------------------
	/** Set Enable Receive Shadow
	*/
	//----------------------------------------------------------------
	void CnModelNode::SetEnableReceiveShadow( bool bEnable )
	{
	}
}