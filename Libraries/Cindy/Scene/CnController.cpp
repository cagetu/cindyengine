//================================================================
// File:               : CnController.cpp
// Related Header File : CnController.h
// Original Author     : changhee
// Creation Date       : 2007. 2. 1
//================================================================
#include "../Cindy.h"
#include "CnNode.h"
#include "CnController.h"

namespace Cindy
{
	//================================================================
	// defines
	__ImplementRtti( Cindy, CnController, CnObject );
	/// Const/Dest
	CnController::CnController()
		: m_pParent( 0 )
	{
	}
	CnController::~CnController()
	{
	}

	//----------------------------------------------------------------
	void CnController::SetParent( CnNode* pParent )
	{
		m_pParent = pParent;
	}
}