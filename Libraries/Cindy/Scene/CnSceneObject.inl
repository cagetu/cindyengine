//================================================================
// File:               : ISceneObject.inl
// Related Header File : ISceneObject.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 26
//================================================================
namespace Cindy
{
	//----------------------------------------------------------------
	inline
	void ISceneObject::DetachAllGlobalStates()
	{
		m_GlobalStates.clear();
	}
	//----------------------------------------------------------------
	inline
	CnGlobalState* ISceneObject::GetGlobalState( CnGlobalState::TYPE eType ) const
	{
		std::vector<GlobalStatePtr>::const_iterator i, iend;
		iend = m_GlobalStates.end();
		for (i=m_GlobalStates.begin(); i!=iend; ++i)
		{
			if ((*i)->GetStateType() == eType)
			{
				return (*i);
			}
		}
		return 0;
	}
	//----------------------------------------------------------------
	inline
	CnGlobalState* ISceneObject::GetGlobalState( uint Index ) const
	{
		return m_GlobalStates[Index];
	}
	//----------------------------------------------------------------
	inline
	unsigned int ISceneObject::GetNumOfGlobalStates() const
	{
		return (unsigned int)m_GlobalStates.size();
	}
}