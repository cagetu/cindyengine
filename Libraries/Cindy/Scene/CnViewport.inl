// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
inline
int CnViewport::GetZOrder() const
{
	return m_nZOrder;
}

//--------------------------------------------------------------
inline
int CnViewport::GetActualTop() const
{
	return m_nActualTop;
}

//--------------------------------------------------------------
inline
int CnViewport::GetActualLeft() const
{
	return m_nActualLeft;
}

//--------------------------------------------------------------
inline
int CnViewport::GetActualWidth() const
{
	return m_nActualWidth;
}

//--------------------------------------------------------------
inline
int CnViewport::GetActualHeight() const
{
	return m_nActualHeight;
}

//--------------------------------------------------------------
inline
void CnViewport::SetDimension( float fRelLeft,
							   float fRelTop,
							   float fRelWidth,
							   float fRelHeight )
{
	m_fRelativeTop = fRelLeft;
	m_fRelativeTop = fRelTop;
	m_fRelativeWidth = fRelWidth;
	m_fRelativeHeight = fRelHeight;

	UpdateDimension();
}

//--------------------------------------------------------------
inline
void CnViewport::SetBackgroundColor( ulong ulColor )
{
	m_ulBackgroundColor = ulColor;
}

//--------------------------------------------------------------
inline
ulong CnViewport::GetBackgroundColor() const
{
	return m_ulBackgroundColor;
}

//--------------------------------------------------------------
inline
void CnViewport::SetScene( const Ptr<CnScene>& pScene )
{
	m_pScene = pScene;
}
