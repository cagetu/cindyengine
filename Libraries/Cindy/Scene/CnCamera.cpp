//================================================================
// File:               : CnCamera.cpp
// Related Header File : CnCamera.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 27
//================================================================
#include "CnCamera.h"
//#include "CnScene.h"

namespace Cindy
{
	#define CN_CAMERA_DEFAULT_FOV				Math::PI / 4
	#define CN_CAMERA_DEFAULT_ASPECT			1.333f
	#define CN_CAMERA_DEFAULT_NEARPLANE			1.0f
	#define CN_CAMERA_DEFAULT_FARPLANE			250.0f

	//==============================================================
	__ImplementRtti( Cindy, CnCamera, CnObject );
	//--------------------------------------------------------------
	CnCamera::CnCamera()
		: m_fNear( CN_CAMERA_DEFAULT_NEARPLANE )
		, m_fFar( CN_CAMERA_DEFAULT_FARPLANE )
		, m_fAspect( CN_CAMERA_DEFAULT_ASPECT )
		, m_fFovY( CN_CAMERA_DEFAULT_FOV )
		, m_fNearWidth(0.0f)
		, m_fNearHeight(0.0f)
		, m_fFarWidth(0.0f)
		, m_fFarHeight(0.0f)
		, m_fFarForFrustum( CN_CAMERA_DEFAULT_FARPLANE )
		, m_bNeedUpdateProjection( true )
		, m_bNeedUpdateView( true )
		, m_bNeedUpdateFrustum( true )
		, m_vUp( 0.0f, 1.0f, 0.0f )
		, m_bPerspective(true)
	{
	}
	CnCamera::CnCamera( const CnString& strName )
		: m_strName( strName )
		//, m_pScene(0)
		, m_fNear( CN_CAMERA_DEFAULT_NEARPLANE )
		, m_fFar( CN_CAMERA_DEFAULT_FARPLANE )
		, m_fAspect( CN_CAMERA_DEFAULT_ASPECT )
		, m_fFovY( CN_CAMERA_DEFAULT_FOV )
		, m_fFarForFrustum( CN_CAMERA_DEFAULT_FARPLANE )
		, m_bNeedUpdateProjection( true )
		, m_bNeedUpdateView( true )
		, m_bNeedUpdateFrustum( true )
		, m_vUp( 0.0f, 1.0f, 0.0f )
		, m_bPerspective(true)
	{
	}
	CnCamera::~CnCamera()
	{
	}

	//================================================================
	/**	UpdateView
		@remarks : View Matrix를 업데이트 한다.
		@param : none
		@return : none
	*/
	//================================================================
	void CnCamera::UpdateView() const
	{
		if( m_bNeedUpdateView )
		{
			m_vLookAt = m_vPosition + Math::Vector3( 0.0f, 0.0f, 1.0f );
			m_mView.LookAtLH( m_vPosition, m_vLookAt, m_vUp );

			m_vDirection = m_vLookAt - m_vPosition;
			m_vDirection.Normalize();

			m_bNeedUpdateView = false;
			m_bNeedUpdateFrustum = true;
		}
	}

	//================================================================
	/**	UpdateProjection
		@remarks : Projection Matrix를 업데이트 한다.
		@param : none
		@return : none
	*/
	//================================================================
	void CnCamera::UpdateProjection() const
	{
		if( m_bNeedUpdateProjection )
		{
			m_mProjection.PerspFovLH( m_fFovY, m_fAspect, m_fNear, m_fFar );

			m_fNearWidth  = 2.0f * m_fNear / m_mProjection._11;
			m_fNearHeight = 2.0f * m_fNear / m_mProjection._22;
			m_fFarWidth   = (m_fNearWidth / m_fNear) * m_fFar;
			m_fFarHeight  = (m_fNearHeight / m_fNear) * m_fFar;

			m_bNeedUpdateFrustum = true;
			m_bNeedUpdateProjection = false;
		}
	}

	//================================================================
	/**	UpdateFrustum
		@remarks : Frustum을 업데이트 한다.
		@param : none
		@return : none
	*/
	//================================================================
	void CnCamera::UpdateFrustum() const
	{
		if( m_bNeedUpdateFrustum )
		{
			//Math::Matrix44 projection;
			//projection.PerspFovLH( m_fFovY, m_fAspect, m_fNear, m_fFarForFrustum );

			//Math::Matrix44 viewProjection;
			//viewProjection.Multiply( m_mView, projection );
			//m_Frustum.BuildPlanes( &viewProjection );

			m_mViewProjection = m_mView * m_mProjection;
			//m_mViewProjection.Multiply(m_mView, m_mProjection);
			m_Frustum.BuildPlanes( &m_mViewProjection );

			m_bNeedUpdateFrustum = false;
		}
	}

	Math::Vector3* CnCamera::GetFrustumPoints() const
	{
		UpdateView();
		UpdateProjection();
		UpdateFrustum();

		return m_Frustum.GetExtrema();
	}

	//================================================================
	/**	IsCulled
		@remarks	차폐 여부 결정
		@param		rBox : AABB(축정렬 충돌상자)
		@return		true/false : 차폐 여부
	*/
	//================================================================
	bool CnCamera::IsCulled( const Math::AABB& rBox ) const
	{
		if( rBox.IsEmpty() )
			return false;

		UpdateView();
		UpdateProjection();
		UpdateFrustum();

		return !m_Frustum.Intersect( rBox );
	}

	//================================================================
	/**	IsCulled
		@remarks	차폐 여부 결정
		@param		rBox : OBB(오브젝트 정렬 충돌상자)
		@return		true/false : 차폐 여부
	*/
	//================================================================
	bool CnCamera::IsCulled( const Math::OBB& rBox ) const
	{
		UpdateView();
		UpdateProjection();
		UpdateFrustum();

		return !m_Frustum.Intersect( rBox );
	}

	//================================================================
	/**	IsCulled
		@remarks	차폐 여부 결정
		@param		rCenter : 중심점
		@param		fRadius : 반지름
		@return		true/false : 차폐 여부
	*/
	//================================================================
	bool CnCamera::IsCulled( const Math::Vector3& rCenter, float fRadius ) const
	{
		UpdateView();
		UpdateProjection();
		UpdateFrustum();

		return !m_Frustum.Intersect( rCenter, fRadius );
	}
	//--------------------------------------------------------------
	bool CnCamera::IsCulled( const Math::Sphere& sphere, const Math::Vector3& sweepDir ) const
	{
		UpdateView();
		UpdateProjection();
		UpdateFrustum();

		return !m_Frustum.Intersect( sphere, sweepDir );
	}
}