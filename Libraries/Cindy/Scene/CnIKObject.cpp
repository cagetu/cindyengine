//================================================================
// File:               : CnIKObject.cpp
// Related Header File : CnIKObject.h
// Original Author     : changhee
//================================================================
#include "../Cindy.h"
#include "CnIKObject.h"
#include "CnNode.h"

namespace Cindy
{
	//================================================================
	__ImplementRtti(Cindy, CnIKObject, CnTransformable);
	//----------------------------------------------------------------
	CnIKObject::CnIKObject()
	{
	}
	CnIKObject::CnIKObject( const wchar* strName, ushort nIndex )
		: CnTransformable( strName, nIndex )
	{
	}
	CnIKObject::~CnIKObject()
	{
	}

	//----------------------------------------------------------------
	/** _UpdateIK
	    @breif	IK 업데이트
		@desc	IK를 계산하고, 그 결과를 적용한다.	 
		@param	GoalPoint : 타겟 위치
		@param	EndEffector : 가장 끝의 작용 점 위치

		<CCD In Action>
			: angle만큼 RE를 RD가 되도록 회전.
		[last link]
			R------->D(Goal)	R------->--> D
			| (angle)					E
			|
			|
			E
		[up link]
			  R		  D			R-->E-->D
			 /				->	| /
			/					|/
		   #---->E				#
		@todo	좌표계를 맞춰줘야 한다. 일단 psudo 형태로 작성해 놓는다.
	*/
	//----------------------------------------------------------------
	bool CnIKObject::_ComputeIK( CnIKController::IKSolver* IKCtrlr, uint& TryCount )
	{
		if (this == IKCtrlr->finish)
			return false;

		Math::Vector3 goalPosition = IKCtrlr->goal->GetWorldPosition();
		Math::Vector3 endEffector = IKCtrlr->endEffector->GetWorldPosition();
		Math::Vector3 distance = goalPosition - endEffector;
		float length = distance.LengthSq();
		if (length > 1.0f)
		{
			Math::Vector3 rootPos = this->GetWorldPosition();

			Math::Vector3 rootToEnd = endEffector - rootPos;
			Math::Vector3 rootToDest = goalPosition - rootPos;

			rootToEnd.Normalize();
			rootToDest.Normalize();

			Radian cosAngle = rootToEnd.Dot( rootToDest );
			if (cosAngle < 0.99999f)
			{
				Radian angle = Math::ACos( cosAngle );
				if (_isnan(angle))
					angle = 0.0f;

				if (angle != 0.0f)
				{
					// damping
					// 부자연스럽게 보이고, 뒤틀림이 생기는 것을 방지하기 위해,
					// 제한된 각도를 넘지 않도록 해준다.
					{
						static const float dampingLimit = 2.0f;
						static const bool enableDamping = false;
						if (enableDamping)
						{
							if (angle > dampingLimit)
								angle = dampingLimit;
						}

						//static float maxAngle = 0.0f;
						//if (maxAngle < angle)
						//{
						//	maxAngle = angle;
						//	CnPrint( L"[ik]maxAngle: %f", maxAngle );
						//}
					}

					Math::Vector3 axis;
					axis.Cross( rootToEnd, rootToDest );
					axis.Normalize();

					Math::Quaternion rot( axis, angle );
					Math::Quaternion myRot = this->GetLocalRotation();

					Math::Quaternion newRot;
					newRot.Multiply( myRot, rot );
					//newRot.Slerp( myRot, newRot, 1.0f );
					this->SetRotation( newRot );

					this->UpdateTransform( true, false );

					// restrictions(DOF(degrees of freedom)을 체크한다.)
					// routine이 joint 회전을 갱신할 때, 테스트로 제한들 밖으로 joint가 있는지를 검사한다.
					// 만약 그렇다면, joint는 그들의 제한된 각도로 clamped 시킨다.
					// 그리고 나머지 joint들은 이 후 step들 동안 문제를 만족시키는데 사용된다.
					static const bool enableDOFRestrict = false;
					if (true == enableDOFRestrict)
					{
						Math::Quaternion rot = this->GetLocalRotation();

						Math::Vector3 euler;
						rot.ToEuler( euler );

						static float maxYaw = Math::DegreeToRadian(30.0f);
						static float minYaw = Math::DegreeToRadian(-30.0f);;		// y
						static float maxRoll = Math::DegreeToRadian(30.0f);
						static float minRoll = Math::DegreeToRadian(-30.0f);		// z
						static float maxPitch = Math::DegreeToRadian(30.0f);
						static float minPitch = Math::DegreeToRadian(-30.0f);	// x

						euler.x = Math::Clamp( euler.x, minPitch, maxPitch );
						euler.y = Math::Clamp( euler.y, minYaw, maxYaw );
						euler.z = Math::Clamp( euler.z, minRoll, maxRoll );

						rot.FromEuler( euler );

						SetRotation( rot );
					}
				}

			}
		}
		else
		{
			return true;
		}

		//endEffector = EndEffector->GetWorldPosition();
		//distance = GoalPoint - endEffector;
		//length = distance.LengthSq();
		//if (length <= 1.0f)
		//	return true;

		if (TryCount++ >= IKCtrlr->maxCCDTries)
			return false;

		return m_pParent->_ComputeIK( IKCtrlr, TryCount );
	}
}
