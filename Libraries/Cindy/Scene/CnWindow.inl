//================================================================
// File:               : CnWindow.inl
// Related Header File : CnWindow.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 23
//================================================================

//================================================================
/** Get Window Handle
	@remarks      윈도우 핸들을 얻는다.
	@param        none
	@return       HWND : 핸들
*/
//================================================================
inline HWND CnWindow::GetHWnd() const
{
	return m_hWnd;
}

//================================================================
/** Get Width
	@remarks      윈도우의 가로 길이를 구한다.
	@param        none
	@return       int : m_nWidth
*/
//================================================================
inline int CnWindow::GetWidth() const
{
	return m_nWidth;
}

//================================================================
/** Get Height
	@remarks      윈도우의 세로 길이를 구한다.
	@param        none
	@return       int : m_nHeight
*/
//================================================================
inline int CnWindow::GetHeight() const
{
	return m_nHeight;
}

//================================================================
/** Get Window Name
	@remarks      윈도우 이름을 얻는다.
	@param        none
	@return       CnString : m_strName
*/
//================================================================
inline const CnString& CnWindow::GetName() const
{
	return m_strName;
}