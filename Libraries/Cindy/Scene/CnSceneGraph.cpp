//================================================================
// File:               : CnSceneGraph.cpp
// Related Header File : CnSceneGraph.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 27
//================================================================
#include "../Cindy.h"
#include "CnSceneGraph.h"
#include "CnSceneNode.h"
#include "CnVisibleSet.h"

namespace Cindy
{
	__ImplementClass(Cindy, CnSceneGraph, CnObject);
	//----------------------------------------------------------------
	/// Const/Dest
	CnSceneGraph::CnSceneGraph( const CnString& strName )
		: m_strName( strName )
	{
		CnString name = strName + L"RootNode";
		m_pRootNode = CnNew CnSceneNode( name.c_str(), 0xffff );
	}
	CnSceneGraph::~CnSceneGraph()
	{
		SAFEDEL( m_pRootNode );
	}

	//----------------------------------------------------------------
	/** Get Root Node
	    @remarks	  루트 노드 얻기
		@param        none
		@return       CnSceneNode	:	루트 노드
	*/
	//----------------------------------------------------------------
	CnSceneNode* CnSceneGraph::GetRoot() const
	{
		return m_pRootNode;
	}

	//----------------------------------------------------------------
	/** @brief	SetName */
	//----------------------------------------------------------------
	void CnSceneGraph::SetName( const CnString& Name )
	{
		m_strName = Name;
		GetRoot()->SetName( Name + L"RootNode" );
	}

	//----------------------------------------------------------------
	/** @brief	이름 얻어오기 */
	//----------------------------------------------------------------
	const CnString& CnSceneGraph::GetName() const
	{
		return m_strName;
	}

	//----------------------------------------------------------------
	/** Attach Node
	    @remarks	  루트 노드에 자식 붙이기
		@param        pNode : CnNode
		@return       none
	*/
	//----------------------------------------------------------------
	void CnSceneGraph::AttachChild( CnNode* pNode )
	{
		GetRoot()->AttachChild( pNode );
	}

	//----------------------------------------------------------------
	/** Update Affine
	    @remarks	  변환 업데이트
		@param        none
		@return       none
	*/
	//----------------------------------------------------------------
	void CnSceneGraph::UpdateTransform()
	{
		GetRoot()->UpdateTransform( true, false );
	}

	//----------------------------------------------------------------
	/** Update
	    @remarks	  장면 그래프 업데이트
		@param        pCamera : 카메라
		@return       none
	*/
	//----------------------------------------------------------------
	void CnSceneGraph::FindVisible( CnCamera* pCamera, CnVisibleSet* pVisibleSet )
	{
		GetRoot()->UpdateScene( pCamera, pVisibleSet );
	}
}