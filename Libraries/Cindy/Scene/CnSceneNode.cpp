//================================================================
// File:               : CnSceneNode.cpp
// Related Header File : CnSceneNode.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 26
//================================================================
#include "Cindy.h"
#include "Math/CnAxisAlignedBox.h"
//#include "Graphics/CnRenderQueue.h"
#include "Util/CnLog.h"
#include "CnSceneNode.h"
#include "CnCamera.h"

namespace Cindy
{
	//============================================================================
	__ImplementClass(Cindy, CnSceneNode, CnAnimNode);
	//__RegisterClass(Cindy, CnSceneNode);
	/// Const/Dest
	CnSceneNode::CnSceneNode()
		: m_bVisible( true )
		, m_bUseCulling( true )
	{
	}
	CnSceneNode::CnSceneNode( const wchar* strName, ushort nIndex )
		: CnAnimNode( strName, nIndex )
		, m_bVisible( true )
		, m_bUseCulling( true )
	{
	}
	CnSceneNode::~CnSceneNode()
	{
		DeleteAllEntities();
	}

	CnNode* CnSceneNode::_CreateChildImpl( const CnString& strName, ushort nIndex )
	{
		return CnNew CnSceneNode( strName.c_str(), nIndex );
	}

	//================================================================
	/** Add SceneNode
	    @brief      씬노드를 따로 목록에 추가
		@param        pChild : 붙일 노드
		@return       none
	*/
	//================================================================
	void CnSceneNode::_AddSceneNode( CnNode* pChild )
	{
		CnSceneNode* scenenode = DynamicCast<CnSceneNode>( pChild );
		if( scenenode )
			m_ChildSceneNodes.push_back( scenenode );
	}

	//================================================================
	/** Remove SceneNode
	    @brief      씬노드를 따로 목록에 삭제
		@param        pChild : 노드
		@return       none
	*/
	//================================================================
	void CnSceneNode::_RemoveSceneNode( CnNode* pChild )
	{
		CnSceneNode* scenenode = DynamicCast<CnSceneNode>( pChild );
		if( scenenode )
		{
			SceneNodeIter iter = std::find( m_ChildSceneNodes.begin(), m_ChildSceneNodes.end(), scenenode );
			if( iter != m_ChildSceneNodes.end() )
				m_ChildSceneNodes.erase( iter );
		}
	}

	//================================================================
	/** Remove All SceneNodes
	    @brief      모든 장면 노드 목록을 해제
		@param        none
		@return       none
	*/
	//================================================================
	void CnSceneNode::_RemoveAllSceneNodes()
	{
		m_ChildSceneNodes.clear();
	}

	//================================================================
	/** Attach Child Node
	    @brief      자식 노드를 붙인다.
		@param        pChild : 자식으로 붙일 노드
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnSceneNode::AttachChild( CnNode* pChild )
	{
		if( CnNode::AttachChild( pChild ) )
		{
			_AddSceneNode( pChild );
			return true;
		}
		return false;
	}

	//================================================================
	/** Detach Child Node
	    @brief      자식 노드를 띤다. (삭제는 하지 않는다.)
		@param        nIndex : 검색할 노드 인덱스
		@return       CnSceneNode : 자식 노드
	*/
	//================================================================
	CnNode* CnSceneNode::DetachChild( ushort nIndex )
	{
		CnNode* node = CnNode::DetachChild( nIndex );
		if( NULL == node )
			return NULL;

		_RemoveSceneNode( node );
		return node;
	}

	//================================================================
	/** Detach Child Node
	    @brief      자식 노드를 띤다. (삭제는 하지 않는다.)
		@param        strName : 검색할 노드 이름
		@return       CnSceneNode : 자식 노드
	*/
	//================================================================
	CnNode* CnSceneNode::DetachChild( const CnString& strName )
	{
		CnNode* node = CnNode::DetachChild( strName );
		if( NULL == node )
			return NULL;

		_RemoveSceneNode( node );
		return node;
	}

	//================================================================
	/** Detach Children
	    @brief      모든 자식 노드를 띤다. (삭제는 하지 않는다.)
		@param		  rChildren : 자식 노드들
		@return		  none
	*/
	//================================================================
	void CnSceneNode::DetachChildren( std::vector<CnNode*>& rChildren )
	{
		CnNode::DetachChildren( rChildren );

		_RemoveAllSceneNodes();
	}

	//================================================================
	/** Delete Child Node
	    @brief      자식 노드를 삭제한다.
		@param        nIndex : 검색할 자식 노드 인덱스
		@return       true/false : 삭제 성공 여부
	*/
	//================================================================
	bool CnSceneNode::DeleteChild( ushort nIndex )
	{
		ChildIter iend = m_Children.end();
		for( ChildIter i = m_Children.begin(); i != iend; ++i )
		{
			if( i->second->GetID() == nIndex )
			{
				_RemoveSceneNode( i->second );

				//delete (i->second);
				m_Children.erase( i );

				return true;
			}
		}
		return false;
	}

	//================================================================
	/** Delete Child Node
	    @brief      자식 노드를 삭제한다.
		@param        strName : 검색할 자식 노드 이름
		@return       true/false : 삭제 성공 여부
	*/
	//================================================================
	bool CnSceneNode::DeleteChild( const CnString& strName )
	{
		ChildIter iter = m_Children.find( strName );
		if( iter == m_Children.end() )
			return false;

		_RemoveSceneNode( iter->second );

		//delete (iter->second);
		m_Children.erase( iter );

		return true;
	}

	//================================================================
	/** Delete Children
	    @brief      모든 자식 노드들을 삭제한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnSceneNode::DeleteChildren()
	{
		CnNode::DeleteChildren();

		_RemoveAllSceneNodes();
	}

	//================================================================
	/** Attach Entity
	    @brief      Entity를 붙인다.
		@param        pEntity : 붙일 개체
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnSceneNode::AttachEntity( CnSceneEntity* pEntity )
	{
		std::pair<EntityIter, bool> res = m_SceneEntities.insert( EntityMap::value_type( pEntity->GetName(), pEntity ) );
		if (!res.second)
		{
			CnPrint( L"[CnSceneNode::AttachEntity] SceneNode[%s], Entity[%s]", GetName().c_str(), pEntity->GetName().c_str() );

#ifdef _DEBUG
			CnError( L"Same Name Entity is exist" );
#endif
			return false;
		}

		pEntity->SetParent( this );
		return true;
	}

	//================================================================
	/** Get Entity
	    @brief      Entity를 반환한다..
		@param        strName : 이름
		@return       CnSceneEntity : 반환한 Entity
	*/
	//================================================================
	CnSceneEntity* CnSceneNode::GetEntity( const CnString& strName )
	{
		EntityIter iter = m_SceneEntities.find( strName );
		if( iter == m_SceneEntities.end() )
			return NULL;

		return (iter->second);
	}

	//================================================================
	/** Detach Entity
	    @brief      Entity를 노드에서 뗀다. 삭제는 되지 않는다.
		@param        strName : 이름
		@return       CnSceneEntity : 반환한 Entity
	*/
	//================================================================
	CnSceneEntity* CnSceneNode::DetachEntity( const CnString& strName )
	{
		EntityIter iter = m_SceneEntities.find( strName );
		if( iter == m_SceneEntities.end() )
			return NULL;

		CnSceneEntity* result = iter->second;
		result->SetParent( NULL );

		m_SceneEntities.erase( iter );
		return result;
	}

	//================================================================
	/** Detach All Entities
	    @brief      모든 Entity들을 노드에서 뗀다. 삭제는 되지 않는다.
		@param        rEntites : 반환할 Entity
		@return       none
	*/
	//================================================================
	void CnSceneNode::DetachAllEntities( std::vector<CnSceneEntity*>& rEntities )
	{
		rEntities.clear();
		rEntities.reserve( m_SceneEntities.size() );

		CnSceneEntity* entity = NULL;

		EntityIter iend = m_SceneEntities.end();
		for( EntityIter i = m_SceneEntities.begin(); i != iend; ++i )
		{
			entity = i->second;
			entity->SetParent(0);

			rEntities.push_back( entity );
		}
		m_SceneEntities.clear();
	}

	//================================================================
	/** Delete Entity
	    @brief      Entity를 노드에서 삭제 시킨다.
		@param        strName : 이름
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnSceneNode::DeleteEntity( const CnString& strName )
	{
		EntityIter iter = m_SceneEntities.find( strName );
		if( iter == m_SceneEntities.end() )
			return false;

		iter->second->SetParent(0);
		delete (iter->second);
		m_SceneEntities.erase( iter );
		return true;
	}

	//================================================================
	/** Delete All Entities
	    @brief      모든 Entity들을 삭제한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnSceneNode::DeleteAllEntities()
	{
		EntityIter iend = m_SceneEntities.end();
		for( EntityIter i = m_SceneEntities.begin(); i != iend; ++i )
		{
			i->second->SetParent(0);
			delete (i->second);
		}
		m_SceneEntities.clear();
	}

	//================================================================
	/** Update
	    @brief      업데이트
		@param        bUpdateChildren : 자식 업데이트 여부
		@param        bChangedParent : 부모 업데이트 여부
		@return       none
	*/
	//================================================================
	void CnSceneNode::UpdateTransform( bool bUpdateChildren, bool bChangedParent )
	{
		if( !IsVisible() )
			return;

		CnNode::UpdateTransform( bUpdateChildren, bChangedParent );

		_UpdateBound();
	}

	//================================================================
	/** Update Scene
	    @brief      실제 랜더링 될 개체들을 업데이트 한다.
		@param        pCamera : 카메라
		@param		  pRenderQueue : 랜더큐
		@param		  pLightQueue : 라이트 큐
		@return       none
	*/
	//================================================================
	void CnSceneNode::UpdateScene( CnCamera* pCamera, CnVisibleSet* pVisibleSet )
	{
		if (false == _FindVisibleEntities( pCamera, pVisibleSet ))
			return;

		CnSceneNode* scenenode = NULL;

#pragma NOTE("장면 업데이트 처리를 확인한다.")
		// 자식 장면 노드만 업데이트 할 것인지, 자식 노드 모두를 업데이트 할 것인지를
		// 확인한다. Bone노드 자식의 장면 노드를 처리할 때 사용한다.

		//SceneNodeIter iend = m_ChildSceneNodes.end();
		//SceneNodeIter i = m_ChildSceneNodes.begin();
		//for (; i != iend; ++i)
		//{
		//	scenenode = (*i);
		//	scenenode->UpdateScene( pCamera, pVisibleSet );
		//}
		ChildIter iend = m_Children.end();
		for( ChildIter i = m_Children.begin(); i != iend; ++i )
			(i->second)->UpdateScene( pCamera, pVisibleSet );
	}

	//================================================================
	/** _FindVisibleEntities
	    @brief      실제 랜더링 될 개체들을 업데이트 한다.
		@param        pCamera : 카메라
		@param		  pRenderQueue : 랜더큐
		@param		  pLightQueue : 라이트 큐
		@return       none
	*/
	//================================================================
	bool CnSceneNode::_FindVisibleEntities( CnCamera* pCamera, CnVisibleSet* pVisibleSet )
	{
		if (!IsVisible())
			return false;

		if (NULL == pCamera)
			return false;

		if (UseCulling() && pCamera->IsCulled( m_Bound ))
			return false;

		_UpdateAfterCulling();

		CnSceneEntity* pEntity = NULL;
		Math::AABB aabb;

		EntityIter iend = m_SceneEntities.end();
		for( EntityIter i = m_SceneEntities.begin(); i != iend; ++i )
		{
			pEntity = i->second;
			if (!pEntity->Update( pCamera ))
				continue;

			pVisibleSet->Register( pEntity );
		}

		return true;
	}

	//================================================================
	/** Update Bounding Volume
	    @brief      바운딩 볼륨을 업데이트 한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnSceneNode::_UpdateBound()
	{
		m_Bound.SetEmpty();

		Math::AABB aabb;

		// 자식 scene entity 바운딩 박스 모으기..
		EntityIter ieend = m_SceneEntities.end();
		for( EntityIter i = m_SceneEntities.begin(); i != ieend; ++i )
		{
			i->second->GetAABB( aabb );
			m_Bound.Merge( aabb );
		}

		SceneNodeIter iend = m_ChildSceneNodes.end();
		for( SceneNodeIter i = m_ChildSceneNodes.begin(); i != iend; ++i )
		{
			m_Bound.Merge( (*i)->m_Bound );
		}
	}

	//================================================================
	/** Update After Culling
	    @brief      이 노드가 화면에 보이게 된다면, 업데이트 할 것을 하자.
		@param        none
		@return       none
	*/
	//================================================================
	void CnSceneNode::_UpdateAfterCulling()
	{	// Blank... Blank.. Blank..
	}
}