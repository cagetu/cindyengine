//================================================================
// File:               : CnNode.cpp
// Related Header File : CnNode.h
// Original Author     : changhee
// Creation Date       : 2007. 1. 26
//================================================================
#include "../Cindy.h"
#include "Math/CindyMath.h"
#include "CnNode.h"
#include "CnController.h"

namespace Cindy
{
	//==============================================================
	__ImplementRtti( Cindy, CnNode, CnIKObject );
	// Const
	CnNode::CnNode()
		: m_bUpdateChildren( true )
		, m_NumOfChildren(0)
	{
	}
	CnNode::CnNode( const wchar* strName, ushort nIndex )
		: CnIKObject( strName, nIndex )
		, m_bUpdateChildren( true )
		, m_NumOfChildren(0)
	{
	}
	//Dest
	CnNode::~CnNode()
	{
		m_ChildrenToUpdate.clear();
		DeleteChildren();
	}

	//================================================================
	/** Create Child
	    @brief      자식 노드를 붙인다.
	*/
	//================================================================
	CnNode* CnNode::CreateChild( const CnString& strName, ushort nIndex,
								 const Math::Vector3& vPosition,
								 const Math::Quaternion& qRotation,
								 const Math::Vector3& vScale )
	{
		ChildIter i = m_Children.find( strName );
		if( i != m_Children.end() )
			return i->second;

		CnNode* pNode = _CreateChildImpl( strName, nIndex );

		pNode->SetPosition( vPosition );
		pNode->SetRotation( qRotation );
		pNode->SetScale( vScale );                               

		AttachChild( pNode );

		return pNode;
	}

	//================================================================
	/** Attach Child Node
	    @brief      자식 노드를 붙인다.
		@param        pChild : 자식으로 붙일 노드
		@return       true/false : 성공 여부
	*/
	//================================================================
	bool CnNode::AttachChild( CnNode* pChild )
	{
		ChildIter i = m_Children.find( pChild->GetName() );
		if (i != m_Children.end())
			return false;

		pChild->_SetParent( this );

		m_Children.insert( ChildMap::value_type( pChild->GetName(), pChild ) );
		m_NumOfChildren = (ushort)m_Children.size();
		return true;
	}

	//================================================================
	/** Detach Child Node
	    @brief      자식 노드를 띤다. (삭제는 하지 않는다.)
		@param        nIndex : 검색할 노드 인덱스
		@return       CnNode : 자식 노드
	*/
	//================================================================
	CnNode* CnNode::DetachChild( ushort nIndex )
	{
		ChildIter iend = m_Children.end();
		for( ChildIter i = m_Children.begin(); i != iend; ++i )
		{
			if( i->second->GetID() == nIndex )
			{
				CnNode* pNode = i->second;

				CancelUpdate( pNode );
				pNode->_SetParent(0);

				m_Children.erase( i );
				m_NumOfChildren = (ushort)m_Children.size();
				return pNode;
			}
		}
		return NULL;
	}

	//================================================================
	/** Detach Child Node
	    @brief      자식 노드를 띤다. (삭제는 하지 않는다.)
		@param        strName : 검색할 노드 이름
		@return       CnNode : 자식 노드
	*/
	//================================================================
	CnNode* CnNode::DetachChild( const CnString& strName )
	{
		ChildIter iter = m_Children.find( strName );
		if( iter == m_Children.end() )
			return false;

		CnNode* pNode = iter->second;
		CancelUpdate( pNode );
		pNode->_SetParent(0);
		m_Children.erase( iter );
		m_NumOfChildren = (ushort)m_Children.size();
		return pNode;
	}

	//================================================================
	/** Detach Children
	    @brief      모든 자식 노드를 띤다. (삭제는 하지 않는다.)
		@param		  rChildren : 자식 노드들
		@return		  none
	*/
	//================================================================
	void CnNode::DetachChildren( std::vector<CnNode*>& rChildren )
	{
		rChildren.clear();
		rChildren.reserve( m_Children.size() );

		CnNode* pNode = NULL;
		ChildIter iend = m_Children.end();
		for( ChildIter i = m_Children.begin(); i != iend; ++i )
		{
			pNode = i->second;
			
			CancelUpdate( pNode );
			pNode->_SetParent( NULL );

			rChildren.push_back( pNode );			
		}
		m_Children.clear();
		m_ChildrenToUpdate.clear();
		m_NumOfChildren = 0;

//		NeedUpdateParent();
	}

	//================================================================
	/** Delete Child Node
	    @brief      자식 노드를 삭제한다.
		@param        nIndex : 검색할 자식 노드 인덱스
		@return       true/false : 삭제 성공 여부
	*/
	//================================================================
	bool CnNode::DeleteChild( ushort nIndex )
	{
		CnNode* pNode = DetachChild( nIndex );
		if (!pNode)
			return false;

		//delete pNode;
		return true;
	}

	//================================================================
	/** Delete Child Node
	    @brief      자식 노드를 삭제한다.
		@param        strName : 검색할 자식 노드 이름
		@return       true/false : 삭제 성공 여부
	*/
	//================================================================
	bool CnNode::DeleteChild( const CnString& strName )
	{
		CnNode* pNode = DetachChild( strName );
		if( !pNode )
			return false;

		//delete pNode;
		return true;
	}

	//================================================================
	/** Delete Children
	    @brief      모든 자식 노드들을 삭제한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnNode::DeleteChildren()
	{
		//ChildIter iend = m_Children.end();
		//for( ChildIter i = m_Children.begin(); i != iend; ++i )
		//	delete (i->second);

		m_Children.clear();
		m_ChildrenToUpdate.clear();

//		NeedUpdateParent();
	}

	//================================================================
	/** Get Child Node
	    @brief      자식 노드를 찾는다.
		@param        nIndex : 검색할 노드 인덱스
		@return       CnNode : 찾은 노드
	*/
	//================================================================
	CnNode* CnNode::GetChild( ushort nIndex, bool bRecursive )
	{
		if( bRecursive )
		{
			if( m_nIndex == nIndex )
				return this;

			CnNode* node = NULL;
			ChildIter iend = m_Children.end();
			for( ChildIter i = m_Children.begin(); i != iend; ++i )
			{
				node = i->second->GetChild( nIndex, true );
				if( node )
					break;
			}
			return node;
		}
		else
		{
			ChildIter iend = m_Children.end();
			for( ChildIter i = m_Children.begin(); i != iend; ++i )
			{
				if( i->second->GetID() == nIndex )
					return i->second;
			} // for
		}
		return NULL;
	}

	//================================================================
	/** Get Child Node
	    @brief      자식 노드를 찾는다.
		@param        strName : 검색할 노드 이름
		@return       CnNode : 찾은 노드
	*/
	//================================================================
	CnNode* CnNode::GetChild( const CnString& strName, bool bRecursive )
	{
		if( bRecursive )
		{
			if( m_strName == strName )
				return this;

			CnNode* node = NULL;
			ChildIter iend = m_Children.end();
			for( ChildIter i = m_Children.begin(); i != iend; ++i )
			{
				node = i->second->GetChild( strName, true );
				if( node )
					break;
			}
			return node;
		}
		else
		{
			ChildIter iter = m_Children.find( strName );
			if( iter != m_Children.end() )
				return iter->second;
		}
		return NULL;
	}

	////================================================================
	///** Attach Controller
	//    @brief      Controller 붙이기
	//	@param        pController : 붙일 Controller
	//	@return       true/false : 성공 여부
	//*/
	////================================================================
	//bool CnNode::AttachController( CnController* pController )
	//{
	//	ControllerIter iter = m_Controllers.find( pController->GetID() );
	//	if( iter == m_Controllers.end() )
	//		return false;

	//	pController->SetParent( this );
	//	m_Controllers.insert( ControllerList::value_type( pController->GetID(), pController ) );
	//	return true;
	//}

	////================================================================
	///** Get Controller
	//    @brief      Controller 반환
	//	@param        nID : Controller ID
	//	@return       CnController : 반환할 Controller
	//*/
	////================================================================
	//CnController* CnNode::GetController( int nID )
	//{
	//	ControllerIter iter = m_Controllers.find( nID );
	//	if( iter == m_Controllers.end() )
	//		return NULL;

	//	return (iter->second);
	//}

	////================================================================
	///** Detach Controller
	//    @brief      Controller 를 뗀다. 삭제는 하지 않는다.
	//	@param        nID : Controller ID
	//	@return       CnController : 반환할 Controller
	//*/
	////================================================================
	//CnController* CnNode::DetachController( int nID )
	//{
	//	ControllerIter iter = m_Controllers.find( nID );
	//	if( iter == m_Controllers.end() )
	//		return NULL;

	//	CnController* controller = iter->second;
	//	controller->SetParent(0);
	//	m_Controllers.erase( iter );

	//	return controller;
	//}

	////================================================================
	///** Detach All Controller
	//    @brief      모든 Controller 를 뗀다. 삭제는 하지 않는다.
	//	@param        rControllers : 반환할 Controller 목록
	//	@return       none
	//*/
	////================================================================
	//void CnNode::DetachAllController( std::vector<CnController*>& rControllers )
	//{
	//	rControllers.clear();
	//	rControllers.reserve( m_Controllers.size() );

	//	CnController* controller = NULL;

	//	ControllerIter iend = m_Controllers.end();
	//	for( ControllerIter i = m_Controllers.begin(); i != iend; ++i )
	//	{
	//		controller = i->second;
	//		controller->SetParent(0);

	//		rControllers.push_back( controller );
	//	}
	//	m_Controllers.clear();
	//}

	////================================================================
	///** Delete Controller
	//    @brief      Controller를 삭제한다.
	//	@param        nID : Controller ID
	//	@return       true/false : 성공 여부
	//*/
	////================================================================
	//bool CnNode::DeleteController( int nID )
	//{
	//	ControllerIter iter = m_Controllers.find( nID );
	//	if( iter == m_Controllers.end() )
	//		return false;

	//	iter->second->SetParent(0);
	//	delete (iter->second);
	//	m_Controllers.erase( iter );

	//	return true;
	//}

	////================================================================
	///** Delete All Controller
	//    @brief      모든 Controller들을 삭제한다.
	//	@param        none
	//	@return       none
	//*/
	////================================================================
	//void CnNode::DeleteAllControllers()
	//{
	//	ControllerIter iend = m_Controllers.end();
	//	for( ControllerIter i = m_Controllers.begin(); i != iend; ++i )
	//	{
	//		i->second->SetParent(0);
	//		delete (i->second);
	//	}
	//	m_Controllers.clear();
	//}

	//================================================================
	/** Need Update
	    @brief      업데이트를 요청한다.
		@param        none
		@return       none
	*/
	//================================================================
	void CnNode::NeedUpdateParent()
	{
		CnTransformable::NeedUpdateParent();

		m_bUpdateChildren = true;
	}

	//================================================================
	/** Request Update
	    @brief      업데이트를 요청한다.
		@param        pNode : 업데이트 할 노드
		@return       none
	*/
	//================================================================
	void CnNode::RequestUpdate( CnTransformable* pNode )
	{
		if( m_bUpdateChildren )
			return;

		m_ChildrenToUpdate.push_back( pNode );

		if( m_pParent && !m_bParentNotified )
		{
			m_pParent->RequestUpdate( this );
			m_bParentNotified = true;
		}
	}

	//================================================================
	/** Cancel Update
	    @brief      업데이트를 취소한다.
		@param        pNode : 업데이트 취소 할 노드
		@return       none
	*/
	//================================================================
	void CnNode::CancelUpdate( CnTransformable* pNode )
	{
		UpdateIter iter = find( m_ChildrenToUpdate.begin(), m_ChildrenToUpdate.end(), pNode );
		if( m_ChildrenToUpdate.end() == iter )
			return;

		m_ChildrenToUpdate.erase( iter );
		if( m_ChildrenToUpdate.empty() && m_pParent && !m_bUpdateChildren )
		{
			m_pParent->CancelUpdate( this );
			m_bParentNotified = false;
		}
	}

	//================================================================
	/** Update Affine
	    @brief      업데이트
		@param        bUpdateChildren : 자식 업데이트 여부
		@param        bChangedParent : 부모 업데이트 여부
		@return       none
	*/
	//================================================================
	void CnNode::UpdateTransform( bool bUpdateChildren, bool bChangedParent )
	{
		m_bParentNotified = false;

		if( !bChangedParent && !m_bUpdateParent && !bUpdateChildren && !m_bUpdateChildren )
			return;

		if( m_bUpdateParent || bChangedParent )
			_UpdateFromParent();

		if( m_bUpdateChildren || bChangedParent )
		{
			ChildIter iend = m_Children.end();
			for( ChildIter i = m_Children.begin(); i != iend; ++i )
				i->second->UpdateTransform( true, true );
		}
		else
		{
			UpdateIter iend = m_ChildrenToUpdate.end();
			for( UpdateIter i = m_ChildrenToUpdate.begin(); i != iend; ++i )
				(*i)->UpdateTransform( true, false );
		}

		m_ChildrenToUpdate.clear();
		m_bUpdateChildren = false;
	}

	//================================================================
	/** Update State
	    @brief      상태 업데이트
		@param        paGSStack : 
		@return       none
	*/
	//================================================================
	void CnNode::UpdateState( std::vector<CnGlobalState*>* paGSStack )
	{
		ChildIter iend = m_Children.end();
		for( ChildIter i = m_Children.begin(); i != iend; ++i )
			i->second->UpdateRS( paGSStack );
	}

}