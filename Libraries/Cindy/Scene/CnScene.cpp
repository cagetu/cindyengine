//================================================================
// File					: CnScene.cpp
// Related Header File	: CnScene.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 22.
//================================================================
#include "../Cindy.h"
#include "CnScene.h"
#include "CnSceneGraph.h"
#include "CnCamera.h"

#include "Util/CnLog.h"
#include "Graphics/CnRenderer.h"
#include "Material/CnTextureManager.h"
#include "Material/CnShaderManager.h"

namespace Cindy
{
	//==============================================================
	__ImplementRtti( Cindy, CnScene, CnObject );

	//--------------------------------------------------------------
	CnScene::CnScene( const CnString& strName )
		: m_strName( strName )
	{
	}
	CnScene::~CnScene()
	{
		ClearComponents();
	}

	//--------------------------------------------------------------
	void CnScene::SetShader( const wchar* FileName, int ShaderType )
	{
		m_Shader = ShaderMgr->Load( FileName, ShaderType );
	}

	//--------------------------------------------------------------
	void CnScene::AddComponent( const Ptr<Scene::Component>& spComponent )
	{
		if (spComponent == NULL)
			return;

		//ComponentIter iter = std::find( m_Components.begin(), m_Components.end(), spComponent );
		//if (iter != m_Components.end())
		//	return;

		spComponent->Open();

		m_Components.push_back( spComponent );
	}

	//--------------------------------------------------------------
	void CnScene::RemoveComponent( const SceneID& ID )
	{
		ComponentIter iend = m_Components.end();
		for (ComponentIter i=m_Components.begin(); i != iend; ++i)
		{
			if ( (*i)->GetID() == ID)
			{
				(*i)->Close();

				m_Components.erase( i );
				return;
			}
		}
	}

	//--------------------------------------------------------------
	Ptr<Scene::Component> CnScene::GetComponent( const SceneID& ID )
	{
		ComponentIter iend = m_Components.end();
		for (ComponentIter i=m_Components.begin(); i != iend; ++i)
		{
			if ( (*i)->GetID() == ID)
				return (*i);
		}

		return Ptr<Scene::Component>();
	}

	//--------------------------------------------------------------
	void CnScene::ClearComponents()
	{
		m_Components.clear();
	}

	//--------------------------------------------------------------
	void CnScene::Update( CnCamera* pCamera )
	{
		//CnSceneManager::Instance()->SetCurrentScene( this );
		//CnSceneManager::Instance()->SetCurrentCamera( this );
		//TexPtr tex = CnTextureManager::Instance()->Get( L"LispSM" );
		//if (!tex.IsNull())
		//{
		//	//LPDIRECT3DDEVICE9 device;
		//	//RenderDevice->GetCurrentDevice( device );

		//	LPDIRECT3DSURFACE9 lastSurface, lastDepthStencil;
		//	RenderDevice->GetSurface( 0, &lastSurface );
		//	RenderDevice->GetDepthStencilSurface( &lastDepthStencil );

		//		LPDIRECT3DSURFACE9 surface, depthStencil;
		//		tex->GetSurface( &surface );
		//		tex->GetDepthStencil( &depthStencil );

		//		RenderDevice->SetSurface( 0, surface );
		//		RenderDevice->SetDepthStencilSurface( depthStencil );

		//		if( RenderDevice->BeginScene() )
		//		{
		//			m_pSceneGraph->Draw( pCamera );

		//			RenderDevice->EndScene();
		//		} // if

		//	RenderDevice->SetSurface( 0, lastSurface );
		//	RenderDevice->SetDepthStencilSurface( lastDepthStencil );
		//}
		//m_pSceneGraph->m_pLightQueue->Project( pCamera );

		//if( RenderDevice->BeginScene() )
		//{
		//	m_pSceneGraph->Draw( pCamera );

		//	m_pSceneGraph->m_pLightQueue->Debug();

		//	RenderDevice->EndScene();
		//} // if

		ComponentIter i = m_Components.begin();
		ComponentIter iend = m_Components.end();
		for (; i != iend; ++i)
		{
			(*i)->OnUpdate();
		}
	}

	//--------------------------------------------------------------
	void CnScene::Draw( CnCamera* pCamera )
	{
/*
		TexPtr tex = TextureMgr->Get( L"LispSM" );
		if (!tex.IsNull())
		{
			//LPDIRECT3DDEVICE9 device;
			//RenderDevice->GetCurrentDevice( device );

			LPDIRECT3DSURFACE9 lastSurface, lastDepthStencil;
			RenderDevice->GetSurface( 0, &lastSurface );
			RenderDevice->GetDepthStencilSurface( &lastDepthStencil );

				LPDIRECT3DSURFACE9 surface, depthStencil;
				tex->GetSurface( &surface );
				tex->GetDepthStencil( &depthStencil );

				RenderDevice->SetSurface( 0, surface );
				RenderDevice->SetDepthStencilSurface( depthStencil );

				if( RenderDevice->BeginScene() )
				{
					m_pSceneGraph->Draw( pCamera );

					RenderDevice->EndScene();
				} // if

			RenderDevice->SetSurface( 0, lastSurface );
			RenderDevice->SetDepthStencilSurface( lastDepthStencil );
		}
		m_pSceneGraph->m_pLightQueue->Project( pCamera );

		if( RenderDevice->BeginScene() )
		{
			m_pSceneGraph->Draw( pCamera );

			m_pSceneGraph->m_pLightQueue->Debug();

			RenderDevice->EndScene();
		} // if
*/

		//CnPrint( L"!!!! Start [%s]Scene !!!!\n", m_strName.c_str() );

		if (!m_Shader.IsNull())
		{
			int numPasses = m_Shader->Begin();
			assert(1 == numPasses);
			m_Shader->BeginPass(0);
			m_Shader->CommitChanges();
		}

		ComponentIter i, iend;

		iend = m_Components.end();
		for (i = m_Components.begin(); i != iend; i++)
		{
			(*i)->OnBeginFrame( pCamera );
		}

		iend = m_Components.end();
		for (i = m_Components.begin(); i != iend; i++)
		{
			(*i)->OnFrame( pCamera );
		}

		iend = m_Components.end();
		for (i = m_Components.begin(); i != iend; i++)
		{
			(*i)->OnEndFrame( pCamera );
		}

		if (!m_Shader.IsNull())
		{
			m_Shader->EndPass();
			m_Shader->End();
		}

		//CnPrint( L"!!!! End [%s]Scene !!!!\n", m_strName.c_str() );
	}
} // end of namespace