//================================================================
// File:           : CnPhysicsNofity.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_NOTIFY_H__
#define __CN_PHYSICS_NOTIFY_H__

#include "CnPhysics.h"

namespace Cindy
{
namespace Physics
{
	//================================================================
	/** Physics Nofity
	    @author    changhee
		@since     2010. 1. 28
		@remarks   이벤트 발생시에 처리되는 Notifier 재정의
	*/
	//================================================================
	class Notify : public NxUserNotify
	{
	public:
		virtual void onWake( NxActor** actors, NxU32 count );
		virtual void onSleep( NxActor** actors, NxU32 count);
		virtual bool onJointBreak( NxReal breakingImpulse, NxJoint& brokenJoint );
	};
} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_NOTIFY_H__