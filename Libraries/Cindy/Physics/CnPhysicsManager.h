//================================================================
// File:           : CnPhysicsManager.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_MANAGER_H__
#define __CN_PHYSICS_MANAGER_H__

#include "Util/CnSingleton.h"
#include "Util/CnUnCopyable.h"
#include "CnPhysicsActor.h"
#include "CnPhysicsLog.h"
#include "CnPhysicsAllocator.h"

namespace Cindy
{
namespace Physics
{
	//================================================================
	/** Physics Manager
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Manager
	*/
	//================================================================
	class Manager
	{
		__DeclareSingleton(Manager);
		__DeclareUnCopy(Manager);
	public:
		void			Open();
		void			Close();

		void			SetParameter( NxParameter Parameter, float Value );

		NxPhysicsSDK*	GetSDK() const;

		///
	private:
		typedef std::map<ulong, Physics::Actor*>	ActorMap;

		friend class Actor;
		//------------------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------------------
		Manager();
		~Manager();

		/// register an entity with the server
		void RegisterEntity(Physics::Actor* entity);
		/// unregister an entity from the server
		void UnregisterEntity(Physics::Actor* entity);

		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		// Physics SDK globals
		NxPhysicsSDK*		m_PhysicsSDK;

		Physics::Allocator	m_Allocator;
		Physics::Log		m_ErrorReporter;

		ActorMap			m_ActorRegistry;
	};

	//------------------------------------------------------------------------------
	/**
	*/
	inline NxPhysicsSDK*
	Manager::GetSDK() const
	{
		return m_PhysicsSDK;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	void
	Manager::RegisterEntity(Physics::Actor* entity)
	{
		assert(entity);
		m_ActorRegistry.insert( ActorMap::value_type(entity->GetUniqueId(), entity) );
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	void
	Manager::UnregisterEntity(Physics::Actor* entity)
	{
		assert(entity);
		ActorMap::iterator iter = m_ActorRegistry.find(entity->GetUniqueId());
		if (iter != m_ActorRegistry.end())
		{
			m_ActorRegistry.erase(iter);
		}
	}
} // namespace Physics
} // namespace Cindy

#define PhysicsManager	Cindy::Physics::Manager::Instance()

#endif	// __CN_PHYSICS_MANAGER_H__