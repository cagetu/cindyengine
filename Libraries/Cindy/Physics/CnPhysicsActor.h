//================================================================
// File:           : CnPhysicsActor.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_ACTOR_H__
#define __CN_PHYSICS_ACTOR_H__

#include "CnPhysicsComposite.h"

namespace Cindy
{
namespace Physics
{
	class Scene;

	//================================================================
	/** Physics Actor
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Actor
	*/
	//================================================================
	class Actor : public CnObject
	{
		__DeclareClass(Actor);
	public:
		Actor();
		virtual ~Actor();

		///
		ulong					GetUniqueId() const;

		/// Composite
		void					SetComposite( const Ptr<Physics::Composite>& Composite );

		///
		void					SetTransform( const Math::Matrix44& Transform );
		const Math::Matrix44&	GetTransform() const;

		///
		void					OnActivate();
		void					OnDeactivate();

		///
		void					OnAttachedToScene( Physics::Scene* Scene );
		void					OnRemoveFromScene();

	private:
		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		Physics::Scene*			m_Owner;

		ulong					m_UniqueId;
		static ulong			m_UniqueIdCounter;

		NxActor*				m_Actor;
		NxActorDesc				m_ActorDesc;

		bool					m_Active;

		Math::Matrix44			m_WorldTM;

		Ptr<Physics::Composite>	m_Composite;
	};

	//------------------------------------------------------------------------------
	/**
	*/
	inline ulong
	Actor::GetUniqueId() const
	{
		return m_UniqueId;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline const Math::Matrix44&
	Actor::GetTransform() const
	{
		return m_WorldTM;
	}

} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_ACTOR_H__