//================================================================
// File:           : CnPhysics.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __PHYSICS_H__
#define __PHYSICS_H__

#ifdef WIN32
#undef min
#undef max
#endif

#include "NxPhysics.h"
#include "NxCooking.h"
#include "NxControllerManager.h"
#include "NxBoxController.h"
#include "NxCapsuleController.h"

#include "Math/CindyMath.h"

namespace Cindy
{
namespace Physics
{
    /// convert Math::matrix44 -> PhysX matrix
    void Matrix44ToPhysX(const Math::Matrix44& from, NxMat33& to);
	void Matrix44ToPhysX(const Math::Matrix44& from, NxMat34& to);
    /// convert PhysX matrix to Math::matrix44
    void PhysXToMatrix44(const NxMat34& from, Math::Matrix44& to);
	void PhysXToMatrix44(const NxMat33& from, Math::Matrix44& to);

    /// convert Math::vector to PhysX vector
	void Float3ToPhysX(const Math::Vector3& from, NxVec3& to);
    /// convert PhysX vector to Math::vector
    void PhysXToFloat3(const NxVec3& from, Math::Vector3& to);
	void PhysXToFloat4(const NxVec3& from, Math::Vector4& to);

} // namespace Physics
} // namespace Cindy

#endif	// __PHYSICS_H__