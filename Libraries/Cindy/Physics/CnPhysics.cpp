//================================================================
// File:               : CnPhysics.cpp
// Related Header File : CnPhysics.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysics.h"

namespace Cindy
{
namespace Physics
{
	//------------------------------------------------------------------------------
	/**
		Converts the rotational part of a Math::Matrix44 to PhsyX. This includes
		a handedness conversion (Mangalore is right handed, PhsyX left handed(?)).
	*/
	void Matrix44ToPhysX(const Math::Matrix44& from, NxMat33& to)
	{
		NxMat34 m;
		m.setColumnMajor44((NxF32*)&from);
		to = m.M;
	}

	//------------------------------------------------------------------------------
	/**
		Converts the rotational part of a Math::Matrix44 to PhsyX. This includes
		a handedness conversion (Mangalore is right handed, PhsyX left handed(?)).
	*/
	void
	Matrix44ToPhysX(const Math::Matrix44& from, NxMat34& to)
	{
		to.setColumnMajor44((NxF32*)&from);
	}

	//------------------------------------------------------------------------------
	/**
		Convert a NxMat34 to the rotational part of a Math::Matrix44. Includes
		handedness conversion.
	*/
	void
	PhysXToMatrix44(const NxMat34& from, Math::Matrix44& to)
	{
		from.getColumnMajor44( (NxF32*)&to );
	}

	//------------------------------------------------------------------------------
	/**
		Convert a NxMat33 to the rotational part of a Math::Matrix44. Includes
		handedness conversion.
	*/
	void
	PhysXToMatrix44(const NxMat33& from, Math::Matrix44& to)
	{
		NxMat34 m;
		m.M = from;
		m.getColumnMajor44( (NxF32*)&to );
	}

	//------------------------------------------------------------------------------
	/**
		Convert a Math::vector to an PhysX vector.
	*/
	void
	Float3ToPhysX(const Math::Vector3& from, NxVec3& to)
	{
		to[0] = from.x;
		to[1] = from.y; 
		to[2] = from.z;
	}

	//------------------------------------------------------------------------------
	/**
		Convert an PhysX vector to a Math::vector.
	*/
	void
	PhysXToFloat3(const NxVec3& from, Math::Vector3& to)
	{
		to.x = from[0];
		to.y = from[1];
		to.z = from[2];
	}
	//------------------------------------------------------------------------------
	void
	PhysXToFloat4(const NxVec3& from, Math::Vector4& to)
	{
		to.x = from[0];
		to.y = from[1];
		to.z = from[2];
		to.w = 1.0f;
	}

} // namespace Physics
} // namespace Cindy