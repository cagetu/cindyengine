//================================================================
// File:               : CnPhysicsComposite.cpp
// Related Header File : CnPhysicsComposite.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsComposite.h"
#include "CnPhysicsScene.h"
#include "CnPhysicsActor.h"

namespace Cindy
{
namespace Physics
{
	__ImplementClass(Physics, Composite, CnObject);
	//------------------------------------------------------------------------------
	/**
	*/
	Composite::Composite()
		: m_NumShapes(0)
		, m_Actor(0)
		, m_IsAttached(false)
	{
	}
	Composite::~Composite()
	{
	}

	//------------------------------------------------------------------------------
	/**
		Actor가 활성화 될 때, Composite이 Actor에 추가된다. 즉, PhysX에 Attach 된다!!
		(Shape은 Composite에 의해 관리된다.)
	*/
	void Composite::Attach( NxActorDesc& ActorDesc )
	{
		for (int i = 0; i < m_NumShapes; i++)
		{
			m_Shapes[i]->Attach( ActorDesc );
		}

		m_IsAttached = true;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Composite::Detach()
	{
		for (int i = 0; i < m_NumShapes; i++)
		{
			m_Shapes[i]->Detach();
		}

		m_IsAttached = false;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Composite::AddShape( const Ptr<Physics::Shape>& Shape )
	{
		m_Shapes.push_back( Shape );
		m_NumShapes++;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	const Ptr<Physics::Shape>& Composite::GetShape( int Index ) const
	{
		return m_Shapes[Index];
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Composite::ClearShapes()
	{
		m_Shapes.clear();
	}
	
	//------------------------------------------------------------------------------
	/**
	*/
	void Composite::SetEntity( Physics::Actor* Actor )
	{
		m_Actor = Actor;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	Physics::Actor* Composite::GetEntity() const
	{
		return m_Actor;
	}
} // namespace Physics
} // namespace Cindy
