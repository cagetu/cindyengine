//================================================================
// File:           : CnPhysicsShape.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_SHAPE_H__
#define __CN_PHYSICS_SHAPE_H__

#include "CnPhysics.h"
#include "Foundation/CnObject.h"

namespace Cindy
{
namespace Physics
{
	class Actor;

	//================================================================
	/** Physics Shape
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Shape
	*/
	//================================================================
	class Shape : public CnObject
	{
		__DeclareClass(Shape);
	public:
		/// shape types
		enum Type
		{
			InvalidShapeType = 0,
			Box,
			Sphere,
			Plane,
			Convex,
			Mesh,
			Capsule,

			NumShapeTypes,
		};

		Shape( const char* Name = 0 );
		virtual ~Shape();

		//
		virtual Shape::Type		GetType() const;

		//
		void					SetName( const char* Name );
		const std::string&		GetName() const;

		//
		void					SetLocalTM( const Math::Matrix44& Transform );
		const Math::Matrix44&	GetLocalTM() const;

		//
		virtual bool			Attach( NxActorDesc& ActorDesc );
		virtual void			Detach();

		//
		bool					IsAttached() const;

	protected:
		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		std::string		m_Name;

		Math::Matrix44	m_LocalTM;

		bool			m_IsAttached;
	};

	//------------------------------------------------------------------------------
	/**
	*/
	inline Shape::Type
	Shape::GetType() const
	{
		return Shape::InvalidShapeType;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline void 
	Shape::SetName( const char* Name )
	{
		m_Name = Name;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline const
	std::string& Shape::GetName() const
	{
		return m_Name;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline void
	Shape::SetLocalTM( const Math::Matrix44& Transform )
	{
		m_LocalTM = Transform;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline const Math::Matrix44&
	Shape::GetLocalTM() const
	{
		return m_LocalTM;
	}
	
	//------------------------------------------------------------------------------
	/**
	*/
	inline bool
	Shape::IsAttached() const
	{
		return m_IsAttached;
	}
} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_SHAPE_H__