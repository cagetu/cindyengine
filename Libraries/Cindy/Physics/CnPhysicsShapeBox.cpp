//================================================================
// File:               : CnPhysicsShapeBox.cpp
// Related Header File : CnPhysicsShapeBox.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsShapeBox.h"

namespace Cindy
{
namespace Physics
{
	__ImplementClass(Physics, BoxShape, Shape);
	//------------------------------------------------------------------------------
	/**
	*/
	BoxShape::BoxShape( const char* Name )
		: Physics::Shape( Name )
		, m_Dimenstion(1.0f, 1.0f, 1.0f)
	{
	}
	BoxShape::~BoxShape()
	{
	}

	//------------------------------------------------------------------------------
	/**
	*/
	bool BoxShape::Attach( NxActorDesc& ActorDesc )
	{
		if (Shape::Attach( ActorDesc ))
		{
			NxBoxShapeDesc desc;
			desc.name = m_Name.c_str();
			Float3ToPhysX( m_Dimenstion, desc.dimensions );
			Matrix44ToPhysX( m_LocalTM, desc.localPose );
			
			ActorDesc.shapes.push_back( &desc );

			m_IsAttached = true;
			return true;
		}
		return false;
	}

} // namespace Physics
} // namespace Cindy
