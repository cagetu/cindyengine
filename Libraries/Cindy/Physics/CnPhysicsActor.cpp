//================================================================
// File:               : CnPhysicsActor.cpp
// Related Header File : CnPhysicsActor.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsActor.h"
#include "CnPhysicsScene.h"
#include "CnPhysicsManager.h"

namespace Cindy
{
namespace Physics
{
	__ImplementClass(Physics, Actor, CnObject);

	ulong Physics::Actor::m_UniqueIdCounter = 1;
	//------------------------------------------------------------------------------
	/**
	*/
	Actor::Actor()
		: m_Owner(0)
		, m_Active(false)
	{
		m_UniqueId = m_UniqueIdCounter++;

		Physics::Manager::Instance()->RegisterEntity( this );
	}
	Actor::~Actor()
	{
		if (m_Actor)
		{
			OnDeactivate();
		}

		Physics::Manager::Instance()->UnregisterEntity( this );
	}

	//------------------------------------------------------------------------------
	/**	활성화...
		@desc	여기서 Actor 객체와 Shape들을 PhysX에 등록
	*/
	void Actor::OnActivate()
	{
		assert(!m_Actor);

		if (!m_Composite->IsAttached())
		{
			m_Composite->Attach( m_ActorDesc );
		}

		m_Actor = m_Owner->GetNxScene()->createActor( m_ActorDesc );

		m_Active = true;
	}

	//------------------------------------------------------------------------------
	/**	비활성화
		@desc	PhysX에서 Actor를 비활성화 한다.
	*/
	void Actor::OnDeactivate()
	{
		assert(m_Active);

		if (m_Composite->IsAttached())
		{
			m_Composite->Detach();
		}

		m_Owner->GetNxScene()->releaseActor( *m_Actor );
		m_Actor = 0;

		// NOTE: WE DON'T RELEASE THE COMPOSITE, INSTEAD WE KEEP
		// THE ENTITY FULLY INTACT HERE ON PURPOSE!!
		m_Active = false;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Actor::OnAttachedToScene( Physics::Scene* Scene )
	{
		m_Owner = Scene;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Actor::OnRemoveFromScene()
	{
		m_Owner = 0;
	}

	//------------------------------------------------------------------------------
	/**	Static Actor는 변환이 불가능하다.
	*/
	void Actor::SetTransform( const Math::Matrix44& Transform )
	{
		if (m_Actor)
		{
			if (m_Actor->isDynamic())
			{
				NxMat34 mat;
				Matrix44ToPhysX( Transform, mat );

				m_Actor->setGlobalPose( mat );

				m_WorldTM = m_WorldTM;
			}
			else
			{
				/// Static Actor는 변환이 불가능하다...
			}
		}
		else
		{
			m_WorldTM = Transform;
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Actor::SetComposite( const Ptr<Physics::Composite>& Composite )
	{
		m_Composite = Composite;
		m_Composite->SetEntity( this );
	}

} // namespace Physics
} // namespace Cindy
