//================================================================
// File:               : CnPhysicsNotify.cpp
// Related Header File : CnPhysicsNotify.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsNotify.h"

namespace Cindy
{
namespace Physics
{
	//------------------------------------------------------------------------------
	/**
	*/
	void Notify::onWake( NxActor** actors, NxU32 count )
	{
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Notify::onSleep( NxActor** actors, NxU32 count)
	{
	}

	//------------------------------------------------------------------------------
	/**
	*/
	bool Notify::onJointBreak( NxReal breakingImpulse, NxJoint& brokenJoint )
	{
		return false;
	}

} // namespace Physics
} // namespace Cindy
