//================================================================
// File:           : CnPhysicsLog.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_LOG_H__
#define __CN_PHYSICS_LOG_H__

#include "CnPhysics.h"

namespace Cindy
{
namespace Physics
{
	//================================================================
	/** Physics Log
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Error ����
	*/
	//================================================================
	class Log : public NxUserOutputStream
	{
		virtual void reportError(NxErrorCode code, const char * message, const char *file, int line)
		{
		}

		virtual NxAssertResponse reportAssertViolation(const char * message, const char *file, int line)
		{
			return NX_AR_CONTINUE;
		}

		virtual void print(const char * message)
		{
		}
	};

} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_LOG_H__