//================================================================
// File:               : CnPhysicsShapeSphere.cpp
// Related Header File : CnPhysicsShapeSphere.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsShapeSphere.h"
#include "CnPhysicsActor.h"

namespace Cindy
{
namespace Physics
{
	__ImplementClass(Physics, SphereShape, Shape);
	//------------------------------------------------------------------------------
	/**
	*/
	SphereShape::SphereShape( const char* Name )
		: Physics::Shape( Name )
		, m_Radius(1.0f)
	{
	}
	SphereShape::~SphereShape()
	{
	}

	//------------------------------------------------------------------------------
	/**
	*/
	bool SphereShape::Attach( NxActorDesc& ActorDesc )
	{
		if (Shape::Attach( ActorDesc ))
		{
			NxSphereShapeDesc desc;
			desc.name = m_Name.c_str();
			desc.radius = m_Radius;
			Matrix44ToPhysX( m_LocalTM, desc.localPose );

			ActorDesc.shapes.push_back( &desc );
			return true;
		}
		return false;
	}

} // namespace Physics
} // namespace Cindy
