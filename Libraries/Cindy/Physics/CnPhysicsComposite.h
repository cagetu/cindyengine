//================================================================
// File:           : CnPhysicsComposite.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_COMPOSITE_H__
#define __CN_PHYSICS_COMPOSITE_H__

#include "CnPhysicsShape.h"

namespace Cindy
{
namespace Physics
{
	class Scene;

	//================================================================
	/** Physics Composite
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Shape, Joint, Body들에 대한 조합 객체
	*/
	//================================================================
	class Composite : public CnObject
	{
		__DeclareClass(Composite);
	public:
		Composite();
		virtual ~Composite();

		/// Shape
		void						AddShape( const Ptr<Physics::Shape>& Shape );
		void						ClearShapes();
		const Ptr<Physics::Shape>&	GetShape( int Index ) const;
		int							GetNumShapes() const;

		/// Actor
		void						SetEntity( Physics::Actor* Actor );
		Physics::Actor*				GetEntity() const;

		///
		virtual void				Attach( NxActorDesc& ActorDesc );
		virtual void				Detach();

		///
		bool						IsAttached() const;
	protected:
		typedef std::vector<Ptr<Physics::Shape> >	ShapeArray;
		//------------------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		Physics::Actor*	m_Actor;

		ShapeArray		m_Shapes;
		ushort			m_NumShapes;
		
		bool			m_IsAttached;
	};
	
	//------------------------------------------------------------------------------
	/**
	*/
	inline
	int Composite::GetNumShapes() const
	{
		return m_NumShapes;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	bool Composite::IsAttached() const
	{
		return m_IsAttached;
	}
} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_COMPOSITE_H__