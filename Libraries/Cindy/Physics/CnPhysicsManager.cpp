//================================================================
// File:               : CnPhysicsManager.cpp
// Related Header File : CnPhysicsManager.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsManager.h"

namespace Cindy
{
namespace Physics
{
	__ImplementSingleton(Manager);
	//------------------------------------------------------------------------------
	Manager::Manager()
		: m_PhysicsSDK(0)
	{
		__ConstructSingleton;
	}
	//------------------------------------------------------------------------------
	Manager::~Manager()
	{
		__DestructSingleton;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Manager::Open()
	{
		// Create the physics SDK
		m_PhysicsSDK = NxCreatePhysicsSDK( NX_PHYSICS_SDK_VERSION, &m_Allocator, &m_ErrorReporter );
		if (!m_PhysicsSDK)
			return;

		// global physics 파라미터인데 SDK에서 표면 폭으로 불려 쓰여진다. 
		// 이것은 중요한 파라미터인데 기본적으로 "grace-depth" 각기 다른 물체들에 관통을 허락하는 정도를 뜻하는거 같다(?) 
		// 0.01의로 설정했다(절대 깊이로 0.01m 또는 1cm) 
		// 만일 이 코드라인을 추가하지 않으면 기본적으로 0.025가 설정이 된다( 0.025m or 2.5cm ) 
		// NX_SKIN_WIDTH 는 물체들의 표면 깊이에 대한 설정입니다. 
		// 물체간에 충돌이 일어났을 때, 어느 깊이까지 충돌을 허용하겠냐는 의미입니다. 
		// 이 설정에는 0.01m(1cm)까지는 Overlap을 허용하고 그 이상은 Collision이 일어나는 것입니다.. 

		// default
		SetParameter( NX_SKIN_WIDTH, 0.01f );

		/* Set the debug visualization parameters */
		SetParameter( NX_VISUALIZATION_SCALE,		 1.0f );
		SetParameter( NX_VISUALIZE_COLLISION_SHAPES, 1.0f );
		SetParameter( NX_VISUALIZE_ACTOR_AXES,		 1.0f );
		
		// 모든 재질에 마찰력 스케일링을 적용할 수 있다.
		// 기본 1 상태에서 0.5 스케일링을 하여 더 잘 미끄러지게 할수 있다.
		// 점차적으로 다시 원상태로 돌아온다 ? 일회성?
		// 게임에서 얼음판상태가 되었을때 사용할 수 있다.
		//m_PhysicsSDK->setParameter(NX_DYN_FRICT_SCALING, 0.5);
		//m_PhysicsSDK->setParameter(NX_STA_FRICT_SCALING, 0.5);

		// relative velocity <상대속도> 
		// 어떤 물체에서 다른 물체를 본 상대적인 속도이다.
		// 관찰자가 등속운동중일 때는 자신이 정지해 있다고 느끼므로 어떤 물체가 관찰자가 되는가에
		// 따라 상대속도는 변한다. 상대성원리는 이것을 체계화한 것이다
		// 높은 탄성력을 가진 오브젝트에 쓸만한 기능임
		//m_PhysicsSDK->setParameter(NX_BOUNCE_THRESHOLD, -100);

		m_PhysicsSDK->getFoundationSDK().getRemoteDebugger()->connect ("localhost", 5425);
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Manager::Close()
	{
		if (m_PhysicsSDK)
		{
			m_PhysicsSDK->release();
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Manager::SetParameter( NxParameter Parameter, float Value )
	{
		m_PhysicsSDK->setParameter( Parameter, Value );
	}
} // namespace Physics
} // namespace Cindy
