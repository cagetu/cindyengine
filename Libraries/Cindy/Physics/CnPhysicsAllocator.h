//================================================================
// File:           : CnPhysicsAllocator.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_ALLOCATOR_H__
#define __CN_PHYSICS_ALLOCATOR_H__

#include "CnPhysics.h"

namespace Cindy
{
namespace Physics
{
	//================================================================
	/** Physics Allocator
	    @author    changhee
		@since     2010. 1. 28
		@remarks   재정의된 메모리 할당자
	*/
	//================================================================
	class Allocator : public NxUserAllocator
	{
	public:
		virtual void* mallocDEBUG( size_t Size, const char* FileName, int Line )	{
			return ::malloc(Size);
		}

		virtual void* malloc( size_t Size ) {
			return ::malloc(Size);
		}

		virtual void* realloc( void* Memory, size_t Size ) {
			return ::realloc(Memory, Size);
		}

		virtual void free( void* Memory ) {
			::free(Memory);
		}
	};

} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_ALLOCATOR_H__