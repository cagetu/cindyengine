//================================================================
// File:               : CnPhysicsScene.cpp
// Related Header File : CnPhysicsScene.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsScene.h"
#include "CnPhysicsManager.h"

#define SIMULATION_HW

namespace Cindy
{
namespace Physics
{
	__ImplementClass(Physics, Scene, CnObject);
	//------------------------------------------------------------------------------
	/**
	*/
	Scene::Scene()
		: m_Scene(0)
		, m_CurrentTime(0)
		, m_TimeStamp(0)
		, m_NumActors(0)
	{
	}
	Scene::~Scene()
	{
	}

	//------------------------------------------------------------------------------
	/**
		활성화
	*/
	void Scene::OnActivate()
	{
		NxVec3 gravity( m_Gravity.x, m_Gravity.y, m_Gravity.z );

		NxSceneDesc sceneDesc;
		sceneDesc.gravity = gravity;
#ifdef SIMULATION_HW
 		sceneDesc.simType = NX_SIMULATION_HW;
		m_Scene = PhysicsManager->GetSDK()->createScene( sceneDesc );	
		if (!m_Scene)
#endif // SIMULATION_HW
		{
			sceneDesc.simType = NX_SIMULATION_SW; 
			m_Scene = PhysicsManager->GetSDK()->createScene( sceneDesc );  
		}

		// 기본 매터리얼 생성
		NxMaterial* defaultMaterial = m_Scene->getMaterialFromIndex(0);		// 0번 세팅된 재질을 가져 옴
		defaultMaterial->setRestitution( 0.5f );							// 탄성력(회복력) 사용
		defaultMaterial->setStaticFriction( 0.5f );							// 정지 마찰력
		defaultMaterial->setDynamicFriction( 0.5f );						// 운동 마찰력

		//phyScene->setUserNotify(&mActorNotify);
	}

	//------------------------------------------------------------------------------
	/**
		비활성화
	*/
	void Scene::OnDeactivate()
	{
		if (!m_Scene)
		{
			PhysicsManager->GetSDK()->releaseScene( *m_Scene );
			m_Scene = NULL;
		}
	}

	//------------------------------------------------------------------------------
	/**
		Simulation
	*/
	void Scene::Simulate()
	{
		float elapsedTime = m_CurrentTime - m_TimeStamp;
		m_TimeStamp = m_CurrentTime;

		m_Scene->simulate( elapsedTime );
		m_Scene->flushStream();
	}

	//------------------------------------------------------------------------------
	/**
		결과 얻어오기
	*/
	void Scene::GetResults()
	{
		// m_pScene->simulate(gDeltaTime)로 부터 결과를 얻어온다.
		while (!m_Scene->fetchResults( NX_RIGID_BODY_FINISHED, false ));
	}

	//------------------------------------------------------------------------------
	/**
		::업데이트 순서::
		1. fetchResults(Begin)
		2. 각 엔티티 등 동작에 대한 처리
		3. Simulate(End)
		4. Draw
	*/
	void Scene::OnBegin()
	{
		GetResults();
	}

	//------------------------------------------------------------------------------
	/**
		::업데이트 순서::
		1. fetchResults(Begin)
		2. 각 엔티티 등 동작에 대한 처리
		3. Simulate(End)
		4. Draw
	*/
	void Scene::OnEnd()
	{
		Simulate();
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Scene::AddActor( Physics::Actor* Actor )
	{
		Actor->OnAttachedToScene( this );
		Actor->OnActivate();

		m_Actors.push_back( Actor );
		m_NumActors++;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Scene::RemoveActor( Physics::Actor* Actor )
	{
		Actor->OnDeactivate();
		Actor->OnRemoveFromScene();

		ActorArray::iterator iter = std::find( m_Actors.begin(), m_Actors.end(), Actor );
		m_Actors.erase( iter );
		delete (*iter);
		m_NumActors--;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	Physics::Actor* Scene::GetActor( int Index ) const
	{
		return m_Actors[Index];
	}

	//------------------------------------------------------------------------------
	/**
	*/
	int Scene::GetNumActors() const
	{
		return m_NumActors;
	}
} // namespace Physics
} // namespace Cindy
