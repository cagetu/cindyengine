//================================================================
// File:           : CnPhysicsScene.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_SCENE_H__
#define __CN_PHYSICS_SCENE_H__

#include "CnPhysicsActor.h"
#include "Math/CindyMath.h"

namespace Cindy
{
namespace Physics
{
	//================================================================
	/** Physics Scene
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Scene
	*/
	//================================================================
	class Scene : public CnObject
	{
		__DeclareClass(Scene);
	public:
		Scene();
		~Scene();

		void					OnActivate();
		void					OnDeactivate();

		void					OnBegin();
		void					OnEnd();

		///
		NxScene*				GetNxScene() const;

		/// Gravity
		void					SetGravity( const Math::Vector3& Gravity );
		const Math::Vector3&	GetGravity() const;

		/// Time
		void					SetTime( float CurrentTime );
		float					GetTime() const;

		/// Actor
		void					AddActor( Physics::Actor* Actor );
		void					RemoveActor( Physics::Actor* Actor );
		Physics::Actor*			GetActor( int Index ) const;
		int						GetNumActors() const;

	private:
		typedef std::vector<Physics::Actor*>	ActorArray;

		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		NxScene*		m_Scene;

		ActorArray		m_Actors;
		ushort			m_NumActors;

		Math::Vector3	m_Gravity;

		float			m_CurrentTime;
		float			m_TimeStamp;

		//------------------------------------------------------------------------------
		//	Methods
		//------------------------------------------------------------------------------
		void	Simulate();
		void	GetResults();
	};

	//------------------------------------------------------------------------------
	/**
		중력 값 받아오기
	*/
	inline void
	Scene::SetGravity( const Math::Vector3& Gravity )
	{
		m_Gravity = Gravity;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline const Math::Vector3&
	Scene::GetGravity() const
	{
		return m_Gravity;
	}

	//------------------------------------------------------------------------------
	/**
		현재 시간 받아오기
	*/
	inline void
	Scene::SetTime( float CurrentTime )
	{
		m_CurrentTime = CurrentTime;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline float
	Scene::GetTime() const
	{
		return m_CurrentTime;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline NxScene*
	Scene::GetNxScene() const
	{
		return m_Scene;
	}
} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_SCENE_H__