//================================================================
// File:           : CnPhysicsShapeBox.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_SHAPE_BOX_H__
#define __CN_PHYSICS_SHAPE_BOX_H__

#include "CnPhysicsShape.h"

namespace Cindy
{
namespace Physics
{
	//================================================================
	/** Physics Box Shape
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Scene
	*/
	//================================================================
	class BoxShape : public Physics::Shape
	{
		__DeclareClass(BoxShape);
	public:
		BoxShape( const char* Name = 0 );
		virtual ~BoxShape();

		//
		Shape::Type				GetType() const override;

		///
		void					SetDimension( const Math::Vector3& Dimension );
		const Math::Vector3&	GetDimension() const;

		//
		bool					Attach( NxActorDesc& ActorDesc ) override;
	private:
		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		Math::Vector3	m_Dimenstion;
	};
	
	//------------------------------------------------------------------------------
	/**
	*/
	inline Shape::Type
	BoxShape::GetType() const
	{
		return Shape::Box;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline void
	BoxShape::SetDimension( const Math::Vector3& Dimension )
	{
		m_Dimenstion = Dimension;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline const Math::Vector3&
	BoxShape::GetDimension() const
	{
		return m_Dimenstion;
	}
} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_SHAPE_BOX_H__