//================================================================
// File:           : CnPhysicsShapeSphere.h
// Original Author : changhee
// Creation Date   : 2010. 1. 29
//================================================================
#ifndef __CN_PHYSICS_SHAPE_SPHERE_H__
#define __CN_PHYSICS_SHAPE_SPHERE_H__

#include "CnPhysicsShape.h"

namespace Cindy
{
namespace Physics
{
	//================================================================
	/** Physics Sphere Shape
	    @author    changhee
		@since     2010. 1. 28
		@remarks   Physics Scene
	*/
	//================================================================
	class SphereShape : public Physics::Shape
	{
		__DeclareClass(SphereShape);
	public:
		SphereShape( const char* Name = 0 );
		virtual ~SphereShape();
		
		//
		Shape::Type		GetType() const override;

		//
		void			SetRadius( float Radius );
		float			GetRadius() const;

		///
		bool			Attach( NxActorDesc& ActorDesc ) override;
	private:
		//------------------------------------------------------------------------------
		//	Variables
		//------------------------------------------------------------------------------
		float				m_Radius;
	};
	
	//------------------------------------------------------------------------------
	/**
	*/
	inline Shape::Type
	SphereShape::GetType() const
	{
		return Shape::Sphere;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline void
	SphereShape::SetRadius( float Radius )
	{
		m_Radius = Radius;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline float
	SphereShape::GetRadius() const
	{
		return m_Radius;
	}
	
} // namespace Physics
} // namespace Cindy

#endif	// __CN_PHYSICS_SHAPE_SPHERE_H__