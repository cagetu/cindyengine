//================================================================
// File:               : CnPhysicsShape.cpp
// Related Header File : CnPhysicsShape.h
// Original Author     : changhee
// Creation Date       : 2010. 1. 29
//================================================================
#include "../Cindy.h"
#include "CnPhysicsShape.h"
#include "CnPhysicsActor.h"

namespace Cindy
{
namespace Physics
{
	__ImplementClass(Physics, Shape, CnObject);
	//------------------------------------------------------------------------------
	/**
	*/
	Shape::Shape( const char* Name )
		: m_Name( Name )
		, m_IsAttached(false)
	{
	}
	Shape::~Shape()
	{
		if (IsAttached())
		{
			Detach();
		}
	}

	//------------------------------------------------------------------------------
	/**
		Actor가 활성화 될 때, Shape이 Actor에 추가된다. 즉, PhysX에 Attach 된다!!
		(Shape은 Composite에 의해 관리된다.)
	*/
	bool Shape::Attach( NxActorDesc& ActorDesc )
	{
		assert(!m_IsAttached);
		return true;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	void Shape::Detach()
	{
		assert(m_IsAttached);
	}
	
} // namespace Physics
} // namespace Cindy
