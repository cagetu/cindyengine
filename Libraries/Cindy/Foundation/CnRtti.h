// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNRTTI_H__
#define __CNRTTI_H__

#include "../Cindy.h"

namespace Cindy
{
	class CnObject;

	//==================================================================
	/** CnRtti
		@author
			cagetu
		@since
			2006년 9월 22일
		@remarks
			Run-time type information system 객체
		@see
			3D Game Engine Architecture( by Eberly ) - WildMagic3
	*/
	//==================================================================
	class CN_DLL CnRtti
	{
	public:
		typedef CnObject*	(*Creator)();
		//typedef void		(*Destroyer)(CnObject*);

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		/** 생성자
			@param rName
				객체 이름
			@param pBase
				부모 클래스
			@param pFnCreator
				생성자 클래스
		*/
		CnRtti( const char* className, const CnRtti* pBaseType, Creator pFnCreator = 0 /**, Destroyer pFnDestroyer = 0*/ );

		/** 타입 이름 / 기본 타입
		*/
		const std::string&	GetClassName() const;
		const CnRtti*		GetBaseType() const;

		/** Create
		*/
		CnObject*			Create() const;
		//void				Destory( CnObject* pObject );

		/** Hierarchy
		*/
		bool				IsInstanceOf( const CnRtti& Type ) const;
		bool				IsInstanceOf( const std::string& TypeName ) const;
		
		bool				IsA( const CnRtti& Type ) const;
		bool				IsA( const std::string& TypeName ) const;

		/** operator
		*/
		bool operator ==( const CnRtti& Rhs ) const;
		bool operator ==( const CnRtti* Rhs ) const;
	private:
		std::string			m_ClassName;		//!< 타입 이름
		const CnRtti*		m_pBaseType;		//!< 부모 타입

		const Creator		m_pCreator;			//!< 생성자
		//const Destroyer		m_pDestroyer;		//!< 소멸자
	};
}

#include "CnRtti.inl"

#endif	// __CNRTTI_H__