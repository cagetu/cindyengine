#pragma once

#include "CnPropertyTypes.h"

namespace Cindy
{
	//==================================================================
	/** Abstract Property
		@author		cagetu
		@remarks	모든 Property 클래스의 기본 클래스
					GPG5 1.4
	*/
	//==================================================================
	class IProperty
	{
	public:
		IProperty( const char* Name );

		const char*						GetName() const;
		virtual PropertyTypeID::Enum	GetType() const abstract;

	protected:
		const char*		m_Name;	// Property Name
	};
}

#include "CnPropertyBase.inl"