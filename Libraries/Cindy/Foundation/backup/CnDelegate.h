#pragma once

#include "CnObject.h"

namespace Cindy
{
	//==================================================================
	/** Delegate/Event System
		@author		cagetu
		@desc	   Insomniac C++ Delegate/Event System
				  
				   Classes:
				  
				    Signature is a template that contains all classes for a given function
				     signature.  Typedef Signature as a starting point for use in code.
				  
				    Delegate is an encapsulation of a function that matches the signature.
				     Delegate::Function delegates invocation to a standard C function.
				     Delegate::Method delegates invocation to a member function.
				  
				    Event is a set of delegates that are invoked together.
				  
				   Comments:
				  
				    The reliance on internally allocated worker (Impl) classes is there to
				     allow for experimentation with memory allocation without having to
				     refactor tons of code to test changes.  Since the allocation is masked
				     client code will never call new itself.  There is lots of overloading
				     and template member functions to support as much efficiency as possible.
				     Utilize template member functions over Delegate prototypes to prevent
				     needless heap thrashing (creation, comparison, and deletion of internal
				     DelegateImpl objects).
				  
				   Usage:
				  
				    struct Args {};
				  
				    void Function( Args args ) {}
				  
				    class Foo
				    {
				      void Method( Args args ) {}
				    };
				  
				    // this explicitly instantiates all the classes required for a signature
				    typedef Nocturnal::Signature<void, Args> ExampleSignature;
				  
				    ExampleSignature::Event g_Event;
				    Foo                     g_Foo;
				  
				    g_Event.Add( &Function  );
				    g_Event.Add( &g_Foo, &Foo::Method ) );
				    g_Event.Raise( Args () );
				    g_Event.Remove( &Function  );
				    g_Event.Remove( &g_Foo, &Foo::Method ) );
				  
				   To Do:
				  
				    * Add support for stl or 'Nocturnal' allocators in place of C++ heap
				    * Elminate heap usage in Delegate with some horrific unions:
				       http://www.codeproject.com/cpp/fastdelegate2.asp
				  
				  ////////////////////////////////////////////////////////////////////////
	*/
	//==================================================================
	namespace DelegateTypes
	{
		enum DelegateType
		{
			Function,
			Method,
		};
	}
	typedef DelegateTypes::DelegateType DelegateType;

	//==================================================================
	/**	Signature
	*/
	//==================================================================
	template <typename ReturnType, typename ParameterType>
	class Signature
	{
	public:
		class Implement;
		class Function;
		class Method;

		/// Delegate
		class Delegate
		{
		public:
			/// Construct
			Delegate();
			Delegate( const Delegate& Other );

			template <typename FunctionType>
			Delegate( FunctionType function )
			{
				m_Impl = new Function( function );
			}
			template <typename ClassType, typename MethodType>
			Delegate( ClassType* instance, MethodType method )
			{
				m_Impl = new Method<ClassType>( instance, method );
			}

			/// Invoke
			ReturnType Invoke( ParameterType Param ) const;

			void Clear();
			bool IsValid() const;

			void Set( const Delegate& Other );
			
			template <typename FunctionType>
			void Set( FunctionType function )
			{
				m_Impl = new Function( function );
			}

			template <typename ClassType, typename MethodType>
			void Set( ClassType* instance, MethodType method )
			{
				m_Impl = new Method<ClassType>( instance, method );
			}
		
		private:
			//-----------------------------------------------------------------------------
			//	Internal Classes
			//-----------------------------------------------------------------------------
			class Implement : public CnRefCount
			{
				friend class Delegate;
			public:
				virtual DelegateType GetType() const = 0;
				virtual bool Equals( const Implement* Other ) = 0;
				virtual ReturnType Invoke( ParameterType Param ) = 0;
			};

			class Function : public Implement
			{
			private:
				friend class Delegate;

				typedef ReturntType (*FunctionType)(ParameterType);
				FunctionType _Function;

			public:
				Function( FunctionType function );

				DelegateType GetType() const override;
				bool Equals( const Implement* Other ) override;
				ReturnType Invoke( ParameterType Param ) override;
			};

			template <class ClassType>
			class Method : public Implement
			{
			private:
				friend class Delegate;

				typedef ReturntType (ClassType::*MethodType)(ParameterType);
				ClassType*	_Function;
				MethodType	_Method;

			public:
				Method( ClassType* instance, MethodType method );

				DelegateType GetType() const override;
				bool Equals( const Implement* Other ) override;
				ReturnType Invoke( ParameterType Param ) override;
			};

			//-----------------------------------------------------------------------------
			//	Variables
			//-----------------------------------------------------------------------------
			Pointer<Implement>	m_Impl;
		};
	};

#include "CnDelegate.inl"
}