#pragma once

#include "CnPropertyBase.h"

namespace Cindy
{
	class CnObject;

	//==================================================================
	/** CnTypedProperty
		@author		cagetu
		@remarks	This intermediate class defines a property that is typed
					,but not bound as a member of a particular class.
					GPG5 1.4
	*/
	//==================================================================
	template <class T>
	class CnTypedProperty : public IProperty
	{
	public:
		CnTypedProperty( const char* Name );

		// Type
		virtual PropertyTypeID::Enum	GetType() const override;

		// ��
		virtual T		GetValue( CnObject* pOwner ) const abstract;
		virtual void	SetValue( CnObject* pOwner, T Value ) abstract;
	};

	// Int Property
	class IntProperty : public CnTypedProperty<int>
	{
	public:
		IntProperty( const char* Name )
			: CnTypedProperty<int>(Name) {}
	};

	// Float Property
	class FloatProperty : public CnTypedProperty<float>
	{
	public:
		FloatProperty( const char* Name )
			: CnTypedProperty<float>(Name) {}
	};

	// Boolean Property
	class BoolProperty : public CnTypedProperty<bool>
	{
	public:
		BoolProperty( const char* Name )
			: CnTypedProperty<bool>(Name) {}
	};

	// Vector
	class Vector3Property : public CnTypedProperty<const CnVector3&>
	{
	public:
		Vector3Property( const char* Name )
			: CnTypedProperty<const CnVector3&>(Name) {}
	};
	class Vector4Property : public CnTypedProperty<const CnVector4&>
	{
	public:
		Vector4Property( const char* Name )
			: CnTypedProperty<const CnVector4&>(Name) {}
	};

	// Matrix
	class Matrix44Property : public CnTypedProperty<const CnMatrix44&>
	{
	public:
		Matrix44Property( const char* Name )
			: CnTypedProperty<const CnMatrix44&>(Name) {}
	};

	// String
	class StringProperty : public CnTypedProperty<const std::string&>
	{
	public:
		StringProperty( const char* Name )
			: CnTypedProperty<const std::string&>(Name) {}
	};
	class CharProperty : public CnTypedProperty<const char*>
	{
	public:
		CharProperty( const char* Name )
			: CnTypedProperty<const char*>(Name) {}
	};

	// WString
	class WStringProperty : public CnTypedProperty<const String&>
	{
	public:
		WStringProperty( const char* Name )
			: CnTypedProperty<const String&>(Name) {}
	};
	class WCharProperty : public CnTypedProperty<const wchar_t*>
	{
	public:
		WCharProperty( const char* Name )
			: CnTypedProperty<const wchar_t*>(Name) {}
	};

	//// Object
	//class ObjectProperty : public CnTypedProperty<CnObject*>
	//{
	//public:
	//	ObjectProperty( const char* Name )
	//		: CnTypedProperty<CnObject*>(Name) {}
	//};
}

#include "CnPropertyTyped.inl"