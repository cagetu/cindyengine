#pragma once

#include "Property.h"

namespace Foundation
{
	class Rtti;

	//==================================================================
	/** PropertyTable
		@author		cagetu
		@remarks	Property를 가지고 있는 Table
					단일 객체 타입이 단일 PropertyTable를 가지고 있다.
					만약, 상속 구조로 되어 있어서, 부모 객체가 PropertyTable을
					가지고 있다면, 부모 PropertyTable을 알고 있어야 한다.
					마치 RTTI의 구현 방식과 동일하다.
	*/
	//==================================================================
	class PropertyTable
	{
	public:
		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		PropertyTable();
		~PropertyTable();

		void				Register( const char* Name, AbstractProperty* Prop );
		AbstractProperty*	Lookup( const char* Name ) const;
	private:
		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		typedef stdext::hash_map<std::string, AbstractProperty*>	PropertyList;

		PropertyList	m_Properties;
	};
}