// Copyright (c) 2008~. cagetu
//
//******************************************************************
#include "../../Cagetu.h"
#include "PropertyTable.h"

namespace Foundation
{
	// Const/Dest
	PropertyTable::PropertyTable()
	{
	}
	PropertyTable::~PropertyTable()
	{
		PropertyList::iterator i, iend;
		iend = m_Properties.end();
		for (i=m_Properties.begin(); i != iend; ++i)
			delete (i->second);
		m_Properties.clear();
	}

	//-----------------------------------------------------------------------------
	/** @brief	��� */
	//-----------------------------------------------------------------------------
	void PropertyTable::Register( const char* Name, AbstractProperty* Prop )
	{
		PropertyList::iterator iter = m_Properties.find( Name );
		assert (iter == m_Properties.end());

		m_Properties.insert( PropertyList::value_type( Name, Prop ) );
	}

	//-----------------------------------------------------------------------------
	/** @brief	ã�� */
	//-----------------------------------------------------------------------------
	AbstractProperty* PropertyTable::Lookup( const char* Name ) const
	{
		PropertyList::const_iterator iter = m_Properties.find( Name );
		if (iter != m_Properties.end())
			return iter->second;

		return NULL;
	}
}