namespace Cindy
{
	//--------------------------------------------------------------
	template <class T>
	inline CnTypedProperty<T>::CnTypedProperty( const char* Name )
		: IProperty( Name )
	{
	}
	//--------------------------------------------------------------
	template <class T>
	PropertyTypeID::Enum CnTypedProperty<T>::GetType() const
	{
		return PropertyType<T>::GetTypeID();
	}
}