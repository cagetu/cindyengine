template <typename ReturnType, typename ParameterType>
Signature<ReturnType, ParameterType>::Delegate::Delegate()
{
}

template <typename ReturnType, typename ParameterType>
Signature<ReturnType, ParameterType>::Delegate::Delegate( const Delegate& Other )
: m_Impl( Other.m_Impl )
{
}

template <typename ReturnType, typename ParameterType>
Signature<ReturnType, ParameterType>::Delegate::Delegate()
{
}

template <typename ReturnType, typename ParameterType>
ReturnType Signature<ReturnType, ParameterType>::Delegate::Invoke( ParameterType Param ) const
{
    if (!m_Impl.IsNull())
    {
		return m_Impl->Invoke( Param );
    }
    else
    {
		return ReturnType ();
    }
}

template <typename ReturnType, typename ParameterType>
void Signature<ReturnType, ParameterType>::Delegate::Clear()
{
	m_Impl = 0;
}

template <typename ReturnType, typename ParameterType>
bool Signature<ReturnType, ParameterType>::Delegate::IsValid() const
{
	return !m_Impl.IsNull();
}

template <typename ReturnType, typename ParameterType>
bool Signature<ReturnType, ParameterType>::Delegate::Set( const Delegate& Other )
{
	m_Impl = Other.m_Impl;
}
