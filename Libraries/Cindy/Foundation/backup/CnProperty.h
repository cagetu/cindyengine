#pragma once

#include "CnPropertyTyped.h"

namespace Cindy
{
	//==================================================================
	/** Property
		@author		cagetu
		@remarks	외부에서 접근 가능한 Property 객체
					(by C# or Java)
	*/
	//==================================================================
	template <class OwnerType, class T>
	class CnProperty : public CnTypedProperty<T>
	{
	public:
		// Getter/Setter
		typedef T		(OwnerType::*pfnGetter)() const;
		typedef void	(OwnerType::*pfnSetter)( T Value );

		CnProperty( const char* Name, pfnGetter Getter, pfnSetter Setter );

		// 값 
		virtual T		GetValue( CnObject* Owner ) const override;
		virtual void	SetValue( CnObject* Owner, T Value ) override;

	protected:
		pfnGetter	m_Getter;
		pfnSetter	m_Setter;
	};
}

#include "CnProperty.inl"