namespace Cindy
{
	template <class OwnerType, class T>
	CnProperty<OwnerType, T>::CnProperty( const char* Name, pfnGetter Getter, pfnSetter Setter )
		: CnTypedProperty<T>(Name)
		, m_Getter(Getter)
		, m_Setter(Setter)
	{
	}

	template <class OwnerType, class T>
	T CnProperty<OwnerType, T>::GetValue( CnObject* Owner ) const
	{
		return (((OwnerType*)Owner)->*m_Getter)();
	}

	template <class OwnerType, class T>
	void CnProperty<OwnerType, T>::SetValue( CnObject* Owner, T Value )
	{
		if (!m_Setter)
		{
			//assert( false );
			return;
		}
		(((OwnerType*)Owner)->*m_Setter)( Value );
	}
}