// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
// insert in root class declaration
#define CN_DECLARE_ROOT_RTTI(classname) \
public: \
    static CnRtti RTTI; \
    virtual CnRtti* GetRTTI() const	{	return &RTTI;	} \
    static bool IsExactOf(const CnRtti* pRTTI, const classname* pObject) \
    { \
        if (!pObject) \
        { \
            return false; \
        } \
        return pObject->IsExactOf(pRTTI); \
    } \
    \
    bool IsExactOf(const CnRtti* pRTTI) const \
    { \
        return (GetRTTI() == pRTTI); \
    } \
    \
    static bool IsDerivedOf(const CnRtti* pRTTI, const classname* pObject) \
    { \
        if (!pObject) \
        { \
            return false; \
        } \
        return pObject->IsDerivedOf(pRTTI); \
    } \
    \
    bool IsDerivedOf(const CnRtti* pRTTI) const \
    { \
        const CnRtti* pType = GetRTTI(); \
        while (pType) \
        { \
            if (pType == pRTTI) \
            { \
                return true; \
            } \
            pType = pType->GetBaseType(); \
        } \
        return false; \
    } \
    \
    static classname* DynamicCast(const CnRtti* pRTTI, const classname* pObject) \
    { \
        if(!pObject) \
        { \
            return 0; \
        } \
        return pObject->DynamicCast(pRTTI); \
    } \
    \
    classname* DynamicCast(const CnRtti* pRTTI) const \
    { \
        return (IsDerivedOf(pRTTI) ? (classname*) this : 0 ); \
    }

//--------------------------------------------------------------
// insert in root class source file
#define CN_IMPLEMENT_ROOT_RTTI(rootclassname) \
    CnRtti rootclassname::RTTI(#rootclassname, 0)
	
//--------------------------------------------------------------
#define CN_DECLARE_RTTI \
public: \
    static CnRtti RTTI; \
    virtual CnRtti* GetRTTI() const { return &RTTI; }
    
//--------------------------------------------------------------
#define CN_DECLARE_TEMPLATE_RTTI \
public: \
    CN_DLL static CnRtti RTTI; \
    virtual CnRtti* GetRTTI() const { return &RTTI; }
    
//--------------------------------------------------------------
#define CN_IMPLEMENT_RTTI(nsname, classname, baseclassname) \
    CnRtti classname::RTTI(#nsname"@"#classname, &baseclassname::RTTI)
	
//--------------------------------------------------------------
#define CN_IMPLEMENT_TEMPLATE_RTTI(nsname, classname, baseclassname) \
    template <> \
    CnRtti classname::RTTI(#nsname"@"#classname, &baseclassname::RTTI)

//--------------------------------------------------------------
#define CN_DECLARE_RTTI_FACTORY(classname)	\
public: \
    static CnRtti RTTI; \
    static CnObject*	FactoryCreator();	\
	static classname*	Creator(); \
	static bool			RegisterFactoryFunction();	\
    virtual CnRtti* GetRTTI() const { return &RTTI; } \
private:

#define CN_IMPLEMENT_RTTI_FACTORY(nsname, classname, baseclassname) \
	classname* classname::Creator() \
	{ \
		classname* result = CnNew classname(); \
		return result; \
	} \
	CnObject* classname::FactoryCreator() \
	{ \
		return classname::Creator(); \
	} \
	bool classname::RegisterFactoryFunction() \
	{ \
		if (!CnFactory::Instance()->Has(classname::RTTI.GetName().c_str()) )\
		{ \
			CnFactory::Instance()->Register( &classname::RTTI, classname::RTTI.GetName().c_str() ); \
		} \
		return true; \
	} \
	CnRtti classname::RTTI(#nsname"@"#classname, &baseclassname::RTTI, classname::FactoryCreator);
	
//--------------------------------------------------------------
//	Casting...
//--------------------------------------------------------------
// macro for run-time type casting
#define CnStaticCast(classname, pObject) \
    ((classname*) pObject)
//--------------------------------------------------------------
#define CnDynamicCast(classname, pObject) \
    ((classname*) classname::DynamicCast(&classname::RTTI, pObject))