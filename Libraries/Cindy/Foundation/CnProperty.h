/*-
 * Copyright 2009 Bang Jun-young.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions, and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
	http://bangjunyoung.blogspot.com/2009/05/c.html

class Car
{
public:
	Car() 
	{
		property_init(Speed);
		readonly_property_init(Rpm);
	}

	... 함수 ...
	void setSpeed(int s);
	int getSpeed();
	int getRpm();
	: Getter의 경우, const는 사용불가.

	property(Car, getSpeed, setSpeed, int, Speed);
    readonly_property(Car, getRpm, int, Rpm);
private:
	... 변수 ... 
*/

//
// Vendor-independent C++ Property Implementation
//

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MOGUA_PROPERTY_H_
#define _MOGUA_PROPERTY_H_

#define property(TClass, Get, Set, TProp, Prop) \
        Property<TClass, TProp, &TClass::Get, &TClass::Set> Prop

#define property_init(Prop)     Prop.Init(this)

#define readonly_property(TClass, Get, TProp, Prop) \
        ReadOnlyProperty<TClass, TProp, &TClass::Get> Prop

#define readonly_property_init  property_init

template <
    typename TClass,
    typename TProp,
    TProp (TClass::*get)()>
class ReadOnlyProperty
{
public:
    // Property initializer
    void Init(TClass* instance)
    {
        this->instance = instance;
    }

    // Function for assignment from the property
    operator TProp()
    {
        return (this->instance->*get)();
    }

protected:
    TClass* instance;
};

template <
    typename TClass,
    typename TProp,
    TProp (TClass::*get)(),
    void (TClass::*set)(TProp)>
class Property : public ReadOnlyProperty<TClass, TProp, get>
{
public:
    // Function for assignment to the property
    TProp operator =(const TProp& rhs)
    {
        (this->instance->*set)(rhs);
        return rhs;
    }

    // Function for assignment between properties of the same type (and name)
    Property& operator =(const Property& rhs)
    {
        (this->instance->*set)((rhs.instance->*get)());
        return *this;
    }

    // Prefix increment operator
    Property& operator ++()
    {
        TProp value = (this->instance->*get)();
        ++value;
        (this->instance->*set)(value);
        return *this;
    }

    // Postfix increment operator
    TProp operator ++(int)
    {
        TProp temp, value;
        temp = value = (this->instance->*get)();
        value++;
        (this->instance->*set)(value);
        return temp;
    }

    TProp operator +=(TProp rhs)
    {
        TProp value;
        value = (this->instance->*get)();
        value += rhs;
        (this->instance->*set)(value);
        return value;
    }

    // Prefix decrement operator
    Property& operator --()
    {
        TProp value = (this->instance->*get)();
        --value;
        (this->instance->*set)(value);
        return *this;
    }

    // Postfix decrement operator
    TProp operator --(int)
    {
        TProp temp, value;
        temp = value = (this->instance->*get)();
        value--;
        (this->instance->*set)(value);
        return temp;
    }

    TProp operator -=(TProp rhs)
    {
        TProp value;
        value = (this->instance->*get)();
        value -= rhs;
        (this->instance->*set)(value);
        return value;
    }
};

#endif // !_MOGUA_PROPERTY_H_

