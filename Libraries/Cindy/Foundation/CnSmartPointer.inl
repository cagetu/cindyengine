// Copyright (c) 2006~. cagetu
//
//******************************************************************

//--------------------------------------------------------------
template <class T>
Ptr<T>::Ptr()
{
	m_pObject = 0;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::Ptr( T* pObject )
{
	m_pObject = pObject;

	if( m_pObject )
		m_pObject->AddRef();
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::Ptr( const Ptr& spObject )
{
	m_pObject = spObject.m_pObject;

	if( m_pObject )
		m_pObject->AddRef();
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::~Ptr()
{
	if( m_pObject )
		m_pObject->Release();
}

//--------------------------------------------------------------
template <class T>
void Ptr<T>::SetNull()
{
	if( m_pObject )
	{
		m_pObject->Release();
		m_pObject = 0;
	}
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::IsNull() const
{
	return ( 0 == m_pObject );
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::IsValid() const
{
	return (0 != m_pObject);
}

//--------------------------------------------------------------
template <class T>
T* Ptr<T>::GetPtr() const
{
	return m_pObject;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::operator T*() const
{
	return m_pObject;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::operator const T*() const
{
	return m_pObject;
}

//--------------------------------------------------------------
template <class T>
T& Ptr<T>::operator* () const
{
	assert( m_pObject );
	return *m_pObject;
}

//--------------------------------------------------------------
template <class T>
T* Ptr<T>::operator-> () const
{
	assert( m_pObject );
	return m_pObject;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>& Ptr<T>::operator= ( T* pObject )
{
	if( m_pObject != pObject )
	{
		if( m_pObject )
			m_pObject->Release();

		if( pObject )
			pObject->AddRef();

		m_pObject = pObject;
	}
	return *this;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>& Ptr<T>::operator= ( const Ptr& spObject )
{
	if( m_pObject != spObject.m_pObject )
	{
		if( m_pObject )
			m_pObject->Release();

		if( spObject.m_pObject )
			spObject.m_pObject->AddRef();

		m_pObject = spObject.m_pObject;
	}
	return *this;
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator== ( T* pObject ) const
{
	return (m_pObject == pObject);
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator!= ( T* pObject ) const
{
	return (m_pObject != pObject);
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator== ( const Ptr& spObject ) const
{
	return (m_pObject == spObject.m_pObject);
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator!= ( const Ptr& spObject ) const
{
	return (m_pObject != spObject.m_pObject);
}

//--------------------------------------------------------------
/** �ٸ� smart pointer�� cast operator
	@remarks	un-safe casting!!!
*/
//--------------------------------------------------------------
template <class T>
template <class OtherT>
const Ptr<OtherT>& Ptr<T>::Cast() const
{
	return *(Ptr<OtherT>*)this;
}
