// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNOBJECT_H__
#define __CNOBJECT_H__

#include "CnRtti.h"
#include "CnSmartPointer.h"
#include "CnMemObject.h"
#include "CnFactory.h"
#include "CnProperty.h"
#include "../Util/CnRefCount.h"

namespace Cindy
{
	//==================================================================
	/** CnObject
		@author
			cagetu
		@since
			2006년 9월 20일
		@remarks
			객체의 최상위 클래스, 객체가 가져야 하는 기본 속성을 가진다.
			<delete this로 삭제한다는 것은 동적 메모리 할당해서 생성하겠다는 것을 뜻한다.>
		@see
			3D Game Engine Architecture( by Eberly ) - WildMagic3
	*/
	//==================================================================
	class CN_DLL CnObject : public CnRefCount,
							public CnMemLeakObject//public CnMemAllocObject,
							
	{
		__DeclareRootRtti(CnObject);
	public:
		typedef std::map<ulong, CnObject*>	ObjectMap;
		typedef ObjectMap::iterator			ObjectIter;

		static ObjectMap		InUse;						//!< 생성된 객체 리스트 (원하는 시점에서 객체의 존재 여부를 알아온다.)

		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		virtual ~CnObject();

		bool	IsInstanceOf( const CnRtti& Type ) const;
		bool	IsInstanceOf( const std::string& TypeName ) const;
		bool	IsA( const CnRtti& Type ) const;
		bool	IsA( const std::string& TypeName ) const;
	protected:
		/** 생성자
			@remarks
				이 객체만을 사용할 수 없음.
		*/
		CnObject();

		//-----------------------------------------------------------------------------
		//	Variables
		//-----------------------------------------------------------------------------
		ulong			m_ulID;						//!< 객체 고유 ID
		static ulong	ms_ulNextID;				//!< 다음 객체에 할당할 ID
	};

	typedef Ptr<CnObject>	ObjectPtr;

	// casting
	template <class T>	T* StaticCast( CnObject* pObject );
	template <class T>	const T* StaticCast( const CnObject* pObject );
	template <class T>	T* DynamicCast( CnObject* pObject );
	template <class T>	const T* DynamicCast( const CnObject* pObject );
}	// end of namespace

#include "CnObject.inl"

#endif	// __CNOBJECT_H__