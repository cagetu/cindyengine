// Copyright (c) 2006~. cagetu
//
//******************************************************************
namespace Cindy
{
	//-----------------------------------------------------------------------------
	inline
	bool CnObject::IsInstanceOf( const CnRtti& Type ) const
	{
		return this->GetRTTI()->IsInstanceOf( Type );
	}

	inline
	bool CnObject::IsInstanceOf( const std::string& TypeName ) const
	{
		return this->GetRTTI()->IsInstanceOf( TypeName );
	}

	//-----------------------------------------------------------------------------
	inline
	bool CnObject::IsA( const CnRtti& Type ) const
	{
		return this->GetRTTI()->IsA( Type );
	}

	inline
	bool CnObject::IsA( const std::string& TypeName ) const
	{
		return this->GetRTTI()->IsA( TypeName );
	}

	//-----------------------------------------------------------------------------
	template <class T>
	T* StaticCast( CnObject* pObject )
	{
		return (T*)pObject;
	}
	template <class T>
	const T* StaticCast( const CnObject* pObject )
	{
		return (const T*)pObject;
	}
	//-----------------------------------------------------------------------------
	template <class T>
	T* DynamicCast( CnObject* pObject )
	{
		return pObject && pObject->IsDerivedOf(&T::RTTI) ? (T*)pObject : 0;
	}
	template <class T>
	const T* DynamicCast( const CnObject* pObject )
	{
		return pObject && pObject->IsDerivedOf(&T::RTTI) ? (const T*)pObject : 0;
	}
}
