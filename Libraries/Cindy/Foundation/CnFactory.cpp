// Copyright (c) 2008~. cagetu
//
//******************************************************************
#include "CnFactory.h"
#include "CnObject.h"

namespace Cindy
{
	CnFactory* CnFactory::Instance()
	{
		static CnFactory instance;
		return &instance;
	}

	// Const/Dest
	CnFactory::CnFactory()
	{
	}
	CnFactory::~CnFactory()
	{
		m_Table.clear();
	}

	//-------------------------------------------------------------------------
	void CnFactory::Register( const CnRtti* CnRtti, const char* Class )
	{
		std::pair<Table::iterator, bool> result = m_Table.insert( Table::value_type( Class, CnRtti ) );
		assert( result.second );
		//m_Table[Class] = newCreator;
	}

	//-------------------------------------------------------------------------
	bool CnFactory::Has( const char* Class ) const
	{
		Table::const_iterator it = m_Table.find( Class );
		if (it == m_Table.end())
			return false;

		return true;
	}

	//-------------------------------------------------------------------------
	CnObject* CnFactory::Create( const char* Class ) const
	{
		Table::const_iterator it = m_Table.find( Class );
		if (it == m_Table.end())
			return NULL;

		return it->second->Create();
	}
}