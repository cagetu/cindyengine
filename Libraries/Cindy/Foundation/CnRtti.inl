// Copyright (c) 2006~. cagetu
//
//******************************************************************
namespace Cindy
{
	//--------------------------------------------------------------
	inline
	const std::string& CnRtti::GetClassName() const
	{
		return m_ClassName;
	}

	//--------------------------------------------------------------
	inline
	const CnRtti* CnRtti::GetBaseType() const
	{
		return m_pBaseType;
	}

	//--------------------------------------------------------------
	inline
	bool CnRtti::IsInstanceOf( const CnRtti& Type ) const
	{
		return &Type == this;
	}
	inline
	bool CnRtti::IsInstanceOf( const std::string& TypeName ) const
	{
		return TypeName == GetClassName();
	}

	//--------------------------------------------------------------
	inline
	bool CnRtti::operator ==( const CnRtti& Rhs ) const
	{
		return this->GetClassName() == Rhs.GetClassName();
	}
	inline
	bool CnRtti::operator ==( const CnRtti* Rhs ) const
	{
		return this->GetClassName() == Rhs->GetClassName();
	}
}

//******************************************************************
//	��ũ��
//******************************************************************
//--------------------------------------------------------------
// insert in root class declaration
#define __DeclareRootRtti(classname) \
public: \
	static Cindy::CnRtti RTTI; \
    virtual Cindy::CnRtti* GetRTTI() const	{	return &RTTI;	} \
	\
    static bool IsExactOf(const CnRtti* pRTTI, const classname* pObject) \
    { \
        if (!pObject) \
        { \
            return false; \
        } \
        return pObject->IsExactOf(pRTTI); \
    } \
    \
    bool IsExactOf(const CnRtti* pRTTI) const \
    { \
        return (GetRTTI() == pRTTI); \
    } \
    \
    static bool IsDerivedOf(const CnRtti* pRTTI, const classname* pObject) \
    { \
        if (!pObject) \
        { \
            return false; \
        } \
        return pObject->IsDerivedOf(pRTTI); \
    } \
    \
    bool IsDerivedOf(const CnRtti* pRTTI) const \
    { \
        return GetRTTI()->IsA(*pRTTI); \
    } \
    \
private:

//--------------------------------------------------------------
// insert in root class source file
#define __ImplementRootRtti(rootclassname) \
    Cindy::CnRtti rootclassname::RTTI(#rootclassname, 0)
	
//--------------------------------------------------------------
#define __DeclareRtti \
public: \
    static Cindy::CnRtti RTTI; \
    virtual Cindy::CnRtti* GetRTTI() const { return &RTTI; } \
private:
    
//--------------------------------------------------------------
#define __DeclareTemplateRtti \
public: \
    CN_DLL static Cindy::CnRtti RTTI; \
    virtual Cindy::CnRtti* GetRTTI() const { return &RTTI; }
    
//--------------------------------------------------------------
#define __ImplementRtti(nsname, classname, baseclassname) \
    Cindy::CnRtti classname::RTTI(#nsname"@"#classname, &baseclassname::RTTI)
	
//--------------------------------------------------------------
#define __ImplementTemplateRtti(nsname, classname, baseclassname) \
    template <> \
    Cindy::CnRtti classname::RTTI(#nsname"@"#classname, &baseclassname::RTTI)

//--------------------------------------------------------------
#define __DeclareClass(classname)	\
public: \
    static Cindy::CnRtti	RTTI; \
    virtual CnRtti*			GetRTTI() const { return &RTTI; } \
	\
	static classname*		Create(); \
    static Cindy::CnObject*	FactoryCreator();	\
	static bool				RegisterFactoryFunction(); \
private:

#define __ImplementClass(nsname, classname, baseclassname) \
	classname* classname::Create() \
	{ \
		classname* result = CnNew classname(); \
		return result; \
	} \
	Cindy::CnObject* classname::FactoryCreator() \
	{ \
		return classname::Create(); \
	} \
	bool classname::RegisterFactoryFunction() \
	{ \
		if (!Cindy::CnFactory::Instance()->Has(#nsname"@"#classname) ) \
		{ \
			Cindy::CnFactory::Instance()->Register( &classname::RTTI, #nsname"@"#classname ); \
		} \
		return true; \
	} \
	Cindy::CnRtti classname::RTTI(#nsname"@"#classname, &baseclassname::RTTI, classname::FactoryCreator);

#define __RegisterClass(nsname, classname) \
	static const bool factoryRegistered_##nsname"@"#classname = nsname::classname::RegisterFactoryFunction();
