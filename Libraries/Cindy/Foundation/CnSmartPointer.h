// Copyright (c) 2006~. cagetu
//
//******************************************************************

#ifndef __CNSMARTPOINTER_H__
#define __CNSMARTPOINTER_H__

#include "../Cindy.h"

namespace Cindy
{
	//==================================================================
	/** Ptr Class
		@author
			cagetu
		@since
			2005년 2월 28일 월요일
		@remarks
			스마트 포인터 클래스. 
			리소스의 생명주기를 관리, 리소스의 소유권 관리등을 하기 위해 사용.
			CnObject를 이용하여, 객체에 레퍼런스 카운트를 둔다.
			이유는, 스마트 포인터의 크기가 커지기를 원치 않기 때문에
			이건 좀 생각해 봐야 하는데, 스마트 포인터를 사용하기 위해서는
			CnObject를 상속 받은 객체이어야 한다.

		<cf>SmartPointer 명명 규칙은 sp로 하기를 제안한다.
	*/
	//==================================================================
	template <class T>
	class Ptr
	{
	public:
		/** Constructor / Destructor
		*/
		Ptr();
		Ptr( T* pObject );
		Ptr( const Ptr& spObject );
		virtual ~Ptr();

		/** Null 판정
		*/
		void		SetNull();
		bool		IsNull() const;
		bool		IsValid() const;

		/* Get Ptr
		*/
		T*			GetPtr() const;

		/** implicit conversions
		*/
		operator T*() const;
		operator const T*() const;
		T& operator* () const;
		T* operator-> () const;

		/** assignment
		*/
		Ptr& operator= ( T* pObject );
		Ptr& operator= ( const Ptr& spObject );

		/** comparisons
		*/
		bool operator== ( T* pObject ) const;
		bool operator!= ( T* pObject ) const;
		bool operator== ( const Ptr& spObject ) const;
		bool operator!= ( const Ptr& spObject ) const;

		/**	cast
		*/
		template <class OtherT> const Ptr<OtherT>& Cast() const;
	protected:
		T*			m_pObject;			//!< 공유 객체
	};

#include "CnSmartPointer.inl"
}

#endif	// __CNSMARTPOINTER_H__