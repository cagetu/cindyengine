// Copyright (c) 2008~. cagetu
//
//******************************************************************
#pragma once

#include "../Cindy.h"

namespace Cindy
{
	class CnRtti;
	class CnObject;

	//==================================================================
	/** Factory
		@author		cagetu
		@remarks	��ü ������ ��ü
	*/
	//==================================================================
	class CN_DLL CnFactory
	{
	public:
		//-----------------------------------------------------------------------------
		//	Methods
		//-----------------------------------------------------------------------------
		CnObject*	Create( const char* Class ) const;

		void		Register( const CnRtti* RTTI, const char* Class );
		bool		Has( const char* Class ) const;

		static CnFactory*	Instance();
	protected:
		CnFactory();
		~CnFactory();

	private:
		typedef HashMap<std::string, const CnRtti*>	Table;
		
		Table	m_Table;
	};

	// CnFactory
	template <class T>	T* CreateObject();
}

#include "CnFactory.inl"