//================================================================
// File					: CnMemAllocObject.cpp
// Related Header File	: CnMemAllocObject.h
// Original Author		: Changhee
// Creation Date		: 2007. 1. 5.
//================================================================
#include "../Cindy.h"
#include "CnMemObject.h"
#include <MoCommon/Memory/MoMemoryManager.h>

#undef new
#undef delete

#include <exception> // for std::bad_alloc
#include <new>

namespace Cindy
{
	//------------------------------------------------------
	//	MemAlloc Class
	//------------------------------------------------------

#ifdef MO_MEMORY_DEBUG
	// define operator new/delete
	void* 
	CnMemAllocObject::operator new( unsigned int p_nSize )
	{
		throw std::bad_alloc();
	}

	void* 
	CnMemAllocObject::operator new[]( unsigned int p_nSize )
	{
		throw std::bad_alloc();
	}

	void* 
	CnMemAllocObject::operator new( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction )
	{
		using namespace MoCommon;

		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW, true, 
									p_strFile, (unsigned int)p_nLine, p_strFunction );

		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}

	void* 
	CnMemAllocObject::operator new[]( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction )
	{
		using namespace MoCommon;

		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW_ARRAY, false,
									p_strFile, (unsigned int)p_nLine, p_strFunction );
		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}

#else
	void* 
	CnMemAllocObject::operator new( unsigned int p_nSize )
	{
		using namespace MoCommon;

		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW, true,
									"temp", 0, "temp" );
		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}

	void* 
	CnMemAllocObject::operator new[]( unsigned int p_nSize )
	{
		using namespace MoCommon;

		if( 0 == p_nSize )
			p_nSize = 1;

		void* r_ptr = g_MemMgr.Alloc( p_nSize, MO_MEM_ALIGNMENT, MoMemTracer::M_NEW_ARRAY, false,
									"temp", 0, "temp" );
		if( 0 == r_ptr )
			throw std::bad_alloc();

		return r_ptr;
	}
#endif

	void 
	CnMemAllocObject::operator delete( void* p_pAddr, unsigned int p_nSize )
	{
		using namespace MoCommon;

		if( p_pAddr )
		{
			g_MemMgr.Dealloc( p_pAddr, MoMemTracer::M_DELETE, p_nSize );
		}
	}

	void 
	CnMemAllocObject::operator delete[]( void* p_pAddr, unsigned int p_nSize )
	{
		using namespace MoCommon;

		if( p_pAddr )
		{
			g_MemMgr.Dealloc( p_pAddr, MoMemTracer::M_DELETE_ARRAY );
		}
	}

	//------------------------------------------------------
	//	MemAlloc Class
	//------------------------------------------------------

#ifdef MO_MEMORY_DEBUG
	// define operator new/delete
	void* 
	CnMemLeakObject::operator new( unsigned int p_nSize )
	{
		throw std::bad_alloc();
	}

	void* 
	CnMemLeakObject::operator new[]( unsigned int p_nSize )
	{
		throw std::bad_alloc();
	}

	void* 
	CnMemLeakObject::operator new( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction )
	{
		using namespace MoCommon;
		void* r_ptr = ::operator new( p_nSize );
		MoMemTracer::AddTrack( (unsigned long)(r_ptr), (unsigned long)p_nSize, p_strFile, (unsigned long)p_nLine, p_strFunction, MoMemTracer::M_NEW, 0 );
		assert( r_ptr );
		return r_ptr;
	}

	void* 
	CnMemLeakObject::operator new[]( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction )
	{
		void* r_ptr = ::operator new[]( p_nSize );
		MoMemTracer::AddTrack( (unsigned long)r_ptr, (unsigned long)p_nSize, p_strFile, (unsigned long)p_nLine, p_strFunction, MoMemTracer::M_NEW_ARRAY, 0 );
		assert( r_ptr );

		return r_ptr;
	}

#else
	void* 
	CnMemLeakObject::operator new( unsigned int p_nSize )
	{
		void* r_ptr = ::operator new( p_nSize );
		return r_ptr;
	}

	void* 
	CnMemLeakObject::operator new[]( unsigned int p_nSize )
	{
		void* r_ptr = ::operator new[]( p_nSize );
		return r_ptr;
	}
#endif

	void 
	CnMemLeakObject::operator delete( void* p_pAddr, unsigned int p_nSize )
	{
		::operator delete( p_pAddr );
		MoMemTracer::RemoveTrack( (unsigned long)p_pAddr, MoMemTracer::M_DELETE );
	}

	void 
	CnMemLeakObject::operator delete[]( void* p_pAddr, unsigned int p_nSize )
	{
		::operator delete[]( p_pAddr );
		MoMemTracer::RemoveTrack( (unsigned long)p_pAddr, MoMemTracer::M_DELETE_ARRAY );
	}

} // end of namespace 