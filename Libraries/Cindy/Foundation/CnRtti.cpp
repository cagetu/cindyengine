// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnRtti.h"
#include "CnObject.h"
#include "CnFactory.h"

namespace Cindy
{
	//------------------------------------------------------------------
	/**
	*/
	CnRtti::CnRtti( const char* className, const CnRtti* pBaseType, Creator pFnCreator/**, Destroyer pFnDestroyer*/ )
		: m_ClassName( className )
		, m_pBaseType( pBaseType )
		, m_pCreator( pFnCreator )
		//, m_pDestroyer( pFnDestroyer );
	{
		if (m_pCreator)
		{
			if (false == CnFactory::Instance()->Has( className ))
			{
				CnFactory::Instance()->Register( this, className );
			}
		}
	}
	
	//------------------------------------------------------------------
	/**
	*/
	CnObject* CnRtti::Create() const
	{
		if (!m_pCreator)
			return NULL;

		return m_pCreator();
	}
	//void CnRtti::Destory( CnObject* pObject )
	//{
	//	delete pObject;
	//}

	//------------------------------------------------------------------
	/**
	*/
	bool CnRtti::IsA( const CnRtti& Type ) const
	{
		const CnRtti* type = this;
		while (type)
		{
			if (type == &Type)
				return true;

			type = type->m_pBaseType;
		}
		return false;
	}
	
	//------------------------------------------------------------------
	/**
	*/
	bool CnRtti::IsA( const std::string& TypeName ) const
	{
		const CnRtti* type = this;
		while (type)
		{
			if (type->GetClassName() == TypeName)
				return true;

			type = type->m_pBaseType;
		}
		return false;
	}

}