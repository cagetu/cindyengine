// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "CnObject.h"

namespace Cindy
{
	//============================================================================
	__ImplementRootRtti( CnObject );
	//--------------------------------------------------------------
	ulong CnObject::ms_ulNextID = 0;
	CnObject::ObjectMap CnObject::InUse;
	//--------------------------------------------------------------
	//Const/Dest
	CnObject::CnObject()
	{
		m_ulID = ms_ulNextID++;
		InUse.insert( ObjectMap::value_type( m_ulID, this ) );
	}
	//--------------------------------------------------------------
	CnObject::~CnObject()
	{
		ObjectIter iter = InUse.find( m_ulID );
		assert( iter != InUse.end() );
		InUse.erase( iter );
	}
}
