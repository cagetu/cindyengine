//================================================================
// File					: CnMemObject.h			
// Original Author		: Changhee
// Creation Date		: 2007. 1. 10.
//================================================================
#pragma once

#include <MoCommon/Memory/MoMemoryDefines.h>
#include <MoCommon/Memory/MoMemoryPool.h>
#include <MoCommon/Memory/MoMemoryTracer.h>

namespace Cindy
{
//================================================================
/** CnNew
	@remarks	디버깅 용도를 위해 new를 다시 설정한다.
*/
//================================================================
#ifdef MO_MEMORY_DEBUG
	#define CnNew		new(__FILE__, __LINE__, __FUNCTION__)
#else
	#define CnNew		new
#endif

	//================================================================
	/**	Memory Allocate Object
		@author		Changhee
		@since		2007.1.10.
		@remarks	객체의 생성/삭제 여부를 메모리풀에 맡기기 위한 객체
	*/
	//================================================================
	class CN_DLL CnMemAllocObject
	{
#ifdef MO_MEMORY_DEBUG
	private:
		// The memory debugger uses the file, line, function 
        // routines. Therefore new and new[] should be 
        // unavailable to the outer application.
		static void* operator new( unsigned int p_nSize );
		static void* operator new[]( unsigned int p_nSize );
	public:
		static void* operator new( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction );
		static void* operator new[]( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction );

		// Required for exception handling in the compiler. These 
        // should not be used in practice.
		static void operator delete( void* p_pAddr, const char* p_strFile, int p_nLine, const char* p_strFunction )		{}
		static void operator delete[]( void* p_pAddr, const char* p_strFile, int p_nLine, const char* p_strFunction )	{}
#else
	public:
		static void* operator new( unsigned int p_nSize );
		static void* operator new[]( unsigned int p_nSize );
#endif
	public:
		static void operator delete( void* p_pAddr, unsigned int p_nSize );
		static void operator delete[]( void* p_pAddr, unsigned int p_nSize );
	};

	//================================================================
	/**	Memory Leak Object
		@author		Changhee
		@since		2007.1.10.
		@remarks	객체의 Leak 여부를 체크하기 위한 객체
	*/
	//================================================================
	class CN_DLL CnMemLeakObject
	{
#ifdef MO_MEMORY_DEBUG
	private:
		// The memory debugger uses the file, line, function 
        // routines. Therefore new and new[] should be 
        // unavailable to the outer application.
		static void* operator new( unsigned int p_nSize );
		static void* operator new[]( unsigned int p_nSize );
	public:
		static void* operator new( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction );
		static void* operator new[]( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction );

		// Required for exception handling in the compiler. These 
        // should not be used in practice.
		static void operator delete( void* p_pAddr, const char* p_strFile, int p_nLine, const char* p_strFunction )		{}
		static void operator delete[]( void* p_pAddr, const char* p_strFile, int p_nLine, const char* p_strFunction )	{}
#else
	public:
		static void* operator new( unsigned int p_nSize );
		static void* operator new[]( unsigned int p_nSize );
#endif
	public:
		static void operator delete( void* p_pAddr, unsigned int p_nSize );
		static void operator delete[]( void* p_pAddr, unsigned int p_nSize );
	};
}

#ifdef _DEBUG
//--------------------------------------------------------------
// macro
#define DECLARE_MEM_POOL( __type ) \
private: \
	static MoCommon::MoMemoryPool< __type >	ms_MemPool;	\
	static void* operator new( unsigned int p_nSize )	\
	{ \
		throw std::bad_alloc(); \
	} \
public:	\
	static void* operator new( unsigned int p_nSize, const char* p_strFile, int p_nLine, const char* p_strFunction ) \
	{	\
		return ms_MemPool.Allocate();	\
	}	\
	static void operator delete( void* p_pAddr, const char* p_strFile, int p_nLine, const char* p_strFunction ) {} \
	static void operator delete( void* p_pAddr, unsigned int p_nSize ) \
	{	\
		ms_MemPool.Release( (__type*)p_pAddr );	\
	}
#else
//--------------------------------------------------------------
// macro
#define DECLARE_MEM_POOL( __type ) \
public: \
	static MoCommon::MoMemoryPool< __type >	ms_MemPool;	\
	static void* operator new( unsigned int p_nSize )	\
	{ \
		return ms_MemPool.Allocate();	\
	} \
	static void operator delete( void* p_pAddr, unsigned int p_nSize ) \
	{	\
		ms_MemPool.Release( (__type*)p_pAddr );	\
	}
#endif
//--------------------------------------------------------------
#define IMPLEMENT_MEM_POOL( __type )	\
	MoCommon::MoMemoryPool< __type >	__type::ms_MemPool;
