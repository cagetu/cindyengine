namespace Cindy
{
	//-----------------------------------------------------------------------------
	template <class T>
	T* CreateObject()
	{
		T* object = static_cast<T*>( T::RTTI.Create() );
		assert( object );
		return object;
	}
}

//-----------------------------------------------------------------------------
//	Factory 매크로 정의부
//-----------------------------------------------------------------------------
//#define CN_DECLARE_FACTORY(classname) \
//public: \
//	static CnObject*	FactoryCreator();	\
//	static classname*	Create(); \
//	static bool			RegisterFactoryFunction();	\
//private:

//-----------------------------------------------------------------------------
//	Factory 매크로 구현부
//-----------------------------------------------------------------------------
//#define CN_IMPLEMENT_FACTORY(classname) \
//	classname* classname::Create() \
//	{ \
//		return (classname*)FactoryCreator(); \
//	} \
//	CnObject* classname::FactoryCreator() \
//	{ \
//		classname* result = CnNew classname(); \
//		return result; \
//	} \
//	bool classname::RegisterFactoryFunction() \
//	{ \
//		if (!CnFactory::Instance()->Has(classname::RTTI.GetName().c_str()) )\
//		{ \
//			CnFactory::Instance()->Register( &classname::RTTI, classname::RTTI.GetName().c_str() ); \
//		} \
//		return true; \
//	}
//	//static bool factoryRegistered_##classname = classname::RegisterFactoryFunction();

//-----------------------------------------------------------------------------
//	Factory 매크로 Factory 등록
//-----------------------------------------------------------------------------
//#define REGISTER_FACTORY(classname) \
//	static bool factoryRegistered_##classname = classname::RegisterFactoryFunction();
