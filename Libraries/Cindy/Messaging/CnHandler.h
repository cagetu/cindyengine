// Copyright (c) 2009~. cagetu
//
//******************************************************************
#ifndef __CN_MESSAGE_HANDLER_H__
#define __CN_MESSAGE_HANDLER_H__

#include "CnMessage.h"

namespace Cindy
{
	namespace Messaging
	{
		//==================================================================
		/** Messaging::Handler
			@author		cagetu
			@brief		Message를 처리하는데 사용된다.
						특정 message들을 처리하기 위해, Handler를 상속 받아서,
						HandleMessage()를 재정의 한다.
			@see		Nebula3 Messaging::Handler
		*/
		//==================================================================
		class Handler : public CnObject
		{
			CN_DECLARE_CLASS(Handler);
		public:
			Handler();
			virtual ~Handler();

			virtual void	Open();
			virtual void	Close();

			bool	IsOpen() const;

			virtual bool	HandleMessage( const Pointer<Messaging::Message>& Msg );
			virtual void	DoWork();

		protected:
			bool	m_IsOpen;
		};

		//-------------------------------------------------------------------------
		/**	@brief	유효성 판정
		*/
		inline
		bool Handler::IsOpen() const
		{
			return m_IsOpen;
		}
	}
}

#endif	// _CN_MESSAGE_HANDLER_H__