// Copyright (c) 2009~. cagetu
//
//******************************************************************
#ifndef __CN_MESSAGE_H__
#define __CN_MESSAGE_H__

#include "CnMessageId.h"
#include "../Foundation/CnObject.h"

namespace Cindy
{
	namespace Messaging
	{
		//==================================================================
		/** Messaging::Message
			@author		cagetu
			@brief		Message 클래스
		*/
		//==================================================================
		class Message : public CnObject
		{
			CN_DECLARE_CLASS( Message );
			CN_DECLARE_MSGID;
		public:
			Message();

			/// id가 같은 지 체크
			bool	CheckId( const Messaging::Id& id ) const;
		};

		//-------------------------------------------------------------------------
		/**
		*/
		inline
		bool Message::CheckId( const Messaging::Id& id ) const
		{
			return (id == this->GetId());
		}
	}
}

#endif	// __CN_MESSAGE_H__