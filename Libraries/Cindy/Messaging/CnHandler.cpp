//================================================================
// File:               : CnHandler.cpp
// Related Header File : CnHandler.h
// Original Author     : changhee
//================================================================
#include "CnHandler.h"

namespace Cindy
{
	namespace Messaging
	{
		CN_IMPLEMENT_CLASS(Cindy::Messaging, Handler, CnObject);
		///
		Handler::Handler()
			: m_IsOpen(false)
		{
		}
		Handler::~Handler()
		{
		}

		//-------------------------------------------------------------------------
		/** @brief	초기화 
		*/
		void Handler::Open()
		{
			m_IsOpen = true;
		}

		//-------------------------------------------------------------------------
		/** @brief	뒷처리
		*/
		void Handler::Close()
		{
			m_IsOpen = false;
		}

		//-------------------------------------------------------------------------
		/**	@brief	메시지 처리, 만약 처리되었다면, 메시지를 true로 변경
		*/
		bool Handler::HandleMessage( const Pointer<Messaging::Message>& Msg )
		{
			return false;
		}

		//-------------------------------------------------------------------------
		/** @brief	option으로 "프레임 마다" 진행(호출)되는 handler들을 위한 메써드
		*/
		void Handler::DoWork()
		{
			if (!m_IsOpen)
				return;
		}
	}
}