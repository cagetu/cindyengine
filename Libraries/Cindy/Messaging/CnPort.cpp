//================================================================
// File:               : CnPort.cpp
// Related Header File : CnPort.h
// Original Author     : changhee
//================================================================
#include "CnPort.h"

namespace Cindy
{
	namespace Messaging
	{
		//-------------------------------------------------------------------------
		CN_IMPLEMENT_CLASS( Cindy::Messaging, Port, CnObject );
		//-------------------------------------------------------------------------
		Port::Port()
		{
		}
		Port::~Port()
		{
		}

		//-------------------------------------------------------------------------
		/** @brief	Port가 받을 수 있는 메시지 등록 */
		//-------------------------------------------------------------------------
		void Port::SetupAcceptedMessages()
		{
		}

		//-------------------------------------------------------------------------
		/** @brief	Port로 message 전송 */
		//-------------------------------------------------------------------------
		void Port::Send( const Pointer<Messaging::Message>& Msg )
		{
			std::vector<Pointer<Handler> >::iterator iend = m_Handlers.end();
			std::vector<Pointer<Handler> >::iterator i = m_Handlers.begin();
			for (; i != iend; ++i)
			{
				if ((*i)->HandleMessage(Msg))
				{
				}
			}
		}

		//-------------------------------------------------------------------------
		/** @brief	메시지 처리
			@desc	특정 메시지를 처리한다. 처리 가능한 유효 메시지에 대해서만
					처리가 가능해야 한다.
		*/
		//-------------------------------------------------------------------------
		void Port::HandleMessage( const Pointer<Messaging::Message>& Msg )
		{
		}
	}
}