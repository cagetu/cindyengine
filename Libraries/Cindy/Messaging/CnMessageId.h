// Copyright (c) 2009~. cagetu
//
//******************************************************************
#ifndef __CN_MESSAGE_ID_H__
#define __CN_MESSAGE_ID_H__

namespace Cindy
{
	namespace Messaging
	{
		//------------------------------------------------------------------------------
		/**	Messaging::Id
			@author		cagetu
			@brief		A message identifier.
			@see		Nebula3 messaging system
		*/
		//------------------------------------------------------------------------------
		class Id
		{
		public:
			/// constructor
			Id();
			/// equality operator
			bool operator==(const Id& rhs) const;
		};

		//------------------------------------------------------------------------------
		/**
		*/
		inline
		Id::Id()
		{
			// empty
		}

		//------------------------------------------------------------------------------
		/**
		*/
		inline
		bool Id::operator==(const Id& rhs) const
		{
			return (this == &rhs);
		}

	} // namespace Messaging

	
//------------------------------------------------------------------------------
/**
    Message Id macros.
*/
#define CN_DECLARE_MSGID \
public:\
    static Messaging::Id Id; \
    virtual const Messaging::Id& GetId() const;\
private:

#define CN_IMPLEMENT_MSGID(type) \
    Messaging::Id type::Id; \
    const Messaging::Id& type::GetId() const { return type::Id; }

}

//------------------------------------------------------------------------------
#endif	// __CN_MESSAGE_ID_H__