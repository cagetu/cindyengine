// Copyright (c) 2009~. cagetu
//
//******************************************************************
#ifndef __CN_MESSAGE_PORT_H__
#define __CN_MESSAGE_PORT_H__

#include "CnHandler.h"

namespace Cindy
{
	namespace Messaging
	{
		//-----------------------------------------------------------------------------
		/**	@class		Message Port
			@author		cagetu

			@brief		메시지 처리기
			@see		Nebula Device의 messsaging system
		*/
		//-----------------------------------------------------------------------------
		class Port : public CnObject
		{
			CN_DECLARE_CLASS(Port);
		public:
			Port();
			virtual ~Port();

			/// 메시지 보내기
			virtual void	Send( const Pointer<Messaging::Message>& Msg );

			/// 메시지 Id 등록
			bool AcceptsMessage(const Id& msgId) const;
			const std::vector<const Id*>& GetAcceptedMessages() const;
			virtual void SetupAcceptedMessages();

			/// 메시지 처리
			virtual void	HandleMessage( const Pointer<Messaging::Message>& Msg );
		protected:
			void RegisterMessage( const Messaging::Id& MsgId );

		private:
			std::vector<const Id*>	m_AcceptedMessageIds;
			std::vector<Pointer<Handler> >	m_Handlers;
		};

		//------------------------------------------------------------------------------
		/**
		*/
		inline
		void Port::RegisterMessage( const Messaging::Id& MsgId )
		{
			// ignore duplicate message ids (may happen when derived classes
			// process the same messages)
			std::vector<const Id*>::iterator iter = std::find( m_AcceptedMessageIds.begin(), m_AcceptedMessageIds.end(), &MsgId );
			if (m_AcceptedMessageIds.end() != iter)
			{
				m_AcceptedMessageIds.push_back( &MsgId );
			}
		}

		//------------------------------------------------------------------------------
		/**	@brief	get the array of accepted messages (sorted)
		*/
		inline
		const std::vector<const Id*>& Port::GetAcceptedMessages() const
		{
			return m_AcceptedMessageIds;
		}

		//------------------------------------------------------------------------------
		/**	@brief	return true if port accepts this msg */
		inline
		bool Port::AcceptsMessage(const Id& MsgId) const
		{
			std::vector<const Id*>::const_iterator iter = std::find( m_AcceptedMessageIds.begin(), m_AcceptedMessageIds.end(), &MsgId );
			return (m_AcceptedMessageIds.end() != iter);
		}
	}
}

#endif	// __CN_MESSAGE_PORT_H__