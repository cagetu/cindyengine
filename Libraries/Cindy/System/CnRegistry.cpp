//================================================================
// File:               : CnRegistry.cpp
// Related Header File : CnRegistry.h
// Original Author     : changhee
//================================================================
#include "CnRegistry.h"
#include "../Util/CnLog.h"

namespace Cindy
{
	namespace System
	{
		//------------------------------------------------------------------------------
		/**
			Convert a RootKey value into a Win32 key handle.
		*/
		HKEY
		Win32Registry::RootKeyToWin32KeyHandle(RootKey rootKey)
		{
			switch (rootKey)
			{
				case ClassesRoot:   return HKEY_CLASSES_ROOT;
				case CurrentUser:   return HKEY_CURRENT_USER;
				case LocalMachine:  return HKEY_LOCAL_MACHINE;
				case Users:         return HKEY_USERS;
			}
			// can't happen
			CnError(L"Can't happen!"); 
			return 0;
		}

		//------------------------------------------------------------------------------
		/**
			Return true if a specific entry exists in the registry. To check only
			for the existence of a key without the contained value, pass an 
			empty 'name' string.
		*/
		bool
		Win32Registry::Exists(RootKey rootKey, const CnString& key, const CnString& name)
		{
			assert(false == key.empty());
			HKEY win32RootKey = RootKeyToWin32KeyHandle(rootKey);
			HKEY hKey = 0;
			LONG res = RegOpenKeyEx(win32RootKey,       // hKey
									key.c_str(),		// lpSubKey
									0,                  // ulOptions (reserved)
									KEY_READ,           // samDesired
									&hKey);
			if (ERROR_SUCCESS != res)
			{
				// key does not exist
				return false;
			}
			if (false == name.empty())
			{
				res = RegQueryValueEx(hKey,             // hKey
									  name.c_str(),		// lpValueName
									  NULL,             // lpReserved
									  NULL,             // lpType
									  NULL,             // lpData
									  NULL);            // lpcbData
				RegCloseKey(hKey);
				return (ERROR_SUCCESS == res);
			}
			else
			{
				// key exists, value name was empty
				RegCloseKey(hKey);
				return true;
			}
		}

		//------------------------------------------------------------------------------
		/**
			Set a key value in the registry. This will create the key if it doesn't
			exist.
		*/
		bool
		Win32Registry::Write(RootKey rootKey, const CnString& key, const CnString& name, const CnString& value)
		{
			assert(false == key.empty());
			assert(false == key.empty());

			HKEY win32RootKey = RootKeyToWin32KeyHandle(rootKey);
			HKEY hKey = 0;
			LONG res = RegCreateKeyEx(win32RootKey,     // hKey
									  key.c_str(),		// lpSubKey
									  0,                // Reserved
									  NULL,             // lpClass
									  REG_OPTION_NON_VOLATILE,  // dwOptions
									  KEY_ALL_ACCESS,   // samDesired
									  NULL,             // lpSecurityAttribute
									  &hKey,            // phkResult
									  NULL);            // lpdwDisposition
			if (ERROR_SUCCESS == res)
			{
				res = RegSetValueEx(hKey,               // hKey
									name.c_str(),		// lpValueName
									0,                  // Reserved
									REG_SZ,             // dwType (normal string)
									(const BYTE*) value.c_str(),    // lpData
									(DWORD)value.size() + 1);                // cbData
				RegCloseKey(hKey);
				return (ERROR_SUCCESS == res);
			}
			else
			{
				return false;
			}
		}

		//------------------------------------------------------------------------------
		/**
			Get a value from the registry. Fails hard if the key doesn't exists
			(use the Exists() method to make sure that the key exists!).
		*/
		CnString
		Win32Registry::Read(RootKey rootKey, const CnString& key, const CnString& name)
		{
			assert(false == key.empty());
			HKEY win32RootKey = RootKeyToWin32KeyHandle(rootKey);
			HKEY hKey = 0;
			LONG res = RegOpenKeyEx(win32RootKey,       // hKey
									key.c_str(),    // lpSubKey
									0,                  // ulOptions (reserved)
									KEY_READ,           // samDesired
									&hKey);
			assert(ERROR_SUCCESS == res);

			// HACK: don't accept data > 1 KByte
			char buf[1024];
			DWORD bufSize = sizeof(buf);
			res = RegQueryValueEx(hKey,                // hKey
								  name.c_str(),    // lpValueName
								  NULL,                // lpReserved
								  NULL,                // lpType
								  (BYTE*)buf,          // lpData
								  &bufSize);           // lpcbData
			assert(ERROR_SUCCESS == res);
			buf[bufSize - 1] = 0;
			//CnString returnString = buf;
			CnString returnString;
			unicode::CharToWChar( buf, returnString );
			return returnString;
		}

		//------------------------------------------------------------------------------
		/**
			This deletes a complete registry key with all its values.
		*/
		bool
		Win32Registry::Delete(RootKey rootKey, const CnString& key)
		{
			assert(false == key.empty());
			HKEY win32RootKey = RootKeyToWin32KeyHandle(rootKey);
			LONG res = RegDeleteKey(win32RootKey,       // hKey
									key.c_str());   // lpSubKey
			return (ERROR_SUCCESS == res);
		}
	}
}