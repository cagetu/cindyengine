//================================================================
// File:               : CnCpuInfo.h
// Original Author     : changhee
//================================================================
#ifndef __CN_CPUINFO_H__
#define __CN_CPUINFO_H__

namespace Cindy
{
	namespace System
	{
		//================================================================
		/** Cpu
			@author		changhee
			@desc		Win32 Cpu ����
		*/
		//================================================================
		class Win32Cpu
		{
		public:
			typedef DWORD CoreId;

			/// core identifiers, under Win32, we basically don't care...
			static const CoreId InvalidCoreId    = 0xffffffff;
			static const CoreId MainThreadCore   = 0;
			static const CoreId IoThreadCore     = 2;
			static const CoreId RenderThreadCore = 1;
			static const CoreId AudioThreadCore  = 3;
			static const CoreId MiscThreadCore   = 4;
		};
	}
}

#endif	// __CN_CPUINFO_H__