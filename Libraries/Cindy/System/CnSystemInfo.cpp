//================================================================
// File:               : CnSystemInfo.cpp
// Related Header File : CnSystemInfo.h
// Original Author     : changhee
//================================================================
#include "CnSystemInfo.h"

namespace Cindy
{
	namespace System
	{
		//------------------------------------------------------------------------------
		/**
		*/
		Win32SystemInfo::Win32SystemInfo()
		{
			this->_platform = Win32;

			// get runtime-info from Windows
			SYSTEM_INFO sysInfo;
			::GetSystemInfo(&sysInfo);    
			switch (sysInfo.wProcessorArchitecture)
			{
				case PROCESSOR_ARCHITECTURE_AMD64:  this->_cpuType = X86_64; break;
				case PROCESSOR_ARCHITECTURE_INTEL:  this->_cpuType = X86_32; break;
				default:                            this->_cpuType = UnknownCpuType; break;
			}
			this->_numCpuCores = sysInfo.dwNumberOfProcessors;
			this->_pageSize = sysInfo.dwPageSize;
		}

		//------------------------------------------------------------------------------
		/**
		*/
		CnString
		Win32SystemInfo::PlatformAsString(Platform p)
		{
			switch (p)
			{
				case Win32:     return L"win32";
				//case Xbox360:   return "xbox360";
				//case Wii:       return "wii";
				//case PS3:       return "ps3";
				default:        return L"unknownplatform";
			}
		}

		//------------------------------------------------------------------------------
		/**
		*/
		CnString
		Win32SystemInfo::CpuTypeAsString(CpuType c)
		{
			switch (c)
			{
				case X86_32:            return L"x86_32";
				case X86_64:            return L"x86_64";
				//case PowerPC_Xbox360:   return "powerpc_xbox360";
				//case PowerPC_PS3:       return "powerpc_ps3";
				//case PowerPC_Wii:       return "powerpc_wii";
				default:                return L"unknowncputype";
			}
		}
	}
}