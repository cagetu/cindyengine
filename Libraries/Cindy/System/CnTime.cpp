//================================================================
// File					: CnTime.cpp
// Related Header File	: CnTime.h
// Original Author		: Changhee
// Creation Date		: 2009. 1. 29.
//================================================================
#include "CnTime.h"
#include <stdio.h>
#include <intrin.h>

#pragma intrinsic(__rdtsc)

namespace Cindy
{
	namespace System
	{
		//----------------------------------------------------------------
		/**	@brief	ClockCycle 얻기
		*/
		uint64 TimerGetClock()
		{
			return __rdtsc();
		}

		//----------------------------------------------------------------
		/**	@brief	ClockSpeed 구하기
			@see	Nocturnal ToolFramework의 Profile
		*/
		inline double GetClockSpeed()
		{
			static double clockSpeed = 0.0;
			if (clockSpeed != 0.0)
			{
				return clockSpeed;
			}

			__int64	i64_perf_start, i64_perf_freq, i64_perf_end;
			__int64	i64_clock_start,i64_clock_end;
			double d_loop_period, d_clock_freq;

			//-----------------------------------------------------------------------
			// Query the performance of the Windows high resolution timer.
			//-----------------------------------------------------------------------
			QueryPerformanceFrequency((LARGE_INTEGER*)&i64_perf_freq);

			//-----------------------------------------------------------------------
			// Query the current value of the Windows high resolution timer.
			//-----------------------------------------------------------------------
			QueryPerformanceCounter((LARGE_INTEGER*)&i64_perf_start);
			i64_perf_end = 0;

			//-----------------------------------------------------------------------
			// Time of loop of 250000 windows cycles with RDTSC
			//-----------------------------------------------------------------------
			i64_clock_start = __rdtsc();
			while(i64_perf_end<i64_perf_start+250000)
			{
				QueryPerformanceCounter((LARGE_INTEGER*)&i64_perf_end);
			}
			i64_clock_end = __rdtsc();

			//-----------------------------------------------------------------------
			// Caclulate the frequency of the RDTSC timer and therefore calculate
			// the frequency of the processor.
			//-----------------------------------------------------------------------
			i64_clock_end -= i64_clock_start;

			d_loop_period = ((double)(i64_perf_freq)) / 250000.0;
			d_clock_freq = ((double)(i64_clock_end & 0xffffffff))*d_loop_period;

			return (float)(clockSpeed = d_clock_freq);
		}

		//----------------------------------------------------------------
		/**	@brief	밀리초단위로 ClockCycle을 변경
		*/
		float CyclesToMillisec( uint64 Cycles )
		{
		  return (float)((1000.0 * (double)Cycles) / GetClockSpeed());
		}

		//----------------------------------------------------------------
		/**	@brief 
		*/
		//float TimeTaken( uint64 StartTime )
		//{
		//  uint64 time = TimerGetClock() - StartTime;
		//  return CyclesToMillisec(time);
		//}
	}
}