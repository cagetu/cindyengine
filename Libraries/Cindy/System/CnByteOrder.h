#ifndef __CN_BYTEORDER_H__
#define __CN_BYTEORDER_H__

#include "../Cindy.h"
#include "../Math/CnMatrix44.h"

namespace Cindy
{
	namespace System
	{
		//================================================================
		/** Byte Order
			@author		changhee
			@remarks	Cross-platform의 경우, byte 정렬 순서가 다를 수 있다.
						LittleEndian:	Intel byte order
						BigEndian:		Motorola byte order

						[참고]
						http://ko.wikipedia.org/wiki/%EC%97%94%EB%94%94%EC%95%88
						Nebula3 system/ByteOrder
		*/
		//================================================================
		class ByteOrder
		{
		public:
			enum Type
			{
				LittleEndian = 0,
				BigEndian,

				#if WIN32
				Host = LittleEndian,
				#else
				Host = BigEndian,
				#endif
			};

			//-----------------------------------------------------------------------------
			//	Methods
			//-----------------------------------------------------------------------------
			ByteOrder();
			ByteOrder( Type fromByteOrder, Type toByteOrder );

			void	SetFromByteOrder( Type fromByteOrder );
			Type	GetFromByteOrder() const;

			void	SetToByteOrder( Type toByteOrder );
			Type	GetToByteOrder() const;

			template <class TYPE>
			void	Convert( TYPE& val ) const;
		private:
			Type	from_;
			Type	to_;
		};

		//-----------------------------------------------------------------------------
		// Constructor
		__forceinline
		ByteOrder::ByteOrder()
			: from_(Host)
			, to_(Host)
		{
		}
		__forceinline
		ByteOrder::ByteOrder( ByteOrder::Type fromByteOrder, ByteOrder::Type toByteOrder )
			: from_(fromByteOrder)
			, to_(toByteOrder)
		{
		}

		//-----------------------------------------------------------------------------
		__forceinline
		void ByteOrder::SetFromByteOrder( ByteOrder::Type fromByteOrder )
		{
			this->from_ = fromByteOrder;
		}
		__forceinline
		ByteOrder::Type ByteOrder::GetFromByteOrder() const
		{
			return this->from_;
		}

		//-----------------------------------------------------------------------------
		__forceinline
		void ByteOrder::SetToByteOrder( ByteOrder::Type toByteOrder )
		{
			this->to_ = toByteOrder;
		}
		__forceinline
		ByteOrder::Type ByteOrder::GetToByteOrder() const
		{
			return this->to_;
		}

		//=============================================================================
		//	특화
		//=============================================================================

		//------------------------------------------------------------------------------
		template<> __forceinline
		void ByteOrder::Convert<short>(short& val) const
		{
			if (this->from_ != this->to_)
			{
				ushort res = _byteswap_ushort(*(ushort*)&val);
				val = *(short*)&res;
			}
		}

		//------------------------------------------------------------------------------
		template<> __forceinline
		void ByteOrder::Convert<ushort>(ushort& val) const
		{
			if (this->from_ != this->to_)
			{
				val = _byteswap_ushort(val);
			}
		}

		//------------------------------------------------------------------------------
		template<> __forceinline
		void ByteOrder::Convert<int>(int& val) const
		{
			if (this->from_ != this->to_)
			{
				uint res = _byteswap_ulong(*(uint*)&val);
				val = *(int*)&res;
			}
		}

		//------------------------------------------------------------------------------
		template<> __forceinline
		void ByteOrder::Convert<uint>(uint& val) const
		{
			if (this->from_ != this->to_)
			{
				val = _byteswap_ulong(val);
			}
		}

		//------------------------------------------------------------------------------
		template<> __forceinline
		void ByteOrder::Convert<float>(float& val) const
		{
			if (this->from_ != this->to_)
			{
				uint res = _byteswap_ulong(*(uint*)&val);
				val = *(float*)&res;
			}
		}

		//------------------------------------------------------------------------------
		/**
		*/
		template<> __forceinline
		void ByteOrder::Convert<double>(double& val) const
		{
			if (this->from_ != this->to_)
			{
				unsigned long long res = _byteswap_uint64(*(unsigned long long*)&val);
				val = *(double*)&res;
			}
		}

		//------------------------------------------------------------------------------
		/**
		*/
		template<> __forceinline
		void ByteOrder::Convert<CnVector4>(CnVector4& val) const
		{
			if (this->from_ != this->to_)
			{
				Convert<float>(val.x);
				Convert<float>(val.y);
				Convert<float>(val.z);
				Convert<float>(val.w);
			}
		}

		//------------------------------------------------------------------------------
		/**
		*/
		template<> __forceinline
		void ByteOrder::Convert<CnMatrix44>(CnMatrix44& val) const
		{
			if (this->from_ != this->to_)
			{
				CnVector4 row0;
				CnVector4 row1;
				CnVector4 row2;
				CnVector4 row3;
				val.GetRow(0, row0)
				val.GetRow(1, row1)
				val.GetRow(2, row2)
				val.GetRow(3, row3)

				Convert<CnVector4>(row0);
				Convert<CnVector4>(row1);
				Convert<CnVector4>(row2);
				Convert<CnVector4>(row3);
				val.SetRow(0, row0);
				val.SetRow(1, row1);
				val.SetRow(2, row2);
				val.SetRow(3, row3);
			}
		}

	}
}

#endif	// __CN_BYTEORDER_H__