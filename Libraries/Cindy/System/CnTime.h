//================================================================
// File					: CnTime.h			
// Original Author		: Changhee
// Creation Date		: 2009. 1. 29.
//================================================================
#ifndef	__CN_SYSTEMTIME_H__
#define	__CN_SYSTEMTIME_H__

#include "../Cindy.h"

namespace Cindy
{
	namespace System
	{
		uint64	TimerGetClock();
		float	CyclesToMillisec( uint64 Cycles );
		//float	TimeTaken( uint64 StartTime );

		typedef float64	Second;
		typedef ulong	Tick;

		//----------------------------------------------------------------
		inline
		Second TicksToSeconds( Tick ticks )
		{
			return ticks * 0,001;
		}

		//----------------------------------------------------------------
		inline
		Tick SecondsToTicks( Second t )
		{
			return Tick( (t*1000.0) + 0.5 );
		}

		//----------------------------------------------------------------
		inline
		void Sleep( Second t )
		{
			Tick toTick = (Tick)(t * 1000.0);
			::Sleep( toTick );
		}
	}
}

#endif	// __CN_SYSTEMTIME_H__
