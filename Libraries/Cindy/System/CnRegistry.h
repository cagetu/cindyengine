//================================================================
// File:               : CnRegistry.h
// Original Author     : changhee
//================================================================
#ifndef __CN_REGISTRY_H__
#define __CN_REGISTRY_H__

#include "../Util/CnString.h"

namespace Cindy
{
	namespace System
	{
		//================================================================
		/** Registry
			@author		changhee
			@desc		윈도우 레지스트리 작업 클래스
		*/
		//================================================================
		class Win32Registry
		{
		public:
			/// key enumeration
			enum RootKey
			{
				ClassesRoot,
				CurrentUser,
				LocalMachine,
				Users,
			};

			/// return true if a registry entry exists
			static bool		Exists(RootKey rootKey, const CnString& key, const CnString& name);
			/// write a registry entry
			static bool		Write(RootKey rootKey, const CnString& key, const CnString& name, const CnString& value);
			/// read a registry entry
			static CnString Read(RootKey rootKey, const CnString& key, const CnString& name);
			/// delete a registry key (and all its contained values)
			static bool		Delete(RootKey rootKey, const CnString& key);
		private:
			/// convert RootKey enum into Win32 key handle
			static HKEY RootKeyToWin32KeyHandle(RootKey rootKey);
		};
	}
}

#endif	// __CN_REGISTRY_H__