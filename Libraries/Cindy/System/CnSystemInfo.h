//================================================================
// File:               : CnSystemInfo.h
// Original Author     : changhee
//================================================================
#ifndef __CN_SYSTEMINFO_H__
#define __CN_SYSTEMINFO_H__

#include "../Util/CnString.h"

namespace Cindy
{
	namespace System
	{
		//================================================================
		/** System Info
			@author		changhee
			@desc		시스템 정보	
		*/
		//================================================================
		class Win32SystemInfo
		{
		public:
			enum Platform
			{
				Win32,
				//Xbox360,
				//Wii,
				//PS3,

				UnknownPlatform,
			};

			enum CpuType
			{
				X86_32,             // any 32-bit x86
				X86_64,             // any 64-bit x86
				//PowerPC_Xbox360,    // Xbox 360 CPU
				//PowerPC_PS3,        // PS3 Cell CPU
				//PowerPC_Wii,        // Wii CPU

				UnknownCpuType,
			};

			//-----------------------------------------------------------------------------
			//	Methods
			//-----------------------------------------------------------------------------
			Win32SystemInfo();

			Platform	GetPlatform() const;
			CpuType		GetCpuType() const;

			uint	GetNumCpuCores() const;
			uint	GetPageSize() const;

			static CnString	PlatformAsString( Platform eType );
			static CnString	CpuTypeAsString( CpuType eType );

		private:
			Platform _platform;
			CpuType	_cpuType;
			uint _numCpuCores;
			uint _pageSize;
		};

		//-----------------------------------------------------------------------------
		inline Win32SystemInfo::Platform
		Win32SystemInfo::GetPlatform() const
		{
			return _platform;
		}

		//-----------------------------------------------------------------------------
		inline Win32SystemInfo::CpuType
		Win32SystemInfo::GetCpuType() const
		{
			return _cpuType;
		}

		//-----------------------------------------------------------------------------
		inline uint
		Win32SystemInfo::GetNumCpuCores() const
		{
			return _numCpuCores;
		}

		//-----------------------------------------------------------------------------
		inline uint
		Win32SystemInfo::GetPageSize() const
		{
			return _pageSize;
		}
	}
}

#endif	// __CN_SYSTEMINFO_H__