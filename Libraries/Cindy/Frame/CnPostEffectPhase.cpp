//================================================================
// File:               : CnPostEffectPhase.cpp
// Related Header File : CnPostEffectPhase.h
// Original Author     : changhee
// Creation Date       : 2009. 1. 31
//================================================================
#include "../Cindy.h"
#include "../Util/CnLog.h"
#include "CnPostEffectPhase.h"
#include "CnPostEffect.h"

namespace Cindy
{
namespace PostEffect
{
	// const
	Phase::Phase( PostEffect::Effect* pOwner )
		: m_pOwner( pOwner )
		, m_bEnable( true )
	{
	}
	// dest
	Phase::~Phase()
	{
		m_Steps.clear();
	}

	//----------------------------------------------------------------
	/**	@brief	Step 추가 */
	//----------------------------------------------------------------
	void Phase::AddStep( PostEffect::Step* pStep )
	{
		m_Steps.push_back( pStep );
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void Phase::Run()
	{
		if (false == IsEnable())
			return;

		StepArray::iterator iend = m_Steps.end();
		for (StepArray::iterator i=m_Steps.begin(); i!=iend; ++i)
		{
			(*i)->Run();
		}
	}

	//----------------------------------------------------------------
	/**	@brief	XML Parsing	*/
	//----------------------------------------------------------------
	void Phase::Parse( CMiniXML::iterator* pPhaseXML )
	{
		const wchar* attr = pPhaseXML->GetAttribute(L"enable");
		if (attr)
		{
			// Phase 사용 여부
			CnString enable(attr);
			unicode::LowerCase( enable );
			if (enable == L"false")
			{
				SetEnable( false );
				return;
			}
		}

		/// Steps
		CnString type;
		CMiniXML::iterator xmlStep = pPhaseXML->GetChild();
		for (xmlStep=xmlStep.Find(_T("Step")); xmlStep != NULL; xmlStep=xmlStep.Find(_T("Step")))
		{
			const wchar* enableAttr = xmlStep.GetAttribute(L"enable");
			if (enableAttr)
			{
				CnString enable( enableAttr );
				unicode::LowerCase( enable );
				if (enable == L"false")
					continue;
			}

			PostEffect::Step* newStep = CnNew PostEffect::Step();
			{
				newStep->SetOwner( m_pOwner );
				newStep->Parse( &xmlStep );
			}
			AddStep( newStep );
		}
	}
} // namespace PostEffect
} // namespace Cindy