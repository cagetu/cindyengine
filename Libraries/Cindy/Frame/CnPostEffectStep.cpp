//================================================================
// File:               : CnPostEffectStep.cpp
// Related Header File : CnPostEffectStep.h
// Original Author     : changhee
// Creation Date       : 2009. 1. 31
//================================================================
#include "Cindy.h"
#include "CnPostEffectStep.h"
#include "CnPostEffect.h"
#include "CnCindy.h"
#include "Util/CnLog.h"
#include "Scene/Component/CindySceneComponents.h"
#include "Scene/Component/CnSceneComponentManager.h"

#pragma TODO("-09/08/20 : RenderTarget이 중복되서 설정되어 Texture가 낭비되는 등의 문제점들을 최적화를 통해 해결해야 한다.")

namespace Cindy
{
namespace PostEffect
{
	//----------------------------------------------------------------
	/**	Constructor
	*/
	Step::Step()
		: m_pOwner(0)
		, m_Enable(true)
	{
	}
	
	//----------------------------------------------------------------
	/**	Destructor
	*/
	Step::~Step()
	{
		m_Viewport = 0;
	}

	//----------------------------------------------------------------
	/**	@brief	Effect 설정
	*/
	void Step::SetOwner( PostEffect::Effect* pOwner )
	{
		m_pOwner = pOwner;
	}

	//----------------------------------------------------------------
	/**	@brief	Effect 얻어오기
	*/
	PostEffect::Effect* Step::GetOwner() const
	{
		return m_pOwner;
	}

	//----------------------------------------------------------------
	/** @brief	Run
	*/
	void Step::Run()
	{
		if (m_Enable)
		{
			m_RenderTarget->Begin();
				m_Viewport->Update();
				m_Viewport->Draw();
			m_RenderTarget->End();
			//m_RenderTarget->Update();
		}
	}

	//----------------------------------------------------------------
	/**	@brief	XML Parsing
	*/
	void Step::Parse( CMiniXML::iterator* pStepXML )
	{
		assert(m_Viewport.IsNull());

		/// step name 설정
		const wchar* stepName = pStepXML->GetAttribute(L"name");
		m_Name = stepName;

		/// renderTarget 설정
		CnString targetName = pStepXML->GetAttribute(L"renderTarget");
		m_RenderTarget = m_pOwner->FindRenderTarget( targetName );
		assert(!m_RenderTarget.IsNull());

		//m_RenderTarget->Active( true );

		// scene
		Scene::PostProcessComponent* postprocessComponent = Scene::PostProcessComponent::Create(m_RenderTarget->GetWidth(), m_RenderTarget->GetHeight());
		postprocessComponent->Init(*pStepXML, m_pOwner);

		// ETC

		Ptr<CnScene> scene = 0;
		Ptr<CnCamera> camera = CnCindy::Instance()->GetDefaultCamera();
		bool hud = false;

		/// Scene
		CMiniXML::iterator xmlScene = pStepXML->GetChild();
		for (xmlScene=xmlScene.Find(_T("Scene")); xmlScene != NULL; xmlScene=xmlScene.Find(_T("Scene")))
		{
			const wchar* attr = 0;
			attr = xmlScene.GetAttribute(L"name");
			if (attr)
			{
			}
			attr = xmlScene.GetAttribute(L"camera");
			if (attr)
			{
				camera = CnCindy::Instance()->GetCamera(attr);
			}
			attr = xmlScene.GetAttribute(L"HUD");
			if (attr)
			{
				hud = unicode::ToBool(attr);
			}
		}

		/// Scene
		if (scene.IsNull())
		{
			scene = CnNew CnScene( m_Name );
			scene->AddComponent( Scene::BeginScene::Create() );
			scene->AddComponent( postprocessComponent );

			if (hud)
			{
				scene->AddComponent( Scene::RenderText::Create() );
				//scene->AddComponent( Scene::DebugLight::Create() );
			}

			scene->AddComponent( Scene::EndScene::Create() );
		}

		m_Viewport = CnNew CnViewport(m_RenderTarget, 0, 0.0f, 0.0f, 1.0f, 1.0f);
		m_Viewport->SetScene( scene );
		m_Viewport->SetCamera( camera );
		m_Viewport->SetBackgroundColor( CN_RGBA( 0, 0, 0, 0 ) );
	}
} // namespace Scene
} // namespace Cindy