//================================================================
// File:               : CnPostEffect.cpp
// Related Header File : CnPostEffect.h
// Original Author     : changhee
// Creation Date       : 2009. 1. 31
//================================================================
#include "../Cindy.h"
#include "Util/CnLog.h"
#include "Material/CnTexture.h"
#include "Graphics/CnAntiAliasing.h"
#include "Graphics/Base/CnRenderTexture.h"
#include "Graphics/Base/CnRenderWindow.h"
#include "Graphics/CnRenderer.h"
#include "CnPostEffect.h"
#include "CnPostEffectSystem.h"

namespace Cindy
{
namespace PostEffect
{
	// const
	Effect::Effect( System* pParent )
		: m_Parent(pParent)
		, m_Index(0)
		, m_Prev(0)
	{
	}
	// dest
	Effect::~Effect()
	{
		Close();
	}

	//----------------------------------------------------------------
	/**
	*/
	void Effect::Open()
	{
	}

	//----------------------------------------------------------------
	/** @brief	Close
	*/
	void Effect::Close()
	{
		m_Phases.clear();
		m_RenderTargets.clear();
	}

	//----------------------------------------------------------------
	/** @brief	실행 */
	//----------------------------------------------------------------
	void Effect::Run()
	{
		if (false == IsEnable())
			return;

		PhaseArray::iterator iend = m_Phases.end();
		for (PhaseArray::iterator i=m_Phases.begin(); i!=iend; i++)
		{
			(*i)->Run();
		}
	}

	//----------------------------------------------------------------
	/**	@brief	RenderTarget 추가 */
	//----------------------------------------------------------------
	void Effect::AddRenderTarget( CnRenderTarget* pRenderTarget )
	{
		pRenderTarget->SetPriority( m_Index );

		std::pair<RenderTargetMap::iterator, bool> res =
			m_RenderTargets.insert( RenderTargetMap::value_type( pRenderTarget->GetName(), pRenderTarget ) );
		assert( res.second );
	}

	//----------------------------------------------------------------
	/**	@brief	RenderTarget 찾기 */
	//----------------------------------------------------------------
	Ptr<CnRenderTarget> Effect::FindRenderTarget( const CnString& Name )
	{
		if (Name[0] == L'*')
		{
			if (Name == L"*CompositeOut")
			{
				m_Output = CompositeOut();
				return m_Output;
			}
			else if (Name == L"*CompositeIn")
			{
				return GetCompositeIn();
			}
			else if (Name == L"*Frame")
			{
				return RenderDevice->GetCurrentRenderWindow();
			}
			else if (Name == L"*Color")
			{
				return m_Parent->GetRT( System::RT_Scene );
			}
			else if (Name == L"*Depth")
			{
				return m_Parent->GetRT( System::RT_Depth );
			}
		}
		else
		{
			RenderTargetMap::iterator iter = m_RenderTargets.find( Name );
			if (iter != m_RenderTargets.end())
			{
				return iter->second;
			}
			else
			{
				return RenderDevice->GetRenderTarget(Name);
			}
		}

		return 0;
	}

	//----------------------------------------------------------------
	/**	@brief	Phase 추가
	*/
	void Effect::AddPhase( PostEffect::Phase* pPhase )
	{
		m_Phases.push_back( pPhase );
	}

	//----------------------------------------------------------------
	/** @brief	이전 이펙트 설정
	*/
	void Effect::SetPrev( Effect* pEffect )
	{
		m_Prev = pEffect;
	}

	//----------------------------------------------------------------
	/** @brief	Composite Out 랜더 타겟 설정
	*/
	CnRenderTarget* Effect::CompositeOut()
	{
		CnRenderWindow* renderWindow = RenderDevice->GetCurrentRenderWindow();
		assert(renderWindow);

		static int _compositeInCount = 0;
		CnString rtName( ToStr(L"CompositeOut_%d", _compositeInCount++) );

		CnRenderTarget* renderTarget = 
			reinterpret_cast<CnRenderTarget*>(RenderDevice->CreateRenderTexture( rtName.c_str(),
																				 renderWindow->GetWidth(),
																				 renderWindow->GetHeight(),
																				 renderWindow->GetColorDepth(),
																				 PixelFormat::A16B16G16R16F,
																				 CnAntiAliasQuality::None,
																				 false, 
																				 false,
																				 false ) );
		renderTarget->SetPriority( this->GetIndex() );
		return renderTarget;
	}

	//----------------------------------------------------------------
	/** @brief	Composite In 얻어오기
		@desc	연결된 앞의 Effect의 포인터를 가지고 있는다.
				1. 앞에 연결된 Effect가 없다면, 장면 텍스쳐를 가지고 온다.
				2. 활성화 된 Effect라면, 앞에 연결된 이펙트의 결과를 가지고 온다.
				3. 혹시 앞의 Effect이 비활성화 되어 있다면, 그 전의 결과를 가지고 온다.
	*/
	CnRenderTarget* Effect::GetCompositeIn()
	{
		if (m_Prev == 0)
		{
			return m_Parent->GetRT( System::RT_Scene );
		}

		if (IsEnable())
		{
			return m_Prev->m_Output;
		}

		return m_Prev->GetCompositeIn();
	}

	//----------------------------------------------------------------
	/**	Parse Effect.xml
	*/
	void Effect::Parse( CMiniXML::iterator* pEffectXML )
	{
		// 현재 생성된 윈도우
		CnRenderWindow* window = RenderDevice->GetCurrentRenderWindow();
		assert(window);

		CnString attr;

		/// RenderTargets
		CMiniXML::iterator xmlRT = pEffectXML->GetChild();
		for (xmlRT=xmlRT.Find(_T("DeclareRenderTarget")); xmlRT != NULL; xmlRT=xmlRT.Find(_T("DeclareRenderTarget")))
		{
			const wchar* name = xmlRT.GetAttribute(L"name");

			uint iHeight = window->GetHeight();
			uint iWidth = window->GetWidth();
			PixelFormat::Code eFormat = PixelFormat::A8R8G8B8;
			bool enableDepthStencil = false;
			bool enableMipMaps = false;

			attr = xmlRT.GetAttribute(L"format");
			if (!attr.empty())
			{
				eFormat = PixelFormat::StringToFormat(attr.c_str());
				assert( eFormat != PixelFormat::NOFORMAT );
			}

			// Determine the width and height.
			attr = xmlRT.GetAttribute( L"width" );
			if (!attr.empty())
			{
				if (attr != L"Screen") {
					iWidth = _wtoi( attr.c_str() );
				}
			}
			attr = xmlRT.GetAttribute( L"height" );
			if (!attr.empty())
			{
				if (attr != L"Screen") {
					iHeight = _wtoi( attr.c_str() );
				}
			}

			// See if we want to scale the size.
			const wchar* scaleAttr = xmlRT.GetAttribute( L"scale" );
			if (scaleAttr)
			{
				float fScale = (float)_wtof( scaleAttr );
				iWidth = uint( iWidth * fScale );
				iHeight = uint( iHeight * fScale );
			}

			//
			attr = xmlRT.GetAttribute( L"depth" );
			if (!attr.empty())
			{
				if (attr == L"true")
				{
					enableDepthStencil = true;
				}
			}
			// mipmap
			const wchar* mipmap = xmlRT.GetAttribute(L"mipmap");
			if (mipmap)
			{
				attr = mipmap;
				unicode::LowerCase(attr);
				if (attr == L"true")
				{
					enableMipMaps = true;
				}
			}

			// 타겟이름
			//CnString targetName = CnString(name) + CnString(effectName);
			CnString targetName(name);

			// 새로운 랜더 타겟 추가
			CnRenderTexture* renderTarget =	RenderDevice->CreateRenderTexture( targetName.c_str(),
																			   iWidth,
																			   iHeight,
																			   window->GetColorDepth(),
																			   eFormat,
																			   CnAntiAliasQuality::None,
																			   enableDepthStencil,
																			   enableMipMaps,
																			   false );
			AddRenderTarget( renderTarget );
			renderTarget->Active( false );
		}

		/// Phases
		CMiniXML::iterator xmlPhase = pEffectXML->GetChild();
		for (xmlPhase=xmlPhase.Find(_T("Phase")); xmlPhase != NULL; xmlPhase=xmlPhase.Find(_T("Phase")))
		{
			PostEffect::Phase* newPhase = new PostEffect::Phase( this );
			newPhase->Parse( &xmlPhase );

			AddPhase( newPhase );
		}

	}

} // namespace PostEffect
} // namespace Cindy