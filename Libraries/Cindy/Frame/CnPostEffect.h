//================================================================
// File:           : CnPostEffect.h
// Original Author : changhee
// Creation Date   : 2009. 1. 31
//================================================================
#ifndef __CN_POSTPROCESS_EFFECT_H__
#define __CN_POSTPROCESS_EFFECT_H__

#include "CnPostEffectPhase.h"

namespace Cindy
{
namespace PostEffect
{
	class System;

	//================================================================
	/** PostProcessEffect
		@author		changhee
		@brief		한 단위 PostProcessing 처리
		@see		ShaderX5 : 8.1 Postprocessing Effects in Design
	*/
	//================================================================
	class CN_DLL Effect : public CnObject
	{
	public:
		void				Open();
		void				Close();

		// Name
		void				SetName( const CnString& Name );
		const CnString&		GetName() const;

		// Index
		uint				GetIndex() const;

		// Enable
		void				SetEnable( bool bEnable );
		bool				IsEnable() const;

		// RenderTarget
		Ptr<CnRenderTarget>	FindRenderTarget( const CnString& Name );

	private:
		typedef std::vector<Ptr<PostEffect::Phase> >	PhaseArray;
		typedef HashMap<CnString, Ptr<CnRenderTarget> >	RenderTargetMap;

		friend class System;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		CnString	m_Name;
		uint		m_Index;
		bool		m_Enable;

		System*		m_Parent;
		Effect*		m_Prev;

		PhaseArray	m_Phases;
		RenderTargetMap	m_RenderTargets;

		Ptr<CnRenderTarget>	m_Output;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		Effect( System* pParent );
		virtual ~Effect();

		//
		void	SetIndex( uint Index );
		void	SetPrev( Effect* pEffect );

		// Parse
		void	Parse( CMiniXML::iterator* pEffectXML );

		// Phase
		void	AddPhase( PostEffect::Phase* pPhase );
		void	AddRenderTarget( CnRenderTarget* pRenderTarget );

		// Run
		void	Run();

		//
		CnRenderTarget* CompositeOut();
		CnRenderTarget*	GetCompositeIn();
	};

	//----------------------------------------------------------------
	inline
	void Effect::SetName( const CnString& Name )
	{
		m_Name = Name;
	}
	inline
	const CnString& Effect::GetName() const
	{
		return m_Name;
	}
	
	//----------------------------------------------------------------
	inline
	void Effect::SetIndex( uint Index )
	{
		m_Index = Index;
	}
	inline
	uint Effect::GetIndex() const
	{
		return m_Index;
	}

	//----------------------------------------------------------------
	/**	@brief	활성화 */
	//----------------------------------------------------------------
	inline
	void Effect::SetEnable( bool bEnable )
	{
		m_Enable = bEnable;
	}

	//----------------------------------------------------------------
	inline
	bool Effect::IsEnable() const
	{
		return m_Enable;
	}
}
}

#endif	// __CN_POSTPROCESS_EFFECT_H__