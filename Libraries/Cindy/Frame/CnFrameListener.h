//================================================================
// File:           : CnFrameListener.h
// Original Author : changhee
// Creation Date   : 2008. 5. 20
//================================================================
#ifndef __CN_FRAMELISTENER_H__
#define __CN_FRAMELISTENER_H__

namespace Cindy
{
	//==================================================================
	/** FrameListener
		@author			cagetu
		@since			2008년 5월 20일
		@remarks		Frame Listener
						매 프레임 시작과 끝에 처리할 내용
	*/
	//==================================================================
	class IFrameListener
	{
	public:
		virtual void	BeginFrame() abstract;
		virtual void	EndFrame() abstract;
	};
}

#endif	//	__CN_FRAMELISTENER_H__