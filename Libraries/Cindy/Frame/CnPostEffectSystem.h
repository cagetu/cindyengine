//================================================================
// File:           : CnPostEffectSystem.h
// Original Author : changhee
// Creation Date   : 2009. 1. 31
//================================================================
#ifndef __CN_POSTPROCESS_EFFECT_SYSTEM_H__
#define __CN_POSTPROCESS_EFFECT_SYSTEM_H__

#include "CnPostEffect.h"
#include "Util/CnSingleton.h"
#include "Util/CnUnCopyable.h"

namespace Cindy
{
namespace PostEffect
{
	//================================================================
	/** PostEffectSystem
		@author		changhee
		@brief		PostProcessing 처리 시스템 관리
		@see		ShaderX5 : 8.1 Postprocessing Effects in Design
	*/
	//================================================================
	class CN_DLL System : public CnObject
	{
		__DeclareClass(System);
		//__DeclareSingleton(System);
		__DeclareUnCopy(System);
	public:
		enum RTType
		{
			RT_Scene,
			RT_Depth,
		};

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		System();
		~System();

		// Clear
		void				Initialize();
		void				DeInitialize();

		// Run
		void				Run();

		// Effect
		Ptr<Effect>			LoadEffect( const CnString& strEffectName );
		Ptr<Effect>			FindEffect( const CnString& strEffectName );

		// RenderTarget
		//void				SetRT( RTType eType, CnRenderTarget* Target );
		void				SetRT( RTType eType, const CnString& Name );
		Ptr<CnRenderTarget>	GetRT( RTType eType );
	private:
		typedef HashMap<CnString, Ptr<Effect> >		EffectMap;
		//typedef map<RTType, Ptr<CnRenderTarget> >	RenderTargetMap;
		typedef map<RTType, CnString>				RenderTargetMap;

		friend class CnFrameManager;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		EffectMap		m_Effects;
		RenderTargetMap	m_RenderTargets;

		uint			m_EffectCount;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		void	AddEffect( PostEffect::Effect* pEffect );
	};
}
// Macro
#define PostEffectSystem	PostEffect::System::Instance()
}

#endif	// __CN_POSTPROCESS_EFFECT_SYSTEM_H__