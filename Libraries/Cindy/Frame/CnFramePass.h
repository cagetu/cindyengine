//================================================================
// File:           : CnFramePass.h
// Original Author : changhee
// Creation Date   : 2009. 11. 9
//================================================================
#ifndef __CN_FRAME_PASS_H__
#define __CN_FRAME_PASS_H__

#include "Foundation/CnObject.h"
#include "Scene/CnViewport.h"
#include "Graphics/CnRenderTarget.h"

namespace Cindy
{
	//================================================================
	/** Frame Pass Class
		@author		changhee
		@since		2009. 11. 9
		@brief		랜더 패스를 설정 - 1번의 랜더링 Begin-End를 설정

					Pass --- RenderTarget 1개 설정
						 |-- Viewport 1개 설정
								|--- Scene
								|--- Camera
	*/
	//================================================================
	class CN_DLL CnFramePass : public CnObject
	{
		__DeclareClass(CnFramePass);
	public:
		enum ProcessType
		{
			U_,	// UpdateOnly
			UR,	// Update + Render
		};

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		CnFramePass();
		virtual ~CnFramePass();

		/// RenderTarget
		void					SetRenderTarget( const Ptr<CnRenderTarget>& RenderTarget );

		/// Process Type
		void					SetProcessType( ProcessType Type );
		ProcessType				GetProcessType() const;

		/// Viewport
		void					SetViewport( const Ptr<CnViewport>& Viewport );
		const Ptr<CnViewport>&	GetViewport() const;

		/// Update
		void					Update();
		/// Draw
		void					Draw();

	protected:
		Ptr<CnRenderTarget> m_RenderTarget;
		Ptr<CnViewport>		m_Viewport;

		ProcessType			m_ProcessType;
	};

	//----------------------------------------------------------------
	/**
	*/
	inline void
	CnFramePass::SetRenderTarget( const Ptr<CnRenderTarget>& RenderTarget )
	{
		m_RenderTarget = RenderTarget;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline void
	CnFramePass::SetProcessType( CnFramePass::ProcessType Type )
	{
		m_ProcessType = Type;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline CnFramePass::ProcessType
	CnFramePass::GetProcessType() const
	{
		return m_ProcessType;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline void
	CnFramePass::SetViewport(const Ptr<CnViewport>& Viewport)
	{
		m_Viewport = Viewport;
	}

	//----------------------------------------------------------------
	/**
	*/
	inline const Ptr<CnViewport>&
	CnFramePass::GetViewport() const
	{
		return m_Viewport;
	}

} // namespace Cindy

#endif	// __CN_FRAME_PASS_H__