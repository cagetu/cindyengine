//================================================================
// File:           : CnPostEffectPhase.h
// Original Author : changhee
// Creation Date   : 2009. 1. 31
//================================================================
#ifndef __CN_POSTPROCESS_EFFECT_PHASE_H__
#define __CN_POSTPROCESS_EFFECT_PHASE_H__

#include "CnPostEffectStep.h"

namespace Cindy
{
namespace PostEffect
{
	class Effect;

	//================================================================
	/** PostProcessEffectPhase
		@author		changhee
		@brief		
		@see		ShaderX5 : 8.1 Postprocessing Effects in Design
	*/
	//================================================================
	class Phase : public CnRefCount
	{
	public:
		Phase( Effect* pOwner );
		virtual ~Phase();

		// Enable
		void	SetEnable( bool bEnable );
		bool	IsEnable() const;

		// Run
		void	Run();
	private:
		friend class Effect;

		typedef std::vector<Ptr<PostEffect::Step> >	StepArray;

		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		Effect*		m_pOwner;
		bool		m_bEnable;

		StepArray	m_Steps;

		//----------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------
		// Parse
		void	Parse( CMiniXML::iterator* pPhaseXML );

		// Step
		void	AddStep( PostEffect::Step* pStep );
	};

	//----------------------------------------------------------------
	/**	@brief	Enable ����
	*/
	inline
	void Phase::SetEnable( bool bEnable )
	{
		m_bEnable = bEnable;
	}
	
	//----------------------------------------------------------------
	/**	@brief	Enable ����
	*/
	inline
	bool Phase::IsEnable() const
	{
		return m_bEnable;
	}
}
}

#endif	// __CN_POSTPROCESS_EFFECT_PHASE_H__