//================================================================
// File:				: CnFramePass.cpp
// Related Header File	: CnFramePass.h
// Original Author		: changhee
// Creation Date		: 2009. 11. 9
//================================================================
#include "Cindy.h"
#include "CnFramePass.h"
#include "Graphics/CnRenderer.h"

namespace Cindy
{
	__ImplementClass(Cindy, CnFramePass, CnObject);
	//----------------------------------------------------------------
	CnFramePass::CnFramePass()
		: m_ProcessType(UR)
	{
	}
	CnFramePass::~CnFramePass()
	{
		m_Viewport = 0;
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnFramePass::Update()
	{
		if (m_Viewport.IsNull())
			return;

		//m_Viewport->Update();
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnFramePass::Draw()
	{
		if (m_Viewport.IsNull())
			return;
		if (m_RenderTarget.IsNull())
			return;

		if (m_RenderTarget->IsActive())
		{
			m_RenderTarget->Begin();
				m_Viewport->Update();
				m_Viewport->Draw();
			m_RenderTarget->End();
		}
	}

} // namespace Cindy