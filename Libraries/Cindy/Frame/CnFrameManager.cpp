//================================================================
// File:				: CnFrameManager.cpp
// Related Header File	: CnFrameManager.h
// Original Author		: changhee
// Creation Date		: 2009. 2. 9
//================================================================
#include "Cindy.h"
#include "CnCindy.h"
#include "CnFramePass.h"
#include "CnFrameManager.h"
#include "CnFrameListener.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/Base/CnRenderTexture.h"
#include "Graphics/Base/CnRenderWindow.h"
#include "Graphics/Base/CnMultipleRenderTarget.h"
#include "Graphics/CnAntiAliasing.h"
#include "Material/CnMaterial.h"
#include "Material/CnMaterialData.h"
#include "Material/CnMaterialDataManager.h"
#include "Scene/CnCameraImpl.h"
#include "Scene/Component/CindySceneComponents.h"
#include "Util/CnLog.h"

namespace Cindy
{
	__ImplementSingleton(CnFrameManager);
	//--------------------------------------------------------------
	// const
	CnFrameManager::CnFrameManager()
	{
		__ConstructSingleton;
	}
	// dest
	CnFrameManager::~CnFrameManager()
	{
		__DestructSingleton;
	}

	//----------------------------------------------------------------
	/**	@brief	시작
	*/
	void CnFrameManager::Initialize()
	{
	}

	//----------------------------------------------------------------
	/**	@brief	끝
	*/
	void CnFrameManager::DeInitialize()
	{
		m_Frames.clear();
	}

	//--------------------------------------------------------------
	/** @brief	FrameListener 등록
	*/
	void CnFrameManager::AddListener( IFrameListener* pListener )
	{
		m_FrameListeners.push_back( pListener );
	}

	//--------------------------------------------------------------
	/** @brief	FrameListener 해제
	*/
	void CnFrameManager::RemoveListener( IFrameListener* pListener )
	{
		ListenerIter iter = std::find( m_FrameListeners.begin(), m_FrameListeners.end(), pListener );
		if (iter == m_FrameListeners.end())
			return;

		m_FrameListeners.erase( iter );
	}

	//----------------------------------------------------------------
	/**	@brief
	*/
	void CnFrameManager::AddFrame( const Ptr<CnFrame>& Frame )
	{
		pair<FrameMap::iterator, bool> result = m_Frames.insert( FrameMap::value_type( Frame->GetName(), Frame ) );
#ifdef _DEBUG
		assert(result.second == true);
#endif
		if (result.second == false)
		{
			CnError( ToStr(L"[CnFrameManager::AddFrame] Alreay exist %s", Frame->GetName().c_str()) );
		}
	}

	//----------------------------------------------------------------
	/**	@brief
	*/
	bool CnFrameManager::HasFrame( const CnString& Name ) const
	{
		FrameMap::const_iterator iter = m_Frames.find( Name );
		if (iter == m_Frames.end())
			return false;

		return true;
	}

	//----------------------------------------------------------------
	/**	@brief
	*/
	Ptr<CnFrame> CnFrameManager::GetFrame( const CnString& Name ) const
	{
		FrameMap::const_iterator iter = m_Frames.find( Name );
		if (iter == m_Frames.end())
			return 0;

		return iter->second;
	}

	//--------------------------------------------------------------
	/** @brief	Frame 시작
	*/
	void CnFrameManager::Begin()
	{
		ListenerIter i, iend;
		iend = m_FrameListeners.end();
		for (i=m_FrameListeners.begin(); i!=iend; ++i)
			(*i)->BeginFrame();
	}

	//--------------------------------------------------------------
	/** @brief	Frame 끝
	*/
	void CnFrameManager::End()
	{
		ListenerIter i, iend;
		iend = m_FrameListeners.end();
		for (i=m_FrameListeners.begin(); i!=iend; ++i)
			(*i)->EndFrame();
	}

} // namespace Cindy