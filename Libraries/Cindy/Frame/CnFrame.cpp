//================================================================
// File:				: CnFrame.cpp
// Related Header File	: CnFrame.h
// Original Author		: changhee
// Creation Date		: 2010. 1. 8
//================================================================
#include "Cindy.h"
#include "CnCindy.h"
#include "CnFrame.h"
#include "CnFrameManager.h"
#include "CnFrameListener.h"
#include "Graphics/CnRenderer.h"
#include "Util/CnLog.h"

namespace Cindy
{
	__ImplementClass(Cindy, CnFrame, CnObject);
	//----------------------------------------------------------------
	CnFrame::CnFrame()
	{
	}
	CnFrame::~CnFrame()
	{
	}
	
	//----------------------------------------------------------------
	/**	@brief	시작
	*/
	void CnFrame::Open()
	{
		m_PostEffectSystem.Initialize();
	}

	//----------------------------------------------------------------
	/**	@brief	끝
	*/
	void CnFrame::Close()
	{
		m_FramePasses.clear();

		m_PostEffectSystem.DeInitialize();
	}

	//----------------------------------------------------------------
	/**	@brief	실행
		@desc	Application에 의해서만 실행 가능
	*/
	void CnFrame::Run()
	{
		BeginFrame();
			DoFrame();
		EndFrame();
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnFrame::AddPass( const Ptr<CnFramePass>& Pass )
	{
		m_FramePasses.push_back( Pass );
	}

	//--------------------------------------------------------------
	/** @brief	Frame 시작
	*/
	void CnFrame::BeginFrame()
	{
		//ListenerIter i, iend;
		//iend = m_FrameListeners.end();
		//for (i=m_FrameListeners.begin(); i!=iend; ++i)
		//	(*i)->BeginFrame();
	}

	//--------------------------------------------------------------
	/** @brief	Frame 끝
	*/
	void CnFrame::EndFrame()
	{
		//ListenerIter i, iend;
		//iend = m_FrameListeners.end();
		//for (i=m_FrameListeners.begin(); i!=iend; ++i)
		//	(*i)->EndFrame();
	}

	//--------------------------------------------------------------
	/**	Do Frame
	*/
	void CnFrame::DoFrame()
	{
		FramePassIter i, iend;
		iend = m_FramePasses.end();
		for (i = m_FramePasses.begin(); i != iend; i++)
		{
			(*i)->Update();
			(*i)->Draw();
		}
		//RenderDevice->UpdateRenderTargets();

		m_PostEffectSystem.Run();
	}

} // namespace Cindy