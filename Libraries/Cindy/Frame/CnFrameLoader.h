//================================================================
// File:           : CnFrameLoader.h
// Original Author : changhee
// Creation Date   : 2010. 1. 8
//================================================================
#ifndef __CN_FRAME_LOADER_H__
#define __CN_FRAME_LOADER_H__

#include "CnFrame.h"
#include <MoCommon/External/minixml.h>

namespace Cindy
{
	//================================================================
	/** Frame Script Loader Class
		@author		changhee
		@since		2010. 1. 8
		@brief		프레임 구성을 읽는다.
	*/
	//================================================================
	class CN_DLL CnFrameLoader
	{
	public:
		static Ptr<CnFrame> Load( const CnString& FileName );

	private:
		// Parse
		static void	ParseRenderTargets( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame );
		static void	ParseCameras( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame );
		static void	ParseSceneGraphs( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame );
		static void	ParseScenes( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame );
		static void	ParseScene( const Ptr<CnScene>& scene, CMiniXML::iterator& xmlNode );

		static void	ParsePasses( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame );
		static void	ParsePostEffects( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame );
	};

} // namespace Cindy

#endif	// __CN_FRAME_LOADER_H__