//================================================================
// File:               : CnPostEffectSystem.cpp
// Related Header File : CnPostEffectSystem.h
// Original Author     : changhee
// Creation Date       : 2009. 1. 31
//================================================================
#include "Cindy.h"
#include "Util/CnLog.h"
#include "Graphics/CnRenderer.h"
#include "CnPostEffectSystem.h"

namespace Cindy
{
namespace PostEffect
{
	__ImplementClass(Cindy, System, CnObject);
	//__ImplementSingleton(System);
	//----------------------------------------------------------------
	// const
	System::System()
		: m_EffectCount(0)
	{
		//__ConstructSingleton;
	}
	// dest
	System::~System()
	{
		m_Effects.clear();
		m_RenderTargets.clear();

		m_EffectCount = 0;

		//__DestructSingleton;
	}

	//----------------------------------------------------------------
	/**	@brief	시작
	*/
	void System::Initialize()
	{
	}

	//----------------------------------------------------------------
	/**	@brief	끝
	*/
	void System::DeInitialize()
	{
		//EffectMap::iterator iend = m_Effects.end();
		//for (EffectMap::iterator i=m_Effects.begin(); i!=iend; ++i)
		//	(i->second)->Close();
		m_Effects.clear();
		m_RenderTargets.clear();

		m_EffectCount = 0;
	}

	//----------------------------------------------------------------
	/** @brief	Run
	*/
	void System::Run()
	{
		//if (false == GetRT( RT_Scene )->Update())
		//	return;

		EffectMap::iterator iend = m_Effects.end();
		for (EffectMap::iterator i=m_Effects.begin(); i!=iend; ++i)
			(i->second)->Run();
	}

	//----------------------------------------------------------------
	/**	@brief	PostEffect::Effect 추가
	*/
	void System::AddEffect( PostEffect::Effect* pEffect )
	{
		if (0 == pEffect)
			return;

		std::pair<EffectMap::iterator, bool> res = m_Effects.insert( EffectMap::value_type( pEffect->GetName(), pEffect ) );
		assert(res.second);

		pEffect->Open();
		pEffect->SetIndex( m_EffectCount );
		m_EffectCount++;
	}

	//----------------------------------------------------------------
	/**	@brief	PostEffect::Effect 찾기
	*/
	Ptr<Effect> System::FindEffect( const CnString& strEffectName )
	{
		EffectMap::iterator iWhere = m_Effects.find( strEffectName );
		if (iWhere != m_Effects.end())
			return iWhere->second;

		return 0;
	}

	//----------------------------------------------------------------
	/**	@brief	랜더 타겟 설정
	*/
	//void System::SetRT( System::RTType eType, CnRenderTarget* Target )
	//{
	//	m_RenderTargets[eType] = Target;
	//	//RenderDevice->DeleteRenderTarget( Target->GetName() );
	//}
	void System::SetRT( System::RTType eType, const CnString& Name )
	{
		m_RenderTargets[eType] = Name;
	}

	//----------------------------------------------------------------
	/**	@brief	랜더 타겟 얻기
	*/
	Ptr<CnRenderTarget> System::GetRT( System::RTType eType )
	{
		//RenderTargetMap::iterator iWhere = m_RenderTargets.find( eType );
		//if (iWhere != m_RenderTargets.end())
		//	return iWhere->second;

		RenderTargetMap::iterator iWhere = m_RenderTargets.find( eType );
		if (iWhere == m_RenderTargets.end())
			return 0;

		return RenderDevice->GetRenderTarget(iWhere->second);
	}

	//----------------------------------------------------------------
	/**	@brief	PostEffect::Effect 읽어오기
	*/
	Ptr<Effect> System::LoadEffect( const CnString& strEffectName )
	{
		CMiniXML doc;
		if (!doc.Open( strEffectName.c_str()))
		{
			CnString newPath = strEffectName;
			if (!doc.Open( newPath.c_str() ))
			{
				CnError( ToStr(L"file not found: %s", newPath) );
				return 0;
			}
		}

		// "Effect" - Root
		CMiniXML::iterator xmlRoot = doc.GetRoot();
		if (xmlRoot == NULL)
			return 0;

		static PostEffect::Effect* prevEffect = 0;
		PostEffect::Effect* newEffect = CnNew PostEffect::Effect(this);
		{
			const wchar* effectName = xmlRoot.GetAttribute( L"name" );
			newEffect->SetName( effectName );
			newEffect->SetPrev( prevEffect );
			newEffect->Parse( &xmlRoot );
			prevEffect = newEffect;
		}
		AddEffect( newEffect );
		return newEffect;
	}
} // namespace PostEffect
} // namespace Cindy