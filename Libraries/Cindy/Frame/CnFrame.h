//================================================================
// File:           : CnFrame.h
// Original Author : changhee
// Creation Date   : 2010. 1. 8
//================================================================
#ifndef __CN_FRAME_H__
#define __CN_FRAME_H__

#include "CnPostEffectSystem.h"
#include "CnFramePass.h"

namespace Cindy
{
	//================================================================
	/** Frame Composite Class
		@author		changhee
		@since		2009. 2. 9
		@brief		프레임 구성 클래스
	*/
	//================================================================
	class CN_DLL CnFrame : public CnObject
	{
		__DeclareClass(CnFrame);
	public:
		CnFrame();
		virtual ~CnFrame();

		// 초기화
		void				Open();
		void				Close();

		// Name
		void				SetName( const CnString& Name );
		const CnString&		GetName() const;

		// 실행
		void				Run();

		// Pass
		void				AddPass( const Ptr<CnFramePass>& Pass );

	private:
		friend class CnFrameLoader;

		typedef std::vector<Ptr<CnFramePass> >	FramePassArray;
		typedef FramePassArray::iterator		FramePassIter;

		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		CnString			m_Name;

		// Passes
		FramePassArray		m_FramePasses;
		// Post-processing
		PostEffect::System	m_PostEffectSystem;

		// Begin
		void	BeginFrame();
		void	DoFrame();
		void	EndFrame();
	};

	//--------------------------------------------------------------
	/**
	*/
	inline void
	CnFrame::SetName( const CnString& Name )
	{
		m_Name = Name;
	}

	//--------------------------------------------------------------
	/**
	*/
	inline const CnString&
	CnFrame::GetName() const
	{
		return m_Name;
	}

} // namespace Cindy

#endif // __CN_FRAME_H__