//================================================================
// File:           : CnPostEffectStep.h
// Original Author : changhee
// Creation Date   : 2009. 1. 31
//================================================================
#ifndef __CN_POSTPROCESS_EFFECT_STEP_H__
#define __CN_POSTPROCESS_EFFECT_STEP_H__

#include "Graphics/CnRenderTarget.h"
#include "Scene/CnViewport.h"

#include <MoCommon/External/minixml.h>

namespace Cindy
{
namespace PostEffect
{
	class Effect;

	//================================================================
	/** PostProcessEffectStep
		@author		changhee
		@brief		
		@see		ShaderX5 : 8.1 Postprocessing Effects in Design
	*/
	//================================================================
	class Step : public CnObject
	{
	public:
		Step();
		virtual ~Step();

		// Effect
		void	SetOwner( Effect* pOwner );
		Effect*	GetOwner() const;

		// Enable
		void	SetEnable( bool bEnable );
		bool	IsEnable() const;

		// Run
		void	Run();

		// Parsing
		void	Parse( CMiniXML::iterator* pStepXML );
	protected:
		//----------------------------------------------------------------
		//	Variables
		//----------------------------------------------------------------
		Effect*		m_pOwner;

		CnString	m_Name;
		bool		m_Enable;

		Ptr<CnRenderTarget>	m_RenderTarget;
		Ptr<CnViewport>		m_Viewport;
	};

	//----------------------------------------------------------------
	/**	@brief	Ȱ��ȭ */
	//----------------------------------------------------------------
	inline
	void Step::SetEnable( bool bEnable )
	{
		m_Enable = bEnable;
	}

	//----------------------------------------------------------------
	inline
	bool Step::IsEnable() const
	{
		return m_Enable;
	}

}
}

#endif	// __CN_POSTPROCESS_EFFECT_STEP_H__