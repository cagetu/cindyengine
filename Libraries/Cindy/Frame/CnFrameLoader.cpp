//================================================================
// File:				: CnFrame.cpp
// Related Header File	: CnFrame.h
// Original Author		: changhee
// Creation Date		: 2010. 1. 8
//================================================================
#include "Cindy.h"
#include "CnCindy.h"
#include "CnFrameLoader.h"
#include "CnFrameManager.h"
#include "Graphics/CnRenderer.h"
#include "Graphics/Base/CnRenderTexture.h"
#include "Graphics/Base/CnRenderWindow.h"
#include "Graphics/Base/CnMultipleRenderTarget.h"
#include "Graphics/CnAntiAliasing.h"
#include "Material/CnMaterial.h"
#include "Material/CnMaterialData.h"
#include "Material/CnMaterialDataManager.h"
#include "Scene/CnCameraImpl.h"
#include "Scene/Component/CindySceneComponents.h"
#include "Util/CnLog.h"

namespace Cindy
{
	//----------------------------------------------------------------
	/**	@brief	
	*/
	Ptr<CnFrame> CnFrameLoader::Load( const CnString& FileName )
	{
		Ptr<CnFrame> frame = CnFrame::Create();
		frame->SetName( FileName );

		CnString fullPathFileName;
		if (CnFrameManager::Instance()->GetDataPath().empty())
		{
			fullPathFileName = FileName;
		}
		else
		{
			fullPathFileName = CnFrameManager::Instance()->GetDataPath() + FileName;
		}

		CMiniXML doc;
		if (!doc.Open( fullPathFileName.c_str()))
			return NULL;

		// "Frame" - Root
		CMiniXML::iterator xmlFrame = doc.GetRoot();
		if (xmlFrame == NULL)
			return NULL;

		CnPrint( L"\n:::: [CnFrame Loading] ::::" );
		CnPrint( L":::: [File] %s ::::\n", fullPathFileName.c_str() );

		/// "DeclareRenderTarget"
		ParseRenderTargets( xmlFrame, frame );
		/// "DeclareSceneGraph"
		ParseSceneGraphs( xmlFrame, frame );
		/// "DeclareCamera"
		ParseCameras( xmlFrame, frame );
		/// "Pass"
		ParsePasses( xmlFrame, frame );
		/// "Scene"
		ParseScenes( xmlFrame, frame );
		/// "PostEffect"
		ParsePostEffects( xmlFrame, frame );

		CnFrameManager::Instance()->AddFrame( frame );
		return frame;
	}

	//----------------------------------------------------------------
	/**	@brief	
	*/
	void CnFrameLoader::ParseRenderTargets( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame )
	{
		// 현재 생성된 윈도우
		CnRenderWindow* window = RenderDevice->GetCurrentRenderWindow();
		assert(window);

		/// RenderTargets
		CMiniXML::iterator xmlRT = xmlCurrent.GetChild();
		for (xmlRT=xmlRT.Find(_T("DeclareRenderTarget")); xmlRT != NULL; xmlRT=xmlRT.Find(_T("DeclareRenderTarget")))
		{
			const wchar* rtName = xmlRT.GetAttribute(L"name");
			const CnString typeName = xmlRT.GetAttribute(L"type");

			if (L"RenderTexture" == typeName)
			{
				uint windowWidth = window->GetWidth();
				uint windowHeight = window->GetHeight();
				ushort windowColorDepth = window->GetColorDepth();
				PixelFormat::Code windowPixelFormat = PixelFormat::A8R8G8B8;
				bool enableDepthStencil = false;
				bool enableMipMaps = false;

				CnString attr;
				// format
				attr = xmlRT.GetAttribute(L"format");
				if (!attr.empty())
				{
					windowPixelFormat = PixelFormat::StringToFormat(attr.c_str());
					assert( windowPixelFormat != PixelFormat::NOFORMAT );
				}
				// Determine the width and height.
				attr = xmlRT.GetAttribute( L"width" );
				if (!attr.empty())
				{
					if (attr != L"Screen")
						windowWidth = _wtoi( attr.c_str() );
				}
				attr = xmlRT.GetAttribute( L"height" );
				if (!attr.empty())
				{
					if (attr != L"Screen")
						windowHeight = _wtoi( attr.c_str() );
				}
				// See if we want to scale the size.
				attr = xmlRT.GetAttribute( L"scale" );
				if (!attr.empty())
				{
					float fScale = (float)_wtof( attr.c_str() );
					windowWidth = uint( windowWidth * fScale );
					windowHeight = uint( windowHeight * fScale );
				}

				// depth
				attr = xmlRT.GetAttribute( L"depth" );
				if (!attr.empty())
				{
					if (attr == L"true")
					{
						enableDepthStencil = true;
					}
				}
				// mipmap
				const wchar* mipmap = xmlRT.GetAttribute(L"mipmaps");
				if (mipmap)
				{
					attr = mipmap;
					unicode::LowerCase(attr);
					if (attr == L"true")
					{
						enableMipMaps = true;
					}
				}

				// 새로운 랜더 타겟 추가
				CnRenderTexture* renderTarget =	RenderDevice->CreateRenderTexture( rtName,
																				   windowWidth,
																				   windowHeight,
																				   windowColorDepth,
																				   windowPixelFormat,
																				   CnAntiAliasQuality::None,
																				   enableDepthStencil,
																				   enableMipMaps,
																				   true );
				//renderTarget->Active( false );
			}
			else if (L"MultiRenderTarget" == typeName)
			{
				uint windowWidth = window->GetWidth();
				uint windowHeight = window->GetHeight();
				ushort windowColorDepth = window->GetColorDepth();
				PixelFormat::Code windowPixelFormat = PixelFormat::A8R8G8B8;
				bool enableDepthStencil = false;

				CnString attr;
				attr = xmlRT.GetAttribute(L"format");
				if (!attr.empty())
				{
					windowPixelFormat = PixelFormat::StringToFormat( attr.c_str() );
					assert( windowPixelFormat != PixelFormat::NOFORMAT );
				}
				// Determine the width and height.
				attr = xmlRT.GetAttribute( L"width" );
				if (!attr.empty())
				{
					if (attr != L"Screen")
						windowWidth = _wtoi( attr.c_str() );
				}
				attr = xmlRT.GetAttribute( L"height" );
				if (!attr.empty())
				{
					if (attr != L"Screen")
						windowHeight = _wtoi( attr.c_str() );
				}
				// See if we want to scale the size.
				attr = xmlRT.GetAttribute( L"scale" );
				if (!attr.empty())
				{
					float fScale = (float)_wtof( attr.c_str() );
					windowWidth = uint( windowWidth * fScale );
					windowHeight = uint( windowHeight * fScale );
				}

				CnMultiRenderTarget* renderTarget = RenderDevice->CreateMultiRenderTarget( rtName, true );

				// 'RenderTarget'
				int surfaceId = 0;
				CMiniXML::iterator xmlSubRT =  xmlRT.GetChild();
				for (xmlSubRT=xmlSubRT.Find(_T("RenderTarget")); xmlSubRT != NULL; xmlSubRT=xmlSubRT.Find(_T("RenderTarget")))
				{
					// name
					CnString nameAttr = xmlSubRT.GetAttribute(L"name");
					// depth
					CnString depthAttr = xmlSubRT.GetAttribute( L"depth" );
					if (depthAttr == L"true")
					{
						enableDepthStencil = true;
					}
					else
					{
						enableDepthStencil = false;
					}

					renderTarget->BindSurface(surfaceId++, 
											  nameAttr.c_str(),
											  windowWidth,
											  windowHeight,
											  windowColorDepth,
											  windowPixelFormat,
											  enableDepthStencil);
				}
			}
			//else if (L"RenderWindow" == typeName)
			//{
			//	const wchar* width = xmlRT.GetAttribute(L"width");
			//	const wchar* height = xmlRT.GetAttribute(L"height");
			//	const wchar* colorDepth = xmlRT.GetAttribute(L"colordepth");
			//	const wchar* refresh = xmlRT.GetAttribute(L"refresh");
			//	const wchar* fullscreen = xmlRT.GetAttribute(L"fullScreen");
			//}
			else
			{
				assert(0);
			}
		}
	}

	//----------------------------------------------------------------
	/**	@brief	SceneGraph 정보 설정
	*/
	void CnFrameLoader::ParseSceneGraphs( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame )
	{
		CMiniXML::iterator xmlNode = xmlCurrent.GetChild();
		for (xmlNode=xmlNode.Find(_T("DeclareSceneGraph")); xmlNode != NULL; xmlNode=xmlNode.Find(_T("DeclareSceneGraph")))
		{
			const wchar* name = xmlNode.GetAttribute(L"name");

			CnCindy::Instance()->AddSceneGraph(name);
		}
	}

	//----------------------------------------------------------------
	/**	@brief	Camera 정보 설정
	*/
	void CnFrameLoader::ParseCameras( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame )
	{
		CMiniXML::iterator xmlNode = xmlCurrent.GetChild();
		for (xmlNode=xmlNode.Find(_T("DeclareCamera")); xmlNode != NULL; xmlNode=xmlNode.Find(_T("DeclareCamera")))
		{
			const wchar* name = xmlNode.GetAttribute(L"name");
			const CnString type = xmlNode.GetAttribute(L"type");

			if (type == L"ModelView")
			{
				Ptr<CnModelViewCamera> camera = CnModelViewCamera::Create();
				camera->SetName(name);

				CnCindy::Instance()->AddCamera( camera.Cast<CnCamera>() );

				CMiniXML::iterator xmlChildNode = xmlNode.GetChild();
				for (; xmlChildNode != NULL; xmlChildNode=xmlChildNode.Next())
				{
					const wchar* nodeName = xmlChildNode.GetName();
					if (0==wcscmp(nodeName, L"Frustum"))
					{
						const wchar* fov = xmlChildNode.GetAttribute(L"fov");
						const wchar* aspect = xmlChildNode.GetAttribute(L"aspect");
						const wchar* nearPlane = xmlChildNode.GetAttribute(L"near");
						const wchar* farPlane = xmlChildNode.GetAttribute(L"far");
						const wchar* clipRange = xmlChildNode.GetAttribute(L"clip");

						camera->SetFrustum(unicode::ToFloat(fov),
										   unicode::ToFloat(aspect),
										   unicode::ToFloat(nearPlane),
										   unicode::ToFloat(farPlane),
										   unicode::ToFloat(clipRange));
					}
					else if (0==wcscmp(nodeName, L"Radius"))
					{
						const wchar* minValue = xmlChildNode.GetAttribute(L"min");
						const wchar* maxValue = xmlChildNode.GetAttribute(L"max");
						const wchar* defaultValue = xmlChildNode.GetAttribute(L"default");

						camera->SetRadius(unicode::ToFloat(minValue), unicode::ToFloat(maxValue));
						camera->SetRadius(unicode::ToFloat(defaultValue));
					}
					else if (0==wcscmp(nodeName, L"Orientation"))
					{
						const wchar* position = xmlChildNode.GetAttribute(L"position");
						const wchar* lookAt = xmlChildNode.GetAttribute(L"lookAt");

						CnStringArray tokens;
						tokens = unicode::Tokenize(position, L", \t");
						camera->SetPosition(Math::Vector3(unicode::ToFloat(tokens[0]), unicode::ToFloat(tokens[1]), unicode::ToFloat(tokens[2])));
						tokens = unicode::Tokenize(lookAt, L", \t");
						camera->SetLookAt(Math::Vector3(unicode::ToFloat(tokens[0]), unicode::ToFloat(tokens[1]), unicode::ToFloat(tokens[2])));
					}
				}
			}
			else if (type == L"FirstPerson")
			{
			}
			else
			{
				assert(0);
			}
		}
	}

	//----------------------------------------------------------------
	/**	@brief	Pass 정보 설정
	*/
	void CnFrameLoader::ParsePasses( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame )
	{
		/// Passes
		CMiniXML::iterator xmlNode = xmlCurrent.GetChild();
		for (xmlNode=xmlNode.Find(_T("Pass")); xmlNode != NULL; xmlNode=xmlNode.Find(_T("Pass")))
		{
			const wchar* sceneName = xmlNode.GetAttribute(L"name");
			const wchar* cameraName = xmlNode.GetAttribute(L"camera");
			const wchar* shaderName = xmlNode.GetAttribute(L"shader");
			const wchar* clearColorValues = xmlNode.GetAttribute(L"clearColor");
			const wchar* viewportValues = xmlNode.GetAttribute(L"viewport");
			//const CnString renderTargetName = xmlNode.GetAttribute(L"renderTarget");
			const wchar* renderTargetName = xmlNode.GetAttribute(L"renderTarget");

			/// renderTarget 설정
			CnRenderTarget* renderTarget = 0;
			if (renderTargetName)
			{
				if (CnString(renderTargetName) == L"*FrameBuffer") {
					renderTarget = RenderDevice->GetCurrentRenderWindow();
				}
				else {
					renderTarget = RenderDevice->GetRenderTarget(renderTargetName);
				}
				assert(renderTarget != 0);
			}

			/// Scene 정보..
			Ptr<CnScene> scene = CnCindy::Instance()->AddScene(sceneName);
			/// Camera 정보
			Ptr<CnCamera> camera;
			if (cameraName)
			{
				camera = CnCindy::Instance()->GetCamera(cameraName);
			}
			else
			{
				camera = CnCindy::Instance()->GetDefaultCamera();
			}
			assert(!camera.IsNull());

			CnStringArray tokens;
			CnColor viewportColor;

			/// Viewport
			if (clearColorValues)
			{
				tokens = unicode::Tokenize(clearColorValues, L" \t");
				assert(tokens.size() == 4);
				viewportColor.r = unicode::ToFloat(tokens[0]);
				viewportColor.g = unicode::ToFloat(tokens[1]);
				viewportColor.b = unicode::ToFloat(tokens[2]);
				viewportColor.a = unicode::ToFloat(tokens[3]);
			}

			int zOrder = 0;
			float relTop = 0.0f;
			float relLeft = 0.0f;
			float relWidth = 1.0f;
			float relHeight = 1.0f;
			if (viewportValues)
			{
				tokens = unicode::Tokenize(viewportValues, L" \t");
				assert(tokens.size() == 5);
				zOrder = unicode::ToInt(tokens[0]);
				relTop = unicode::ToFloat(tokens[1]);
				relLeft = unicode::ToFloat(tokens[2]);
				relWidth = unicode::ToFloat(tokens[3]);
				relHeight = unicode::ToFloat(tokens[4]);
			}

			CnViewport* viewport = CnNew CnViewport(renderTarget, zOrder, relTop, relLeft, relWidth, relHeight);
			viewport->SetCamera(camera);
			viewport->SetScene(scene);
			viewport->SetBackgroundColor(viewportColor.GetRGBA());

			///
			CnFramePass* framePass = CnFramePass::Create();
			framePass->SetRenderTarget(renderTarget);
			framePass->SetViewport(viewport);

			Frame->AddPass( framePass );

			/// Shader 정보
			if (shaderName)
			{
				scene->SetShader(shaderName);
			}

			ParseScene( scene, xmlNode );
		}
	}

	//----------------------------------------------------------------
	/**	@brief	Pass 정보 설정
	*/
	void CnFrameLoader::ParseScenes( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame )
	{
		// 현재 생성된 윈도우
		CnRenderWindow* window = RenderDevice->GetCurrentRenderWindow();
		uint windowWidth = window->GetWidth();
		uint windowHeight = window->GetHeight();
		ushort windowColorDepth = window->GetColorDepth();
		PixelFormat::Code windowPixelFormat = PixelFormat::A8R8G8B8;

		/// Scene 설정
		CMiniXML::iterator xmlNode = xmlCurrent.GetChild();
		for (xmlNode=xmlNode.Find(_T("Scene")); xmlNode != NULL; xmlNode=xmlNode.Find(_T("Scene")))
		{
			const wchar* sceneName = xmlNode.GetAttribute(L"name");
			const CnString renderTargetName = xmlNode.GetAttribute(L"renderTarget");
			const wchar* cameraName = xmlNode.GetAttribute(L"camera");
			const wchar* shaderName = xmlNode.GetAttribute(L"shader");
			const wchar* clearColor = xmlNode.GetAttribute(L"clearColor");

			/// renderTarget 설정
			CnRenderTarget* renderTarget = 0;
			if (renderTargetName == L"*FrameBuffer") {
				renderTarget = RenderDevice->GetCurrentRenderWindow();
			}
			else {
				renderTarget = RenderDevice->GetRenderTarget(renderTargetName);
			}
			assert(renderTarget != 0);

			/// Scene 정보..
			Ptr<CnScene> scene = CnCindy::Instance()->AddScene(sceneName);
			/// Camera 정보
			Ptr<CnCamera> camera = CnCindy::Instance()->GetCamera(cameraName);
			assert(!camera.IsNull());

			CnStringArray tokens = unicode::Tokenize(clearColor, L" \t");
			assert(tokens.size() == 4);
			CnColor color(unicode::ToFloat(tokens[0]), unicode::ToFloat(tokens[1]), unicode::ToFloat(tokens[2]), unicode::ToFloat(tokens[3]));

			CnViewport* viewport = renderTarget->AddViewport();
			viewport->SetCamera(camera);
			viewport->SetScene(scene);
			viewport->SetBackgroundColor(color.GetRGBA());

			/// Shader 정보
			if (shaderName)
			{
				scene->SetShader(shaderName);
			}
			
			ParseScene( scene, xmlNode );
		}
	}

	//----------------------------------------------------------------
	/**
	*/
	void CnFrameLoader::ParseScene( const Ptr<CnScene>& scene, CMiniXML::iterator& xmlNode )
	{
		CnRenderWindow* renderWindow = RenderDevice->GetCurrentRenderWindow();
		uint windowWidth = renderWindow->GetWidth();
		uint windowHeight = renderWindow->GetHeight();
		ushort windowColorDepth = renderWindow->GetColorDepth();
		PixelFormat::Code windowPixelFormat = PixelFormat::A8R8G8B8;

		CnStringArray tokens;

		CMiniXML::iterator xmlChildNode = xmlNode.GetChild();
		for (;xmlChildNode != NULL; xmlChildNode=xmlChildNode.Next())
		{
			const CnString sceneType = xmlChildNode.GetAttribute(L"type");
			const wchar* componentName = xmlChildNode.GetAttribute(L"name");
			const wchar* dependencyName = xmlChildNode.GetAttribute(L"dependency");
			const wchar* materialName = xmlChildNode.GetAttribute(L"material");

			/// load material
			MtlDataPtr materialResource;
			if (materialName)
			{
				materialResource = MaterialDataMgr->Load(materialName);
			}

			/// Type 별 설정
			if (CnString(L"UpdateScene") == sceneType)
			{
				Ptr<Scene::UpdateScene> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::UpdateScene>();
				if (component.IsNull())
				{
					component = Scene::UpdateScene::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}

				/// sceneGraph
				const wchar* graphName = xmlChildNode.GetAttribute(L"sceneGraph");
				Ptr<CnSceneGraph> sceneGraph = CnCindy::Instance()->GetSceneGraph(graphName);
				if (!sceneGraph.IsNull())
				{
					component->AddSceneGraph(sceneGraph);
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"BeginScene") == sceneType)
			{
				Ptr<Scene::BeginScene> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::BeginScene>();
				if (component.IsNull())
				{
					component = Scene::BeginScene::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}

				const wchar* clearFlag = xmlChildNode.GetAttribute(L"clear");
				if (clearFlag)
				{
					ulong flag = 0;
					tokens = unicode::Tokenize(clearFlag, L" |");
					int tokenSize= (int)tokens.size();
					for (int i=0; i<tokenSize; i++)
					{
						if (L"Target" == tokens[i]) {
							flag |= IRenderer::CLEAR_TARGET;
						}
						else if (L"ZBuffer" == tokens[i]) {
							flag |= IRenderer::CLEAR_ZBUFFER;
						}
						else if (L"Stencil" == tokens[i]) {
							flag |= IRenderer::CLEAR_STENCIL;
						}
						else {
							//assert(0);
						}
					}
					component->SetClearFlag(flag);
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"EndScene") == sceneType)
			{
				Ptr<Scene::EndScene> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::EndScene>();
				if (component.IsNull())
				{
					component = Scene::EndScene::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"RenderGeometry") == sceneType)
			{
				Ptr<Scene::RenderGeometry> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::RenderGeometry>();
				if (component.IsNull())
				{
					component = Scene::RenderGeometry::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				///
				CMiniXML::iterator xmlComNode = xmlChildNode.GetChild();
				for (;xmlComNode != NULL; xmlComNode=xmlComNode.Next())
				{
					const CnString nodeName = xmlComNode.GetName();
					if (nodeName == L"Chunk")
					{
						const wchar* type = xmlComNode.GetAttribute(L"type");
						const wchar* feature = xmlComNode.GetAttribute(L"feature");
						const wchar* shader = xmlComNode.GetAttribute(L"shader");
						const wchar* sort = xmlComNode.GetAttribute(L"sort");

						component->AddChunk( type, feature, shader, sort );
					}
				} // for

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			//else if (CnString(L"RenderModel") == sceneType)
			//{
			//	Ptr<Scene::RenderModel> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::RenderModel>();
			//	if (component.IsNull())
			//	{
			//		component = Scene::RenderModel::Create();
			//		SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
			//	}
			//	if (dependencyName)
			//	{
			//		Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
			//		component->Link(dependency);
			//	}

			//	/// component 추가
			//	scene->AddComponent(component.Cast<Scene::Component>());
			//}
			//else if (CnString(L"RenderDepth") == sceneType)
			//{
			//	Ptr<Scene::RenderDepth> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::RenderDepth>();
			//	if (component.IsNull())
			//	{
			//		component = Scene::RenderDepth::Create();
			//		SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
			//	}
			//	if (dependencyName)
			//	{
			//		Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
			//		component->Link(dependency);
			//	}

			//	const wchar* shader = xmlChildNode.GetAttribute(L"shader");
			//	component->SetShader(shader);

			//	/// component 추가
			//	scene->AddComponent(component.Cast<Scene::Component>());
			//}
			else if (CnString(L"RenderText") == sceneType)
			{
				Ptr<Scene::RenderText> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::RenderText>();
				if (component.IsNull())
				{
					component = Scene::RenderText::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"RenderAABB") == sceneType)
			{
				Ptr<Scene::RenderAABB> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::RenderAABB>();
				if (component.IsNull())
				{
					component = Scene::RenderAABB::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			/// LightPrePass Lighting
			else if (CnString(L"LightPrePassDirLight") == sceneType)
			{
				Ptr<Scene::LightPrePass::DirectionLighting> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::LightPrePass::DirectionLighting>();
				if (component.IsNull())
				{
					component = Scene::LightPrePass::DirectionLighting::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				// 초기화!!
				component->Init(windowWidth, windowHeight);

#if 0
				// 매터리얼!!
				CnMaterial* mtl = component->GetMaterial();
				{
					CMiniXML::iterator xmlComNode = xmlChildNode.GetChild();
					for (;xmlComNode != NULL; xmlComNode=xmlComNode.Next())
					{
						const CnString nodeName = xmlComNode.GetName();
						if (nodeName == L"ApplyShader")
						{
							const wchar* shader = xmlComNode.GetAttribute(L"shader");
							mtl->SetShader(shader);
						} 
						else if (nodeName == L"Texture")
						{
							const wchar* texName = xmlComNode.GetAttribute(L"name");
							const CnString sementic = xmlComNode.GetAttribute(L"sementic");

							MapType::Define bindType;

							if (sementic == L"DiffuseMap0")	{
								bindType = MapType::DiffuseMap0;
							}
							else if (sementic == L"DiffuseMap1") {
								bindType = MapType::DiffuseMap1;
							}
							else if (sementic == L"DiffuseMap2") {
								bindType = MapType::DiffuseMap2;
							}
							else {
								assert(0);
							}
							mtl->SetTexture( bindType, texName );
						}
					} // for
				}
#endif
				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"LightPrePassPointLight") == sceneType)
			{
				Ptr<Scene::LightPrePass::PointLighting> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::LightPrePass::PointLighting>();
				if (component.IsNull())
				{
					component = Scene::LightPrePass::PointLighting::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}
#if 0
				// 매터리얼!!
				CnMaterial* mtl = component->GetMaterial();
				{
					///
					CMiniXML::iterator xmlComNode = xmlChildNode.GetChild();
					for (;xmlComNode != NULL; xmlComNode=xmlComNode.Next())
					{
						const CnString nodeName = xmlComNode.GetName();
						if (nodeName == L"ApplyShader")
						{
							const wchar* shader = xmlComNode.GetAttribute(L"shader");
							mtl->SetShader(shader);
						} 
						else if (nodeName == L"Texture")
						{
							const wchar* texName = xmlComNode.GetAttribute(L"name");
							const CnString sementic = xmlComNode.GetAttribute(L"sementic");

							MapType::Define bindType;

							if (sementic == L"DiffuseMap0")	{
								bindType = MapType::DiffuseMap0;
							}
							else if (sementic == L"DiffuseMap1") {
								bindType = MapType::DiffuseMap1;
							}
							else if (sementic == L"DiffuseMap2") {
								bindType = MapType::DiffuseMap2;
							}
							else {
								assert(0);
							}
							mtl->SetTexture( bindType, texName );
						}
					} // for
				}
#endif
				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			/// Deferred Lighting
			else if (CnString(L"DeferredGlobalLight") == sceneType)
			{
				Ptr<Scene::Deferred::GlobalLight> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::Deferred::GlobalLight>();
				if (component.IsNull())
				{
					component = Scene::Deferred::GlobalLight::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				// 초기화!!
				component->Init(windowWidth, windowHeight);
#if 0
				// 매터리얼!!
				CnMaterial* mtl = component->GetMaterial();
				{
					CMiniXML::iterator xmlComNode = xmlChildNode.GetChild();
					for (;xmlComNode != NULL; xmlComNode=xmlComNode.Next())
					{
						const CnString nodeName = xmlComNode.GetName();
						if (nodeName == L"ApplyShader")
						{
							const wchar* shader = xmlComNode.GetAttribute(L"shader");
							mtl->SetShader(shader);
						} 
						else if (nodeName == L"Texture")
						{
							const wchar* texName = xmlComNode.GetAttribute(L"name");
							const CnString sementic = xmlComNode.GetAttribute(L"sementic");

							MapType::Define bindType;

							if (sementic == L"DiffuseMap0")	{
								bindType = MapType::DiffuseMap0;
							}
							else if (sementic == L"DiffuseMap1") {
								bindType = MapType::DiffuseMap1;
							}
							else if (sementic == L"DiffuseMap2") {
								bindType = MapType::DiffuseMap2;
							}
							else {
								assert(0);
							}
							mtl->SetTexture( bindType, texName );
						}
					} // for
				}
#endif
				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"DeferredPointLight") == sceneType)
			{
				Ptr<Scene::Deferred::PointLight> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::Deferred::PointLight>();
				if (component.IsNull())
				{
					component = Scene::Deferred::PointLight::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				// 초기화!!
				component->Open();

				// 매터리얼!!
				CnMaterial* mtl = component->GetMaterial();
				{
					///
					CMiniXML::iterator xmlComNode = xmlChildNode.GetChild();
					for (;xmlComNode != NULL; xmlComNode=xmlComNode.Next())
					{
						const CnString nodeName = xmlComNode.GetName();
						if (nodeName == L"ApplyShader")
						{
							const wchar* shader = xmlComNode.GetAttribute(L"shader");
							mtl->SetShader(shader);
						} 
						else if (nodeName == L"Texture")
						{
							const wchar* texName = xmlComNode.GetAttribute(L"name");
							const CnString sementic = xmlComNode.GetAttribute(L"sementic");

							MapType::Define bindType = MapType::StringToDefine(sementic.c_str());
							if (bindType != MapType::InvalidType)
							{
								mtl->SetTexture( bindType, texName );
							}
						}
					} // for
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			/// Hardware Occlusion
			else if (CnString(L"HardwareOcclusion") == sceneType)
			{
				assert(0);
			}
			/// SkyBox
			else if (CnString(L"RenderSkyBox") == sceneType)
			{
				Ptr<Scene::RenderSkyBox> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::RenderSkyBox>();
				if (component.IsNull())
				{
					component = Scene::RenderSkyBox::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				if (!materialResource.IsNull())
				{
					CnMaterial* newMaterial = materialResource->GetMtl(0)->CreateInstance();
					component->SetMaterial(newMaterial);
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			/// MotionBlur
			//else if (CnString(L"MotionBlur") == sceneType)
			//{
			//	Ptr<Scene::MotionBlur::Velocity> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::MotionBlur::Velocity>();
			//	if (component.IsNull())
			//	{
			//		component = Scene::MotionBlur::Velocity::Create();
			//		SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
			//	}
			//	if (dependencyName)
			//	{
			//		Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
			//		component->Link(dependency);
			//	}
			//	const wchar* shader = xmlChildNode.GetAttribute(L"shader");
			//	component->SetShader(shader);
			//	/// component 추가
			//	scene->AddComponent(component.Cast<Scene::Component>());
			//}
			/// ShadowMapping
			else if (CnString(L"CastShadow") == sceneType)
			{
				Ptr<Scene::CastShadow> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::CastShadow>();
				if (component.IsNull())
				{
					component = Scene::CastShadow::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"BuildShadowMap") == sceneType)
			{
				Ptr<Scene::BuildShadowMap> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::BuildShadowMap>();
				if (component.IsNull())
				{
					component = Scene::BuildShadowMap::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				///
				CMiniXML::iterator xmlComNode = xmlChildNode.GetChild();
				for (;xmlComNode != NULL; xmlComNode=xmlComNode.Next())
				{
					const CnString nodeName = xmlComNode.GetName();
					if (nodeName == L"Chunk")
					{
						const wchar* type = xmlComNode.GetAttribute(L"type");
						const wchar* feature = xmlComNode.GetAttribute(L"feature");
						const wchar* shader = xmlComNode.GetAttribute(L"shader");
						const wchar* sort = xmlComNode.GetAttribute(L"sort");

						component->AddChunk( type, feature, shader, sort );
					}
				} // for

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"DebugLight") == sceneType)
			{
				Ptr<Scene::DebugLight> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::DebugLight>();
				if (component.IsNull())
				{
					component = Scene::DebugLight::Create();
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}
				if (dependencyName)
				{
					Ptr<Scene::Component> dependency = SceneComponentMgr->GetComponent(dependencyName);
					component->Link(dependency);
				}

				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else if (CnString(L"PostProcessComponent") == sceneType)
			{
				Ptr<Scene::PostProcessComponent> component = SceneComponentMgr->GetComponent(componentName).Cast<Scene::PostProcessComponent>();
				if (component.IsNull())
				{
					component = Scene::PostProcessComponent::Create(windowWidth, windowHeight);
					SceneComponentMgr->AddComponent(componentName, component.Cast<Scene::Component>());
				}

				// 초기화!!
				component->Open();
				// 매터리얼!!
				component->SetMaterial(xmlChildNode);
				/// component 추가
				scene->AddComponent(component.Cast<Scene::Component>());
			}
			else
			{
				assert(0);
			}
		}			
	}

	//----------------------------------------------------------------
	/**	@brief	Parse PostEffects
	*/
	void CnFrameLoader::ParsePostEffects( CMiniXML::iterator& xmlCurrent, const Ptr<CnFrame>& Frame )
	{
		CMiniXML::iterator xmlPostEffect = xmlCurrent.GetChild();
		xmlPostEffect = xmlPostEffect.Find(L"DeclarePostEffect");
		if (xmlPostEffect == NULL)
			return;

		//RenderDevice->GetCurrentRenderWindow()->Active(false);

		const wchar* sceneTargetName = xmlPostEffect.GetAttribute(L"sceneTarget");
		if (sceneTargetName)
		{
			Frame->m_PostEffectSystem.SetRT( PostEffect::System::RT_Scene,sceneTargetName );
			//CnRenderTarget* sceneRenderTarget = RenderDevice->GetRenderTarget(sceneTargetName);
			//if (sceneRenderTarget)
			//{
			//	Frame->m_PostEffectSystem.SetRT( PostEffect::System::RT_Scene, sceneRenderTarget );
			//}
		}
		const wchar* depthTargetName = xmlPostEffect.GetAttribute(L"depthTarget");
		if (depthTargetName)
		{
			Frame->m_PostEffectSystem.SetRT( PostEffect::System::RT_Depth, depthTargetName );
			//CnRenderTarget* depthRenderTarget = RenderDevice->GetRenderTarget(depthTargetName);
			//if (depthRenderTarget)
			//{
			//	Frame->m_PostEffectSystem.SetRT( PostEffect::System::RT_Depth, depthRenderTarget );
			//}
		}

		// Effects
		CMiniXML::iterator xmlEffect = xmlPostEffect.GetChild();
		for (xmlEffect=xmlEffect.Find(L"PostEffect"); xmlEffect != NULL; xmlEffect = xmlEffect.Find(L"PostEffect"))
		{
			bool enableEffect = true;
			const wchar* attrEnable = xmlEffect.GetAttribute(L"enable");
			if (attrEnable)
			{
				CnString enable(attrEnable);
				if (enable == L"false")
				{
					enableEffect = false;
				}
			}

			if (enableEffect)
			{
				const wchar* effectName = xmlEffect.GetAttribute(L"name");
				const CnString fileName = xmlEffect.GetAttribute(L"file");
				if (!fileName.empty())
				{
					CnString path = CnFrameManager::Instance()->GetDataPath() + fileName;
					Ptr<PostEffect::Effect> effect = Frame->m_PostEffectSystem.LoadEffect(path);
					if (!effect.IsNull())
					{
						effect->SetName(effectName);
						CnPrint( L":::: [%s] PostEffect Loading ::::\n", effectName );
					}
				} // if
			} // if 
		} // for
	}
} // namespace Cindy