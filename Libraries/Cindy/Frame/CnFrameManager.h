//================================================================
// File:           : CnFrameManager.h
// Original Author : changhee
// Creation Date   : 2009. 2. 9
//================================================================
#ifndef __CN_FRAME_MANAGER_H__
#define __CN_FRAME_MANAGER_H__

#include "CnFrame.h"
#include "CnFrameLoader.h"
#include "Util/CnSingleton.h"
#include "Util/CnUnCopyable.h"

namespace Cindy
{
	class IFrameListener;

	//================================================================
	/** Frame Composite Class
		@author		changhee
		@since		2009. 2. 9
		@brief		프레임 구성 클래스
	*/
	//================================================================
	class CN_DLL CnFrameManager
	{
		__DeclareSingleton(CnFrameManager);
		__DeclareUnCopy(CnFrameManager);
	public:
		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		// Data Path
		void			SetDataPath( const CnString& strDataPath );
		const CnString&	GetDataPath() const;

		// 등록
		void			AddListener( IFrameListener* pListener );
		void			RemoveListener( IFrameListener* pListener );

		// Frame 추가
		void			AddFrame( const Ptr<CnFrame>& Frame );
		bool			HasFrame( const CnString& Name ) const;
		Ptr<CnFrame>	GetFrame( const CnString& Name ) const;

		/// Begin
		void			Begin();
		/// End
		void			End();

	private:
		typedef std::vector<IFrameListener*>	ListenerArray;
		typedef ListenerArray::iterator			ListenerIter;

		typedef HashMap<CnString, Ptr<CnFrame> > FrameMap;

		friend class CnApplication;

		//--------------------------------------------------------------
		//	Variables
		//--------------------------------------------------------------
		// DataPath
		CnString			m_DataPath;
		// Frames
		FrameMap			m_Frames;
		// Listener
		ListenerArray		m_FrameListeners;

		//--------------------------------------------------------------
		//	Methods
		//--------------------------------------------------------------
		CnFrameManager();
		~CnFrameManager();

		// 초기화
		void	Initialize();
		void	DeInitialize();
	};

	//----------------------------------------------------------------
	/**	@brief	데이터 경로 설정
	*/
	inline void
	CnFrameManager::SetDataPath( const CnString& strDataPath )
	{
		m_DataPath = strDataPath;
	}

	//----------------------------------------------------------------
	/**	@brief	데이터 경로 설정
	*/
	inline const CnString&
	CnFrameManager::GetDataPath() const
	{
		return m_DataPath;
	}

#define FrameMgr CnFrameManager::Instance()
}

#endif	// __CN_FRAME_MANAGER_H__